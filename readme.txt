readme.txt

 Created on: Feb 13, 2014
     Author: OlegKarpov

Сборка.

1. Распаковываем boost.tar.gz в корень исходников плагина.
2. mkdir build
3. cd build
4. cmake .. && make

Установка.

1. получить права root.
2. make install из папки build

Паковка.

1. После сборки в папке build - make package.
2. Установить полученный deb с помощью dpkg
