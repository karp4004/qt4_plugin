/** 
 * @file /qt4plugin/libre42/QT4Timer.cxx
 * @date 2014
 * @copyright (c) ЗАО "Голлард"
 * @author Олег В. Карпов [OlegKarpov]
 * @brief
 */
#include "config.h"

#include <unx/saldisp.hxx>
#include "QT4Timer.hxx"

QT4Timer::QT4Timer( SalXLib *pXLib ) : mpXLib( pXLib )
{
}

QT4Timer::~QT4Timer()
{
}

void QT4Timer::Stop()
{
    mpXLib->StopTimer();
}

void QT4Timer::Start( sal_uLong nMS )
{
	  fprintf(stderr, "%s\n", __FUNCTION__);

	  fprintf(stderr, "mpXLib:%p\n", mpXLib);

    mpXLib->StartTimer( nMS );
}



