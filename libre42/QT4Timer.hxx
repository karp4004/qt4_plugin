/** 
 * @file /qt4plugin/libre42/QT4Timer.hxx
 * @date 2014
 * @copyright (c) ЗАО "Голлард"
 * @author Олег В. Карпов [OlegKarpov]
 * @brief
 */

#ifndef QT4TIMER_HXX_
#define QT4TIMER_HXX_

#include <unx/saltimer.h>

class SalXLib;
class QT4Timer: public SalTimer
{
		SalXLib *mpXLib;
	public:
		QT4Timer(SalXLib *pXLib);
		virtual ~QT4Timer();

		virtual void Stop();
		virtual void Start( sal_uLong nMS );

};


#endif /* QT4TIMER_HXX_ */
