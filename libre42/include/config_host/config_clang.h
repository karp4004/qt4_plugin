/* config_host/config_clang.h.  Generated from config_clang.h.in by configure.  */
/*

Settings related to Clang compiler plugins.

*/

#ifndef CONFIG_CLANG_H
#define CONFIG_CLANG_H

#define BUILDDIR "/home/karp4004/workspace/libreoffice"
#define SRCDIR "/home/karp4004/workspace/libreoffice"
#define WORKDIR "/home/karp4004/workspace/libreoffice/workdir"

/* This is actually unused, but it should change whenever Clang changes,
thus causing update of this .h file and triggerring rebuild of our Clang plugin. */
/* #undef CLANG_FULL_VERSION */

#endif
