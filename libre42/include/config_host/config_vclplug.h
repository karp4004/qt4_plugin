/* config_host/config_vclplug.h.  Generated from config_vclplug.h.in by configure.  */
/*

Settings about which X11 desktops have support enabled.

*/

#ifndef CONFIG_VCLPLUG_H
#define CONFIG_VCLPLUG_H

#define ENABLE_GTK 0
#define ENABLE_KDE 0
#define ENABLE_KDE4 0
#define ENABLE_TDE 0
#define ENABLE_GNOME_VFS 0
#define ENABLE_GIO 0

#endif
