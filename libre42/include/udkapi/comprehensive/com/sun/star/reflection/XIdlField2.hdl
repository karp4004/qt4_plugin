#ifndef INCLUDED_COM_SUN_STAR_REFLECTION_XIDLFIELD2_HDL
#define INCLUDED_COM_SUN_STAR_REFLECTION_XIDLFIELD2_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalAccessException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/reflection/FieldAccessMode.hdl"
namespace com { namespace sun { namespace star { namespace reflection { class XIdlClass; } } } }
#include "com/sun/star/reflection/XIdlMember.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace reflection {

class SAL_NO_VTABLE XIdlField2 : public ::css::reflection::XIdlMember
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::reflection::XIdlClass > SAL_CALL getType() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::reflection::FieldAccessMode SAL_CALL getAccessMode() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL get( const ::css::uno::Any& obj ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL set( ::css::uno::Any& obj, const ::css::uno::Any& value ) /* throw (::css::lang::IllegalArgumentException, ::css::lang::IllegalAccessException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XIdlField2() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::reflection::XIdlField2 const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::reflection::XIdlField2 > *) SAL_THROW(());

#endif
