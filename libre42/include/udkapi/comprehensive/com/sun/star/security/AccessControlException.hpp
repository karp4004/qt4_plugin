#ifndef INCLUDED_COM_SUN_STAR_SECURITY_ACCESSCONTROLEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_ACCESSCONTROLEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/security/AccessControlException.hdl"

#include "com/sun/star/uno/SecurityException.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace security {

inline AccessControlException::AccessControlException() SAL_THROW(())
    : ::css::uno::SecurityException()
    , LackingPermission()
{
    ::cppu::UnoType< ::css::security::AccessControlException >::get();
}

inline AccessControlException::AccessControlException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& LackingPermission_) SAL_THROW(())
    : ::css::uno::SecurityException(Message_, Context_)
    , LackingPermission(LackingPermission_)
{
    ::cppu::UnoType< ::css::security::AccessControlException >::get();
}

AccessControlException::AccessControlException(AccessControlException const & the_other): ::css::uno::SecurityException(the_other), LackingPermission(the_other.LackingPermission) {}

AccessControlException::~AccessControlException() {}

AccessControlException & AccessControlException::operator =(AccessControlException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::SecurityException::operator =(the_other);
    LackingPermission = the_other.LackingPermission;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace security { namespace detail {

struct theAccessControlExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theAccessControlExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.security.AccessControlException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::SecurityException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "any" );
        ::rtl::OUString sMemberName0( "LackingPermission" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ANY;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::AccessControlException const *) {
    return *detail::theAccessControlExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::AccessControlException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::AccessControlException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_ACCESSCONTROLEXCEPTION_HPP
