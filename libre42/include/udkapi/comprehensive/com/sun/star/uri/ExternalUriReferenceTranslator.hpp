#ifndef INCLUDED_COM_SUN_STAR_URI_EXTERNALURIREFERENCETRANSLATOR_HPP
#define INCLUDED_COM_SUN_STAR_URI_EXTERNALURIREFERENCETRANSLATOR_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uri/XExternalUriReferenceTranslator.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uri {

class ExternalUriReferenceTranslator {
public:
    static ::css::uno::Reference< ::css::uri::XExternalUriReferenceTranslator > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::uri::XExternalUriReferenceTranslator > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::uri::XExternalUriReferenceTranslator >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.uri.ExternalUriReferenceTranslator" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.uri.ExternalUriReferenceTranslator of type com.sun.star.uri.XExternalUriReferenceTranslator: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.uri.ExternalUriReferenceTranslator of type com.sun.star.uri.XExternalUriReferenceTranslator" ), the_context);
        }
        return the_instance;
    }

private:
    ExternalUriReferenceTranslator(); // not implemented
    ExternalUriReferenceTranslator(ExternalUriReferenceTranslator &); // not implemented
    ~ExternalUriReferenceTranslator(); // not implemented
    void operator =(ExternalUriReferenceTranslator); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_URI_EXTERNALURIREFERENCETRANSLATOR_HPP
