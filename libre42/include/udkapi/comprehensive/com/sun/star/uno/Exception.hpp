#ifndef INCLUDED_COM_SUN_STAR_UNO_EXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UNO_EXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/uno/Exception.hdl"

#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace uno {

inline Exception::Exception() SAL_THROW(())
    : Message()
    , Context()
{
    ::cppu::UnoType< ::css::uno::Exception >::get();
}

inline Exception::Exception(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : Message(Message_)
    , Context(Context_)
{
    ::cppu::UnoType< ::css::uno::Exception >::get();
}

Exception::Exception(Exception const & the_other): Message(the_other.Message), Context(the_other.Context) {}

Exception::~Exception() {}

Exception & Exception::operator =(Exception const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    Message = the_other.Message;
    Context = the_other.Context;
    return *this;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER const ::css::uno::Exception *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Exception >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UNO_EXCEPTION_HPP
