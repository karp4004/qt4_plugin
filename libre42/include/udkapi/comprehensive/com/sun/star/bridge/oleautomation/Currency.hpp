#ifndef INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_CURRENCY_HPP
#define INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_CURRENCY_HPP

#include "sal/config.h"

#include "com/sun/star/bridge/oleautomation/Currency.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation {

inline Currency::Currency() SAL_THROW(())
    : Value(0)
{
}

inline Currency::Currency(const ::sal_Int64& Value_) SAL_THROW(())
    : Value(Value_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation { namespace detail {

struct theCurrencyType : public rtl::StaticWithInit< ::css::uno::Type *, theCurrencyType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.bridge.oleautomation.Currency" );
        ::rtl::OUString the_tname0( "hyper" );
        ::rtl::OUString the_name0( "Value" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_HYPER, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::bridge::oleautomation::Currency const *) {
    return *detail::theCurrencyType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::bridge::oleautomation::Currency const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::bridge::oleautomation::Currency >::get();
}

#endif // INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_CURRENCY_HPP
