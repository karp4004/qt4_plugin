#ifndef INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_DECIMAL_HPP
#define INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_DECIMAL_HPP

#include "sal/config.h"

#include "com/sun/star/bridge/oleautomation/Decimal.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation {

inline Decimal::Decimal() SAL_THROW(())
    : Scale(0)
    , Sign(0)
    , LowValue(0)
    , MiddleValue(0)
    , HighValue(0)
{
}

inline Decimal::Decimal(const ::sal_Int8& Scale_, const ::sal_Int8& Sign_, const ::sal_uInt32& LowValue_, const ::sal_uInt32& MiddleValue_, const ::sal_uInt32& HighValue_) SAL_THROW(())
    : Scale(Scale_)
    , Sign(Sign_)
    , LowValue(LowValue_)
    , MiddleValue(MiddleValue_)
    , HighValue(HighValue_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation { namespace detail {

struct theDecimalType : public rtl::StaticWithInit< ::css::uno::Type *, theDecimalType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.bridge.oleautomation.Decimal" );
        ::rtl::OUString the_tname0( "byte" );
        ::rtl::OUString the_name0( "Scale" );
        ::rtl::OUString the_name1( "Sign" );
        ::rtl::OUString the_tname1( "unsigned long" );
        ::rtl::OUString the_name2( "LowValue" );
        ::rtl::OUString the_name3( "MiddleValue" );
        ::rtl::OUString the_name4( "HighValue" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_UNSIGNED_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_UNSIGNED_LONG, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_UNSIGNED_LONG, the_tname1.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::bridge::oleautomation::Decimal const *) {
    return *detail::theDecimalType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::bridge::oleautomation::Decimal const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::bridge::oleautomation::Decimal >::get();
}

#endif // INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_DECIMAL_HPP
