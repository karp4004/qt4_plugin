#ifndef INCLUDED_COM_SUN_STAR_BRIDGE_INVALIDPROTOCOLCHANGEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_BRIDGE_INVALIDPROTOCOLCHANGEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/bridge/InvalidProtocolChangeException.hdl"

#include "com/sun/star/bridge/ProtocolProperty.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace bridge {

inline InvalidProtocolChangeException::InvalidProtocolChangeException() SAL_THROW(())
    : ::css::uno::Exception()
    , invalidProperty()
    , reason(0)
{
    ::cppu::UnoType< ::css::bridge::InvalidProtocolChangeException >::get();
}

inline InvalidProtocolChangeException::InvalidProtocolChangeException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::bridge::ProtocolProperty& invalidProperty_, const ::sal_Int32& reason_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , invalidProperty(invalidProperty_)
    , reason(reason_)
{
    ::cppu::UnoType< ::css::bridge::InvalidProtocolChangeException >::get();
}

InvalidProtocolChangeException::InvalidProtocolChangeException(InvalidProtocolChangeException const & the_other): ::css::uno::Exception(the_other), invalidProperty(the_other.invalidProperty), reason(the_other.reason) {}

InvalidProtocolChangeException::~InvalidProtocolChangeException() {}

InvalidProtocolChangeException & InvalidProtocolChangeException::operator =(InvalidProtocolChangeException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    invalidProperty = the_other.invalidProperty;
    reason = the_other.reason;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace detail {

struct theInvalidProtocolChangeExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInvalidProtocolChangeExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.bridge.InvalidProtocolChangeException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::bridge::ProtocolProperty >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "com.sun.star.bridge.ProtocolProperty" );
        ::rtl::OUString sMemberName0( "invalidProperty" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRUCT;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "long" );
        ::rtl::OUString sMemberName1( "reason" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace bridge {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::bridge::InvalidProtocolChangeException const *) {
    return *detail::theInvalidProtocolChangeExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::bridge::InvalidProtocolChangeException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::bridge::InvalidProtocolChangeException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_BRIDGE_INVALIDPROTOCOLCHANGEEXCEPTION_HPP
