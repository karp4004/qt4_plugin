#ifndef INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_PROPERTYPUTARGUMENT_HPP
#define INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_PROPERTYPUTARGUMENT_HPP

#include "sal/config.h"

#include "com/sun/star/bridge/oleautomation/PropertyPutArgument.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation {

inline PropertyPutArgument::PropertyPutArgument() SAL_THROW(())
    : Value()
{
}

inline PropertyPutArgument::PropertyPutArgument(const ::css::uno::Any& Value_) SAL_THROW(())
    : Value(Value_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation { namespace detail {

struct thePropertyPutArgumentType : public rtl::StaticWithInit< ::css::uno::Type *, thePropertyPutArgumentType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.bridge.oleautomation.PropertyPutArgument" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "Value" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::bridge::oleautomation::PropertyPutArgument const *) {
    return *detail::thePropertyPutArgumentType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::bridge::oleautomation::PropertyPutArgument const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::bridge::oleautomation::PropertyPutArgument >::get();
}

#endif // INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_PROPERTYPUTARGUMENT_HPP
