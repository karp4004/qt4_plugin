#ifndef INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_NAMEDARGUMENT_HPP
#define INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_NAMEDARGUMENT_HPP

#include "sal/config.h"

#include "com/sun/star/bridge/oleautomation/NamedArgument.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation {

inline NamedArgument::NamedArgument() SAL_THROW(())
    : Name()
    , Value()
{
}

inline NamedArgument::NamedArgument(const ::rtl::OUString& Name_, const ::css::uno::Any& Value_) SAL_THROW(())
    : Name(Name_)
    , Value(Value_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation { namespace detail {

struct theNamedArgumentType : public rtl::StaticWithInit< ::css::uno::Type *, theNamedArgumentType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.bridge.oleautomation.NamedArgument" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "Value" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace bridge { namespace oleautomation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::bridge::oleautomation::NamedArgument const *) {
    return *detail::theNamedArgumentType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::bridge::oleautomation::NamedArgument const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::bridge::oleautomation::NamedArgument >::get();
}

#endif // INCLUDED_COM_SUN_STAR_BRIDGE_OLEAUTOMATION_NAMEDARGUMENT_HPP
