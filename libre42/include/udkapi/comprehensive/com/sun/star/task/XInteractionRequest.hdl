#ifndef INCLUDED_COM_SUN_STAR_TASK_XINTERACTIONREQUEST_HDL
#define INCLUDED_COM_SUN_STAR_TASK_XINTERACTIONREQUEST_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace task { class XInteractionContinuation; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace task {

class SAL_NO_VTABLE XInteractionRequest : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Any SAL_CALL getRequest() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::task::XInteractionContinuation > > SAL_CALL getContinuations() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XInteractionRequest() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::XInteractionRequest const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::task::XInteractionRequest > *) SAL_THROW(());

#endif
