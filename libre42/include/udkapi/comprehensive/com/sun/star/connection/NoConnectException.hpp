#ifndef INCLUDED_COM_SUN_STAR_CONNECTION_NOCONNECTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONNECTION_NOCONNECTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/connection/NoConnectException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace connection {

inline NoConnectException::NoConnectException() SAL_THROW(())
    : ::css::uno::Exception()
{
    ::cppu::UnoType< ::css::connection::NoConnectException >::get();
}

inline NoConnectException::NoConnectException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{
    ::cppu::UnoType< ::css::connection::NoConnectException >::get();
}

NoConnectException::NoConnectException(NoConnectException const & the_other): ::css::uno::Exception(the_other) {}

NoConnectException::~NoConnectException() {}

NoConnectException & NoConnectException::operator =(NoConnectException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace connection { namespace detail {

struct theNoConnectExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theNoConnectExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.connection.NoConnectException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace connection {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::connection::NoConnectException const *) {
    return *detail::theNoConnectExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::connection::NoConnectException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::connection::NoConnectException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONNECTION_NOCONNECTEXCEPTION_HPP
