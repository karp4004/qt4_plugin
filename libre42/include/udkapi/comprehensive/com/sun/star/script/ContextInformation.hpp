#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_CONTEXTINFORMATION_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_CONTEXTINFORMATION_HPP

#include "sal/config.h"

#include "com/sun/star/script/ContextInformation.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline ContextInformation::ContextInformation() SAL_THROW(())
    : Name()
    , SourceCode()
    , StartLine(0)
    , StartColumn(0)
    , EndLine(0)
    , EndColumn(0)
    , LocalVariableNames()
{
}

inline ContextInformation::ContextInformation(const ::rtl::OUString& Name_, const ::rtl::OUString& SourceCode_, const ::sal_Int32& StartLine_, const ::sal_Int32& StartColumn_, const ::sal_Int32& EndLine_, const ::sal_Int32& EndColumn_, const ::css::uno::Sequence< ::rtl::OUString >& LocalVariableNames_) SAL_THROW(())
    : Name(Name_)
    , SourceCode(SourceCode_)
    , StartLine(StartLine_)
    , StartColumn(StartColumn_)
    , EndLine(EndLine_)
    , EndColumn(EndColumn_)
    , LocalVariableNames(LocalVariableNames_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theContextInformationType : public rtl::StaticWithInit< ::css::uno::Type *, theContextInformationType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.ContextInformation" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::rtl::OUString the_name1( "SourceCode" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name2( "StartLine" );
        ::rtl::OUString the_name3( "StartColumn" );
        ::rtl::OUString the_name4( "EndLine" );
        ::rtl::OUString the_name5( "EndColumn" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
        ::rtl::OUString the_tname2( "[]string" );
        ::rtl::OUString the_name6( "LocalVariableNames" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname2.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::ContextInformation const *) {
    return *detail::theContextInformationType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::ContextInformation const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::ContextInformation >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_CONTEXTINFORMATION_HPP
