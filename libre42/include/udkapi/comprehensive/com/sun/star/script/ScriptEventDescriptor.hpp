#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_SCRIPTEVENTDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_SCRIPTEVENTDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/script/ScriptEventDescriptor.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline ScriptEventDescriptor::ScriptEventDescriptor() SAL_THROW(())
    : ListenerType()
    , EventMethod()
    , AddListenerParam()
    , ScriptType()
    , ScriptCode()
{
}

inline ScriptEventDescriptor::ScriptEventDescriptor(const ::rtl::OUString& ListenerType_, const ::rtl::OUString& EventMethod_, const ::rtl::OUString& AddListenerParam_, const ::rtl::OUString& ScriptType_, const ::rtl::OUString& ScriptCode_) SAL_THROW(())
    : ListenerType(ListenerType_)
    , EventMethod(EventMethod_)
    , AddListenerParam(AddListenerParam_)
    , ScriptType(ScriptType_)
    , ScriptCode(ScriptCode_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theScriptEventDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, theScriptEventDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.ScriptEventDescriptor" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "ListenerType" );
        ::rtl::OUString the_name1( "EventMethod" );
        ::rtl::OUString the_name2( "AddListenerParam" );
        ::rtl::OUString the_name3( "ScriptType" );
        ::rtl::OUString the_name4( "ScriptCode" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::ScriptEventDescriptor const *) {
    return *detail::theScriptEventDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::ScriptEventDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::ScriptEventDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_SCRIPTEVENTDESCRIPTOR_HPP
