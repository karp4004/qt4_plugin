#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_INTERRUPTENGINEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_INTERRUPTENGINEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/script/InterruptEngineEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/script/InterruptReason.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline InterruptEngineEvent::InterruptEngineEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Name()
    , SourceCode()
    , StartLine(0)
    , StartColumn(0)
    , EndLine(0)
    , EndColumn(0)
    , ErrorMessage()
    , Reason(::css::script::InterruptReason_Cancel)
{
}

inline InterruptEngineEvent::InterruptEngineEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& Name_, const ::rtl::OUString& SourceCode_, const ::sal_Int32& StartLine_, const ::sal_Int32& StartColumn_, const ::sal_Int32& EndLine_, const ::sal_Int32& EndColumn_, const ::rtl::OUString& ErrorMessage_, const ::css::script::InterruptReason& Reason_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Name(Name_)
    , SourceCode(SourceCode_)
    , StartLine(StartLine_)
    , StartColumn(StartColumn_)
    , EndLine(EndLine_)
    , EndColumn(EndColumn_)
    , ErrorMessage(ErrorMessage_)
    , Reason(Reason_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theInterruptEngineEventType : public rtl::StaticWithInit< ::css::uno::Type *, theInterruptEngineEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.InterruptEngineEvent" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::rtl::OUString the_name1( "SourceCode" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name2( "StartLine" );
        ::rtl::OUString the_name3( "StartColumn" );
        ::rtl::OUString the_name4( "EndLine" );
        ::rtl::OUString the_name5( "EndColumn" );
        ::rtl::OUString the_name6( "ErrorMessage" );
        ::cppu::UnoType< ::css::script::InterruptReason >::get();
        ::rtl::OUString the_tname2( "com.sun.star.script.InterruptReason" );
        ::rtl::OUString the_name7( "Reason" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name6.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname2.pData, the_name7.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 8, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::InterruptEngineEvent const *) {
    return *detail::theInterruptEngineEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::InterruptEngineEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::InterruptEngineEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_INTERRUPTENGINEEVENT_HPP
