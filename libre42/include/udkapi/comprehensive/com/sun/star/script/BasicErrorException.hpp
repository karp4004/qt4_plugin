#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_BASICERROREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_BASICERROREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/script/BasicErrorException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace script {

inline BasicErrorException::BasicErrorException() SAL_THROW(())
    : ::css::uno::Exception()
    , ErrorCode(0)
    , ErrorMessageArgument()
{
    ::cppu::UnoType< ::css::script::BasicErrorException >::get();
}

inline BasicErrorException::BasicErrorException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& ErrorCode_, const ::rtl::OUString& ErrorMessageArgument_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ErrorCode(ErrorCode_)
    , ErrorMessageArgument(ErrorMessageArgument_)
{
    ::cppu::UnoType< ::css::script::BasicErrorException >::get();
}

BasicErrorException::BasicErrorException(BasicErrorException const & the_other): ::css::uno::Exception(the_other), ErrorCode(the_other.ErrorCode), ErrorMessageArgument(the_other.ErrorMessageArgument) {}

BasicErrorException::~BasicErrorException() {}

BasicErrorException & BasicErrorException::operator =(BasicErrorException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ErrorCode = the_other.ErrorCode;
    ErrorMessageArgument = the_other.ErrorMessageArgument;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theBasicErrorExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theBasicErrorExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.script.BasicErrorException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "long" );
        ::rtl::OUString sMemberName0( "ErrorCode" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "ErrorMessageArgument" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::BasicErrorException const *) {
    return *detail::theBasicErrorExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::BasicErrorException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::BasicErrorException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_BASICERROREXCEPTION_HPP
