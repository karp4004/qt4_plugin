#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_INVOCATIONINFO_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_INVOCATIONINFO_HPP

#include "sal/config.h"

#include "com/sun/star/script/InvocationInfo.hdl"

#include "com/sun/star/reflection/ParamMode.hpp"
#include "com/sun/star/script/MemberType.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline InvocationInfo::InvocationInfo() SAL_THROW(())
    : aName()
    , eMemberType(::css::script::MemberType_METHOD)
    , PropertyAttribute(0)
    , aType()
    , aParamTypes()
    , aParamModes()
{
}

inline InvocationInfo::InvocationInfo(const ::rtl::OUString& aName_, const ::css::script::MemberType& eMemberType_, const ::sal_Int16& PropertyAttribute_, const ::css::uno::Type& aType_, const ::css::uno::Sequence< ::css::uno::Type >& aParamTypes_, const ::css::uno::Sequence< ::css::reflection::ParamMode >& aParamModes_) SAL_THROW(())
    : aName(aName_)
    , eMemberType(eMemberType_)
    , PropertyAttribute(PropertyAttribute_)
    , aType(aType_)
    , aParamTypes(aParamTypes_)
    , aParamModes(aParamModes_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theInvocationInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theInvocationInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.InvocationInfo" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "aName" );
        ::cppu::UnoType< ::css::script::MemberType >::get();
        ::rtl::OUString the_tname1( "com.sun.star.script.MemberType" );
        ::rtl::OUString the_name1( "eMemberType" );
        ::rtl::OUString the_tname2( "short" );
        ::rtl::OUString the_name2( "PropertyAttribute" );
        ::rtl::OUString the_tname3( "type" );
        ::rtl::OUString the_name3( "aType" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::uno::Type > >::get();
        ::rtl::OUString the_tname4( "[]type" );
        ::rtl::OUString the_name4( "aParamTypes" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::reflection::ParamMode > >::get();
        ::rtl::OUString the_tname5( "[]com.sun.star.reflection.ParamMode" );
        ::rtl::OUString the_name5( "aParamModes" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_TYPE, the_tname3.pData, the_name3.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname4.pData, the_name4.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname5.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::InvocationInfo const *) {
    return *detail::theInvocationInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::InvocationInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::InvocationInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_INVOCATIONINFO_HPP
