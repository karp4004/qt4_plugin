#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_SCRIPTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_SCRIPTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/script/ScriptEvent.hdl"

#include "com/sun/star/script/AllEventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline ScriptEvent::ScriptEvent() SAL_THROW(())
    : ::css::script::AllEventObject()
    , ScriptType()
    , ScriptCode()
{
}

inline ScriptEvent::ScriptEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Any& Helper_, const ::css::uno::Type& ListenerType_, const ::rtl::OUString& MethodName_, const ::css::uno::Sequence< ::css::uno::Any >& Arguments_, const ::rtl::OUString& ScriptType_, const ::rtl::OUString& ScriptCode_) SAL_THROW(())
    : ::css::script::AllEventObject(Source_, Helper_, ListenerType_, MethodName_, Arguments_)
    , ScriptType(ScriptType_)
    , ScriptCode(ScriptCode_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theScriptEventType : public rtl::StaticWithInit< ::css::uno::Type *, theScriptEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.ScriptEvent" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "ScriptType" );
        ::rtl::OUString the_name1( "ScriptCode" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::script::AllEventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::ScriptEvent const *) {
    return *detail::theScriptEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::ScriptEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::ScriptEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_SCRIPTEVENT_HPP
