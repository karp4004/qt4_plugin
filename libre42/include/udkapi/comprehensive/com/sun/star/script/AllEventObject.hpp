#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_ALLEVENTOBJECT_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_ALLEVENTOBJECT_HPP

#include "sal/config.h"

#include "com/sun/star/script/AllEventObject.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline AllEventObject::AllEventObject() SAL_THROW(())
    : ::css::lang::EventObject()
    , Helper()
    , ListenerType()
    , MethodName()
    , Arguments()
{
}

inline AllEventObject::AllEventObject(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Any& Helper_, const ::css::uno::Type& ListenerType_, const ::rtl::OUString& MethodName_, const ::css::uno::Sequence< ::css::uno::Any >& Arguments_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Helper(Helper_)
    , ListenerType(ListenerType_)
    , MethodName(MethodName_)
    , Arguments(Arguments_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theAllEventObjectType : public rtl::StaticWithInit< ::css::uno::Type *, theAllEventObjectType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.AllEventObject" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "Helper" );
        ::rtl::OUString the_tname1( "type" );
        ::rtl::OUString the_name1( "ListenerType" );
        ::rtl::OUString the_tname2( "string" );
        ::rtl::OUString the_name2( "MethodName" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::uno::Any > >::get();
        ::rtl::OUString the_tname3( "[]any" );
        ::rtl::OUString the_name3( "Arguments" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_TYPE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname3.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::AllEventObject const *) {
    return *detail::theAllEventObjectType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::AllEventObject const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::AllEventObject >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_ALLEVENTOBJECT_HPP
