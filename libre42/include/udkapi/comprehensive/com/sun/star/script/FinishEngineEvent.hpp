#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_FINISHENGINEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_FINISHENGINEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/script/FinishEngineEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/script/FinishReason.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline FinishEngineEvent::FinishEngineEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Finish(::css::script::FinishReason_OK)
    , ErrorMessage()
    , Return()
{
}

inline FinishEngineEvent::FinishEngineEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::script::FinishReason& Finish_, const ::rtl::OUString& ErrorMessage_, const ::css::uno::Any& Return_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Finish(Finish_)
    , ErrorMessage(ErrorMessage_)
    , Return(Return_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theFinishEngineEventType : public rtl::StaticWithInit< ::css::uno::Type *, theFinishEngineEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.FinishEngineEvent" );
        ::cppu::UnoType< ::css::script::FinishReason >::get();
        ::rtl::OUString the_tname0( "com.sun.star.script.FinishReason" );
        ::rtl::OUString the_name0( "Finish" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "ErrorMessage" );
        ::rtl::OUString the_tname2( "any" );
        ::rtl::OUString the_name2( "Return" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::FinishEngineEvent const *) {
    return *detail::theFinishEngineEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::FinishEngineEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::FinishEngineEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_FINISHENGINEEVENT_HPP
