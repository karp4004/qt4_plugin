#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_EVENTLISTENER_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_EVENTLISTENER_HPP

#include "sal/config.h"

#include "com/sun/star/script/EventListener.hdl"

#include "com/sun/star/script/XAllListener.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline EventListener::EventListener() SAL_THROW(())
    : AllListener()
    , Helper()
    , ListenerType()
    , AddListenerParam()
    , EventMethod()
{
}

inline EventListener::EventListener(const ::css::uno::Reference< ::css::script::XAllListener >& AllListener_, const ::css::uno::Any& Helper_, const ::rtl::OUString& ListenerType_, const ::rtl::OUString& AddListenerParam_, const ::rtl::OUString& EventMethod_) SAL_THROW(())
    : AllListener(AllListener_)
    , Helper(Helper_)
    , ListenerType(ListenerType_)
    , AddListenerParam(AddListenerParam_)
    , EventMethod(EventMethod_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theEventListenerType : public rtl::StaticWithInit< ::css::uno::Type *, theEventListenerType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.EventListener" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::script::XAllListener > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.script.XAllListener" );
        ::rtl::OUString the_name0( "AllListener" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "Helper" );
        ::rtl::OUString the_tname2( "string" );
        ::rtl::OUString the_name2( "ListenerType" );
        ::rtl::OUString the_name3( "AddListenerParam" );
        ::rtl::OUString the_name4( "EventMethod" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname2.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::EventListener const *) {
    return *detail::theEventListenerType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::EventListener const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::EventListener >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_EVENTLISTENER_HPP
