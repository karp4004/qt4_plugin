#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_CANNOTCONVERTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_CANNOTCONVERTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/script/CannotConvertException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/TypeClass.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace script {

inline CannotConvertException::CannotConvertException() SAL_THROW(())
    : ::css::uno::Exception()
    , DestinationTypeClass(::css::uno::TypeClass_VOID)
    , Reason(0)
    , ArgumentIndex(0)
{
    ::cppu::UnoType< ::css::script::CannotConvertException >::get();
}

inline CannotConvertException::CannotConvertException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::TypeClass& DestinationTypeClass_, const ::sal_Int32& Reason_, const ::sal_Int32& ArgumentIndex_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , DestinationTypeClass(DestinationTypeClass_)
    , Reason(Reason_)
    , ArgumentIndex(ArgumentIndex_)
{
    ::cppu::UnoType< ::css::script::CannotConvertException >::get();
}

CannotConvertException::CannotConvertException(CannotConvertException const & the_other): ::css::uno::Exception(the_other), DestinationTypeClass(the_other.DestinationTypeClass), Reason(the_other.Reason), ArgumentIndex(the_other.ArgumentIndex) {}

CannotConvertException::~CannotConvertException() {}

CannotConvertException & CannotConvertException::operator =(CannotConvertException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    DestinationTypeClass = the_other.DestinationTypeClass;
    Reason = the_other.Reason;
    ArgumentIndex = the_other.ArgumentIndex;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theCannotConvertExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theCannotConvertExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.script.CannotConvertException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::uno::TypeClass >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "com.sun.star.uno.TypeClass" );
        ::rtl::OUString sMemberName0( "DestinationTypeClass" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ENUM;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "long" );
        ::rtl::OUString sMemberName1( "Reason" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "long" );
        ::rtl::OUString sMemberName2( "ArgumentIndex" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::CannotConvertException const *) {
    return *detail::theCannotConvertExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::CannotConvertException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::CannotConvertException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_CANNOTCONVERTEXCEPTION_HPP
