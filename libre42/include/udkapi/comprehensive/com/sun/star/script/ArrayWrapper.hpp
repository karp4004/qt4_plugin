#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_ARRAYWRAPPER_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_ARRAYWRAPPER_HPP

#include "sal/config.h"

#include "com/sun/star/script/ArrayWrapper.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline ArrayWrapper::ArrayWrapper() SAL_THROW(())
    : IsZeroIndex(false)
    , Array()
{
}

inline ArrayWrapper::ArrayWrapper(const ::sal_Bool& IsZeroIndex_, const ::css::uno::Any& Array_) SAL_THROW(())
    : IsZeroIndex(IsZeroIndex_)
    , Array(Array_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theArrayWrapperType : public rtl::StaticWithInit< ::css::uno::Type *, theArrayWrapperType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.ArrayWrapper" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "IsZeroIndex" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "Array" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::ArrayWrapper const *) {
    return *detail::theArrayWrapperType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::ArrayWrapper const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::ArrayWrapper >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_ARRAYWRAPPER_HPP
