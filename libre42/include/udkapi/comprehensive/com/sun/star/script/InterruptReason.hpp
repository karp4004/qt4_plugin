#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_INTERRUPTREASON_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_INTERRUPTREASON_HPP

#include "sal/config.h"

#include "com/sun/star/script/InterruptReason.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theInterruptReasonType : public rtl::StaticWithInit< ::css::uno::Type *, theInterruptReasonType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.script.InterruptReason" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[8];
        ::rtl::OUString sEnumValue0( "Cancel" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "RuntimeError" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "CompileError" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "BreakPoint" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "Step" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "StepOver" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "StepOut" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "StepStatement" );
        enumValueNames[7] = sEnumValue7.pData;

        sal_Int32 enumValues[8];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::script::InterruptReason_Cancel,
            8, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::InterruptReason const *) {
    return *detail::theInterruptReasonType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::InterruptReason const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::InterruptReason >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_INTERRUPTREASON_HPP
