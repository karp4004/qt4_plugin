#ifndef INCLUDED_COM_SUN_STAR_LANG_XSINGLECOMPONENTFACTORY_HDL
#define INCLUDED_COM_SUN_STAR_LANG_XSINGLECOMPONENTFACTORY_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
namespace com { namespace sun { namespace star { namespace uno { class XComponentContext; } } } }
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace lang {

class SAL_NO_VTABLE XSingleComponentFactory : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::uno::XInterface > SAL_CALL createInstanceWithContext( const ::css::uno::Reference< ::css::uno::XComponentContext >& Context ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::uno::XInterface > SAL_CALL createInstanceWithArgumentsAndContext( const ::css::uno::Sequence< ::css::uno::Any >& Arguments, const ::css::uno::Reference< ::css::uno::XComponentContext >& Context ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSingleComponentFactory() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::lang::XSingleComponentFactory const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::lang::XSingleComponentFactory > *) SAL_THROW(());

#endif
