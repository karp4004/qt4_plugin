#ifndef INCLUDED_COM_SUN_STAR_CONTAINER_XHIERARCHICALNAMEREPLACE_HDL
#define INCLUDED_COM_SUN_STAR_CONTAINER_XHIERARCHICALNAMEREPLACE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/NoSuchElementException.hdl"
#include "com/sun/star/container/XHierarchicalNameAccess.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace container {

class SAL_NO_VTABLE XHierarchicalNameReplace : public ::css::container::XHierarchicalNameAccess
{
public:

    // Methods
    virtual void SAL_CALL replaceByHierarchicalName( const ::rtl::OUString& aName, const ::css::uno::Any& aElement ) /* throw (::css::lang::IllegalArgumentException, ::css::container::NoSuchElementException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XHierarchicalNameReplace() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::container::XHierarchicalNameReplace const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::container::XHierarchicalNameReplace > *) SAL_THROW(());

#endif
