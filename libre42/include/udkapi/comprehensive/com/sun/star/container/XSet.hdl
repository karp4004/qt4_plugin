#ifndef INCLUDED_COM_SUN_STAR_CONTAINER_XSET_HDL
#define INCLUDED_COM_SUN_STAR_CONTAINER_XSET_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/ElementExistException.hdl"
#include "com/sun/star/container/NoSuchElementException.hdl"
#include "com/sun/star/container/XEnumerationAccess.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace container {

class SAL_NO_VTABLE XSet : public ::css::container::XEnumerationAccess
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL has( const ::css::uno::Any& aElement ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL insert( const ::css::uno::Any& aElement ) /* throw (::css::lang::IllegalArgumentException, ::css::container::ElementExistException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL remove( const ::css::uno::Any& aElement ) /* throw (::css::lang::IllegalArgumentException, ::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSet() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::container::XSet const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::container::XSet > *) SAL_THROW(());

#endif
