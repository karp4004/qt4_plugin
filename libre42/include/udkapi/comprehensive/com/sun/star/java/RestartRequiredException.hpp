#ifndef INCLUDED_COM_SUN_STAR_JAVA_RESTARTREQUIREDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_RESTARTREQUIREDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/RestartRequiredException.hdl"

#include "com/sun/star/java/JavaInitializationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace java {

inline RestartRequiredException::RestartRequiredException() SAL_THROW(())
    : ::css::java::JavaInitializationException()
{
    ::cppu::UnoType< ::css::java::RestartRequiredException >::get();
}

inline RestartRequiredException::RestartRequiredException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::java::JavaInitializationException(Message_, Context_)
{
    ::cppu::UnoType< ::css::java::RestartRequiredException >::get();
}

RestartRequiredException::RestartRequiredException(RestartRequiredException const & the_other): ::css::java::JavaInitializationException(the_other) {}

RestartRequiredException::~RestartRequiredException() {}

RestartRequiredException & RestartRequiredException::operator =(RestartRequiredException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::java::JavaInitializationException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java { namespace detail {

struct theRestartRequiredExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theRestartRequiredExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.java.RestartRequiredException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::java::JavaInitializationException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::RestartRequiredException const *) {
    return *detail::theRestartRequiredExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::RestartRequiredException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::RestartRequiredException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_RESTARTREQUIREDEXCEPTION_HPP
