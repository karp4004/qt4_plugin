#ifndef INCLUDED_COM_SUN_STAR_JAVA_JAVAVMCREATIONFAILUREEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_JAVAVMCREATIONFAILUREEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/JavaVMCreationFailureException.hdl"

#include "com/sun/star/java/JavaInitializationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace java {

inline JavaVMCreationFailureException::JavaVMCreationFailureException() SAL_THROW(())
    : ::css::java::JavaInitializationException()
    , ErrorCode(0)
{
    ::cppu::UnoType< ::css::java::JavaVMCreationFailureException >::get();
}

inline JavaVMCreationFailureException::JavaVMCreationFailureException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& ErrorCode_) SAL_THROW(())
    : ::css::java::JavaInitializationException(Message_, Context_)
    , ErrorCode(ErrorCode_)
{
    ::cppu::UnoType< ::css::java::JavaVMCreationFailureException >::get();
}

JavaVMCreationFailureException::JavaVMCreationFailureException(JavaVMCreationFailureException const & the_other): ::css::java::JavaInitializationException(the_other), ErrorCode(the_other.ErrorCode) {}

JavaVMCreationFailureException::~JavaVMCreationFailureException() {}

JavaVMCreationFailureException & JavaVMCreationFailureException::operator =(JavaVMCreationFailureException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::java::JavaInitializationException::operator =(the_other);
    ErrorCode = the_other.ErrorCode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java { namespace detail {

struct theJavaVMCreationFailureExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theJavaVMCreationFailureExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.java.JavaVMCreationFailureException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::java::JavaInitializationException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "long" );
        ::rtl::OUString sMemberName0( "ErrorCode" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::JavaVMCreationFailureException const *) {
    return *detail::theJavaVMCreationFailureExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::JavaVMCreationFailureException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::JavaVMCreationFailureException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_JAVAVMCREATIONFAILUREEXCEPTION_HPP
