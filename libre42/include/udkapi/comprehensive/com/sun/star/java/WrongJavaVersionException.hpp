#ifndef INCLUDED_COM_SUN_STAR_JAVA_WRONGJAVAVERSIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_WRONGJAVAVERSIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/WrongJavaVersionException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace java {

inline WrongJavaVersionException::WrongJavaVersionException() SAL_THROW(())
    : ::css::uno::Exception()
    , LowestSupportedVersion()
    , HighestSupportedVersion()
    , DetectedVersion()
{
    ::cppu::UnoType< ::css::java::WrongJavaVersionException >::get();
}

inline WrongJavaVersionException::WrongJavaVersionException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& LowestSupportedVersion_, const ::rtl::OUString& HighestSupportedVersion_, const ::rtl::OUString& DetectedVersion_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , LowestSupportedVersion(LowestSupportedVersion_)
    , HighestSupportedVersion(HighestSupportedVersion_)
    , DetectedVersion(DetectedVersion_)
{
    ::cppu::UnoType< ::css::java::WrongJavaVersionException >::get();
}

WrongJavaVersionException::WrongJavaVersionException(WrongJavaVersionException const & the_other): ::css::uno::Exception(the_other), LowestSupportedVersion(the_other.LowestSupportedVersion), HighestSupportedVersion(the_other.HighestSupportedVersion), DetectedVersion(the_other.DetectedVersion) {}

WrongJavaVersionException::~WrongJavaVersionException() {}

WrongJavaVersionException & WrongJavaVersionException::operator =(WrongJavaVersionException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    LowestSupportedVersion = the_other.LowestSupportedVersion;
    HighestSupportedVersion = the_other.HighestSupportedVersion;
    DetectedVersion = the_other.DetectedVersion;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java { namespace detail {

struct theWrongJavaVersionExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theWrongJavaVersionExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.java.WrongJavaVersionException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "LowestSupportedVersion" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "HighestSupportedVersion" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "string" );
        ::rtl::OUString sMemberName2( "DetectedVersion" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::WrongJavaVersionException const *) {
    return *detail::theWrongJavaVersionExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::WrongJavaVersionException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::WrongJavaVersionException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_WRONGJAVAVERSIONEXCEPTION_HPP
