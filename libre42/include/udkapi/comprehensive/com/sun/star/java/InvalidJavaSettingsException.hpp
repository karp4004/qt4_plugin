#ifndef INCLUDED_COM_SUN_STAR_JAVA_INVALIDJAVASETTINGSEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_INVALIDJAVASETTINGSEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/InvalidJavaSettingsException.hdl"

#include "com/sun/star/java/JavaInitializationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace java {

inline InvalidJavaSettingsException::InvalidJavaSettingsException() SAL_THROW(())
    : ::css::java::JavaInitializationException()
{
    ::cppu::UnoType< ::css::java::InvalidJavaSettingsException >::get();
}

inline InvalidJavaSettingsException::InvalidJavaSettingsException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::java::JavaInitializationException(Message_, Context_)
{
    ::cppu::UnoType< ::css::java::InvalidJavaSettingsException >::get();
}

InvalidJavaSettingsException::InvalidJavaSettingsException(InvalidJavaSettingsException const & the_other): ::css::java::JavaInitializationException(the_other) {}

InvalidJavaSettingsException::~InvalidJavaSettingsException() {}

InvalidJavaSettingsException & InvalidJavaSettingsException::operator =(InvalidJavaSettingsException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::java::JavaInitializationException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java { namespace detail {

struct theInvalidJavaSettingsExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInvalidJavaSettingsExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.java.InvalidJavaSettingsException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::java::JavaInitializationException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::InvalidJavaSettingsException const *) {
    return *detail::theInvalidJavaSettingsExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::InvalidJavaSettingsException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::InvalidJavaSettingsException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_INVALIDJAVASETTINGSEXCEPTION_HPP
