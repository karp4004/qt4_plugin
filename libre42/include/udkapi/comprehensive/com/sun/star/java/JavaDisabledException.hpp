#ifndef INCLUDED_COM_SUN_STAR_JAVA_JAVADISABLEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_JAVADISABLEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/JavaDisabledException.hdl"

#include "com/sun/star/java/JavaInitializationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace java {

inline JavaDisabledException::JavaDisabledException() SAL_THROW(())
    : ::css::java::JavaInitializationException()
{
    ::cppu::UnoType< ::css::java::JavaDisabledException >::get();
}

inline JavaDisabledException::JavaDisabledException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::java::JavaInitializationException(Message_, Context_)
{
    ::cppu::UnoType< ::css::java::JavaDisabledException >::get();
}

JavaDisabledException::JavaDisabledException(JavaDisabledException const & the_other): ::css::java::JavaInitializationException(the_other) {}

JavaDisabledException::~JavaDisabledException() {}

JavaDisabledException & JavaDisabledException::operator =(JavaDisabledException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::java::JavaInitializationException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java { namespace detail {

struct theJavaDisabledExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theJavaDisabledExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.java.JavaDisabledException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::java::JavaInitializationException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::JavaDisabledException const *) {
    return *detail::theJavaDisabledExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::JavaDisabledException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::JavaDisabledException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_JAVADISABLEDEXCEPTION_HPP
