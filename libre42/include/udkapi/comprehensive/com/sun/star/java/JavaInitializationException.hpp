#ifndef INCLUDED_COM_SUN_STAR_JAVA_JAVAINITIALIZATIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_JAVAINITIALIZATIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/JavaInitializationException.hdl"

#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace java {

inline JavaInitializationException::JavaInitializationException() SAL_THROW(())
    : ::css::uno::DeploymentException()
{
    ::cppu::UnoType< ::css::java::JavaInitializationException >::get();
}

inline JavaInitializationException::JavaInitializationException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::DeploymentException(Message_, Context_)
{
    ::cppu::UnoType< ::css::java::JavaInitializationException >::get();
}

JavaInitializationException::JavaInitializationException(JavaInitializationException const & the_other): ::css::uno::DeploymentException(the_other) {}

JavaInitializationException::~JavaInitializationException() {}

JavaInitializationException & JavaInitializationException::operator =(JavaInitializationException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::DeploymentException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java { namespace detail {

struct theJavaInitializationExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theJavaInitializationExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.java.JavaInitializationException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::DeploymentException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::JavaInitializationException const *) {
    return *detail::theJavaInitializationExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::JavaInitializationException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::JavaInitializationException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_JAVAINITIALIZATIONEXCEPTION_HPP
