#ifndef INCLUDED_COM_SUN_STAR_JAVA_JAVANOTFOUNDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_JAVANOTFOUNDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/JavaNotFoundException.hdl"

#include "com/sun/star/java/JavaInitializationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace java {

inline JavaNotFoundException::JavaNotFoundException() SAL_THROW(())
    : ::css::java::JavaInitializationException()
{
    ::cppu::UnoType< ::css::java::JavaNotFoundException >::get();
}

inline JavaNotFoundException::JavaNotFoundException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::java::JavaInitializationException(Message_, Context_)
{
    ::cppu::UnoType< ::css::java::JavaNotFoundException >::get();
}

JavaNotFoundException::JavaNotFoundException(JavaNotFoundException const & the_other): ::css::java::JavaInitializationException(the_other) {}

JavaNotFoundException::~JavaNotFoundException() {}

JavaNotFoundException & JavaNotFoundException::operator =(JavaNotFoundException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::java::JavaInitializationException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java { namespace detail {

struct theJavaNotFoundExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theJavaNotFoundExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.java.JavaNotFoundException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::java::JavaInitializationException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::JavaNotFoundException const *) {
    return *detail::theJavaNotFoundExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::JavaNotFoundException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::JavaNotFoundException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_JAVANOTFOUNDEXCEPTION_HPP
