#ifndef INCLUDED_COM_SUN_STAR_JAVA_MISSINGJAVARUNTIMEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_MISSINGJAVARUNTIMEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/MissingJavaRuntimeException.hdl"

#include "com/sun/star/java/JavaInitializationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace java {

inline MissingJavaRuntimeException::MissingJavaRuntimeException() SAL_THROW(())
    : ::css::java::JavaInitializationException()
    , URLRuntimeLib()
{
    ::cppu::UnoType< ::css::java::MissingJavaRuntimeException >::get();
}

inline MissingJavaRuntimeException::MissingJavaRuntimeException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& URLRuntimeLib_) SAL_THROW(())
    : ::css::java::JavaInitializationException(Message_, Context_)
    , URLRuntimeLib(URLRuntimeLib_)
{
    ::cppu::UnoType< ::css::java::MissingJavaRuntimeException >::get();
}

MissingJavaRuntimeException::MissingJavaRuntimeException(MissingJavaRuntimeException const & the_other): ::css::java::JavaInitializationException(the_other), URLRuntimeLib(the_other.URLRuntimeLib) {}

MissingJavaRuntimeException::~MissingJavaRuntimeException() {}

MissingJavaRuntimeException & MissingJavaRuntimeException::operator =(MissingJavaRuntimeException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::java::JavaInitializationException::operator =(the_other);
    URLRuntimeLib = the_other.URLRuntimeLib;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java { namespace detail {

struct theMissingJavaRuntimeExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theMissingJavaRuntimeExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.java.MissingJavaRuntimeException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::java::JavaInitializationException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "URLRuntimeLib" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::MissingJavaRuntimeException const *) {
    return *detail::theMissingJavaRuntimeExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::MissingJavaRuntimeException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::MissingJavaRuntimeException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_MISSINGJAVARUNTIMEEXCEPTION_HPP
