#ifndef INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYSET_HDL
#define INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYSET_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyVetoException.hdl"
#include "com/sun/star/beans/UnknownPropertyException.hdl"
namespace com { namespace sun { namespace star { namespace beans { class XPropertyChangeListener; } } } }
namespace com { namespace sun { namespace star { namespace beans { class XPropertySetInfo; } } } }
namespace com { namespace sun { namespace star { namespace beans { class XVetoableChangeListener; } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace beans {

class SAL_NO_VTABLE XPropertySet : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::beans::XPropertySetInfo > SAL_CALL getPropertySetInfo() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPropertyValue( const ::rtl::OUString& aPropertyName, const ::css::uno::Any& aValue ) /* throw (::css::beans::UnknownPropertyException, ::css::beans::PropertyVetoException, ::css::lang::IllegalArgumentException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getPropertyValue( const ::rtl::OUString& PropertyName ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addPropertyChangeListener( const ::rtl::OUString& aPropertyName, const ::css::uno::Reference< ::css::beans::XPropertyChangeListener >& xListener ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removePropertyChangeListener( const ::rtl::OUString& aPropertyName, const ::css::uno::Reference< ::css::beans::XPropertyChangeListener >& aListener ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addVetoableChangeListener( const ::rtl::OUString& PropertyName, const ::css::uno::Reference< ::css::beans::XVetoableChangeListener >& aListener ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeVetoableChangeListener( const ::rtl::OUString& PropertyName, const ::css::uno::Reference< ::css::beans::XVetoableChangeListener >& aListener ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPropertySet() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XPropertySet const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::beans::XPropertySet > *) SAL_THROW(());

#endif
