#ifndef INCLUDED_COM_SUN_STAR_BEANS_STRINGPAIR_HPP
#define INCLUDED_COM_SUN_STAR_BEANS_STRINGPAIR_HPP

#include "sal/config.h"

#include "com/sun/star/beans/StringPair.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace beans {

inline StringPair::StringPair() SAL_THROW(())
    : First()
    , Second()
{
}

inline StringPair::StringPair(const ::rtl::OUString& First_, const ::rtl::OUString& Second_) SAL_THROW(())
    : First(First_)
    , Second(Second_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace beans { namespace detail {

struct theStringPairType : public rtl::StaticWithInit< ::css::uno::Type *, theStringPairType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.beans.StringPair" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "First" );
        ::rtl::OUString the_name1( "Second" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace beans {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::StringPair const *) {
    return *detail::theStringPairType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::beans::StringPair const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::beans::StringPair >::get();
}

#endif // INCLUDED_COM_SUN_STAR_BEANS_STRINGPAIR_HPP
