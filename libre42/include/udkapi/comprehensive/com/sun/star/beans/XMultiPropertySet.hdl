#ifndef INCLUDED_COM_SUN_STAR_BEANS_XMULTIPROPERTYSET_HDL
#define INCLUDED_COM_SUN_STAR_BEANS_XMULTIPROPERTYSET_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyVetoException.hdl"
namespace com { namespace sun { namespace star { namespace beans { class XPropertiesChangeListener; } } } }
namespace com { namespace sun { namespace star { namespace beans { class XPropertySetInfo; } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace beans {

class SAL_NO_VTABLE XMultiPropertySet : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::beans::XPropertySetInfo > SAL_CALL getPropertySetInfo() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPropertyValues( const ::css::uno::Sequence< ::rtl::OUString >& aPropertyNames, const ::css::uno::Sequence< ::css::uno::Any >& aValues ) /* throw (::css::beans::PropertyVetoException, ::css::lang::IllegalArgumentException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Any > SAL_CALL getPropertyValues( const ::css::uno::Sequence< ::rtl::OUString >& aPropertyNames ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addPropertiesChangeListener( const ::css::uno::Sequence< ::rtl::OUString >& aPropertyNames, const ::css::uno::Reference< ::css::beans::XPropertiesChangeListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removePropertiesChangeListener( const ::css::uno::Reference< ::css::beans::XPropertiesChangeListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL firePropertiesChangeEvent( const ::css::uno::Sequence< ::rtl::OUString >& aPropertyNames, const ::css::uno::Reference< ::css::beans::XPropertiesChangeListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XMultiPropertySet() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XMultiPropertySet const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::beans::XMultiPropertySet > *) SAL_THROW(());

#endif
