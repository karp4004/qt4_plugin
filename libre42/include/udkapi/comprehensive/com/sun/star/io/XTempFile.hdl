#ifndef INCLUDED_COM_SUN_STAR_IO_XTEMPFILE_HDL
#define INCLUDED_COM_SUN_STAR_IO_XTEMPFILE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/io/XSeekable.hdl"
#include "com/sun/star/io/XStream.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace io {

class SAL_NO_VTABLE XTempFile : public ::css::io::XStream, public ::css::io::XSeekable
{
public:

    // Attributes
    virtual ::sal_Bool SAL_CALL getRemoveFile() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRemoveFile( ::sal_Bool _removefile ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getUri() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getResourceName() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTempFile() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::io::XTempFile const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::io::XTempFile > *) SAL_THROW(());

#endif
