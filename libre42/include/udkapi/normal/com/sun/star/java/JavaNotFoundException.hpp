#ifndef INCLUDED_COM_SUN_STAR_JAVA_JAVANOTFOUNDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_JAVA_JAVANOTFOUNDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/java/JavaNotFoundException.hdl"

#include "com/sun/star/java/JavaInitializationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace java {

inline JavaNotFoundException::JavaNotFoundException() SAL_THROW(())
    : ::css::java::JavaInitializationException()
{ }

inline JavaNotFoundException::JavaNotFoundException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::java::JavaInitializationException(Message_, Context_)
{ }

JavaNotFoundException::JavaNotFoundException(JavaNotFoundException const & the_other): ::css::java::JavaInitializationException(the_other) {}

JavaNotFoundException::~JavaNotFoundException() {}

JavaNotFoundException & JavaNotFoundException::operator =(JavaNotFoundException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::java::JavaInitializationException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace java {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::java::JavaNotFoundException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.java.JavaNotFoundException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::java::JavaNotFoundException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::java::JavaNotFoundException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_JAVA_JAVANOTFOUNDEXCEPTION_HPP
