#ifndef INCLUDED_COM_SUN_STAR_BRIDGE_XPROTOCOLPROPERTIES_HDL
#define INCLUDED_COM_SUN_STAR_BRIDGE_XPROTOCOLPROPERTIES_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/bridge/InvalidProtocolChangeException.hdl"
#include "com/sun/star/bridge/ProtocolProperty.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace bridge {

class SAL_NO_VTABLE XProtocolProperties : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::bridge::ProtocolProperty > SAL_CALL getProperties() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL requestChange( ::sal_Int32 nRandomNumber ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL commitChange( const ::css::uno::Sequence< ::css::bridge::ProtocolProperty >& newValues ) /* throw (::css::bridge::InvalidProtocolChangeException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XProtocolProperties() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::bridge::XProtocolProperties const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::bridge::XProtocolProperties > *) SAL_THROW(());

#endif
