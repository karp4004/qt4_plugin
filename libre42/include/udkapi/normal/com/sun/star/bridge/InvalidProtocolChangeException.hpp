#ifndef INCLUDED_COM_SUN_STAR_BRIDGE_INVALIDPROTOCOLCHANGEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_BRIDGE_INVALIDPROTOCOLCHANGEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/bridge/InvalidProtocolChangeException.hdl"

#include "com/sun/star/bridge/ProtocolProperty.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace bridge {

inline InvalidProtocolChangeException::InvalidProtocolChangeException() SAL_THROW(())
    : ::css::uno::Exception()
    , invalidProperty()
    , reason(0)
{ }

inline InvalidProtocolChangeException::InvalidProtocolChangeException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::bridge::ProtocolProperty& invalidProperty_, const ::sal_Int32& reason_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , invalidProperty(invalidProperty_)
    , reason(reason_)
{ }

InvalidProtocolChangeException::InvalidProtocolChangeException(InvalidProtocolChangeException const & the_other): ::css::uno::Exception(the_other), invalidProperty(the_other.invalidProperty), reason(the_other.reason) {}

InvalidProtocolChangeException::~InvalidProtocolChangeException() {}

InvalidProtocolChangeException & InvalidProtocolChangeException::operator =(InvalidProtocolChangeException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    invalidProperty = the_other.invalidProperty;
    reason = the_other.reason;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace bridge {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::bridge::InvalidProtocolChangeException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.bridge.InvalidProtocolChangeException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::bridge::InvalidProtocolChangeException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::bridge::InvalidProtocolChangeException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_BRIDGE_INVALIDPROTOCOLCHANGEEXCEPTION_HPP
