#ifndef INCLUDED_COM_SUN_STAR_BRIDGE_XUNOURLRESOLVER_HDL
#define INCLUDED_COM_SUN_STAR_BRIDGE_XUNOURLRESOLVER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/connection/ConnectionSetupException.hdl"
#include "com/sun/star/connection/NoConnectException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace bridge {

class SAL_NO_VTABLE XUnoUrlResolver : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::uno::XInterface > SAL_CALL resolve( const ::rtl::OUString& sUnoUrl ) /* throw (::css::connection::NoConnectException, ::css::connection::ConnectionSetupException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XUnoUrlResolver() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::bridge::XUnoUrlResolver const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::bridge::XUnoUrlResolver > *) SAL_THROW(());

#endif
