#ifndef INCLUDED_COM_SUN_STAR_LANG_XCONNECTIONPOINTCONTAINER_HDL
#define INCLUDED_COM_SUN_STAR_LANG_XCONNECTIONPOINTCONTAINER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace lang { class XConnectionPoint; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "com/sun/star/uno/Type.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace lang {

class SAL_NO_VTABLE XConnectionPointContainer : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::uno::Type > SAL_CALL getConnectionPointTypes() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::lang::XConnectionPoint > SAL_CALL queryConnectionPoint( const ::css::uno::Type& aType ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL advise( const ::css::uno::Type& aType, const ::css::uno::Reference< ::css::uno::XInterface >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL unadvise( const ::css::uno::Type& aType, const ::css::uno::Reference< ::css::uno::XInterface >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XConnectionPointContainer() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::lang::XConnectionPointContainer const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::lang::XConnectionPointContainer > *) SAL_THROW(());

#endif
