#ifndef INCLUDED_COM_SUN_STAR_CONTAINER_XHIERARCHICALNAMECONTAINER_HDL
#define INCLUDED_COM_SUN_STAR_CONTAINER_XHIERARCHICALNAMECONTAINER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/ElementExistException.hdl"
#include "com/sun/star/container/NoSuchElementException.hdl"
#include "com/sun/star/container/XHierarchicalNameReplace.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace container {

class SAL_NO_VTABLE XHierarchicalNameContainer : public ::css::container::XHierarchicalNameReplace
{
public:

    // Methods
    virtual void SAL_CALL insertByHierarchicalName( const ::rtl::OUString& aName, const ::css::uno::Any& aElement ) /* throw (::css::lang::IllegalArgumentException, ::css::container::ElementExistException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeByHierarchicalName( const ::rtl::OUString& Name ) /* throw (::css::container::NoSuchElementException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XHierarchicalNameContainer() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::container::XHierarchicalNameContainer const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::container::XHierarchicalNameContainer > *) SAL_THROW(());

#endif
