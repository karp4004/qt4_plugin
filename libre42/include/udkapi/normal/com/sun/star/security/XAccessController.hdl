#ifndef INCLUDED_COM_SUN_STAR_SECURITY_XACCESSCONTROLLER_HDL
#define INCLUDED_COM_SUN_STAR_SECURITY_XACCESSCONTROLLER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/security/AccessControlException.hdl"
namespace com { namespace sun { namespace star { namespace security { class XAccessControlContext; } } } }
namespace com { namespace sun { namespace star { namespace security { class XAction; } } } }
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace security {

class SAL_NO_VTABLE XAccessController : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL checkPermission( const ::css::uno::Any& perm ) /* throw (::css::security::AccessControlException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL doRestricted( const ::css::uno::Reference< ::css::security::XAction >& action, const ::css::uno::Reference< ::css::security::XAccessControlContext >& restriction ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL doPrivileged( const ::css::uno::Reference< ::css::security::XAction >& action, const ::css::uno::Reference< ::css::security::XAccessControlContext >& restriction ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::security::XAccessControlContext > SAL_CALL getContext() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAccessController() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::XAccessController const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::security::XAccessController > *) SAL_THROW(());

#endif
