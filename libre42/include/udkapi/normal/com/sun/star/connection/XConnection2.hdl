#ifndef INCLUDED_COM_SUN_STAR_CONNECTION_XCONNECTION2_HDL
#define INCLUDED_COM_SUN_STAR_CONNECTION_XCONNECTION2_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/connection/XConnection.hdl"
#include "com/sun/star/io/IOException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace connection {

class SAL_NO_VTABLE XConnection2 : public ::css::connection::XConnection
{
public:

    // Methods
    virtual ::sal_Int32 SAL_CALL available() /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL readSomeBytes( ::css::uno::Sequence< ::sal_Int8 >& aData, ::sal_Int32 nMaxBytesToRead ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XConnection2() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::connection::XConnection2 const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::connection::XConnection2 > *) SAL_THROW(());

#endif
