#ifndef INCLUDED_COM_SUN_STAR_IO_XOUTPUTSTREAM_HDL
#define INCLUDED_COM_SUN_STAR_IO_XOUTPUTSTREAM_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/io/BufferSizeExceededException.hdl"
#include "com/sun/star/io/IOException.hdl"
#include "com/sun/star/io/NotConnectedException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace io {

class SAL_NO_VTABLE XOutputStream : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL writeBytes( const ::css::uno::Sequence< ::sal_Int8 >& aData ) /* throw (::css::io::NotConnectedException, ::css::io::BufferSizeExceededException, ::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL flush() /* throw (::css::io::NotConnectedException, ::css::io::BufferSizeExceededException, ::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL closeOutput() /* throw (::css::io::NotConnectedException, ::css::io::BufferSizeExceededException, ::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XOutputStream() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::io::XOutputStream const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::io::XOutputStream > *) SAL_THROW(());

#endif
