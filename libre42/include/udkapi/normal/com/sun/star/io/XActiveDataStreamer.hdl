#ifndef INCLUDED_COM_SUN_STAR_IO_XACTIVEDATASTREAMER_HDL
#define INCLUDED_COM_SUN_STAR_IO_XACTIVEDATASTREAMER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace io { class XStream; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace io {

class SAL_NO_VTABLE XActiveDataStreamer : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setStream( const ::css::uno::Reference< ::css::io::XStream >& aStream ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::io::XStream > SAL_CALL getStream() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XActiveDataStreamer() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::io::XActiveDataStreamer const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::io::XActiveDataStreamer > *) SAL_THROW(());

#endif
