#ifndef INCLUDED_COM_SUN_STAR_IO_DATATRANSFEREVENT_HPP
#define INCLUDED_COM_SUN_STAR_IO_DATATRANSFEREVENT_HPP

#include "sal/config.h"

#include "com/sun/star/io/DataTransferEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace io {

inline DataTransferEvent::DataTransferEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , aException()
{
}

inline DataTransferEvent::DataTransferEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Any& aException_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , aException(aException_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace io {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::io::DataTransferEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.io.DataTransferEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::io::DataTransferEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::io::DataTransferEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_IO_DATATRANSFEREVENT_HPP
