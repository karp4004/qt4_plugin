#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_EVENTLISTENER_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_EVENTLISTENER_HPP

#include "sal/config.h"

#include "com/sun/star/script/EventListener.hdl"

#include "com/sun/star/script/XAllListener.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline EventListener::EventListener() SAL_THROW(())
    : AllListener()
    , Helper()
    , ListenerType()
    , AddListenerParam()
    , EventMethod()
{
}

inline EventListener::EventListener(const ::css::uno::Reference< ::css::script::XAllListener >& AllListener_, const ::css::uno::Any& Helper_, const ::rtl::OUString& ListenerType_, const ::rtl::OUString& AddListenerParam_, const ::rtl::OUString& EventMethod_) SAL_THROW(())
    : AllListener(AllListener_)
    , Helper(Helper_)
    , ListenerType(ListenerType_)
    , AddListenerParam(AddListenerParam_)
    , EventMethod(EventMethod_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::EventListener const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.script.EventListener");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::EventListener const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::EventListener >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_EVENTLISTENER_HPP
