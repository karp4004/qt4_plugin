#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_XAUTOMATIONINVOCATION_HDL
#define INCLUDED_COM_SUN_STAR_SCRIPT_XAUTOMATIONINVOCATION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/reflection/InvocationTargetException.hdl"
#include "com/sun/star/script/CannotConvertException.hdl"
#include "com/sun/star/script/XInvocation.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace script {

class SAL_NO_VTABLE XAutomationInvocation : public ::css::script::XInvocation
{
public:

    // Methods
    virtual ::css::uno::Any SAL_CALL invokeGetProperty( const ::rtl::OUString& aFunctionName, const ::css::uno::Sequence< ::css::uno::Any >& aParams, ::css::uno::Sequence< ::sal_Int16 >& aOutParamIndex, ::css::uno::Sequence< ::css::uno::Any >& aOutParam ) /* throw (::css::lang::IllegalArgumentException, ::css::script::CannotConvertException, ::css::reflection::InvocationTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL invokePutProperty( const ::rtl::OUString& aFunctionName, const ::css::uno::Sequence< ::css::uno::Any >& aParams, ::css::uno::Sequence< ::sal_Int16 >& aOutParamIndex, ::css::uno::Sequence< ::css::uno::Any >& aOutParam ) /* throw (::css::lang::IllegalArgumentException, ::css::script::CannotConvertException, ::css::reflection::InvocationTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAutomationInvocation() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::XAutomationInvocation const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::script::XAutomationInvocation > *) SAL_THROW(());

#endif
