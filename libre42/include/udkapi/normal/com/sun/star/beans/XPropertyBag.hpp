#ifndef INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYBAG_HPP
#define INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYBAG_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/XPropertyBag.hdl"

#include "com/sun/star/beans/XPropertyAccess.hpp"
#include "com/sun/star/beans/XPropertyContainer.hpp"
#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace beans {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XPropertyBag const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.beans.XPropertyBag" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::beans::XPropertyBag > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::beans::XPropertyBag > >::get();
}

::css::uno::Type const & ::css::beans::XPropertyBag::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::beans::XPropertyBag > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYBAG_HPP
