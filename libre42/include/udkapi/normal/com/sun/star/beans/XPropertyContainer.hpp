#ifndef INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYCONTAINER_HPP
#define INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYCONTAINER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/XPropertyContainer.hdl"

#include "com/sun/star/beans/IllegalTypeException.hpp"
#include "com/sun/star/beans/NotRemoveableException.hpp"
#include "com/sun/star/beans/PropertyExistException.hpp"
#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace beans {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XPropertyContainer const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.beans.XPropertyContainer" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::beans::XPropertyContainer > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::beans::XPropertyContainer > >::get();
}

::css::uno::Type const & ::css::beans::XPropertyContainer::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::beans::XPropertyContainer > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYCONTAINER_HPP
