#ifndef INCLUDED_COM_SUN_STAR_BEANS_XHIERARCHICALPROPERTYSETINFO_HDL
#define INCLUDED_COM_SUN_STAR_BEANS_XHIERARCHICALPROPERTYSETINFO_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/Property.hdl"
#include "com/sun/star/beans/UnknownPropertyException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace beans {

class SAL_NO_VTABLE XHierarchicalPropertySetInfo : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::beans::Property SAL_CALL getPropertyByHierarchicalName( const ::rtl::OUString& aHierarchicalName ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL hasPropertyByHierarchicalName( const ::rtl::OUString& aHierarchicalName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XHierarchicalPropertySetInfo() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XHierarchicalPropertySetInfo const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::beans::XHierarchicalPropertySetInfo > *) SAL_THROW(());

#endif
