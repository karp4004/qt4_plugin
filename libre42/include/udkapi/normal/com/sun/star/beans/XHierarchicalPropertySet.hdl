#ifndef INCLUDED_COM_SUN_STAR_BEANS_XHIERARCHICALPROPERTYSET_HDL
#define INCLUDED_COM_SUN_STAR_BEANS_XHIERARCHICALPROPERTYSET_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyVetoException.hdl"
#include "com/sun/star/beans/UnknownPropertyException.hdl"
namespace com { namespace sun { namespace star { namespace beans { class XHierarchicalPropertySetInfo; } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace beans {

class SAL_NO_VTABLE XHierarchicalPropertySet : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::beans::XHierarchicalPropertySetInfo > SAL_CALL getHierarchicalPropertySetInfo() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setHierarchicalPropertyValue( const ::rtl::OUString& aHierarchicalPropertyName, const ::css::uno::Any& aValue ) /* throw (::css::beans::UnknownPropertyException, ::css::beans::PropertyVetoException, ::css::lang::IllegalArgumentException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getHierarchicalPropertyValue( const ::rtl::OUString& aHierarchicalPropertyName ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::IllegalArgumentException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XHierarchicalPropertySet() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XHierarchicalPropertySet const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::beans::XHierarchicalPropertySet > *) SAL_THROW(());

#endif
