#ifndef INCLUDED_COM_SUN_STAR_UNO_XWEAK_HDL
#define INCLUDED_COM_SUN_STAR_UNO_XWEAK_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
namespace com { namespace sun { namespace star { namespace uno { class XAdapter; } } } }
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace uno {

class SAL_NO_VTABLE XWeak : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::uno::XAdapter > SAL_CALL queryAdapter() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XWeak() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::uno::XWeak const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::uno::XWeak > *) SAL_THROW(());

#endif
