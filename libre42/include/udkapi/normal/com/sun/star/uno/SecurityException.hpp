#ifndef INCLUDED_COM_SUN_STAR_UNO_SECURITYEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UNO_SECURITYEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/uno/SecurityException.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace uno {

inline SecurityException::SecurityException() SAL_THROW(())
    : ::css::uno::RuntimeException()
{ }

inline SecurityException::SecurityException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
{ }

SecurityException::SecurityException(SecurityException const & the_other): ::css::uno::RuntimeException(the_other) {}

SecurityException::~SecurityException() {}

SecurityException & SecurityException::operator =(SecurityException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace uno {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::uno::SecurityException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.uno.SecurityException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::SecurityException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::SecurityException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UNO_SECURITYEXCEPTION_HPP
