#ifndef INCLUDED_COM_SUN_STAR_REFLECTION_XIDLARRAY_HDL
#define INCLUDED_COM_SUN_STAR_REFLECTION_XIDLARRAY_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/ArrayIndexOutOfBoundsException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace reflection {

class SAL_NO_VTABLE XIdlArray : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL realloc( ::css::uno::Any& array, ::sal_Int32 length ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getLen( const ::css::uno::Any& array ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL get( const ::css::uno::Any& aArray, ::sal_Int32 nIndex ) /* throw (::css::lang::IllegalArgumentException, ::css::lang::ArrayIndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL set( ::css::uno::Any& aArray, ::sal_Int32 nIndex, const ::css::uno::Any& aNewValue ) /* throw (::css::lang::IllegalArgumentException, ::css::lang::ArrayIndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XIdlArray() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::reflection::XIdlArray const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::reflection::XIdlArray > *) SAL_THROW(());

#endif
