#ifndef INCLUDED_COM_SUN_STAR_REFLECTION_XIDLCLASSPROVIDER_HPP
#define INCLUDED_COM_SUN_STAR_REFLECTION_XIDLCLASSPROVIDER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/reflection/XIdlClassProvider.hdl"

#include "com/sun/star/reflection/XIdlClass.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace reflection {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::reflection::XIdlClassProvider const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.reflection.XIdlClassProvider" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::reflection::XIdlClassProvider > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::reflection::XIdlClassProvider > >::get();
}

::css::uno::Type const & ::css::reflection::XIdlClassProvider::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::reflection::XIdlClassProvider > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REFLECTION_XIDLCLASSPROVIDER_HPP
