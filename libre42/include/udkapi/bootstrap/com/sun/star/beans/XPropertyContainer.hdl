#ifndef INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYCONTAINER_HDL
#define INCLUDED_COM_SUN_STAR_BEANS_XPROPERTYCONTAINER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/IllegalTypeException.hdl"
#include "com/sun/star/beans/NotRemoveableException.hdl"
#include "com/sun/star/beans/PropertyExistException.hdl"
#include "com/sun/star/beans/UnknownPropertyException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace beans {

class SAL_NO_VTABLE XPropertyContainer : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addProperty( const ::rtl::OUString& Name, ::sal_Int16 Attributes, const ::css::uno::Any& DefaultValue ) /* throw (::css::beans::PropertyExistException, ::css::beans::IllegalTypeException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeProperty( const ::rtl::OUString& Name ) /* throw (::css::beans::UnknownPropertyException, ::css::beans::NotRemoveableException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPropertyContainer() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XPropertyContainer const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::beans::XPropertyContainer > *) SAL_THROW(());

#endif
