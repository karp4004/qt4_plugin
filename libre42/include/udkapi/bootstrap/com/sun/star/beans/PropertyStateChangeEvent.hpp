#ifndef INCLUDED_COM_SUN_STAR_BEANS_PROPERTYSTATECHANGEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_BEANS_PROPERTYSTATECHANGEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/beans/PropertyStateChangeEvent.hdl"

#include "com/sun/star/beans/PropertyState.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace beans {

inline PropertyStateChangeEvent::PropertyStateChangeEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , PropertyName()
    , PropertyHandle(0)
    , OldValue(::css::beans::PropertyState_DIRECT_VALUE)
    , NewValue(::css::beans::PropertyState_DIRECT_VALUE)
{
}

inline PropertyStateChangeEvent::PropertyStateChangeEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& PropertyName_, const ::sal_Int32& PropertyHandle_, const ::css::beans::PropertyState& OldValue_, const ::css::beans::PropertyState& NewValue_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , PropertyName(PropertyName_)
    , PropertyHandle(PropertyHandle_)
    , OldValue(OldValue_)
    , NewValue(NewValue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace beans {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::PropertyStateChangeEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_TypeDescriptionReference * the_members[] = {
            ::cppu::UnoType< ::rtl::OUString >::get().getTypeLibType(),
            ::cppu::UnoType< ::sal_Int32 >::get().getTypeLibType(),
            ::cppu::UnoType< ::css::beans::PropertyState >::get().getTypeLibType(),
            ::cppu::UnoType< ::css::beans::PropertyState >::get().getTypeLibType() };
        ::typelib_static_struct_type_init(&the_type, "com.sun.star.beans.PropertyStateChangeEvent", ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 4, the_members, 0);
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::beans::PropertyStateChangeEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::beans::PropertyStateChangeEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_BEANS_PROPERTYSTATECHANGEEVENT_HPP
