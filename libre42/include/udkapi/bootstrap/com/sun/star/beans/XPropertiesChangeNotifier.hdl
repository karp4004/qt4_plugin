#ifndef INCLUDED_COM_SUN_STAR_BEANS_XPROPERTIESCHANGENOTIFIER_HDL
#define INCLUDED_COM_SUN_STAR_BEANS_XPROPERTIESCHANGENOTIFIER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace beans { class XPropertiesChangeListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace beans {

class SAL_NO_VTABLE XPropertiesChangeNotifier : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addPropertiesChangeListener( const ::css::uno::Sequence< ::rtl::OUString >& PropertyNames, const ::css::uno::Reference< ::css::beans::XPropertiesChangeListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removePropertiesChangeListener( const ::css::uno::Sequence< ::rtl::OUString >& PropertyNames, const ::css::uno::Reference< ::css::beans::XPropertiesChangeListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPropertiesChangeNotifier() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XPropertiesChangeNotifier const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::beans::XPropertiesChangeNotifier > *) SAL_THROW(());

#endif
