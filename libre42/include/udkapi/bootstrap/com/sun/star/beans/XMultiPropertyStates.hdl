#ifndef INCLUDED_COM_SUN_STAR_BEANS_XMULTIPROPERTYSTATES_HDL
#define INCLUDED_COM_SUN_STAR_BEANS_XMULTIPROPERTYSTATES_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyState.hdl"
#include "com/sun/star/beans/UnknownPropertyException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace beans {

class SAL_NO_VTABLE XMultiPropertyStates : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::beans::PropertyState > SAL_CALL getPropertyStates( const ::css::uno::Sequence< ::rtl::OUString >& aPropertyName ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setAllPropertiesToDefault() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPropertiesToDefault( const ::css::uno::Sequence< ::rtl::OUString >& aPropertyNames ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Any > SAL_CALL getPropertyDefaults( const ::css::uno::Sequence< ::rtl::OUString >& aPropertyNames ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XMultiPropertyStates() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::beans::XMultiPropertyStates const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::beans::XMultiPropertyStates > *) SAL_THROW(());

#endif
