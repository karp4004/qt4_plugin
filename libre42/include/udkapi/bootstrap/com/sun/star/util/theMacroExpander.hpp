#ifndef INCLUDED_COM_SUN_STAR_UTIL_THEMACROEXPANDER_HPP
#define INCLUDED_COM_SUN_STAR_UTIL_THEMACROEXPANDER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/util/XMacroExpander.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace util {

class theMacroExpander {
public:
    static ::css::uno::Reference< ::css::util::XMacroExpander > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::util::XMacroExpander > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.util.theMacroExpander" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.util.theMacroExpander of type com.sun.star.util.XMacroExpander" ), the_context);
        }
        return instance;
    }

private:
    theMacroExpander(); // not implemented
    theMacroExpander(theMacroExpander &); // not implemented
    ~theMacroExpander(); // not implemented
    void operator =(theMacroExpander); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_UTIL_THEMACROEXPANDER_HPP
