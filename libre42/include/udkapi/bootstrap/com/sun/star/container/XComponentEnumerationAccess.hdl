#ifndef INCLUDED_COM_SUN_STAR_CONTAINER_XCOMPONENTENUMERATIONACCESS_HDL
#define INCLUDED_COM_SUN_STAR_CONTAINER_XCOMPONENTENUMERATIONACCESS_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace container { class XComponentEnumeration; } } } }
#include "com/sun/star/container/XEnumerationAccess.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace container {

class SAL_NO_VTABLE XComponentEnumerationAccess : public ::css::container::XEnumerationAccess
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::container::XComponentEnumeration > SAL_CALL createComponentEnumeration() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XComponentEnumerationAccess() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::container::XComponentEnumerationAccess const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::container::XComponentEnumerationAccess > *) SAL_THROW(());

#endif
