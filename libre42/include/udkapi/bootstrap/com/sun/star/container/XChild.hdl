#ifndef INCLUDED_COM_SUN_STAR_CONTAINER_XCHILD_HDL
#define INCLUDED_COM_SUN_STAR_CONTAINER_XCHILD_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/NoSupportException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace container {

class SAL_NO_VTABLE XChild : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::uno::XInterface > SAL_CALL getParent() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setParent( const ::css::uno::Reference< ::css::uno::XInterface >& Parent ) /* throw (::css::lang::NoSupportException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XChild() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::container::XChild const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::container::XChild > *) SAL_THROW(());

#endif
