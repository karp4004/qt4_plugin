#ifndef INCLUDED_COM_SUN_STAR_CONTAINER_XCOMPONENTENUMERATIONACCESS_HPP
#define INCLUDED_COM_SUN_STAR_CONTAINER_XCOMPONENTENUMERATIONACCESS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/XComponentEnumerationAccess.hdl"

#include "com/sun/star/container/XComponentEnumeration.hpp"
#include "com/sun/star/container/XEnumerationAccess.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace container {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::container::XComponentEnumerationAccess const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< const ::css::uno::Reference< ::css::container::XEnumerationAccess > >::get().getTypeLibType();
        typelib_static_mi_interface_type_init( &the_type, "com.sun.star.container.XComponentEnumerationAccess", 1, aSuperTypes );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::container::XComponentEnumerationAccess > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::container::XComponentEnumerationAccess > >::get();
}

::css::uno::Type const & ::css::container::XComponentEnumerationAccess::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::container::XComponentEnumerationAccess > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_CONTAINER_XCOMPONENTENUMERATIONACCESS_HPP
