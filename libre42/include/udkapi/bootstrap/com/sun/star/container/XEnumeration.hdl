#ifndef INCLUDED_COM_SUN_STAR_CONTAINER_XENUMERATION_HDL
#define INCLUDED_COM_SUN_STAR_CONTAINER_XENUMERATION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/NoSuchElementException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace container {

class SAL_NO_VTABLE XEnumeration : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL hasMoreElements() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL nextElement() /* throw (::css::container::NoSuchElementException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XEnumeration() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::container::XEnumeration const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::container::XEnumeration > *) SAL_THROW(());

#endif
