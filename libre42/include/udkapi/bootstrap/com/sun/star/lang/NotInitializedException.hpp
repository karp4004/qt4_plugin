#ifndef INCLUDED_COM_SUN_STAR_LANG_NOTINITIALIZEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_LANG_NOTINITIALIZEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/lang/NotInitializedException.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace lang {

inline NotInitializedException::NotInitializedException() SAL_THROW(())
    : ::css::uno::RuntimeException()
{ }

inline NotInitializedException::NotInitializedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
{ }

NotInitializedException::NotInitializedException(NotInitializedException const & the_other): ::css::uno::RuntimeException(the_other) {}

NotInitializedException::~NotInitializedException() {}

NotInitializedException & NotInitializedException::operator =(NotInitializedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace lang {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::lang::NotInitializedException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        const ::css::uno::Type& rBaseType = ::cppu::UnoType< const ::css::uno::RuntimeException >::get();

        typelib_static_compound_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.lang.NotInitializedException", rBaseType.getTypeLibType(), 0,  0 );
    }
    return * reinterpret_cast< const ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::lang::NotInitializedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::lang::NotInitializedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LANG_NOTINITIALIZEDEXCEPTION_HPP
