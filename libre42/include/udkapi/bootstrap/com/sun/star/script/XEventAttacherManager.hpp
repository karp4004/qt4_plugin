#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_XEVENTATTACHERMANAGER_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_XEVENTATTACHERMANAGER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/script/XEventAttacherManager.hdl"

#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/ServiceNotRegisteredException.hpp"
#include "com/sun/star/script/ScriptEventDescriptor.hpp"
#include "com/sun/star/script/XScriptListener.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::XEventAttacherManager const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_mi_interface_type_init( &the_type, "com.sun.star.script.XEventAttacherManager", 0, 0 );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::script::XEventAttacherManager > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::script::XEventAttacherManager > >::get();
}

::css::uno::Type const & ::css::script::XEventAttacherManager::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::script::XEventAttacherManager > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_XEVENTATTACHERMANAGER_HPP
