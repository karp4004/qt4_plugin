#ifndef INCLUDED_COM_SUN_STAR_URI_XURISCHEMEPARSER_HDL
#define INCLUDED_COM_SUN_STAR_URI_XURISCHEMEPARSER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
namespace com { namespace sun { namespace star { namespace uri { class XUriReference; } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace uri {

class SAL_NO_VTABLE XUriSchemeParser : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::uri::XUriReference > SAL_CALL parse( const ::rtl::OUString& scheme, const ::rtl::OUString& schemeSpecificPart ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XUriSchemeParser() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::uri::XUriSchemeParser const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::uri::XUriSchemeParser > *) SAL_THROW(());

#endif
