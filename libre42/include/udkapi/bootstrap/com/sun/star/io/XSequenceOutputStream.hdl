#ifndef INCLUDED_COM_SUN_STAR_IO_XSEQUENCEOUTPUTSTREAM_HDL
#define INCLUDED_COM_SUN_STAR_IO_XSEQUENCEOUTPUTSTREAM_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/io/IOException.hdl"
#include "com/sun/star/io/NotConnectedException.hdl"
#include "com/sun/star/io/XOutputStream.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace io {

class SAL_NO_VTABLE XSequenceOutputStream : public ::css::io::XOutputStream
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::sal_Int8 > SAL_CALL getWrittenBytes() /* throw (::css::io::NotConnectedException, ::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSequenceOutputStream() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::io::XSequenceOutputStream const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::io::XSequenceOutputStream > *) SAL_THROW(());

#endif
