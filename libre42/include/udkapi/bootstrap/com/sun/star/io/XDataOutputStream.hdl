#ifndef INCLUDED_COM_SUN_STAR_IO_XDATAOUTPUTSTREAM_HDL
#define INCLUDED_COM_SUN_STAR_IO_XDATAOUTPUTSTREAM_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/io/IOException.hdl"
#include "com/sun/star/io/XOutputStream.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace io {

class SAL_NO_VTABLE XDataOutputStream : public ::css::io::XOutputStream
{
public:

    // Methods
    virtual void SAL_CALL writeBoolean( ::sal_Bool Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL writeByte( ::sal_Int8 Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL writeChar( ::sal_Unicode Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL writeShort( ::sal_Int16 Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL writeLong( ::sal_Int32 Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL writeHyper( ::sal_Int64 Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL writeFloat( float Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL writeDouble( double Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL writeUTF( const ::rtl::OUString& Value ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDataOutputStream() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::io::XDataOutputStream const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::io::XDataOutputStream > *) SAL_THROW(());

#endif
