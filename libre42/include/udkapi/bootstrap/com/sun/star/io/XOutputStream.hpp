#ifndef INCLUDED_COM_SUN_STAR_IO_XOUTPUTSTREAM_HPP
#define INCLUDED_COM_SUN_STAR_IO_XOUTPUTSTREAM_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/io/XOutputStream.hdl"

#include "com/sun/star/io/BufferSizeExceededException.hpp"
#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/io/NotConnectedException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace io {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::io::XOutputStream const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_mi_interface_type_init( &the_type, "com.sun.star.io.XOutputStream", 0, 0 );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::io::XOutputStream > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::io::XOutputStream > >::get();
}

::css::uno::Type const & ::css::io::XOutputStream::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::io::XOutputStream > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_IO_XOUTPUTSTREAM_HPP
