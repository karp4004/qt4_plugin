#ifndef INCLUDED_COM_SUN_STAR_UNO_XNAMINGSERVICE_HPP
#define INCLUDED_COM_SUN_STAR_UNO_XNAMINGSERVICE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/XNamingService.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace uno {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::uno::XNamingService const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_mi_interface_type_init( &the_type, "com.sun.star.uno.XNamingService", 0, 0 );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::uno::XNamingService > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XNamingService > >::get();
}

::css::uno::Type const & ::css::uno::XNamingService::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::uno::XNamingService > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_UNO_XNAMINGSERVICE_HPP
