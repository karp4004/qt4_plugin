#ifndef INCLUDED_COM_SUN_STAR_REFLECTION_THECOREREFLECTION_HPP
#define INCLUDED_COM_SUN_STAR_REFLECTION_THECOREREFLECTION_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/reflection/XIdlReflection.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace reflection {

class theCoreReflection {
public:
    static ::css::uno::Reference< ::css::reflection::XIdlReflection > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::reflection::XIdlReflection > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.reflection.theCoreReflection" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.reflection.theCoreReflection of type com.sun.star.reflection.XIdlReflection" ), the_context);
        }
        return instance;
    }

private:
    theCoreReflection(); // not implemented
    theCoreReflection(theCoreReflection &); // not implemented
    ~theCoreReflection(); // not implemented
    void operator =(theCoreReflection); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_REFLECTION_THECOREREFLECTION_HPP
