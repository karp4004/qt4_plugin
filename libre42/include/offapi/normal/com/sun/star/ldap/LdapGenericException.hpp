#ifndef INCLUDED_COM_SUN_STAR_LDAP_LDAPGENERICEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_LDAP_LDAPGENERICEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ldap/LdapGenericException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace ldap {

inline LdapGenericException::LdapGenericException() SAL_THROW(())
    : ::css::uno::Exception()
    , ErrorCode(0)
{ }

inline LdapGenericException::LdapGenericException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& ErrorCode_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ErrorCode(ErrorCode_)
{ }

LdapGenericException::LdapGenericException(LdapGenericException const & the_other): ::css::uno::Exception(the_other), ErrorCode(the_other.ErrorCode) {}

LdapGenericException::~LdapGenericException() {}

LdapGenericException & LdapGenericException::operator =(LdapGenericException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ErrorCode = the_other.ErrorCode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ldap {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ldap::LdapGenericException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ldap.LdapGenericException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ldap::LdapGenericException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ldap::LdapGenericException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LDAP_LDAPGENERICEXCEPTION_HPP
