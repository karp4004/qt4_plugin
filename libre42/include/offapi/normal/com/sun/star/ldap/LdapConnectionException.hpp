#ifndef INCLUDED_COM_SUN_STAR_LDAP_LDAPCONNECTIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_LDAP_LDAPCONNECTIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ldap/LdapConnectionException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ldap {

inline LdapConnectionException::LdapConnectionException() SAL_THROW(())
    : ::css::uno::Exception()
{ }

inline LdapConnectionException::LdapConnectionException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{ }

LdapConnectionException::LdapConnectionException(LdapConnectionException const & the_other): ::css::uno::Exception(the_other) {}

LdapConnectionException::~LdapConnectionException() {}

LdapConnectionException & LdapConnectionException::operator =(LdapConnectionException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ldap {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ldap::LdapConnectionException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ldap.LdapConnectionException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ldap::LdapConnectionException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ldap::LdapConnectionException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LDAP_LDAPCONNECTIONEXCEPTION_HPP
