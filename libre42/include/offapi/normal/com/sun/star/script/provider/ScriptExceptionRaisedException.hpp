#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_SCRIPTEXCEPTIONRAISEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_SCRIPTEXCEPTIONRAISEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/script/provider/ScriptExceptionRaisedException.hdl"

#include "com/sun/star/script/provider/ScriptErrorRaisedException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace script { namespace provider {

inline ScriptExceptionRaisedException::ScriptExceptionRaisedException() SAL_THROW(())
    : ::css::script::provider::ScriptErrorRaisedException()
    , exceptionType()
{ }

inline ScriptExceptionRaisedException::ScriptExceptionRaisedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& scriptName_, const ::rtl::OUString& language_, const ::sal_Int32& lineNum_, const ::rtl::OUString& exceptionType_) SAL_THROW(())
    : ::css::script::provider::ScriptErrorRaisedException(Message_, Context_, scriptName_, language_, lineNum_)
    , exceptionType(exceptionType_)
{ }

ScriptExceptionRaisedException::ScriptExceptionRaisedException(ScriptExceptionRaisedException const & the_other): ::css::script::provider::ScriptErrorRaisedException(the_other), exceptionType(the_other.exceptionType) {}

ScriptExceptionRaisedException::~ScriptExceptionRaisedException() {}

ScriptExceptionRaisedException & ScriptExceptionRaisedException::operator =(ScriptExceptionRaisedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::script::provider::ScriptErrorRaisedException::operator =(the_other);
    exceptionType = the_other.exceptionType;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace script { namespace provider {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::provider::ScriptExceptionRaisedException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.script.provider.ScriptExceptionRaisedException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::provider::ScriptExceptionRaisedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::provider::ScriptExceptionRaisedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_SCRIPTEXCEPTIONRAISEDEXCEPTION_HPP
