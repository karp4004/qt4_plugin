#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_XLIBRARYCONTAINEREXPORT_HDL
#define INCLUDED_COM_SUN_STAR_SCRIPT_XLIBRARYCONTAINEREXPORT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/NoSuchElementException.hdl"
namespace com { namespace sun { namespace star { namespace task { class XInteractionHandler; } } } }
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace script {

class SAL_NO_VTABLE XLibraryContainerExport : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL exportLibrary( const ::rtl::OUString& Name, const ::rtl::OUString& URL, const ::css::uno::Reference< ::css::task::XInteractionHandler >& Handler ) /* throw (::css::uno::Exception, ::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XLibraryContainerExport() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::XLibraryContainerExport const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::script::XLibraryContainerExport > *) SAL_THROW(());

#endif
