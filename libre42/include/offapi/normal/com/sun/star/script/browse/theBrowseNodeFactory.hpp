#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_BROWSE_THEBROWSENODEFACTORY_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_BROWSE_THEBROWSENODEFACTORY_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/script/browse/XBrowseNodeFactory.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace script { namespace browse {

class theBrowseNodeFactory {
public:
    static ::css::uno::Reference< ::css::script::browse::XBrowseNodeFactory > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::script::browse::XBrowseNodeFactory > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.script.browse.theBrowseNodeFactory" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.script.browse.theBrowseNodeFactory of type com.sun.star.script.browse.XBrowseNodeFactory" ), the_context);
        }
        return instance;
    }

private:
    theBrowseNodeFactory(); // not implemented
    theBrowseNodeFactory(theBrowseNodeFactory &); // not implemented
    ~theBrowseNodeFactory(); // not implemented
    void operator =(theBrowseNodeFactory); // not implemented
};

} } } } }

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_BROWSE_THEBROWSENODEFACTORY_HPP
