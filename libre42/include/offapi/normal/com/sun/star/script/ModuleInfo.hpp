#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_MODULEINFO_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_MODULEINFO_HPP

#include "sal/config.h"

#include "com/sun/star/script/ModuleInfo.hdl"

#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline ModuleInfo::ModuleInfo() SAL_THROW(())
    : ModuleObject()
    , ModuleType(0)
{
}

inline ModuleInfo::ModuleInfo(const ::css::uno::Reference< ::css::uno::XInterface >& ModuleObject_, const ::sal_Int32& ModuleType_) SAL_THROW(())
    : ModuleObject(ModuleObject_)
    , ModuleType(ModuleType_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::ModuleInfo const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.script.ModuleInfo");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::ModuleInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::ModuleInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_MODULEINFO_HPP
