#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_NATIVEOBJECTWRAPPER_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_NATIVEOBJECTWRAPPER_HPP

#include "sal/config.h"

#include "com/sun/star/script/NativeObjectWrapper.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline NativeObjectWrapper::NativeObjectWrapper() SAL_THROW(())
    : ObjectId()
{
}

inline NativeObjectWrapper::NativeObjectWrapper(const ::css::uno::Any& ObjectId_) SAL_THROW(())
    : ObjectId(ObjectId_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::NativeObjectWrapper const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.script.NativeObjectWrapper");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::NativeObjectWrapper const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::NativeObjectWrapper >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_NATIVEOBJECTWRAPPER_HPP
