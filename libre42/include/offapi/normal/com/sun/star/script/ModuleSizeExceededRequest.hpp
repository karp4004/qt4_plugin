#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_MODULESIZEEXCEEDEDREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_MODULESIZEEXCEEDEDREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/script/ModuleSizeExceededRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace script {

inline ModuleSizeExceededRequest::ModuleSizeExceededRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , Names()
{ }

inline ModuleSizeExceededRequest::ModuleSizeExceededRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Sequence< ::rtl::OUString >& Names_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Names(Names_)
{ }

ModuleSizeExceededRequest::ModuleSizeExceededRequest(ModuleSizeExceededRequest const & the_other): ::css::uno::Exception(the_other), Names(the_other.Names) {}

ModuleSizeExceededRequest::~ModuleSizeExceededRequest() {}

ModuleSizeExceededRequest & ModuleSizeExceededRequest::operator =(ModuleSizeExceededRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Names = the_other.Names;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::ModuleSizeExceededRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.script.ModuleSizeExceededRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::ModuleSizeExceededRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::ModuleSizeExceededRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_MODULESIZEEXCEEDEDREQUEST_HPP
