#ifndef INCLUDED_COM_SUN_STAR_AUTH_XSSOMANAGER_HPP
#define INCLUDED_COM_SUN_STAR_AUTH_XSSOMANAGER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/auth/XSSOManager.hdl"

#include "com/sun/star/auth/InvalidArgumentException.hpp"
#include "com/sun/star/auth/InvalidCredentialException.hpp"
#include "com/sun/star/auth/InvalidPrincipalException.hpp"
#include "com/sun/star/auth/UnsupportedException.hpp"
#include "com/sun/star/auth/XSSOAcceptorContext.hpp"
#include "com/sun/star/auth/XSSOInitiatorContext.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace auth {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::auth::XSSOManager const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.auth.XSSOManager" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::auth::XSSOManager > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::auth::XSSOManager > >::get();
}

::css::uno::Type const & ::css::auth::XSSOManager::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::auth::XSSOManager > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_AUTH_XSSOMANAGER_HPP
