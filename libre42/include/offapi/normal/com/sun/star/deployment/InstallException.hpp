#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_INSTALLEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_INSTALLEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/InstallException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

inline InstallException::InstallException() SAL_THROW(())
    : ::css::uno::Exception()
    , displayName()
{ }

inline InstallException::InstallException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& displayName_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , displayName(displayName_)
{ }

InstallException::InstallException(InstallException const & the_other): ::css::uno::Exception(the_other), displayName(the_other.displayName) {}

InstallException::~InstallException() {}

InstallException & InstallException::operator =(InstallException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    displayName = the_other.displayName;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::InstallException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.deployment.InstallException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::InstallException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::InstallException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_INSTALLEXCEPTION_HPP
