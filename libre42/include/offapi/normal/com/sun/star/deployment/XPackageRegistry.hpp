#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_XPACKAGEREGISTRY_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_XPACKAGEREGISTRY_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/deployment/XPackageRegistry.hdl"

#include "com/sun/star/deployment/DeploymentException.hpp"
#include "com/sun/star/deployment/InvalidRemovedParameterException.hpp"
#include "com/sun/star/deployment/XPackage.hpp"
#include "com/sun/star/deployment/XPackageTypeInfo.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/ucb/CommandFailedException.hpp"
#include "com/sun/star/ucb/XCommandEnvironment.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::XPackageRegistry const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.deployment.XPackageRegistry" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::deployment::XPackageRegistry > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::deployment::XPackageRegistry > >::get();
}

::css::uno::Type const & ::css::deployment::XPackageRegistry::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::deployment::XPackageRegistry > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_XPACKAGEREGISTRY_HPP
