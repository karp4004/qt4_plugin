#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_THEPACKAGEMANAGERFACTORY_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_THEPACKAGEMANAGERFACTORY_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/deployment/XPackageManagerFactory.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

class thePackageManagerFactory {
public:
    static ::css::uno::Reference< ::css::deployment::XPackageManagerFactory > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::deployment::XPackageManagerFactory > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.deployment.thePackageManagerFactory" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.deployment.thePackageManagerFactory of type com.sun.star.deployment.XPackageManagerFactory" ), the_context);
        }
        return instance;
    }

private:
    thePackageManagerFactory(); // not implemented
    thePackageManagerFactory(thePackageManagerFactory &); // not implemented
    ~thePackageManagerFactory(); // not implemented
    void operator =(thePackageManagerFactory); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_THEPACKAGEMANAGERFACTORY_HPP
