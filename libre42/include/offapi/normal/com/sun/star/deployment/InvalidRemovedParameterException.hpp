#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_INVALIDREMOVEDPARAMETEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_INVALIDREMOVEDPARAMETEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/InvalidRemovedParameterException.hdl"

#include "com/sun/star/deployment/XPackage.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace deployment {

inline InvalidRemovedParameterException::InvalidRemovedParameterException() SAL_THROW(())
    : ::css::uno::Exception()
    , PreviousValue(false)
    , Extension()
{ }

inline InvalidRemovedParameterException::InvalidRemovedParameterException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Bool& PreviousValue_, const ::css::uno::Reference< ::css::deployment::XPackage >& Extension_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , PreviousValue(PreviousValue_)
    , Extension(Extension_)
{ }

InvalidRemovedParameterException::InvalidRemovedParameterException(InvalidRemovedParameterException const & the_other): ::css::uno::Exception(the_other), PreviousValue(the_other.PreviousValue), Extension(the_other.Extension) {}

InvalidRemovedParameterException::~InvalidRemovedParameterException() {}

InvalidRemovedParameterException & InvalidRemovedParameterException::operator =(InvalidRemovedParameterException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    PreviousValue = the_other.PreviousValue;
    Extension = the_other.Extension;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::InvalidRemovedParameterException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.deployment.InvalidRemovedParameterException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::InvalidRemovedParameterException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::InvalidRemovedParameterException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_INVALIDREMOVEDPARAMETEREXCEPTION_HPP
