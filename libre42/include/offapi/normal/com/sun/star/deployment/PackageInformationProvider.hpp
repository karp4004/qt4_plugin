#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_PACKAGEINFORMATIONPROVIDER_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_PACKAGEINFORMATIONPROVIDER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/deployment/XPackageInformationProvider.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

class PackageInformationProvider {
public:
    static ::css::uno::Reference< ::css::deployment::XPackageInformationProvider > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::deployment::XPackageInformationProvider > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.deployment.PackageInformationProvider" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.deployment.PackageInformationProvider of type com.sun.star.deployment.XPackageInformationProvider" ), the_context);
        }
        return instance;
    }

private:
    PackageInformationProvider(); // not implemented
    PackageInformationProvider(PackageInformationProvider &); // not implemented
    ~PackageInformationProvider(); // not implemented
    void operator =(PackageInformationProvider); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_PACKAGEINFORMATIONPROVIDER_HPP
