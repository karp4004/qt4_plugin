#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPLOYMENTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPLOYMENTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/DeploymentException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

inline DeploymentException::DeploymentException() SAL_THROW(())
    : ::css::uno::Exception()
    , Cause()
{ }

inline DeploymentException::DeploymentException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& Cause_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Cause(Cause_)
{ }

DeploymentException::DeploymentException(DeploymentException const & the_other): ::css::uno::Exception(the_other), Cause(the_other.Cause) {}

DeploymentException::~DeploymentException() {}

DeploymentException & DeploymentException::operator =(DeploymentException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Cause = the_other.Cause;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::DeploymentException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.deployment.DeploymentException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::DeploymentException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::DeploymentException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPLOYMENTEXCEPTION_HPP
