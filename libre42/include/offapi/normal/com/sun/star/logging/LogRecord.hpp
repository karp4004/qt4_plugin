#ifndef INCLUDED_COM_SUN_STAR_LOGGING_LOGRECORD_HPP
#define INCLUDED_COM_SUN_STAR_LOGGING_LOGRECORD_HPP

#include "sal/config.h"

#include "com/sun/star/logging/LogRecord.hdl"

#include "com/sun/star/util/DateTime.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace logging {

inline LogRecord::LogRecord() SAL_THROW(())
    : LoggerName()
    , SourceClassName()
    , SourceMethodName()
    , Message()
    , LogTime()
    , SequenceNumber(0)
    , ThreadID()
    , Level(0)
{
}

inline LogRecord::LogRecord(const ::rtl::OUString& LoggerName_, const ::rtl::OUString& SourceClassName_, const ::rtl::OUString& SourceMethodName_, const ::rtl::OUString& Message_, const ::css::util::DateTime& LogTime_, const ::sal_Int64& SequenceNumber_, const ::rtl::OUString& ThreadID_, const ::sal_Int32& Level_) SAL_THROW(())
    : LoggerName(LoggerName_)
    , SourceClassName(SourceClassName_)
    , SourceMethodName(SourceMethodName_)
    , Message(Message_)
    , LogTime(LogTime_)
    , SequenceNumber(SequenceNumber_)
    , ThreadID(ThreadID_)
    , Level(Level_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace logging {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::logging::LogRecord const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.logging.LogRecord");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::logging::LogRecord const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::logging::LogRecord >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LOGGING_LOGRECORD_HPP
