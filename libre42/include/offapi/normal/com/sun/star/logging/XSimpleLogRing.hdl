#ifndef INCLUDED_COM_SUN_STAR_LOGGING_XSIMPLELOGRING_HDL
#define INCLUDED_COM_SUN_STAR_LOGGING_XSIMPLELOGRING_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace logging {

class SAL_NO_VTABLE XSimpleLogRing : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL logString( const ::rtl::OUString& aMessage ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getCollectedLog() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSimpleLogRing() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::logging::XSimpleLogRing const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::logging::XSimpleLogRing > *) SAL_THROW(());

#endif
