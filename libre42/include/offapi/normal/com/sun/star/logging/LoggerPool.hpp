#ifndef INCLUDED_COM_SUN_STAR_LOGGING_LOGGERPOOL_HPP
#define INCLUDED_COM_SUN_STAR_LOGGING_LOGGERPOOL_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/logging/XLoggerPool.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace logging {

class LoggerPool {
public:
    static ::css::uno::Reference< ::css::logging::XLoggerPool > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::logging::XLoggerPool > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.logging.LoggerPool" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.logging.LoggerPool of type com.sun.star.logging.XLoggerPool" ), the_context);
        }
        return instance;
    }

private:
    LoggerPool(); // not implemented
    LoggerPool(LoggerPool &); // not implemented
    ~LoggerPool(); // not implemented
    void operator =(LoggerPool); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_LOGGING_LOGGERPOOL_HPP
