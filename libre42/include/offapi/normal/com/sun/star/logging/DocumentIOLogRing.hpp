#ifndef INCLUDED_COM_SUN_STAR_LOGGING_DOCUMENTIOLOGRING_HPP
#define INCLUDED_COM_SUN_STAR_LOGGING_DOCUMENTIOLOGRING_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/logging/XSimpleLogRing.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace logging {

class DocumentIOLogRing {
public:
    static ::css::uno::Reference< ::css::logging::XSimpleLogRing > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::logging::XSimpleLogRing > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.logging.DocumentIOLogRing" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.logging.DocumentIOLogRing of type com.sun.star.logging.XSimpleLogRing" ), the_context);
        }
        return instance;
    }

private:
    DocumentIOLogRing(); // not implemented
    DocumentIOLogRing(DocumentIOLogRing &); // not implemented
    ~DocumentIOLogRing(); // not implemented
    void operator =(DocumentIOLogRing); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_LOGGING_DOCUMENTIOLOGRING_HPP
