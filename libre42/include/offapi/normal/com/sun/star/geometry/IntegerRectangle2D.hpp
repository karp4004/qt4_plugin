#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERRECTANGLE2D_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERRECTANGLE2D_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/IntegerRectangle2D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline IntegerRectangle2D::IntegerRectangle2D() SAL_THROW(())
    : X1(0)
    , Y1(0)
    , X2(0)
    , Y2(0)
{
}

inline IntegerRectangle2D::IntegerRectangle2D(const ::sal_Int32& X1_, const ::sal_Int32& Y1_, const ::sal_Int32& X2_, const ::sal_Int32& Y2_) SAL_THROW(())
    : X1(X1_)
    , Y1(Y1_)
    , X2(X2_)
    , Y2(Y2_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::IntegerRectangle2D const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.geometry.IntegerRectangle2D");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::IntegerRectangle2D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::IntegerRectangle2D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERRECTANGLE2D_HPP
