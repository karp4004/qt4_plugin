#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_REALSIZE2D_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_REALSIZE2D_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/RealSize2D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline RealSize2D::RealSize2D() SAL_THROW(())
    : Width(0)
    , Height(0)
{
}

inline RealSize2D::RealSize2D(const double& Width_, const double& Height_) SAL_THROW(())
    : Width(Width_)
    , Height(Height_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::RealSize2D const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.geometry.RealSize2D");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::RealSize2D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::RealSize2D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_REALSIZE2D_HPP
