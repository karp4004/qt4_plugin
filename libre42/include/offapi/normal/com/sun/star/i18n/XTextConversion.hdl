#ifndef INCLUDED_COM_SUN_STAR_I18N_XTEXTCONVERSION_HDL
#define INCLUDED_COM_SUN_STAR_I18N_XTEXTCONVERSION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/i18n/TextConversionResult.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/Locale.hdl"
#include "com/sun/star/lang/NoSupportException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace i18n {

class SAL_NO_VTABLE XTextConversion : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::i18n::TextConversionResult SAL_CALL getConversions( const ::rtl::OUString& aText, ::sal_Int32 nStartPos, ::sal_Int32 nLength, const ::css::lang::Locale& Locale, ::sal_Int16 nTextConversionType, ::sal_Int32 nTextConversionOptions ) /* throw (::css::lang::IllegalArgumentException, ::css::lang::NoSupportException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getConversion( const ::rtl::OUString& aText, ::sal_Int32 nStartPos, ::sal_Int32 nLength, const ::css::lang::Locale& Locale, ::sal_Int16 nTextConversionType, ::sal_Int32 nTextConversionOptions ) /* throw (::css::lang::IllegalArgumentException, ::css::lang::NoSupportException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL interactiveConversion( const ::css::lang::Locale& Locale, ::sal_Int16 nTextConversionType, ::sal_Int32 nTextConversionOptions ) /* throw (::css::lang::IllegalArgumentException, ::css::lang::NoSupportException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTextConversion() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::XTextConversion const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::i18n::XTextConversion > *) SAL_THROW(());

#endif
