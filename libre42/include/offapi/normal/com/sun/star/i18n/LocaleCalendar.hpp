#ifndef INCLUDED_COM_SUN_STAR_I18N_LOCALECALENDAR_HPP
#define INCLUDED_COM_SUN_STAR_I18N_LOCALECALENDAR_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/i18n/XCalendar3.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace i18n {

class LocaleCalendar {
public:
    static ::css::uno::Reference< ::css::i18n::XCalendar3 > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::i18n::XCalendar3 > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::i18n::XCalendar3 >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.i18n.LocaleCalendar" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.i18n.LocaleCalendar of type com.sun.star.i18n.XCalendar3: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.i18n.LocaleCalendar of type com.sun.star.i18n.XCalendar3" ), the_context);
        }
        return the_instance;
    }

private:
    LocaleCalendar(); // not implemented
    LocaleCalendar(LocaleCalendar &); // not implemented
    ~LocaleCalendar(); // not implemented
    void operator =(LocaleCalendar); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_I18N_LOCALECALENDAR_HPP
