#ifndef INCLUDED_COM_SUN_STAR_I18N_XINDEXENTRYSUPPLIER_HDL
#define INCLUDED_COM_SUN_STAR_I18N_XINDEXENTRYSUPPLIER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/Locale.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace i18n {

class SAL_NO_VTABLE XIndexEntrySupplier : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::rtl::OUString SAL_CALL getIndexCharacter( const ::rtl::OUString& aIndexEntry, const ::css::lang::Locale& aLocale, const ::rtl::OUString& aSortAlgorithm ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getIndexFollowPageWord( ::sal_Bool bMorePages, const ::css::lang::Locale& aLocale ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XIndexEntrySupplier() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::XIndexEntrySupplier const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::i18n::XIndexEntrySupplier > *) SAL_THROW(());

#endif
