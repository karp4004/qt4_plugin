#ifndef INCLUDED_COM_SUN_STAR_I18N_CALENDARITEM2_HPP
#define INCLUDED_COM_SUN_STAR_I18N_CALENDARITEM2_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/CalendarItem2.hdl"

#include "com/sun/star/i18n/CalendarItem.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline CalendarItem2::CalendarItem2() SAL_THROW(())
    : ::css::i18n::CalendarItem()
    , NarrowName()
{
}

inline CalendarItem2::CalendarItem2(const ::rtl::OUString& ID_, const ::rtl::OUString& AbbrevName_, const ::rtl::OUString& FullName_, const ::rtl::OUString& NarrowName_) SAL_THROW(())
    : ::css::i18n::CalendarItem(ID_, AbbrevName_, FullName_)
    , NarrowName(NarrowName_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::CalendarItem2 const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.i18n.CalendarItem2");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::CalendarItem2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::CalendarItem2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_CALENDARITEM2_HPP
