#ifndef INCLUDED_COM_SUN_STAR_I18N_NATIVENUMBERXMLATTRIBUTES_HPP
#define INCLUDED_COM_SUN_STAR_I18N_NATIVENUMBERXMLATTRIBUTES_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/NativeNumberXmlAttributes.hdl"

#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline NativeNumberXmlAttributes::NativeNumberXmlAttributes() SAL_THROW(())
    : Locale()
    , Format()
    , Style()
{
}

inline NativeNumberXmlAttributes::NativeNumberXmlAttributes(const ::css::lang::Locale& Locale_, const ::rtl::OUString& Format_, const ::rtl::OUString& Style_) SAL_THROW(())
    : Locale(Locale_)
    , Format(Format_)
    , Style(Style_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::NativeNumberXmlAttributes const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.i18n.NativeNumberXmlAttributes");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::NativeNumberXmlAttributes const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::NativeNumberXmlAttributes >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_NATIVENUMBERXMLATTRIBUTES_HPP
