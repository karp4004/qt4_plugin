#ifndef INCLUDED_COM_SUN_STAR_I18N_NUMBERFORMATCODE_HPP
#define INCLUDED_COM_SUN_STAR_I18N_NUMBERFORMATCODE_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/NumberFormatCode.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline NumberFormatCode::NumberFormatCode() SAL_THROW(())
    : Type(0)
    , Usage(0)
    , Code()
    , DefaultName()
    , NameID()
    , Index(0)
    , Default(false)
{
}

inline NumberFormatCode::NumberFormatCode(const ::sal_Int16& Type_, const ::sal_Int16& Usage_, const ::rtl::OUString& Code_, const ::rtl::OUString& DefaultName_, const ::rtl::OUString& NameID_, const ::sal_Int16& Index_, const ::sal_Bool& Default_) SAL_THROW(())
    : Type(Type_)
    , Usage(Usage_)
    , Code(Code_)
    , DefaultName(DefaultName_)
    , NameID(NameID_)
    , Index(Index_)
    , Default(Default_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::NumberFormatCode const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.i18n.NumberFormatCode");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::NumberFormatCode const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::NumberFormatCode >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_NUMBERFORMATCODE_HPP
