#ifndef INCLUDED_COM_SUN_STAR_I18N_BOUNDARY_HPP
#define INCLUDED_COM_SUN_STAR_I18N_BOUNDARY_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/Boundary.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline Boundary::Boundary() SAL_THROW(())
    : startPos(0)
    , endPos(0)
{
}

inline Boundary::Boundary(const ::sal_Int32& startPos_, const ::sal_Int32& endPos_) SAL_THROW(())
    : startPos(startPos_)
    , endPos(endPos_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::Boundary const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.i18n.Boundary");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::Boundary const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::Boundary >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_BOUNDARY_HPP
