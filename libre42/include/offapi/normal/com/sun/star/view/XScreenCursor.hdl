#ifndef INCLUDED_COM_SUN_STAR_VIEW_XSCREENCURSOR_HDL
#define INCLUDED_COM_SUN_STAR_VIEW_XSCREENCURSOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace view {

class SAL_NO_VTABLE XScreenCursor : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL screenDown() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL screenUp() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XScreenCursor() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::view::XScreenCursor const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::view::XScreenCursor > *) SAL_THROW(());

#endif
