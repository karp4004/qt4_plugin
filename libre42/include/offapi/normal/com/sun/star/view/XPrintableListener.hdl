#ifndef INCLUDED_COM_SUN_STAR_VIEW_XPRINTABLELISTENER_HDL
#define INCLUDED_COM_SUN_STAR_VIEW_XPRINTABLELISTENER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/XEventListener.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/view/PrintableStateEvent.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace view {

class SAL_NO_VTABLE XPrintableListener : public ::css::lang::XEventListener
{
public:

    // Methods
    virtual void SAL_CALL stateChanged( const ::css::view::PrintableStateEvent& Event ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPrintableListener() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::view::XPrintableListener const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::view::XPrintableListener > *) SAL_THROW(());

#endif
