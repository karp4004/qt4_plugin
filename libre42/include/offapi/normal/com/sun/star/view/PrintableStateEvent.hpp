#ifndef INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/view/PrintableStateEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/view/PrintableState.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace view {

inline PrintableStateEvent::PrintableStateEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , State(::css::view::PrintableState_JOB_STARTED)
{
}

inline PrintableStateEvent::PrintableStateEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::view::PrintableState& State_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , State(State_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace view {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::view::PrintableStateEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.view.PrintableStateEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::view::PrintableStateEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::view::PrintableStateEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATEEVENT_HPP
