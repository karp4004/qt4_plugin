#ifndef INCLUDED_COM_SUN_STAR_VIEW_XLINECURSOR_HDL
#define INCLUDED_COM_SUN_STAR_VIEW_XLINECURSOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace view {

class SAL_NO_VTABLE XLineCursor : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL isAtStartOfLine() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isAtEndOfLine() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL gotoEndOfLine( ::sal_Bool bExpand ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL gotoStartOfLine( ::sal_Bool bExpand ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XLineCursor() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::view::XLineCursor const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::view::XLineCursor > *) SAL_THROW(());

#endif
