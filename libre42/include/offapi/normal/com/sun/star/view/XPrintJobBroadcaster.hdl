#ifndef INCLUDED_COM_SUN_STAR_VIEW_XPRINTJOBBROADCASTER_HDL
#define INCLUDED_COM_SUN_STAR_VIEW_XPRINTJOBBROADCASTER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
namespace com { namespace sun { namespace star { namespace view { class XPrintJobListener; } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace view {

class SAL_NO_VTABLE XPrintJobBroadcaster : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addPrintJobListener( const ::css::uno::Reference< ::css::view::XPrintJobListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removePrintJobListener( const ::css::uno::Reference< ::css::view::XPrintJobListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPrintJobBroadcaster() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::view::XPrintJobBroadcaster const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::view::XPrintJobBroadcaster > *) SAL_THROW(());

#endif
