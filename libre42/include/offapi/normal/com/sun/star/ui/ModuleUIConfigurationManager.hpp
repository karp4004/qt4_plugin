#ifndef INCLUDED_COM_SUN_STAR_UI_MODULEUICONFIGURATIONMANAGER_HPP
#define INCLUDED_COM_SUN_STAR_UI_MODULEUICONFIGURATIONMANAGER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/configuration/CorruptedUIConfigurationException.hpp"
#include "com/sun/star/lang/WrappedTargetException.hpp"
#include "com/sun/star/ui/XModuleUIConfigurationManager2.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace ui {

class ModuleUIConfigurationManager {
public:
    static ::css::uno::Reference< ::css::ui::XModuleUIConfigurationManager2 > createDefault(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& ModuleShortName, const ::rtl::OUString& ModuleIdentifier) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(2);
        the_arguments[0] <<= ModuleShortName;
        the_arguments[1] <<= ModuleIdentifier;
        ::css::uno::Reference< ::css::ui::XModuleUIConfigurationManager2 > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::ui::XModuleUIConfigurationManager2 >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.ui.ModuleUIConfigurationManager" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::beans::UnknownPropertyException &) {
            throw;
        } catch (const ::css::lang::WrappedTargetException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ui.ModuleUIConfigurationManager of type com.sun.star.ui.XModuleUIConfigurationManager2: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ui.ModuleUIConfigurationManager of type com.sun.star.ui.XModuleUIConfigurationManager2" ), the_context);
        }
        return the_instance;
    }

private:
    ModuleUIConfigurationManager(); // not implemented
    ModuleUIConfigurationManager(ModuleUIConfigurationManager &); // not implemented
    ~ModuleUIConfigurationManager(); // not implemented
    void operator =(ModuleUIConfigurationManager); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_UI_MODULEUICONFIGURATIONMANAGER_HPP
