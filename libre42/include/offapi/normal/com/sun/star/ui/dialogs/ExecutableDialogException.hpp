#ifndef INCLUDED_COM_SUN_STAR_UI_DIALOGS_EXECUTABLEDIALOGEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UI_DIALOGS_EXECUTABLEDIALOGEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ui/dialogs/ExecutableDialogException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

inline ExecutableDialogException::ExecutableDialogException() SAL_THROW(())
    : ::css::uno::Exception()
{ }

inline ExecutableDialogException::ExecutableDialogException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{ }

ExecutableDialogException::ExecutableDialogException(ExecutableDialogException const & the_other): ::css::uno::Exception(the_other) {}

ExecutableDialogException::~ExecutableDialogException() {}

ExecutableDialogException & ExecutableDialogException::operator =(ExecutableDialogException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::dialogs::ExecutableDialogException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ui.dialogs.ExecutableDialogException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::dialogs::ExecutableDialogException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::dialogs::ExecutableDialogException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_DIALOGS_EXECUTABLEDIALOGEXCEPTION_HPP
