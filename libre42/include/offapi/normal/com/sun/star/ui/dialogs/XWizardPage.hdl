#ifndef INCLUDED_COM_SUN_STAR_UI_DIALOGS_XWIZARDPAGE_HDL
#define INCLUDED_COM_SUN_STAR_UI_DIALOGS_XWIZARDPAGE_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { class XWindow; } } } }
#include "com/sun/star/lang/XComponent.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

class SAL_NO_VTABLE XWizardPage : public ::css::lang::XComponent
{
public:

    // Attributes
    virtual ::css::uno::Reference< ::css::awt::XWindow > SAL_CALL getWindow() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getPageId() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual void SAL_CALL activatePage() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL commitPage( ::sal_Int16 Reason ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL canAdvance() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XWizardPage() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::dialogs::XWizardPage const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ui::dialogs::XWizardPage > *) SAL_THROW(());

#endif
