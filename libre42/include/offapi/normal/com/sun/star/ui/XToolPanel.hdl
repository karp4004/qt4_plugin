#ifndef INCLUDED_COM_SUN_STAR_UI_XTOOLPANEL_HDL
#define INCLUDED_COM_SUN_STAR_UI_XTOOLPANEL_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace accessibility { class XAccessible; } } } }
namespace com { namespace sun { namespace star { namespace awt { class XWindow; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ui {

class SAL_NO_VTABLE XToolPanel : public ::css::uno::XInterface
{
public:

    // Attributes
    virtual ::css::uno::Reference< ::css::awt::XWindow > SAL_CALL getWindow() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual ::css::uno::Reference< ::css::accessibility::XAccessible > SAL_CALL createAccessible( const ::css::uno::Reference< ::css::accessibility::XAccessible >& ParentAccessible ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XToolPanel() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::XToolPanel const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ui::XToolPanel > *) SAL_THROW(());

#endif
