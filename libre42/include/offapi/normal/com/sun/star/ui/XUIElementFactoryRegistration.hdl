#ifndef INCLUDED_COM_SUN_STAR_UI_XUIELEMENTFACTORYREGISTRATION_HDL
#define INCLUDED_COM_SUN_STAR_UI_XUIELEMENTFACTORYREGISTRATION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
#include "com/sun/star/container/ElementExistException.hdl"
#include "com/sun/star/container/NoSuchElementException.hdl"
namespace com { namespace sun { namespace star { namespace ui { class XUIElementFactory; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ui {

class SAL_NO_VTABLE XUIElementFactoryRegistration : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::uno::Sequence< ::css::beans::PropertyValue > > SAL_CALL getRegisteredFactories() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::ui::XUIElementFactory > SAL_CALL getFactory( const ::rtl::OUString& ResourceURL, const ::rtl::OUString& ModuleIdentifier ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL registerFactory( const ::rtl::OUString& aType, const ::rtl::OUString& aName, const ::rtl::OUString& aModuleIdentifier, const ::rtl::OUString& aFactoryImplementationName ) /* throw (::css::container::ElementExistException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL deregisterFactory( const ::rtl::OUString& aType, const ::rtl::OUString& aName, const ::rtl::OUString& ModuleIdentifier ) /* throw (::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XUIElementFactoryRegistration() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::XUIElementFactoryRegistration const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ui::XUIElementFactoryRegistration > *) SAL_THROW(());

#endif
