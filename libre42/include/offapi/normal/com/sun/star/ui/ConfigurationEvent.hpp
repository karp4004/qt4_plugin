#ifndef INCLUDED_COM_SUN_STAR_UI_CONFIGURATIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_UI_CONFIGURATIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/ui/ConfigurationEvent.hdl"

#include "com/sun/star/container/ContainerEvent.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ui {

inline ConfigurationEvent::ConfigurationEvent() SAL_THROW(())
    : ::css::container::ContainerEvent()
    , ResourceURL()
    , aInfo()
{
}

inline ConfigurationEvent::ConfigurationEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Any& Accessor_, const ::css::uno::Any& Element_, const ::css::uno::Any& ReplacedElement_, const ::rtl::OUString& ResourceURL_, const ::css::uno::Any& aInfo_) SAL_THROW(())
    : ::css::container::ContainerEvent(Source_, Accessor_, Element_, ReplacedElement_)
    , ResourceURL(ResourceURL_)
    , aInfo(aInfo_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ui {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::ConfigurationEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ui.ConfigurationEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::ConfigurationEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::ConfigurationEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_CONFIGURATIONEVENT_HPP
