#ifndef INCLUDED_COM_SUN_STAR_UI_DIALOGS_WIZARD_HPP
#define INCLUDED_COM_SUN_STAR_UI_DIALOGS_WIZARD_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/ui/dialogs/XWizard.hpp"
#include "com/sun/star/ui/dialogs/XWizardController.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

class Wizard {
public:
    static ::css::uno::Reference< ::css::ui::dialogs::XWizard > createSinglePathWizard(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Sequence< ::sal_Int16 >& PageIds, const ::css::uno::Reference< ::css::ui::dialogs::XWizardController >& Controller) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(2);
        the_arguments[0] <<= PageIds;
        the_arguments[1] <<= Controller;
        ::css::uno::Reference< ::css::ui::dialogs::XWizard > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::ui::dialogs::XWizard >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.ui.dialogs.Wizard" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ui.dialogs.Wizard of type com.sun.star.ui.dialogs.XWizard: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ui.dialogs.Wizard of type com.sun.star.ui.dialogs.XWizard" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::ui::dialogs::XWizard > createMultiplePathsWizard(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Sequence< ::css::uno::Sequence< ::sal_Int16 > >& PageIds, const ::css::uno::Reference< ::css::ui::dialogs::XWizardController >& Controller) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(2);
        the_arguments[0] <<= PageIds;
        the_arguments[1] <<= Controller;
        ::css::uno::Reference< ::css::ui::dialogs::XWizard > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::ui::dialogs::XWizard >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.ui.dialogs.Wizard" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ui.dialogs.Wizard of type com.sun.star.ui.dialogs.XWizard: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ui.dialogs.Wizard of type com.sun.star.ui.dialogs.XWizard" ), the_context);
        }
        return the_instance;
    }

private:
    Wizard(); // not implemented
    Wizard(Wizard &); // not implemented
    ~Wizard(); // not implemented
    void operator =(Wizard); // not implemented
};

} } } } }

#endif // INCLUDED_COM_SUN_STAR_UI_DIALOGS_WIZARD_HPP
