#ifndef INCLUDED_COM_SUN_STAR_UI_CONTEXTMENUEXECUTEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_UI_CONTEXTMENUEXECUTEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/ui/ContextMenuExecuteEvent.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/awt/XWindow.hpp"
#include "com/sun/star/container/XIndexContainer.hpp"
#include "com/sun/star/view/XSelectionSupplier.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ui {

inline ContextMenuExecuteEvent::ContextMenuExecuteEvent() SAL_THROW(())
    : SourceWindow()
    , ExecutePosition()
    , ActionTriggerContainer()
    , Selection()
{
}

inline ContextMenuExecuteEvent::ContextMenuExecuteEvent(const ::css::uno::Reference< ::css::awt::XWindow >& SourceWindow_, const ::css::awt::Point& ExecutePosition_, const ::css::uno::Reference< ::css::container::XIndexContainer >& ActionTriggerContainer_, const ::css::uno::Reference< ::css::view::XSelectionSupplier >& Selection_) SAL_THROW(())
    : SourceWindow(SourceWindow_)
    , ExecutePosition(ExecutePosition_)
    , ActionTriggerContainer(ActionTriggerContainer_)
    , Selection(Selection_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ui {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::ContextMenuExecuteEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ui.ContextMenuExecuteEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::ContextMenuExecuteEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::ContextMenuExecuteEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_CONTEXTMENUEXECUTEEVENT_HPP
