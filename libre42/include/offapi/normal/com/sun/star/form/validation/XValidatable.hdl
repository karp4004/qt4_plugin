#ifndef INCLUDED_COM_SUN_STAR_FORM_VALIDATION_XVALIDATABLE_HDL
#define INCLUDED_COM_SUN_STAR_FORM_VALIDATION_XVALIDATABLE_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace form { namespace validation { class XValidator; } } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/util/VetoException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace form { namespace validation {

class SAL_NO_VTABLE XValidatable : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setValidator( const ::css::uno::Reference< ::css::form::validation::XValidator >& Validator ) /* throw (::css::util::VetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::form::validation::XValidator > SAL_CALL getValidator() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XValidatable() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::validation::XValidatable const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::form::validation::XValidatable > *) SAL_THROW(());

#endif
