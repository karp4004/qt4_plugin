#ifndef INCLUDED_COM_SUN_STAR_FORM_RUNTIME_XFORMOPERATIONS_HPP
#define INCLUDED_COM_SUN_STAR_FORM_RUNTIME_XFORMOPERATIONS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/form/runtime/XFormOperations.hdl"

#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/form/runtime/FeatureState.hpp"
#include "com/sun/star/form/runtime/XFeatureInvalidation.hpp"
#include "com/sun/star/form/runtime/XFormController.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/WrappedTargetException.hpp"
#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/sdbc/SQLException.hpp"
#include "com/sun/star/sdbc/XResultSetUpdate.hpp"
#include "com/sun/star/sdbc/XRowSet.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace form { namespace runtime {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::runtime::XFormOperations const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.form.runtime.XFormOperations" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::form::runtime::XFormOperations > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::form::runtime::XFormOperations > >::get();
}

::css::uno::Type const & ::css::form::runtime::XFormOperations::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::form::runtime::XFormOperations > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_FORM_RUNTIME_XFORMOPERATIONS_HPP
