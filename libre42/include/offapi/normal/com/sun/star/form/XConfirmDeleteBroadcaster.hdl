#ifndef INCLUDED_COM_SUN_STAR_FORM_XCONFIRMDELETEBROADCASTER_HDL
#define INCLUDED_COM_SUN_STAR_FORM_XCONFIRMDELETEBROADCASTER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace form { class XConfirmDeleteListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace form {

class SAL_NO_VTABLE XConfirmDeleteBroadcaster : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addConfirmDeleteListener( const ::css::uno::Reference< ::css::form::XConfirmDeleteListener >& aListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeConfirmDeleteListener( const ::css::uno::Reference< ::css::form::XConfirmDeleteListener >& aListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XConfirmDeleteBroadcaster() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::XConfirmDeleteBroadcaster const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::form::XConfirmDeleteBroadcaster > *) SAL_THROW(());

#endif
