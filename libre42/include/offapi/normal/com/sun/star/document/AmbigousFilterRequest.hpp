#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_AMBIGOUSFILTERREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_AMBIGOUSFILTERREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/document/AmbigousFilterRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline AmbigousFilterRequest::AmbigousFilterRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , URL()
    , SelectedFilter()
    , DetectedFilter()
{ }

inline AmbigousFilterRequest::AmbigousFilterRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& URL_, const ::rtl::OUString& SelectedFilter_, const ::rtl::OUString& DetectedFilter_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , URL(URL_)
    , SelectedFilter(SelectedFilter_)
    , DetectedFilter(DetectedFilter_)
{ }

AmbigousFilterRequest::AmbigousFilterRequest(AmbigousFilterRequest const & the_other): ::css::uno::Exception(the_other), URL(the_other.URL), SelectedFilter(the_other.SelectedFilter), DetectedFilter(the_other.DetectedFilter) {}

AmbigousFilterRequest::~AmbigousFilterRequest() {}

AmbigousFilterRequest & AmbigousFilterRequest::operator =(AmbigousFilterRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    URL = the_other.URL;
    SelectedFilter = the_other.SelectedFilter;
    DetectedFilter = the_other.DetectedFilter;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::AmbigousFilterRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.document.AmbigousFilterRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::AmbigousFilterRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::AmbigousFilterRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_AMBIGOUSFILTERREQUEST_HPP
