#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_FILTEROPTIONSREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_FILTEROPTIONSREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/document/FilterOptionsRequest.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/frame/XModel.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline FilterOptionsRequest::FilterOptionsRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , rModel()
    , rProperties()
{ }

inline FilterOptionsRequest::FilterOptionsRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Reference< ::css::frame::XModel >& rModel_, const ::css::uno::Sequence< ::css::beans::PropertyValue >& rProperties_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , rModel(rModel_)
    , rProperties(rProperties_)
{ }

FilterOptionsRequest::FilterOptionsRequest(FilterOptionsRequest const & the_other): ::css::uno::Exception(the_other), rModel(the_other.rModel), rProperties(the_other.rProperties) {}

FilterOptionsRequest::~FilterOptionsRequest() {}

FilterOptionsRequest & FilterOptionsRequest::operator =(FilterOptionsRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    rModel = the_other.rModel;
    rProperties = the_other.rProperties;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::FilterOptionsRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.document.FilterOptionsRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::FilterOptionsRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::FilterOptionsRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_FILTEROPTIONSREQUEST_HPP
