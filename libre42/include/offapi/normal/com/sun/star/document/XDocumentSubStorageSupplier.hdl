#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_XDOCUMENTSUBSTORAGESUPPLIER_HDL
#define INCLUDED_COM_SUN_STAR_DOCUMENT_XDOCUMENTSUBSTORAGESUPPLIER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace embed { class XStorage; } } } }
#include "com/sun/star/io/IOException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace document {

class SAL_NO_VTABLE XDocumentSubStorageSupplier : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::embed::XStorage > SAL_CALL getDocumentSubStorage( const ::rtl::OUString& StorageName, ::sal_Int32 nMode ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getDocumentSubStoragesNames() /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDocumentSubStorageSupplier() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::XDocumentSubStorageSupplier const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::document::XDocumentSubStorageSupplier > *) SAL_THROW(());

#endif
