#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_XXMLOASISBASICIMPORTER_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_XXMLOASISBASICIMPORTER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/document/XXMLOasisBasicImporter.hdl"

#include "com/sun/star/document/XImporter.hpp"
#include "com/sun/star/xml/sax/XDocumentHandler.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::XXMLOasisBasicImporter const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.document.XXMLOasisBasicImporter" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::document::XXMLOasisBasicImporter > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::document::XXMLOasisBasicImporter > >::get();
}

::css::uno::Type const & ::css::document::XXMLOasisBasicImporter::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::document::XXMLOasisBasicImporter > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_XXMLOASISBASICIMPORTER_HPP
