#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_LOCKEDDOCUMENTREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_LOCKEDDOCUMENTREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/document/LockedDocumentRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline LockedDocumentRequest::LockedDocumentRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , DocumentURL()
    , UserInfo()
{ }

inline LockedDocumentRequest::LockedDocumentRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& DocumentURL_, const ::rtl::OUString& UserInfo_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , DocumentURL(DocumentURL_)
    , UserInfo(UserInfo_)
{ }

LockedDocumentRequest::LockedDocumentRequest(LockedDocumentRequest const & the_other): ::css::uno::Exception(the_other), DocumentURL(the_other.DocumentURL), UserInfo(the_other.UserInfo) {}

LockedDocumentRequest::~LockedDocumentRequest() {}

LockedDocumentRequest & LockedDocumentRequest::operator =(LockedDocumentRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    DocumentURL = the_other.DocumentURL;
    UserInfo = the_other.UserInfo;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::LockedDocumentRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.document.LockedDocumentRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::LockedDocumentRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::LockedDocumentRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_LOCKEDDOCUMENTREQUEST_HPP
