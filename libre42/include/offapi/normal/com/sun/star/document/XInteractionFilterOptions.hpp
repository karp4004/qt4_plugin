#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_XINTERACTIONFILTEROPTIONS_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_XINTERACTIONFILTEROPTIONS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/document/XInteractionFilterOptions.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/task/XInteractionContinuation.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::XInteractionFilterOptions const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.document.XInteractionFilterOptions" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::document::XInteractionFilterOptions > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::document::XInteractionFilterOptions > >::get();
}

::css::uno::Type const & ::css::document::XInteractionFilterOptions::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::document::XInteractionFilterOptions > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_XINTERACTIONFILTEROPTIONS_HPP
