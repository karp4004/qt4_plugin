#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_XINTERACTIONFILTERSELECT_HDL
#define INCLUDED_COM_SUN_STAR_DOCUMENT_XINTERACTIONFILTERSELECT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/task/XInteractionContinuation.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace document {

class SAL_NO_VTABLE XInteractionFilterSelect : public ::css::task::XInteractionContinuation
{
public:

    // Methods
    virtual void SAL_CALL setFilter( const ::rtl::OUString& Name ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getFilter() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XInteractionFilterSelect() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::XInteractionFilterSelect const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::document::XInteractionFilterSelect > *) SAL_THROW(());

#endif
