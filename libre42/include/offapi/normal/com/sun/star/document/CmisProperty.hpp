#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_CMISPROPERTY_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_CMISPROPERTY_HPP

#include "sal/config.h"

#include "com/sun/star/document/CmisProperty.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace document {

inline CmisProperty::CmisProperty() SAL_THROW(())
    : Id()
    , Name()
    , Type()
    , Updatable(false)
    , Required(false)
    , MultiValued(false)
    , OpenChoice(false)
    , Choices()
    , Value()
{
}

inline CmisProperty::CmisProperty(const ::rtl::OUString& Id_, const ::rtl::OUString& Name_, const ::rtl::OUString& Type_, const ::sal_Bool& Updatable_, const ::sal_Bool& Required_, const ::sal_Bool& MultiValued_, const ::sal_Bool& OpenChoice_, const ::css::uno::Any& Choices_, const ::css::uno::Any& Value_) SAL_THROW(())
    : Id(Id_)
    , Name(Name_)
    , Type(Type_)
    , Updatable(Updatable_)
    , Required(Required_)
    , MultiValued(MultiValued_)
    , OpenChoice(OpenChoice_)
    , Choices(Choices_)
    , Value(Value_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::CmisProperty const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.document.CmisProperty");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::CmisProperty const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::CmisProperty >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_CMISPROPERTY_HPP
