#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_DOCUMENTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_DOCUMENTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/document/DocumentEvent.hdl"

#include "com/sun/star/frame/XController2.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace document {

inline DocumentEvent::DocumentEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , EventName()
    , ViewController()
    , Supplement()
{
}

inline DocumentEvent::DocumentEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& EventName_, const ::css::uno::Reference< ::css::frame::XController2 >& ViewController_, const ::css::uno::Any& Supplement_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , EventName(EventName_)
    , ViewController(ViewController_)
    , Supplement(Supplement_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::DocumentEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.document.DocumentEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::DocumentEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::DocumentEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_DOCUMENTEVENT_HPP
