#ifndef INCLUDED_COM_SUN_STAR_ANIMATIONS_XANIMATEMOTION_HDL
#define INCLUDED_COM_SUN_STAR_ANIMATIONS_XANIMATEMOTION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/animations/XAnimate.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace animations {

class SAL_NO_VTABLE XAnimateMotion : public ::css::animations::XAnimate
{
public:

    // Attributes
    virtual ::css::uno::Any SAL_CALL getPath() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPath( const ::css::uno::Any& _path ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getOrigin() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setOrigin( const ::css::uno::Any& _origin ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAnimateMotion() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::animations::XAnimateMotion const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::animations::XAnimateMotion > *) SAL_THROW(());

#endif
