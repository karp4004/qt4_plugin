#ifndef INCLUDED_COM_SUN_STAR_ANIMATIONS_TARGETPROPERTIES_HPP
#define INCLUDED_COM_SUN_STAR_ANIMATIONS_TARGETPROPERTIES_HPP

#include "sal/config.h"

#include "com/sun/star/animations/TargetProperties.hdl"

#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace animations {

inline TargetProperties::TargetProperties() SAL_THROW(())
    : Target()
    , Properties()
{
}

inline TargetProperties::TargetProperties(const ::css::uno::Any& Target_, const ::css::uno::Sequence< ::css::beans::NamedValue >& Properties_) SAL_THROW(())
    : Target(Target_)
    , Properties(Properties_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace animations {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::animations::TargetProperties const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.animations.TargetProperties");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::animations::TargetProperties const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::animations::TargetProperties >::get();
}

#endif // INCLUDED_COM_SUN_STAR_ANIMATIONS_TARGETPROPERTIES_HPP
