#ifndef INCLUDED_COM_SUN_STAR_GRAPHIC_PRIMITIVEFACTORY2D_HPP
#define INCLUDED_COM_SUN_STAR_GRAPHIC_PRIMITIVEFACTORY2D_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/graphic/XPrimitiveFactory2D.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace graphic {

class PrimitiveFactory2D {
public:
    static ::css::uno::Reference< ::css::graphic::XPrimitiveFactory2D > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::graphic::XPrimitiveFactory2D > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::graphic::XPrimitiveFactory2D >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.graphic.PrimitiveFactory2D" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.graphic.PrimitiveFactory2D of type com.sun.star.graphic.XPrimitiveFactory2D: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.graphic.PrimitiveFactory2D of type com.sun.star.graphic.XPrimitiveFactory2D" ), the_context);
        }
        return the_instance;
    }

private:
    PrimitiveFactory2D(); // not implemented
    PrimitiveFactory2D(PrimitiveFactory2D &); // not implemented
    ~PrimitiveFactory2D(); // not implemented
    void operator =(PrimitiveFactory2D); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_GRAPHIC_PRIMITIVEFACTORY2D_HPP
