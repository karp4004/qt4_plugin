#ifndef INCLUDED_COM_SUN_STAR_GRAPHIC_GRAPHICOBJECT_HPP
#define INCLUDED_COM_SUN_STAR_GRAPHIC_GRAPHICOBJECT_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/graphic/XGraphicObject.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace graphic {

class GraphicObject {
public:
    static ::css::uno::Reference< ::css::graphic::XGraphicObject > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::graphic::XGraphicObject > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::graphic::XGraphicObject >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.graphic.GraphicObject" ), ::css::uno::Sequence< ::css::uno::Any >(), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.graphic.GraphicObject of type com.sun.star.graphic.XGraphicObject: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.graphic.GraphicObject of type com.sun.star.graphic.XGraphicObject" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::graphic::XGraphicObject > createWithId(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& uniqueId) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= uniqueId;
        ::css::uno::Reference< ::css::graphic::XGraphicObject > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::graphic::XGraphicObject >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.graphic.GraphicObject" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.graphic.GraphicObject of type com.sun.star.graphic.XGraphicObject: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.graphic.GraphicObject of type com.sun.star.graphic.XGraphicObject" ), the_context);
        }
        return the_instance;
    }

private:
    GraphicObject(); // not implemented
    GraphicObject(GraphicObject &); // not implemented
    ~GraphicObject(); // not implemented
    void operator =(GraphicObject); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_GRAPHIC_GRAPHICOBJECT_HPP
