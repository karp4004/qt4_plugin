#ifndef INCLUDED_COM_SUN_STAR_GRAPHIC_XGRAPHICOBJECT_HPP
#define INCLUDED_COM_SUN_STAR_GRAPHIC_XGRAPHICOBJECT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/graphic/XGraphicObject.hdl"

#include "com/sun/star/graphic/XGraphic.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace graphic {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::graphic::XGraphicObject const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.graphic.XGraphicObject" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::graphic::XGraphicObject > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::graphic::XGraphicObject > >::get();
}

::css::uno::Type const & ::css::graphic::XGraphicObject::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::graphic::XGraphicObject > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_GRAPHIC_XGRAPHICOBJECT_HPP
