#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_VERB_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_VERB_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/Verb.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline Verb::Verb() SAL_THROW(())
    : VerbId(0)
    , VerbName()
    , VerbIsOnMenu(false)
    , VerbIsConst(false)
{
}

inline Verb::Verb(const ::sal_Int32& VerbId_, const ::rtl::OUString& VerbName_, const ::sal_Bool& VerbIsOnMenu_, const ::sal_Bool& VerbIsConst_) SAL_THROW(())
    : VerbId(VerbId_)
    , VerbName(VerbName_)
    , VerbIsOnMenu(VerbIsOnMenu_)
    , VerbIsConst(VerbIsConst_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::Verb const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.frame.status.Verb");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::Verb const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::Verb >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_VERB_HPP
