#ifndef INCLUDED_COM_SUN_STAR_FRAME_FRAMEACTIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_FRAMEACTIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/frame/FrameActionEvent.hdl"

#include "com/sun/star/frame/FrameAction.hpp"
#include "com/sun/star/frame/XFrame.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame {

inline FrameActionEvent::FrameActionEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Frame()
    , Action(::css::frame::FrameAction_COMPONENT_ATTACHED)
{
}

inline FrameActionEvent::FrameActionEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::frame::XFrame >& Frame_, const ::css::frame::FrameAction& Action_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Frame(Frame_)
    , Action(Action_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::FrameActionEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.frame.FrameActionEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::FrameActionEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::FrameActionEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_FRAMEACTIONEVENT_HPP
