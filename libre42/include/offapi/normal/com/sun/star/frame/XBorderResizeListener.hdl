#ifndef INCLUDED_COM_SUN_STAR_FRAME_XBORDERRESIZELISTENER_HDL
#define INCLUDED_COM_SUN_STAR_FRAME_XBORDERRESIZELISTENER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/frame/BorderWidths.hdl"
#include "com/sun/star/lang/XEventListener.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
namespace com { namespace sun { namespace star { namespace uno { class XInterface; } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace frame {

class SAL_NO_VTABLE XBorderResizeListener : public ::css::lang::XEventListener
{
public:

    // Methods
    virtual void SAL_CALL borderWidthsChanged( const ::css::uno::Reference< ::css::uno::XInterface >& Object, const ::css::frame::BorderWidths& NewSize ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XBorderResizeListener() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XBorderResizeListener const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::frame::XBorderResizeListener > *) SAL_THROW(());

#endif
