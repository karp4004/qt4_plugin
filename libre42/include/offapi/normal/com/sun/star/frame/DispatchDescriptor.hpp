#ifndef INCLUDED_COM_SUN_STAR_FRAME_DISPATCHDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_DISPATCHDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/frame/DispatchDescriptor.hdl"

#include "com/sun/star/util/URL.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame {

inline DispatchDescriptor::DispatchDescriptor() SAL_THROW(())
    : FeatureURL()
    , FrameName()
    , SearchFlags(0)
{
}

inline DispatchDescriptor::DispatchDescriptor(const ::css::util::URL& FeatureURL_, const ::rtl::OUString& FrameName_, const ::sal_Int32& SearchFlags_) SAL_THROW(())
    : FeatureURL(FeatureURL_)
    , FrameName(FrameName_)
    , SearchFlags(SearchFlags_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::DispatchDescriptor const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.frame.DispatchDescriptor");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::DispatchDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::DispatchDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_DISPATCHDESCRIPTOR_HPP
