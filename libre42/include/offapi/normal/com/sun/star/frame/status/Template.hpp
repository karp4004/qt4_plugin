#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_TEMPLATE_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_TEMPLATE_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/Template.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline Template::Template() SAL_THROW(())
    : StyleName()
    , Value(0)
{
}

inline Template::Template(const ::rtl::OUString& StyleName_, const ::sal_Int32& Value_) SAL_THROW(())
    : StyleName(StyleName_)
    , Value(Value_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::Template const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.frame.status.Template");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::Template const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::Template >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_TEMPLATE_HPP
