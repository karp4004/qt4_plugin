#ifndef INCLUDED_COM_SUN_STAR_FRAME_XTASK_HDL
#define INCLUDED_COM_SUN_STAR_FRAME_XTASK_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/frame/XFrame.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace frame {

class SAL_NO_VTABLE XTask : public ::css::frame::XFrame
{
public:

    // Methods
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL close() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual void SAL_CALL tileWindows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual void SAL_CALL arrangeWindowsVertical() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual void SAL_CALL arrangeWindowsHorizontal() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTask() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XTask const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::frame::XTask > *) SAL_THROW(());

#endif
