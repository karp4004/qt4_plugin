#ifndef INCLUDED_COM_SUN_STAR_FRAME_XDESKTOP_HDL
#define INCLUDED_COM_SUN_STAR_FRAME_XDESKTOP_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace container { class XEnumerationAccess; } } } }
namespace com { namespace sun { namespace star { namespace frame { class XFrame; } } } }
namespace com { namespace sun { namespace star { namespace frame { class XTerminateListener; } } } }
namespace com { namespace sun { namespace star { namespace lang { class XComponent; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace frame {

class SAL_NO_VTABLE XDesktop : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL terminate() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addTerminateListener( const ::css::uno::Reference< ::css::frame::XTerminateListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeTerminateListener( const ::css::uno::Reference< ::css::frame::XTerminateListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::container::XEnumerationAccess > SAL_CALL getComponents() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::lang::XComponent > SAL_CALL getCurrentComponent() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::frame::XFrame > SAL_CALL getCurrentFrame() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDesktop() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XDesktop const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::frame::XDesktop > *) SAL_THROW(());

#endif
