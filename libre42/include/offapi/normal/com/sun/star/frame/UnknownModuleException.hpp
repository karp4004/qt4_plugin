#ifndef INCLUDED_COM_SUN_STAR_FRAME_UNKNOWNMODULEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_UNKNOWNMODULEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/frame/UnknownModuleException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace frame {

inline UnknownModuleException::UnknownModuleException() SAL_THROW(())
    : ::css::uno::Exception()
{ }

inline UnknownModuleException::UnknownModuleException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{ }

UnknownModuleException::UnknownModuleException(UnknownModuleException const & the_other): ::css::uno::Exception(the_other) {}

UnknownModuleException::~UnknownModuleException() {}

UnknownModuleException & UnknownModuleException::operator =(UnknownModuleException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::UnknownModuleException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.frame.UnknownModuleException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::UnknownModuleException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::UnknownModuleException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_UNKNOWNMODULEEXCEPTION_HPP
