#ifndef INCLUDED_COM_SUN_STAR_FRAME_XTITLECHANGELISTENER_HDL
#define INCLUDED_COM_SUN_STAR_FRAME_XTITLECHANGELISTENER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/frame/TitleChangedEvent.hdl"
#include "com/sun/star/lang/XEventListener.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace frame {

class SAL_NO_VTABLE XTitleChangeListener : public ::css::lang::XEventListener
{
public:

    // Methods
    virtual void SAL_CALL titleChanged( const ::css::frame::TitleChangedEvent& aEvent ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTitleChangeListener() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XTitleChangeListener const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::frame::XTitleChangeListener > *) SAL_THROW(());

#endif
