#ifndef INCLUDED_COM_SUN_STAR_FRAME_XGLOBALEVENTBROADCASTER_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_XGLOBALEVENTBROADCASTER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/frame/XGlobalEventBroadcaster.hdl"

#include "com/sun/star/container/XSet.hpp"
#include "com/sun/star/document/XDocumentEventBroadcaster.hpp"
#include "com/sun/star/document/XDocumentEventListener.hpp"
#include "com/sun/star/document/XEventBroadcaster.hpp"
#include "com/sun/star/document/XEventsSupplier.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XGlobalEventBroadcaster const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.frame.XGlobalEventBroadcaster" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::frame::XGlobalEventBroadcaster > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XGlobalEventBroadcaster > >::get();
}

::css::uno::Type const & ::css::frame::XGlobalEventBroadcaster::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::frame::XGlobalEventBroadcaster > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_XGLOBALEVENTBROADCASTER_HPP
