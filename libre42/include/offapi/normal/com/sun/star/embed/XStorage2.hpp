#ifndef INCLUDED_COM_SUN_STAR_EMBED_XSTORAGE2_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_XSTORAGE2_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/embed/XStorage2.hdl"

#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/embed/InvalidStorageException.hpp"
#include "com/sun/star/embed/StorageWrappedTargetException.hpp"
#include "com/sun/star/embed/XStorage.hpp"
#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/io/XStream.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/packages/NoEncryptionException.hpp"
#include "com/sun/star/packages/WrongPasswordException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XStorage2 const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.embed.XStorage2" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::embed::XStorage2 > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::embed::XStorage2 > >::get();
}

::css::uno::Type const & ::css::embed::XStorage2::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::embed::XStorage2 > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_XSTORAGE2_HPP
