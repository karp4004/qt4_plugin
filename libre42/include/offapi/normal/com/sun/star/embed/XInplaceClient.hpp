#ifndef INCLUDED_COM_SUN_STAR_EMBED_XINPLACECLIENT_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_XINPLACECLIENT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/embed/XInplaceClient.hdl"

#include "com/sun/star/awt/KeyEvent.hpp"
#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/awt/Size.hpp"
#include "com/sun/star/embed/WrongStateException.hpp"
#include "com/sun/star/frame/XDispatchProvider.hpp"
#include "com/sun/star/frame/XLayoutManager.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XInplaceClient const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.embed.XInplaceClient" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::embed::XInplaceClient > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::embed::XInplaceClient > >::get();
}

::css::uno::Type const & ::css::embed::XInplaceClient::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::embed::XInplaceClient > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_XINPLACECLIENT_HPP
