#ifndef INCLUDED_COM_SUN_STAR_EMBED_XSTATECHANGELISTENER_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_XSTATECHANGELISTENER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/embed/XStateChangeListener.hdl"

#include "com/sun/star/embed/WrongStateException.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/lang/XEventListener.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XStateChangeListener const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.embed.XStateChangeListener" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::embed::XStateChangeListener > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::embed::XStateChangeListener > >::get();
}

::css::uno::Type const & ::css::embed::XStateChangeListener::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::embed::XStateChangeListener > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_XSTATECHANGELISTENER_HPP
