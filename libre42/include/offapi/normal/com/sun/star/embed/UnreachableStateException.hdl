#ifndef INCLUDED_COM_SUN_STAR_EMBED_UNREACHABLESTATEEXCEPTION_HDL
#define INCLUDED_COM_SUN_STAR_EMBED_UNREACHABLESTATEEXCEPTION_HDL

#include "sal/config.h"

#include "com/sun/star/uno/Exception.hdl"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace embed {

class CPPU_GCC_DLLPUBLIC_EXPORT UnreachableStateException : public ::css::uno::Exception
{
public:
    inline CPPU_GCC_DLLPRIVATE UnreachableStateException() SAL_THROW(());

    inline CPPU_GCC_DLLPRIVATE UnreachableStateException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& CurrentState_, const ::sal_Int32& NextState_) SAL_THROW(());

    inline CPPU_GCC_DLLPRIVATE UnreachableStateException(UnreachableStateException const &);

    inline CPPU_GCC_DLLPRIVATE ~UnreachableStateException();

    inline CPPU_GCC_DLLPRIVATE UnreachableStateException & operator =(UnreachableStateException const &);

    ::sal_Int32 CurrentState CPPU_GCC3_ALIGN( ::css::uno::Exception );
    ::sal_Int32 NextState;
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::UnreachableStateException const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::embed::UnreachableStateException *) SAL_THROW(());

#endif
