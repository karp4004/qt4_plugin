#ifndef INCLUDED_COM_SUN_STAR_EMBED_STATECHANGEINPROGRESSEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_STATECHANGEINPROGRESSEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/embed/StateChangeInProgressException.hdl"

#include "com/sun/star/embed/WrongStateException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace embed {

inline StateChangeInProgressException::StateChangeInProgressException() SAL_THROW(())
    : ::css::embed::WrongStateException()
    , TargetState(0)
{ }

inline StateChangeInProgressException::StateChangeInProgressException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& TargetState_) SAL_THROW(())
    : ::css::embed::WrongStateException(Message_, Context_)
    , TargetState(TargetState_)
{ }

StateChangeInProgressException::StateChangeInProgressException(StateChangeInProgressException const & the_other): ::css::embed::WrongStateException(the_other), TargetState(the_other.TargetState) {}

StateChangeInProgressException::~StateChangeInProgressException() {}

StateChangeInProgressException & StateChangeInProgressException::operator =(StateChangeInProgressException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::embed::WrongStateException::operator =(the_other);
    TargetState = the_other.TargetState;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::StateChangeInProgressException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.embed.StateChangeInProgressException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::StateChangeInProgressException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::StateChangeInProgressException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_STATECHANGEINPROGRESSEXCEPTION_HPP
