#ifndef INCLUDED_COM_SUN_STAR_EMBED_XEMBEDPERSIST_HDL
#define INCLUDED_COM_SUN_STAR_EMBED_XEMBEDPERSIST_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
#include "com/sun/star/embed/WrongStateException.hdl"
#include "com/sun/star/embed/XCommonEmbedPersist.hdl"
namespace com { namespace sun { namespace star { namespace embed { class XStorage; } } } }
#include "com/sun/star/io/IOException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace embed {

class SAL_NO_VTABLE XEmbedPersist : public ::css::embed::XCommonEmbedPersist
{
public:

    // Methods
    virtual void SAL_CALL setPersistentEntry( const ::css::uno::Reference< ::css::embed::XStorage >& xStorage, const ::rtl::OUString& sEntName, ::sal_Int32 nEntryConnectionMode, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aMediaArgs, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aObjectArgs ) /* throw (::css::lang::IllegalArgumentException, ::css::embed::WrongStateException, ::css::io::IOException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL storeToEntry( const ::css::uno::Reference< ::css::embed::XStorage >& xStorage, const ::rtl::OUString& sEntName, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aMediaArgs, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aObjectArgs ) /* throw (::css::lang::IllegalArgumentException, ::css::embed::WrongStateException, ::css::io::IOException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL storeAsEntry( const ::css::uno::Reference< ::css::embed::XStorage >& xStorage, const ::rtl::OUString& sEntName, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aMediaArgs, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aObjectArgs ) /* throw (::css::lang::IllegalArgumentException, ::css::embed::WrongStateException, ::css::io::IOException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL saveCompleted( ::sal_Bool bUseNew ) /* throw (::css::embed::WrongStateException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL hasEntry() /* throw (::css::embed::WrongStateException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getEntryName() /* throw (::css::embed::WrongStateException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XEmbedPersist() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XEmbedPersist const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::embed::XEmbedPersist > *) SAL_THROW(());

#endif
