#ifndef INCLUDED_COM_SUN_STAR_EMBED_UNREACHABLESTATEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_UNREACHABLESTATEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/embed/UnreachableStateException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace embed {

inline UnreachableStateException::UnreachableStateException() SAL_THROW(())
    : ::css::uno::Exception()
    , CurrentState(0)
    , NextState(0)
{ }

inline UnreachableStateException::UnreachableStateException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& CurrentState_, const ::sal_Int32& NextState_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , CurrentState(CurrentState_)
    , NextState(NextState_)
{ }

UnreachableStateException::UnreachableStateException(UnreachableStateException const & the_other): ::css::uno::Exception(the_other), CurrentState(the_other.CurrentState), NextState(the_other.NextState) {}

UnreachableStateException::~UnreachableStateException() {}

UnreachableStateException & UnreachableStateException::operator =(UnreachableStateException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    CurrentState = the_other.CurrentState;
    NextState = the_other.NextState;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::UnreachableStateException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.embed.UnreachableStateException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::UnreachableStateException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::UnreachableStateException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_UNREACHABLESTATEEXCEPTION_HPP
