#ifndef INCLUDED_COM_SUN_STAR_EMBED_MSOLEOBJECTSYSTEMCREATOR_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_MSOLEOBJECTSYSTEMCREATOR_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/embed/XEmbedObjectClipboardCreator.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace embed {

class MSOLEObjectSystemCreator {
public:
    static ::css::uno::Reference< ::css::embed::XEmbedObjectClipboardCreator > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::embed::XEmbedObjectClipboardCreator > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::embed::XEmbedObjectClipboardCreator >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.embed.MSOLEObjectSystemCreator" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.embed.MSOLEObjectSystemCreator of type com.sun.star.embed.XEmbedObjectClipboardCreator: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.embed.MSOLEObjectSystemCreator of type com.sun.star.embed.XEmbedObjectClipboardCreator" ), the_context);
        }
        return the_instance;
    }

private:
    MSOLEObjectSystemCreator(); // not implemented
    MSOLEObjectSystemCreator(MSOLEObjectSystemCreator &); // not implemented
    ~MSOLEObjectSystemCreator(); // not implemented
    void operator =(MSOLEObjectSystemCreator); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_EMBED_MSOLEOBJECTSYSTEMCREATOR_HPP
