#ifndef INCLUDED_COM_SUN_STAR_EMBED_XTRANSFERABLESUPPLIER_HDL
#define INCLUDED_COM_SUN_STAR_EMBED_XTRANSFERABLESUPPLIER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace datatransfer { class XTransferable; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace embed {

class SAL_NO_VTABLE XTransferableSupplier : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::datatransfer::XTransferable > SAL_CALL getTransferable() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTransferableSupplier() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XTransferableSupplier const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::embed::XTransferableSupplier > *) SAL_THROW(());

#endif
