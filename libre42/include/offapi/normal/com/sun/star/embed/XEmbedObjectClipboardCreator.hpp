#ifndef INCLUDED_COM_SUN_STAR_EMBED_XEMBEDOBJECTCLIPBOARDCREATOR_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_XEMBEDOBJECTCLIPBOARDCREATOR_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/embed/XEmbedObjectClipboardCreator.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/embed/InsertedObjectInfo.hpp"
#include "com/sun/star/embed/XStorage.hpp"
#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XEmbedObjectClipboardCreator const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.embed.XEmbedObjectClipboardCreator" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::embed::XEmbedObjectClipboardCreator > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::embed::XEmbedObjectClipboardCreator > >::get();
}

::css::uno::Type const & ::css::embed::XEmbedObjectClipboardCreator::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::embed::XEmbedObjectClipboardCreator > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_XEMBEDOBJECTCLIPBOARDCREATOR_HPP
