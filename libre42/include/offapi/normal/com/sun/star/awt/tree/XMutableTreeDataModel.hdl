#ifndef INCLUDED_COM_SUN_STAR_AWT_TREE_XMUTABLETREEDATAMODEL_HDL
#define INCLUDED_COM_SUN_STAR_AWT_TREE_XMUTABLETREEDATAMODEL_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { namespace tree { class XMutableTreeNode; } } } } }
#include "com/sun/star/awt/tree/XTreeDataModel.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

class SAL_NO_VTABLE XMutableTreeDataModel : public ::css::awt::tree::XTreeDataModel
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::awt::tree::XMutableTreeNode > SAL_CALL createNode( const ::css::uno::Any& DisplayValue, ::sal_Bool ChildrenOnDemand ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRoot( const ::css::uno::Reference< ::css::awt::tree::XMutableTreeNode >& RootNode ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XMutableTreeDataModel() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::tree::XMutableTreeDataModel const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::tree::XMutableTreeDataModel > *) SAL_THROW(());

#endif
