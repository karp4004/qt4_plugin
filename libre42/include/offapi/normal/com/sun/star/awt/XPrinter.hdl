#ifndef INCLUDED_COM_SUN_STAR_AWT_XPRINTER_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XPRINTER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/PrinterException.hdl"
namespace com { namespace sun { namespace star { namespace awt { class XDevice; } } } }
#include "com/sun/star/awt/XPrinterPropertySet.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XPrinter : public ::css::awt::XPrinterPropertySet
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL start( const ::rtl::OUString& nJobName, ::sal_Int16 nCopies, ::sal_Bool nCollate ) /* throw (::css::awt::PrinterException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL end() /* throw (::css::awt::PrinterException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL terminate() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::XDevice > SAL_CALL startPage() /* throw (::css::awt::PrinterException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL endPage() /* throw (::css::awt::PrinterException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPrinter() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XPrinter const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XPrinter > *) SAL_THROW(());

#endif
