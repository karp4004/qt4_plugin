#ifndef INCLUDED_COM_SUN_STAR_AWT_TREE_EXPANDVETOEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_AWT_TREE_EXPANDVETOEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/awt/tree/ExpandVetoException.hdl"

#include "com/sun/star/awt/tree/TreeExpansionEvent.hpp"
#include "com/sun/star/util/VetoException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline ExpandVetoException::ExpandVetoException() SAL_THROW(())
    : ::css::util::VetoException()
    , Event()
{ }

inline ExpandVetoException::ExpandVetoException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::awt::tree::TreeExpansionEvent& Event_) SAL_THROW(())
    : ::css::util::VetoException(Message_, Context_)
    , Event(Event_)
{ }

ExpandVetoException::ExpandVetoException(ExpandVetoException const & the_other): ::css::util::VetoException(the_other), Event(the_other.Event) {}

ExpandVetoException::~ExpandVetoException() {}

ExpandVetoException & ExpandVetoException::operator =(ExpandVetoException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::util::VetoException::operator =(the_other);
    Event = the_other.Event;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::tree::ExpandVetoException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.awt.tree.ExpandVetoException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::tree::ExpandVetoException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::tree::ExpandVetoException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_TREE_EXPANDVETOEXCEPTION_HPP
