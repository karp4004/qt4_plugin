#ifndef INCLUDED_COM_SUN_STAR_AWT_DOCKINGEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_DOCKINGEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/DockingEvent.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline DockingEvent::DockingEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , TrackingRectangle()
    , MousePos()
    , bLiveMode(false)
    , bInteractive(false)
{
}

inline DockingEvent::DockingEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::awt::Rectangle& TrackingRectangle_, const ::css::awt::Point& MousePos_, const ::sal_Bool& bLiveMode_, const ::sal_Bool& bInteractive_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , TrackingRectangle(TrackingRectangle_)
    , MousePos(MousePos_)
    , bLiveMode(bLiveMode_)
    , bInteractive(bInteractive_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::DockingEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.awt.DockingEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::DockingEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::DockingEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_DOCKINGEVENT_HPP
