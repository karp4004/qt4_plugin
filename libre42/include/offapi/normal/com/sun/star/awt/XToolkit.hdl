#ifndef INCLUDED_COM_SUN_STAR_AWT_XTOOLKIT_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XTOOLKIT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/Rectangle.hdl"
#include "com/sun/star/awt/WindowDescriptor.hdl"
namespace com { namespace sun { namespace star { namespace awt { class XDevice; } } } }
namespace com { namespace sun { namespace star { namespace awt { class XRegion; } } } }
namespace com { namespace sun { namespace star { namespace awt { class XWindowPeer; } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XToolkit : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::awt::XWindowPeer > SAL_CALL getDesktopWindow() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::awt::Rectangle SAL_CALL getWorkArea() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::XWindowPeer > SAL_CALL createWindow( const ::css::awt::WindowDescriptor& Descriptor ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::awt::XWindowPeer > > SAL_CALL createWindows( const ::css::uno::Sequence< ::css::awt::WindowDescriptor >& Descriptors ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::XDevice > SAL_CALL createScreenCompatibleDevice( ::sal_Int32 Width, ::sal_Int32 Height ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::XRegion > SAL_CALL createRegion() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XToolkit() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XToolkit const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XToolkit > *) SAL_THROW(());

#endif
