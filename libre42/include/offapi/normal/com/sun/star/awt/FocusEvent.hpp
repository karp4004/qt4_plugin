#ifndef INCLUDED_COM_SUN_STAR_AWT_FOCUSEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_FOCUSEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/FocusEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline FocusEvent::FocusEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , FocusFlags(0)
    , NextFocus()
    , Temporary(false)
{
}

inline FocusEvent::FocusEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& FocusFlags_, const ::css::uno::Reference< ::css::uno::XInterface >& NextFocus_, const ::sal_Bool& Temporary_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , FocusFlags(FocusFlags_)
    , NextFocus(NextFocus_)
    , Temporary(Temporary_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::FocusEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.awt.FocusEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::FocusEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::FocusEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_FOCUSEVENT_HPP
