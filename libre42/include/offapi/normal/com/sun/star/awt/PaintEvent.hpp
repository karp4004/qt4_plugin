#ifndef INCLUDED_COM_SUN_STAR_AWT_PAINTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_PAINTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/PaintEvent.hdl"

#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline PaintEvent::PaintEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , UpdateRect()
    , Count(0)
{
}

inline PaintEvent::PaintEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::awt::Rectangle& UpdateRect_, const ::sal_Int16& Count_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , UpdateRect(UpdateRect_)
    , Count(Count_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::PaintEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.awt.PaintEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::PaintEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::PaintEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_PAINTEVENT_HPP
