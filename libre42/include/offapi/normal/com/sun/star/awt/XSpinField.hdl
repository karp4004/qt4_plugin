#ifndef INCLUDED_COM_SUN_STAR_AWT_XSPINFIELD_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XSPINFIELD_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { class XSpinListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XSpinField : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addSpinListener( const ::css::uno::Reference< ::css::awt::XSpinListener >& l ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeSpinListener( const ::css::uno::Reference< ::css::awt::XSpinListener >& l ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL up() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL down() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL first() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL last() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL enableRepeat( ::sal_Bool bRepeat ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSpinField() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XSpinField const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XSpinField > *) SAL_THROW(());

#endif
