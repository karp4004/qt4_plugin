#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDCONTROL_HDL
#define INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDCONTROL_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IndexOutOfBoundsException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/util/VetoException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

class SAL_NO_VTABLE XGridControl : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Int32 SAL_CALL getColumnAtPoint( ::sal_Int32 X, ::sal_Int32 Y ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getRowAtPoint( ::sal_Int32 X, ::sal_Int32 Y ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getCurrentColumn() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getCurrentRow() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL goToCell( ::sal_Int32 ColumnIndex, ::sal_Int32 RowIndex ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::util::VetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XGridControl() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::XGridControl const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::grid::XGridControl > *) SAL_THROW(());

#endif
