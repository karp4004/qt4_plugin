#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDDATALISTENER_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDDATALISTENER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/grid/XGridDataListener.hdl"

#include "com/sun/star/awt/grid/GridDataEvent.hpp"
#include "com/sun/star/lang/XEventListener.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::XGridDataListener const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.awt.grid.XGridDataListener" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::awt::grid::XGridDataListener > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::awt::grid::XGridDataListener > >::get();
}

::css::uno::Type const & ::css::awt::grid::XGridDataListener::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::awt::grid::XGridDataListener > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDDATALISTENER_HPP
