#ifndef INCLUDED_COM_SUN_STAR_AWT_XTEXTLAYOUTCONSTRAINS_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XTEXTLAYOUTCONSTRAINS_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/Size.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XTextLayoutConstrains : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::awt::Size SAL_CALL getMinimumSize( ::sal_Int16 nCols, ::sal_Int16 nLines ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL getColumnsAndLines( ::sal_Int16& nCols, ::sal_Int16& nLines ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTextLayoutConstrains() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XTextLayoutConstrains const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XTextLayoutConstrains > *) SAL_THROW(());

#endif
