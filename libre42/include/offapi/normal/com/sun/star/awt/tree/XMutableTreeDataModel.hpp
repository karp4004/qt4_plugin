#ifndef INCLUDED_COM_SUN_STAR_AWT_TREE_XMUTABLETREEDATAMODEL_HPP
#define INCLUDED_COM_SUN_STAR_AWT_TREE_XMUTABLETREEDATAMODEL_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/tree/XMutableTreeDataModel.hdl"

#include "com/sun/star/awt/tree/XMutableTreeNode.hpp"
#include "com/sun/star/awt/tree/XTreeDataModel.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::tree::XMutableTreeDataModel const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.awt.tree.XMutableTreeDataModel" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::awt::tree::XMutableTreeDataModel > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::awt::tree::XMutableTreeDataModel > >::get();
}

::css::uno::Type const & ::css::awt::tree::XMutableTreeDataModel::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::awt::tree::XMutableTreeDataModel > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_AWT_TREE_XMUTABLETREEDATAMODEL_HPP
