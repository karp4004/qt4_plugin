#ifndef INCLUDED_COM_SUN_STAR_AWT_XCONTROLCONTAINER_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XCONTROLCONTAINER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { class XControl; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XControlContainer : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setStatusText( const ::rtl::OUString& StatusText ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::awt::XControl > > SAL_CALL getControls() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::XControl > SAL_CALL getControl( const ::rtl::OUString& aName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addControl( const ::rtl::OUString& Name, const ::css::uno::Reference< ::css::awt::XControl >& Control ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeControl( const ::css::uno::Reference< ::css::awt::XControl >& Control ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XControlContainer() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XControlContainer const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XControlContainer > *) SAL_THROW(());

#endif
