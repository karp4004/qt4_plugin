#ifndef INCLUDED_COM_SUN_STAR_AWT_XSIMPLETABCONTROLLER_HPP
#define INCLUDED_COM_SUN_STAR_AWT_XSIMPLETABCONTROLLER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/XSimpleTabController.hdl"

#include "com/sun/star/awt/XTabListener.hpp"
#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/lang/IndexOutOfBoundsException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XSimpleTabController const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.awt.XSimpleTabController" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::awt::XSimpleTabController > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::awt::XSimpleTabController > >::get();
}

::css::uno::Type const & ::css::awt::XSimpleTabController::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::awt::XSimpleTabController > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_AWT_XSIMPLETABCONTROLLER_HPP
