#ifndef INCLUDED_COM_SUN_STAR_AWT_KEYEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_KEYEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/KeyEvent.hdl"

#include "com/sun/star/awt/InputEvent.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline KeyEvent::KeyEvent() SAL_THROW(())
    : ::css::awt::InputEvent()
    , KeyCode(0)
    , KeyChar(0)
    , KeyFunc(0)
{
}

inline KeyEvent::KeyEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& Modifiers_, const ::sal_Int16& KeyCode_, const ::sal_Unicode& KeyChar_, const ::sal_Int16& KeyFunc_) SAL_THROW(())
    : ::css::awt::InputEvent(Source_, Modifiers_)
    , KeyCode(KeyCode_)
    , KeyChar(KeyChar_)
    , KeyFunc(KeyFunc_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::KeyEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.awt.KeyEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::KeyEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::KeyEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_KEYEVENT_HPP
