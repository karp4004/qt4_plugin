#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDSELECTIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDSELECTIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/grid/GridSelectionEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline GridSelectionEvent::GridSelectionEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , SelectedRowIndexes()
    , SelectedColumnIndexes()
{
}

inline GridSelectionEvent::GridSelectionEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Sequence< ::sal_Int32 >& SelectedRowIndexes_, const ::css::uno::Sequence< ::sal_Int32 >& SelectedColumnIndexes_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , SelectedRowIndexes(SelectedRowIndexes_)
    , SelectedColumnIndexes(SelectedColumnIndexes_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridSelectionEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.awt.grid.GridSelectionEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridSelectionEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::grid::GridSelectionEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDSELECTIONEVENT_HPP
