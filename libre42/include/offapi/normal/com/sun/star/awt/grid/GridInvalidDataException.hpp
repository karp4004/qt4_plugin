#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDDATAEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDDATAEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/awt/grid/GridInvalidDataException.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline GridInvalidDataException::GridInvalidDataException() SAL_THROW(())
    : ::css::uno::RuntimeException()
{ }

inline GridInvalidDataException::GridInvalidDataException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
{ }

GridInvalidDataException::GridInvalidDataException(GridInvalidDataException const & the_other): ::css::uno::RuntimeException(the_other) {}

GridInvalidDataException::~GridInvalidDataException() {}

GridInvalidDataException & GridInvalidDataException::operator =(GridInvalidDataException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridInvalidDataException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.awt.grid.GridInvalidDataException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridInvalidDataException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::grid::GridInvalidDataException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDDATAEXCEPTION_HPP
