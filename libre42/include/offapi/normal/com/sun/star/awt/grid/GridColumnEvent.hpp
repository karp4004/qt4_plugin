#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDCOLUMNEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDCOLUMNEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/grid/GridColumnEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline GridColumnEvent::GridColumnEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , AttributeName()
    , OldValue()
    , NewValue()
    , ColumnIndex(0)
{
}

inline GridColumnEvent::GridColumnEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& AttributeName_, const ::css::uno::Any& OldValue_, const ::css::uno::Any& NewValue_, const ::sal_Int32& ColumnIndex_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , AttributeName(AttributeName_)
    , OldValue(OldValue_)
    , NewValue(NewValue_)
    , ColumnIndex(ColumnIndex_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridColumnEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.awt.grid.GridColumnEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridColumnEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::grid::GridColumnEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDCOLUMNEVENT_HPP
