#ifndef INCLUDED_COM_SUN_STAR_AWT_WINDOWDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_AWT_WINDOWDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/awt/WindowDescriptor.hdl"

#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/awt/WindowClass.hpp"
#include "com/sun/star/awt/XWindowPeer.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline WindowDescriptor::WindowDescriptor() SAL_THROW(())
    : Type(::css::awt::WindowClass_TOP)
    , WindowServiceName()
    , Parent()
    , ParentIndex(0)
    , Bounds()
    , WindowAttributes(0)
{
}

inline WindowDescriptor::WindowDescriptor(const ::css::awt::WindowClass& Type_, const ::rtl::OUString& WindowServiceName_, const ::css::uno::Reference< ::css::awt::XWindowPeer >& Parent_, const ::sal_Int16& ParentIndex_, const ::css::awt::Rectangle& Bounds_, const ::sal_Int32& WindowAttributes_) SAL_THROW(())
    : Type(Type_)
    , WindowServiceName(WindowServiceName_)
    , Parent(Parent_)
    , ParentIndex(ParentIndex_)
    , Bounds(Bounds_)
    , WindowAttributes(WindowAttributes_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::WindowDescriptor const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.awt.WindowDescriptor");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::WindowDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::WindowDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_WINDOWDESCRIPTOR_HPP
