#ifndef INCLUDED_COM_SUN_STAR_AWT_FONTDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_AWT_FONTDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/awt/FontDescriptor.hdl"

#include "com/sun/star/awt/FontSlant.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline FontDescriptor::FontDescriptor() SAL_THROW(())
    : Name()
    , Height(0)
    , Width(0)
    , StyleName()
    , Family(0)
    , CharSet(0)
    , Pitch(0)
    , CharacterWidth(0)
    , Weight(0)
    , Slant(::css::awt::FontSlant_NONE)
    , Underline(0)
    , Strikeout(0)
    , Orientation(0)
    , Kerning(false)
    , WordLineMode(false)
    , Type(0)
{
}

inline FontDescriptor::FontDescriptor(const ::rtl::OUString& Name_, const ::sal_Int16& Height_, const ::sal_Int16& Width_, const ::rtl::OUString& StyleName_, const ::sal_Int16& Family_, const ::sal_Int16& CharSet_, const ::sal_Int16& Pitch_, const float& CharacterWidth_, const float& Weight_, const ::css::awt::FontSlant& Slant_, const ::sal_Int16& Underline_, const ::sal_Int16& Strikeout_, const float& Orientation_, const ::sal_Bool& Kerning_, const ::sal_Bool& WordLineMode_, const ::sal_Int16& Type_) SAL_THROW(())
    : Name(Name_)
    , Height(Height_)
    , Width(Width_)
    , StyleName(StyleName_)
    , Family(Family_)
    , CharSet(CharSet_)
    , Pitch(Pitch_)
    , CharacterWidth(CharacterWidth_)
    , Weight(Weight_)
    , Slant(Slant_)
    , Underline(Underline_)
    , Strikeout(Strikeout_)
    , Orientation(Orientation_)
    , Kerning(Kerning_)
    , WordLineMode(WordLineMode_)
    , Type(Type_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::FontDescriptor const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.awt.FontDescriptor");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::FontDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::FontDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_FONTDESCRIPTOR_HPP
