#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDCOLUMN_HDL
#define INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDCOLUMN_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { namespace grid { class XGridColumnListener; } } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/XComponent.hdl"
#include "com/sun/star/style/HorizontalAlignment.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/util/XCloneable.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

class SAL_NO_VTABLE XGridColumn : public ::css::lang::XComponent, public ::css::util::XCloneable
{
public:

    // Attributes
    virtual ::css::uno::Any SAL_CALL getIdentifier() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setIdentifier( const ::css::uno::Any& _identifier ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getColumnWidth() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setColumnWidth( ::sal_Int32 _columnwidth ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getMinWidth() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMinWidth( ::sal_Int32 _minwidth ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getMaxWidth() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMaxWidth( ::sal_Int32 _maxwidth ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getResizeable() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setResizeable( ::sal_Bool _resizeable ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getFlexibility() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setFlexibility( ::sal_Int32 _flexibility ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::style::HorizontalAlignment SAL_CALL getHorizontalAlign() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setHorizontalAlign( ::css::style::HorizontalAlignment _horizontalalign ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getTitle() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setTitle( const ::rtl::OUString& _title ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getHelpText() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setHelpText( const ::rtl::OUString& _helptext ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getIndex() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getDataColumnIndex() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setDataColumnIndex( ::sal_Int32 _datacolumnindex ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual void SAL_CALL addGridColumnListener( const ::css::uno::Reference< ::css::awt::grid::XGridColumnListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeGridColumnListener( const ::css::uno::Reference< ::css::awt::grid::XGridColumnListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XGridColumn() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::XGridColumn const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::grid::XGridColumn > *) SAL_THROW(());

#endif
