#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDROWSELECTION_HDL
#define INCLUDED_COM_SUN_STAR_AWT_GRID_XGRIDROWSELECTION_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { namespace grid { class XGridSelectionListener; } } } } }
#include "com/sun/star/lang/IndexOutOfBoundsException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

class SAL_NO_VTABLE XGridRowSelection : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL selectAllRows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL selectRow( ::sal_Int32 RowIndex ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL deselectAllRows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL deselectRow( ::sal_Int32 RowIndex ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int32 > SAL_CALL getSelectedRows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL hasSelectedRows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isRowSelected( ::sal_Int32 RowIndex ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addSelectionListener( const ::css::uno::Reference< ::css::awt::grid::XGridSelectionListener >& listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeSelectionListener( const ::css::uno::Reference< ::css::awt::grid::XGridSelectionListener >& listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XGridRowSelection() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::XGridRowSelection const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::grid::XGridRowSelection > *) SAL_THROW(());

#endif
