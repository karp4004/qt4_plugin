#ifndef INCLUDED_COM_SUN_STAR_AWT_XCONTROL_HPP
#define INCLUDED_COM_SUN_STAR_AWT_XCONTROL_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/XControl.hdl"

#include "com/sun/star/awt/XControlModel.hpp"
#include "com/sun/star/awt/XToolkit.hpp"
#include "com/sun/star/awt/XView.hpp"
#include "com/sun/star/awt/XWindowPeer.hpp"
#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XControl const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.awt.XControl" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::awt::XControl > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::awt::XControl > >::get();
}

::css::uno::Type const & ::css::awt::XControl::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::awt::XControl > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_AWT_XCONTROL_HPP
