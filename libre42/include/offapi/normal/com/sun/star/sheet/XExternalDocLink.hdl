#ifndef INCLUDED_COM_SUN_STAR_SHEET_XEXTERNALDOCLINK_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XEXTERNALDOCLINK_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/XEnumerationAccess.hdl"
#include "com/sun/star/container/XIndexAccess.hdl"
#include "com/sun/star/container/XNameAccess.hdl"
namespace com { namespace sun { namespace star { namespace sheet { class XExternalSheetCache; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XExternalDocLink : public ::css::container::XNameAccess, public ::css::container::XIndexAccess, public ::css::container::XEnumerationAccess
{
public:

    // Attributes
    virtual ::sal_Int32 SAL_CALL getTokenIndex() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual ::css::uno::Reference< ::css::sheet::XExternalSheetCache > SAL_CALL addSheetCache( const ::rtl::OUString& aSheetName, ::sal_Bool DynamicCache ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XExternalDocLink() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XExternalDocLink const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XExternalDocLink > *) SAL_THROW(());

#endif
