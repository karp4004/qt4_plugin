#ifndef INCLUDED_COM_SUN_STAR_SHEET_DDELINKINFO_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DDELINKINFO_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DDELinkInfo.hdl"

#include "com/sun/star/sheet/DDEItemInfo.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DDELinkInfo::DDELinkInfo() SAL_THROW(())
    : Service()
    , Topic()
    , Items()
{
}

inline DDELinkInfo::DDELinkInfo(const ::rtl::OUString& Service_, const ::rtl::OUString& Topic_, const ::css::uno::Sequence< ::css::sheet::DDEItemInfo >& Items_) SAL_THROW(())
    : Service(Service_)
    , Topic(Topic_)
    , Items(Items_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DDELinkInfo const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.DDELinkInfo");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DDELinkInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DDELinkInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DDELINKINFO_HPP
