#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDAUTOSHOWINFO_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDAUTOSHOWINFO_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotFieldAutoShowInfo.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotFieldAutoShowInfo::DataPilotFieldAutoShowInfo() SAL_THROW(())
    : IsEnabled(false)
    , ShowItemsMode(0)
    , ItemCount(0)
    , DataField()
{
}

inline DataPilotFieldAutoShowInfo::DataPilotFieldAutoShowInfo(const ::sal_Bool& IsEnabled_, const ::sal_Int32& ShowItemsMode_, const ::sal_Int32& ItemCount_, const ::rtl::OUString& DataField_) SAL_THROW(())
    : IsEnabled(IsEnabled_)
    , ShowItemsMode(ShowItemsMode_)
    , ItemCount(ItemCount_)
    , DataField(DataField_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldAutoShowInfo const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.DataPilotFieldAutoShowInfo");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldAutoShowInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotFieldAutoShowInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDAUTOSHOWINFO_HPP
