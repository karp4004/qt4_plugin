#ifndef INCLUDED_COM_SUN_STAR_SHEET_XDATAPILOTDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_XDATAPILOTDESCRIPTOR_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sheet/XDataPilotDescriptor.hdl"

#include "com/sun/star/container/XIndexAccess.hpp"
#include "com/sun/star/container/XNamed.hpp"
#include "com/sun/star/sheet/XSheetFilterDescriptor.hpp"
#include "com/sun/star/table/CellRangeAddress.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XDataPilotDescriptor const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.sheet.XDataPilotDescriptor" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::sheet::XDataPilotDescriptor > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::sheet::XDataPilotDescriptor > >::get();
}

::css::uno::Type const & ::css::sheet::XDataPilotDescriptor::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::sheet::XDataPilotDescriptor > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_XDATAPILOTDESCRIPTOR_HPP
