#ifndef INCLUDED_COM_SUN_STAR_SHEET_XSPREADSHEET_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_XSPREADSHEET_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sheet/XSpreadsheet.hdl"

#include "com/sun/star/sheet/XSheetCellCursor.hpp"
#include "com/sun/star/sheet/XSheetCellRange.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XSpreadsheet const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.sheet.XSpreadsheet" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::sheet::XSpreadsheet > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::sheet::XSpreadsheet > >::get();
}

::css::uno::Type const & ::css::sheet::XSpreadsheet::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::sheet::XSpreadsheet > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_XSPREADSHEET_HPP
