#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLERESULTDATA_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLERESULTDATA_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotTableResultData.hdl"

#include "com/sun/star/sheet/DataPilotFieldFilter.hpp"
#include "com/sun/star/sheet/DataResult.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotTableResultData::DataPilotTableResultData() SAL_THROW(())
    : FieldFilters()
    , DataFieldIndex(0)
    , Result()
{
}

inline DataPilotTableResultData::DataPilotTableResultData(const ::css::uno::Sequence< ::css::sheet::DataPilotFieldFilter >& FieldFilters_, const ::sal_Int32& DataFieldIndex_, const ::css::sheet::DataResult& Result_) SAL_THROW(())
    : FieldFilters(FieldFilters_)
    , DataFieldIndex(DataFieldIndex_)
    , Result(Result_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotTableResultData const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.DataPilotTableResultData");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotTableResultData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotTableResultData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLERESULTDATA_HPP
