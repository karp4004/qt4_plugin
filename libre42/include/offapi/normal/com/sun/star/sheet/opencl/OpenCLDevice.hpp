#ifndef INCLUDED_COM_SUN_STAR_SHEET_OPENCL_OPENCLDEVICE_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_OPENCL_OPENCLDEVICE_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/opencl/OpenCLDevice.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet { namespace opencl {

inline OpenCLDevice::OpenCLDevice() SAL_THROW(())
    : Name()
    , Vendor()
    , Driver()
{
}

inline OpenCLDevice::OpenCLDevice(const ::rtl::OUString& Name_, const ::rtl::OUString& Vendor_, const ::rtl::OUString& Driver_) SAL_THROW(())
    : Name(Name_)
    , Vendor(Vendor_)
    , Driver(Driver_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace opencl {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::opencl::OpenCLDevice const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.opencl.OpenCLDevice");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::opencl::OpenCLDevice const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::opencl::OpenCLDevice >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_OPENCL_OPENCLDEVICE_HPP
