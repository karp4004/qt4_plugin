#ifndef INCLUDED_COM_SUN_STAR_SHEET_FUNCTIONARGUMENT_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_FUNCTIONARGUMENT_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/FunctionArgument.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline FunctionArgument::FunctionArgument() SAL_THROW(())
    : Name()
    , Description()
    , IsOptional(false)
{
}

inline FunctionArgument::FunctionArgument(const ::rtl::OUString& Name_, const ::rtl::OUString& Description_, const ::sal_Bool& IsOptional_) SAL_THROW(())
    : Name(Name_)
    , Description(Description_)
    , IsOptional(IsOptional_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::FunctionArgument const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.FunctionArgument");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::FunctionArgument const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::FunctionArgument >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_FUNCTIONARGUMENT_HPP
