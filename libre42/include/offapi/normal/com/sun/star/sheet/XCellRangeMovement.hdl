#ifndef INCLUDED_COM_SUN_STAR_SHEET_XCELLRANGEMOVEMENT_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XCELLRANGEMOVEMENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sheet/CellDeleteMode.hdl"
#include "com/sun/star/sheet/CellInsertMode.hdl"
#include "com/sun/star/table/CellAddress.hdl"
#include "com/sun/star/table/CellRangeAddress.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XCellRangeMovement : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL insertCells( const ::css::table::CellRangeAddress& aRange, ::css::sheet::CellInsertMode nMode ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeRange( const ::css::table::CellRangeAddress& aRange, ::css::sheet::CellDeleteMode nMode ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL moveRange( const ::css::table::CellAddress& aDestination, const ::css::table::CellRangeAddress& aSource ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL copyRange( const ::css::table::CellAddress& aDestination, const ::css::table::CellRangeAddress& aSource ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XCellRangeMovement() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XCellRangeMovement const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XCellRangeMovement > *) SAL_THROW(());

#endif
