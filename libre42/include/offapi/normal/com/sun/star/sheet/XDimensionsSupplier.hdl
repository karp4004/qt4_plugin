#ifndef INCLUDED_COM_SUN_STAR_SHEET_XDIMENSIONSSUPPLIER_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XDIMENSIONSSUPPLIER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace container { class XNameAccess; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XDimensionsSupplier : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::container::XNameAccess > SAL_CALL getDimensions() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDimensionsSupplier() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XDimensionsSupplier const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XDimensionsSupplier > *) SAL_THROW(());

#endif
