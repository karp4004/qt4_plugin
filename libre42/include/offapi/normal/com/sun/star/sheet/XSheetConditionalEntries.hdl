#ifndef INCLUDED_COM_SUN_STAR_SHEET_XSHEETCONDITIONALENTRIES_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XSHEETCONDITIONALENTRIES_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
#include "com/sun/star/container/XIndexAccess.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XSheetConditionalEntries : public ::css::container::XIndexAccess
{
public:

    // Methods
    virtual void SAL_CALL addNew( const ::css::uno::Sequence< ::css::beans::PropertyValue >& aConditionalEntry ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeByIndex( ::sal_Int32 nIndex ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL clear() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSheetConditionalEntries() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XSheetConditionalEntries const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XSheetConditionalEntries > *) SAL_THROW(());

#endif
