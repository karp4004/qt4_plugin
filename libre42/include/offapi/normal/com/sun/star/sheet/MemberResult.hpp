#ifndef INCLUDED_COM_SUN_STAR_SHEET_MEMBERRESULT_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_MEMBERRESULT_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/MemberResult.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline MemberResult::MemberResult() SAL_THROW(())
    : Name()
    , Caption()
    , Flags(0)
{
}

inline MemberResult::MemberResult(const ::rtl::OUString& Name_, const ::rtl::OUString& Caption_, const ::sal_Int32& Flags_) SAL_THROW(())
    : Name(Name_)
    , Caption(Caption_)
    , Flags(Flags_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::MemberResult const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.MemberResult");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::MemberResult const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::MemberResult >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_MEMBERRESULT_HPP
