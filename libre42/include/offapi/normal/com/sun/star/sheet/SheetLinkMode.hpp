#ifndef INCLUDED_COM_SUN_STAR_SHEET_SHEETLINKMODE_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_SHEETLINKMODE_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/SheetLinkMode.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::SheetLinkMode const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_ENUM, "com.sun.star.sheet.SheetLinkMode" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::SheetLinkMode const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::SheetLinkMode >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_SHEETLINKMODE_HPP
