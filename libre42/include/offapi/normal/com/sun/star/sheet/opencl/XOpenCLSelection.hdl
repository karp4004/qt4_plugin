#ifndef INCLUDED_COM_SUN_STAR_SHEET_OPENCL_XOPENCLSELECTION_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_OPENCL_XOPENCLSELECTION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sheet/opencl/OpenCLPlatform.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace opencl {

class SAL_NO_VTABLE XOpenCLSelection : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL isOpenCLEnabled() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL enableOpenCL( ::sal_Bool enable ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL enableAutomaticDeviceSelection( ::sal_Bool force ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL disableAutomaticDeviceSelection() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL selectOpenCLDevice( ::sal_Int32 platform, ::sal_Int32 device ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getPlatformID() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getDeviceID() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::sheet::opencl::OpenCLPlatform > SAL_CALL getOpenCLPlatforms() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XOpenCLSelection() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::opencl::XOpenCLSelection const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::opencl::XOpenCLSelection > *) SAL_THROW(());

#endif
