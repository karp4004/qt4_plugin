#ifndef INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/TableFilterField.hdl"

#include "com/sun/star/sheet/FilterConnection.hpp"
#include "com/sun/star/sheet/FilterOperator.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline TableFilterField::TableFilterField() SAL_THROW(())
    : Connection(::css::sheet::FilterConnection_AND)
    , Field(0)
    , Operator(::css::sheet::FilterOperator_EMPTY)
    , IsNumeric(false)
    , NumericValue(0)
    , StringValue()
{
}

inline TableFilterField::TableFilterField(const ::css::sheet::FilterConnection& Connection_, const ::sal_Int32& Field_, const ::css::sheet::FilterOperator& Operator_, const ::sal_Bool& IsNumeric_, const double& NumericValue_, const ::rtl::OUString& StringValue_) SAL_THROW(())
    : Connection(Connection_)
    , Field(Field_)
    , Operator(Operator_)
    , IsNumeric(IsNumeric_)
    , NumericValue(NumericValue_)
    , StringValue(StringValue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::TableFilterField const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.TableFilterField");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::TableFilterField const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::TableFilterField >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD_HPP
