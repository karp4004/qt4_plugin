#ifndef INCLUDED_COM_SUN_STAR_SHEET_XSHEETANNOTATIONS_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_XSHEETANNOTATIONS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sheet/XSheetAnnotations.hdl"

#include "com/sun/star/container/XIndexAccess.hpp"
#include "com/sun/star/table/CellAddress.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XSheetAnnotations const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.sheet.XSheetAnnotations" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::sheet::XSheetAnnotations > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::sheet::XSheetAnnotations > >::get();
}

::css::uno::Type const & ::css::sheet::XSheetAnnotations::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::sheet::XSheetAnnotations > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_XSHEETANNOTATIONS_HPP
