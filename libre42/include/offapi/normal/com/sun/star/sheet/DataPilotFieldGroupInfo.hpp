#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDGROUPINFO_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDGROUPINFO_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotFieldGroupInfo.hdl"

#include "com/sun/star/container/XNameAccess.hpp"
#include "com/sun/star/sheet/XDataPilotField.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotFieldGroupInfo::DataPilotFieldGroupInfo() SAL_THROW(())
    : HasAutoStart(false)
    , HasAutoEnd(false)
    , HasDateValues(false)
    , Start(0)
    , End(0)
    , Step(0)
    , GroupBy(0)
    , SourceField()
    , Groups()
{
}

inline DataPilotFieldGroupInfo::DataPilotFieldGroupInfo(const ::sal_Bool& HasAutoStart_, const ::sal_Bool& HasAutoEnd_, const ::sal_Bool& HasDateValues_, const double& Start_, const double& End_, const double& Step_, const ::sal_Int32& GroupBy_, const ::css::uno::Reference< ::css::sheet::XDataPilotField >& SourceField_, const ::css::uno::Reference< ::css::container::XNameAccess >& Groups_) SAL_THROW(())
    : HasAutoStart(HasAutoStart_)
    , HasAutoEnd(HasAutoEnd_)
    , HasDateValues(HasDateValues_)
    , Start(Start_)
    , End(End_)
    , Step(Step_)
    , GroupBy(GroupBy_)
    , SourceField(SourceField_)
    , Groups(Groups_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldGroupInfo const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.DataPilotFieldGroupInfo");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldGroupInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotFieldGroupInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDGROUPINFO_HPP
