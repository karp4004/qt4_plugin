#ifndef INCLUDED_COM_SUN_STAR_SHEET_SUBTOTALCOLUMN_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_SUBTOTALCOLUMN_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/SubTotalColumn.hdl"

#include "com/sun/star/sheet/GeneralFunction.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline SubTotalColumn::SubTotalColumn() SAL_THROW(())
    : Column(0)
    , Function(::css::sheet::GeneralFunction_NONE)
{
}

inline SubTotalColumn::SubTotalColumn(const ::sal_Int32& Column_, const ::css::sheet::GeneralFunction& Function_) SAL_THROW(())
    : Column(Column_)
    , Function(Function_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::SubTotalColumn const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sheet.SubTotalColumn");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::SubTotalColumn const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::SubTotalColumn >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_SUBTOTALCOLUMN_HPP
