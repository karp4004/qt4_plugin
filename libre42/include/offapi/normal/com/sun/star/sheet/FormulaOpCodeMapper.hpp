#ifndef INCLUDED_COM_SUN_STAR_SHEET_FORMULAOPCODEMAPPER_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_FORMULAOPCODEMAPPER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/sheet/XFormulaOpCodeMapper.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace sheet {

class FormulaOpCodeMapper {
public:
    static ::css::uno::Reference< ::css::sheet::XFormulaOpCodeMapper > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::sheet::XFormulaOpCodeMapper > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::sheet::XFormulaOpCodeMapper >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.sheet.FormulaOpCodeMapper" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.sheet.FormulaOpCodeMapper of type com.sun.star.sheet.XFormulaOpCodeMapper: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.sheet.FormulaOpCodeMapper of type com.sun.star.sheet.XFormulaOpCodeMapper" ), the_context);
        }
        return the_instance;
    }

private:
    FormulaOpCodeMapper(); // not implemented
    FormulaOpCodeMapper(FormulaOpCodeMapper &); // not implemented
    ~FormulaOpCodeMapper(); // not implemented
    void operator =(FormulaOpCodeMapper); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_SHEET_FORMULAOPCODEMAPPER_HPP
