#ifndef INCLUDED_COM_SUN_STAR_RESOURCE_STRINGRESOURCEWITHSTORAGE_HPP
#define INCLUDED_COM_SUN_STAR_RESOURCE_STRINGRESOURCEWITHSTORAGE_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/embed/XStorage.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/resource/XStringResourceWithStorage.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace resource {

class StringResourceWithStorage {
public:
    static ::css::uno::Reference< ::css::resource::XStringResourceWithStorage > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Reference< ::css::embed::XStorage >& Storage, ::sal_Bool ReadOnly, const ::css::lang::Locale& locale, const ::rtl::OUString& BaseName, const ::rtl::OUString& Comment) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(5);
        the_arguments[0] <<= Storage;
        the_arguments[1] <<= ReadOnly;
        the_arguments[2] <<= locale;
        the_arguments[3] <<= BaseName;
        the_arguments[4] <<= Comment;
        ::css::uno::Reference< ::css::resource::XStringResourceWithStorage > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::resource::XStringResourceWithStorage >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.resource.StringResourceWithStorage" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.resource.StringResourceWithStorage of type com.sun.star.resource.XStringResourceWithStorage: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.resource.StringResourceWithStorage of type com.sun.star.resource.XStringResourceWithStorage" ), the_context);
        }
        return the_instance;
    }

private:
    StringResourceWithStorage(); // not implemented
    StringResourceWithStorage(StringResourceWithStorage &); // not implemented
    ~StringResourceWithStorage(); // not implemented
    void operator =(StringResourceWithStorage); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_RESOURCE_STRINGRESOURCEWITHSTORAGE_HPP
