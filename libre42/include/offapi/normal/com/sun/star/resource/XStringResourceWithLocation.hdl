#ifndef INCLUDED_COM_SUN_STAR_RESOURCE_XSTRINGRESOURCEWITHLOCATION_HDL
#define INCLUDED_COM_SUN_STAR_RESOURCE_XSTRINGRESOURCEWITHLOCATION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/resource/XStringResourcePersistence.hdl"
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace resource {

class SAL_NO_VTABLE XStringResourceWithLocation : public ::css::resource::XStringResourcePersistence
{
public:

    // Methods
    virtual void SAL_CALL storeAsURL( const ::rtl::OUString& URL ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setURL( const ::rtl::OUString& URL ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XStringResourceWithLocation() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::resource::XStringResourceWithLocation const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::resource::XStringResourceWithLocation > *) SAL_THROW(());

#endif
