#ifndef INCLUDED_COM_SUN_STAR_RESOURCE_XRESOURCEBUNDLE_HPP
#define INCLUDED_COM_SUN_STAR_RESOURCE_XRESOURCEBUNDLE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/resource/XResourceBundle.hdl"

#include "com/sun/star/container/XNameAccess.hpp"
#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/resource/XResourceBundle.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace resource {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::resource::XResourceBundle const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.resource.XResourceBundle" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::resource::XResourceBundle > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::resource::XResourceBundle > >::get();
}

::css::uno::Type const & ::css::resource::XResourceBundle::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::resource::XResourceBundle > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_RESOURCE_XRESOURCEBUNDLE_HPP
