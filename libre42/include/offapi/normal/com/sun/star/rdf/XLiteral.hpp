#ifndef INCLUDED_COM_SUN_STAR_RDF_XLITERAL_HPP
#define INCLUDED_COM_SUN_STAR_RDF_XLITERAL_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/rdf/XLiteral.hdl"

#include "com/sun/star/rdf/XNode.hpp"
#include "com/sun/star/rdf/XURI.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace rdf {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rdf::XLiteral const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.rdf.XLiteral" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::rdf::XLiteral > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::rdf::XLiteral > >::get();
}

::css::uno::Type const & ::css::rdf::XLiteral::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::rdf::XLiteral > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_RDF_XLITERAL_HPP
