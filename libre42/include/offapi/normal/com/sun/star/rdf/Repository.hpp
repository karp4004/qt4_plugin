#ifndef INCLUDED_COM_SUN_STAR_RDF_REPOSITORY_HPP
#define INCLUDED_COM_SUN_STAR_RDF_REPOSITORY_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/rdf/XRepository.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace rdf {

class Repository {
public:
    static ::css::uno::Reference< ::css::rdf::XRepository > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::rdf::XRepository > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::rdf::XRepository >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.rdf.Repository" ), ::css::uno::Sequence< ::css::uno::Any >(), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.rdf.Repository of type com.sun.star.rdf.XRepository: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.rdf.Repository of type com.sun.star.rdf.XRepository" ), the_context);
        }
        return the_instance;
    }

private:
    Repository(); // not implemented
    Repository(Repository &); // not implemented
    ~Repository(); // not implemented
    void operator =(Repository); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_RDF_REPOSITORY_HPP
