#ifndef INCLUDED_COM_SUN_STAR_RDF_XDOCUMENTREPOSITORY_HPP
#define INCLUDED_COM_SUN_STAR_RDF_XDOCUMENTREPOSITORY_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/rdf/XDocumentRepository.hdl"

#include "com/sun/star/beans/Pair.hpp"
#include "com/sun/star/container/XEnumeration.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/rdf/RepositoryException.hpp"
#include "com/sun/star/rdf/Statement.hpp"
#include "com/sun/star/rdf/XMetadatable.hpp"
#include "com/sun/star/rdf/XNode.hpp"
#include "com/sun/star/rdf/XRepository.hpp"
#include "com/sun/star/rdf/XResource.hpp"
#include "com/sun/star/rdf/XURI.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace rdf {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rdf::XDocumentRepository const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.rdf.XDocumentRepository" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::rdf::XDocumentRepository > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::rdf::XDocumentRepository > >::get();
}

::css::uno::Type const & ::css::rdf::XDocumentRepository::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::rdf::XDocumentRepository > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_RDF_XDOCUMENTREPOSITORY_HPP
