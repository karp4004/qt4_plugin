#ifndef INCLUDED_COM_SUN_STAR_RDF_STATEMENT_HPP
#define INCLUDED_COM_SUN_STAR_RDF_STATEMENT_HPP

#include "sal/config.h"

#include "com/sun/star/rdf/Statement.hdl"

#include "com/sun/star/rdf/XNode.hpp"
#include "com/sun/star/rdf/XResource.hpp"
#include "com/sun/star/rdf/XURI.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rdf {

inline Statement::Statement() SAL_THROW(())
    : Subject()
    , Predicate()
    , Object()
    , Graph()
{
}

inline Statement::Statement(const ::css::uno::Reference< ::css::rdf::XResource >& Subject_, const ::css::uno::Reference< ::css::rdf::XURI >& Predicate_, const ::css::uno::Reference< ::css::rdf::XNode >& Object_, const ::css::uno::Reference< ::css::rdf::XURI >& Graph_) SAL_THROW(())
    : Subject(Subject_)
    , Predicate(Predicate_)
    , Object(Object_)
    , Graph(Graph_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rdf {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rdf::Statement const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.rdf.Statement");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rdf::Statement const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rdf::Statement >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RDF_STATEMENT_HPP
