#ifndef INCLUDED_COM_SUN_STAR_RDF_XDOCUMENTMETADATAACCESS_HPP
#define INCLUDED_COM_SUN_STAR_RDF_XDOCUMENTMETADATAACCESS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/rdf/XDocumentMetadataAccess.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/beans/StringPair.hpp"
#include "com/sun/star/container/ElementExistException.hpp"
#include "com/sun/star/container/NoSuchElementException.hpp"
#include "com/sun/star/datatransfer/UnsupportedFlavorException.hpp"
#include "com/sun/star/embed/XStorage.hpp"
#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/io/XInputStream.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/WrappedTargetException.hpp"
#include "com/sun/star/rdf/ParseException.hpp"
#include "com/sun/star/rdf/XMetadatable.hpp"
#include "com/sun/star/rdf/XRepositorySupplier.hpp"
#include "com/sun/star/rdf/XURI.hpp"
#include "com/sun/star/task/XInteractionHandler.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace rdf {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rdf::XDocumentMetadataAccess const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.rdf.XDocumentMetadataAccess" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::rdf::XDocumentMetadataAccess > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::rdf::XDocumentMetadataAccess > >::get();
}

::css::uno::Type const & ::css::rdf::XDocumentMetadataAccess::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::rdf::XDocumentMetadataAccess > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_RDF_XDOCUMENTMETADATAACCESS_HPP
