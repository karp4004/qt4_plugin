#ifndef INCLUDED_COM_SUN_STAR_RDF_XREIFIEDSTATEMENT_HDL
#define INCLUDED_COM_SUN_STAR_RDF_XREIFIEDSTATEMENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/rdf/Statement.hdl"
#include "com/sun/star/rdf/XResource.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace rdf {

class SAL_NO_VTABLE XReifiedStatement : public ::css::rdf::XResource
{
public:

    // Attributes
    virtual ::css::rdf::Statement SAL_CALL getStatement() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XReifiedStatement() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rdf::XReifiedStatement const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::rdf::XReifiedStatement > *) SAL_THROW(());

#endif
