#ifndef INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSBRIDGE_HPP
#define INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSBRIDGE_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/awt/XExtendedToolkit.hpp"
#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace accessibility {

class AccessBridge {
public:
    static ::css::uno::Reference< ::css::lang::XComponent > createWithToolkit(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Reference< ::css::awt::XExtendedToolkit >& toolkit) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= toolkit;
        ::css::uno::Reference< ::css::lang::XComponent > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::lang::XComponent >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.accessibility.AccessBridge" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.accessibility.AccessBridge of type com.sun.star.lang.XComponent: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.accessibility.AccessBridge of type com.sun.star.lang.XComponent" ), the_context);
        }
        return the_instance;
    }

private:
    AccessBridge(); // not implemented
    AccessBridge(AccessBridge &); // not implemented
    ~AccessBridge(); // not implemented
    void operator =(AccessBridge); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSBRIDGE_HPP
