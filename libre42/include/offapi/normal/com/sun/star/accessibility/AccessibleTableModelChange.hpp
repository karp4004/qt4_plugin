#ifndef INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLETABLEMODELCHANGE_HPP
#define INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLETABLEMODELCHANGE_HPP

#include "sal/config.h"

#include "com/sun/star/accessibility/AccessibleTableModelChange.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace accessibility {

inline AccessibleTableModelChange::AccessibleTableModelChange() SAL_THROW(())
    : Type(0)
    , FirstRow(0)
    , LastRow(0)
    , FirstColumn(0)
    , LastColumn(0)
{
}

inline AccessibleTableModelChange::AccessibleTableModelChange(const ::sal_Int16& Type_, const ::sal_Int32& FirstRow_, const ::sal_Int32& LastRow_, const ::sal_Int32& FirstColumn_, const ::sal_Int32& LastColumn_) SAL_THROW(())
    : Type(Type_)
    , FirstRow(FirstRow_)
    , LastRow(LastRow_)
    , FirstColumn(FirstColumn_)
    , LastColumn(LastColumn_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace accessibility {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::accessibility::AccessibleTableModelChange const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.accessibility.AccessibleTableModelChange");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::accessibility::AccessibleTableModelChange const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::accessibility::AccessibleTableModelChange >::get();
}

#endif // INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLETABLEMODELCHANGE_HPP
