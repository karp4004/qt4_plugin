#ifndef INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLEEVENTOBJECT_HPP
#define INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLEEVENTOBJECT_HPP

#include "sal/config.h"

#include "com/sun/star/accessibility/AccessibleEventObject.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace accessibility {

inline AccessibleEventObject::AccessibleEventObject() SAL_THROW(())
    : ::css::lang::EventObject()
    , EventId(0)
    , NewValue()
    , OldValue()
{
}

inline AccessibleEventObject::AccessibleEventObject(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& EventId_, const ::css::uno::Any& NewValue_, const ::css::uno::Any& OldValue_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , EventId(EventId_)
    , NewValue(NewValue_)
    , OldValue(OldValue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace accessibility {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::accessibility::AccessibleEventObject const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.accessibility.AccessibleEventObject");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::accessibility::AccessibleEventObject const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::accessibility::AccessibleEventObject >::get();
}

#endif // INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLEEVENTOBJECT_HPP
