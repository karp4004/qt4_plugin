#ifndef INCLUDED_COM_SUN_STAR_ACCESSIBILITY_XACCESSIBLECOMPONENT_HDL
#define INCLUDED_COM_SUN_STAR_ACCESSIBILITY_XACCESSIBLECOMPONENT_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace accessibility { class XAccessible; } } } }
#include "com/sun/star/awt/Point.hdl"
#include "com/sun/star/awt/Rectangle.hdl"
#include "com/sun/star/awt/Size.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/util/Color.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace accessibility {

class SAL_NO_VTABLE XAccessibleComponent : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL containsPoint( const ::css::awt::Point& Point ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::accessibility::XAccessible > SAL_CALL getAccessibleAtPoint( const ::css::awt::Point& Point ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::awt::Rectangle SAL_CALL getBounds() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::awt::Point SAL_CALL getLocation() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::awt::Point SAL_CALL getLocationOnScreen() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::awt::Size SAL_CALL getSize() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL grabFocus() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getForeground() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getBackground() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAccessibleComponent() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::accessibility::XAccessibleComponent const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::accessibility::XAccessibleComponent > *) SAL_THROW(());

#endif
