#ifndef INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLERELATION_HPP
#define INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLERELATION_HPP

#include "sal/config.h"

#include "com/sun/star/accessibility/AccessibleRelation.hdl"

#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace accessibility {

inline AccessibleRelation::AccessibleRelation() SAL_THROW(())
    : RelationType(0)
    , TargetSet()
{
}

inline AccessibleRelation::AccessibleRelation(const ::sal_Int16& RelationType_, const ::css::uno::Sequence< ::css::uno::Reference< ::css::uno::XInterface > >& TargetSet_) SAL_THROW(())
    : RelationType(RelationType_)
    , TargetSet(TargetSet_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace accessibility {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::accessibility::AccessibleRelation const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.accessibility.AccessibleRelation");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::accessibility::AccessibleRelation const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::accessibility::AccessibleRelation >::get();
}

#endif // INCLUDED_COM_SUN_STAR_ACCESSIBILITY_ACCESSIBLERELATION_HPP
