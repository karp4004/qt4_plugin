#ifndef INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_XZIPFILEACCESS_HPP
#define INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_XZIPFILEACCESS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/packages/zip/XZipFileAccess.hdl"

#include "com/sun/star/container/NoSuchElementException.hpp"
#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/io/XInputStream.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace packages { namespace zip {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::packages::zip::XZipFileAccess const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.packages.zip.XZipFileAccess" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::packages::zip::XZipFileAccess > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::packages::zip::XZipFileAccess > >::get();
}

::css::uno::Type const & ::css::packages::zip::XZipFileAccess::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::packages::zip::XZipFileAccess > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_XZIPFILEACCESS_HPP
