#ifndef INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_ZIPIOEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_ZIPIOEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/packages/zip/ZipIOException.hdl"

#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace packages { namespace zip {

inline ZipIOException::ZipIOException() SAL_THROW(())
    : ::css::io::IOException()
{ }

inline ZipIOException::ZipIOException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::io::IOException(Message_, Context_)
{ }

ZipIOException::ZipIOException(ZipIOException const & the_other): ::css::io::IOException(the_other) {}

ZipIOException::~ZipIOException() {}

ZipIOException & ZipIOException::operator =(ZipIOException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::io::IOException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace packages { namespace zip {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::packages::zip::ZipIOException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.packages.zip.ZipIOException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::packages::zip::ZipIOException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::packages::zip::ZipIOException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_ZIPIOEXCEPTION_HPP
