#ifndef INCLUDED_COM_SUN_STAR_PACKAGES_XDATASINKENCRSUPPORT_HPP
#define INCLUDED_COM_SUN_STAR_PACKAGES_XDATASINKENCRSUPPORT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/packages/XDataSinkEncrSupport.hdl"

#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/io/XInputStream.hpp"
#include "com/sun/star/packages/EncryptionNotAllowedException.hpp"
#include "com/sun/star/packages/NoEncryptionException.hpp"
#include "com/sun/star/packages/NoRawFormatException.hpp"
#include "com/sun/star/packages/WrongPasswordException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace packages {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::packages::XDataSinkEncrSupport const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.packages.XDataSinkEncrSupport" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::packages::XDataSinkEncrSupport > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::packages::XDataSinkEncrSupport > >::get();
}

::css::uno::Type const & ::css::packages::XDataSinkEncrSupport::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::packages::XDataSinkEncrSupport > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_PACKAGES_XDATASINKENCRSUPPORT_HPP
