#ifndef INCLUDED_COM_SUN_STAR_UTIL_DATETIMERANGE_HPP
#define INCLUDED_COM_SUN_STAR_UTIL_DATETIMERANGE_HPP

#include "sal/config.h"

#include "com/sun/star/util/DateTimeRange.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace util {

inline DateTimeRange::DateTimeRange() SAL_THROW(())
    : StartNanoSeconds(0)
    , StartSeconds(0)
    , StartMinutes(0)
    , StartHours(0)
    , StartDay(0)
    , StartMonth(0)
    , StartYear(0)
    , EndNanoSeconds(0)
    , EndSeconds(0)
    , EndMinutes(0)
    , EndHours(0)
    , EndDay(0)
    , EndMonth(0)
    , EndYear(0)
    , IsUTC(false)
{
}

inline DateTimeRange::DateTimeRange(const ::sal_uInt32& StartNanoSeconds_, const ::sal_uInt16& StartSeconds_, const ::sal_uInt16& StartMinutes_, const ::sal_uInt16& StartHours_, const ::sal_uInt16& StartDay_, const ::sal_uInt16& StartMonth_, const ::sal_Int16& StartYear_, const ::sal_uInt32& EndNanoSeconds_, const ::sal_uInt16& EndSeconds_, const ::sal_uInt16& EndMinutes_, const ::sal_uInt16& EndHours_, const ::sal_uInt16& EndDay_, const ::sal_uInt16& EndMonth_, const ::sal_Int16& EndYear_, const ::sal_Bool& IsUTC_) SAL_THROW(())
    : StartNanoSeconds(StartNanoSeconds_)
    , StartSeconds(StartSeconds_)
    , StartMinutes(StartMinutes_)
    , StartHours(StartHours_)
    , StartDay(StartDay_)
    , StartMonth(StartMonth_)
    , StartYear(StartYear_)
    , EndNanoSeconds(EndNanoSeconds_)
    , EndSeconds(EndSeconds_)
    , EndMinutes(EndMinutes_)
    , EndHours(EndHours_)
    , EndDay(EndDay_)
    , EndMonth(EndMonth_)
    , EndYear(EndYear_)
    , IsUTC(IsUTC_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace util {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::DateTimeRange const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.util.DateTimeRange");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::util::DateTimeRange const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::util::DateTimeRange >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UTIL_DATETIMERANGE_HPP
