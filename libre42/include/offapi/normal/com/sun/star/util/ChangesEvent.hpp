#ifndef INCLUDED_COM_SUN_STAR_UTIL_CHANGESEVENT_HPP
#define INCLUDED_COM_SUN_STAR_UTIL_CHANGESEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/util/ChangesEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/util/ChangesSet.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace util {

inline ChangesEvent::ChangesEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Base()
    , Changes()
{
}

inline ChangesEvent::ChangesEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Any& Base_, const ::css::uno::Sequence< ::css::util::ElementChange >& Changes_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Base(Base_)
    , Changes(Changes_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace util {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::ChangesEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.util.ChangesEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::util::ChangesEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::util::ChangesEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UTIL_CHANGESEVENT_HPP
