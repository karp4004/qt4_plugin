#ifndef INCLUDED_COM_SUN_STAR_UTIL_NOTLOCKEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UTIL_NOTLOCKEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/util/NotLockedException.hdl"

#include "com/sun/star/util/InvalidStateException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace util {

inline NotLockedException::NotLockedException() SAL_THROW(())
    : ::css::util::InvalidStateException()
{ }

inline NotLockedException::NotLockedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::util::InvalidStateException(Message_, Context_)
{ }

NotLockedException::NotLockedException(NotLockedException const & the_other): ::css::util::InvalidStateException(the_other) {}

NotLockedException::~NotLockedException() {}

NotLockedException & NotLockedException::operator =(NotLockedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::util::InvalidStateException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace util {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::NotLockedException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.util.NotLockedException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::util::NotLockedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::util::NotLockedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UTIL_NOTLOCKEDEXCEPTION_HPP
