#ifndef INCLUDED_COM_SUN_STAR_UTIL_DATE_HPP
#define INCLUDED_COM_SUN_STAR_UTIL_DATE_HPP

#include "sal/config.h"

#include "com/sun/star/util/Date.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace util {

inline Date::Date() SAL_THROW(())
    : Day(0)
    , Month(0)
    , Year(0)
{
}

inline Date::Date(const ::sal_uInt16& Day_, const ::sal_uInt16& Month_, const ::sal_Int16& Year_) SAL_THROW(())
    : Day(Day_)
    , Month(Month_)
    , Year(Year_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace util {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::Date const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.util.Date");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::util::Date const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::util::Date >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UTIL_DATE_HPP
