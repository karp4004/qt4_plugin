#ifndef INCLUDED_COM_SUN_STAR_UTIL_THEOFFICEINSTALLATIONDIRECTORIES_HPP
#define INCLUDED_COM_SUN_STAR_UTIL_THEOFFICEINSTALLATIONDIRECTORIES_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/util/XOfficeInstallationDirectories.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace util {

class theOfficeInstallationDirectories {
public:
    static ::css::uno::Reference< ::css::util::XOfficeInstallationDirectories > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::util::XOfficeInstallationDirectories > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.util.theOfficeInstallationDirectories" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.util.theOfficeInstallationDirectories of type com.sun.star.util.XOfficeInstallationDirectories" ), the_context);
        }
        return instance;
    }

private:
    theOfficeInstallationDirectories(); // not implemented
    theOfficeInstallationDirectories(theOfficeInstallationDirectories &); // not implemented
    ~theOfficeInstallationDirectories(); // not implemented
    void operator =(theOfficeInstallationDirectories); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_UTIL_THEOFFICEINSTALLATIONDIRECTORIES_HPP
