#ifndef INCLUDED_COM_SUN_STAR_UTIL_XREFRESHABLE_HDL
#define INCLUDED_COM_SUN_STAR_UTIL_XREFRESHABLE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
namespace com { namespace sun { namespace star { namespace util { class XRefreshListener; } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace util {

class SAL_NO_VTABLE XRefreshable : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL refresh() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addRefreshListener( const ::css::uno::Reference< ::css::util::XRefreshListener >& l ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeRefreshListener( const ::css::uno::Reference< ::css::util::XRefreshListener >& l ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XRefreshable() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::XRefreshable const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::util::XRefreshable > *) SAL_THROW(());

#endif
