#ifndef INCLUDED_COM_SUN_STAR_UTIL_MALFORMEDNUMBERFORMATEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UTIL_MALFORMEDNUMBERFORMATEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/util/MalformedNumberFormatException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace util {

inline MalformedNumberFormatException::MalformedNumberFormatException() SAL_THROW(())
    : ::css::uno::Exception()
    , CheckPos(0)
{ }

inline MalformedNumberFormatException::MalformedNumberFormatException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& CheckPos_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , CheckPos(CheckPos_)
{ }

MalformedNumberFormatException::MalformedNumberFormatException(MalformedNumberFormatException const & the_other): ::css::uno::Exception(the_other), CheckPos(the_other.CheckPos) {}

MalformedNumberFormatException::~MalformedNumberFormatException() {}

MalformedNumberFormatException & MalformedNumberFormatException::operator =(MalformedNumberFormatException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    CheckPos = the_other.CheckPos;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace util {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::MalformedNumberFormatException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.util.MalformedNumberFormatException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::util::MalformedNumberFormatException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::util::MalformedNumberFormatException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UTIL_MALFORMEDNUMBERFORMATEXCEPTION_HPP
