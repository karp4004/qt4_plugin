#ifndef INCLUDED_COM_SUN_STAR_UTIL_XSEARCHDESCRIPTOR_HDL
#define INCLUDED_COM_SUN_STAR_UTIL_XSEARCHDESCRIPTOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/XPropertySet.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace util {

class SAL_NO_VTABLE XSearchDescriptor : public ::css::beans::XPropertySet
{
public:

    // Methods
    virtual ::rtl::OUString SAL_CALL getSearchString() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setSearchString( const ::rtl::OUString& aString ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSearchDescriptor() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::XSearchDescriptor const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::util::XSearchDescriptor > *) SAL_THROW(());

#endif
