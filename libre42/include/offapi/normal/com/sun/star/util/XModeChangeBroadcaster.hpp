#ifndef INCLUDED_COM_SUN_STAR_UTIL_XMODECHANGEBROADCASTER_HPP
#define INCLUDED_COM_SUN_STAR_UTIL_XMODECHANGEBROADCASTER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/util/XModeChangeBroadcaster.hdl"

#include "com/sun/star/lang/NoSupportException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/util/XModeChangeApproveListener.hpp"
#include "com/sun/star/util/XModeChangeListener.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace util {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::XModeChangeBroadcaster const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.util.XModeChangeBroadcaster" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::util::XModeChangeBroadcaster > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::util::XModeChangeBroadcaster > >::get();
}

::css::uno::Type const & ::css::util::XModeChangeBroadcaster::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::util::XModeChangeBroadcaster > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_UTIL_XMODECHANGEBROADCASTER_HPP
