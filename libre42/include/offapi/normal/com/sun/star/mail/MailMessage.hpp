#ifndef INCLUDED_COM_SUN_STAR_MAIL_MAILMESSAGE_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_MAILMESSAGE_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/datatransfer/XTransferable.hpp"
#include "com/sun/star/mail/MailAttachment.hpp"
#include "com/sun/star/mail/XMailMessage.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace mail {

class MailMessage {
public:
    static ::css::uno::Reference< ::css::mail::XMailMessage > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& sTo, const ::rtl::OUString& sFrom, const ::rtl::OUString& sSubject, const ::css::uno::Reference< ::css::datatransfer::XTransferable >& xBody) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(4);
        the_arguments[0] <<= sTo;
        the_arguments[1] <<= sFrom;
        the_arguments[2] <<= sSubject;
        the_arguments[3] <<= xBody;
        ::css::uno::Reference< ::css::mail::XMailMessage > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::mail::XMailMessage >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.mail.MailMessage" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.mail.MailMessage of type com.sun.star.mail.XMailMessage: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.mail.MailMessage of type com.sun.star.mail.XMailMessage" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::mail::XMailMessage > createWithAttachment(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& sTo, const ::rtl::OUString& sFrom, const ::rtl::OUString& sSubject, const ::css::uno::Reference< ::css::datatransfer::XTransferable >& xBody, const ::css::mail::MailAttachment& aMailAttachment) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(5);
        the_arguments[0] <<= sTo;
        the_arguments[1] <<= sFrom;
        the_arguments[2] <<= sSubject;
        the_arguments[3] <<= xBody;
        the_arguments[4] <<= aMailAttachment;
        ::css::uno::Reference< ::css::mail::XMailMessage > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::mail::XMailMessage >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.mail.MailMessage" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.mail.MailMessage of type com.sun.star.mail.XMailMessage: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.mail.MailMessage of type com.sun.star.mail.XMailMessage" ), the_context);
        }
        return the_instance;
    }

private:
    MailMessage(); // not implemented
    MailMessage(MailMessage &); // not implemented
    ~MailMessage(); // not implemented
    void operator =(MailMessage); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_MAIL_MAILMESSAGE_HPP
