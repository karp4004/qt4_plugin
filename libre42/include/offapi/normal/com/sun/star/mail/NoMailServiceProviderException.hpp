#ifndef INCLUDED_COM_SUN_STAR_MAIL_NOMAILSERVICEPROVIDEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_NOMAILSERVICEPROVIDEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/mail/NoMailServiceProviderException.hdl"

#include "com/sun/star/mail/MailException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace mail {

inline NoMailServiceProviderException::NoMailServiceProviderException() SAL_THROW(())
    : ::css::mail::MailException()
{ }

inline NoMailServiceProviderException::NoMailServiceProviderException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::mail::MailException(Message_, Context_)
{ }

NoMailServiceProviderException::NoMailServiceProviderException(NoMailServiceProviderException const & the_other): ::css::mail::MailException(the_other) {}

NoMailServiceProviderException::~NoMailServiceProviderException() {}

NoMailServiceProviderException & NoMailServiceProviderException::operator =(NoMailServiceProviderException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::mail::MailException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::NoMailServiceProviderException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.mail.NoMailServiceProviderException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mail::NoMailServiceProviderException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mail::NoMailServiceProviderException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_NOMAILSERVICEPROVIDEREXCEPTION_HPP
