#ifndef INCLUDED_COM_SUN_STAR_MAIL_XSMTPSERVICE_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_XSMTPSERVICE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/mail/XSmtpService.hdl"

#include "com/sun/star/datatransfer/UnsupportedFlavorException.hpp"
#include "com/sun/star/io/NotConnectedException.hpp"
#include "com/sun/star/mail/MailException.hpp"
#include "com/sun/star/mail/SendMailMessageFailedException.hpp"
#include "com/sun/star/mail/XMailMessage.hpp"
#include "com/sun/star/mail/XMailService.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::XSmtpService const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.mail.XSmtpService" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::mail::XSmtpService > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::mail::XSmtpService > >::get();
}

::css::uno::Type const & ::css::mail::XSmtpService::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::mail::XSmtpService > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_XSMTPSERVICE_HPP
