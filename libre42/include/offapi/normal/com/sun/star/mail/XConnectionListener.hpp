#ifndef INCLUDED_COM_SUN_STAR_MAIL_XCONNECTIONLISTENER_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_XCONNECTIONLISTENER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/mail/XConnectionListener.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/lang/XEventListener.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::XConnectionListener const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.mail.XConnectionListener" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::mail::XConnectionListener > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::mail::XConnectionListener > >::get();
}

::css::uno::Type const & ::css::mail::XConnectionListener::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::mail::XConnectionListener > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_XCONNECTIONLISTENER_HPP
