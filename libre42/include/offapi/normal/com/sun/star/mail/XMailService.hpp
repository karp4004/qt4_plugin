#ifndef INCLUDED_COM_SUN_STAR_MAIL_XMAILSERVICE_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_XMAILSERVICE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/mail/XMailService.hdl"

#include "com/sun/star/auth/AuthenticationFailedException.hpp"
#include "com/sun/star/io/AlreadyConnectedException.hpp"
#include "com/sun/star/io/ConnectException.hpp"
#include "com/sun/star/io/NoRouteToHostException.hpp"
#include "com/sun/star/io/NotConnectedException.hpp"
#include "com/sun/star/io/UnknownHostException.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/mail/MailException.hpp"
#include "com/sun/star/mail/XAuthenticator.hpp"
#include "com/sun/star/mail/XConnectionListener.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XCurrentContext.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::XMailService const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.mail.XMailService" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::mail::XMailService > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::mail::XMailService > >::get();
}

::css::uno::Type const & ::css::mail::XMailService::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::mail::XMailService > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_XMAILSERVICE_HPP
