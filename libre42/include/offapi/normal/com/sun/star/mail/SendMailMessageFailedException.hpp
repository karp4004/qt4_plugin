#ifndef INCLUDED_COM_SUN_STAR_MAIL_SENDMAILMESSAGEFAILEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_SENDMAILMESSAGEFAILEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/mail/SendMailMessageFailedException.hdl"

#include "com/sun/star/mail/MailException.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace mail {

inline SendMailMessageFailedException::SendMailMessageFailedException() SAL_THROW(())
    : ::css::mail::MailException()
    , InvalidAddresses()
    , ValidSentAddresses()
    , ValidUnsentAddresses()
{ }

inline SendMailMessageFailedException::SendMailMessageFailedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Sequence< ::rtl::OUString >& InvalidAddresses_, const ::css::uno::Sequence< ::rtl::OUString >& ValidSentAddresses_, const ::css::uno::Sequence< ::rtl::OUString >& ValidUnsentAddresses_) SAL_THROW(())
    : ::css::mail::MailException(Message_, Context_)
    , InvalidAddresses(InvalidAddresses_)
    , ValidSentAddresses(ValidSentAddresses_)
    , ValidUnsentAddresses(ValidUnsentAddresses_)
{ }

SendMailMessageFailedException::SendMailMessageFailedException(SendMailMessageFailedException const & the_other): ::css::mail::MailException(the_other), InvalidAddresses(the_other.InvalidAddresses), ValidSentAddresses(the_other.ValidSentAddresses), ValidUnsentAddresses(the_other.ValidUnsentAddresses) {}

SendMailMessageFailedException::~SendMailMessageFailedException() {}

SendMailMessageFailedException & SendMailMessageFailedException::operator =(SendMailMessageFailedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::mail::MailException::operator =(the_other);
    InvalidAddresses = the_other.InvalidAddresses;
    ValidSentAddresses = the_other.ValidSentAddresses;
    ValidUnsentAddresses = the_other.ValidUnsentAddresses;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::SendMailMessageFailedException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.mail.SendMailMessageFailedException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mail::SendMailMessageFailedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mail::SendMailMessageFailedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_SENDMAILMESSAGEFAILEDEXCEPTION_HPP
