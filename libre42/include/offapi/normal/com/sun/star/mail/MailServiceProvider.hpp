#ifndef INCLUDED_COM_SUN_STAR_MAIL_MAILSERVICEPROVIDER_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_MAILSERVICEPROVIDER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/mail/XMailServiceProvider.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace mail {

class MailServiceProvider {
public:
    static ::css::uno::Reference< ::css::mail::XMailServiceProvider > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::mail::XMailServiceProvider > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::mail::XMailServiceProvider >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.mail.MailServiceProvider" ), ::css::uno::Sequence< ::css::uno::Any >(), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.mail.MailServiceProvider of type com.sun.star.mail.XMailServiceProvider: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.mail.MailServiceProvider of type com.sun.star.mail.XMailServiceProvider" ), the_context);
        }
        return the_instance;
    }

private:
    MailServiceProvider(); // not implemented
    MailServiceProvider(MailServiceProvider &); // not implemented
    ~MailServiceProvider(); // not implemented
    void operator =(MailServiceProvider); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_MAIL_MAILSERVICEPROVIDER_HPP
