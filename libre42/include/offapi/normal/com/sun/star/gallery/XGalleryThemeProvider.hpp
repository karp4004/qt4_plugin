#ifndef INCLUDED_COM_SUN_STAR_GALLERY_XGALLERYTHEMEPROVIDER_HPP
#define INCLUDED_COM_SUN_STAR_GALLERY_XGALLERYTHEMEPROVIDER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/gallery/XGalleryThemeProvider.hdl"

#include "com/sun/star/container/ElementExistException.hpp"
#include "com/sun/star/container/NoSuchElementException.hpp"
#include "com/sun/star/container/XNameAccess.hpp"
#include "com/sun/star/gallery/XGalleryTheme.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace gallery {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::gallery::XGalleryThemeProvider const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.gallery.XGalleryThemeProvider" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::gallery::XGalleryThemeProvider > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::gallery::XGalleryThemeProvider > >::get();
}

::css::uno::Type const & ::css::gallery::XGalleryThemeProvider::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::gallery::XGalleryThemeProvider > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_GALLERY_XGALLERYTHEMEPROVIDER_HPP
