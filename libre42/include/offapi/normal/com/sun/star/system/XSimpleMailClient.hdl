#ifndef INCLUDED_COM_SUN_STAR_SYSTEM_XSIMPLEMAILCLIENT_HDL
#define INCLUDED_COM_SUN_STAR_SYSTEM_XSIMPLEMAILCLIENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
namespace com { namespace sun { namespace star { namespace system { class XSimpleMailMessage; } } } }
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace system {

class SAL_NO_VTABLE XSimpleMailClient : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::system::XSimpleMailMessage > SAL_CALL createSimpleMailMessage() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL sendSimpleMailMessage( const ::css::uno::Reference< ::css::system::XSimpleMailMessage >& xSimpleMailMessage, ::sal_Int32 aFlag ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSimpleMailClient() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::system::XSimpleMailClient const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::system::XSimpleMailClient > *) SAL_THROW(());

#endif
