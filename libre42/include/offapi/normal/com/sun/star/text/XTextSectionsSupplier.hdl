#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTSECTIONSSUPPLIER_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTSECTIONSSUPPLIER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace container { class XNameAccess; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XTextSectionsSupplier : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::container::XNameAccess > SAL_CALL getTextSections() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTextSectionsSupplier() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextSectionsSupplier const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XTextSectionsSupplier > *) SAL_THROW(());

#endif
