#ifndef INCLUDED_COM_SUN_STAR_TEXT_TEXTMARKUPDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_TEXTMARKUPDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/text/TextMarkupDescriptor.hdl"

#include "com/sun/star/container/XStringKeyMap.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline TextMarkupDescriptor::TextMarkupDescriptor() SAL_THROW(())
    : nType(0)
    , aIdentifier()
    , nOffset(0)
    , nLength(0)
    , xMarkupInfoContainer()
{
}

inline TextMarkupDescriptor::TextMarkupDescriptor(const ::sal_Int32& nType_, const ::rtl::OUString& aIdentifier_, const ::sal_Int32& nOffset_, const ::sal_Int32& nLength_, const ::css::uno::Reference< ::css::container::XStringKeyMap >& xMarkupInfoContainer_) SAL_THROW(())
    : nType(nType_)
    , aIdentifier(aIdentifier_)
    , nOffset(nOffset_)
    , nLength(nLength_)
    , xMarkupInfoContainer(xMarkupInfoContainer_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::TextMarkupDescriptor const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.text.TextMarkupDescriptor");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::TextMarkupDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::TextMarkupDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_TEXTMARKUPDESCRIPTOR_HPP
