#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTCONTENT_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTCONTENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/XComponent.hdl"
namespace com { namespace sun { namespace star { namespace text { class XTextRange; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XTextContent : public ::css::lang::XComponent
{
public:

    // Methods
    virtual void SAL_CALL attach( const ::css::uno::Reference< ::css::text::XTextRange >& xTextRange ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::text::XTextRange > SAL_CALL getAnchor() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTextContent() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextContent const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XTextContent > *) SAL_THROW(());

#endif
