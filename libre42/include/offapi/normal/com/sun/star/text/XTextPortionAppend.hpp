#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTPORTIONAPPEND_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTPORTIONAPPEND_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/text/XTextPortionAppend.hdl"

#include "com/sun/star/beans/PropertyValues.hpp"
#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/text/XTextRange.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextPortionAppend const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.text.XTextPortionAppend" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::text::XTextPortionAppend > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::text::XTextPortionAppend > >::get();
}

::css::uno::Type const & ::css::text::XTextPortionAppend::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::text::XTextPortionAppend > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_XTEXTPORTIONAPPEND_HPP
