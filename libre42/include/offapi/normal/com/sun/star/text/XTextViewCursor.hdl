#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTVIEWCURSOR_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTVIEWCURSOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/Point.hdl"
#include "com/sun/star/text/XTextCursor.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XTextViewCursor : public ::css::text::XTextCursor
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL isVisible() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setVisible( ::sal_Bool bVisible ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::awt::Point SAL_CALL getPosition() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTextViewCursor() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextViewCursor const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XTextViewCursor > *) SAL_THROW(());

#endif
