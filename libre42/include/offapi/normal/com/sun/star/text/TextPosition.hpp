#ifndef INCLUDED_COM_SUN_STAR_TEXT_TEXTPOSITION_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_TEXTPOSITION_HPP

#include "sal/config.h"

#include "com/sun/star/text/TextPosition.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline TextPosition::TextPosition() SAL_THROW(())
    : Paragraph(0)
    , PositionInParagraph(0)
{
}

inline TextPosition::TextPosition(const ::sal_Int32& Paragraph_, const ::sal_Int32& PositionInParagraph_) SAL_THROW(())
    : Paragraph(Paragraph_)
    , PositionInParagraph(PositionInParagraph_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::TextPosition const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.text.TextPosition");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::TextPosition const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::TextPosition >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_TEXTPOSITION_HPP
