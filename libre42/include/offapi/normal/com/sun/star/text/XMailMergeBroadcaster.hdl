#ifndef INCLUDED_COM_SUN_STAR_TEXT_XMAILMERGEBROADCASTER_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XMAILMERGEBROADCASTER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace text { class XMailMergeListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XMailMergeBroadcaster : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addMailMergeEventListener( const ::css::uno::Reference< ::css::text::XMailMergeListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeMailMergeEventListener( const ::css::uno::Reference< ::css::text::XMailMergeListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XMailMergeBroadcaster() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XMailMergeBroadcaster const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XMailMergeBroadcaster > *) SAL_THROW(());

#endif
