#ifndef INCLUDED_COM_SUN_STAR_TEXT_XREDLINE_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XREDLINE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValues.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XRedline : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL makeRedline( const ::rtl::OUString& RedlineType, const ::css::uno::Sequence< ::css::beans::PropertyValue >& RedlineProperties ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XRedline() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XRedline const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XRedline > *) SAL_THROW(());

#endif
