#ifndef INCLUDED_COM_SUN_STAR_TEXT_INVALIDTEXTCONTENTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_INVALIDTEXTCONTENTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/text/InvalidTextContentException.hdl"

#include "com/sun/star/text/XTextContent.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace text {

inline InvalidTextContentException::InvalidTextContentException() SAL_THROW(())
    : ::css::uno::Exception()
    , TextContent()
{ }

inline InvalidTextContentException::InvalidTextContentException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Reference< ::css::text::XTextContent >& TextContent_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , TextContent(TextContent_)
{ }

InvalidTextContentException::InvalidTextContentException(InvalidTextContentException const & the_other): ::css::uno::Exception(the_other), TextContent(the_other.TextContent) {}

InvalidTextContentException::~InvalidTextContentException() {}

InvalidTextContentException & InvalidTextContentException::operator =(InvalidTextContentException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    TextContent = the_other.TextContent;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::InvalidTextContentException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.text.InvalidTextContentException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::InvalidTextContentException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::InvalidTextContentException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_INVALIDTEXTCONTENTEXCEPTION_HPP
