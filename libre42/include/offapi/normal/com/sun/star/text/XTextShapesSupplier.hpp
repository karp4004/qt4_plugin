#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTSHAPESSUPPLIER_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTSHAPESSUPPLIER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/text/XTextShapesSupplier.hdl"

#include "com/sun/star/container/XIndexAccess.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextShapesSupplier const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.text.XTextShapesSupplier" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::text::XTextShapesSupplier > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::text::XTextShapesSupplier > >::get();
}

::css::uno::Type const & ::css::text::XTextShapesSupplier::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::text::XTextShapesSupplier > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_XTEXTSHAPESSUPPLIER_HPP
