#ifndef INCLUDED_COM_SUN_STAR_TEXT_XREDLINE_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_XREDLINE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/text/XRedline.hdl"

#include "com/sun/star/beans/PropertyValues.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XRedline const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.text.XRedline" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::text::XRedline > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::text::XRedline > >::get();
}

::css::uno::Type const & ::css::text::XRedline::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::text::XRedline > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_XREDLINE_HPP
