#ifndef INCLUDED_COM_SUN_STAR_TEXT_XRUBYSELECTION_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XRUBYSELECTION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValues.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XRubySelection : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::uno::Sequence< ::css::beans::PropertyValue > > SAL_CALL getRubyList( ::sal_Bool Automatic ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRubyList( const ::css::uno::Sequence< ::css::uno::Sequence< ::css::beans::PropertyValue > >& RubyList, ::sal_Bool Automatic ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XRubySelection() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XRubySelection const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XRubySelection > *) SAL_THROW(());

#endif
