#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTSECTION_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTSECTION_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/text/XTextSection.hdl"

#include "com/sun/star/text/XTextContent.hpp"
#include "com/sun/star/text/XTextSection.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextSection const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.text.XTextSection" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::text::XTextSection > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::text::XTextSection > >::get();
}

::css::uno::Type const & ::css::text::XTextSection::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::text::XTextSection > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_XTEXTSECTION_HPP
