#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_XPROOFREADINGITERATOR_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_XPROOFREADINGITERATOR_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/linguistic2/XProofreadingIterator.hdl"

#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/linguistic2/ProofreadingResult.hpp"
#include "com/sun/star/text/XFlatParagraph.hpp"
#include "com/sun/star/text/XFlatParagraphIteratorProvider.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::XProofreadingIterator const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.linguistic2.XProofreadingIterator" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::linguistic2::XProofreadingIterator > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::linguistic2::XProofreadingIterator > >::get();
}

::css::uno::Type const & ::css::linguistic2::XProofreadingIterator::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::linguistic2::XProofreadingIterator > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_XPROOFREADINGITERATOR_HPP
