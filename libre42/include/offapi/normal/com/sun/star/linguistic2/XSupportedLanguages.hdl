#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_XSUPPORTEDLANGUAGES_HDL
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_XSUPPORTEDLANGUAGES_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

class SAL_NO_VTABLE XSupportedLanguages : public ::css::uno::XInterface
{
public:

    // Methods
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::css::uno::Sequence< ::sal_Int16 > SAL_CALL getLanguages() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL hasLanguage( ::sal_Int16 nLanguage ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSupportedLanguages() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::XSupportedLanguages const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::linguistic2::XSupportedLanguages > *) SAL_THROW(());

#endif
