#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_XSPELLALTERNATIVES_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_XSPELLALTERNATIVES_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/linguistic2/XSpellAlternatives.hdl"

#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::XSpellAlternatives const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.linguistic2.XSpellAlternatives" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::linguistic2::XSpellAlternatives > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::linguistic2::XSpellAlternatives > >::get();
}

::css::uno::Type const & ::css::linguistic2::XSpellAlternatives::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::linguistic2::XSpellAlternatives > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_XSPELLALTERNATIVES_HPP
