#ifndef INCLUDED_COM_SUN_STAR_UCB_RULE_HPP
#define INCLUDED_COM_SUN_STAR_UCB_RULE_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/Rule.hdl"

#include "com/sun/star/ucb/RuleTerm.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline Rule::Rule() SAL_THROW(())
    : Terms()
    , Parameter()
    , Action(0)
{
}

inline Rule::Rule(const ::css::uno::Sequence< ::css::ucb::RuleTerm >& Terms_, const ::rtl::OUString& Parameter_, const ::sal_Int16& Action_) SAL_THROW(())
    : Terms(Terms_)
    , Parameter(Parameter_)
    , Action(Action_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::Rule const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.Rule");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::Rule const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::Rule >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_RULE_HPP
