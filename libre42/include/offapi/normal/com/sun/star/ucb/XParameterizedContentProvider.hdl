#ifndef INCLUDED_COM_SUN_STAR_UCB_XPARAMETERIZEDCONTENTPROVIDER_HDL
#define INCLUDED_COM_SUN_STAR_UCB_XPARAMETERIZEDCONTENTPROVIDER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
namespace com { namespace sun { namespace star { namespace ucb { class XContentProvider; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ucb {

class SAL_NO_VTABLE XParameterizedContentProvider : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::ucb::XContentProvider > SAL_CALL registerInstance( const ::rtl::OUString& Template, const ::rtl::OUString& Arguments, ::sal_Bool ReplaceExisting ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::ucb::XContentProvider > SAL_CALL deregisterInstance( const ::rtl::OUString& Template, const ::rtl::OUString& Arguments ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XParameterizedContentProvider() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::XParameterizedContentProvider const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ucb::XParameterizedContentProvider > *) SAL_THROW(());

#endif
