#ifndef INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGLOCKEXPIREDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGLOCKEXPIREDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/InteractiveLockingLockExpiredException.hdl"

#include "com/sun/star/ucb/InteractiveLockingException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline InteractiveLockingLockExpiredException::InteractiveLockingLockExpiredException() SAL_THROW(())
    : ::css::ucb::InteractiveLockingException()
{ }

inline InteractiveLockingLockExpiredException::InteractiveLockingLockExpiredException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& Url_) SAL_THROW(())
    : ::css::ucb::InteractiveLockingException(Message_, Context_, Classification_, Url_)
{ }

InteractiveLockingLockExpiredException::InteractiveLockingLockExpiredException(InteractiveLockingLockExpiredException const & the_other): ::css::ucb::InteractiveLockingException(the_other) {}

InteractiveLockingLockExpiredException::~InteractiveLockingLockExpiredException() {}

InteractiveLockingLockExpiredException & InteractiveLockingLockExpiredException::operator =(InteractiveLockingLockExpiredException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::ucb::InteractiveLockingException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveLockingLockExpiredException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ucb.InteractiveLockingLockExpiredException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveLockingLockExpiredException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::InteractiveLockingLockExpiredException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGLOCKEXPIREDEXCEPTION_HPP
