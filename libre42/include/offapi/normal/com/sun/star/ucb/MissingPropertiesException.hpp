#ifndef INCLUDED_COM_SUN_STAR_UCB_MISSINGPROPERTIESEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_MISSINGPROPERTIESEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/MissingPropertiesException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline MissingPropertiesException::MissingPropertiesException() SAL_THROW(())
    : ::css::uno::Exception()
    , Properties()
{ }

inline MissingPropertiesException::MissingPropertiesException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Sequence< ::rtl::OUString >& Properties_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Properties(Properties_)
{ }

MissingPropertiesException::MissingPropertiesException(MissingPropertiesException const & the_other): ::css::uno::Exception(the_other), Properties(the_other.Properties) {}

MissingPropertiesException::~MissingPropertiesException() {}

MissingPropertiesException & MissingPropertiesException::operator =(MissingPropertiesException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Properties = the_other.Properties;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::MissingPropertiesException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ucb.MissingPropertiesException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::MissingPropertiesException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::MissingPropertiesException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_MISSINGPROPERTIESEXCEPTION_HPP
