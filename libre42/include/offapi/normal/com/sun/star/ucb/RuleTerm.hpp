#ifndef INCLUDED_COM_SUN_STAR_UCB_RULETERM_HPP
#define INCLUDED_COM_SUN_STAR_UCB_RULETERM_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/RuleTerm.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline RuleTerm::RuleTerm() SAL_THROW(())
    : Property()
    , Operand()
    , Operator(0)
    , CaseSensitive(false)
    , RegularExpression(false)
{
}

inline RuleTerm::RuleTerm(const ::rtl::OUString& Property_, const ::css::uno::Any& Operand_, const ::sal_Int16& Operator_, const ::sal_Bool& CaseSensitive_, const ::sal_Bool& RegularExpression_) SAL_THROW(())
    : Property(Property_)
    , Operand(Operand_)
    , Operator(Operator_)
    , CaseSensitive(CaseSensitive_)
    , RegularExpression(RegularExpression_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::RuleTerm const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.RuleTerm");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::RuleTerm const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::RuleTerm >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_RULETERM_HPP
