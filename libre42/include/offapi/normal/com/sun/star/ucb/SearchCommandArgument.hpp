#ifndef INCLUDED_COM_SUN_STAR_UCB_SEARCHCOMMANDARGUMENT_HPP
#define INCLUDED_COM_SUN_STAR_UCB_SEARCHCOMMANDARGUMENT_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/SearchCommandArgument.hdl"

#include "com/sun/star/beans/Property.hpp"
#include "com/sun/star/ucb/SearchInfo.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline SearchCommandArgument::SearchCommandArgument() SAL_THROW(())
    : Info()
    , Properties()
{
}

inline SearchCommandArgument::SearchCommandArgument(const ::css::ucb::SearchInfo& Info_, const ::css::uno::Sequence< ::css::beans::Property >& Properties_) SAL_THROW(())
    : Info(Info_)
    , Properties(Properties_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::SearchCommandArgument const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.SearchCommandArgument");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::SearchCommandArgument const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::SearchCommandArgument >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_SEARCHCOMMANDARGUMENT_HPP
