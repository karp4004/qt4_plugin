#ifndef INCLUDED_COM_SUN_STAR_UCB_SORTINGINFO_HPP
#define INCLUDED_COM_SUN_STAR_UCB_SORTINGINFO_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/SortingInfo.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline SortingInfo::SortingInfo() SAL_THROW(())
    : PropertyName()
    , Ascending(false)
{
}

inline SortingInfo::SortingInfo(const ::rtl::OUString& PropertyName_, const ::sal_Bool& Ascending_) SAL_THROW(())
    : PropertyName(PropertyName_)
    , Ascending(Ascending_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::SortingInfo const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.SortingInfo");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::SortingInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::SortingInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_SORTINGINFO_HPP
