#ifndef INCLUDED_COM_SUN_STAR_UCB_LINK_HPP
#define INCLUDED_COM_SUN_STAR_UCB_LINK_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/Link.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline Link::Link() SAL_THROW(())
    : Source()
    , Destination()
{
}

inline Link::Link(const ::rtl::OUString& Source_, const ::rtl::OUString& Destination_) SAL_THROW(())
    : Source(Source_)
    , Destination(Destination_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::Link const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.Link");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::Link const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::Link >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_LINK_HPP
