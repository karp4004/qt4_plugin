#ifndef INCLUDED_COM_SUN_STAR_UCB_LOCK_HPP
#define INCLUDED_COM_SUN_STAR_UCB_LOCK_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/Lock.hdl"

#include "com/sun/star/ucb/LockDepth.hpp"
#include "com/sun/star/ucb/LockEntry.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline Lock::Lock() SAL_THROW(())
    : ::css::ucb::LockEntry()
    , Depth(::css::ucb::LockDepth_ZERO)
    , Owner()
    , Timeout(0)
    , LockTokens()
{
}

inline Lock::Lock(const ::css::ucb::LockScope& Scope_, const ::css::ucb::LockType& Type_, const ::css::ucb::LockDepth& Depth_, const ::css::uno::Any& Owner_, const ::sal_Int64& Timeout_, const ::css::uno::Sequence< ::rtl::OUString >& LockTokens_) SAL_THROW(())
    : ::css::ucb::LockEntry(Scope_, Type_)
    , Depth(Depth_)
    , Owner(Owner_)
    , Timeout(Timeout_)
    , LockTokens(LockTokens_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::Lock const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.Lock");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::Lock const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::Lock >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_LOCK_HPP
