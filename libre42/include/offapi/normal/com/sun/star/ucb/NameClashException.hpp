#ifndef INCLUDED_COM_SUN_STAR_UCB_NAMECLASHEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_NAMECLASHEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/NameClashException.hdl"

#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline NameClashException::NameClashException() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , Name()
{ }

inline NameClashException::NameClashException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& Name_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , Name(Name_)
{ }

NameClashException::NameClashException(NameClashException const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), Name(the_other.Name) {}

NameClashException::~NameClashException() {}

NameClashException & NameClashException::operator =(NameClashException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    Name = the_other.Name;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::NameClashException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ucb.NameClashException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::NameClashException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::NameClashException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_NAMECLASHEXCEPTION_HPP
