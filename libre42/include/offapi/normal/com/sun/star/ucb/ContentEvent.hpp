#ifndef INCLUDED_COM_SUN_STAR_UCB_CONTENTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_UCB_CONTENTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/ContentEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/ucb/XContent.hpp"
#include "com/sun/star/ucb/XContentIdentifier.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline ContentEvent::ContentEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Action(0)
    , Content()
    , Id()
{
}

inline ContentEvent::ContentEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& Action_, const ::css::uno::Reference< ::css::ucb::XContent >& Content_, const ::css::uno::Reference< ::css::ucb::XContentIdentifier >& Id_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Action(Action_)
    , Content(Content_)
    , Id(Id_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::ContentEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.ContentEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::ContentEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::ContentEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_CONTENTEVENT_HPP
