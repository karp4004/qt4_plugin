#ifndef INCLUDED_COM_SUN_STAR_UCB_NUMBEREDSORTINGINFO_HPP
#define INCLUDED_COM_SUN_STAR_UCB_NUMBEREDSORTINGINFO_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/NumberedSortingInfo.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline NumberedSortingInfo::NumberedSortingInfo() SAL_THROW(())
    : ColumnIndex(0)
    , Ascending(false)
{
}

inline NumberedSortingInfo::NumberedSortingInfo(const ::sal_Int32& ColumnIndex_, const ::sal_Bool& Ascending_) SAL_THROW(())
    : ColumnIndex(ColumnIndex_)
    , Ascending(Ascending_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::NumberedSortingInfo const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.NumberedSortingInfo");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::NumberedSortingInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::NumberedSortingInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_NUMBEREDSORTINGINFO_HPP
