#ifndef INCLUDED_COM_SUN_STAR_UCB_CONTENTINFO_HPP
#define INCLUDED_COM_SUN_STAR_UCB_CONTENTINFO_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/ContentInfo.hdl"

#include "com/sun/star/beans/Property.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline ContentInfo::ContentInfo() SAL_THROW(())
    : Type()
    , Attributes(0)
    , Properties()
{
}

inline ContentInfo::ContentInfo(const ::rtl::OUString& Type_, const ::sal_Int32& Attributes_, const ::css::uno::Sequence< ::css::beans::Property >& Properties_) SAL_THROW(())
    : Type(Type_)
    , Attributes(Attributes_)
    , Properties(Properties_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::ContentInfo const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.ContentInfo");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::ContentInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::ContentInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_CONTENTINFO_HPP
