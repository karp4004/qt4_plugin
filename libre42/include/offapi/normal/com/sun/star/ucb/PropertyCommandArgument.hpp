#ifndef INCLUDED_COM_SUN_STAR_UCB_PROPERTYCOMMANDARGUMENT_HPP
#define INCLUDED_COM_SUN_STAR_UCB_PROPERTYCOMMANDARGUMENT_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/PropertyCommandArgument.hdl"

#include "com/sun/star/beans/Property.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline PropertyCommandArgument::PropertyCommandArgument() SAL_THROW(())
    : Property()
    , DefaultValue()
{
}

inline PropertyCommandArgument::PropertyCommandArgument(const ::css::beans::Property& Property_, const ::css::uno::Any& DefaultValue_) SAL_THROW(())
    : Property(Property_)
    , DefaultValue(DefaultValue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::PropertyCommandArgument const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.PropertyCommandArgument");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::PropertyCommandArgument const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::PropertyCommandArgument >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_PROPERTYCOMMANDARGUMENT_HPP
