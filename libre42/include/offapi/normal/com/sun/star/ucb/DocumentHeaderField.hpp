#ifndef INCLUDED_COM_SUN_STAR_UCB_DOCUMENTHEADERFIELD_HPP
#define INCLUDED_COM_SUN_STAR_UCB_DOCUMENTHEADERFIELD_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/DocumentHeaderField.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline DocumentHeaderField::DocumentHeaderField() SAL_THROW(())
    : Name()
    , Value()
{
}

inline DocumentHeaderField::DocumentHeaderField(const ::rtl::OUString& Name_, const ::rtl::OUString& Value_) SAL_THROW(())
    : Name(Name_)
    , Value(Value_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::DocumentHeaderField const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.DocumentHeaderField");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::DocumentHeaderField const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::DocumentHeaderField >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_DOCUMENTHEADERFIELD_HPP
