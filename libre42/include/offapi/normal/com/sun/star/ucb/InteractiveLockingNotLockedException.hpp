#ifndef INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGNOTLOCKEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGNOTLOCKEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/InteractiveLockingNotLockedException.hdl"

#include "com/sun/star/ucb/InteractiveLockingException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline InteractiveLockingNotLockedException::InteractiveLockingNotLockedException() SAL_THROW(())
    : ::css::ucb::InteractiveLockingException()
{ }

inline InteractiveLockingNotLockedException::InteractiveLockingNotLockedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& Url_) SAL_THROW(())
    : ::css::ucb::InteractiveLockingException(Message_, Context_, Classification_, Url_)
{ }

InteractiveLockingNotLockedException::InteractiveLockingNotLockedException(InteractiveLockingNotLockedException const & the_other): ::css::ucb::InteractiveLockingException(the_other) {}

InteractiveLockingNotLockedException::~InteractiveLockingNotLockedException() {}

InteractiveLockingNotLockedException & InteractiveLockingNotLockedException::operator =(InteractiveLockingNotLockedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::ucb::InteractiveLockingException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveLockingNotLockedException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ucb.InteractiveLockingNotLockedException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveLockingNotLockedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::InteractiveLockingNotLockedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGNOTLOCKEDEXCEPTION_HPP
