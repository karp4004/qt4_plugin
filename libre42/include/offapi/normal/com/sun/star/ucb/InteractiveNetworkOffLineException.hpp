#ifndef INCLUDED_COM_SUN_STAR_UCB_INTERACTIVENETWORKOFFLINEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_INTERACTIVENETWORKOFFLINEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/InteractiveNetworkOffLineException.hdl"

#include "com/sun/star/ucb/InteractiveNetworkException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline InteractiveNetworkOffLineException::InteractiveNetworkOffLineException() SAL_THROW(())
    : ::css::ucb::InteractiveNetworkException()
{ }

inline InteractiveNetworkOffLineException::InteractiveNetworkOffLineException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_) SAL_THROW(())
    : ::css::ucb::InteractiveNetworkException(Message_, Context_, Classification_)
{ }

InteractiveNetworkOffLineException::InteractiveNetworkOffLineException(InteractiveNetworkOffLineException const & the_other): ::css::ucb::InteractiveNetworkException(the_other) {}

InteractiveNetworkOffLineException::~InteractiveNetworkOffLineException() {}

InteractiveNetworkOffLineException & InteractiveNetworkOffLineException::operator =(InteractiveNetworkOffLineException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::ucb::InteractiveNetworkException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveNetworkOffLineException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ucb.InteractiveNetworkOffLineException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveNetworkOffLineException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::InteractiveNetworkOffLineException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_INTERACTIVENETWORKOFFLINEEXCEPTION_HPP
