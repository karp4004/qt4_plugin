#ifndef INCLUDED_COM_SUN_STAR_UCB_XFETCHPROVIDER_HDL
#define INCLUDED_COM_SUN_STAR_UCB_XFETCHPROVIDER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/ucb/FetchResult.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ucb {

class SAL_NO_VTABLE XFetchProvider : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::ucb::FetchResult SAL_CALL fetch( ::sal_Int32 nRowStartPosition, ::sal_Int32 nRowCount, ::sal_Bool bDirection ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFetchProvider() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::XFetchProvider const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ucb::XFetchProvider > *) SAL_THROW(());

#endif
