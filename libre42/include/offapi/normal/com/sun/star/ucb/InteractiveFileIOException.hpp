#ifndef INCLUDED_COM_SUN_STAR_UCB_INTERACTIVEFILEIOEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_INTERACTIVEFILEIOEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/InteractiveFileIOException.hdl"

#include "com/sun/star/ucb/InteractiveIOException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline InteractiveFileIOException::InteractiveFileIOException() SAL_THROW(())
    : ::css::ucb::InteractiveIOException()
    , FileName()
{ }

inline InteractiveFileIOException::InteractiveFileIOException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::ucb::IOErrorCode& Code_, const ::rtl::OUString& FileName_) SAL_THROW(())
    : ::css::ucb::InteractiveIOException(Message_, Context_, Classification_, Code_)
    , FileName(FileName_)
{ }

InteractiveFileIOException::InteractiveFileIOException(InteractiveFileIOException const & the_other): ::css::ucb::InteractiveIOException(the_other), FileName(the_other.FileName) {}

InteractiveFileIOException::~InteractiveFileIOException() {}

InteractiveFileIOException & InteractiveFileIOException::operator =(InteractiveFileIOException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::ucb::InteractiveIOException::operator =(the_other);
    FileName = the_other.FileName;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveFileIOException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ucb.InteractiveFileIOException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveFileIOException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::InteractiveFileIOException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_INTERACTIVEFILEIOEXCEPTION_HPP
