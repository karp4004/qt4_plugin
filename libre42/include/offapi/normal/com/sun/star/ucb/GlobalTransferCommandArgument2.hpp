#ifndef INCLUDED_COM_SUN_STAR_UCB_GLOBALTRANSFERCOMMANDARGUMENT2_HPP
#define INCLUDED_COM_SUN_STAR_UCB_GLOBALTRANSFERCOMMANDARGUMENT2_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/GlobalTransferCommandArgument2.hdl"

#include "com/sun/star/ucb/GlobalTransferCommandArgument.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline GlobalTransferCommandArgument2::GlobalTransferCommandArgument2() SAL_THROW(())
    : ::css::ucb::GlobalTransferCommandArgument()
    , MimeType()
    , DocumentId()
{
}

inline GlobalTransferCommandArgument2::GlobalTransferCommandArgument2(const ::css::ucb::TransferCommandOperation& Operation_, const ::rtl::OUString& SourceURL_, const ::rtl::OUString& TargetURL_, const ::rtl::OUString& NewTitle_, const ::sal_Int32& NameClash_, const ::rtl::OUString& MimeType_, const ::rtl::OUString& DocumentId_) SAL_THROW(())
    : ::css::ucb::GlobalTransferCommandArgument(Operation_, SourceURL_, TargetURL_, NewTitle_, NameClash_)
    , MimeType(MimeType_)
    , DocumentId(DocumentId_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::GlobalTransferCommandArgument2 const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.GlobalTransferCommandArgument2");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::GlobalTransferCommandArgument2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::GlobalTransferCommandArgument2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_GLOBALTRANSFERCOMMANDARGUMENT2_HPP
