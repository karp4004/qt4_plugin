#ifndef INCLUDED_COM_SUN_STAR_UCB_XDYNAMICRESULTSET_HPP
#define INCLUDED_COM_SUN_STAR_UCB_XDYNAMICRESULTSET_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/ucb/XDynamicResultSet.hdl"

#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/sdbc/XResultSet.hpp"
#include "com/sun/star/ucb/AlreadyInitializedException.hpp"
#include "com/sun/star/ucb/ListenerAlreadySetException.hpp"
#include "com/sun/star/ucb/ServiceNotFoundException.hpp"
#include "com/sun/star/ucb/XDynamicResultSet.hpp"
#include "com/sun/star/ucb/XDynamicResultSetListener.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::XDynamicResultSet const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.ucb.XDynamicResultSet" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::ucb::XDynamicResultSet > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::ucb::XDynamicResultSet > >::get();
}

::css::uno::Type const & ::css::ucb::XDynamicResultSet::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::ucb::XDynamicResultSet > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_UCB_XDYNAMICRESULTSET_HPP
