#ifndef INCLUDED_COM_SUN_STAR_UCB_CONTENTPROVIDERINFO_HPP
#define INCLUDED_COM_SUN_STAR_UCB_CONTENTPROVIDERINFO_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/ContentProviderInfo.hdl"

#include "com/sun/star/ucb/XContentProvider.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline ContentProviderInfo::ContentProviderInfo() SAL_THROW(())
    : ContentProvider()
    , Scheme()
{
}

inline ContentProviderInfo::ContentProviderInfo(const ::css::uno::Reference< ::css::ucb::XContentProvider >& ContentProvider_, const ::rtl::OUString& Scheme_) SAL_THROW(())
    : ContentProvider(ContentProvider_)
    , Scheme(Scheme_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::ContentProviderInfo const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.ContentProviderInfo");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::ContentProviderInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::ContentProviderInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_CONTENTPROVIDERINFO_HPP
