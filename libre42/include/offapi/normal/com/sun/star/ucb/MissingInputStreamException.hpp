#ifndef INCLUDED_COM_SUN_STAR_UCB_MISSINGINPUTSTREAMEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_MISSINGINPUTSTREAMEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/MissingInputStreamException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline MissingInputStreamException::MissingInputStreamException() SAL_THROW(())
    : ::css::uno::Exception()
{ }

inline MissingInputStreamException::MissingInputStreamException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{ }

MissingInputStreamException::MissingInputStreamException(MissingInputStreamException const & the_other): ::css::uno::Exception(the_other) {}

MissingInputStreamException::~MissingInputStreamException() {}

MissingInputStreamException & MissingInputStreamException::operator =(MissingInputStreamException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::MissingInputStreamException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ucb.MissingInputStreamException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::MissingInputStreamException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::MissingInputStreamException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_MISSINGINPUTSTREAMEXCEPTION_HPP
