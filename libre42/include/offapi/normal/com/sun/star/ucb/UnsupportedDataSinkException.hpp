#ifndef INCLUDED_COM_SUN_STAR_UCB_UNSUPPORTEDDATASINKEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_UNSUPPORTEDDATASINKEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/UnsupportedDataSinkException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline UnsupportedDataSinkException::UnsupportedDataSinkException() SAL_THROW(())
    : ::css::uno::Exception()
    , Sink()
{ }

inline UnsupportedDataSinkException::UnsupportedDataSinkException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Reference< ::css::uno::XInterface >& Sink_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Sink(Sink_)
{ }

UnsupportedDataSinkException::UnsupportedDataSinkException(UnsupportedDataSinkException const & the_other): ::css::uno::Exception(the_other), Sink(the_other.Sink) {}

UnsupportedDataSinkException::~UnsupportedDataSinkException() {}

UnsupportedDataSinkException & UnsupportedDataSinkException::operator =(UnsupportedDataSinkException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Sink = the_other.Sink;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::UnsupportedDataSinkException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.ucb.UnsupportedDataSinkException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::UnsupportedDataSinkException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::UnsupportedDataSinkException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_UNSUPPORTEDDATASINKEXCEPTION_HPP
