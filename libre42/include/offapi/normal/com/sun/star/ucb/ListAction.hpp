#ifndef INCLUDED_COM_SUN_STAR_UCB_LISTACTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_LISTACTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/ListAction.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline ListAction::ListAction() SAL_THROW(())
    : Position(0)
    , Count(0)
    , ListActionType(0)
    , ActionInfo()
{
}

inline ListAction::ListAction(const ::sal_Int32& Position_, const ::sal_Int32& Count_, const ::sal_Int32& ListActionType_, const ::css::uno::Any& ActionInfo_) SAL_THROW(())
    : Position(Position_)
    , Count(Count_)
    , ListActionType(ListActionType_)
    , ActionInfo(ActionInfo_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::ListAction const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.ListAction");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::ListAction const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::ListAction >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_LISTACTION_HPP
