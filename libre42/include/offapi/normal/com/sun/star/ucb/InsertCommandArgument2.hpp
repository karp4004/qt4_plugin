#ifndef INCLUDED_COM_SUN_STAR_UCB_INSERTCOMMANDARGUMENT2_HPP
#define INCLUDED_COM_SUN_STAR_UCB_INSERTCOMMANDARGUMENT2_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/InsertCommandArgument2.hdl"

#include "com/sun/star/ucb/InsertCommandArgument.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline InsertCommandArgument2::InsertCommandArgument2() SAL_THROW(())
    : ::css::ucb::InsertCommandArgument()
    , MimeType()
    , DocumentId()
{
}

inline InsertCommandArgument2::InsertCommandArgument2(const ::css::uno::Reference< ::css::io::XInputStream >& Data_, const ::sal_Bool& ReplaceExisting_, const ::rtl::OUString& MimeType_, const ::rtl::OUString& DocumentId_) SAL_THROW(())
    : ::css::ucb::InsertCommandArgument(Data_, ReplaceExisting_)
    , MimeType(MimeType_)
    , DocumentId(DocumentId_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::InsertCommandArgument2 const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.InsertCommandArgument2");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::InsertCommandArgument2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::InsertCommandArgument2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_INSERTCOMMANDARGUMENT2_HPP
