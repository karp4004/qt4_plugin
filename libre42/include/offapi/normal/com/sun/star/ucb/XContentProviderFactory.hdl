#ifndef INCLUDED_COM_SUN_STAR_UCB_XCONTENTPROVIDERFACTORY_HDL
#define INCLUDED_COM_SUN_STAR_UCB_XCONTENTPROVIDERFACTORY_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace ucb { class XContentProvider; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ucb {

class SAL_NO_VTABLE XContentProviderFactory : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::ucb::XContentProvider > SAL_CALL createContentProvider( const ::rtl::OUString& Service ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XContentProviderFactory() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::XContentProviderFactory const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ucb::XContentProviderFactory > *) SAL_THROW(());

#endif
