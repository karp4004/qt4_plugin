#ifndef INCLUDED_COM_SUN_STAR_UCB_FETCHRESULT_HPP
#define INCLUDED_COM_SUN_STAR_UCB_FETCHRESULT_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/FetchResult.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline FetchResult::FetchResult() SAL_THROW(())
    : Rows()
    , StartIndex(0)
    , Orientation(false)
    , FetchError(0)
{
}

inline FetchResult::FetchResult(const ::css::uno::Sequence< ::css::uno::Any >& Rows_, const ::sal_Int32& StartIndex_, const ::sal_Bool& Orientation_, const ::sal_Int16& FetchError_) SAL_THROW(())
    : Rows(Rows_)
    , StartIndex(StartIndex_)
    , Orientation(Orientation_)
    , FetchError(FetchError_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::FetchResult const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.ucb.FetchResult");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::FetchResult const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::FetchResult >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_FETCHRESULT_HPP
