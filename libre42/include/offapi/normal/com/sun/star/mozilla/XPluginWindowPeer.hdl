#ifndef INCLUDED_COM_SUN_STAR_MOZILLA_XPLUGINWINDOWPEER_HDL
#define INCLUDED_COM_SUN_STAR_MOZILLA_XPLUGINWINDOWPEER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace mozilla {

class SAL_NO_VTABLE XPluginWindowPeer : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setChildWindow( const ::css::uno::Any& SystemWindowData ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPluginWindowPeer() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mozilla::XPluginWindowPeer const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::mozilla::XPluginWindowPeer > *) SAL_THROW(());

#endif
