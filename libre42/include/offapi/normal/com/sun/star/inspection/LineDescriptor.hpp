#ifndef INCLUDED_COM_SUN_STAR_INSPECTION_LINEDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_INSPECTION_LINEDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/inspection/LineDescriptor.hdl"

#include "com/sun/star/graphic/XGraphic.hpp"
#include "com/sun/star/inspection/XPropertyControl.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace inspection {

inline LineDescriptor::LineDescriptor() SAL_THROW(())
    : DisplayName()
    , Control()
    , HelpURL()
    , HasPrimaryButton(false)
    , PrimaryButtonId()
    , PrimaryButtonImageURL()
    , PrimaryButtonImage()
    , HasSecondaryButton(false)
    , SecondaryButtonId()
    , SecondaryButtonImageURL()
    , SecondaryButtonImage()
    , IndentLevel(0)
    , Category()
{
}

inline LineDescriptor::LineDescriptor(const ::rtl::OUString& DisplayName_, const ::css::uno::Reference< ::css::inspection::XPropertyControl >& Control_, const ::rtl::OUString& HelpURL_, const ::sal_Bool& HasPrimaryButton_, const ::rtl::OUString& PrimaryButtonId_, const ::rtl::OUString& PrimaryButtonImageURL_, const ::css::uno::Reference< ::css::graphic::XGraphic >& PrimaryButtonImage_, const ::sal_Bool& HasSecondaryButton_, const ::rtl::OUString& SecondaryButtonId_, const ::rtl::OUString& SecondaryButtonImageURL_, const ::css::uno::Reference< ::css::graphic::XGraphic >& SecondaryButtonImage_, const ::sal_Int16& IndentLevel_, const ::rtl::OUString& Category_) SAL_THROW(())
    : DisplayName(DisplayName_)
    , Control(Control_)
    , HelpURL(HelpURL_)
    , HasPrimaryButton(HasPrimaryButton_)
    , PrimaryButtonId(PrimaryButtonId_)
    , PrimaryButtonImageURL(PrimaryButtonImageURL_)
    , PrimaryButtonImage(PrimaryButtonImage_)
    , HasSecondaryButton(HasSecondaryButton_)
    , SecondaryButtonId(SecondaryButtonId_)
    , SecondaryButtonImageURL(SecondaryButtonImageURL_)
    , SecondaryButtonImage(SecondaryButtonImage_)
    , IndentLevel(IndentLevel_)
    , Category(Category_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace inspection {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::inspection::LineDescriptor const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.inspection.LineDescriptor");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::inspection::LineDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::inspection::LineDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_INSPECTION_LINEDESCRIPTOR_HPP
