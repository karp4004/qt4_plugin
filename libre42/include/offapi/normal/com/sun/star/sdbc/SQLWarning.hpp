#ifndef INCLUDED_COM_SUN_STAR_SDBC_SQLWARNING_HPP
#define INCLUDED_COM_SUN_STAR_SDBC_SQLWARNING_HPP

#include "sal/config.h"

#include "com/sun/star/sdbc/SQLWarning.hdl"

#include "com/sun/star/sdbc/SQLException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdbc {

inline SQLWarning::SQLWarning() SAL_THROW(())
    : ::css::sdbc::SQLException()
{ }

inline SQLWarning::SQLWarning(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& SQLState_, const ::sal_Int32& ErrorCode_, const ::css::uno::Any& NextException_) SAL_THROW(())
    : ::css::sdbc::SQLException(Message_, Context_, SQLState_, ErrorCode_, NextException_)
{ }

SQLWarning::SQLWarning(SQLWarning const & the_other): ::css::sdbc::SQLException(the_other) {}

SQLWarning::~SQLWarning() {}

SQLWarning & SQLWarning::operator =(SQLWarning const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::sdbc::SQLException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace sdbc {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdbc::SQLWarning const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.sdbc.SQLWarning" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdbc::SQLWarning const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdbc::SQLWarning >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDBC_SQLWARNING_HPP
