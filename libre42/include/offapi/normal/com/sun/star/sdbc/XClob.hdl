#ifndef INCLUDED_COM_SUN_STAR_SDBC_XCLOB_HDL
#define INCLUDED_COM_SUN_STAR_SDBC_XCLOB_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace io { class XInputStream; } } } }
#include "com/sun/star/sdbc/SQLException.hdl"
namespace com { namespace sun { namespace star { namespace sdbc { class XClob; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sdbc {

class SAL_NO_VTABLE XClob : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Int64 SAL_CALL length() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getSubString( ::sal_Int64 pos, ::sal_Int32 length ) /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::io::XInputStream > SAL_CALL getCharacterStream() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int64 SAL_CALL position( const ::rtl::OUString& searchstr, ::sal_Int32 start ) /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int64 SAL_CALL positionOfClob( const ::css::uno::Reference< ::css::sdbc::XClob >& pattern, ::sal_Int64 start ) /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XClob() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdbc::XClob const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sdbc::XClob > *) SAL_THROW(());

#endif
