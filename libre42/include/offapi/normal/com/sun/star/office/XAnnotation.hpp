#ifndef INCLUDED_COM_SUN_STAR_OFFICE_XANNOTATION_HPP
#define INCLUDED_COM_SUN_STAR_OFFICE_XANNOTATION_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/office/XAnnotation.hdl"

#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/geometry/RealPoint2D.hpp"
#include "com/sun/star/geometry/RealSize2D.hpp"
#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/text/XText.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/util/DateTime.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace office {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::office::XAnnotation const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.office.XAnnotation" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::office::XAnnotation > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::office::XAnnotation > >::get();
}

::css::uno::Type const & ::css::office::XAnnotation::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::office::XAnnotation > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_OFFICE_XANNOTATION_HPP
