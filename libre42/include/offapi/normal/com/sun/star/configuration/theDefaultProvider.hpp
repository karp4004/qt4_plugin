#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_THEDEFAULTPROVIDER_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_THEDEFAULTPROVIDER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/lang/XMultiServiceFactory.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

class theDefaultProvider {
public:
    static ::css::uno::Reference< ::css::lang::XMultiServiceFactory > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::lang::XMultiServiceFactory > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.configuration.theDefaultProvider" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.configuration.theDefaultProvider of type com.sun.star.lang.XMultiServiceFactory" ), the_context);
        }
        return instance;
    }

private:
    theDefaultProvider(); // not implemented
    theDefaultProvider(theDefaultProvider &); // not implemented
    ~theDefaultProvider(); // not implemented
    void operator =(theDefaultProvider); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_THEDEFAULTPROVIDER_HPP
