#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_INVALIDBOOTSTRAPFILEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_INVALIDBOOTSTRAPFILEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/InvalidBootstrapFileException.hdl"

#include "com/sun/star/configuration/CannotLoadConfigurationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

inline InvalidBootstrapFileException::InvalidBootstrapFileException() SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException()
    , BootstrapFileURL()
{ }

inline InvalidBootstrapFileException::InvalidBootstrapFileException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& BootstrapFileURL_) SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException(Message_, Context_)
    , BootstrapFileURL(BootstrapFileURL_)
{ }

InvalidBootstrapFileException::InvalidBootstrapFileException(InvalidBootstrapFileException const & the_other): ::css::configuration::CannotLoadConfigurationException(the_other), BootstrapFileURL(the_other.BootstrapFileURL) {}

InvalidBootstrapFileException::~InvalidBootstrapFileException() {}

InvalidBootstrapFileException & InvalidBootstrapFileException::operator =(InvalidBootstrapFileException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::CannotLoadConfigurationException::operator =(the_other);
    BootstrapFileURL = the_other.BootstrapFileURL;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace configuration {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::InvalidBootstrapFileException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.configuration.InvalidBootstrapFileException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::InvalidBootstrapFileException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::InvalidBootstrapFileException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_INVALIDBOOTSTRAPFILEEXCEPTION_HPP
