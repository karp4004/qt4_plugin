#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDACCESSEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDACCESSEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/BackendAccessException.hdl"

#include "com/sun/star/lang/WrappedTargetException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline BackendAccessException::BackendAccessException() SAL_THROW(())
    : ::css::lang::WrappedTargetException()
{ }

inline BackendAccessException::BackendAccessException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& TargetException_) SAL_THROW(())
    : ::css::lang::WrappedTargetException(Message_, Context_, TargetException_)
{ }

BackendAccessException::BackendAccessException(BackendAccessException const & the_other): ::css::lang::WrappedTargetException(the_other) {}

BackendAccessException::~BackendAccessException() {}

BackendAccessException & BackendAccessException::operator =(BackendAccessException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::lang::WrappedTargetException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::BackendAccessException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.configuration.backend.BackendAccessException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::BackendAccessException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::BackendAccessException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDACCESSEXCEPTION_HPP
