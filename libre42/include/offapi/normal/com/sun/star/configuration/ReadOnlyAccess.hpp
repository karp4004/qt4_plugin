#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_READONLYACCESS_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_READONLYACCESS_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/container/XHierarchicalNameAccess.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

class ReadOnlyAccess {
public:
    static ::css::uno::Reference< ::css::container::XHierarchicalNameAccess > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& locale) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= locale;
        ::css::uno::Reference< ::css::container::XHierarchicalNameAccess > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::container::XHierarchicalNameAccess >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.configuration.ReadOnlyAccess" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.configuration.ReadOnlyAccess of type com.sun.star.container.XHierarchicalNameAccess: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.configuration.ReadOnlyAccess of type com.sun.star.container.XHierarchicalNameAccess" ), the_context);
        }
        return the_instance;
    }

private:
    ReadOnlyAccess(); // not implemented
    ReadOnlyAccess(ReadOnlyAccess &); // not implemented
    ~ReadOnlyAccess(); // not implemented
    void operator =(ReadOnlyAccess); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_READONLYACCESS_HPP
