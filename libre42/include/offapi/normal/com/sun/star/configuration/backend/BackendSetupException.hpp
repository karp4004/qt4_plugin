#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDSETUPEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDSETUPEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/BackendSetupException.hdl"

#include "com/sun/star/configuration/CannotLoadConfigurationException.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline BackendSetupException::BackendSetupException() SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException()
    , BackendException()
{ }

inline BackendSetupException::BackendSetupException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& BackendException_) SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException(Message_, Context_)
    , BackendException(BackendException_)
{ }

BackendSetupException::BackendSetupException(BackendSetupException const & the_other): ::css::configuration::CannotLoadConfigurationException(the_other), BackendException(the_other.BackendException) {}

BackendSetupException::~BackendSetupException() {}

BackendSetupException & BackendSetupException::operator =(BackendSetupException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::CannotLoadConfigurationException::operator =(the_other);
    BackendException = the_other.BackendException;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::BackendSetupException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.configuration.backend.BackendSetupException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::BackendSetupException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::BackendSetupException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDSETUPEXCEPTION_HPP
