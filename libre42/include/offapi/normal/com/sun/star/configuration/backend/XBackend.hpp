#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_XBACKEND_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_XBACKEND_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/configuration/backend/XBackend.hdl"

#include "com/sun/star/configuration/backend/BackendAccessException.hpp"
#include "com/sun/star/configuration/backend/XLayer.hpp"
#include "com/sun/star/configuration/backend/XUpdateHandler.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/NoSupportException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::XBackend const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.configuration.backend.XBackend" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::configuration::backend::XBackend > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::configuration::backend::XBackend > >::get();
}

::css::uno::Type const & ::css::configuration::backend::XBackend::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::configuration::backend::XBackend > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_XBACKEND_HPP
