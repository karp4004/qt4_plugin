#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_XTEMPLATEINSTANCE_HDL
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_XTEMPLATEINSTANCE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace configuration {

class SAL_NO_VTABLE XTemplateInstance : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::rtl::OUString SAL_CALL getTemplateName() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTemplateInstance() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::XTemplateInstance const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::configuration::XTemplateInstance > *) SAL_THROW(());

#endif
