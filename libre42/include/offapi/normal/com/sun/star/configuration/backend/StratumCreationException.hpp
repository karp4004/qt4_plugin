#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_STRATUMCREATIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_STRATUMCREATIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/StratumCreationException.hdl"

#include "com/sun/star/configuration/backend/BackendSetupException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline StratumCreationException::StratumCreationException() SAL_THROW(())
    : ::css::configuration::backend::BackendSetupException()
    , StratumService()
    , StratumData()
{ }

inline StratumCreationException::StratumCreationException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& BackendException_, const ::rtl::OUString& StratumService_, const ::rtl::OUString& StratumData_) SAL_THROW(())
    : ::css::configuration::backend::BackendSetupException(Message_, Context_, BackendException_)
    , StratumService(StratumService_)
    , StratumData(StratumData_)
{ }

StratumCreationException::StratumCreationException(StratumCreationException const & the_other): ::css::configuration::backend::BackendSetupException(the_other), StratumService(the_other.StratumService), StratumData(the_other.StratumData) {}

StratumCreationException::~StratumCreationException() {}

StratumCreationException & StratumCreationException::operator =(StratumCreationException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::backend::BackendSetupException::operator =(the_other);
    StratumService = the_other.StratumService;
    StratumData = the_other.StratumData;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::StratumCreationException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.configuration.backend.StratumCreationException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::StratumCreationException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::StratumCreationException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_STRATUMCREATIONEXCEPTION_HPP
