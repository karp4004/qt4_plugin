#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_MALFORMEDDATAEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_MALFORMEDDATAEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/MalformedDataException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline MalformedDataException::MalformedDataException() SAL_THROW(())
    : ::css::uno::Exception()
    , ErrorDetails()
{ }

inline MalformedDataException::MalformedDataException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& ErrorDetails_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ErrorDetails(ErrorDetails_)
{ }

MalformedDataException::MalformedDataException(MalformedDataException const & the_other): ::css::uno::Exception(the_other), ErrorDetails(the_other.ErrorDetails) {}

MalformedDataException::~MalformedDataException() {}

MalformedDataException & MalformedDataException::operator =(MalformedDataException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ErrorDetails = the_other.ErrorDetails;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::MalformedDataException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.configuration.backend.MalformedDataException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::MalformedDataException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::MalformedDataException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_MALFORMEDDATAEXCEPTION_HPP
