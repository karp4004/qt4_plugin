#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_INSTALLATIONINCOMPLETEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_INSTALLATIONINCOMPLETEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/InstallationIncompleteException.hdl"

#include "com/sun/star/configuration/CannotLoadConfigurationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

inline InstallationIncompleteException::InstallationIncompleteException() SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException()
{ }

inline InstallationIncompleteException::InstallationIncompleteException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException(Message_, Context_)
{ }

InstallationIncompleteException::InstallationIncompleteException(InstallationIncompleteException const & the_other): ::css::configuration::CannotLoadConfigurationException(the_other) {}

InstallationIncompleteException::~InstallationIncompleteException() {}

InstallationIncompleteException & InstallationIncompleteException::operator =(InstallationIncompleteException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::CannotLoadConfigurationException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace configuration {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::InstallationIncompleteException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.configuration.InstallationIncompleteException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::InstallationIncompleteException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::InstallationIncompleteException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_INSTALLATIONINCOMPLETEEXCEPTION_HPP
