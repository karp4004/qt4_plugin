#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_INSUFFICIENTACCESSRIGHTSEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_INSUFFICIENTACCESSRIGHTSEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/InsufficientAccessRightsException.hdl"

#include "com/sun/star/configuration/backend/BackendAccessException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline InsufficientAccessRightsException::InsufficientAccessRightsException() SAL_THROW(())
    : ::css::configuration::backend::BackendAccessException()
{ }

inline InsufficientAccessRightsException::InsufficientAccessRightsException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& TargetException_) SAL_THROW(())
    : ::css::configuration::backend::BackendAccessException(Message_, Context_, TargetException_)
{ }

InsufficientAccessRightsException::InsufficientAccessRightsException(InsufficientAccessRightsException const & the_other): ::css::configuration::backend::BackendAccessException(the_other) {}

InsufficientAccessRightsException::~InsufficientAccessRightsException() {}

InsufficientAccessRightsException & InsufficientAccessRightsException::operator =(InsufficientAccessRightsException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::backend::BackendAccessException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::InsufficientAccessRightsException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.configuration.backend.InsufficientAccessRightsException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::InsufficientAccessRightsException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::InsufficientAccessRightsException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_INSUFFICIENTACCESSRIGHTSEXCEPTION_HPP
