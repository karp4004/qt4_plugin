#ifndef INCLUDED_COM_SUN_STAR_REPORT_XFIXEDTEXT_HDL
#define INCLUDED_COM_SUN_STAR_REPORT_XFIXEDTEXT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XReportControlModel.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace report {

class SAL_NO_VTABLE XFixedText : public ::css::report::XReportControlModel
{
public:

    // Attributes
    virtual ::rtl::OUString SAL_CALL getLabel() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setLabel( const ::rtl::OUString& _label ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFixedText() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XFixedText const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::report::XFixedText > *) SAL_THROW(());

#endif
