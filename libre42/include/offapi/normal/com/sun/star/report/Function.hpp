#ifndef INCLUDED_COM_SUN_STAR_REPORT_FUNCTION_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_FUNCTION_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/report/XFunction.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace report {

class Function {
public:
    static ::css::uno::Reference< ::css::report::XFunction > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::report::XFunction > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::report::XFunction >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.report.Function" ), ::css::uno::Sequence< ::css::uno::Any >(), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.report.Function of type com.sun.star.report.XFunction: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.report.Function of type com.sun.star.report.XFunction" ), the_context);
        }
        return the_instance;
    }

private:
    Function(); // not implemented
    Function(Function &); // not implemented
    ~Function(); // not implemented
    void operator =(Function); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_REPORT_FUNCTION_HPP
