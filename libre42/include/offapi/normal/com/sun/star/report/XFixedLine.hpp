#ifndef INCLUDED_COM_SUN_STAR_REPORT_XFIXEDLINE_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_XFIXEDLINE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XFixedLine.hdl"

#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/drawing/LineDash.hpp"
#include "com/sun/star/drawing/LineStyle.hpp"
#include "com/sun/star/report/XReportControlModel.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/util/Color.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace report {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XFixedLine const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.report.XFixedLine" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::report::XFixedLine > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::report::XFixedLine > >::get();
}

::css::uno::Type const & ::css::report::XFixedLine::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::report::XFixedLine > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REPORT_XFIXEDLINE_HPP
