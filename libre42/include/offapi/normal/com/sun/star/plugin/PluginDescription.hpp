#ifndef INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINDESCRIPTION_HPP
#define INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINDESCRIPTION_HPP

#include "sal/config.h"

#include "com/sun/star/plugin/PluginDescription.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace plugin {

inline PluginDescription::PluginDescription() SAL_THROW(())
    : PluginName()
    , Mimetype()
    , Extension()
    , Description()
{
}

inline PluginDescription::PluginDescription(const ::rtl::OUString& PluginName_, const ::rtl::OUString& Mimetype_, const ::rtl::OUString& Extension_, const ::rtl::OUString& Description_) SAL_THROW(())
    : PluginName(PluginName_)
    , Mimetype(Mimetype_)
    , Extension(Extension_)
    , Description(Description_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace plugin {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::plugin::PluginDescription const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.plugin.PluginDescription");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::plugin::PluginDescription const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::plugin::PluginDescription >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINDESCRIPTION_HPP
