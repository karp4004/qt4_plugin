#ifndef INCLUDED_COM_SUN_STAR_PRESENTATION_XTRANSITIONFACTORY_HPP
#define INCLUDED_COM_SUN_STAR_PRESENTATION_XTRANSITIONFACTORY_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/presentation/XTransitionFactory.hdl"

#include "com/sun/star/presentation/XSlideShowView.hpp"
#include "com/sun/star/presentation/XTransition.hpp"
#include "com/sun/star/rendering/XBitmap.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace presentation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::presentation::XTransitionFactory const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.presentation.XTransitionFactory" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::presentation::XTransitionFactory > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::presentation::XTransitionFactory > >::get();
}

::css::uno::Type const & ::css::presentation::XTransitionFactory::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::presentation::XTransitionFactory > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_PRESENTATION_XTRANSITIONFACTORY_HPP
