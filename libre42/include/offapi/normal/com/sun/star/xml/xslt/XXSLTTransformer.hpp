#ifndef INCLUDED_COM_SUN_STAR_XML_XSLT_XXSLTTRANSFORMER_HPP
#define INCLUDED_COM_SUN_STAR_XML_XSLT_XXSLTTRANSFORMER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xml/xslt/XXSLTTransformer.hdl"

#include "com/sun/star/io/XActiveDataControl.hpp"
#include "com/sun/star/io/XActiveDataSink.hpp"
#include "com/sun/star/io/XActiveDataSource.hpp"
#include "com/sun/star/lang/XInitialization.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace xslt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::xslt::XXSLTTransformer const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.xml.xslt.XXSLTTransformer" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xml::xslt::XXSLTTransformer > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xml::xslt::XXSLTTransformer > >::get();
}

::css::uno::Type const & ::css::xml::xslt::XXSLTTransformer::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xml::xslt::XXSLTTransformer > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XML_XSLT_XXSLTTRANSFORMER_HPP
