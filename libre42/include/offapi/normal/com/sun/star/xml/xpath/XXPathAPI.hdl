#ifndef INCLUDED_COM_SUN_STAR_XML_XPATH_XXPATHAPI_HDL
#define INCLUDED_COM_SUN_STAR_XML_XPATH_XXPATHAPI_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XNode; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XNodeList; } } } } }
#include "com/sun/star/xml/xpath/XPathException.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace xpath { class XXPathExtension; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace xpath { class XXPathObject; } } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace xpath {

class SAL_NO_VTABLE XXPathAPI : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL registerNS( const ::rtl::OUString& prefix, const ::rtl::OUString& url ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL unregisterNS( const ::rtl::OUString& prefix, const ::rtl::OUString& url ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL registerExtension( const ::rtl::OUString& serviceName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL registerExtensionInstance( const ::css::uno::Reference< ::css::xml::xpath::XXPathExtension >& aExtension ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::xpath::XXPathObject > SAL_CALL eval( const ::css::uno::Reference< ::css::xml::dom::XNode >& contextNode, const ::rtl::OUString& expr ) /* throw (::css::xml::xpath::XPathException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::xpath::XXPathObject > SAL_CALL evalNS( const ::css::uno::Reference< ::css::xml::dom::XNode >& contextNode, const ::rtl::OUString& expr, const ::css::uno::Reference< ::css::xml::dom::XNode >& namespaceNode ) /* throw (::css::xml::xpath::XPathException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XNodeList > SAL_CALL selectNodeList( const ::css::uno::Reference< ::css::xml::dom::XNode >& contextNode, const ::rtl::OUString& expr ) /* throw (::css::xml::xpath::XPathException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XNodeList > SAL_CALL selectNodeListNS( const ::css::uno::Reference< ::css::xml::dom::XNode >& contextNode, const ::rtl::OUString& expr, const ::css::uno::Reference< ::css::xml::dom::XNode >& namespaceNode ) /* throw (::css::xml::xpath::XPathException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XNode > SAL_CALL selectSingleNode( const ::css::uno::Reference< ::css::xml::dom::XNode >& contextNode, const ::rtl::OUString& expr ) /* throw (::css::xml::xpath::XPathException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XNode > SAL_CALL selectSingleNodeNS( const ::css::uno::Reference< ::css::xml::dom::XNode >& contextNode, const ::rtl::OUString& expr, const ::css::uno::Reference< ::css::xml::dom::XNode >& namespaceNode ) /* throw (::css::xml::xpath::XPathException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XXPathAPI() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::xpath::XXPathAPI const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::xpath::XXPathAPI > *) SAL_THROW(());

#endif
