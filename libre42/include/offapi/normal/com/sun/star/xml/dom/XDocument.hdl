#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_XDOCUMENT_HDL
#define INCLUDED_COM_SUN_STAR_XML_DOM_XDOCUMENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/xml/dom/DOMException.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XAttr; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XCDATASection; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XComment; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XDOMImplementation; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XDocumentFragment; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XDocumentType; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XElement; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XEntityReference; } } } } }
#include "com/sun/star/xml/dom/XNode.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XNodeList; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XProcessingInstruction; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XText; } } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom {

class SAL_NO_VTABLE XDocument : public ::css::xml::dom::XNode
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::xml::dom::XAttr > SAL_CALL createAttribute( const ::rtl::OUString& name ) /* throw (::css::xml::dom::DOMException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XAttr > SAL_CALL createAttributeNS( const ::rtl::OUString& namespaceURI, const ::rtl::OUString& qualifiedName ) /* throw (::css::xml::dom::DOMException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XCDATASection > SAL_CALL createCDATASection( const ::rtl::OUString& data ) /* throw (::css::xml::dom::DOMException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XComment > SAL_CALL createComment( const ::rtl::OUString& data ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XDocumentFragment > SAL_CALL createDocumentFragment() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XElement > SAL_CALL createElement( const ::rtl::OUString& tagName ) /* throw (::css::xml::dom::DOMException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XElement > SAL_CALL createElementNS( const ::rtl::OUString& namespaceURI, const ::rtl::OUString& qualifiedName ) /* throw (::css::xml::dom::DOMException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XEntityReference > SAL_CALL createEntityReference( const ::rtl::OUString& name ) /* throw (::css::xml::dom::DOMException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XProcessingInstruction > SAL_CALL createProcessingInstruction( const ::rtl::OUString& target, const ::rtl::OUString& data ) /* throw (::css::xml::dom::DOMException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XText > SAL_CALL createTextNode( const ::rtl::OUString& data ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XDocumentType > SAL_CALL getDoctype() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XElement > SAL_CALL getDocumentElement() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XElement > SAL_CALL getElementById( const ::rtl::OUString& elementId ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XNodeList > SAL_CALL getElementsByTagName( const ::rtl::OUString& tagname ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XNodeList > SAL_CALL getElementsByTagNameNS( const ::rtl::OUString& namespaceURI, const ::rtl::OUString& localName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XDOMImplementation > SAL_CALL getImplementation() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::dom::XNode > SAL_CALL importNode( const ::css::uno::Reference< ::css::xml::dom::XNode >& importedNode, ::sal_Bool deep ) /* throw (::css::xml::dom::DOMException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDocument() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::XDocument const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::dom::XDocument > *) SAL_THROW(());

#endif
