#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_XDOCUMENT_HPP
#define INCLUDED_COM_SUN_STAR_XML_DOM_XDOCUMENT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xml/dom/XDocument.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/xml/dom/DOMException.hpp"
#include "com/sun/star/xml/dom/XAttr.hpp"
#include "com/sun/star/xml/dom/XCDATASection.hpp"
#include "com/sun/star/xml/dom/XComment.hpp"
#include "com/sun/star/xml/dom/XDOMImplementation.hpp"
#include "com/sun/star/xml/dom/XDocumentFragment.hpp"
#include "com/sun/star/xml/dom/XDocumentType.hpp"
#include "com/sun/star/xml/dom/XElement.hpp"
#include "com/sun/star/xml/dom/XEntityReference.hpp"
#include "com/sun/star/xml/dom/XNode.hpp"
#include "com/sun/star/xml/dom/XNodeList.hpp"
#include "com/sun/star/xml/dom/XProcessingInstruction.hpp"
#include "com/sun/star/xml/dom/XText.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace xml { namespace dom {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::XDocument const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.xml.dom.XDocument" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xml::dom::XDocument > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xml::dom::XDocument > >::get();
}

::css::uno::Type const & ::css::xml::dom::XDocument::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xml::dom::XDocument > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XML_DOM_XDOCUMENT_HPP
