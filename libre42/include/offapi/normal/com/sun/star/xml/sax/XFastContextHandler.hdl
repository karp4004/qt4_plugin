#ifndef INCLUDED_COM_SUN_STAR_XML_SAX_XFASTCONTEXTHANDLER_HDL
#define INCLUDED_COM_SUN_STAR_XML_SAX_XFASTCONTEXTHANDLER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/xml/sax/SAXException.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace sax { class XFastAttributeList; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace sax { class XFastContextHandler; } } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

class SAL_NO_VTABLE XFastContextHandler : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL startFastElement( ::sal_Int32 Element, const ::css::uno::Reference< ::css::xml::sax::XFastAttributeList >& Attribs ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL startUnknownElement( const ::rtl::OUString& Namespace, const ::rtl::OUString& Name, const ::css::uno::Reference< ::css::xml::sax::XFastAttributeList >& Attribs ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL endFastElement( ::sal_Int32 Element ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL endUnknownElement( const ::rtl::OUString& Namespace, const ::rtl::OUString& Name ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::sax::XFastContextHandler > SAL_CALL createFastChildContext( ::sal_Int32 Element, const ::css::uno::Reference< ::css::xml::sax::XFastAttributeList >& Attribs ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::sax::XFastContextHandler > SAL_CALL createUnknownChildContext( const ::rtl::OUString& Namespace, const ::rtl::OUString& Name, const ::css::uno::Reference< ::css::xml::sax::XFastAttributeList >& Attribs ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL characters( const ::rtl::OUString& aChars ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFastContextHandler() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::sax::XFastContextHandler const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::sax::XFastContextHandler > *) SAL_THROW(());

#endif
