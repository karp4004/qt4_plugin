#ifndef INCLUDED_COM_SUN_STAR_XML_SAX_SAXINVALIDCHARACTEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XML_SAX_SAXINVALIDCHARACTEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xml/sax/SAXInvalidCharacterException.hdl"

#include "com/sun/star/xml/sax/SAXException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

inline SAXInvalidCharacterException::SAXInvalidCharacterException() SAL_THROW(())
    : ::css::xml::sax::SAXException()
{ }

inline SAXInvalidCharacterException::SAXInvalidCharacterException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& WrappedException_) SAL_THROW(())
    : ::css::xml::sax::SAXException(Message_, Context_, WrappedException_)
{ }

SAXInvalidCharacterException::SAXInvalidCharacterException(SAXInvalidCharacterException const & the_other): ::css::xml::sax::SAXException(the_other) {}

SAXInvalidCharacterException::~SAXInvalidCharacterException() {}

SAXInvalidCharacterException & SAXInvalidCharacterException::operator =(SAXInvalidCharacterException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::xml::sax::SAXException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::sax::SAXInvalidCharacterException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.xml.sax.SAXInvalidCharacterException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::sax::SAXInvalidCharacterException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::sax::SAXInvalidCharacterException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_SAX_SAXINVALIDCHARACTEREXCEPTION_HPP
