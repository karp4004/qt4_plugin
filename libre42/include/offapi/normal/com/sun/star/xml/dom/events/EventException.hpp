#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_EVENTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_EVENTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xml/dom/events/EventException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace events {

inline EventException::EventException() SAL_THROW(())
    : ::css::uno::Exception()
    , code(0)
{ }

inline EventException::EventException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int16& code_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , code(code_)
{ }

EventException::EventException(EventException const & the_other): ::css::uno::Exception(the_other), code(the_other.code) {}

EventException::~EventException() {}

EventException & EventException::operator =(EventException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    code = the_other.code;
    return *this;
}

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace events {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::events::EventException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.xml.dom.events.EventException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::dom::events::EventException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::dom::events::EventException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_EVENTEXCEPTION_HPP
