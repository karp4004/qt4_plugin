#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_XENTITY_HPP
#define INCLUDED_COM_SUN_STAR_XML_DOM_XENTITY_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xml/dom/XEntity.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/xml/dom/XNode.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace dom {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::XEntity const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.xml.dom.XEntity" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xml::dom::XEntity > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xml::dom::XEntity > >::get();
}

::css::uno::Type const & ::css::xml::dom::XEntity::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xml::dom::XEntity > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XML_DOM_XENTITY_HPP
