#ifndef INCLUDED_COM_SUN_STAR_XML_SAX_XLOCATOR_HDL
#define INCLUDED_COM_SUN_STAR_XML_SAX_XLOCATOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

class SAL_NO_VTABLE XLocator : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Int32 SAL_CALL getColumnNumber() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getLineNumber() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getPublicId() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getSystemId() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XLocator() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::sax::XLocator const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::sax::XLocator > *) SAL_THROW(());

#endif
