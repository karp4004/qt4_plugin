#ifndef INCLUDED_COM_SUN_STAR_DRAWING_XDRAWSUBCONTROLLER_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_XDRAWSUBCONTROLLER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/drawing/XDrawSubController.hdl"

#include "com/sun/star/beans/XFastPropertySet.hpp"
#include "com/sun/star/drawing/XDrawView.hpp"
#include "com/sun/star/view/XSelectionSupplier.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::XDrawSubController const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.drawing.XDrawSubController" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::drawing::XDrawSubController > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::drawing::XDrawSubController > >::get();
}

::css::uno::Type const & ::css::drawing::XDrawSubController::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::drawing::XDrawSubController > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_XDRAWSUBCONTROLLER_HPP
