#ifndef INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX4_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX4_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/HomogenMatrix4.hdl"

#include "com/sun/star/drawing/HomogenMatrixLine4.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline HomogenMatrix4::HomogenMatrix4() SAL_THROW(())
    : Line1()
    , Line2()
    , Line3()
    , Line4()
{
}

inline HomogenMatrix4::HomogenMatrix4(const ::css::drawing::HomogenMatrixLine4& Line1_, const ::css::drawing::HomogenMatrixLine4& Line2_, const ::css::drawing::HomogenMatrixLine4& Line3_, const ::css::drawing::HomogenMatrixLine4& Line4_) SAL_THROW(())
    : Line1(Line1_)
    , Line2(Line2_)
    , Line3(Line3_)
    , Line4(Line4_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::HomogenMatrix4 const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.drawing.HomogenMatrix4");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::HomogenMatrix4 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::HomogenMatrix4 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX4_HPP
