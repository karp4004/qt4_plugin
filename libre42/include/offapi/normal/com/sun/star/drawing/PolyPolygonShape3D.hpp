#ifndef INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONSHAPE3D_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONSHAPE3D_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/PolyPolygonShape3D.hdl"

#include "com/sun/star/drawing/DoubleSequenceSequence.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline PolyPolygonShape3D::PolyPolygonShape3D() SAL_THROW(())
    : SequenceX()
    , SequenceY()
    , SequenceZ()
{
}

inline PolyPolygonShape3D::PolyPolygonShape3D(const ::css::uno::Sequence< ::css::uno::Sequence< double > >& SequenceX_, const ::css::uno::Sequence< ::css::uno::Sequence< double > >& SequenceY_, const ::css::uno::Sequence< ::css::uno::Sequence< double > >& SequenceZ_) SAL_THROW(())
    : SequenceX(SequenceX_)
    , SequenceY(SequenceY_)
    , SequenceZ(SequenceZ_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::PolyPolygonShape3D const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.drawing.PolyPolygonShape3D");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::PolyPolygonShape3D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::PolyPolygonShape3D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONSHAPE3D_HPP
