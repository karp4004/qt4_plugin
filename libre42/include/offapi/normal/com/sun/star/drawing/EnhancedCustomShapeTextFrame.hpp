#ifndef INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPETEXTFRAME_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPETEXTFRAME_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/EnhancedCustomShapeTextFrame.hdl"

#include "com/sun/star/drawing/EnhancedCustomShapeParameterPair.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline EnhancedCustomShapeTextFrame::EnhancedCustomShapeTextFrame() SAL_THROW(())
    : TopLeft()
    , BottomRight()
{
}

inline EnhancedCustomShapeTextFrame::EnhancedCustomShapeTextFrame(const ::css::drawing::EnhancedCustomShapeParameterPair& TopLeft_, const ::css::drawing::EnhancedCustomShapeParameterPair& BottomRight_) SAL_THROW(())
    : TopLeft(TopLeft_)
    , BottomRight(BottomRight_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::EnhancedCustomShapeTextFrame const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.drawing.EnhancedCustomShapeTextFrame");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::EnhancedCustomShapeTextFrame const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::EnhancedCustomShapeTextFrame >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPETEXTFRAME_HPP
