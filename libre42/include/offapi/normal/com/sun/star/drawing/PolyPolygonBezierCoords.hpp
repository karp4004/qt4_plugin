#ifndef INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONBEZIERCOORDS_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONBEZIERCOORDS_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/PolyPolygonBezierCoords.hdl"

#include "com/sun/star/drawing/FlagSequenceSequence.hpp"
#include "com/sun/star/drawing/PointSequenceSequence.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline PolyPolygonBezierCoords::PolyPolygonBezierCoords() SAL_THROW(())
    : Coordinates()
    , Flags()
{
}

inline PolyPolygonBezierCoords::PolyPolygonBezierCoords(const ::css::uno::Sequence< ::css::uno::Sequence< ::css::awt::Point > >& Coordinates_, const ::css::uno::Sequence< ::css::uno::Sequence< ::css::drawing::PolygonFlags > >& Flags_) SAL_THROW(())
    : Coordinates(Coordinates_)
    , Flags(Flags_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::PolyPolygonBezierCoords const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.drawing.PolyPolygonBezierCoords");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::PolyPolygonBezierCoords const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::PolyPolygonBezierCoords >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONBEZIERCOORDS_HPP
