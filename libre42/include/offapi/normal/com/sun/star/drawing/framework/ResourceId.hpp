#ifndef INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_RESOURCEID_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_RESOURCEID_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/drawing/framework/XResourceId.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace drawing { namespace framework {

class ResourceId {
public:
    static ::css::uno::Reference< ::css::drawing::framework::XResourceId > createEmpty(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::drawing::framework::XResourceId > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::drawing::framework::XResourceId >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.drawing.framework.ResourceId" ), ::css::uno::Sequence< ::css::uno::Any >(), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ResourceId of type com.sun.star.drawing.framework.XResourceId: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ResourceId of type com.sun.star.drawing.framework.XResourceId" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::drawing::framework::XResourceId > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& sResourceURL) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= sResourceURL;
        ::css::uno::Reference< ::css::drawing::framework::XResourceId > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::drawing::framework::XResourceId >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.drawing.framework.ResourceId" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ResourceId of type com.sun.star.drawing.framework.XResourceId: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ResourceId of type com.sun.star.drawing.framework.XResourceId" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::drawing::framework::XResourceId > createWithAnchor(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& sResourceURL, const ::css::uno::Reference< ::css::drawing::framework::XResourceId >& xAnchor) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(2);
        the_arguments[0] <<= sResourceURL;
        the_arguments[1] <<= xAnchor;
        ::css::uno::Reference< ::css::drawing::framework::XResourceId > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::drawing::framework::XResourceId >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.drawing.framework.ResourceId" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ResourceId of type com.sun.star.drawing.framework.XResourceId: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ResourceId of type com.sun.star.drawing.framework.XResourceId" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::drawing::framework::XResourceId > createWithAnchorURL(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& sResourceURL, const ::rtl::OUString& sAnchorURL) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(2);
        the_arguments[0] <<= sResourceURL;
        the_arguments[1] <<= sAnchorURL;
        ::css::uno::Reference< ::css::drawing::framework::XResourceId > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::drawing::framework::XResourceId >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.drawing.framework.ResourceId" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ResourceId of type com.sun.star.drawing.framework.XResourceId: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ResourceId of type com.sun.star.drawing.framework.XResourceId" ), the_context);
        }
        return the_instance;
    }

private:
    ResourceId(); // not implemented
    ResourceId(ResourceId &); // not implemented
    ~ResourceId(); // not implemented
    void operator =(ResourceId); // not implemented
};

} } } } }

#endif // INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_RESOURCEID_HPP
