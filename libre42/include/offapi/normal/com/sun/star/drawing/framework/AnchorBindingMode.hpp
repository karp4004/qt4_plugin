#ifndef INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_ANCHORBINDINGMODE_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_ANCHORBINDINGMODE_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/framework/AnchorBindingMode.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace drawing { namespace framework {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::framework::AnchorBindingMode const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_ENUM, "com.sun.star.drawing.framework.AnchorBindingMode" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::framework::AnchorBindingMode const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::framework::AnchorBindingMode >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_ANCHORBINDINGMODE_HPP
