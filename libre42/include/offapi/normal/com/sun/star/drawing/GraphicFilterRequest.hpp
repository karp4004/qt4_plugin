#ifndef INCLUDED_COM_SUN_STAR_DRAWING_GRAPHICFILTERREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_GRAPHICFILTERREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/GraphicFilterRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline GraphicFilterRequest::GraphicFilterRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , ErrCode(0)
{ }

inline GraphicFilterRequest::GraphicFilterRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& ErrCode_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ErrCode(ErrCode_)
{ }

GraphicFilterRequest::GraphicFilterRequest(GraphicFilterRequest const & the_other): ::css::uno::Exception(the_other), ErrCode(the_other.ErrCode) {}

GraphicFilterRequest::~GraphicFilterRequest() {}

GraphicFilterRequest & GraphicFilterRequest::operator =(GraphicFilterRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ErrCode = the_other.ErrCode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::GraphicFilterRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.drawing.GraphicFilterRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::GraphicFilterRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::GraphicFilterRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_GRAPHICFILTERREQUEST_HPP
