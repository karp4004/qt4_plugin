#ifndef INCLUDED_COM_SUN_STAR_CHART_TIMEINCREMENT_HPP
#define INCLUDED_COM_SUN_STAR_CHART_TIMEINCREMENT_HPP

#include "sal/config.h"

#include "com/sun/star/chart/TimeIncrement.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart {

inline TimeIncrement::TimeIncrement() SAL_THROW(())
    : MajorTimeInterval()
    , MinorTimeInterval()
    , TimeResolution()
{
}

inline TimeIncrement::TimeIncrement(const ::css::uno::Any& MajorTimeInterval_, const ::css::uno::Any& MinorTimeInterval_, const ::css::uno::Any& TimeResolution_) SAL_THROW(())
    : MajorTimeInterval(MajorTimeInterval_)
    , MinorTimeInterval(MinorTimeInterval_)
    , TimeResolution(TimeResolution_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::TimeIncrement const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.chart.TimeIncrement");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::TimeIncrement const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::TimeIncrement >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_TIMEINCREMENT_HPP
