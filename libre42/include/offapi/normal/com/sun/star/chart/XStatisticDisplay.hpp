#ifndef INCLUDED_COM_SUN_STAR_CHART_XSTATISTICDISPLAY_HPP
#define INCLUDED_COM_SUN_STAR_CHART_XSTATISTICDISPLAY_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/chart/XStatisticDisplay.hdl"

#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::XStatisticDisplay const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.chart.XStatisticDisplay" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::chart::XStatisticDisplay > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::chart::XStatisticDisplay > >::get();
}

::css::uno::Type const & ::css::chart::XStatisticDisplay::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::chart::XStatisticDisplay > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_CHART_XSTATISTICDISPLAY_HPP
