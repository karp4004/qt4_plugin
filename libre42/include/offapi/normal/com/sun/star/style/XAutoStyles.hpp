#ifndef INCLUDED_COM_SUN_STAR_STYLE_XAUTOSTYLES_HPP
#define INCLUDED_COM_SUN_STAR_STYLE_XAUTOSTYLES_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/style/XAutoStyles.hdl"

#include "com/sun/star/container/XIndexAccess.hpp"
#include "com/sun/star/container/XNameAccess.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace style {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::XAutoStyles const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.style.XAutoStyles" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::style::XAutoStyles > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::style::XAutoStyles > >::get();
}

::css::uno::Type const & ::css::style::XAutoStyles::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::style::XAutoStyles > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_STYLE_XAUTOSTYLES_HPP
