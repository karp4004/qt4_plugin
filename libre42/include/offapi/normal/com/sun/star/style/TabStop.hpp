#ifndef INCLUDED_COM_SUN_STAR_STYLE_TABSTOP_HPP
#define INCLUDED_COM_SUN_STAR_STYLE_TABSTOP_HPP

#include "sal/config.h"

#include "com/sun/star/style/TabStop.hdl"

#include "com/sun/star/style/TabAlign.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace style {

inline TabStop::TabStop() SAL_THROW(())
    : Position(0)
    , Alignment(::css::style::TabAlign_LEFT)
    , DecimalChar(0)
    , FillChar(0)
{
}

inline TabStop::TabStop(const ::sal_Int32& Position_, const ::css::style::TabAlign& Alignment_, const ::sal_Unicode& DecimalChar_, const ::sal_Unicode& FillChar_) SAL_THROW(())
    : Position(Position_)
    , Alignment(Alignment_)
    , DecimalChar(DecimalChar_)
    , FillChar(FillChar_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace style {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::TabStop const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.style.TabStop");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::style::TabStop const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::style::TabStop >::get();
}

#endif // INCLUDED_COM_SUN_STAR_STYLE_TABSTOP_HPP
