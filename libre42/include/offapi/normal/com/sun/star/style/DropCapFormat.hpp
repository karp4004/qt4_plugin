#ifndef INCLUDED_COM_SUN_STAR_STYLE_DROPCAPFORMAT_HPP
#define INCLUDED_COM_SUN_STAR_STYLE_DROPCAPFORMAT_HPP

#include "sal/config.h"

#include "com/sun/star/style/DropCapFormat.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace style {

inline DropCapFormat::DropCapFormat() SAL_THROW(())
    : Lines(0)
    , Count(0)
    , Distance(0)
{
}

inline DropCapFormat::DropCapFormat(const ::sal_Int8& Lines_, const ::sal_Int8& Count_, const ::sal_Int16& Distance_) SAL_THROW(())
    : Lines(Lines_)
    , Count(Count_)
    , Distance(Distance_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace style {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::DropCapFormat const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.style.DropCapFormat");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::style::DropCapFormat const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::style::DropCapFormat >::get();
}

#endif // INCLUDED_COM_SUN_STAR_STYLE_DROPCAPFORMAT_HPP
