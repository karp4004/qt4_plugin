#ifndef INCLUDED_COM_SUN_STAR_STYLE_LINESPACING_HPP
#define INCLUDED_COM_SUN_STAR_STYLE_LINESPACING_HPP

#include "sal/config.h"

#include "com/sun/star/style/LineSpacing.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace style {

inline LineSpacing::LineSpacing() SAL_THROW(())
    : Mode(0)
    , Height(0)
{
}

inline LineSpacing::LineSpacing(const ::sal_Int16& Mode_, const ::sal_Int16& Height_) SAL_THROW(())
    : Mode(Mode_)
    , Height(Height_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace style {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::LineSpacing const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.style.LineSpacing");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::style::LineSpacing const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::style::LineSpacing >::get();
}

#endif // INCLUDED_COM_SUN_STAR_STYLE_LINESPACING_HPP
