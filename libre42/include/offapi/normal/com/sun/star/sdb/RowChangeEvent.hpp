#ifndef INCLUDED_COM_SUN_STAR_SDB_ROWCHANGEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SDB_ROWCHANGEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/RowChangeEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sdb {

inline RowChangeEvent::RowChangeEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Action(0)
    , Rows(0)
{
}

inline RowChangeEvent::RowChangeEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& Action_, const ::sal_Int32& Rows_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Action(Action_)
    , Rows(Rows_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::RowChangeEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.sdb.RowChangeEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::RowChangeEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::RowChangeEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_ROWCHANGEEVENT_HPP
