#ifndef INCLUDED_COM_SUN_STAR_SDB_DATAACCESSDESCRIPTORFACTORY_HPP
#define INCLUDED_COM_SUN_STAR_SDB_DATAACCESSDESCRIPTORFACTORY_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/sdb/XDataAccessDescriptorFactory.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace sdb {

class DataAccessDescriptorFactory {
public:
    static ::css::uno::Reference< ::css::sdb::XDataAccessDescriptorFactory > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::sdb::XDataAccessDescriptorFactory > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.sdb.DataAccessDescriptorFactory" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.sdb.DataAccessDescriptorFactory of type com.sun.star.sdb.XDataAccessDescriptorFactory" ), the_context);
        }
        return instance;
    }

private:
    DataAccessDescriptorFactory(); // not implemented
    DataAccessDescriptorFactory(DataAccessDescriptorFactory &); // not implemented
    ~DataAccessDescriptorFactory(); // not implemented
    void operator =(DataAccessDescriptorFactory); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_SDB_DATAACCESSDESCRIPTORFACTORY_HPP
