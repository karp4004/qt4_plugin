#ifndef INCLUDED_COM_SUN_STAR_SDB_APPLICATION_XDATABASEDOCUMENTUI_HDL
#define INCLUDED_COM_SUN_STAR_SDB_APPLICATION_XDATABASEDOCUMENTUI_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { class XWindow; } } } }
#include "com/sun/star/beans/Pair.hdl"
#include "com/sun/star/beans/PropertyValue.hdl"
#include "com/sun/star/container/NoSuchElementException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
namespace com { namespace sun { namespace star { namespace lang { class XComponent; } } } }
#include "com/sun/star/sdbc/SQLException.hdl"
namespace com { namespace sun { namespace star { namespace sdbc { class XConnection; } } } }
namespace com { namespace sun { namespace star { namespace sdbc { class XDataSource; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace application {

class SAL_NO_VTABLE XDatabaseDocumentUI : public ::css::uno::XInterface
{
public:

    // Attributes
    virtual ::css::uno::Reference< ::css::sdbc::XDataSource > SAL_CALL getDataSource() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::XWindow > SAL_CALL getApplicationMainWindow() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::sdbc::XConnection > SAL_CALL getActiveConnection() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::lang::XComponent > > SAL_CALL getSubComponents() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual ::sal_Bool SAL_CALL isConnected() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL connect() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::beans::Pair< ::sal_Int32, ::rtl::OUString > SAL_CALL identifySubComponent( const ::css::uno::Reference< ::css::lang::XComponent >& SubComponent ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL closeSubComponents() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::lang::XComponent > SAL_CALL loadComponent( ::sal_Int32 ObjectType, const ::rtl::OUString& ObjectName, ::sal_Bool ForEditing ) /* throw (::css::lang::IllegalArgumentException, ::css::container::NoSuchElementException, ::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::lang::XComponent > SAL_CALL loadComponentWithArguments( ::sal_Int32 ObjectType, const ::rtl::OUString& ObjectName, ::sal_Bool ForEditing, const ::css::uno::Sequence< ::css::beans::PropertyValue >& Arguments ) /* throw (::css::lang::IllegalArgumentException, ::css::container::NoSuchElementException, ::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::lang::XComponent > SAL_CALL createComponent( ::sal_Int32 ObjectType, ::css::uno::Reference< ::css::lang::XComponent >& DocumentDefinition ) /* throw (::css::lang::IllegalArgumentException, ::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::lang::XComponent > SAL_CALL createComponentWithArguments( ::sal_Int32 ObjectType, const ::css::uno::Sequence< ::css::beans::PropertyValue >& Arguments, ::css::uno::Reference< ::css::lang::XComponent >& DocumentDefinition ) /* throw (::css::lang::IllegalArgumentException, ::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDatabaseDocumentUI() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::application::XDatabaseDocumentUI const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sdb::application::XDatabaseDocumentUI > *) SAL_THROW(());

#endif
