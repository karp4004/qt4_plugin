#ifndef INCLUDED_COM_SUN_STAR_SDB_XALTERQUERY_HDL
#define INCLUDED_COM_SUN_STAR_SDB_XALTERQUERY_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sdbc/SQLException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sdb {

class SAL_NO_VTABLE XAlterQuery : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL alterCommand( const ::rtl::OUString& command, ::sal_Bool useEscapeProcessing ) /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAlterQuery() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::XAlterQuery const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sdb::XAlterQuery > *) SAL_THROW(());

#endif
