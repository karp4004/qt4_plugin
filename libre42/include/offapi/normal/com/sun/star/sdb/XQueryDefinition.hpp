#ifndef INCLUDED_COM_SUN_STAR_SDB_XQUERYDEFINITION_HPP
#define INCLUDED_COM_SUN_STAR_SDB_XQUERYDEFINITION_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sdb/XQueryDefinition.hdl"

#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/ucb/XContent.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::XQueryDefinition const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.sdb.XQueryDefinition" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::sdb::XQueryDefinition > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::sdb::XQueryDefinition > >::get();
}

::css::uno::Type const & ::css::sdb::XQueryDefinition::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::sdb::XQueryDefinition > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SDB_XQUERYDEFINITION_HPP
