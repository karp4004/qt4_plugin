#ifndef INCLUDED_COM_SUN_STAR_SDB_DOCUMENTSAVEREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_SDB_DOCUMENTSAVEREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/DocumentSaveRequest.hdl"

#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/ucb/XContent.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdb {

inline DocumentSaveRequest::DocumentSaveRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , Content()
    , Name()
{ }

inline DocumentSaveRequest::DocumentSaveRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::uno::Reference< ::css::ucb::XContent >& Content_, const ::rtl::OUString& Name_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , Content(Content_)
    , Name(Name_)
{ }

DocumentSaveRequest::DocumentSaveRequest(DocumentSaveRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), Content(the_other.Content), Name(the_other.Name) {}

DocumentSaveRequest::~DocumentSaveRequest() {}

DocumentSaveRequest & DocumentSaveRequest::operator =(DocumentSaveRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    Content = the_other.Content;
    Name = the_other.Name;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::DocumentSaveRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.sdb.DocumentSaveRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::DocumentSaveRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::DocumentSaveRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_DOCUMENTSAVEREQUEST_HPP
