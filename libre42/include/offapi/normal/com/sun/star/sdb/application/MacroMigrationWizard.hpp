#ifndef INCLUDED_COM_SUN_STAR_SDB_APPLICATION_MACROMIGRATIONWIZARD_HPP
#define INCLUDED_COM_SUN_STAR_SDB_APPLICATION_MACROMIGRATIONWIZARD_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/sdb/XOfficeDatabaseDocument.hpp"
#include "com/sun/star/ui/dialogs/XExecutableDialog.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace sdb { namespace application {

class MacroMigrationWizard {
public:
    static ::css::uno::Reference< ::css::ui::dialogs::XExecutableDialog > createWithDocument(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Reference< ::css::sdb::XOfficeDatabaseDocument >& Document) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= Document;
        ::css::uno::Reference< ::css::ui::dialogs::XExecutableDialog > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::ui::dialogs::XExecutableDialog >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.sdb.application.MacroMigrationWizard" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.sdb.application.MacroMigrationWizard of type com.sun.star.ui.dialogs.XExecutableDialog: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.sdb.application.MacroMigrationWizard of type com.sun.star.ui.dialogs.XExecutableDialog" ), the_context);
        }
        return the_instance;
    }

private:
    MacroMigrationWizard(); // not implemented
    MacroMigrationWizard(MacroMigrationWizard &); // not implemented
    ~MacroMigrationWizard(); // not implemented
    void operator =(MacroMigrationWizard); // not implemented
};

} } } } }

#endif // INCLUDED_COM_SUN_STAR_SDB_APPLICATION_MACROMIGRATIONWIZARD_HPP
