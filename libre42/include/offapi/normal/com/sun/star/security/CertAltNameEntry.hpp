#ifndef INCLUDED_COM_SUN_STAR_SECURITY_CERTALTNAMEENTRY_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_CERTALTNAMEENTRY_HPP

#include "sal/config.h"

#include "com/sun/star/security/CertAltNameEntry.hdl"

#include "com/sun/star/security/ExtAltNameType.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace security {

inline CertAltNameEntry::CertAltNameEntry() SAL_THROW(())
    : Type(::css::security::ExtAltNameType_OTHER_NAME)
    , Value()
{
}

inline CertAltNameEntry::CertAltNameEntry(const ::css::security::ExtAltNameType& Type_, const ::css::uno::Any& Value_) SAL_THROW(())
    : Type(Type_)
    , Value(Value_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::CertAltNameEntry const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.security.CertAltNameEntry");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::CertAltNameEntry const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::CertAltNameEntry >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_CERTALTNAMEENTRY_HPP
