#ifndef INCLUDED_COM_SUN_STAR_SECURITY_XCERTIFICATEEXTENSION_HDL
#define INCLUDED_COM_SUN_STAR_SECURITY_XCERTIFICATEEXTENSION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace security {

class SAL_NO_VTABLE XCertificateExtension : public ::css::uno::XInterface
{
public:

    // Attributes
    virtual ::css::uno::Sequence< ::sal_Int8 > SAL_CALL getExtensionId() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int8 > SAL_CALL getExtensionValue() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual ::sal_Bool SAL_CALL isCritical() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XCertificateExtension() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::XCertificateExtension const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::security::XCertificateExtension > *) SAL_THROW(());

#endif
