#ifndef INCLUDED_COM_SUN_STAR_SECURITY_NOPASSWORDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_NOPASSWORDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/security/NoPasswordException.hdl"

#include "com/sun/star/uno/SecurityException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace security {

inline NoPasswordException::NoPasswordException() SAL_THROW(())
    : ::css::uno::SecurityException()
{ }

inline NoPasswordException::NoPasswordException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::SecurityException(Message_, Context_)
{ }

NoPasswordException::NoPasswordException(NoPasswordException const & the_other): ::css::uno::SecurityException(the_other) {}

NoPasswordException::~NoPasswordException() {}

NoPasswordException & NoPasswordException::operator =(NoPasswordException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::SecurityException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::NoPasswordException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.security.NoPasswordException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::NoPasswordException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::NoPasswordException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_NOPASSWORDEXCEPTION_HPP
