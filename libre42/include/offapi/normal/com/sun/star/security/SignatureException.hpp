#ifndef INCLUDED_COM_SUN_STAR_SECURITY_SIGNATUREEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_SIGNATUREEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/security/SignatureException.hdl"

#include "com/sun/star/security/CryptographyException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace security {

inline SignatureException::SignatureException() SAL_THROW(())
    : ::css::security::CryptographyException()
{ }

inline SignatureException::SignatureException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::security::CryptographyException(Message_, Context_)
{ }

SignatureException::SignatureException(SignatureException const & the_other): ::css::security::CryptographyException(the_other) {}

SignatureException::~SignatureException() {}

SignatureException & SignatureException::operator =(SignatureException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::security::CryptographyException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::SignatureException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.security.SignatureException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::SignatureException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::SignatureException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_SIGNATUREEXCEPTION_HPP
