#ifndef INCLUDED_COM_SUN_STAR_SECURITY_DOCUMENTSIGNATUREINFORMATION_HDL
#define INCLUDED_COM_SUN_STAR_SECURITY_DOCUMENTSIGNATUREINFORMATION_HDL

#include "sal/config.h"

namespace com { namespace sun { namespace star { namespace security { class XCertificate; } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace security {

#ifdef SAL_W32
#   pragma pack(push, 8)
#endif

struct DocumentSignatureInformation {
    inline DocumentSignatureInformation() SAL_THROW(());

    inline DocumentSignatureInformation(const ::css::uno::Reference< ::css::security::XCertificate >& Signer_, const ::sal_Int32& SignatureDate_, const ::sal_Int32& SignatureTime_, const ::sal_Bool& SignatureIsValid_, const ::sal_Int32& CertificateStatus_, const ::sal_Bool& PartialDocumentSignature_) SAL_THROW(());

    ::css::uno::Reference< ::css::security::XCertificate > Signer;
    ::sal_Int32 SignatureDate;
    ::sal_Int32 SignatureTime;
    ::sal_Bool SignatureIsValid;
    ::sal_Int32 CertificateStatus;
    ::sal_Bool PartialDocumentSignature;
};

#ifdef SAL_W32
#   pragma pack(pop)
#endif


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::DocumentSignatureInformation const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::security::DocumentSignatureInformation *) SAL_THROW(());

#endif
