#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_INVALIDDNDOPERATIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_INVALIDDNDOPERATIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/dnd/InvalidDNDOperationException.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline InvalidDNDOperationException::InvalidDNDOperationException() SAL_THROW(())
    : ::css::uno::RuntimeException()
{ }

inline InvalidDNDOperationException::InvalidDNDOperationException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
{ }

InvalidDNDOperationException::InvalidDNDOperationException(InvalidDNDOperationException const & the_other): ::css::uno::RuntimeException(the_other) {}

InvalidDNDOperationException::~InvalidDNDOperationException() {}

InvalidDNDOperationException & InvalidDNDOperationException::operator =(InvalidDNDOperationException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::InvalidDNDOperationException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.datatransfer.dnd.InvalidDNDOperationException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::InvalidDNDOperationException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::dnd::InvalidDNDOperationException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_INVALIDDNDOPERATIONEXCEPTION_HPP
