#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGSOURCEDRAGEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGSOURCEDRAGEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/dnd/DragSourceDragEvent.hdl"

#include "com/sun/star/datatransfer/dnd/DragSourceEvent.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline DragSourceDragEvent::DragSourceDragEvent() SAL_THROW(())
    : ::css::datatransfer::dnd::DragSourceEvent()
    , DropAction(0)
    , UserAction(0)
{
}

inline DragSourceDragEvent::DragSourceDragEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::datatransfer::dnd::XDragSourceContext >& DragSourceContext_, const ::css::uno::Reference< ::css::datatransfer::dnd::XDragSource >& DragSource_, const ::sal_Int8& DropAction_, const ::sal_Int8& UserAction_) SAL_THROW(())
    : ::css::datatransfer::dnd::DragSourceEvent(Source_, DragSourceContext_, DragSource_)
    , DropAction(DropAction_)
    , UserAction(UserAction_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DragSourceDragEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.datatransfer.dnd.DragSourceDragEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DragSourceDragEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::dnd::DragSourceDragEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGSOURCEDRAGEVENT_HPP
