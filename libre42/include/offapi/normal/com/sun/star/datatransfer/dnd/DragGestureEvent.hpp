#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGGESTUREEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGGESTUREEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/dnd/DragGestureEvent.hdl"

#include "com/sun/star/datatransfer/dnd/XDragSource.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline DragGestureEvent::DragGestureEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , DragAction(0)
    , DragOriginX(0)
    , DragOriginY(0)
    , DragSource()
    , Event()
{
}

inline DragGestureEvent::DragGestureEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int8& DragAction_, const ::sal_Int32& DragOriginX_, const ::sal_Int32& DragOriginY_, const ::css::uno::Reference< ::css::datatransfer::dnd::XDragSource >& DragSource_, const ::css::uno::Any& Event_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , DragAction(DragAction_)
    , DragOriginX(DragOriginX_)
    , DragOriginY(DragOriginY_)
    , DragSource(DragSource_)
    , Event(Event_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DragGestureEvent const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.datatransfer.dnd.DragGestureEvent");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DragGestureEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::dnd::DragGestureEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGGESTUREEVENT_HPP
