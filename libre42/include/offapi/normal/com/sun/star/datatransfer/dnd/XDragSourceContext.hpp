#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_XDRAGSOURCECONTEXT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_XDRAGSOURCECONTEXT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/datatransfer/dnd/XDragSourceContext.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::XDragSourceContext const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.datatransfer.dnd.XDragSourceContext" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::datatransfer::dnd::XDragSourceContext > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::datatransfer::dnd::XDragSourceContext > >::get();
}

::css::uno::Type const & ::css::datatransfer::dnd::XDragSourceContext::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::datatransfer::dnd::XDragSourceContext > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_XDRAGSOURCECONTEXT_HPP
