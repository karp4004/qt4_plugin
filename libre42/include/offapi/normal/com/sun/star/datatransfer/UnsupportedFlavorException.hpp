#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_UNSUPPORTEDFLAVOREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_UNSUPPORTEDFLAVOREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/UnsupportedFlavorException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace datatransfer {

inline UnsupportedFlavorException::UnsupportedFlavorException() SAL_THROW(())
    : ::css::uno::Exception()
{ }

inline UnsupportedFlavorException::UnsupportedFlavorException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{ }

UnsupportedFlavorException::UnsupportedFlavorException(UnsupportedFlavorException const & the_other): ::css::uno::Exception(the_other) {}

UnsupportedFlavorException::~UnsupportedFlavorException() {}

UnsupportedFlavorException & UnsupportedFlavorException::operator =(UnsupportedFlavorException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace datatransfer {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::UnsupportedFlavorException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.datatransfer.UnsupportedFlavorException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::UnsupportedFlavorException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::UnsupportedFlavorException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_UNSUPPORTEDFLAVOREXCEPTION_HPP
