#ifndef INCLUDED_COM_SUN_STAR_XFORMS_XSUBMISSION_HPP
#define INCLUDED_COM_SUN_STAR_XFORMS_XSUBMISSION_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xforms/XSubmission.hdl"

#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/container/XNamed.hpp"
#include "com/sun/star/form/submission/XSubmission.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xforms {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xforms::XSubmission const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.xforms.XSubmission" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xforms::XSubmission > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xforms::XSubmission > >::get();
}

::css::uno::Type const & ::css::xforms::XSubmission::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xforms::XSubmission > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XFORMS_XSUBMISSION_HPP
