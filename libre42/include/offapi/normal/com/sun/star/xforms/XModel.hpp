#ifndef INCLUDED_COM_SUN_STAR_XFORMS_XMODEL_HPP
#define INCLUDED_COM_SUN_STAR_XFORMS_XMODEL_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xforms/XModel.hdl"

#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/container/XSet.hpp"
#include "com/sun/star/lang/WrappedTargetException.hpp"
#include "com/sun/star/task/XInteractionHandler.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/util/VetoException.hpp"
#include "com/sun/star/xforms/XDataTypeRepository.hpp"
#include "com/sun/star/xforms/XSubmission.hpp"
#include "com/sun/star/xml/dom/XDocument.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xforms {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xforms::XModel const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.xforms.XModel" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xforms::XModel > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xforms::XModel > >::get();
}

::css::uno::Type const & ::css::xforms::XModel::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xforms::XModel > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XFORMS_XMODEL_HPP
