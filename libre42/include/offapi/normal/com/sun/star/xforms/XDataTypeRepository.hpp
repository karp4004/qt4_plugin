#ifndef INCLUDED_COM_SUN_STAR_XFORMS_XDATATYPEREPOSITORY_HPP
#define INCLUDED_COM_SUN_STAR_XFORMS_XDATATYPEREPOSITORY_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xforms/XDataTypeRepository.hdl"

#include "com/sun/star/container/ElementExistException.hpp"
#include "com/sun/star/container/NoSuchElementException.hpp"
#include "com/sun/star/container/XEnumerationAccess.hpp"
#include "com/sun/star/container/XNameAccess.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/util/VetoException.hpp"
#include "com/sun/star/xsd/XDataType.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace xforms {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xforms::XDataTypeRepository const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.xforms.XDataTypeRepository" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xforms::XDataTypeRepository > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xforms::XDataTypeRepository > >::get();
}

::css::uno::Type const & ::css::xforms::XDataTypeRepository::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xforms::XDataTypeRepository > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XFORMS_XDATATYPEREPOSITORY_HPP
