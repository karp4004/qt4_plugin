#ifndef INCLUDED_COM_SUN_STAR_RENDERING_XANIMATEDSPRITE_HDL
#define INCLUDED_COM_SUN_STAR_RENDERING_XANIMATEDSPRITE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/geometry/RealPoint2D.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/rendering/RenderState.hdl"
#include "com/sun/star/rendering/ViewState.hdl"
#include "com/sun/star/rendering/XSprite.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace rendering {

class SAL_NO_VTABLE XAnimatedSprite : public ::css::rendering::XSprite
{
public:

    // Methods
    virtual void SAL_CALL startAnimation( double nSpeed ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL stopAnimation() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL resetAnimation() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL updateAnimation() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setViewState( const ::css::rendering::ViewState& aViewState ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setAll( const ::css::geometry::RealPoint2D& aNewPos, const ::css::rendering::ViewState& aViewState, const ::css::rendering::RenderState& aRenderState, double nAlpha, ::sal_Bool bUpdateAnimation ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAnimatedSprite() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::XAnimatedSprite const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::rendering::XAnimatedSprite > *) SAL_THROW(());

#endif
