#ifndef INCLUDED_COM_SUN_STAR_RENDERING_XANIMATION_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_XANIMATION_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/rendering/XAnimation.hdl"

#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/rendering/AnimationAttributes.hpp"
#include "com/sun/star/rendering/ViewState.hpp"
#include "com/sun/star/rendering/XCanvas.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::XAnimation const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.rendering.XAnimation" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::rendering::XAnimation > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XAnimation > >::get();
}

::css::uno::Type const & ::css::rendering::XAnimation::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::rendering::XAnimation > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_XANIMATION_HPP
