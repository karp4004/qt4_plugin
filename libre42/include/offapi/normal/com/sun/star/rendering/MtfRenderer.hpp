#ifndef INCLUDED_COM_SUN_STAR_RENDERING_MTFRENDERER_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_MTFRENDERER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/rendering/XBitmapCanvas.hpp"
#include "com/sun/star/rendering/XMtfRenderer.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace rendering {

class MtfRenderer {
public:
    static ::css::uno::Reference< ::css::rendering::XMtfRenderer > createWithBitmapCanvas(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Reference< ::css::rendering::XBitmapCanvas >& Canvas) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= Canvas;
        ::css::uno::Reference< ::css::rendering::XMtfRenderer > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::rendering::XMtfRenderer >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.rendering.MtfRenderer" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.rendering.MtfRenderer of type com.sun.star.rendering.XMtfRenderer: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.rendering.MtfRenderer of type com.sun.star.rendering.XMtfRenderer" ), the_context);
        }
        return the_instance;
    }

private:
    MtfRenderer(); // not implemented
    MtfRenderer(MtfRenderer &); // not implemented
    ~MtfRenderer(); // not implemented
    void operator =(MtfRenderer); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_RENDERING_MTFRENDERER_HPP
