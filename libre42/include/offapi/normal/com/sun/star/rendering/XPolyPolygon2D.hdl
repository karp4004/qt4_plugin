#ifndef INCLUDED_COM_SUN_STAR_RENDERING_XPOLYPOLYGON2D_HDL
#define INCLUDED_COM_SUN_STAR_RENDERING_XPOLYPOLYGON2D_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/geometry/RealPoint2D.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/IndexOutOfBoundsException.hdl"
#include "com/sun/star/rendering/FillRule.hdl"
namespace com { namespace sun { namespace star { namespace rendering { class XPolyPolygon2D; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace rendering {

class SAL_NO_VTABLE XPolyPolygon2D : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addPolyPolygon( const ::css::geometry::RealPoint2D& position, const ::css::uno::Reference< ::css::rendering::XPolyPolygon2D >& polyPolygon ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getNumberOfPolygons() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getNumberOfPolygonPoints( ::sal_Int32 polygon ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::rendering::FillRule SAL_CALL getFillRule() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setFillRule( ::css::rendering::FillRule fillRule ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isClosed( ::sal_Int32 index ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setClosed( ::sal_Int32 index, ::sal_Bool closedState ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPolyPolygon2D() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::XPolyPolygon2D const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::rendering::XPolyPolygon2D > *) SAL_THROW(());

#endif
