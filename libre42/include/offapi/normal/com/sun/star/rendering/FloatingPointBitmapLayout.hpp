#ifndef INCLUDED_COM_SUN_STAR_RENDERING_FLOATINGPOINTBITMAPLAYOUT_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_FLOATINGPOINTBITMAPLAYOUT_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/FloatingPointBitmapLayout.hdl"

#include "com/sun/star/rendering/XColorSpace.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline FloatingPointBitmapLayout::FloatingPointBitmapLayout() SAL_THROW(())
    : ScanLines(0)
    , ScanLineBytes(0)
    , ScanLineStride(0)
    , PlaneStride(0)
    , ColorSpace()
    , NumComponents(0)
    , Endianness(0)
    , Format(0)
{
}

inline FloatingPointBitmapLayout::FloatingPointBitmapLayout(const ::sal_Int32& ScanLines_, const ::sal_Int32& ScanLineBytes_, const ::sal_Int32& ScanLineStride_, const ::sal_Int32& PlaneStride_, const ::css::uno::Reference< ::css::rendering::XColorSpace >& ColorSpace_, const ::sal_Int32& NumComponents_, const ::sal_Int8& Endianness_, const ::sal_Int8& Format_) SAL_THROW(())
    : ScanLines(ScanLines_)
    , ScanLineBytes(ScanLineBytes_)
    , ScanLineStride(ScanLineStride_)
    , PlaneStride(PlaneStride_)
    , ColorSpace(ColorSpace_)
    , NumComponents(NumComponents_)
    , Endianness(Endianness_)
    , Format(Format_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::FloatingPointBitmapLayout const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.rendering.FloatingPointBitmapLayout");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::FloatingPointBitmapLayout const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::FloatingPointBitmapLayout >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_FLOATINGPOINTBITMAPLAYOUT_HPP
