#ifndef INCLUDED_COM_SUN_STAR_RENDERING_ANIMATIONATTRIBUTES_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_ANIMATIONATTRIBUTES_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/AnimationAttributes.hdl"

#include "com/sun/star/geometry/RealSize2D.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline AnimationAttributes::AnimationAttributes() SAL_THROW(())
    : Duration(0)
    , RepeatMode(0)
    , UntransformedSize()
{
}

inline AnimationAttributes::AnimationAttributes(const double& Duration_, const ::sal_Int8& RepeatMode_, const ::css::geometry::RealSize2D& UntransformedSize_) SAL_THROW(())
    : Duration(Duration_)
    , RepeatMode(RepeatMode_)
    , UntransformedSize(UntransformedSize_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::AnimationAttributes const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.rendering.AnimationAttributes");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::AnimationAttributes const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::AnimationAttributes >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_ANIMATIONATTRIBUTES_HPP
