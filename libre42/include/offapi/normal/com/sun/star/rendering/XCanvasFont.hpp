#ifndef INCLUDED_COM_SUN_STAR_RENDERING_XCANVASFONT_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_XCANVASFONT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/rendering/XCanvasFont.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/rendering/FontMetrics.hpp"
#include "com/sun/star/rendering/FontRequest.hpp"
#include "com/sun/star/rendering/StringContext.hpp"
#include "com/sun/star/rendering/XTextLayout.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::XCanvasFont const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.rendering.XCanvasFont" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::rendering::XCanvasFont > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XCanvasFont > >::get();
}

::css::uno::Type const & ::css::rendering::XCanvasFont::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::rendering::XCanvasFont > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_XCANVASFONT_HPP
