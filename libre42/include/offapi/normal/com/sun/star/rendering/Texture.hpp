#ifndef INCLUDED_COM_SUN_STAR_RENDERING_TEXTURE_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_TEXTURE_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/Texture.hdl"

#include "com/sun/star/geometry/AffineMatrix2D.hpp"
#include "com/sun/star/rendering/StrokeAttributes.hpp"
#include "com/sun/star/rendering/XBitmap.hpp"
#include "com/sun/star/rendering/XParametricPolyPolygon2D.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline Texture::Texture() SAL_THROW(())
    : AffineTransform()
    , Alpha(0)
    , NumberOfHatchPolygons(0)
    , Bitmap()
    , Gradient()
    , Hatching()
    , HatchAttributes()
    , RepeatModeX(0)
    , RepeatModeY(0)
{
}

inline Texture::Texture(const ::css::geometry::AffineMatrix2D& AffineTransform_, const double& Alpha_, const ::sal_Int32& NumberOfHatchPolygons_, const ::css::uno::Reference< ::css::rendering::XBitmap >& Bitmap_, const ::css::uno::Reference< ::css::rendering::XParametricPolyPolygon2D >& Gradient_, const ::css::uno::Reference< ::css::rendering::XParametricPolyPolygon2D >& Hatching_, const ::css::rendering::StrokeAttributes& HatchAttributes_, const ::sal_Int8& RepeatModeX_, const ::sal_Int8& RepeatModeY_) SAL_THROW(())
    : AffineTransform(AffineTransform_)
    , Alpha(Alpha_)
    , NumberOfHatchPolygons(NumberOfHatchPolygons_)
    , Bitmap(Bitmap_)
    , Gradient(Gradient_)
    , Hatching(Hatching_)
    , HatchAttributes(HatchAttributes_)
    , RepeatModeX(RepeatModeX_)
    , RepeatModeY(RepeatModeY_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::Texture const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.rendering.Texture");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::Texture const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::Texture >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_TEXTURE_HPP
