#ifndef INCLUDED_COM_SUN_STAR_RENDERING_XSPRITECANVAS_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_XSPRITECANVAS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/rendering/XSpriteCanvas.hdl"

#include "com/sun/star/geometry/RealSize2D.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/rendering/VolatileContentDestroyedException.hpp"
#include "com/sun/star/rendering/XAnimatedSprite.hpp"
#include "com/sun/star/rendering/XAnimation.hpp"
#include "com/sun/star/rendering/XBitmap.hpp"
#include "com/sun/star/rendering/XCanvas.hpp"
#include "com/sun/star/rendering/XCustomSprite.hpp"
#include "com/sun/star/rendering/XSprite.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::XSpriteCanvas const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.rendering.XSpriteCanvas" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::rendering::XSpriteCanvas > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XSpriteCanvas > >::get();
}

::css::uno::Type const & ::css::rendering::XSpriteCanvas::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::rendering::XSpriteCanvas > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_XSPRITECANVAS_HPP
