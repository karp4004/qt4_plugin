#ifndef INCLUDED_COM_SUN_STAR_RENDERING_STROKEATTRIBUTES_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_STROKEATTRIBUTES_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/StrokeAttributes.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline StrokeAttributes::StrokeAttributes() SAL_THROW(())
    : StrokeWidth(0)
    , MiterLimit(0)
    , DashArray()
    , LineArray()
    , StartCapType(0)
    , EndCapType(0)
    , JoinType(0)
{
}

inline StrokeAttributes::StrokeAttributes(const double& StrokeWidth_, const double& MiterLimit_, const ::css::uno::Sequence< double >& DashArray_, const ::css::uno::Sequence< double >& LineArray_, const ::sal_Int8& StartCapType_, const ::sal_Int8& EndCapType_, const ::sal_Int8& JoinType_) SAL_THROW(())
    : StrokeWidth(StrokeWidth_)
    , MiterLimit(MiterLimit_)
    , DashArray(DashArray_)
    , LineArray(LineArray_)
    , StartCapType(StartCapType_)
    , EndCapType(EndCapType_)
    , JoinType(JoinType_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::StrokeAttributes const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.rendering.StrokeAttributes");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::StrokeAttributes const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::StrokeAttributes >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_STROKEATTRIBUTES_HPP
