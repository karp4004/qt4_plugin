#ifndef INCLUDED_COM_SUN_STAR_RENDERING_STRINGCONTEXT_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_STRINGCONTEXT_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/StringContext.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline StringContext::StringContext() SAL_THROW(())
    : Text()
    , StartPosition(0)
    , Length(0)
{
}

inline StringContext::StringContext(const ::rtl::OUString& Text_, const ::sal_Int32& StartPosition_, const ::sal_Int32& Length_) SAL_THROW(())
    : Text(Text_)
    , StartPosition(StartPosition_)
    , Length(Length_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::StringContext const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.rendering.StringContext");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::StringContext const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::StringContext >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_STRINGCONTEXT_HPP
