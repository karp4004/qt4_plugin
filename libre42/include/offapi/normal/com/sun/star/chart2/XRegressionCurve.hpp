#ifndef INCLUDED_COM_SUN_STAR_CHART2_XREGRESSIONCURVE_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_XREGRESSIONCURVE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/chart2/XRegressionCurve.hdl"

#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/chart2/XRegressionCurveCalculator.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::XRegressionCurve const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.chart2.XRegressionCurve" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::chart2::XRegressionCurve > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::chart2::XRegressionCurve > >::get();
}

::css::uno::Type const & ::css::chart2::XRegressionCurve::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::chart2::XRegressionCurve > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_XREGRESSIONCURVE_HPP
