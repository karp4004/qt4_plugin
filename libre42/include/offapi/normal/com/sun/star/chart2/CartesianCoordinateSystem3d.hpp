#ifndef INCLUDED_COM_SUN_STAR_CHART2_CARTESIANCOORDINATESYSTEM3D_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_CARTESIANCOORDINATESYSTEM3D_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/chart2/XCoordinateSystem.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace chart2 {

class CartesianCoordinateSystem3d {
public:
    static ::css::uno::Reference< ::css::chart2::XCoordinateSystem > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::chart2::XCoordinateSystem > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::chart2::XCoordinateSystem >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.chart2.CartesianCoordinateSystem3d" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.chart2.CartesianCoordinateSystem3d of type com.sun.star.chart2.XCoordinateSystem: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.chart2.CartesianCoordinateSystem3d of type com.sun.star.chart2.XCoordinateSystem" ), the_context);
        }
        return the_instance;
    }

private:
    CartesianCoordinateSystem3d(); // not implemented
    CartesianCoordinateSystem3d(CartesianCoordinateSystem3d &); // not implemented
    ~CartesianCoordinateSystem3d(); // not implemented
    void operator =(CartesianCoordinateSystem3d); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_CHART2_CARTESIANCOORDINATESYSTEM3D_HPP
