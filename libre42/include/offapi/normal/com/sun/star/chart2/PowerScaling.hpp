#ifndef INCLUDED_COM_SUN_STAR_CHART2_POWERSCALING_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_POWERSCALING_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/chart2/XScaling.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace chart2 {

class PowerScaling {
public:
    static ::css::uno::Reference< ::css::chart2::XScaling > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::chart2::XScaling > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::chart2::XScaling >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.chart2.PowerScaling" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.chart2.PowerScaling of type com.sun.star.chart2.XScaling: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.chart2.PowerScaling of type com.sun.star.chart2.XScaling" ), the_context);
        }
        return the_instance;
    }

private:
    PowerScaling(); // not implemented
    PowerScaling(PowerScaling &); // not implemented
    ~PowerScaling(); // not implemented
    void operator =(PowerScaling); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_CHART2_POWERSCALING_HPP
