#ifndef INCLUDED_COM_SUN_STAR_CHART2_XTRANSFORMATION_HDL
#define INCLUDED_COM_SUN_STAR_CHART2_XTRANSFORMATION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

class SAL_NO_VTABLE XTransformation : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< double > SAL_CALL transform( const ::css::uno::Sequence< double >& aValues ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getSourceDimension() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getTargetDimension() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTransformation() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::XTransformation const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart2::XTransformation > *) SAL_THROW(());

#endif
