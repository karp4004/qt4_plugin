#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATASEQUENCE_HDL
#define INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATASEQUENCE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/chart2/data/LabelOrigin.hdl"
#include "com/sun/star/lang/IndexOutOfBoundsException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

class SAL_NO_VTABLE XDataSequence : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::uno::Any > SAL_CALL getData() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getSourceRangeRepresentation() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL generateLabel( ::css::chart2::data::LabelOrigin eLabelOrigin ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getNumberFormatKeyByIndex( ::sal_Int32 nIndex ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDataSequence() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::data::XDataSequence const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart2::data::XDataSequence > *) SAL_THROW(());

#endif
