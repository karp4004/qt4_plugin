#ifndef INCLUDED_COM_SUN_STAR_CHART2_XAXIS_HDL
#define INCLUDED_COM_SUN_STAR_CHART2_XAXIS_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace beans { class XPropertySet; } } } }
#include "com/sun/star/chart2/ScaleData.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

class SAL_NO_VTABLE XAxis : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setScaleData( const ::css::chart2::ScaleData& aScale ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::chart2::ScaleData SAL_CALL getScaleData() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::beans::XPropertySet > SAL_CALL getGridProperties() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::beans::XPropertySet > > SAL_CALL getSubGridProperties() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::beans::XPropertySet > > SAL_CALL getSubTickProperties() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAxis() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::XAxis const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart2::XAxis > *) SAL_THROW(());

#endif
