#ifndef INCLUDED_COM_SUN_STAR_CHART2_XDATASERIES_HDL
#define INCLUDED_COM_SUN_STAR_CHART2_XDATASERIES_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace beans { class XPropertySet; } } } }
#include "com/sun/star/lang/IndexOutOfBoundsException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

class SAL_NO_VTABLE XDataSeries : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::beans::XPropertySet > SAL_CALL getDataPointByIndex( ::sal_Int32 nIndex ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL resetDataPoint( ::sal_Int32 nIndex ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL resetAllDataPoints() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDataSeries() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::XDataSeries const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart2::XDataSeries > *) SAL_THROW(());

#endif
