#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATAPROVIDER_HDL
#define INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATAPROVIDER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
namespace com { namespace sun { namespace star { namespace chart2 { namespace data { class XDataSequence; } } } } }
namespace com { namespace sun { namespace star { namespace chart2 { namespace data { class XDataSource; } } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
namespace com { namespace sun { namespace star { namespace sheet { class XRangeSelection; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

class SAL_NO_VTABLE XDataProvider : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL createDataSourcePossible( const ::css::uno::Sequence< ::css::beans::PropertyValue >& aArguments ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::chart2::data::XDataSource > SAL_CALL createDataSource( const ::css::uno::Sequence< ::css::beans::PropertyValue >& aArguments ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::beans::PropertyValue > SAL_CALL detectArguments( const ::css::uno::Reference< ::css::chart2::data::XDataSource >& xDataSource ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL createDataSequenceByRangeRepresentationPossible( const ::rtl::OUString& aRangeRepresentation ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::chart2::data::XDataSequence > SAL_CALL createDataSequenceByRangeRepresentation( const ::rtl::OUString& aRangeRepresentation ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::sheet::XRangeSelection > SAL_CALL getRangeSelection() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDataProvider() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::data::XDataProvider const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart2::data::XDataProvider > *) SAL_THROW(());

#endif
