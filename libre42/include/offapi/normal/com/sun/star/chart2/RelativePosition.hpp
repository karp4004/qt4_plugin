#ifndef INCLUDED_COM_SUN_STAR_CHART2_RELATIVEPOSITION_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_RELATIVEPOSITION_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/RelativePosition.hdl"

#include "com/sun/star/drawing/Alignment.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline RelativePosition::RelativePosition() SAL_THROW(())
    : Primary(0)
    , Secondary(0)
    , Anchor(::css::drawing::Alignment_TOP_LEFT)
{
}

inline RelativePosition::RelativePosition(const double& Primary_, const double& Secondary_, const ::css::drawing::Alignment& Anchor_) SAL_THROW(())
    : Primary(Primary_)
    , Secondary(Secondary_)
    , Anchor(Anchor_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::RelativePosition const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.chart2.RelativePosition");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::RelativePosition const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::RelativePosition >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_RELATIVEPOSITION_HPP
