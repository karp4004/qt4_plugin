#ifndef INCLUDED_COM_SUN_STAR_MEDIA_XMANAGER_HDL
#define INCLUDED_COM_SUN_STAR_MEDIA_XMANAGER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace media { class XPlayer; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace media {

class SAL_NO_VTABLE XManager : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::media::XPlayer > SAL_CALL createPlayer( const ::rtl::OUString& aURL ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XManager() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::media::XManager const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::media::XManager > *) SAL_THROW(());

#endif
