#ifndef INCLUDED_COM_SUN_STAR_TABLE_XAUTOFORMATTABLE_HDL
#define INCLUDED_COM_SUN_STAR_TABLE_XAUTOFORMATTABLE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace table {

class SAL_NO_VTABLE XAutoFormattable : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL autoFormat( const ::rtl::OUString& aName ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAutoFormattable() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::XAutoFormattable const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::table::XAutoFormattable > *) SAL_THROW(());

#endif
