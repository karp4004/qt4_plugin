#ifndef INCLUDED_COM_SUN_STAR_TABLE_XTABLECOLUMNS_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_XTABLECOLUMNS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/table/XTableColumns.hdl"

#include "com/sun/star/container/XIndexAccess.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::XTableColumns const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.table.XTableColumns" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::table::XTableColumns > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::table::XTableColumns > >::get();
}

::css::uno::Type const & ::css::table::XTableColumns::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::table::XTableColumns > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_XTABLECOLUMNS_HPP
