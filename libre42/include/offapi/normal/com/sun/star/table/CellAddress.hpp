#ifndef INCLUDED_COM_SUN_STAR_TABLE_CELLADDRESS_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_CELLADDRESS_HPP

#include "sal/config.h"

#include "com/sun/star/table/CellAddress.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline CellAddress::CellAddress() SAL_THROW(())
    : Sheet(0)
    , Column(0)
    , Row(0)
{
}

inline CellAddress::CellAddress(const ::sal_Int16& Sheet_, const ::sal_Int32& Column_, const ::sal_Int32& Row_) SAL_THROW(())
    : Sheet(Sheet_)
    , Column(Column_)
    , Row(Row_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::CellAddress const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.table.CellAddress");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::CellAddress const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::CellAddress >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_CELLADDRESS_HPP
