#ifndef INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDER2_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDER2_HPP

#include "sal/config.h"

#include "com/sun/star/table/TableBorder2.hdl"

#include "com/sun/star/table/BorderLine2.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline TableBorder2::TableBorder2() SAL_THROW(())
    : TopLine()
    , IsTopLineValid(false)
    , BottomLine()
    , IsBottomLineValid(false)
    , LeftLine()
    , IsLeftLineValid(false)
    , RightLine()
    , IsRightLineValid(false)
    , HorizontalLine()
    , IsHorizontalLineValid(false)
    , VerticalLine()
    , IsVerticalLineValid(false)
    , Distance(0)
    , IsDistanceValid(false)
{
}

inline TableBorder2::TableBorder2(const ::css::table::BorderLine2& TopLine_, const ::sal_Bool& IsTopLineValid_, const ::css::table::BorderLine2& BottomLine_, const ::sal_Bool& IsBottomLineValid_, const ::css::table::BorderLine2& LeftLine_, const ::sal_Bool& IsLeftLineValid_, const ::css::table::BorderLine2& RightLine_, const ::sal_Bool& IsRightLineValid_, const ::css::table::BorderLine2& HorizontalLine_, const ::sal_Bool& IsHorizontalLineValid_, const ::css::table::BorderLine2& VerticalLine_, const ::sal_Bool& IsVerticalLineValid_, const ::sal_Int16& Distance_, const ::sal_Bool& IsDistanceValid_) SAL_THROW(())
    : TopLine(TopLine_)
    , IsTopLineValid(IsTopLineValid_)
    , BottomLine(BottomLine_)
    , IsBottomLineValid(IsBottomLineValid_)
    , LeftLine(LeftLine_)
    , IsLeftLineValid(IsLeftLineValid_)
    , RightLine(RightLine_)
    , IsRightLineValid(IsRightLineValid_)
    , HorizontalLine(HorizontalLine_)
    , IsHorizontalLineValid(IsHorizontalLineValid_)
    , VerticalLine(VerticalLine_)
    , IsVerticalLineValid(IsVerticalLineValid_)
    , Distance(Distance_)
    , IsDistanceValid(IsDistanceValid_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::TableBorder2 const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.table.TableBorder2");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::TableBorder2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::TableBorder2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDER2_HPP
