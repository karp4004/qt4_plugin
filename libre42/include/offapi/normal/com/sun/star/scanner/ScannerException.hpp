#ifndef INCLUDED_COM_SUN_STAR_SCANNER_SCANNEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SCANNER_SCANNEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/scanner/ScannerException.hdl"

#include "com/sun/star/scanner/ScanError.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace scanner {

inline ScannerException::ScannerException() SAL_THROW(())
    : ::css::uno::Exception()
    , Error(::css::scanner::ScanError_ScanErrorNone)
{ }

inline ScannerException::ScannerException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::scanner::ScanError& Error_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Error(Error_)
{ }

ScannerException::ScannerException(ScannerException const & the_other): ::css::uno::Exception(the_other), Error(the_other.Error) {}

ScannerException::~ScannerException() {}

ScannerException & ScannerException::operator =(ScannerException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Error = the_other.Error;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace scanner {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::scanner::ScannerException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.scanner.ScannerException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::scanner::ScannerException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::scanner::ScannerException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCANNER_SCANNEREXCEPTION_HPP
