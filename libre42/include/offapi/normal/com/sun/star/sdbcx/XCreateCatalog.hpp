#ifndef INCLUDED_COM_SUN_STAR_SDBCX_XCREATECATALOG_HPP
#define INCLUDED_COM_SUN_STAR_SDBCX_XCREATECATALOG_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sdbcx/XCreateCatalog.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/container/ElementExistException.hpp"
#include "com/sun/star/sdbc/SQLException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdbcx {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdbcx::XCreateCatalog const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_INTERFACE, "com.sun.star.sdbcx.XCreateCatalog" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::sdbcx::XCreateCatalog > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::sdbcx::XCreateCatalog > >::get();
}

::css::uno::Type const & ::css::sdbcx::XCreateCatalog::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::sdbcx::XCreateCatalog > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SDBCX_XCREATECATALOG_HPP
