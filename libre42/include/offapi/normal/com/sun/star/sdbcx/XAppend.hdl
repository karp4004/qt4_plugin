#ifndef INCLUDED_COM_SUN_STAR_SDBCX_XAPPEND_HDL
#define INCLUDED_COM_SUN_STAR_SDBCX_XAPPEND_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace beans { class XPropertySet; } } } }
#include "com/sun/star/container/ElementExistException.hdl"
#include "com/sun/star/sdbc/SQLException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sdbcx {

class SAL_NO_VTABLE XAppend : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL appendByDescriptor( const ::css::uno::Reference< ::css::beans::XPropertySet >& descriptor ) /* throw (::css::sdbc::SQLException, ::css::container::ElementExistException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAppend() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdbcx::XAppend const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sdbcx::XAppend > *) SAL_THROW(());

#endif
