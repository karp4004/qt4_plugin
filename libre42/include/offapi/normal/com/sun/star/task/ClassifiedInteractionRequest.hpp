#ifndef INCLUDED_COM_SUN_STAR_TASK_CLASSIFIEDINTERACTIONREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_CLASSIFIEDINTERACTIONREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/ClassifiedInteractionRequest.hdl"

#include "com/sun/star/task/InteractionClassification.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline ClassifiedInteractionRequest::ClassifiedInteractionRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , Classification(::css::task::InteractionClassification_ERROR)
{ }

inline ClassifiedInteractionRequest::ClassifiedInteractionRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Classification(Classification_)
{ }

ClassifiedInteractionRequest::ClassifiedInteractionRequest(ClassifiedInteractionRequest const & the_other): ::css::uno::Exception(the_other), Classification(the_other.Classification) {}

ClassifiedInteractionRequest::~ClassifiedInteractionRequest() {}

ClassifiedInteractionRequest & ClassifiedInteractionRequest::operator =(ClassifiedInteractionRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Classification = the_other.Classification;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::ClassifiedInteractionRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.task.ClassifiedInteractionRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::ClassifiedInteractionRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_CLASSIFIEDINTERACTIONREQUEST_HPP
