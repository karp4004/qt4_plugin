#ifndef INCLUDED_COM_SUN_STAR_TASK_DOCUMENTPASSWORDREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_DOCUMENTPASSWORDREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/DocumentPasswordRequest.hdl"

#include "com/sun/star/task/PasswordRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline DocumentPasswordRequest::DocumentPasswordRequest() SAL_THROW(())
    : ::css::task::PasswordRequest()
    , Name()
{ }

inline DocumentPasswordRequest::DocumentPasswordRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::task::PasswordRequestMode& Mode_, const ::rtl::OUString& Name_) SAL_THROW(())
    : ::css::task::PasswordRequest(Message_, Context_, Classification_, Mode_)
    , Name(Name_)
{ }

DocumentPasswordRequest::DocumentPasswordRequest(DocumentPasswordRequest const & the_other): ::css::task::PasswordRequest(the_other), Name(the_other.Name) {}

DocumentPasswordRequest::~DocumentPasswordRequest() {}

DocumentPasswordRequest & DocumentPasswordRequest::operator =(DocumentPasswordRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::PasswordRequest::operator =(the_other);
    Name = the_other.Name;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::DocumentPasswordRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.task.DocumentPasswordRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::DocumentPasswordRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::DocumentPasswordRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_DOCUMENTPASSWORDREQUEST_HPP
