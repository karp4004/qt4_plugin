#ifndef INCLUDED_COM_SUN_STAR_TASK_ERRORCODEIOEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_TASK_ERRORCODEIOEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/task/ErrorCodeIOException.hdl"

#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace task {

inline ErrorCodeIOException::ErrorCodeIOException() SAL_THROW(())
    : ::css::io::IOException()
    , ErrCode(0)
{ }

inline ErrorCodeIOException::ErrorCodeIOException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& ErrCode_) SAL_THROW(())
    : ::css::io::IOException(Message_, Context_)
    , ErrCode(ErrCode_)
{ }

ErrorCodeIOException::ErrorCodeIOException(ErrorCodeIOException const & the_other): ::css::io::IOException(the_other), ErrCode(the_other.ErrCode) {}

ErrorCodeIOException::~ErrorCodeIOException() {}

ErrorCodeIOException & ErrorCodeIOException::operator =(ErrorCodeIOException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::io::IOException::operator =(the_other);
    ErrCode = the_other.ErrCode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::ErrorCodeIOException const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.task.ErrorCodeIOException" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::ErrorCodeIOException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::ErrorCodeIOException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_ERRORCODEIOEXCEPTION_HPP
