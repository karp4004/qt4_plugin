#ifndef INCLUDED_COM_SUN_STAR_TASK_FUTUREDOCUMENTVERSIONPRODUCTUPDATEREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_FUTUREDOCUMENTVERSIONPRODUCTUPDATEREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/FutureDocumentVersionProductUpdateRequest.hdl"

#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline FutureDocumentVersionProductUpdateRequest::FutureDocumentVersionProductUpdateRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , DocumentURL()
    , DocumentODFVersion()
{ }

inline FutureDocumentVersionProductUpdateRequest::FutureDocumentVersionProductUpdateRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& DocumentURL_, const ::rtl::OUString& DocumentODFVersion_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , DocumentURL(DocumentURL_)
    , DocumentODFVersion(DocumentODFVersion_)
{ }

FutureDocumentVersionProductUpdateRequest::FutureDocumentVersionProductUpdateRequest(FutureDocumentVersionProductUpdateRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), DocumentURL(the_other.DocumentURL), DocumentODFVersion(the_other.DocumentODFVersion) {}

FutureDocumentVersionProductUpdateRequest::~FutureDocumentVersionProductUpdateRequest() {}

FutureDocumentVersionProductUpdateRequest & FutureDocumentVersionProductUpdateRequest::operator =(FutureDocumentVersionProductUpdateRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    DocumentURL = the_other.DocumentURL;
    DocumentODFVersion = the_other.DocumentODFVersion;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::FutureDocumentVersionProductUpdateRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.task.FutureDocumentVersionProductUpdateRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::FutureDocumentVersionProductUpdateRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::FutureDocumentVersionProductUpdateRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_FUTUREDOCUMENTVERSIONPRODUCTUPDATEREQUEST_HPP
