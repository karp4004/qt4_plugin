#ifndef INCLUDED_COM_SUN_STAR_TASK_USERRECORD_HPP
#define INCLUDED_COM_SUN_STAR_TASK_USERRECORD_HPP

#include "sal/config.h"

#include "com/sun/star/task/UserRecord.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace task {

inline UserRecord::UserRecord() SAL_THROW(())
    : UserName()
    , Passwords()
{
}

inline UserRecord::UserRecord(const ::rtl::OUString& UserName_, const ::css::uno::Sequence< ::rtl::OUString >& Passwords_) SAL_THROW(())
    : UserName(UserName_)
    , Passwords(Passwords_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::UserRecord const *) {
    //TODO: On certain platforms with weak memory models, the following code can result in some threads observing that the_type points to garbage
    static ::typelib_TypeDescriptionReference * the_type = 0;
    if (the_type == 0) {
        ::typelib_static_type_init(&the_type, typelib_TypeClass_STRUCT, "com.sun.star.task.UserRecord");
    }
    return *reinterpret_cast< ::css::uno::Type * >(&the_type);
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::UserRecord const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::UserRecord >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_USERRECORD_HPP
