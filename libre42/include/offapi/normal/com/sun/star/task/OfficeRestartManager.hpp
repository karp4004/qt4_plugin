#ifndef INCLUDED_COM_SUN_STAR_TASK_OFFICERESTARTMANAGER_HPP
#define INCLUDED_COM_SUN_STAR_TASK_OFFICERESTARTMANAGER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/task/XRestartManager.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace task {

class OfficeRestartManager {
public:
    static ::css::uno::Reference< ::css::task::XRestartManager > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::task::XRestartManager > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.task.OfficeRestartManager" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.task.OfficeRestartManager of type com.sun.star.task.XRestartManager" ), the_context);
        }
        return instance;
    }

private:
    OfficeRestartManager(); // not implemented
    OfficeRestartManager(OfficeRestartManager &); // not implemented
    ~OfficeRestartManager(); // not implemented
    void operator =(OfficeRestartManager); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_TASK_OFFICERESTARTMANAGER_HPP
