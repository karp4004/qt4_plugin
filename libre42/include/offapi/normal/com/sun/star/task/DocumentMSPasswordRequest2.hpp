#ifndef INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMSPASSWORDREQUEST2_HPP
#define INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMSPASSWORDREQUEST2_HPP

#include "sal/config.h"

#include "com/sun/star/task/DocumentMSPasswordRequest2.hdl"

#include "com/sun/star/task/DocumentMSPasswordRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace task {

inline DocumentMSPasswordRequest2::DocumentMSPasswordRequest2() SAL_THROW(())
    : ::css::task::DocumentMSPasswordRequest()
    , IsRequestPasswordToModify(false)
{ }

inline DocumentMSPasswordRequest2::DocumentMSPasswordRequest2(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::task::PasswordRequestMode& Mode_, const ::rtl::OUString& Name_, const ::sal_Bool& IsRequestPasswordToModify_) SAL_THROW(())
    : ::css::task::DocumentMSPasswordRequest(Message_, Context_, Classification_, Mode_, Name_)
    , IsRequestPasswordToModify(IsRequestPasswordToModify_)
{ }

DocumentMSPasswordRequest2::DocumentMSPasswordRequest2(DocumentMSPasswordRequest2 const & the_other): ::css::task::DocumentMSPasswordRequest(the_other), IsRequestPasswordToModify(the_other.IsRequestPasswordToModify) {}

DocumentMSPasswordRequest2::~DocumentMSPasswordRequest2() {}

DocumentMSPasswordRequest2 & DocumentMSPasswordRequest2::operator =(DocumentMSPasswordRequest2 const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::DocumentMSPasswordRequest::operator =(the_other);
    IsRequestPasswordToModify = the_other.IsRequestPasswordToModify;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::DocumentMSPasswordRequest2 const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.task.DocumentMSPasswordRequest2" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::DocumentMSPasswordRequest2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::DocumentMSPasswordRequest2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMSPASSWORDREQUEST2_HPP
