#ifndef INCLUDED_COM_SUN_STAR_TASK_UNSUPPORTEDOVERWRITEREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_UNSUPPORTEDOVERWRITEREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/UnsupportedOverwriteRequest.hdl"

#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline UnsupportedOverwriteRequest::UnsupportedOverwriteRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , Name()
{ }

inline UnsupportedOverwriteRequest::UnsupportedOverwriteRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& Name_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , Name(Name_)
{ }

UnsupportedOverwriteRequest::UnsupportedOverwriteRequest(UnsupportedOverwriteRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), Name(the_other.Name) {}

UnsupportedOverwriteRequest::~UnsupportedOverwriteRequest() {}

UnsupportedOverwriteRequest & UnsupportedOverwriteRequest::operator =(UnsupportedOverwriteRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    Name = the_other.Name;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::UnsupportedOverwriteRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.task.UnsupportedOverwriteRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::UnsupportedOverwriteRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::UnsupportedOverwriteRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_UNSUPPORTEDOVERWRITEREQUEST_HPP
