#ifndef INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMACROCONFIRMATIONREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMACROCONFIRMATIONREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/DocumentMacroConfirmationRequest.hdl"

#include "com/sun/star/embed/XStorage.hpp"
#include "com/sun/star/security/DocumentSignatureInformation.hpp"
#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline DocumentMacroConfirmationRequest::DocumentMacroConfirmationRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , DocumentURL()
    , DocumentStorage()
    , DocumentVersion()
    , DocumentSignatureInformation()
{ }

inline DocumentMacroConfirmationRequest::DocumentMacroConfirmationRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& DocumentURL_, const ::css::uno::Reference< ::css::embed::XStorage >& DocumentStorage_, const ::rtl::OUString& DocumentVersion_, const ::css::uno::Sequence< ::css::security::DocumentSignatureInformation >& DocumentSignatureInformation_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , DocumentURL(DocumentURL_)
    , DocumentStorage(DocumentStorage_)
    , DocumentVersion(DocumentVersion_)
    , DocumentSignatureInformation(DocumentSignatureInformation_)
{ }

DocumentMacroConfirmationRequest::DocumentMacroConfirmationRequest(DocumentMacroConfirmationRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), DocumentURL(the_other.DocumentURL), DocumentStorage(the_other.DocumentStorage), DocumentVersion(the_other.DocumentVersion), DocumentSignatureInformation(the_other.DocumentSignatureInformation) {}

DocumentMacroConfirmationRequest::~DocumentMacroConfirmationRequest() {}

DocumentMacroConfirmationRequest & DocumentMacroConfirmationRequest::operator =(DocumentMacroConfirmationRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    DocumentURL = the_other.DocumentURL;
    DocumentStorage = the_other.DocumentStorage;
    DocumentVersion = the_other.DocumentVersion;
    DocumentSignatureInformation = the_other.DocumentSignatureInformation;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::DocumentMacroConfirmationRequest const *) {
    static typelib_TypeDescriptionReference * the_type = 0;
    if ( !the_type )
    {
        typelib_static_type_init( &the_type, typelib_TypeClass_EXCEPTION, "com.sun.star.task.DocumentMacroConfirmationRequest" );
    }
    return * reinterpret_cast< ::css::uno::Type * >( &the_type );
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::DocumentMacroConfirmationRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::DocumentMacroConfirmationRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMACROCONFIRMATIONREQUEST_HPP
