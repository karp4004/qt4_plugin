#ifndef INCLUDED_ORG_FREEDESKTOP_PACKAGEKIT_XSYNCDBUSSESSIONHELPER_HPP
#define INCLUDED_ORG_FREEDESKTOP_PACKAGEKIT_XSYNCDBUSSESSIONHELPER_HPP

#include "sal/config.h"

#include <exception>

#include "org/freedesktop/PackageKit/XSyncDbusSessionHelper.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "org/freedesktop/PackageKit/XModify.hpp"
#include "org/freedesktop/PackageKit/XQuery.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace org { namespace freedesktop { namespace PackageKit { namespace detail {

struct theXSyncDbusSessionHelperType : public rtl::StaticWithInit< ::css::uno::Type *, theXSyncDbusSessionHelperType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "org.freedesktop.PackageKit.XSyncDbusSessionHelper" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[2];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::org::freedesktop::PackageKit::XModify > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::org::freedesktop::PackageKit::XQuery > >::get().getTypeLibType();

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            2, aSuperTypes,
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } }

namespace org { namespace freedesktop { namespace PackageKit {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::org::freedesktop::PackageKit::XSyncDbusSessionHelper const *) {
    const ::css::uno::Type &rRet = *detail::theXSyncDbusSessionHelperType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::org::freedesktop::PackageKit::XSyncDbusSessionHelper > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::org::freedesktop::PackageKit::XSyncDbusSessionHelper > >::get();
}

::css::uno::Type const & ::org::freedesktop::PackageKit::XSyncDbusSessionHelper::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::org::freedesktop::PackageKit::XSyncDbusSessionHelper > * >(0));
}

#endif // INCLUDED_ORG_FREEDESKTOP_PACKAGEKIT_XSYNCDBUSSESSIONHELPER_HPP
