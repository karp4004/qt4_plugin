#ifndef INCLUDED_COM_SUN_STAR_ANIMATIONS_TARGETPROPERTIES_HPP
#define INCLUDED_COM_SUN_STAR_ANIMATIONS_TARGETPROPERTIES_HPP

#include "sal/config.h"

#include "com/sun/star/animations/TargetProperties.hdl"

#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace animations {

inline TargetProperties::TargetProperties() SAL_THROW(())
    : Target()
    , Properties()
{
}

inline TargetProperties::TargetProperties(const ::css::uno::Any& Target_, const ::css::uno::Sequence< ::css::beans::NamedValue >& Properties_) SAL_THROW(())
    : Target(Target_)
    , Properties(Properties_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace animations { namespace detail {

struct theTargetPropertiesType : public rtl::StaticWithInit< ::css::uno::Type *, theTargetPropertiesType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.animations.TargetProperties" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "Target" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::NamedValue > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.beans.NamedValue" );
        ::rtl::OUString the_name1( "Properties" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace animations {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::animations::TargetProperties const *) {
    return *detail::theTargetPropertiesType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::animations::TargetProperties const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::animations::TargetProperties >::get();
}

#endif // INCLUDED_COM_SUN_STAR_ANIMATIONS_TARGETPROPERTIES_HPP
