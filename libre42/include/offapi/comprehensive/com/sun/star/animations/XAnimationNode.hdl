#ifndef INCLUDED_COM_SUN_STAR_ANIMATIONS_XANIMATIONNODE_HDL
#define INCLUDED_COM_SUN_STAR_ANIMATIONS_XANIMATIONNODE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/NamedValue.hdl"
#include "com/sun/star/container/XChild.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace animations {

class SAL_NO_VTABLE XAnimationNode : public ::css::container::XChild
{
public:

    // Attributes
    virtual ::sal_Int16 SAL_CALL getType() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getBegin() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setBegin( const ::css::uno::Any& _begin ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getDuration() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setDuration( const ::css::uno::Any& _duration ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getEnd() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setEnd( const ::css::uno::Any& _end ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getEndSync() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setEndSync( const ::css::uno::Any& _endsync ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getRepeatCount() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRepeatCount( const ::css::uno::Any& _repeatcount ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getRepeatDuration() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRepeatDuration( const ::css::uno::Any& _repeatduration ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getFill() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setFill( ::sal_Int16 _fill ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getFillDefault() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setFillDefault( ::sal_Int16 _filldefault ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getRestart() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRestart( ::sal_Int16 _restart ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getRestartDefault() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRestartDefault( ::sal_Int16 _restartdefault ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual double SAL_CALL getAcceleration() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setAcceleration( double _acceleration ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual double SAL_CALL getDecelerate() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setDecelerate( double _decelerate ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getAutoReverse() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setAutoReverse( ::sal_Bool _autoreverse ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::beans::NamedValue > SAL_CALL getUserData() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setUserData( const ::css::uno::Sequence< ::css::beans::NamedValue >& _userdata ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAnimationNode() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::animations::XAnimationNode const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::animations::XAnimationNode > *) SAL_THROW(());

#endif
