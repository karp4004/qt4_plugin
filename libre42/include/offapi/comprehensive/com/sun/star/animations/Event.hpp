#ifndef INCLUDED_COM_SUN_STAR_ANIMATIONS_EVENT_HPP
#define INCLUDED_COM_SUN_STAR_ANIMATIONS_EVENT_HPP

#include "sal/config.h"

#include "com/sun/star/animations/Event.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace animations {

inline Event::Event() SAL_THROW(())
    : Source()
    , Trigger(0)
    , Offset()
    , Repeat(0)
{
}

inline Event::Event(const ::css::uno::Any& Source_, const ::sal_Int16& Trigger_, const ::css::uno::Any& Offset_, const ::sal_uInt16& Repeat_) SAL_THROW(())
    : Source(Source_)
    , Trigger(Trigger_)
    , Offset(Offset_)
    , Repeat(Repeat_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace animations { namespace detail {

struct theEventType : public rtl::StaticWithInit< ::css::uno::Type *, theEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.animations.Event" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "Source" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "Trigger" );
        ::rtl::OUString the_name2( "Offset" );
        ::rtl::OUString the_tname2( "unsigned short" );
        ::rtl::OUString the_name3( "Repeat" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_UNSIGNED_SHORT, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace animations {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::animations::Event const *) {
    return *detail::theEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::animations::Event const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::animations::Event >::get();
}

#endif // INCLUDED_COM_SUN_STAR_ANIMATIONS_EVENT_HPP
