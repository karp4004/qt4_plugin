#ifndef INCLUDED_COM_SUN_STAR_ANIMATIONS_VALUEPAIR_HPP
#define INCLUDED_COM_SUN_STAR_ANIMATIONS_VALUEPAIR_HPP

#include "sal/config.h"

#include "com/sun/star/animations/ValuePair.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace animations {

inline ValuePair::ValuePair() SAL_THROW(())
    : First()
    , Second()
{
}

inline ValuePair::ValuePair(const ::css::uno::Any& First_, const ::css::uno::Any& Second_) SAL_THROW(())
    : First(First_)
    , Second(Second_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace animations { namespace detail {

struct theValuePairType : public rtl::StaticWithInit< ::css::uno::Type *, theValuePairType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.animations.ValuePair" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "First" );
        ::rtl::OUString the_name1( "Second" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace animations {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::animations::ValuePair const *) {
    return *detail::theValuePairType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::animations::ValuePair const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::animations::ValuePair >::get();
}

#endif // INCLUDED_COM_SUN_STAR_ANIMATIONS_VALUEPAIR_HPP
