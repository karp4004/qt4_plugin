#ifndef INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/view/PrintableStateEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/view/PrintableState.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace view {

inline PrintableStateEvent::PrintableStateEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , State(::css::view::PrintableState_JOB_STARTED)
{
}

inline PrintableStateEvent::PrintableStateEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::view::PrintableState& State_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , State(State_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace view { namespace detail {

struct thePrintableStateEventType : public rtl::StaticWithInit< ::css::uno::Type *, thePrintableStateEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.view.PrintableStateEvent" );
        ::cppu::UnoType< ::css::view::PrintableState >::get();
        ::rtl::OUString the_tname0( "com.sun.star.view.PrintableState" );
        ::rtl::OUString the_name0( "State" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace view {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::view::PrintableStateEvent const *) {
    return *detail::thePrintableStateEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::view::PrintableStateEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::view::PrintableStateEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATEEVENT_HPP
