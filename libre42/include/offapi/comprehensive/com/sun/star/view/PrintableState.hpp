#ifndef INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATE_HPP
#define INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATE_HPP

#include "sal/config.h"

#include "com/sun/star/view/PrintableState.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace view { namespace detail {

struct thePrintableStateType : public rtl::StaticWithInit< ::css::uno::Type *, thePrintableStateType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.view.PrintableState" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[6];
        ::rtl::OUString sEnumValue0( "JOB_STARTED" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "JOB_COMPLETED" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "JOB_SPOOLED" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "JOB_ABORTED" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "JOB_FAILED" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "JOB_SPOOLING_FAILED" );
        enumValueNames[5] = sEnumValue5.pData;

        sal_Int32 enumValues[6];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::view::PrintableState_JOB_STARTED,
            6, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace view {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::view::PrintableState const *) {
    return *detail::thePrintableStateType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::view::PrintableState const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::view::PrintableState >::get();
}

#endif // INCLUDED_COM_SUN_STAR_VIEW_PRINTABLESTATE_HPP
