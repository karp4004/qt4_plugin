#ifndef INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_ZIPENTRY_HPP
#define INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_ZIPENTRY_HPP

#include "sal/config.h"

#include "com/sun/star/packages/zip/ZipEntry.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace packages { namespace zip {

inline ZipEntry::ZipEntry() SAL_THROW(())
    : nVersion(0)
    , nFlag(0)
    , nMethod(0)
    , nTime(0)
    , nCrc(0)
    , nCompressedSize(0)
    , nSize(0)
    , nOffset(0)
    , nDiskNumber(0)
    , sName()
    , extra()
    , sComment()
{
}

inline ZipEntry::ZipEntry(const ::sal_Int16& nVersion_, const ::sal_Int16& nFlag_, const ::sal_Int16& nMethod_, const ::sal_Int32& nTime_, const ::sal_Int32& nCrc_, const ::sal_Int32& nCompressedSize_, const ::sal_Int32& nSize_, const ::sal_Int32& nOffset_, const ::sal_Int16& nDiskNumber_, const ::rtl::OUString& sName_, const ::css::uno::Sequence< ::sal_Int8 >& extra_, const ::rtl::OUString& sComment_) SAL_THROW(())
    : nVersion(nVersion_)
    , nFlag(nFlag_)
    , nMethod(nMethod_)
    , nTime(nTime_)
    , nCrc(nCrc_)
    , nCompressedSize(nCompressedSize_)
    , nSize(nSize_)
    , nOffset(nOffset_)
    , nDiskNumber(nDiskNumber_)
    , sName(sName_)
    , extra(extra_)
    , sComment(sComment_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace packages { namespace zip { namespace detail {

struct theZipEntryType : public rtl::StaticWithInit< ::css::uno::Type *, theZipEntryType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.packages.zip.ZipEntry" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "nVersion" );
        ::rtl::OUString the_name1( "nFlag" );
        ::rtl::OUString the_name2( "nMethod" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name3( "nTime" );
        ::rtl::OUString the_name4( "nCrc" );
        ::rtl::OUString the_name5( "nCompressedSize" );
        ::rtl::OUString the_name6( "nSize" );
        ::rtl::OUString the_name7( "nOffset" );
        ::rtl::OUString the_name8( "nDiskNumber" );
        ::rtl::OUString the_tname2( "string" );
        ::rtl::OUString the_name9( "sName" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::sal_Int8 > >::get();
        ::rtl::OUString the_tname3( "[]byte" );
        ::rtl::OUString the_name10( "extra" );
        ::rtl::OUString the_name11( "sComment" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name6.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name7.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name8.pData }, false },
            { { typelib_TypeClass_STRING, the_tname2.pData, the_name9.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname3.pData, the_name10.pData }, false },
            { { typelib_TypeClass_STRING, the_tname2.pData, the_name11.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 12, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace packages { namespace zip {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::packages::zip::ZipEntry const *) {
    return *detail::theZipEntryType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::packages::zip::ZipEntry const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::packages::zip::ZipEntry >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PACKAGES_ZIP_ZIPENTRY_HPP
