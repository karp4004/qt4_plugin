#ifndef INCLUDED_COM_SUN_STAR_PACKAGES_NORAWFORMATEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_PACKAGES_NORAWFORMATEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/packages/NoRawFormatException.hdl"

#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace packages {

inline NoRawFormatException::NoRawFormatException() SAL_THROW(())
    : ::css::io::IOException()
{
    ::cppu::UnoType< ::css::packages::NoRawFormatException >::get();
}

inline NoRawFormatException::NoRawFormatException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::io::IOException(Message_, Context_)
{
    ::cppu::UnoType< ::css::packages::NoRawFormatException >::get();
}

NoRawFormatException::NoRawFormatException(NoRawFormatException const & the_other): ::css::io::IOException(the_other) {}

NoRawFormatException::~NoRawFormatException() {}

NoRawFormatException & NoRawFormatException::operator =(NoRawFormatException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::io::IOException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace packages { namespace detail {

struct theNoRawFormatExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theNoRawFormatExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.packages.NoRawFormatException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::io::IOException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace packages {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::packages::NoRawFormatException const *) {
    return *detail::theNoRawFormatExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::packages::NoRawFormatException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::packages::NoRawFormatException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PACKAGES_NORAWFORMATEXCEPTION_HPP
