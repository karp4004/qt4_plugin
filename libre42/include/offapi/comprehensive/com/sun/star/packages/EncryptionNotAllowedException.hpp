#ifndef INCLUDED_COM_SUN_STAR_PACKAGES_ENCRYPTIONNOTALLOWEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_PACKAGES_ENCRYPTIONNOTALLOWEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/packages/EncryptionNotAllowedException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace packages {

inline EncryptionNotAllowedException::EncryptionNotAllowedException() SAL_THROW(())
    : ::css::uno::Exception()
{
    ::cppu::UnoType< ::css::packages::EncryptionNotAllowedException >::get();
}

inline EncryptionNotAllowedException::EncryptionNotAllowedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{
    ::cppu::UnoType< ::css::packages::EncryptionNotAllowedException >::get();
}

EncryptionNotAllowedException::EncryptionNotAllowedException(EncryptionNotAllowedException const & the_other): ::css::uno::Exception(the_other) {}

EncryptionNotAllowedException::~EncryptionNotAllowedException() {}

EncryptionNotAllowedException & EncryptionNotAllowedException::operator =(EncryptionNotAllowedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace packages { namespace detail {

struct theEncryptionNotAllowedExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theEncryptionNotAllowedExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.packages.EncryptionNotAllowedException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace packages {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::packages::EncryptionNotAllowedException const *) {
    return *detail::theEncryptionNotAllowedExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::packages::EncryptionNotAllowedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::packages::EncryptionNotAllowedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PACKAGES_ENCRYPTIONNOTALLOWEDEXCEPTION_HPP
