#ifndef INCLUDED_COM_SUN_STAR_I18N_LANGUAGECOUNTRYINFO_HPP
#define INCLUDED_COM_SUN_STAR_I18N_LANGUAGECOUNTRYINFO_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/LanguageCountryInfo.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline LanguageCountryInfo::LanguageCountryInfo() SAL_THROW(())
    : Language()
    , LanguageDefaultName()
    , Country()
    , CountryDefaultName()
    , Variant()
{
}

inline LanguageCountryInfo::LanguageCountryInfo(const ::rtl::OUString& Language_, const ::rtl::OUString& LanguageDefaultName_, const ::rtl::OUString& Country_, const ::rtl::OUString& CountryDefaultName_, const ::rtl::OUString& Variant_) SAL_THROW(())
    : Language(Language_)
    , LanguageDefaultName(LanguageDefaultName_)
    , Country(Country_)
    , CountryDefaultName(CountryDefaultName_)
    , Variant(Variant_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theLanguageCountryInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theLanguageCountryInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.LanguageCountryInfo" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Language" );
        ::rtl::OUString the_name1( "LanguageDefaultName" );
        ::rtl::OUString the_name2( "Country" );
        ::rtl::OUString the_name3( "CountryDefaultName" );
        ::rtl::OUString the_name4( "Variant" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::LanguageCountryInfo const *) {
    return *detail::theLanguageCountryInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::LanguageCountryInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::LanguageCountryInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_LANGUAGECOUNTRYINFO_HPP
