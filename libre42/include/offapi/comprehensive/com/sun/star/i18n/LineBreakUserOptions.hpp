#ifndef INCLUDED_COM_SUN_STAR_I18N_LINEBREAKUSEROPTIONS_HPP
#define INCLUDED_COM_SUN_STAR_I18N_LINEBREAKUSEROPTIONS_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/LineBreakUserOptions.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline LineBreakUserOptions::LineBreakUserOptions() SAL_THROW(())
    : forbiddenBeginCharacters()
    , forbiddenEndCharacters()
    , applyForbiddenRules(false)
    , allowPunctuationOutsideMargin(false)
    , allowHyphenateEnglish(false)
{
}

inline LineBreakUserOptions::LineBreakUserOptions(const ::rtl::OUString& forbiddenBeginCharacters_, const ::rtl::OUString& forbiddenEndCharacters_, const ::sal_Bool& applyForbiddenRules_, const ::sal_Bool& allowPunctuationOutsideMargin_, const ::sal_Bool& allowHyphenateEnglish_) SAL_THROW(())
    : forbiddenBeginCharacters(forbiddenBeginCharacters_)
    , forbiddenEndCharacters(forbiddenEndCharacters_)
    , applyForbiddenRules(applyForbiddenRules_)
    , allowPunctuationOutsideMargin(allowPunctuationOutsideMargin_)
    , allowHyphenateEnglish(allowHyphenateEnglish_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theLineBreakUserOptionsType : public rtl::StaticWithInit< ::css::uno::Type *, theLineBreakUserOptionsType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.LineBreakUserOptions" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "forbiddenBeginCharacters" );
        ::rtl::OUString the_name1( "forbiddenEndCharacters" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name2( "applyForbiddenRules" );
        ::rtl::OUString the_name3( "allowPunctuationOutsideMargin" );
        ::rtl::OUString the_name4( "allowHyphenateEnglish" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::LineBreakUserOptions const *) {
    return *detail::theLineBreakUserOptionsType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::LineBreakUserOptions const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::LineBreakUserOptions >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_LINEBREAKUSEROPTIONS_HPP
