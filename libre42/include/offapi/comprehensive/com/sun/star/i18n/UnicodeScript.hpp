#ifndef INCLUDED_COM_SUN_STAR_I18N_UNICODESCRIPT_HPP
#define INCLUDED_COM_SUN_STAR_I18N_UNICODESCRIPT_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/UnicodeScript.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theUnicodeScriptType : public rtl::StaticWithInit< ::css::uno::Type *, theUnicodeScriptType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.i18n.UnicodeScript" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[88];
        ::rtl::OUString sEnumValue0( "kBasicLatin" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "kLatin1Supplement" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "kLatinExtendedA" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "kLatinExtendedB" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "kIPAExtension" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "kSpacingModifier" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "kCombiningDiacritical" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "kGreek" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "kCyrillic" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "kArmenian" );
        enumValueNames[9] = sEnumValue9.pData;
        ::rtl::OUString sEnumValue10( "kHebrew" );
        enumValueNames[10] = sEnumValue10.pData;
        ::rtl::OUString sEnumValue11( "kArabic" );
        enumValueNames[11] = sEnumValue11.pData;
        ::rtl::OUString sEnumValue12( "kSyriac" );
        enumValueNames[12] = sEnumValue12.pData;
        ::rtl::OUString sEnumValue13( "kThaana" );
        enumValueNames[13] = sEnumValue13.pData;
        ::rtl::OUString sEnumValue14( "kDevanagari" );
        enumValueNames[14] = sEnumValue14.pData;
        ::rtl::OUString sEnumValue15( "kBengali" );
        enumValueNames[15] = sEnumValue15.pData;
        ::rtl::OUString sEnumValue16( "kGurmukhi" );
        enumValueNames[16] = sEnumValue16.pData;
        ::rtl::OUString sEnumValue17( "kGujarati" );
        enumValueNames[17] = sEnumValue17.pData;
        ::rtl::OUString sEnumValue18( "kOriya" );
        enumValueNames[18] = sEnumValue18.pData;
        ::rtl::OUString sEnumValue19( "kTamil" );
        enumValueNames[19] = sEnumValue19.pData;
        ::rtl::OUString sEnumValue20( "kTelugu" );
        enumValueNames[20] = sEnumValue20.pData;
        ::rtl::OUString sEnumValue21( "kKannada" );
        enumValueNames[21] = sEnumValue21.pData;
        ::rtl::OUString sEnumValue22( "kMalayalam" );
        enumValueNames[22] = sEnumValue22.pData;
        ::rtl::OUString sEnumValue23( "kSinhala" );
        enumValueNames[23] = sEnumValue23.pData;
        ::rtl::OUString sEnumValue24( "kThai" );
        enumValueNames[24] = sEnumValue24.pData;
        ::rtl::OUString sEnumValue25( "kLao" );
        enumValueNames[25] = sEnumValue25.pData;
        ::rtl::OUString sEnumValue26( "kTibetan" );
        enumValueNames[26] = sEnumValue26.pData;
        ::rtl::OUString sEnumValue27( "kMyanmar" );
        enumValueNames[27] = sEnumValue27.pData;
        ::rtl::OUString sEnumValue28( "kGeorgian" );
        enumValueNames[28] = sEnumValue28.pData;
        ::rtl::OUString sEnumValue29( "kHangulJamo" );
        enumValueNames[29] = sEnumValue29.pData;
        ::rtl::OUString sEnumValue30( "kEthiopic" );
        enumValueNames[30] = sEnumValue30.pData;
        ::rtl::OUString sEnumValue31( "kCherokee" );
        enumValueNames[31] = sEnumValue31.pData;
        ::rtl::OUString sEnumValue32( "kUnifiedCanadianAboriginalSyllabics" );
        enumValueNames[32] = sEnumValue32.pData;
        ::rtl::OUString sEnumValue33( "kOgham" );
        enumValueNames[33] = sEnumValue33.pData;
        ::rtl::OUString sEnumValue34( "kRunic" );
        enumValueNames[34] = sEnumValue34.pData;
        ::rtl::OUString sEnumValue35( "kKhmer" );
        enumValueNames[35] = sEnumValue35.pData;
        ::rtl::OUString sEnumValue36( "kMongolian" );
        enumValueNames[36] = sEnumValue36.pData;
        ::rtl::OUString sEnumValue37( "kLatinExtendedAdditional" );
        enumValueNames[37] = sEnumValue37.pData;
        ::rtl::OUString sEnumValue38( "kGreekExtended" );
        enumValueNames[38] = sEnumValue38.pData;
        ::rtl::OUString sEnumValue39( "kGeneralPunctuation" );
        enumValueNames[39] = sEnumValue39.pData;
        ::rtl::OUString sEnumValue40( "kSuperSubScript" );
        enumValueNames[40] = sEnumValue40.pData;
        ::rtl::OUString sEnumValue41( "kCurrencySymbolScript" );
        enumValueNames[41] = sEnumValue41.pData;
        ::rtl::OUString sEnumValue42( "kSymbolCombiningMark" );
        enumValueNames[42] = sEnumValue42.pData;
        ::rtl::OUString sEnumValue43( "kLetterlikeSymbol" );
        enumValueNames[43] = sEnumValue43.pData;
        ::rtl::OUString sEnumValue44( "kNumberForm" );
        enumValueNames[44] = sEnumValue44.pData;
        ::rtl::OUString sEnumValue45( "kArrow" );
        enumValueNames[45] = sEnumValue45.pData;
        ::rtl::OUString sEnumValue46( "kMathOperator" );
        enumValueNames[46] = sEnumValue46.pData;
        ::rtl::OUString sEnumValue47( "kMiscTechnical" );
        enumValueNames[47] = sEnumValue47.pData;
        ::rtl::OUString sEnumValue48( "kControlPicture" );
        enumValueNames[48] = sEnumValue48.pData;
        ::rtl::OUString sEnumValue49( "kOpticalCharacter" );
        enumValueNames[49] = sEnumValue49.pData;
        ::rtl::OUString sEnumValue50( "kEnclosedAlphanumeric" );
        enumValueNames[50] = sEnumValue50.pData;
        ::rtl::OUString sEnumValue51( "kBoxDrawing" );
        enumValueNames[51] = sEnumValue51.pData;
        ::rtl::OUString sEnumValue52( "kBlockElement" );
        enumValueNames[52] = sEnumValue52.pData;
        ::rtl::OUString sEnumValue53( "kGeometricShape" );
        enumValueNames[53] = sEnumValue53.pData;
        ::rtl::OUString sEnumValue54( "kMiscSymbol" );
        enumValueNames[54] = sEnumValue54.pData;
        ::rtl::OUString sEnumValue55( "kDingbat" );
        enumValueNames[55] = sEnumValue55.pData;
        ::rtl::OUString sEnumValue56( "kBraillePatterns" );
        enumValueNames[56] = sEnumValue56.pData;
        ::rtl::OUString sEnumValue57( "kCJKRadicalsSupplement" );
        enumValueNames[57] = sEnumValue57.pData;
        ::rtl::OUString sEnumValue58( "kKangxiRadicals" );
        enumValueNames[58] = sEnumValue58.pData;
        ::rtl::OUString sEnumValue59( "kIdeographicDescriptionCharacters" );
        enumValueNames[59] = sEnumValue59.pData;
        ::rtl::OUString sEnumValue60( "kCJKSymbolPunctuation" );
        enumValueNames[60] = sEnumValue60.pData;
        ::rtl::OUString sEnumValue61( "kHiragana" );
        enumValueNames[61] = sEnumValue61.pData;
        ::rtl::OUString sEnumValue62( "kKatakana" );
        enumValueNames[62] = sEnumValue62.pData;
        ::rtl::OUString sEnumValue63( "kBopomofo" );
        enumValueNames[63] = sEnumValue63.pData;
        ::rtl::OUString sEnumValue64( "kHangulCompatibilityJamo" );
        enumValueNames[64] = sEnumValue64.pData;
        ::rtl::OUString sEnumValue65( "kKanbun" );
        enumValueNames[65] = sEnumValue65.pData;
        ::rtl::OUString sEnumValue66( "kBopomofoExtended" );
        enumValueNames[66] = sEnumValue66.pData;
        ::rtl::OUString sEnumValue67( "kEnclosedCJKLetterMonth" );
        enumValueNames[67] = sEnumValue67.pData;
        ::rtl::OUString sEnumValue68( "kCJKCompatibility" );
        enumValueNames[68] = sEnumValue68.pData;
        ::rtl::OUString sEnumValue69( "k_CJKUnifiedIdeographsExtensionA" );
        enumValueNames[69] = sEnumValue69.pData;
        ::rtl::OUString sEnumValue70( "kCJKUnifiedIdeograph" );
        enumValueNames[70] = sEnumValue70.pData;
        ::rtl::OUString sEnumValue71( "kYiSyllables" );
        enumValueNames[71] = sEnumValue71.pData;
        ::rtl::OUString sEnumValue72( "kYiRadicals" );
        enumValueNames[72] = sEnumValue72.pData;
        ::rtl::OUString sEnumValue73( "kHangulSyllable" );
        enumValueNames[73] = sEnumValue73.pData;
        ::rtl::OUString sEnumValue74( "kHighSurrogate" );
        enumValueNames[74] = sEnumValue74.pData;
        ::rtl::OUString sEnumValue75( "kHighPrivateUseSurrogate" );
        enumValueNames[75] = sEnumValue75.pData;
        ::rtl::OUString sEnumValue76( "kLowSurrogate" );
        enumValueNames[76] = sEnumValue76.pData;
        ::rtl::OUString sEnumValue77( "kPrivateUse" );
        enumValueNames[77] = sEnumValue77.pData;
        ::rtl::OUString sEnumValue78( "kCJKCompatibilityIdeograph" );
        enumValueNames[78] = sEnumValue78.pData;
        ::rtl::OUString sEnumValue79( "kAlphabeticPresentation" );
        enumValueNames[79] = sEnumValue79.pData;
        ::rtl::OUString sEnumValue80( "kArabicPresentationA" );
        enumValueNames[80] = sEnumValue80.pData;
        ::rtl::OUString sEnumValue81( "kCombiningHalfMark" );
        enumValueNames[81] = sEnumValue81.pData;
        ::rtl::OUString sEnumValue82( "kCJKCompatibilityForm" );
        enumValueNames[82] = sEnumValue82.pData;
        ::rtl::OUString sEnumValue83( "kSmallFormVariant" );
        enumValueNames[83] = sEnumValue83.pData;
        ::rtl::OUString sEnumValue84( "kArabicPresentationB" );
        enumValueNames[84] = sEnumValue84.pData;
        ::rtl::OUString sEnumValue85( "kNoScript" );
        enumValueNames[85] = sEnumValue85.pData;
        ::rtl::OUString sEnumValue86( "kHalfwidthFullwidthForm" );
        enumValueNames[86] = sEnumValue86.pData;
        ::rtl::OUString sEnumValue87( "kScriptCount" );
        enumValueNames[87] = sEnumValue87.pData;

        sal_Int32 enumValues[88];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;
        enumValues[9] = 9;
        enumValues[10] = 10;
        enumValues[11] = 11;
        enumValues[12] = 12;
        enumValues[13] = 13;
        enumValues[14] = 14;
        enumValues[15] = 15;
        enumValues[16] = 16;
        enumValues[17] = 17;
        enumValues[18] = 18;
        enumValues[19] = 19;
        enumValues[20] = 20;
        enumValues[21] = 21;
        enumValues[22] = 22;
        enumValues[23] = 23;
        enumValues[24] = 24;
        enumValues[25] = 25;
        enumValues[26] = 26;
        enumValues[27] = 27;
        enumValues[28] = 28;
        enumValues[29] = 29;
        enumValues[30] = 30;
        enumValues[31] = 31;
        enumValues[32] = 32;
        enumValues[33] = 33;
        enumValues[34] = 34;
        enumValues[35] = 35;
        enumValues[36] = 36;
        enumValues[37] = 37;
        enumValues[38] = 38;
        enumValues[39] = 39;
        enumValues[40] = 40;
        enumValues[41] = 41;
        enumValues[42] = 42;
        enumValues[43] = 43;
        enumValues[44] = 44;
        enumValues[45] = 45;
        enumValues[46] = 46;
        enumValues[47] = 47;
        enumValues[48] = 48;
        enumValues[49] = 49;
        enumValues[50] = 50;
        enumValues[51] = 51;
        enumValues[52] = 52;
        enumValues[53] = 53;
        enumValues[54] = 54;
        enumValues[55] = 55;
        enumValues[56] = 56;
        enumValues[57] = 57;
        enumValues[58] = 58;
        enumValues[59] = 59;
        enumValues[60] = 60;
        enumValues[61] = 61;
        enumValues[62] = 62;
        enumValues[63] = 63;
        enumValues[64] = 64;
        enumValues[65] = 65;
        enumValues[66] = 66;
        enumValues[67] = 67;
        enumValues[68] = 68;
        enumValues[69] = 69;
        enumValues[70] = 70;
        enumValues[71] = 71;
        enumValues[72] = 72;
        enumValues[73] = 73;
        enumValues[74] = 74;
        enumValues[75] = 75;
        enumValues[76] = 76;
        enumValues[77] = 77;
        enumValues[78] = 78;
        enumValues[79] = 79;
        enumValues[80] = 80;
        enumValues[81] = 81;
        enumValues[82] = 82;
        enumValues[83] = 83;
        enumValues[84] = 84;
        enumValues[85] = 85;
        enumValues[86] = 86;
        enumValues[87] = 87;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::i18n::UnicodeScript_kBasicLatin,
            88, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::UnicodeScript const *) {
    return *detail::theUnicodeScriptType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::UnicodeScript const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::UnicodeScript >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_UNICODESCRIPT_HPP
