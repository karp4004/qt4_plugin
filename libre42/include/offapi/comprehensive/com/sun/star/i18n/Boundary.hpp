#ifndef INCLUDED_COM_SUN_STAR_I18N_BOUNDARY_HPP
#define INCLUDED_COM_SUN_STAR_I18N_BOUNDARY_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/Boundary.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline Boundary::Boundary() SAL_THROW(())
    : startPos(0)
    , endPos(0)
{
}

inline Boundary::Boundary(const ::sal_Int32& startPos_, const ::sal_Int32& endPos_) SAL_THROW(())
    : startPos(startPos_)
    , endPos(endPos_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theBoundaryType : public rtl::StaticWithInit< ::css::uno::Type *, theBoundaryType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.Boundary" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "startPos" );
        ::rtl::OUString the_name1( "endPos" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::Boundary const *) {
    return *detail::theBoundaryType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::Boundary const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::Boundary >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_BOUNDARY_HPP
