#ifndef INCLUDED_COM_SUN_STAR_I18N_LINEBREAKHYPHENATIONOPTIONS_HPP
#define INCLUDED_COM_SUN_STAR_I18N_LINEBREAKHYPHENATIONOPTIONS_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/LineBreakHyphenationOptions.hdl"

#include "com/sun/star/beans/PropertyValues.hpp"
#include "com/sun/star/linguistic2/XHyphenator.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline LineBreakHyphenationOptions::LineBreakHyphenationOptions() SAL_THROW(())
    : rHyphenator()
    , aHyphenationOptions()
    , hyphenIndex(0)
{
}

inline LineBreakHyphenationOptions::LineBreakHyphenationOptions(const ::css::uno::Reference< ::css::linguistic2::XHyphenator >& rHyphenator_, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aHyphenationOptions_, const ::sal_Int32& hyphenIndex_) SAL_THROW(())
    : rHyphenator(rHyphenator_)
    , aHyphenationOptions(aHyphenationOptions_)
    , hyphenIndex(hyphenIndex_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theLineBreakHyphenationOptionsType : public rtl::StaticWithInit< ::css::uno::Type *, theLineBreakHyphenationOptionsType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.LineBreakHyphenationOptions" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::linguistic2::XHyphenator > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.linguistic2.XHyphenator" );
        ::rtl::OUString the_name0( "rHyphenator" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::PropertyValue > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.beans.PropertyValue" );
        ::rtl::OUString the_name1( "aHyphenationOptions" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name2( "hyphenIndex" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::LineBreakHyphenationOptions const *) {
    return *detail::theLineBreakHyphenationOptionsType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::LineBreakHyphenationOptions const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::LineBreakHyphenationOptions >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_LINEBREAKHYPHENATIONOPTIONS_HPP
