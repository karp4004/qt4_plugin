#ifndef INCLUDED_COM_SUN_STAR_I18N_PARSERESULT_HPP
#define INCLUDED_COM_SUN_STAR_I18N_PARSERESULT_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/ParseResult.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline ParseResult::ParseResult() SAL_THROW(())
    : LeadingWhiteSpace(0)
    , EndPos(0)
    , CharLen(0)
    , Value(0)
    , TokenType(0)
    , StartFlags(0)
    , ContFlags(0)
    , DequotedNameOrString()
{
}

inline ParseResult::ParseResult(const ::sal_Int32& LeadingWhiteSpace_, const ::sal_Int32& EndPos_, const ::sal_Int32& CharLen_, const double& Value_, const ::sal_Int32& TokenType_, const ::sal_Int32& StartFlags_, const ::sal_Int32& ContFlags_, const ::rtl::OUString& DequotedNameOrString_) SAL_THROW(())
    : LeadingWhiteSpace(LeadingWhiteSpace_)
    , EndPos(EndPos_)
    , CharLen(CharLen_)
    , Value(Value_)
    , TokenType(TokenType_)
    , StartFlags(StartFlags_)
    , ContFlags(ContFlags_)
    , DequotedNameOrString(DequotedNameOrString_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theParseResultType : public rtl::StaticWithInit< ::css::uno::Type *, theParseResultType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.ParseResult" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "LeadingWhiteSpace" );
        ::rtl::OUString the_name1( "EndPos" );
        ::rtl::OUString the_name2( "CharLen" );
        ::rtl::OUString the_tname1( "double" );
        ::rtl::OUString the_name3( "Value" );
        ::rtl::OUString the_name4( "TokenType" );
        ::rtl::OUString the_name5( "StartFlags" );
        ::rtl::OUString the_name6( "ContFlags" );
        ::rtl::OUString the_tname2( "string" );
        ::rtl::OUString the_name7( "DequotedNameOrString" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name6.pData }, false },
            { { typelib_TypeClass_STRING, the_tname2.pData, the_name7.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 8, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::ParseResult const *) {
    return *detail::theParseResultType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::ParseResult const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::ParseResult >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_PARSERESULT_HPP
