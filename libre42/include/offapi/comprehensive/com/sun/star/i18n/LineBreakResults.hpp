#ifndef INCLUDED_COM_SUN_STAR_I18N_LINEBREAKRESULTS_HPP
#define INCLUDED_COM_SUN_STAR_I18N_LINEBREAKRESULTS_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/LineBreakResults.hdl"

#include "com/sun/star/linguistic2/XHyphenatedWord.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline LineBreakResults::LineBreakResults() SAL_THROW(())
    : breakType(0)
    , breakIndex(0)
    , rHyphenatedWord()
{
}

inline LineBreakResults::LineBreakResults(const ::sal_Int16& breakType_, const ::sal_Int32& breakIndex_, const ::css::uno::Reference< ::css::linguistic2::XHyphenatedWord >& rHyphenatedWord_) SAL_THROW(())
    : breakType(breakType_)
    , breakIndex(breakIndex_)
    , rHyphenatedWord(rHyphenatedWord_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theLineBreakResultsType : public rtl::StaticWithInit< ::css::uno::Type *, theLineBreakResultsType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.LineBreakResults" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "breakType" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "breakIndex" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::linguistic2::XHyphenatedWord > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.linguistic2.XHyphenatedWord" );
        ::rtl::OUString the_name2( "rHyphenatedWord" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::LineBreakResults const *) {
    return *detail::theLineBreakResultsType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::LineBreakResults const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::LineBreakResults >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_LINEBREAKRESULTS_HPP
