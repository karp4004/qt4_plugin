#ifndef INCLUDED_COM_SUN_STAR_I18N_CURRENCY2_HPP
#define INCLUDED_COM_SUN_STAR_I18N_CURRENCY2_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/Currency2.hdl"

#include "com/sun/star/i18n/Currency.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline Currency2::Currency2() SAL_THROW(())
    : ::css::i18n::Currency()
    , LegacyOnly(false)
{
}

inline Currency2::Currency2(const ::rtl::OUString& ID_, const ::rtl::OUString& Symbol_, const ::rtl::OUString& BankSymbol_, const ::rtl::OUString& Name_, const ::sal_Bool& Default_, const ::sal_Bool& UsedInCompatibleFormatCodes_, const ::sal_Int16& DecimalPlaces_, const ::sal_Bool& LegacyOnly_) SAL_THROW(())
    : ::css::i18n::Currency(ID_, Symbol_, BankSymbol_, Name_, Default_, UsedInCompatibleFormatCodes_, DecimalPlaces_)
    , LegacyOnly(LegacyOnly_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theCurrency2Type : public rtl::StaticWithInit< ::css::uno::Type *, theCurrency2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.Currency2" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "LegacyOnly" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::i18n::Currency >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::Currency2 const *) {
    return *detail::theCurrency2Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::Currency2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::Currency2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_CURRENCY2_HPP
