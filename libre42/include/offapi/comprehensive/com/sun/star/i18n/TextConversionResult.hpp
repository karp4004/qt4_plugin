#ifndef INCLUDED_COM_SUN_STAR_I18N_TEXTCONVERSIONRESULT_HPP
#define INCLUDED_COM_SUN_STAR_I18N_TEXTCONVERSIONRESULT_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/TextConversionResult.hdl"

#include "com/sun/star/i18n/Boundary.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline TextConversionResult::TextConversionResult() SAL_THROW(())
    : Boundary()
    , Candidates()
{
}

inline TextConversionResult::TextConversionResult(const ::css::i18n::Boundary& Boundary_, const ::css::uno::Sequence< ::rtl::OUString >& Candidates_) SAL_THROW(())
    : Boundary(Boundary_)
    , Candidates(Candidates_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theTextConversionResultType : public rtl::StaticWithInit< ::css::uno::Type *, theTextConversionResultType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.TextConversionResult" );
        ::cppu::UnoType< ::css::i18n::Boundary >::get();
        ::rtl::OUString the_tname0( "com.sun.star.i18n.Boundary" );
        ::rtl::OUString the_name0( "Boundary" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
        ::rtl::OUString the_tname1( "[]string" );
        ::rtl::OUString the_name1( "Candidates" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::TextConversionResult const *) {
    return *detail::theTextConversionResultType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::TextConversionResult const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::TextConversionResult >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_TEXTCONVERSIONRESULT_HPP
