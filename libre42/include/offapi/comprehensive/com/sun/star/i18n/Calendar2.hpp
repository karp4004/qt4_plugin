#ifndef INCLUDED_COM_SUN_STAR_I18N_CALENDAR2_HPP
#define INCLUDED_COM_SUN_STAR_I18N_CALENDAR2_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/Calendar2.hdl"

#include "com/sun/star/i18n/CalendarItem2.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline Calendar2::Calendar2() SAL_THROW(())
    : Days()
    , Months()
    , GenitiveMonths()
    , PartitiveMonths()
    , Eras()
    , StartOfWeek()
    , MinimumNumberOfDaysForFirstWeek(0)
    , Default(false)
    , Name()
{
}

inline Calendar2::Calendar2(const ::css::uno::Sequence< ::css::i18n::CalendarItem2 >& Days_, const ::css::uno::Sequence< ::css::i18n::CalendarItem2 >& Months_, const ::css::uno::Sequence< ::css::i18n::CalendarItem2 >& GenitiveMonths_, const ::css::uno::Sequence< ::css::i18n::CalendarItem2 >& PartitiveMonths_, const ::css::uno::Sequence< ::css::i18n::CalendarItem2 >& Eras_, const ::rtl::OUString& StartOfWeek_, const ::sal_Int16& MinimumNumberOfDaysForFirstWeek_, const ::sal_Bool& Default_, const ::rtl::OUString& Name_) SAL_THROW(())
    : Days(Days_)
    , Months(Months_)
    , GenitiveMonths(GenitiveMonths_)
    , PartitiveMonths(PartitiveMonths_)
    , Eras(Eras_)
    , StartOfWeek(StartOfWeek_)
    , MinimumNumberOfDaysForFirstWeek(MinimumNumberOfDaysForFirstWeek_)
    , Default(Default_)
    , Name(Name_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theCalendar2Type : public rtl::StaticWithInit< ::css::uno::Type *, theCalendar2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.Calendar2" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::i18n::CalendarItem2 > >::get();
        ::rtl::OUString the_tname0( "[]com.sun.star.i18n.CalendarItem2" );
        ::rtl::OUString the_name0( "Days" );
        ::rtl::OUString the_name1( "Months" );
        ::rtl::OUString the_name2( "GenitiveMonths" );
        ::rtl::OUString the_name3( "PartitiveMonths" );
        ::rtl::OUString the_name4( "Eras" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name5( "StartOfWeek" );
        ::rtl::OUString the_tname2( "short" );
        ::rtl::OUString the_name6( "MinimumNumberOfDaysForFirstWeek" );
        ::rtl::OUString the_tname3( "boolean" );
        ::rtl::OUString the_name7( "Default" );
        ::rtl::OUString the_name8( "Name" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname3.pData, the_name7.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name8.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 9, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::Calendar2 const *) {
    return *detail::theCalendar2Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::Calendar2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::Calendar2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_CALENDAR2_HPP
