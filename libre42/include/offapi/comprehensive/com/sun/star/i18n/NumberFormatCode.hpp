#ifndef INCLUDED_COM_SUN_STAR_I18N_NUMBERFORMATCODE_HPP
#define INCLUDED_COM_SUN_STAR_I18N_NUMBERFORMATCODE_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/NumberFormatCode.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline NumberFormatCode::NumberFormatCode() SAL_THROW(())
    : Type(0)
    , Usage(0)
    , Code()
    , DefaultName()
    , NameID()
    , Index(0)
    , Default(false)
{
}

inline NumberFormatCode::NumberFormatCode(const ::sal_Int16& Type_, const ::sal_Int16& Usage_, const ::rtl::OUString& Code_, const ::rtl::OUString& DefaultName_, const ::rtl::OUString& NameID_, const ::sal_Int16& Index_, const ::sal_Bool& Default_) SAL_THROW(())
    : Type(Type_)
    , Usage(Usage_)
    , Code(Code_)
    , DefaultName(DefaultName_)
    , NameID(NameID_)
    , Index(Index_)
    , Default(Default_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theNumberFormatCodeType : public rtl::StaticWithInit< ::css::uno::Type *, theNumberFormatCodeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.NumberFormatCode" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "Type" );
        ::rtl::OUString the_name1( "Usage" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name2( "Code" );
        ::rtl::OUString the_name3( "DefaultName" );
        ::rtl::OUString the_name4( "NameID" );
        ::rtl::OUString the_name5( "Index" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name6( "Default" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::NumberFormatCode const *) {
    return *detail::theNumberFormatCodeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::NumberFormatCode const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::NumberFormatCode >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_NUMBERFORMATCODE_HPP
