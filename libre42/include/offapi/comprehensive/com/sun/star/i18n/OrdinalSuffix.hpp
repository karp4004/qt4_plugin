#ifndef INCLUDED_COM_SUN_STAR_I18N_ORDINALSUFFIX_HPP
#define INCLUDED_COM_SUN_STAR_I18N_ORDINALSUFFIX_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/i18n/XOrdinalSuffix.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace i18n {

class OrdinalSuffix {
public:
    static ::css::uno::Reference< ::css::i18n::XOrdinalSuffix > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::i18n::XOrdinalSuffix > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::i18n::XOrdinalSuffix >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.i18n.OrdinalSuffix" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.i18n.OrdinalSuffix of type com.sun.star.i18n.XOrdinalSuffix: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.i18n.OrdinalSuffix of type com.sun.star.i18n.XOrdinalSuffix" ), the_context);
        }
        return the_instance;
    }

private:
    OrdinalSuffix(); // not implemented
    OrdinalSuffix(OrdinalSuffix &); // not implemented
    ~OrdinalSuffix(); // not implemented
    void operator =(OrdinalSuffix); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_I18N_ORDINALSUFFIX_HPP
