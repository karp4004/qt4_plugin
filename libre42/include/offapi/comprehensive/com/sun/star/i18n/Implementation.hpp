#ifndef INCLUDED_COM_SUN_STAR_I18N_IMPLEMENTATION_HPP
#define INCLUDED_COM_SUN_STAR_I18N_IMPLEMENTATION_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/Implementation.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline Implementation::Implementation() SAL_THROW(())
    : unoID()
    , isDefault(false)
{
}

inline Implementation::Implementation(const ::rtl::OUString& unoID_, const ::sal_Bool& isDefault_) SAL_THROW(())
    : unoID(unoID_)
    , isDefault(isDefault_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theImplementationType : public rtl::StaticWithInit< ::css::uno::Type *, theImplementationType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.Implementation" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "unoID" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "isDefault" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::Implementation const *) {
    return *detail::theImplementationType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::Implementation const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::Implementation >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_IMPLEMENTATION_HPP
