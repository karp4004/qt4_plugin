#ifndef INCLUDED_COM_SUN_STAR_I18N_FORMATELEMENT_HPP
#define INCLUDED_COM_SUN_STAR_I18N_FORMATELEMENT_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/FormatElement.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline FormatElement::FormatElement() SAL_THROW(())
    : formatCode()
    , formatName()
    , formatKey()
    , formatType()
    , formatUsage()
    , formatIndex(0)
    , isDefault(false)
{
}

inline FormatElement::FormatElement(const ::rtl::OUString& formatCode_, const ::rtl::OUString& formatName_, const ::rtl::OUString& formatKey_, const ::rtl::OUString& formatType_, const ::rtl::OUString& formatUsage_, const ::sal_Int16& formatIndex_, const ::sal_Bool& isDefault_) SAL_THROW(())
    : formatCode(formatCode_)
    , formatName(formatName_)
    , formatKey(formatKey_)
    , formatType(formatType_)
    , formatUsage(formatUsage_)
    , formatIndex(formatIndex_)
    , isDefault(isDefault_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theFormatElementType : public rtl::StaticWithInit< ::css::uno::Type *, theFormatElementType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.FormatElement" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "formatCode" );
        ::rtl::OUString the_name1( "formatName" );
        ::rtl::OUString the_name2( "formatKey" );
        ::rtl::OUString the_name3( "formatType" );
        ::rtl::OUString the_name4( "formatUsage" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name5( "formatIndex" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name6( "isDefault" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::FormatElement const *) {
    return *detail::theFormatElementType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::FormatElement const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::FormatElement >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_FORMATELEMENT_HPP
