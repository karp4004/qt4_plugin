#ifndef INCLUDED_COM_SUN_STAR_I18N_DIRECTIONPROPERTY_HPP
#define INCLUDED_COM_SUN_STAR_I18N_DIRECTIONPROPERTY_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/DirectionProperty.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theDirectionPropertyType : public rtl::StaticWithInit< ::css::uno::Type *, theDirectionPropertyType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.i18n.DirectionProperty" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[19];
        ::rtl::OUString sEnumValue0( "LEFT_TO_RIGHT" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "RIGHT_TO_LEFT" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "EUROPEAN_NUMBER" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "EUROPEAN_NUMBER_SEPARATOR" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "EUROPEAN_NUMBER_TERMINATOR" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "ARABIC_NUMBER" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "COMMON_NUMBER_SEPARATOR" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "BLOCK_SEPARATOR" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "SEGMENT_SEPARATOR" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "WHITE_SPACE_NEUTRAL" );
        enumValueNames[9] = sEnumValue9.pData;
        ::rtl::OUString sEnumValue10( "OTHER_NEUTRAL" );
        enumValueNames[10] = sEnumValue10.pData;
        ::rtl::OUString sEnumValue11( "LEFT_TO_RIGHT_EMBEDDING" );
        enumValueNames[11] = sEnumValue11.pData;
        ::rtl::OUString sEnumValue12( "LEFT_TO_RIGHT_OVERRIDE" );
        enumValueNames[12] = sEnumValue12.pData;
        ::rtl::OUString sEnumValue13( "RIGHT_TO_LEFT_ARABIC" );
        enumValueNames[13] = sEnumValue13.pData;
        ::rtl::OUString sEnumValue14( "RIGHT_TO_LEFT_EMBEDDING" );
        enumValueNames[14] = sEnumValue14.pData;
        ::rtl::OUString sEnumValue15( "RIGHT_TO_LEFT_OVERRIDE" );
        enumValueNames[15] = sEnumValue15.pData;
        ::rtl::OUString sEnumValue16( "POP_DIRECTIONAL_FORMAT" );
        enumValueNames[16] = sEnumValue16.pData;
        ::rtl::OUString sEnumValue17( "DIR_NON_SPACING_MARK" );
        enumValueNames[17] = sEnumValue17.pData;
        ::rtl::OUString sEnumValue18( "BOUNDARY_NEUTRAL" );
        enumValueNames[18] = sEnumValue18.pData;

        sal_Int32 enumValues[19];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;
        enumValues[9] = 9;
        enumValues[10] = 10;
        enumValues[11] = 11;
        enumValues[12] = 12;
        enumValues[13] = 13;
        enumValues[14] = 14;
        enumValues[15] = 15;
        enumValues[16] = 16;
        enumValues[17] = 17;
        enumValues[18] = 18;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::i18n::DirectionProperty_LEFT_TO_RIGHT,
            19, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::DirectionProperty const *) {
    return *detail::theDirectionPropertyType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::DirectionProperty const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::DirectionProperty >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_DIRECTIONPROPERTY_HPP
