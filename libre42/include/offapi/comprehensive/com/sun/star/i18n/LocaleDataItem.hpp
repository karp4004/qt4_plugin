#ifndef INCLUDED_COM_SUN_STAR_I18N_LOCALEDATAITEM_HPP
#define INCLUDED_COM_SUN_STAR_I18N_LOCALEDATAITEM_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/LocaleDataItem.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline LocaleDataItem::LocaleDataItem() SAL_THROW(())
    : unoID()
    , dateSeparator()
    , thousandSeparator()
    , decimalSeparator()
    , timeSeparator()
    , time100SecSeparator()
    , listSeparator()
    , quotationStart()
    , quotationEnd()
    , doubleQuotationStart()
    , doubleQuotationEnd()
    , timeAM()
    , timePM()
    , measurementSystem()
    , LongDateDayOfWeekSeparator()
    , LongDateDaySeparator()
    , LongDateMonthSeparator()
    , LongDateYearSeparator()
{
}

inline LocaleDataItem::LocaleDataItem(const ::rtl::OUString& unoID_, const ::rtl::OUString& dateSeparator_, const ::rtl::OUString& thousandSeparator_, const ::rtl::OUString& decimalSeparator_, const ::rtl::OUString& timeSeparator_, const ::rtl::OUString& time100SecSeparator_, const ::rtl::OUString& listSeparator_, const ::rtl::OUString& quotationStart_, const ::rtl::OUString& quotationEnd_, const ::rtl::OUString& doubleQuotationStart_, const ::rtl::OUString& doubleQuotationEnd_, const ::rtl::OUString& timeAM_, const ::rtl::OUString& timePM_, const ::rtl::OUString& measurementSystem_, const ::rtl::OUString& LongDateDayOfWeekSeparator_, const ::rtl::OUString& LongDateDaySeparator_, const ::rtl::OUString& LongDateMonthSeparator_, const ::rtl::OUString& LongDateYearSeparator_) SAL_THROW(())
    : unoID(unoID_)
    , dateSeparator(dateSeparator_)
    , thousandSeparator(thousandSeparator_)
    , decimalSeparator(decimalSeparator_)
    , timeSeparator(timeSeparator_)
    , time100SecSeparator(time100SecSeparator_)
    , listSeparator(listSeparator_)
    , quotationStart(quotationStart_)
    , quotationEnd(quotationEnd_)
    , doubleQuotationStart(doubleQuotationStart_)
    , doubleQuotationEnd(doubleQuotationEnd_)
    , timeAM(timeAM_)
    , timePM(timePM_)
    , measurementSystem(measurementSystem_)
    , LongDateDayOfWeekSeparator(LongDateDayOfWeekSeparator_)
    , LongDateDaySeparator(LongDateDaySeparator_)
    , LongDateMonthSeparator(LongDateMonthSeparator_)
    , LongDateYearSeparator(LongDateYearSeparator_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theLocaleDataItemType : public rtl::StaticWithInit< ::css::uno::Type *, theLocaleDataItemType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.LocaleDataItem" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "unoID" );
        ::rtl::OUString the_name1( "dateSeparator" );
        ::rtl::OUString the_name2( "thousandSeparator" );
        ::rtl::OUString the_name3( "decimalSeparator" );
        ::rtl::OUString the_name4( "timeSeparator" );
        ::rtl::OUString the_name5( "time100SecSeparator" );
        ::rtl::OUString the_name6( "listSeparator" );
        ::rtl::OUString the_name7( "quotationStart" );
        ::rtl::OUString the_name8( "quotationEnd" );
        ::rtl::OUString the_name9( "doubleQuotationStart" );
        ::rtl::OUString the_name10( "doubleQuotationEnd" );
        ::rtl::OUString the_name11( "timeAM" );
        ::rtl::OUString the_name12( "timePM" );
        ::rtl::OUString the_name13( "measurementSystem" );
        ::rtl::OUString the_name14( "LongDateDayOfWeekSeparator" );
        ::rtl::OUString the_name15( "LongDateDaySeparator" );
        ::rtl::OUString the_name16( "LongDateMonthSeparator" );
        ::rtl::OUString the_name17( "LongDateYearSeparator" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name6.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name7.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name8.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name9.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name10.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name11.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name12.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name13.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name14.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name15.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name16.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name17.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 18, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::LocaleDataItem const *) {
    return *detail::theLocaleDataItemType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::LocaleDataItem const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::LocaleDataItem >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_LOCALEDATAITEM_HPP
