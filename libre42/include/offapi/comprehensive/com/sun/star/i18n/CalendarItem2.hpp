#ifndef INCLUDED_COM_SUN_STAR_I18N_CALENDARITEM2_HPP
#define INCLUDED_COM_SUN_STAR_I18N_CALENDARITEM2_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/CalendarItem2.hdl"

#include "com/sun/star/i18n/CalendarItem.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline CalendarItem2::CalendarItem2() SAL_THROW(())
    : ::css::i18n::CalendarItem()
    , NarrowName()
{
}

inline CalendarItem2::CalendarItem2(const ::rtl::OUString& ID_, const ::rtl::OUString& AbbrevName_, const ::rtl::OUString& FullName_, const ::rtl::OUString& NarrowName_) SAL_THROW(())
    : ::css::i18n::CalendarItem(ID_, AbbrevName_, FullName_)
    , NarrowName(NarrowName_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theCalendarItem2Type : public rtl::StaticWithInit< ::css::uno::Type *, theCalendarItem2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.CalendarItem2" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "NarrowName" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::i18n::CalendarItem >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::CalendarItem2 const *) {
    return *detail::theCalendarItem2Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::CalendarItem2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::CalendarItem2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_CALENDARITEM2_HPP
