#ifndef INCLUDED_COM_SUN_STAR_I18N_CURRENCY_HPP
#define INCLUDED_COM_SUN_STAR_I18N_CURRENCY_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/Currency.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline Currency::Currency() SAL_THROW(())
    : ID()
    , Symbol()
    , BankSymbol()
    , Name()
    , Default(false)
    , UsedInCompatibleFormatCodes(false)
    , DecimalPlaces(0)
{
}

inline Currency::Currency(const ::rtl::OUString& ID_, const ::rtl::OUString& Symbol_, const ::rtl::OUString& BankSymbol_, const ::rtl::OUString& Name_, const ::sal_Bool& Default_, const ::sal_Bool& UsedInCompatibleFormatCodes_, const ::sal_Int16& DecimalPlaces_) SAL_THROW(())
    : ID(ID_)
    , Symbol(Symbol_)
    , BankSymbol(BankSymbol_)
    , Name(Name_)
    , Default(Default_)
    , UsedInCompatibleFormatCodes(UsedInCompatibleFormatCodes_)
    , DecimalPlaces(DecimalPlaces_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theCurrencyType : public rtl::StaticWithInit< ::css::uno::Type *, theCurrencyType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.Currency" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "ID" );
        ::rtl::OUString the_name1( "Symbol" );
        ::rtl::OUString the_name2( "BankSymbol" );
        ::rtl::OUString the_name3( "Name" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name4( "Default" );
        ::rtl::OUString the_name5( "UsedInCompatibleFormatCodes" );
        ::rtl::OUString the_tname2( "short" );
        ::rtl::OUString the_name6( "DecimalPlaces" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::Currency const *) {
    return *detail::theCurrencyType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::Currency const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::Currency >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_CURRENCY_HPP
