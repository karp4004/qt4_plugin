#ifndef INCLUDED_COM_SUN_STAR_I18N_TRANSLITERATIONMODULESNEW_HPP
#define INCLUDED_COM_SUN_STAR_I18N_TRANSLITERATIONMODULESNEW_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/TransliterationModulesNew.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theTransliterationModulesNewType : public rtl::StaticWithInit< ::css::uno::Type *, theTransliterationModulesNewType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.i18n.TransliterationModulesNew" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[64];
        ::rtl::OUString sEnumValue0( "UPPERCASE_LOWERCASE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "LOWERCASE_UPPERCASE" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "HALFWIDTH_FULLWIDTH" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "FULLWIDTH_HALFWIDTH" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "KATAKANA_HIRAGANA" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "HIRAGANA_KATAKANA" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "IGNORE_CASE" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "IGNORE_KANA" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "IGNORE_WIDTH" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "ignoreTraditionalKanji_ja_JP" );
        enumValueNames[9] = sEnumValue9.pData;
        ::rtl::OUString sEnumValue10( "ignoreTraditionalKana_ja_JP" );
        enumValueNames[10] = sEnumValue10.pData;
        ::rtl::OUString sEnumValue11( "ignoreMinusSign_ja_JP" );
        enumValueNames[11] = sEnumValue11.pData;
        ::rtl::OUString sEnumValue12( "ignoreIterationMark_ja_JP" );
        enumValueNames[12] = sEnumValue12.pData;
        ::rtl::OUString sEnumValue13( "ignoreSeparator_ja_JP" );
        enumValueNames[13] = sEnumValue13.pData;
        ::rtl::OUString sEnumValue14( "ignoreZiZu_ja_JP" );
        enumValueNames[14] = sEnumValue14.pData;
        ::rtl::OUString sEnumValue15( "ignoreBaFa_ja_JP" );
        enumValueNames[15] = sEnumValue15.pData;
        ::rtl::OUString sEnumValue16( "ignoreTiJi_ja_JP" );
        enumValueNames[16] = sEnumValue16.pData;
        ::rtl::OUString sEnumValue17( "ignoreHyuByu_ja_JP" );
        enumValueNames[17] = sEnumValue17.pData;
        ::rtl::OUString sEnumValue18( "ignoreSeZe_ja_JP" );
        enumValueNames[18] = sEnumValue18.pData;
        ::rtl::OUString sEnumValue19( "ignoreIandEfollowedByYa_ja_JP" );
        enumValueNames[19] = sEnumValue19.pData;
        ::rtl::OUString sEnumValue20( "ignoreKiKuFollowedBySa_ja_JP" );
        enumValueNames[20] = sEnumValue20.pData;
        ::rtl::OUString sEnumValue21( "ignoreSize_ja_JP" );
        enumValueNames[21] = sEnumValue21.pData;
        ::rtl::OUString sEnumValue22( "ignoreProlongedSoundMark_ja_JP" );
        enumValueNames[22] = sEnumValue22.pData;
        ::rtl::OUString sEnumValue23( "ignoreMiddleDot_ja_JP" );
        enumValueNames[23] = sEnumValue23.pData;
        ::rtl::OUString sEnumValue24( "ignoreSpace_ja_JP" );
        enumValueNames[24] = sEnumValue24.pData;
        ::rtl::OUString sEnumValue25( "smallToLarge_ja_JP" );
        enumValueNames[25] = sEnumValue25.pData;
        ::rtl::OUString sEnumValue26( "largeToSmall_ja_JP" );
        enumValueNames[26] = sEnumValue26.pData;
        ::rtl::OUString sEnumValue27( "NumToTextLower_zh_CN" );
        enumValueNames[27] = sEnumValue27.pData;
        ::rtl::OUString sEnumValue28( "NumToTextUpper_zh_CN" );
        enumValueNames[28] = sEnumValue28.pData;
        ::rtl::OUString sEnumValue29( "NumToTextLower_zh_TW" );
        enumValueNames[29] = sEnumValue29.pData;
        ::rtl::OUString sEnumValue30( "NumToTextUpper_zh_TW" );
        enumValueNames[30] = sEnumValue30.pData;
        ::rtl::OUString sEnumValue31( "NumToTextFormalHangul_ko" );
        enumValueNames[31] = sEnumValue31.pData;
        ::rtl::OUString sEnumValue32( "NumToTextFormalLower_ko" );
        enumValueNames[32] = sEnumValue32.pData;
        ::rtl::OUString sEnumValue33( "NumToTextFormalUpper_ko" );
        enumValueNames[33] = sEnumValue33.pData;
        ::rtl::OUString sEnumValue34( "NumToTextInformalHangul_ko" );
        enumValueNames[34] = sEnumValue34.pData;
        ::rtl::OUString sEnumValue35( "NumToTextInformalLower_ko" );
        enumValueNames[35] = sEnumValue35.pData;
        ::rtl::OUString sEnumValue36( "NumToTextInformalUpper_ko" );
        enumValueNames[36] = sEnumValue36.pData;
        ::rtl::OUString sEnumValue37( "NumToCharLower_zh_CN" );
        enumValueNames[37] = sEnumValue37.pData;
        ::rtl::OUString sEnumValue38( "NumToCharUpper_zh_CN" );
        enumValueNames[38] = sEnumValue38.pData;
        ::rtl::OUString sEnumValue39( "NumToCharLower_zh_TW" );
        enumValueNames[39] = sEnumValue39.pData;
        ::rtl::OUString sEnumValue40( "NumToCharUpper_zh_TW" );
        enumValueNames[40] = sEnumValue40.pData;
        ::rtl::OUString sEnumValue41( "NumToCharHangul_ko" );
        enumValueNames[41] = sEnumValue41.pData;
        ::rtl::OUString sEnumValue42( "NumToCharLower_ko" );
        enumValueNames[42] = sEnumValue42.pData;
        ::rtl::OUString sEnumValue43( "NumToCharUpper_ko" );
        enumValueNames[43] = sEnumValue43.pData;
        ::rtl::OUString sEnumValue44( "NumToCharFullwidth" );
        enumValueNames[44] = sEnumValue44.pData;
        ::rtl::OUString sEnumValue45( "NumToCharKanjiShort_ja_JP" );
        enumValueNames[45] = sEnumValue45.pData;
        ::rtl::OUString sEnumValue46( "TextToNumLower_zh_CN" );
        enumValueNames[46] = sEnumValue46.pData;
        ::rtl::OUString sEnumValue47( "TextToNumUpper_zh_CN" );
        enumValueNames[47] = sEnumValue47.pData;
        ::rtl::OUString sEnumValue48( "TextToNumLower_zh_TW" );
        enumValueNames[48] = sEnumValue48.pData;
        ::rtl::OUString sEnumValue49( "TextToNumUpper_zh_TW" );
        enumValueNames[49] = sEnumValue49.pData;
        ::rtl::OUString sEnumValue50( "TextToNumFormalHangul_ko" );
        enumValueNames[50] = sEnumValue50.pData;
        ::rtl::OUString sEnumValue51( "TextToNumFormalLower_ko" );
        enumValueNames[51] = sEnumValue51.pData;
        ::rtl::OUString sEnumValue52( "TextToNumFormalUpper_ko" );
        enumValueNames[52] = sEnumValue52.pData;
        ::rtl::OUString sEnumValue53( "TextToNumInformalHangul_ko" );
        enumValueNames[53] = sEnumValue53.pData;
        ::rtl::OUString sEnumValue54( "TextToNumInformalLower_ko" );
        enumValueNames[54] = sEnumValue54.pData;
        ::rtl::OUString sEnumValue55( "TextToNumInformalUpper_ko" );
        enumValueNames[55] = sEnumValue55.pData;
        ::rtl::OUString sEnumValue56( "CharToNumLower_zh_CN" );
        enumValueNames[56] = sEnumValue56.pData;
        ::rtl::OUString sEnumValue57( "CharToNumUpper_zh_CN" );
        enumValueNames[57] = sEnumValue57.pData;
        ::rtl::OUString sEnumValue58( "CharToNumLower_zh_TW" );
        enumValueNames[58] = sEnumValue58.pData;
        ::rtl::OUString sEnumValue59( "CharToNumUpper_zh_TW" );
        enumValueNames[59] = sEnumValue59.pData;
        ::rtl::OUString sEnumValue60( "CharToNumHangul_ko" );
        enumValueNames[60] = sEnumValue60.pData;
        ::rtl::OUString sEnumValue61( "CharToNumLower_ko" );
        enumValueNames[61] = sEnumValue61.pData;
        ::rtl::OUString sEnumValue62( "CharToNumUpper_ko" );
        enumValueNames[62] = sEnumValue62.pData;
        ::rtl::OUString sEnumValue63( "END_OF_MODULE" );
        enumValueNames[63] = sEnumValue63.pData;

        sal_Int32 enumValues[64];
        enumValues[0] = 1;
        enumValues[1] = 2;
        enumValues[2] = 3;
        enumValues[3] = 4;
        enumValues[4] = 5;
        enumValues[5] = 6;
        enumValues[6] = 7;
        enumValues[7] = 8;
        enumValues[8] = 9;
        enumValues[9] = 10;
        enumValues[10] = 11;
        enumValues[11] = 12;
        enumValues[12] = 13;
        enumValues[13] = 14;
        enumValues[14] = 15;
        enumValues[15] = 16;
        enumValues[16] = 17;
        enumValues[17] = 18;
        enumValues[18] = 19;
        enumValues[19] = 20;
        enumValues[20] = 21;
        enumValues[21] = 22;
        enumValues[22] = 23;
        enumValues[23] = 24;
        enumValues[24] = 25;
        enumValues[25] = 26;
        enumValues[26] = 27;
        enumValues[27] = 28;
        enumValues[28] = 29;
        enumValues[29] = 30;
        enumValues[30] = 31;
        enumValues[31] = 32;
        enumValues[32] = 33;
        enumValues[33] = 34;
        enumValues[34] = 35;
        enumValues[35] = 36;
        enumValues[36] = 37;
        enumValues[37] = 38;
        enumValues[38] = 39;
        enumValues[39] = 40;
        enumValues[40] = 41;
        enumValues[41] = 42;
        enumValues[42] = 43;
        enumValues[43] = 44;
        enumValues[44] = 45;
        enumValues[45] = 46;
        enumValues[46] = 47;
        enumValues[47] = 48;
        enumValues[48] = 49;
        enumValues[49] = 50;
        enumValues[50] = 51;
        enumValues[51] = 52;
        enumValues[52] = 53;
        enumValues[53] = 54;
        enumValues[54] = 55;
        enumValues[55] = 56;
        enumValues[56] = 59;
        enumValues[57] = 60;
        enumValues[58] = 61;
        enumValues[59] = 62;
        enumValues[60] = 63;
        enumValues[61] = 64;
        enumValues[62] = 65;
        enumValues[63] = 0;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::i18n::TransliterationModulesNew_UPPERCASE_LOWERCASE,
            64, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::TransliterationModulesNew const *) {
    return *detail::theTransliterationModulesNewType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::TransliterationModulesNew const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::TransliterationModulesNew >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_TRANSLITERATIONMODULESNEW_HPP
