#ifndef INCLUDED_COM_SUN_STAR_I18N_NATIVENUMBERXMLATTRIBUTES_HPP
#define INCLUDED_COM_SUN_STAR_I18N_NATIVENUMBERXMLATTRIBUTES_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/NativeNumberXmlAttributes.hdl"

#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace i18n {

inline NativeNumberXmlAttributes::NativeNumberXmlAttributes() SAL_THROW(())
    : Locale()
    , Format()
    , Style()
{
}

inline NativeNumberXmlAttributes::NativeNumberXmlAttributes(const ::css::lang::Locale& Locale_, const ::rtl::OUString& Format_, const ::rtl::OUString& Style_) SAL_THROW(())
    : Locale(Locale_)
    , Format(Format_)
    , Style(Style_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theNativeNumberXmlAttributesType : public rtl::StaticWithInit< ::css::uno::Type *, theNativeNumberXmlAttributesType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.i18n.NativeNumberXmlAttributes" );
        ::cppu::UnoType< ::css::lang::Locale >::get();
        ::rtl::OUString the_tname0( "com.sun.star.lang.Locale" );
        ::rtl::OUString the_name0( "Locale" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "Format" );
        ::rtl::OUString the_name2( "Style" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::NativeNumberXmlAttributes const *) {
    return *detail::theNativeNumberXmlAttributesType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::NativeNumberXmlAttributes const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::NativeNumberXmlAttributes >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_NATIVENUMBERXMLATTRIBUTES_HPP
