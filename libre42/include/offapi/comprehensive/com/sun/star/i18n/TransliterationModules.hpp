#ifndef INCLUDED_COM_SUN_STAR_I18N_TRANSLITERATIONMODULES_HPP
#define INCLUDED_COM_SUN_STAR_I18N_TRANSLITERATIONMODULES_HPP

#include "sal/config.h"

#include "com/sun/star/i18n/TransliterationModules.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace i18n { namespace detail {

struct theTransliterationModulesType : public rtl::StaticWithInit< ::css::uno::Type *, theTransliterationModulesType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.i18n.TransliterationModules" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[37];
        ::rtl::OUString sEnumValue0( "UPPERCASE_LOWERCASE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "LOWERCASE_UPPERCASE" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "HALFWIDTH_FULLWIDTH" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "FULLWIDTH_HALFWIDTH" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "KATAKANA_HIRAGANA" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "HIRAGANA_KATAKANA" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "NumToTextLower_zh_CN" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "NumToTextUpper_zh_CN" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "NumToTextLower_zh_TW" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "NumToTextUpper_zh_TW" );
        enumValueNames[9] = sEnumValue9.pData;
        ::rtl::OUString sEnumValue10( "NumToTextFormalHangul_ko" );
        enumValueNames[10] = sEnumValue10.pData;
        ::rtl::OUString sEnumValue11( "NumToTextFormalLower_ko" );
        enumValueNames[11] = sEnumValue11.pData;
        ::rtl::OUString sEnumValue12( "NumToTextFormalUpper_ko" );
        enumValueNames[12] = sEnumValue12.pData;
        ::rtl::OUString sEnumValue13( "NON_IGNORE_MASK" );
        enumValueNames[13] = sEnumValue13.pData;
        ::rtl::OUString sEnumValue14( "IGNORE_MASK" );
        enumValueNames[14] = sEnumValue14.pData;
        ::rtl::OUString sEnumValue15( "IGNORE_CASE" );
        enumValueNames[15] = sEnumValue15.pData;
        ::rtl::OUString sEnumValue16( "IGNORE_KANA" );
        enumValueNames[16] = sEnumValue16.pData;
        ::rtl::OUString sEnumValue17( "IGNORE_WIDTH" );
        enumValueNames[17] = sEnumValue17.pData;
        ::rtl::OUString sEnumValue18( "ignoreTraditionalKanji_ja_JP" );
        enumValueNames[18] = sEnumValue18.pData;
        ::rtl::OUString sEnumValue19( "ignoreTraditionalKana_ja_JP" );
        enumValueNames[19] = sEnumValue19.pData;
        ::rtl::OUString sEnumValue20( "ignoreMinusSign_ja_JP" );
        enumValueNames[20] = sEnumValue20.pData;
        ::rtl::OUString sEnumValue21( "ignoreIterationMark_ja_JP" );
        enumValueNames[21] = sEnumValue21.pData;
        ::rtl::OUString sEnumValue22( "ignoreSeparator_ja_JP" );
        enumValueNames[22] = sEnumValue22.pData;
        ::rtl::OUString sEnumValue23( "ignoreZiZu_ja_JP" );
        enumValueNames[23] = sEnumValue23.pData;
        ::rtl::OUString sEnumValue24( "ignoreBaFa_ja_JP" );
        enumValueNames[24] = sEnumValue24.pData;
        ::rtl::OUString sEnumValue25( "ignoreTiJi_ja_JP" );
        enumValueNames[25] = sEnumValue25.pData;
        ::rtl::OUString sEnumValue26( "ignoreHyuByu_ja_JP" );
        enumValueNames[26] = sEnumValue26.pData;
        ::rtl::OUString sEnumValue27( "ignoreSeZe_ja_JP" );
        enumValueNames[27] = sEnumValue27.pData;
        ::rtl::OUString sEnumValue28( "ignoreIandEfollowedByYa_ja_JP" );
        enumValueNames[28] = sEnumValue28.pData;
        ::rtl::OUString sEnumValue29( "ignoreKiKuFollowedBySa_ja_JP" );
        enumValueNames[29] = sEnumValue29.pData;
        ::rtl::OUString sEnumValue30( "ignoreSize_ja_JP" );
        enumValueNames[30] = sEnumValue30.pData;
        ::rtl::OUString sEnumValue31( "ignoreProlongedSoundMark_ja_JP" );
        enumValueNames[31] = sEnumValue31.pData;
        ::rtl::OUString sEnumValue32( "ignoreMiddleDot_ja_JP" );
        enumValueNames[32] = sEnumValue32.pData;
        ::rtl::OUString sEnumValue33( "ignoreSpace_ja_JP" );
        enumValueNames[33] = sEnumValue33.pData;
        ::rtl::OUString sEnumValue34( "smallToLarge_ja_JP" );
        enumValueNames[34] = sEnumValue34.pData;
        ::rtl::OUString sEnumValue35( "largeToSmall_ja_JP" );
        enumValueNames[35] = sEnumValue35.pData;
        ::rtl::OUString sEnumValue36( "END_OF_MODULE" );
        enumValueNames[36] = sEnumValue36.pData;

        sal_Int32 enumValues[37];
        enumValues[0] = 1;
        enumValues[1] = 2;
        enumValues[2] = 3;
        enumValues[3] = 4;
        enumValues[4] = 5;
        enumValues[5] = 6;
        enumValues[6] = 7;
        enumValues[7] = 8;
        enumValues[8] = 9;
        enumValues[9] = 10;
        enumValues[10] = 11;
        enumValues[11] = 12;
        enumValues[12] = 13;
        enumValues[13] = 255;
        enumValues[14] = -256;
        enumValues[15] = 256;
        enumValues[16] = 512;
        enumValues[17] = 1024;
        enumValues[18] = 4096;
        enumValues[19] = 8192;
        enumValues[20] = 16384;
        enumValues[21] = 32768;
        enumValues[22] = 65536;
        enumValues[23] = 131072;
        enumValues[24] = 262144;
        enumValues[25] = 524288;
        enumValues[26] = 1048576;
        enumValues[27] = 2097152;
        enumValues[28] = 4194304;
        enumValues[29] = 8388608;
        enumValues[30] = 16777216;
        enumValues[31] = 33554432;
        enumValues[32] = 67108864;
        enumValues[33] = 134217728;
        enumValues[34] = 268435456;
        enumValues[35] = 536870912;
        enumValues[36] = 0;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::i18n::TransliterationModules_UPPERCASE_LOWERCASE,
            37, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace i18n {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::i18n::TransliterationModules const *) {
    return *detail::theTransliterationModulesType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::i18n::TransliterationModules const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::i18n::TransliterationModules >::get();
}

#endif // INCLUDED_COM_SUN_STAR_I18N_TRANSLITERATIONMODULES_HPP
