#ifndef INCLUDED_COM_SUN_STAR_CHART_CHARTREGRESSIONCURVETYPE_HPP
#define INCLUDED_COM_SUN_STAR_CHART_CHARTREGRESSIONCURVETYPE_HPP

#include "sal/config.h"

#include "com/sun/star/chart/ChartRegressionCurveType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace chart { namespace detail {

struct theChartRegressionCurveTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theChartRegressionCurveTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.chart.ChartRegressionCurveType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[6];
        ::rtl::OUString sEnumValue0( "NONE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "LINEAR" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "LOGARITHM" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "EXPONENTIAL" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "POLYNOMIAL" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "POWER" );
        enumValueNames[5] = sEnumValue5.pData;

        sal_Int32 enumValues[6];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::chart::ChartRegressionCurveType_NONE,
            6, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::ChartRegressionCurveType const *) {
    return *detail::theChartRegressionCurveTypeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::ChartRegressionCurveType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::ChartRegressionCurveType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_CHARTREGRESSIONCURVETYPE_HPP
