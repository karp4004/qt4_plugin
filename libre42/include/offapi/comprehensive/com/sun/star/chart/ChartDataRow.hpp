#ifndef INCLUDED_COM_SUN_STAR_CHART_CHARTDATAROW_HPP
#define INCLUDED_COM_SUN_STAR_CHART_CHARTDATAROW_HPP

#include "sal/config.h"

#include "com/sun/star/chart/ChartDataRow.hdl"

#include "com/sun/star/chart/ChartDataValue.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart {

inline ChartDataRow::ChartDataRow() SAL_THROW(())
    : Name()
    , Points()
{
}

inline ChartDataRow::ChartDataRow(const ::rtl::OUString& Name_, const ::css::uno::Sequence< ::css::uno::Sequence< ::css::chart::ChartDataValue > >& Points_) SAL_THROW(())
    : Name(Name_)
    , Points(Points_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart { namespace detail {

struct theChartDataRowType : public rtl::StaticWithInit< ::css::uno::Type *, theChartDataRowType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart.ChartDataRow" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::cppu::UnoSequenceType< ::css::chart::ChartDataValue > > >::get();
        ::rtl::OUString the_tname1( "[][]com.sun.star.chart.ChartDataValue" );
        ::rtl::OUString the_name1( "Points" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::ChartDataRow const *) {
    return *detail::theChartDataRowType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::ChartDataRow const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::ChartDataRow >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_CHARTDATAROW_HPP
