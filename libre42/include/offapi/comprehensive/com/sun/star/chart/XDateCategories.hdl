#ifndef INCLUDED_COM_SUN_STAR_CHART_XDATECATEGORIES_HDL
#define INCLUDED_COM_SUN_STAR_CHART_XDATECATEGORIES_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart {

class SAL_NO_VTABLE XDateCategories : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setDateCategories( const ::css::uno::Sequence< double >& rDates ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< double > SAL_CALL getDateCategories() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDateCategories() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::XDateCategories const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart::XDateCategories > *) SAL_THROW(());

#endif
