#ifndef INCLUDED_COM_SUN_STAR_CHART_CHARTERRORINDICATORTYPE_HPP
#define INCLUDED_COM_SUN_STAR_CHART_CHARTERRORINDICATORTYPE_HPP

#include "sal/config.h"

#include "com/sun/star/chart/ChartErrorIndicatorType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace chart { namespace detail {

struct theChartErrorIndicatorTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theChartErrorIndicatorTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.chart.ChartErrorIndicatorType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[4];
        ::rtl::OUString sEnumValue0( "NONE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "TOP_AND_BOTTOM" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "UPPER" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "LOWER" );
        enumValueNames[3] = sEnumValue3.pData;

        sal_Int32 enumValues[4];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::chart::ChartErrorIndicatorType_NONE,
            4, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::ChartErrorIndicatorType const *) {
    return *detail::theChartErrorIndicatorTypeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::ChartErrorIndicatorType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::ChartErrorIndicatorType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_CHARTERRORINDICATORTYPE_HPP
