#ifndef INCLUDED_COM_SUN_STAR_CHART_TIMEINTERVAL_HPP
#define INCLUDED_COM_SUN_STAR_CHART_TIMEINTERVAL_HPP

#include "sal/config.h"

#include "com/sun/star/chart/TimeInterval.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart {

inline TimeInterval::TimeInterval() SAL_THROW(())
    : Number(0)
    , TimeUnit(0)
{
}

inline TimeInterval::TimeInterval(const ::sal_Int32& Number_, const ::sal_Int32& TimeUnit_) SAL_THROW(())
    : Number(Number_)
    , TimeUnit(TimeUnit_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart { namespace detail {

struct theTimeIntervalType : public rtl::StaticWithInit< ::css::uno::Type *, theTimeIntervalType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart.TimeInterval" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Number" );
        ::rtl::OUString the_name1( "TimeUnit" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::TimeInterval const *) {
    return *detail::theTimeIntervalType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::TimeInterval const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::TimeInterval >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_TIMEINTERVAL_HPP
