#ifndef INCLUDED_COM_SUN_STAR_CHART_XSTATISTICDISPLAY_HDL
#define INCLUDED_COM_SUN_STAR_CHART_XSTATISTICDISPLAY_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace beans { class XPropertySet; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart {

class SAL_NO_VTABLE XStatisticDisplay : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::beans::XPropertySet > SAL_CALL getUpBar() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::beans::XPropertySet > SAL_CALL getDownBar() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::beans::XPropertySet > SAL_CALL getMinMaxLine() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XStatisticDisplay() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::XStatisticDisplay const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart::XStatisticDisplay > *) SAL_THROW(());

#endif
