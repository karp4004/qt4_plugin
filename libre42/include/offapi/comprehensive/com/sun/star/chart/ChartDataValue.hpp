#ifndef INCLUDED_COM_SUN_STAR_CHART_CHARTDATAVALUE_HPP
#define INCLUDED_COM_SUN_STAR_CHART_CHARTDATAVALUE_HPP

#include "sal/config.h"

#include "com/sun/star/chart/ChartDataValue.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart {

inline ChartDataValue::ChartDataValue() SAL_THROW(())
    : Value(0)
    , HighError(0)
    , LowError(0)
{
}

inline ChartDataValue::ChartDataValue(const double& Value_, const double& HighError_, const double& LowError_) SAL_THROW(())
    : Value(Value_)
    , HighError(HighError_)
    , LowError(LowError_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart { namespace detail {

struct theChartDataValueType : public rtl::StaticWithInit< ::css::uno::Type *, theChartDataValueType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart.ChartDataValue" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Value" );
        ::rtl::OUString the_name1( "HighError" );
        ::rtl::OUString the_name2( "LowError" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::ChartDataValue const *) {
    return *detail::theChartDataValueType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::ChartDataValue const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::ChartDataValue >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_CHARTDATAVALUE_HPP
