#ifndef INCLUDED_COM_SUN_STAR_CHART_CHARTDATACHANGEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_CHART_CHARTDATACHANGEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/chart/ChartDataChangeEvent.hdl"

#include "com/sun/star/chart/ChartDataChangeType.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart {

inline ChartDataChangeEvent::ChartDataChangeEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Type(::css::chart::ChartDataChangeType_ALL)
    , StartColumn(0)
    , EndColumn(0)
    , StartRow(0)
    , EndRow(0)
{
}

inline ChartDataChangeEvent::ChartDataChangeEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::chart::ChartDataChangeType& Type_, const ::sal_Int16& StartColumn_, const ::sal_Int16& EndColumn_, const ::sal_Int16& StartRow_, const ::sal_Int16& EndRow_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Type(Type_)
    , StartColumn(StartColumn_)
    , EndColumn(EndColumn_)
    , StartRow(StartRow_)
    , EndRow(EndRow_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart { namespace detail {

struct theChartDataChangeEventType : public rtl::StaticWithInit< ::css::uno::Type *, theChartDataChangeEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart.ChartDataChangeEvent" );
        ::cppu::UnoType< ::css::chart::ChartDataChangeType >::get();
        ::rtl::OUString the_tname0( "com.sun.star.chart.ChartDataChangeType" );
        ::rtl::OUString the_name0( "Type" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "StartColumn" );
        ::rtl::OUString the_name2( "EndColumn" );
        ::rtl::OUString the_name3( "StartRow" );
        ::rtl::OUString the_name4( "EndRow" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::ChartDataChangeEvent const *) {
    return *detail::theChartDataChangeEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::ChartDataChangeEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::ChartDataChangeEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_CHARTDATACHANGEEVENT_HPP
