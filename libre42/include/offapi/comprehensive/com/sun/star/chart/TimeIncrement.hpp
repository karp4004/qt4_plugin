#ifndef INCLUDED_COM_SUN_STAR_CHART_TIMEINCREMENT_HPP
#define INCLUDED_COM_SUN_STAR_CHART_TIMEINCREMENT_HPP

#include "sal/config.h"

#include "com/sun/star/chart/TimeIncrement.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart {

inline TimeIncrement::TimeIncrement() SAL_THROW(())
    : MajorTimeInterval()
    , MinorTimeInterval()
    , TimeResolution()
{
}

inline TimeIncrement::TimeIncrement(const ::css::uno::Any& MajorTimeInterval_, const ::css::uno::Any& MinorTimeInterval_, const ::css::uno::Any& TimeResolution_) SAL_THROW(())
    : MajorTimeInterval(MajorTimeInterval_)
    , MinorTimeInterval(MinorTimeInterval_)
    , TimeResolution(TimeResolution_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart { namespace detail {

struct theTimeIncrementType : public rtl::StaticWithInit< ::css::uno::Type *, theTimeIncrementType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart.TimeIncrement" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "MajorTimeInterval" );
        ::rtl::OUString the_name1( "MinorTimeInterval" );
        ::rtl::OUString the_name2( "TimeResolution" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::TimeIncrement const *) {
    return *detail::theTimeIncrementType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::TimeIncrement const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::TimeIncrement >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_TIMEINCREMENT_HPP
