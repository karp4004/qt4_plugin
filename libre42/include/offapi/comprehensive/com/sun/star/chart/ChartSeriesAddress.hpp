#ifndef INCLUDED_COM_SUN_STAR_CHART_CHARTSERIESADDRESS_HPP
#define INCLUDED_COM_SUN_STAR_CHART_CHARTSERIESADDRESS_HPP

#include "sal/config.h"

#include "com/sun/star/chart/ChartSeriesAddress.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart {

inline ChartSeriesAddress::ChartSeriesAddress() SAL_THROW(())
    : DataRangeAddress()
    , LabelAddress()
    , DomainRangeAddresses()
{
}

inline ChartSeriesAddress::ChartSeriesAddress(const ::rtl::OUString& DataRangeAddress_, const ::rtl::OUString& LabelAddress_, const ::css::uno::Sequence< ::rtl::OUString >& DomainRangeAddresses_) SAL_THROW(())
    : DataRangeAddress(DataRangeAddress_)
    , LabelAddress(LabelAddress_)
    , DomainRangeAddresses(DomainRangeAddresses_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart { namespace detail {

struct theChartSeriesAddressType : public rtl::StaticWithInit< ::css::uno::Type *, theChartSeriesAddressType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart.ChartSeriesAddress" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "DataRangeAddress" );
        ::rtl::OUString the_name1( "LabelAddress" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
        ::rtl::OUString the_tname1( "[]string" );
        ::rtl::OUString the_name2( "DomainRangeAddresses" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart::ChartSeriesAddress const *) {
    return *detail::theChartSeriesAddressType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart::ChartSeriesAddress const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart::ChartSeriesAddress >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART_CHARTSERIESADDRESS_HPP
