#ifndef INCLUDED_COM_SUN_STAR_TASK_INTERACTIONCLASSIFICATION_HPP
#define INCLUDED_COM_SUN_STAR_TASK_INTERACTIONCLASSIFICATION_HPP

#include "sal/config.h"

#include "com/sun/star/task/InteractionClassification.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theInteractionClassificationType : public rtl::StaticWithInit< ::css::uno::Type *, theInteractionClassificationType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.InteractionClassification" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[4];
        ::rtl::OUString sEnumValue0( "ERROR" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "WARNING" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "INFO" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "QUERY" );
        enumValueNames[3] = sEnumValue3.pData;

        sal_Int32 enumValues[4];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::task::InteractionClassification_ERROR,
            4, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::InteractionClassification const *) {
    return *detail::theInteractionClassificationType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::InteractionClassification const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::InteractionClassification >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_INTERACTIONCLASSIFICATION_HPP
