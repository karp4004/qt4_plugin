#ifndef INCLUDED_COM_SUN_STAR_TASK_NOMASTEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_TASK_NOMASTEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/task/NoMasterException.hdl"

#include "com/sun/star/task/PasswordRequestMode.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline NoMasterException::NoMasterException() SAL_THROW(())
    : ::css::uno::RuntimeException()
    , Mode(::css::task::PasswordRequestMode_PASSWORD_CREATE)
{
    ::cppu::UnoType< ::css::task::NoMasterException >::get();
}

inline NoMasterException::NoMasterException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::PasswordRequestMode& Mode_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
    , Mode(Mode_)
{
    ::cppu::UnoType< ::css::task::NoMasterException >::get();
}

NoMasterException::NoMasterException(NoMasterException const & the_other): ::css::uno::RuntimeException(the_other), Mode(the_other.Mode) {}

NoMasterException::~NoMasterException() {}

NoMasterException & NoMasterException::operator =(NoMasterException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    Mode = the_other.Mode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theNoMasterExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theNoMasterExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.NoMasterException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::RuntimeException >::get();
        ::cppu::UnoType< ::css::task::PasswordRequestMode >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "com.sun.star.task.PasswordRequestMode" );
        ::rtl::OUString sMemberName0( "Mode" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ENUM;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::NoMasterException const *) {
    return *detail::theNoMasterExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::NoMasterException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::NoMasterException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_NOMASTEREXCEPTION_HPP
