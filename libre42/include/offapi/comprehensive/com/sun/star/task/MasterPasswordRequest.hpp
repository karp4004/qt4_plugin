#ifndef INCLUDED_COM_SUN_STAR_TASK_MASTERPASSWORDREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_MASTERPASSWORDREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/MasterPasswordRequest.hdl"

#include "com/sun/star/task/PasswordRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline MasterPasswordRequest::MasterPasswordRequest() SAL_THROW(())
    : ::css::task::PasswordRequest()
{
    ::cppu::UnoType< ::css::task::MasterPasswordRequest >::get();
}

inline MasterPasswordRequest::MasterPasswordRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::task::PasswordRequestMode& Mode_) SAL_THROW(())
    : ::css::task::PasswordRequest(Message_, Context_, Classification_, Mode_)
{
    ::cppu::UnoType< ::css::task::MasterPasswordRequest >::get();
}

MasterPasswordRequest::MasterPasswordRequest(MasterPasswordRequest const & the_other): ::css::task::PasswordRequest(the_other) {}

MasterPasswordRequest::~MasterPasswordRequest() {}

MasterPasswordRequest & MasterPasswordRequest::operator =(MasterPasswordRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::PasswordRequest::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theMasterPasswordRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theMasterPasswordRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.MasterPasswordRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::PasswordRequest >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::MasterPasswordRequest const *) {
    return *detail::theMasterPasswordRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::MasterPasswordRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::MasterPasswordRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_MASTERPASSWORDREQUEST_HPP
