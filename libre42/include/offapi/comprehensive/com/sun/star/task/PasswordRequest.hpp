#ifndef INCLUDED_COM_SUN_STAR_TASK_PASSWORDREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_PASSWORDREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/PasswordRequest.hdl"

#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/task/PasswordRequestMode.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline PasswordRequest::PasswordRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , Mode(::css::task::PasswordRequestMode_PASSWORD_CREATE)
{
    ::cppu::UnoType< ::css::task::PasswordRequest >::get();
}

inline PasswordRequest::PasswordRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::task::PasswordRequestMode& Mode_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , Mode(Mode_)
{
    ::cppu::UnoType< ::css::task::PasswordRequest >::get();
}

PasswordRequest::PasswordRequest(PasswordRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), Mode(the_other.Mode) {}

PasswordRequest::~PasswordRequest() {}

PasswordRequest & PasswordRequest::operator =(PasswordRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    Mode = the_other.Mode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct thePasswordRequestType : public rtl::StaticWithInit< ::css::uno::Type *, thePasswordRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.PasswordRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();
        ::cppu::UnoType< ::css::task::PasswordRequestMode >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "com.sun.star.task.PasswordRequestMode" );
        ::rtl::OUString sMemberName0( "Mode" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ENUM;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::PasswordRequest const *) {
    return *detail::thePasswordRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::PasswordRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::PasswordRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_PASSWORDREQUEST_HPP
