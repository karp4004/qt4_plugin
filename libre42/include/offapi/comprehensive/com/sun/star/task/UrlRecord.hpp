#ifndef INCLUDED_COM_SUN_STAR_TASK_URLRECORD_HPP
#define INCLUDED_COM_SUN_STAR_TASK_URLRECORD_HPP

#include "sal/config.h"

#include "com/sun/star/task/UrlRecord.hdl"

#include "com/sun/star/task/UserRecord.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace task {

inline UrlRecord::UrlRecord() SAL_THROW(())
    : Url()
    , UserList()
{
}

inline UrlRecord::UrlRecord(const ::rtl::OUString& Url_, const ::css::uno::Sequence< ::css::task::UserRecord >& UserList_) SAL_THROW(())
    : Url(Url_)
    , UserList(UserList_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theUrlRecordType : public rtl::StaticWithInit< ::css::uno::Type *, theUrlRecordType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.task.UrlRecord" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Url" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::task::UserRecord > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.task.UserRecord" );
        ::rtl::OUString the_name1( "UserList" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::UrlRecord const *) {
    return *detail::theUrlRecordType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::UrlRecord const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::UrlRecord >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_URLRECORD_HPP
