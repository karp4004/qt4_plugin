#ifndef INCLUDED_COM_SUN_STAR_TASK_USERRECORD_HPP
#define INCLUDED_COM_SUN_STAR_TASK_USERRECORD_HPP

#include "sal/config.h"

#include "com/sun/star/task/UserRecord.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace task {

inline UserRecord::UserRecord() SAL_THROW(())
    : UserName()
    , Passwords()
{
}

inline UserRecord::UserRecord(const ::rtl::OUString& UserName_, const ::css::uno::Sequence< ::rtl::OUString >& Passwords_) SAL_THROW(())
    : UserName(UserName_)
    , Passwords(Passwords_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theUserRecordType : public rtl::StaticWithInit< ::css::uno::Type *, theUserRecordType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.task.UserRecord" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "UserName" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
        ::rtl::OUString the_tname1( "[]string" );
        ::rtl::OUString the_name1( "Passwords" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::UserRecord const *) {
    return *detail::theUserRecordType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::UserRecord const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::UserRecord >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_USERRECORD_HPP
