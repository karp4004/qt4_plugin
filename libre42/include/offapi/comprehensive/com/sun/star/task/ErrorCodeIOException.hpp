#ifndef INCLUDED_COM_SUN_STAR_TASK_ERRORCODEIOEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_TASK_ERRORCODEIOEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/task/ErrorCodeIOException.hdl"

#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace task {

inline ErrorCodeIOException::ErrorCodeIOException() SAL_THROW(())
    : ::css::io::IOException()
    , ErrCode(0)
{
    ::cppu::UnoType< ::css::task::ErrorCodeIOException >::get();
}

inline ErrorCodeIOException::ErrorCodeIOException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& ErrCode_) SAL_THROW(())
    : ::css::io::IOException(Message_, Context_)
    , ErrCode(ErrCode_)
{
    ::cppu::UnoType< ::css::task::ErrorCodeIOException >::get();
}

ErrorCodeIOException::ErrorCodeIOException(ErrorCodeIOException const & the_other): ::css::io::IOException(the_other), ErrCode(the_other.ErrCode) {}

ErrorCodeIOException::~ErrorCodeIOException() {}

ErrorCodeIOException & ErrorCodeIOException::operator =(ErrorCodeIOException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::io::IOException::operator =(the_other);
    ErrCode = the_other.ErrCode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theErrorCodeIOExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theErrorCodeIOExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.ErrorCodeIOException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::io::IOException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "long" );
        ::rtl::OUString sMemberName0( "ErrCode" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::ErrorCodeIOException const *) {
    return *detail::theErrorCodeIOExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::ErrorCodeIOException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::ErrorCodeIOException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_ERRORCODEIOEXCEPTION_HPP
