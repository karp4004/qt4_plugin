#ifndef INCLUDED_COM_SUN_STAR_TASK_FUTUREDOCUMENTVERSIONPRODUCTUPDATEREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_FUTUREDOCUMENTVERSIONPRODUCTUPDATEREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/FutureDocumentVersionProductUpdateRequest.hdl"

#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline FutureDocumentVersionProductUpdateRequest::FutureDocumentVersionProductUpdateRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , DocumentURL()
    , DocumentODFVersion()
{
    ::cppu::UnoType< ::css::task::FutureDocumentVersionProductUpdateRequest >::get();
}

inline FutureDocumentVersionProductUpdateRequest::FutureDocumentVersionProductUpdateRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& DocumentURL_, const ::rtl::OUString& DocumentODFVersion_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , DocumentURL(DocumentURL_)
    , DocumentODFVersion(DocumentODFVersion_)
{
    ::cppu::UnoType< ::css::task::FutureDocumentVersionProductUpdateRequest >::get();
}

FutureDocumentVersionProductUpdateRequest::FutureDocumentVersionProductUpdateRequest(FutureDocumentVersionProductUpdateRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), DocumentURL(the_other.DocumentURL), DocumentODFVersion(the_other.DocumentODFVersion) {}

FutureDocumentVersionProductUpdateRequest::~FutureDocumentVersionProductUpdateRequest() {}

FutureDocumentVersionProductUpdateRequest & FutureDocumentVersionProductUpdateRequest::operator =(FutureDocumentVersionProductUpdateRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    DocumentURL = the_other.DocumentURL;
    DocumentODFVersion = the_other.DocumentODFVersion;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theFutureDocumentVersionProductUpdateRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theFutureDocumentVersionProductUpdateRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.FutureDocumentVersionProductUpdateRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "DocumentURL" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "DocumentODFVersion" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::FutureDocumentVersionProductUpdateRequest const *) {
    return *detail::theFutureDocumentVersionProductUpdateRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::FutureDocumentVersionProductUpdateRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::FutureDocumentVersionProductUpdateRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_FUTUREDOCUMENTVERSIONPRODUCTUPDATEREQUEST_HPP
