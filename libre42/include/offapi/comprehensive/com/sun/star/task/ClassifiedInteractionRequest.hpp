#ifndef INCLUDED_COM_SUN_STAR_TASK_CLASSIFIEDINTERACTIONREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_CLASSIFIEDINTERACTIONREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/ClassifiedInteractionRequest.hdl"

#include "com/sun/star/task/InteractionClassification.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline ClassifiedInteractionRequest::ClassifiedInteractionRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , Classification(::css::task::InteractionClassification_ERROR)
{
    ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();
}

inline ClassifiedInteractionRequest::ClassifiedInteractionRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Classification(Classification_)
{
    ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();
}

ClassifiedInteractionRequest::ClassifiedInteractionRequest(ClassifiedInteractionRequest const & the_other): ::css::uno::Exception(the_other), Classification(the_other.Classification) {}

ClassifiedInteractionRequest::~ClassifiedInteractionRequest() {}

ClassifiedInteractionRequest & ClassifiedInteractionRequest::operator =(ClassifiedInteractionRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Classification = the_other.Classification;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theClassifiedInteractionRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theClassifiedInteractionRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.ClassifiedInteractionRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::task::InteractionClassification >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "com.sun.star.task.InteractionClassification" );
        ::rtl::OUString sMemberName0( "Classification" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ENUM;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::ClassifiedInteractionRequest const *) {
    return *detail::theClassifiedInteractionRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::ClassifiedInteractionRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_CLASSIFIEDINTERACTIONREQUEST_HPP
