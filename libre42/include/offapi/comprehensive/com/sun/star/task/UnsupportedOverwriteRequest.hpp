#ifndef INCLUDED_COM_SUN_STAR_TASK_UNSUPPORTEDOVERWRITEREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_UNSUPPORTEDOVERWRITEREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/UnsupportedOverwriteRequest.hdl"

#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline UnsupportedOverwriteRequest::UnsupportedOverwriteRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , Name()
{
    ::cppu::UnoType< ::css::task::UnsupportedOverwriteRequest >::get();
}

inline UnsupportedOverwriteRequest::UnsupportedOverwriteRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& Name_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , Name(Name_)
{
    ::cppu::UnoType< ::css::task::UnsupportedOverwriteRequest >::get();
}

UnsupportedOverwriteRequest::UnsupportedOverwriteRequest(UnsupportedOverwriteRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), Name(the_other.Name) {}

UnsupportedOverwriteRequest::~UnsupportedOverwriteRequest() {}

UnsupportedOverwriteRequest & UnsupportedOverwriteRequest::operator =(UnsupportedOverwriteRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    Name = the_other.Name;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theUnsupportedOverwriteRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theUnsupportedOverwriteRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.UnsupportedOverwriteRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "Name" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::UnsupportedOverwriteRequest const *) {
    return *detail::theUnsupportedOverwriteRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::UnsupportedOverwriteRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::UnsupportedOverwriteRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_UNSUPPORTEDOVERWRITEREQUEST_HPP
