#ifndef INCLUDED_COM_SUN_STAR_TASK_ERRORCODEREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_ERRORCODEREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/ErrorCodeRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace task {

inline ErrorCodeRequest::ErrorCodeRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , ErrCode(0)
{
    ::cppu::UnoType< ::css::task::ErrorCodeRequest >::get();
}

inline ErrorCodeRequest::ErrorCodeRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& ErrCode_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ErrCode(ErrCode_)
{
    ::cppu::UnoType< ::css::task::ErrorCodeRequest >::get();
}

ErrorCodeRequest::ErrorCodeRequest(ErrorCodeRequest const & the_other): ::css::uno::Exception(the_other), ErrCode(the_other.ErrCode) {}

ErrorCodeRequest::~ErrorCodeRequest() {}

ErrorCodeRequest & ErrorCodeRequest::operator =(ErrorCodeRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ErrCode = the_other.ErrCode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theErrorCodeRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theErrorCodeRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.ErrorCodeRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "long" );
        ::rtl::OUString sMemberName0( "ErrCode" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::ErrorCodeRequest const *) {
    return *detail::theErrorCodeRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::ErrorCodeRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::ErrorCodeRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_ERRORCODEREQUEST_HPP
