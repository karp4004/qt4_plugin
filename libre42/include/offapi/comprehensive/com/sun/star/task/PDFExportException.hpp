#ifndef INCLUDED_COM_SUN_STAR_TASK_PDFEXPORTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_TASK_PDFEXPORTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/task/PDFExportException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace task {

inline PDFExportException::PDFExportException() SAL_THROW(())
    : ::css::uno::Exception()
    , ErrorCodes()
{
    ::cppu::UnoType< ::css::task::PDFExportException >::get();
}

inline PDFExportException::PDFExportException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Sequence< ::sal_Int32 >& ErrorCodes_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ErrorCodes(ErrorCodes_)
{
    ::cppu::UnoType< ::css::task::PDFExportException >::get();
}

PDFExportException::PDFExportException(PDFExportException const & the_other): ::css::uno::Exception(the_other), ErrorCodes(the_other.ErrorCodes) {}

PDFExportException::~PDFExportException() {}

PDFExportException & PDFExportException::operator =(PDFExportException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ErrorCodes = the_other.ErrorCodes;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct thePDFExportExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, thePDFExportExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.PDFExportException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::sal_Int32 > >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "[]long" );
        ::rtl::OUString sMemberName0( "ErrorCodes" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::PDFExportException const *) {
    return *detail::thePDFExportExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::PDFExportException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::PDFExportException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_PDFEXPORTEXCEPTION_HPP
