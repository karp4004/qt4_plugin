#ifndef INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMSPASSWORDREQUEST2_HPP
#define INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMSPASSWORDREQUEST2_HPP

#include "sal/config.h"

#include "com/sun/star/task/DocumentMSPasswordRequest2.hdl"

#include "com/sun/star/task/DocumentMSPasswordRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace task {

inline DocumentMSPasswordRequest2::DocumentMSPasswordRequest2() SAL_THROW(())
    : ::css::task::DocumentMSPasswordRequest()
    , IsRequestPasswordToModify(false)
{
    ::cppu::UnoType< ::css::task::DocumentMSPasswordRequest2 >::get();
}

inline DocumentMSPasswordRequest2::DocumentMSPasswordRequest2(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::task::PasswordRequestMode& Mode_, const ::rtl::OUString& Name_, const ::sal_Bool& IsRequestPasswordToModify_) SAL_THROW(())
    : ::css::task::DocumentMSPasswordRequest(Message_, Context_, Classification_, Mode_, Name_)
    , IsRequestPasswordToModify(IsRequestPasswordToModify_)
{
    ::cppu::UnoType< ::css::task::DocumentMSPasswordRequest2 >::get();
}

DocumentMSPasswordRequest2::DocumentMSPasswordRequest2(DocumentMSPasswordRequest2 const & the_other): ::css::task::DocumentMSPasswordRequest(the_other), IsRequestPasswordToModify(the_other.IsRequestPasswordToModify) {}

DocumentMSPasswordRequest2::~DocumentMSPasswordRequest2() {}

DocumentMSPasswordRequest2 & DocumentMSPasswordRequest2::operator =(DocumentMSPasswordRequest2 const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::DocumentMSPasswordRequest::operator =(the_other);
    IsRequestPasswordToModify = the_other.IsRequestPasswordToModify;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theDocumentMSPasswordRequest2Type : public rtl::StaticWithInit< ::css::uno::Type *, theDocumentMSPasswordRequest2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.DocumentMSPasswordRequest2" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::DocumentMSPasswordRequest >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "boolean" );
        ::rtl::OUString sMemberName0( "IsRequestPasswordToModify" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::DocumentMSPasswordRequest2 const *) {
    return *detail::theDocumentMSPasswordRequest2Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::DocumentMSPasswordRequest2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::DocumentMSPasswordRequest2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMSPASSWORDREQUEST2_HPP
