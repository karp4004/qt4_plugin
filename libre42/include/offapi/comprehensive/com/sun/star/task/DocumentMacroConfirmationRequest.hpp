#ifndef INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMACROCONFIRMATIONREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMACROCONFIRMATIONREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/DocumentMacroConfirmationRequest.hdl"

#include "com/sun/star/embed/XStorage.hpp"
#include "com/sun/star/security/DocumentSignatureInformation.hpp"
#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline DocumentMacroConfirmationRequest::DocumentMacroConfirmationRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , DocumentURL()
    , DocumentStorage()
    , DocumentVersion()
    , DocumentSignatureInformation()
{
    ::cppu::UnoType< ::css::task::DocumentMacroConfirmationRequest >::get();
}

inline DocumentMacroConfirmationRequest::DocumentMacroConfirmationRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& DocumentURL_, const ::css::uno::Reference< ::css::embed::XStorage >& DocumentStorage_, const ::rtl::OUString& DocumentVersion_, const ::css::uno::Sequence< ::css::security::DocumentSignatureInformation >& DocumentSignatureInformation_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , DocumentURL(DocumentURL_)
    , DocumentStorage(DocumentStorage_)
    , DocumentVersion(DocumentVersion_)
    , DocumentSignatureInformation(DocumentSignatureInformation_)
{
    ::cppu::UnoType< ::css::task::DocumentMacroConfirmationRequest >::get();
}

DocumentMacroConfirmationRequest::DocumentMacroConfirmationRequest(DocumentMacroConfirmationRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), DocumentURL(the_other.DocumentURL), DocumentStorage(the_other.DocumentStorage), DocumentVersion(the_other.DocumentVersion), DocumentSignatureInformation(the_other.DocumentSignatureInformation) {}

DocumentMacroConfirmationRequest::~DocumentMacroConfirmationRequest() {}

DocumentMacroConfirmationRequest & DocumentMacroConfirmationRequest::operator =(DocumentMacroConfirmationRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    DocumentURL = the_other.DocumentURL;
    DocumentStorage = the_other.DocumentStorage;
    DocumentVersion = the_other.DocumentVersion;
    DocumentSignatureInformation = the_other.DocumentSignatureInformation;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theDocumentMacroConfirmationRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theDocumentMacroConfirmationRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.DocumentMacroConfirmationRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::embed::XStorage > >::get();
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::security::DocumentSignatureInformation > >::get();

        typelib_CompoundMember_Init aMembers[4];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "DocumentURL" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "com.sun.star.embed.XStorage" );
        ::rtl::OUString sMemberName1( "DocumentStorage" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "string" );
        ::rtl::OUString sMemberName2( "DocumentVersion" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;
        ::rtl::OUString sMemberType3( "[]com.sun.star.security.DocumentSignatureInformation" );
        ::rtl::OUString sMemberName3( "DocumentSignatureInformation" );
        aMembers[3].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
        aMembers[3].pTypeName = sMemberType3.pData;
        aMembers[3].pMemberName = sMemberName3.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            4,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::DocumentMacroConfirmationRequest const *) {
    return *detail::theDocumentMacroConfirmationRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::DocumentMacroConfirmationRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::DocumentMacroConfirmationRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_DOCUMENTMACROCONFIRMATIONREQUEST_HPP
