#ifndef INCLUDED_COM_SUN_STAR_TASK_DOCUMENTPASSWORDREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_TASK_DOCUMENTPASSWORDREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/task/DocumentPasswordRequest.hdl"

#include "com/sun/star/task/PasswordRequest.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace task {

inline DocumentPasswordRequest::DocumentPasswordRequest() SAL_THROW(())
    : ::css::task::PasswordRequest()
    , Name()
{
    ::cppu::UnoType< ::css::task::DocumentPasswordRequest >::get();
}

inline DocumentPasswordRequest::DocumentPasswordRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::task::PasswordRequestMode& Mode_, const ::rtl::OUString& Name_) SAL_THROW(())
    : ::css::task::PasswordRequest(Message_, Context_, Classification_, Mode_)
    , Name(Name_)
{
    ::cppu::UnoType< ::css::task::DocumentPasswordRequest >::get();
}

DocumentPasswordRequest::DocumentPasswordRequest(DocumentPasswordRequest const & the_other): ::css::task::PasswordRequest(the_other), Name(the_other.Name) {}

DocumentPasswordRequest::~DocumentPasswordRequest() {}

DocumentPasswordRequest & DocumentPasswordRequest::operator =(DocumentPasswordRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::PasswordRequest::operator =(the_other);
    Name = the_other.Name;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace task { namespace detail {

struct theDocumentPasswordRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theDocumentPasswordRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.task.DocumentPasswordRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::PasswordRequest >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "Name" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace task {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::task::DocumentPasswordRequest const *) {
    return *detail::theDocumentPasswordRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::task::DocumentPasswordRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::task::DocumentPasswordRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TASK_DOCUMENTPASSWORDREQUEST_HPP
