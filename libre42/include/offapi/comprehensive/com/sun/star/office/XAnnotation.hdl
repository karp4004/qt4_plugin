#ifndef INCLUDED_COM_SUN_STAR_OFFICE_XANNOTATION_HDL
#define INCLUDED_COM_SUN_STAR_OFFICE_XANNOTATION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/XPropertySet.hdl"
#include "com/sun/star/geometry/RealPoint2D.hdl"
#include "com/sun/star/geometry/RealSize2D.hdl"
#include "com/sun/star/lang/XComponent.hdl"
namespace com { namespace sun { namespace star { namespace text { class XText; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/util/DateTime.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace office {

class SAL_NO_VTABLE XAnnotation : public ::css::beans::XPropertySet, public ::css::lang::XComponent
{
public:

    // Attributes
    virtual ::css::uno::Any SAL_CALL getAnchor() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::geometry::RealPoint2D SAL_CALL getPosition() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPosition( const ::css::geometry::RealPoint2D& _position ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::geometry::RealSize2D SAL_CALL getSize() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setSize( const ::css::geometry::RealSize2D& _size ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getAuthor() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setAuthor( const ::rtl::OUString& _author ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::util::DateTime SAL_CALL getDateTime() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setDateTime( const ::css::util::DateTime& _datetime ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::text::XText > SAL_CALL getTextRange() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAnnotation() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::office::XAnnotation const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::office::XAnnotation > *) SAL_THROW(());

#endif
