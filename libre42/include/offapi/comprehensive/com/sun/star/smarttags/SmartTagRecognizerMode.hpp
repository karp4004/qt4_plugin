#ifndef INCLUDED_COM_SUN_STAR_SMARTTAGS_SMARTTAGRECOGNIZERMODE_HPP
#define INCLUDED_COM_SUN_STAR_SMARTTAGS_SMARTTAGRECOGNIZERMODE_HPP

#include "sal/config.h"

#include "com/sun/star/smarttags/SmartTagRecognizerMode.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace smarttags { namespace detail {

struct theSmartTagRecognizerModeType : public rtl::StaticWithInit< ::css::uno::Type *, theSmartTagRecognizerModeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.smarttags.SmartTagRecognizerMode" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[4];
        ::rtl::OUString sEnumValue0( "CHAR" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "SINGLE_WORD" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "PARAGRAPH" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "CELL" );
        enumValueNames[3] = sEnumValue3.pData;

        sal_Int32 enumValues[4];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::smarttags::SmartTagRecognizerMode_CHAR,
            4, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace smarttags {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::smarttags::SmartTagRecognizerMode const *) {
    return *detail::theSmartTagRecognizerModeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::smarttags::SmartTagRecognizerMode const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::smarttags::SmartTagRecognizerMode >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SMARTTAGS_SMARTTAGRECOGNIZERMODE_HPP
