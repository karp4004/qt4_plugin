#ifndef INCLUDED_COM_SUN_STAR_UI_CONTEXTCHANGEEVENTOBJECT_HPP
#define INCLUDED_COM_SUN_STAR_UI_CONTEXTCHANGEEVENTOBJECT_HPP

#include "sal/config.h"

#include "com/sun/star/ui/ContextChangeEventObject.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ui {

inline ContextChangeEventObject::ContextChangeEventObject() SAL_THROW(())
    : ::css::lang::EventObject()
    , ApplicationName()
    , ContextName()
{
}

inline ContextChangeEventObject::ContextChangeEventObject(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& ApplicationName_, const ::rtl::OUString& ContextName_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , ApplicationName(ApplicationName_)
    , ContextName(ContextName_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ui { namespace detail {

struct theContextChangeEventObjectType : public rtl::StaticWithInit< ::css::uno::Type *, theContextChangeEventObjectType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.ui.ContextChangeEventObject" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "ApplicationName" );
        ::rtl::OUString the_name1( "ContextName" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace ui {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::ContextChangeEventObject const *) {
    return *detail::theContextChangeEventObjectType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::ContextChangeEventObject const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::ContextChangeEventObject >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_CONTEXTCHANGEEVENTOBJECT_HPP
