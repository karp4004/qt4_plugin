#ifndef INCLUDED_COM_SUN_STAR_UI_XUIELEMENTSETTINGS_HDL
#define INCLUDED_COM_SUN_STAR_UI_XUIELEMENTSETTINGS_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace container { class XIndexAccess; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ui {

class SAL_NO_VTABLE XUIElementSettings : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL updateSettings() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::container::XIndexAccess > SAL_CALL getSettings( ::sal_Bool bWriteable ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setSettings( const ::css::uno::Reference< ::css::container::XIndexAccess >& UISettings ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XUIElementSettings() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::XUIElementSettings const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ui::XUIElementSettings > *) SAL_THROW(());

#endif
