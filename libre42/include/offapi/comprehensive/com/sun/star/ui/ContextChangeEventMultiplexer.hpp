#ifndef INCLUDED_COM_SUN_STAR_UI_CONTEXTCHANGEEVENTMULTIPLEXER_HPP
#define INCLUDED_COM_SUN_STAR_UI_CONTEXTCHANGEEVENTMULTIPLEXER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/ui/XContextChangeEventMultiplexer.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace ui {

class ContextChangeEventMultiplexer {
public:
    static ::css::uno::Reference< ::css::ui::XContextChangeEventMultiplexer > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::ui::XContextChangeEventMultiplexer > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.ui.ContextChangeEventMultiplexer" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.ui.ContextChangeEventMultiplexer of type com.sun.star.ui.XContextChangeEventMultiplexer" ), the_context);
        }
        return instance;
    }

private:
    ContextChangeEventMultiplexer(); // not implemented
    ContextChangeEventMultiplexer(ContextChangeEventMultiplexer &); // not implemented
    ~ContextChangeEventMultiplexer(); // not implemented
    void operator =(ContextChangeEventMultiplexer); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_UI_CONTEXTCHANGEEVENTMULTIPLEXER_HPP
