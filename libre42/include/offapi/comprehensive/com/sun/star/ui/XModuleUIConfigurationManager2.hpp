#ifndef INCLUDED_COM_SUN_STAR_UI_XMODULEUICONFIGURATIONMANAGER2_HPP
#define INCLUDED_COM_SUN_STAR_UI_XMODULEUICONFIGURATIONMANAGER2_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/ui/XModuleUIConfigurationManager2.hdl"

#include "com/sun/star/ui/XModuleUIConfigurationManager.hpp"
#include "com/sun/star/ui/XUIConfiguration.hpp"
#include "com/sun/star/ui/XUIConfigurationManager.hpp"
#include "com/sun/star/ui/XUIConfigurationPersistence.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ui { namespace detail {

struct theXModuleUIConfigurationManager2Type : public rtl::StaticWithInit< ::css::uno::Type *, theXModuleUIConfigurationManager2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.ui.XModuleUIConfigurationManager2" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[4];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::XUIConfigurationPersistence > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::XUIConfigurationManager > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::XModuleUIConfigurationManager > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::XUIConfiguration > >::get().getTypeLibType();

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            4, aSuperTypes,
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace ui {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::XModuleUIConfigurationManager2 const *) {
    const ::css::uno::Type &rRet = *detail::theXModuleUIConfigurationManager2Type::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::ui::XModuleUIConfigurationManager2 > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::ui::XModuleUIConfigurationManager2 > >::get();
}

::css::uno::Type const & ::css::ui::XModuleUIConfigurationManager2::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::ui::XModuleUIConfigurationManager2 > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_UI_XMODULEUICONFIGURATIONMANAGER2_HPP
