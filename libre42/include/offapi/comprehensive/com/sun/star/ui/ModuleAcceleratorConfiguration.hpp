#ifndef INCLUDED_COM_SUN_STAR_UI_MODULEACCELERATORCONFIGURATION_HPP
#define INCLUDED_COM_SUN_STAR_UI_MODULEACCELERATORCONFIGURATION_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/ui/XAcceleratorConfiguration.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace ui {

class ModuleAcceleratorConfiguration {
public:
    static ::css::uno::Reference< ::css::ui::XAcceleratorConfiguration > createWithModuleIdentifier(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& ModuleIdentifier) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= ModuleIdentifier;
        ::css::uno::Reference< ::css::ui::XAcceleratorConfiguration > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::ui::XAcceleratorConfiguration >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.ui.ModuleAcceleratorConfiguration" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ui.ModuleAcceleratorConfiguration of type com.sun.star.ui.XAcceleratorConfiguration: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ui.ModuleAcceleratorConfiguration of type com.sun.star.ui.XAcceleratorConfiguration" ), the_context);
        }
        return the_instance;
    }

private:
    ModuleAcceleratorConfiguration(); // not implemented
    ModuleAcceleratorConfiguration(ModuleAcceleratorConfiguration &); // not implemented
    ~ModuleAcceleratorConfiguration(); // not implemented
    void operator =(ModuleAcceleratorConfiguration); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_UI_MODULEACCELERATORCONFIGURATION_HPP
