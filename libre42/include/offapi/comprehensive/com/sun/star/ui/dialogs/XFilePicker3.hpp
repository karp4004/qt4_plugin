#ifndef INCLUDED_COM_SUN_STAR_UI_DIALOGS_XFILEPICKER3_HPP
#define INCLUDED_COM_SUN_STAR_UI_DIALOGS_XFILEPICKER3_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/ui/dialogs/XFilePicker3.hdl"

#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/ui/dialogs/XFilePicker.hpp"
#include "com/sun/star/ui/dialogs/XFilePickerNotifier.hpp"
#include "com/sun/star/ui/dialogs/XFilterGroupManager.hpp"
#include "com/sun/star/ui/dialogs/XFilterManager.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/util/XCancellable.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs { namespace detail {

struct theXFilePicker3Type : public rtl::StaticWithInit< ::css::uno::Type *, theXFilePicker3Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.ui.dialogs.XFilePicker3" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[6];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::dialogs::XFilePicker > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::dialogs::XFilePickerNotifier > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::dialogs::XFilterManager > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::dialogs::XFilterGroupManager > >::get().getTypeLibType();
        aSuperTypes[4] = ::cppu::UnoType< ::css::uno::Reference< ::css::util::XCancellable > >::get().getTypeLibType();
        aSuperTypes[5] = ::cppu::UnoType< ::css::uno::Reference< ::css::lang::XComponent > >::get().getTypeLibType();

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            6, aSuperTypes,
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::dialogs::XFilePicker3 const *) {
    const ::css::uno::Type &rRet = *detail::theXFilePicker3Type::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::ui::dialogs::XFilePicker3 > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::ui::dialogs::XFilePicker3 > >::get();
}

::css::uno::Type const & ::css::ui::dialogs::XFilePicker3::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::ui::dialogs::XFilePicker3 > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_UI_DIALOGS_XFILEPICKER3_HPP
