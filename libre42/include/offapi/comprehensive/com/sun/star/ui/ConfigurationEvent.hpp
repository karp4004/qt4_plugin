#ifndef INCLUDED_COM_SUN_STAR_UI_CONFIGURATIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_UI_CONFIGURATIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/ui/ConfigurationEvent.hdl"

#include "com/sun/star/container/ContainerEvent.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ui {

inline ConfigurationEvent::ConfigurationEvent() SAL_THROW(())
    : ::css::container::ContainerEvent()
    , ResourceURL()
    , aInfo()
{
}

inline ConfigurationEvent::ConfigurationEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Any& Accessor_, const ::css::uno::Any& Element_, const ::css::uno::Any& ReplacedElement_, const ::rtl::OUString& ResourceURL_, const ::css::uno::Any& aInfo_) SAL_THROW(())
    : ::css::container::ContainerEvent(Source_, Accessor_, Element_, ReplacedElement_)
    , ResourceURL(ResourceURL_)
    , aInfo(aInfo_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ui { namespace detail {

struct theConfigurationEventType : public rtl::StaticWithInit< ::css::uno::Type *, theConfigurationEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.ui.ConfigurationEvent" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "ResourceURL" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "aInfo" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::container::ContainerEvent >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace ui {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::ConfigurationEvent const *) {
    return *detail::theConfigurationEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::ConfigurationEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::ConfigurationEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_CONFIGURATIONEVENT_HPP
