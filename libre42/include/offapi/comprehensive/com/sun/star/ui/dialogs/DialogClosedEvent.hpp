#ifndef INCLUDED_COM_SUN_STAR_UI_DIALOGS_DIALOGCLOSEDEVENT_HPP
#define INCLUDED_COM_SUN_STAR_UI_DIALOGS_DIALOGCLOSEDEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/ui/dialogs/DialogClosedEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

inline DialogClosedEvent::DialogClosedEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , DialogResult(0)
{
}

inline DialogClosedEvent::DialogClosedEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& DialogResult_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , DialogResult(DialogResult_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs { namespace detail {

struct theDialogClosedEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDialogClosedEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.ui.dialogs.DialogClosedEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "DialogResult" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::dialogs::DialogClosedEvent const *) {
    return *detail::theDialogClosedEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::dialogs::DialogClosedEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::dialogs::DialogClosedEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_DIALOGS_DIALOGCLOSEDEVENT_HPP
