#ifndef INCLUDED_COM_SUN_STAR_UI_CONTEXTMENUEXECUTEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_UI_CONTEXTMENUEXECUTEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/ui/ContextMenuExecuteEvent.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/awt/XWindow.hpp"
#include "com/sun/star/container/XIndexContainer.hpp"
#include "com/sun/star/view/XSelectionSupplier.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ui {

inline ContextMenuExecuteEvent::ContextMenuExecuteEvent() SAL_THROW(())
    : SourceWindow()
    , ExecutePosition()
    , ActionTriggerContainer()
    , Selection()
{
}

inline ContextMenuExecuteEvent::ContextMenuExecuteEvent(const ::css::uno::Reference< ::css::awt::XWindow >& SourceWindow_, const ::css::awt::Point& ExecutePosition_, const ::css::uno::Reference< ::css::container::XIndexContainer >& ActionTriggerContainer_, const ::css::uno::Reference< ::css::view::XSelectionSupplier >& Selection_) SAL_THROW(())
    : SourceWindow(SourceWindow_)
    , ExecutePosition(ExecutePosition_)
    , ActionTriggerContainer(ActionTriggerContainer_)
    , Selection(Selection_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ui { namespace detail {

struct theContextMenuExecuteEventType : public rtl::StaticWithInit< ::css::uno::Type *, theContextMenuExecuteEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.ui.ContextMenuExecuteEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::awt::XWindow > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.XWindow" );
        ::rtl::OUString the_name0( "SourceWindow" );
        ::cppu::UnoType< ::css::awt::Point >::get();
        ::rtl::OUString the_tname1( "com.sun.star.awt.Point" );
        ::rtl::OUString the_name1( "ExecutePosition" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::container::XIndexContainer > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.container.XIndexContainer" );
        ::rtl::OUString the_name2( "ActionTriggerContainer" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::view::XSelectionSupplier > >::get();
        ::rtl::OUString the_tname3( "com.sun.star.view.XSelectionSupplier" );
        ::rtl::OUString the_name3( "Selection" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace ui {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::ContextMenuExecuteEvent const *) {
    return *detail::theContextMenuExecuteEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::ContextMenuExecuteEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::ContextMenuExecuteEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_CONTEXTMENUEXECUTEEVENT_HPP
