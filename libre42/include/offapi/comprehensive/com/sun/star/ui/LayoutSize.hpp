#ifndef INCLUDED_COM_SUN_STAR_UI_LAYOUTSIZE_HPP
#define INCLUDED_COM_SUN_STAR_UI_LAYOUTSIZE_HPP

#include "sal/config.h"

#include "com/sun/star/ui/LayoutSize.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ui {

inline LayoutSize::LayoutSize() SAL_THROW(())
    : Minimum(0)
    , Maximum(0)
    , Preferred(0)
{
}

inline LayoutSize::LayoutSize(const ::sal_Int32& Minimum_, const ::sal_Int32& Maximum_, const ::sal_Int32& Preferred_) SAL_THROW(())
    : Minimum(Minimum_)
    , Maximum(Maximum_)
    , Preferred(Preferred_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ui { namespace detail {

struct theLayoutSizeType : public rtl::StaticWithInit< ::css::uno::Type *, theLayoutSizeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.ui.LayoutSize" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Minimum" );
        ::rtl::OUString the_name1( "Maximum" );
        ::rtl::OUString the_name2( "Preferred" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace ui {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::LayoutSize const *) {
    return *detail::theLayoutSizeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::LayoutSize const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::LayoutSize >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_LAYOUTSIZE_HPP
