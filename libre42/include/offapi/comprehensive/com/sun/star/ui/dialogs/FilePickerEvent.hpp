#ifndef INCLUDED_COM_SUN_STAR_UI_DIALOGS_FILEPICKEREVENT_HPP
#define INCLUDED_COM_SUN_STAR_UI_DIALOGS_FILEPICKEREVENT_HPP

#include "sal/config.h"

#include "com/sun/star/ui/dialogs/FilePickerEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

inline FilePickerEvent::FilePickerEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , ElementId(0)
{
}

inline FilePickerEvent::FilePickerEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& ElementId_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , ElementId(ElementId_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs { namespace detail {

struct theFilePickerEventType : public rtl::StaticWithInit< ::css::uno::Type *, theFilePickerEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.ui.dialogs.FilePickerEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "ElementId" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace ui { namespace dialogs {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ui::dialogs::FilePickerEvent const *) {
    return *detail::theFilePickerEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ui::dialogs::FilePickerEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ui::dialogs::FilePickerEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UI_DIALOGS_FILEPICKEREVENT_HPP
