#ifndef INCLUDED_COM_SUN_STAR_FORM_RUNTIME_FILTEREVENT_HPP
#define INCLUDED_COM_SUN_STAR_FORM_RUNTIME_FILTEREVENT_HPP

#include "sal/config.h"

#include "com/sun/star/form/runtime/FilterEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace form { namespace runtime {

inline FilterEvent::FilterEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , DisjunctiveTerm(0)
    , FilterComponent(0)
    , PredicateExpression()
{
}

inline FilterEvent::FilterEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& DisjunctiveTerm_, const ::sal_Int32& FilterComponent_, const ::rtl::OUString& PredicateExpression_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , DisjunctiveTerm(DisjunctiveTerm_)
    , FilterComponent(FilterComponent_)
    , PredicateExpression(PredicateExpression_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace form { namespace runtime { namespace detail {

struct theFilterEventType : public rtl::StaticWithInit< ::css::uno::Type *, theFilterEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.form.runtime.FilterEvent" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "DisjunctiveTerm" );
        ::rtl::OUString the_name1( "FilterComponent" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name2( "PredicateExpression" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace form { namespace runtime {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::runtime::FilterEvent const *) {
    return *detail::theFilterEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::form::runtime::FilterEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::form::runtime::FilterEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FORM_RUNTIME_FILTEREVENT_HPP
