#ifndef INCLUDED_COM_SUN_STAR_FORM_DATABASEPARAMETEREVENT_HPP
#define INCLUDED_COM_SUN_STAR_FORM_DATABASEPARAMETEREVENT_HPP

#include "sal/config.h"

#include "com/sun/star/form/DatabaseParameterEvent.hdl"

#include "com/sun/star/container/XIndexAccess.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace form {

inline DatabaseParameterEvent::DatabaseParameterEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Parameters()
{
}

inline DatabaseParameterEvent::DatabaseParameterEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::container::XIndexAccess >& Parameters_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Parameters(Parameters_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace form { namespace detail {

struct theDatabaseParameterEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDatabaseParameterEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.form.DatabaseParameterEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::container::XIndexAccess > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.container.XIndexAccess" );
        ::rtl::OUString the_name0( "Parameters" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace form {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::DatabaseParameterEvent const *) {
    return *detail::theDatabaseParameterEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::form::DatabaseParameterEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::form::DatabaseParameterEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FORM_DATABASEPARAMETEREVENT_HPP
