#ifndef INCLUDED_COM_SUN_STAR_FORM_BINDING_LISTENTRYEVENT_HPP
#define INCLUDED_COM_SUN_STAR_FORM_BINDING_LISTENTRYEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/form/binding/ListEntryEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace form { namespace binding {

inline ListEntryEvent::ListEntryEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Position(0)
    , Count(0)
    , Entries()
{
}

inline ListEntryEvent::ListEntryEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& Position_, const ::sal_Int32& Count_, const ::css::uno::Sequence< ::rtl::OUString >& Entries_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Position(Position_)
    , Count(Count_)
    , Entries(Entries_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace form { namespace binding { namespace detail {

struct theListEntryEventType : public rtl::StaticWithInit< ::css::uno::Type *, theListEntryEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.form.binding.ListEntryEvent" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Position" );
        ::rtl::OUString the_name1( "Count" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
        ::rtl::OUString the_tname1( "[]string" );
        ::rtl::OUString the_name2( "Entries" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace form { namespace binding {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::binding::ListEntryEvent const *) {
    return *detail::theListEntryEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::form::binding::ListEntryEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::form::binding::ListEntryEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FORM_BINDING_LISTENTRYEVENT_HPP
