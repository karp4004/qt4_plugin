#ifndef INCLUDED_COM_SUN_STAR_FORM_TABULATORCYCLE_HPP
#define INCLUDED_COM_SUN_STAR_FORM_TABULATORCYCLE_HPP

#include "sal/config.h"

#include "com/sun/star/form/TabulatorCycle.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace form { namespace detail {

struct theTabulatorCycleType : public rtl::StaticWithInit< ::css::uno::Type *, theTabulatorCycleType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.form.TabulatorCycle" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[3];
        ::rtl::OUString sEnumValue0( "RECORDS" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "CURRENT" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "PAGE" );
        enumValueNames[2] = sEnumValue2.pData;

        sal_Int32 enumValues[3];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::form::TabulatorCycle_RECORDS,
            3, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace form {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::TabulatorCycle const *) {
    return *detail::theTabulatorCycleType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::form::TabulatorCycle const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::form::TabulatorCycle >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FORM_TABULATORCYCLE_HPP
