#ifndef INCLUDED_COM_SUN_STAR_FORM_XFORMCONTROLLERLISTENER_HDL
#define INCLUDED_COM_SUN_STAR_FORM_XFORMCONTROLLERLISTENER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/EventObject.hdl"
#include "com/sun/star/lang/XEventListener.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace form {

class SAL_NO_VTABLE XFormControllerListener : public ::css::lang::XEventListener
{
public:

    // Methods
    virtual void SAL_CALL formActivated( const ::css::lang::EventObject& rEvent ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL formDeactivated( const ::css::lang::EventObject& rEvent ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFormControllerListener() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::XFormControllerListener const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::form::XFormControllerListener > *) SAL_THROW(());

#endif
