#ifndef INCLUDED_COM_SUN_STAR_FORM_RUNTIME_FEATURESTATE_HPP
#define INCLUDED_COM_SUN_STAR_FORM_RUNTIME_FEATURESTATE_HPP

#include "sal/config.h"

#include "com/sun/star/form/runtime/FeatureState.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace form { namespace runtime {

inline FeatureState::FeatureState() SAL_THROW(())
    : Enabled(false)
    , State()
{
}

inline FeatureState::FeatureState(const ::sal_Bool& Enabled_, const ::css::uno::Any& State_) SAL_THROW(())
    : Enabled(Enabled_)
    , State(State_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace form { namespace runtime { namespace detail {

struct theFeatureStateType : public rtl::StaticWithInit< ::css::uno::Type *, theFeatureStateType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.form.runtime.FeatureState" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "Enabled" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "State" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace form { namespace runtime {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::runtime::FeatureState const *) {
    return *detail::theFeatureStateType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::form::runtime::FeatureState const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::form::runtime::FeatureState >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FORM_RUNTIME_FEATURESTATE_HPP
