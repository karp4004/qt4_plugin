#ifndef INCLUDED_COM_SUN_STAR_FORM_DATABASEDELETEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_FORM_DATABASEDELETEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/form/DatabaseDeleteEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace form {

inline DatabaseDeleteEvent::DatabaseDeleteEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Bookmarks()
{
}

inline DatabaseDeleteEvent::DatabaseDeleteEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Sequence< ::css::uno::Any >& Bookmarks_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Bookmarks(Bookmarks_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace form { namespace detail {

struct theDatabaseDeleteEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDatabaseDeleteEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.form.DatabaseDeleteEvent" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::uno::Any > >::get();
        ::rtl::OUString the_tname0( "[]any" );
        ::rtl::OUString the_name0( "Bookmarks" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace form {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::form::DatabaseDeleteEvent const *) {
    return *detail::theDatabaseDeleteEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::form::DatabaseDeleteEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::form::DatabaseDeleteEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FORM_DATABASEDELETEEVENT_HPP
