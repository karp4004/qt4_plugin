#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLERESULTDATA_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLERESULTDATA_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotTableResultData.hdl"

#include "com/sun/star/sheet/DataPilotFieldFilter.hpp"
#include "com/sun/star/sheet/DataResult.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotTableResultData::DataPilotTableResultData() SAL_THROW(())
    : FieldFilters()
    , DataFieldIndex(0)
    , Result()
{
}

inline DataPilotTableResultData::DataPilotTableResultData(const ::css::uno::Sequence< ::css::sheet::DataPilotFieldFilter >& FieldFilters_, const ::sal_Int32& DataFieldIndex_, const ::css::sheet::DataResult& Result_) SAL_THROW(())
    : FieldFilters(FieldFilters_)
    , DataFieldIndex(DataFieldIndex_)
    , Result(Result_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDataPilotTableResultDataType : public rtl::StaticWithInit< ::css::uno::Type *, theDataPilotTableResultDataType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DataPilotTableResultData" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::sheet::DataPilotFieldFilter > >::get();
        ::rtl::OUString the_tname0( "[]com.sun.star.sheet.DataPilotFieldFilter" );
        ::rtl::OUString the_name0( "FieldFilters" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "DataFieldIndex" );
        ::cppu::UnoType< ::css::sheet::DataResult >::get();
        ::rtl::OUString the_tname2( "com.sun.star.sheet.DataResult" );
        ::rtl::OUString the_name2( "Result" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotTableResultData const *) {
    return *detail::theDataPilotTableResultDataType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotTableResultData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotTableResultData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLERESULTDATA_HPP
