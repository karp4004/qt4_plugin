#ifndef INCLUDED_COM_SUN_STAR_SHEET_XSHEETOPERATION_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XSHEETOPERATION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sheet/GeneralFunction.hdl"
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XSheetOperation : public ::css::uno::XInterface
{
public:

    // Methods
    virtual double SAL_CALL computeFunction( ::css::sheet::GeneralFunction nFunction ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL clearContents( ::sal_Int32 nContentFlags ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSheetOperation() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XSheetOperation const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XSheetOperation > *) SAL_THROW(());

#endif
