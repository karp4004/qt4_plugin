#ifndef INCLUDED_COM_SUN_STAR_SHEET_DDELINKINFO_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DDELINKINFO_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DDELinkInfo.hdl"

#include "com/sun/star/sheet/DDEItemInfo.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DDELinkInfo::DDELinkInfo() SAL_THROW(())
    : Service()
    , Topic()
    , Items()
{
}

inline DDELinkInfo::DDELinkInfo(const ::rtl::OUString& Service_, const ::rtl::OUString& Topic_, const ::css::uno::Sequence< ::css::sheet::DDEItemInfo >& Items_) SAL_THROW(())
    : Service(Service_)
    , Topic(Topic_)
    , Items(Items_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDDELinkInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theDDELinkInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DDELinkInfo" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Service" );
        ::rtl::OUString the_name1( "Topic" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::sheet::DDEItemInfo > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.sheet.DDEItemInfo" );
        ::rtl::OUString the_name2( "Items" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DDELinkInfo const *) {
    return *detail::theDDELinkInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DDELinkInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DDELinkInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DDELINKINFO_HPP
