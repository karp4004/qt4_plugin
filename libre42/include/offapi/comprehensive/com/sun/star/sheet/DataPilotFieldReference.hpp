#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDREFERENCE_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDREFERENCE_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotFieldReference.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotFieldReference::DataPilotFieldReference() SAL_THROW(())
    : ReferenceType(0)
    , ReferenceField()
    , ReferenceItemType(0)
    , ReferenceItemName()
{
}

inline DataPilotFieldReference::DataPilotFieldReference(const ::sal_Int32& ReferenceType_, const ::rtl::OUString& ReferenceField_, const ::sal_Int32& ReferenceItemType_, const ::rtl::OUString& ReferenceItemName_) SAL_THROW(())
    : ReferenceType(ReferenceType_)
    , ReferenceField(ReferenceField_)
    , ReferenceItemType(ReferenceItemType_)
    , ReferenceItemName(ReferenceItemName_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDataPilotFieldReferenceType : public rtl::StaticWithInit< ::css::uno::Type *, theDataPilotFieldReferenceType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DataPilotFieldReference" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "ReferenceType" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "ReferenceField" );
        ::rtl::OUString the_name2( "ReferenceItemType" );
        ::rtl::OUString the_name3( "ReferenceItemName" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldReference const *) {
    return *detail::theDataPilotFieldReferenceType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldReference const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotFieldReference >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDREFERENCE_HPP
