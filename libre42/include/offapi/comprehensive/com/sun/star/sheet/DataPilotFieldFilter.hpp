#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDFILTER_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDFILTER_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotFieldFilter.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotFieldFilter::DataPilotFieldFilter() SAL_THROW(())
    : FieldName()
    , MatchValue()
{
}

inline DataPilotFieldFilter::DataPilotFieldFilter(const ::rtl::OUString& FieldName_, const ::rtl::OUString& MatchValue_) SAL_THROW(())
    : FieldName(FieldName_)
    , MatchValue(MatchValue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDataPilotFieldFilterType : public rtl::StaticWithInit< ::css::uno::Type *, theDataPilotFieldFilterType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DataPilotFieldFilter" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "FieldName" );
        ::rtl::OUString the_name1( "MatchValue" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldFilter const *) {
    return *detail::theDataPilotFieldFilterType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldFilter const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotFieldFilter >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDFILTER_HPP
