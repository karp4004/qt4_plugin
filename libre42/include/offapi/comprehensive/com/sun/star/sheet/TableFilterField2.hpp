#ifndef INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD2_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD2_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/TableFilterField2.hdl"

#include "com/sun/star/sheet/FilterConnection.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline TableFilterField2::TableFilterField2() SAL_THROW(())
    : Connection(::css::sheet::FilterConnection_AND)
    , Field(0)
    , Operator(0)
    , IsNumeric(false)
    , NumericValue(0)
    , StringValue()
{
}

inline TableFilterField2::TableFilterField2(const ::css::sheet::FilterConnection& Connection_, const ::sal_Int32& Field_, const ::sal_Int32& Operator_, const ::sal_Bool& IsNumeric_, const double& NumericValue_, const ::rtl::OUString& StringValue_) SAL_THROW(())
    : Connection(Connection_)
    , Field(Field_)
    , Operator(Operator_)
    , IsNumeric(IsNumeric_)
    , NumericValue(NumericValue_)
    , StringValue(StringValue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theTableFilterField2Type : public rtl::StaticWithInit< ::css::uno::Type *, theTableFilterField2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.TableFilterField2" );
        ::cppu::UnoType< ::css::sheet::FilterConnection >::get();
        ::rtl::OUString the_tname0( "com.sun.star.sheet.FilterConnection" );
        ::rtl::OUString the_name0( "Connection" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "Field" );
        ::rtl::OUString the_name2( "Operator" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name3( "IsNumeric" );
        ::rtl::OUString the_tname3( "double" );
        ::rtl::OUString the_name4( "NumericValue" );
        ::rtl::OUString the_tname4( "string" );
        ::rtl::OUString the_name5( "StringValue" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname3.pData, the_name4.pData }, false },
            { { typelib_TypeClass_STRING, the_tname4.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::TableFilterField2 const *) {
    return *detail::theTableFilterField2Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::TableFilterField2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::TableFilterField2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD2_HPP
