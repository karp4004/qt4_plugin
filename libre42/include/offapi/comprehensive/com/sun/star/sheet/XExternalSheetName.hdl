#ifndef INCLUDED_COM_SUN_STAR_SHEET_XEXTERNALSHEETNAME_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XEXTERNALSHEETNAME_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/ElementExistException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XExternalSheetName : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setExternalName( const ::rtl::OUString& aUrl, const ::rtl::OUString& aSheetName ) /* throw (::css::container::ElementExistException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XExternalSheetName() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XExternalSheetName const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XExternalSheetName > *) SAL_THROW(());

#endif
