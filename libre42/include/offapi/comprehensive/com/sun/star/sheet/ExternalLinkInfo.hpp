#ifndef INCLUDED_COM_SUN_STAR_SHEET_EXTERNALLINKINFO_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_EXTERNALLINKINFO_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/ExternalLinkInfo.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline ExternalLinkInfo::ExternalLinkInfo() SAL_THROW(())
    : Type(0)
    , Data()
{
}

inline ExternalLinkInfo::ExternalLinkInfo(const ::sal_Int32& Type_, const ::css::uno::Any& Data_) SAL_THROW(())
    : Type(Type_)
    , Data(Data_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theExternalLinkInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theExternalLinkInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.ExternalLinkInfo" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Type" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "Data" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::ExternalLinkInfo const *) {
    return *detail::theExternalLinkInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::ExternalLinkInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::ExternalLinkInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_EXTERNALLINKINFO_HPP
