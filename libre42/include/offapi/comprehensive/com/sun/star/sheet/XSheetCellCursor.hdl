#ifndef INCLUDED_COM_SUN_STAR_SHEET_XSHEETCELLCURSOR_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XSHEETCELLCURSOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sheet/XSheetCellRange.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XSheetCellCursor : public ::css::sheet::XSheetCellRange
{
public:

    // Methods
    virtual void SAL_CALL collapseToCurrentRegion() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL collapseToCurrentArray() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL collapseToMergedArea() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL expandToEntireColumns() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL expandToEntireRows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL collapseToSize( ::sal_Int32 nColumns, ::sal_Int32 nRows ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSheetCellCursor() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XSheetCellCursor const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XSheetCellCursor > *) SAL_THROW(());

#endif
