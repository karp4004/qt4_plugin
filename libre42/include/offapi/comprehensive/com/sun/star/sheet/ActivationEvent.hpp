#ifndef INCLUDED_COM_SUN_STAR_SHEET_ACTIVATIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_ACTIVATIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/ActivationEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/sheet/XSpreadsheet.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline ActivationEvent::ActivationEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , ActiveSheet()
{
}

inline ActivationEvent::ActivationEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::sheet::XSpreadsheet >& ActiveSheet_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , ActiveSheet(ActiveSheet_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theActivationEventType : public rtl::StaticWithInit< ::css::uno::Type *, theActivationEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.ActivationEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::sheet::XSpreadsheet > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.sheet.XSpreadsheet" );
        ::rtl::OUString the_name0( "ActiveSheet" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::ActivationEvent const *) {
    return *detail::theActivationEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::ActivationEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::ActivationEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_ACTIVATIONEVENT_HPP
