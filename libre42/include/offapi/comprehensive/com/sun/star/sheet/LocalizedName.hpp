#ifndef INCLUDED_COM_SUN_STAR_SHEET_LOCALIZEDNAME_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_LOCALIZEDNAME_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/LocalizedName.hdl"

#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline LocalizedName::LocalizedName() SAL_THROW(())
    : Locale()
    , Name()
{
}

inline LocalizedName::LocalizedName(const ::css::lang::Locale& Locale_, const ::rtl::OUString& Name_) SAL_THROW(())
    : Locale(Locale_)
    , Name(Name_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theLocalizedNameType : public rtl::StaticWithInit< ::css::uno::Type *, theLocalizedNameType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.LocalizedName" );
        ::cppu::UnoType< ::css::lang::Locale >::get();
        ::rtl::OUString the_tname0( "com.sun.star.lang.Locale" );
        ::rtl::OUString the_name0( "Locale" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "Name" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::LocalizedName const *) {
    return *detail::theLocalizedNameType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::LocalizedName const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::LocalizedName >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_LOCALIZEDNAME_HPP
