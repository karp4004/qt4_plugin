#ifndef INCLUDED_COM_SUN_STAR_SHEET_XEXTERNALSHEETCACHE_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XEXTERNALSHEETCACHE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XExternalSheetCache : public ::css::uno::XInterface
{
public:

    // Attributes
    virtual ::sal_Int32 SAL_CALL getTokenIndex() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual void SAL_CALL setCellValue( ::sal_Int32 nColumn, ::sal_Int32 nRow, const ::css::uno::Any& aValue ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getCellValue( ::sal_Int32 nColumn, ::sal_Int32 nRow ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int32 > SAL_CALL getAllRows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int32 > SAL_CALL getAllColumns( ::sal_Int32 nRow ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XExternalSheetCache() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XExternalSheetCache const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XExternalSheetCache > *) SAL_THROW(());

#endif
