#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLEHEADERDATA_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLEHEADERDATA_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotTableHeaderData.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotTableHeaderData::DataPilotTableHeaderData() SAL_THROW(())
    : Dimension(0)
    , Hierarchy(0)
    , Level(0)
    , Flags(0)
    , MemberName()
{
}

inline DataPilotTableHeaderData::DataPilotTableHeaderData(const ::sal_Int32& Dimension_, const ::sal_Int32& Hierarchy_, const ::sal_Int32& Level_, const ::sal_Int32& Flags_, const ::rtl::OUString& MemberName_) SAL_THROW(())
    : Dimension(Dimension_)
    , Hierarchy(Hierarchy_)
    , Level(Level_)
    , Flags(Flags_)
    , MemberName(MemberName_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDataPilotTableHeaderDataType : public rtl::StaticWithInit< ::css::uno::Type *, theDataPilotTableHeaderDataType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DataPilotTableHeaderData" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Dimension" );
        ::rtl::OUString the_name1( "Hierarchy" );
        ::rtl::OUString the_name2( "Level" );
        ::rtl::OUString the_name3( "Flags" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name4( "MemberName" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotTableHeaderData const *) {
    return *detail::theDataPilotTableHeaderDataType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotTableHeaderData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotTableHeaderData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLEHEADERDATA_HPP
