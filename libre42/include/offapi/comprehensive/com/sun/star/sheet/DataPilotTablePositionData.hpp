#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLEPOSITIONDATA_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLEPOSITIONDATA_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotTablePositionData.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotTablePositionData::DataPilotTablePositionData() SAL_THROW(())
    : PositionType(0)
    , PositionData()
{
}

inline DataPilotTablePositionData::DataPilotTablePositionData(const ::sal_Int32& PositionType_, const ::css::uno::Any& PositionData_) SAL_THROW(())
    : PositionType(PositionType_)
    , PositionData(PositionData_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDataPilotTablePositionDataType : public rtl::StaticWithInit< ::css::uno::Type *, theDataPilotTablePositionDataType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DataPilotTablePositionData" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "PositionType" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "PositionData" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotTablePositionData const *) {
    return *detail::theDataPilotTablePositionDataType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotTablePositionData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotTablePositionData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTTABLEPOSITIONDATA_HPP
