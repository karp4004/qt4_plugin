#ifndef INCLUDED_COM_SUN_STAR_SHEET_XRANGESELECTION_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XRANGESELECTION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
namespace com { namespace sun { namespace star { namespace sheet { class XRangeSelectionChangeListener; } } } }
namespace com { namespace sun { namespace star { namespace sheet { class XRangeSelectionListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XRangeSelection : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL startRangeSelection( const ::css::uno::Sequence< ::css::beans::PropertyValue >& aArguments ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL abortRangeSelection() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addRangeSelectionListener( const ::css::uno::Reference< ::css::sheet::XRangeSelectionListener >& aListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeRangeSelectionListener( const ::css::uno::Reference< ::css::sheet::XRangeSelectionListener >& aListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addRangeSelectionChangeListener( const ::css::uno::Reference< ::css::sheet::XRangeSelectionChangeListener >& aListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeRangeSelectionChangeListener( const ::css::uno::Reference< ::css::sheet::XRangeSelectionChangeListener >& aListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XRangeSelection() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XRangeSelection const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XRangeSelection > *) SAL_THROW(());

#endif
