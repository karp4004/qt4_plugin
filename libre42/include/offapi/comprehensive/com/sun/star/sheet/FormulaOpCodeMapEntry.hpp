#ifndef INCLUDED_COM_SUN_STAR_SHEET_FORMULAOPCODEMAPENTRY_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_FORMULAOPCODEMAPENTRY_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/FormulaOpCodeMapEntry.hdl"

#include "com/sun/star/sheet/FormulaToken.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline FormulaOpCodeMapEntry::FormulaOpCodeMapEntry() SAL_THROW(())
    : Name()
    , Token()
{
}

inline FormulaOpCodeMapEntry::FormulaOpCodeMapEntry(const ::rtl::OUString& Name_, const ::css::sheet::FormulaToken& Token_) SAL_THROW(())
    : Name(Name_)
    , Token(Token_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theFormulaOpCodeMapEntryType : public rtl::StaticWithInit< ::css::uno::Type *, theFormulaOpCodeMapEntryType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.FormulaOpCodeMapEntry" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::cppu::UnoType< ::css::sheet::FormulaToken >::get();
        ::rtl::OUString the_tname1( "com.sun.star.sheet.FormulaToken" );
        ::rtl::OUString the_name1( "Token" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::FormulaOpCodeMapEntry const *) {
    return *detail::theFormulaOpCodeMapEntryType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::FormulaOpCodeMapEntry const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::FormulaOpCodeMapEntry >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_FORMULAOPCODEMAPENTRY_HPP
