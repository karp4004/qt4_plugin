#ifndef INCLUDED_COM_SUN_STAR_SHEET_FILTERFIELDVALUE_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_FILTERFIELDVALUE_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/FilterFieldValue.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline FilterFieldValue::FilterFieldValue() SAL_THROW(())
    : IsNumeric(false)
    , NumericValue(0)
    , StringValue()
{
}

inline FilterFieldValue::FilterFieldValue(const ::sal_Bool& IsNumeric_, const double& NumericValue_, const ::rtl::OUString& StringValue_) SAL_THROW(())
    : IsNumeric(IsNumeric_)
    , NumericValue(NumericValue_)
    , StringValue(StringValue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theFilterFieldValueType : public rtl::StaticWithInit< ::css::uno::Type *, theFilterFieldValueType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.FilterFieldValue" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "IsNumeric" );
        ::rtl::OUString the_tname1( "double" );
        ::rtl::OUString the_name1( "NumericValue" );
        ::rtl::OUString the_tname2( "string" );
        ::rtl::OUString the_name2( "StringValue" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::FilterFieldValue const *) {
    return *detail::theFilterFieldValueType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::FilterFieldValue const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::FilterFieldValue >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_FILTERFIELDVALUE_HPP
