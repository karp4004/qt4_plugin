#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDLAYOUTINFO_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDLAYOUTINFO_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotFieldLayoutInfo.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotFieldLayoutInfo::DataPilotFieldLayoutInfo() SAL_THROW(())
    : LayoutMode(0)
    , AddEmptyLines(false)
{
}

inline DataPilotFieldLayoutInfo::DataPilotFieldLayoutInfo(const ::sal_Int32& LayoutMode_, const ::sal_Bool& AddEmptyLines_) SAL_THROW(())
    : LayoutMode(LayoutMode_)
    , AddEmptyLines(AddEmptyLines_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDataPilotFieldLayoutInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theDataPilotFieldLayoutInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DataPilotFieldLayoutInfo" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "LayoutMode" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "AddEmptyLines" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldLayoutInfo const *) {
    return *detail::theDataPilotFieldLayoutInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldLayoutInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotFieldLayoutInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDLAYOUTINFO_HPP
