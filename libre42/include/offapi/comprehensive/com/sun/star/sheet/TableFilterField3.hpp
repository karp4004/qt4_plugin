#ifndef INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD3_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD3_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/TableFilterField3.hdl"

#include "com/sun/star/sheet/FilterConnection.hpp"
#include "com/sun/star/sheet/FilterFieldValue.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline TableFilterField3::TableFilterField3() SAL_THROW(())
    : Connection(::css::sheet::FilterConnection_AND)
    , Field(0)
    , Operator(0)
    , Values()
{
}

inline TableFilterField3::TableFilterField3(const ::css::sheet::FilterConnection& Connection_, const ::sal_Int32& Field_, const ::sal_Int32& Operator_, const ::css::uno::Sequence< ::css::sheet::FilterFieldValue >& Values_) SAL_THROW(())
    : Connection(Connection_)
    , Field(Field_)
    , Operator(Operator_)
    , Values(Values_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theTableFilterField3Type : public rtl::StaticWithInit< ::css::uno::Type *, theTableFilterField3Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.TableFilterField3" );
        ::cppu::UnoType< ::css::sheet::FilterConnection >::get();
        ::rtl::OUString the_tname0( "com.sun.star.sheet.FilterConnection" );
        ::rtl::OUString the_name0( "Connection" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "Field" );
        ::rtl::OUString the_name2( "Operator" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::sheet::FilterFieldValue > >::get();
        ::rtl::OUString the_tname2( "[]com.sun.star.sheet.FilterFieldValue" );
        ::rtl::OUString the_name3( "Values" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::TableFilterField3 const *) {
    return *detail::theTableFilterField3Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::TableFilterField3 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::TableFilterField3 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_TABLEFILTERFIELD3_HPP
