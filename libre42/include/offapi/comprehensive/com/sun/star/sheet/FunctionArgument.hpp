#ifndef INCLUDED_COM_SUN_STAR_SHEET_FUNCTIONARGUMENT_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_FUNCTIONARGUMENT_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/FunctionArgument.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline FunctionArgument::FunctionArgument() SAL_THROW(())
    : Name()
    , Description()
    , IsOptional(false)
{
}

inline FunctionArgument::FunctionArgument(const ::rtl::OUString& Name_, const ::rtl::OUString& Description_, const ::sal_Bool& IsOptional_) SAL_THROW(())
    : Name(Name_)
    , Description(Description_)
    , IsOptional(IsOptional_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theFunctionArgumentType : public rtl::StaticWithInit< ::css::uno::Type *, theFunctionArgumentType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.FunctionArgument" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::rtl::OUString the_name1( "Description" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name2( "IsOptional" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::FunctionArgument const *) {
    return *detail::theFunctionArgumentType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::FunctionArgument const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::FunctionArgument >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_FUNCTIONARGUMENT_HPP
