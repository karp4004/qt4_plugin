#ifndef INCLUDED_COM_SUN_STAR_SHEET_XSCENARIOENHANCED_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XSCENARIOENHANCED_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/table/CellRangeAddress.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XScenarioEnhanced : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::table::CellRangeAddress > SAL_CALL getRanges() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XScenarioEnhanced() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XScenarioEnhanced const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XScenarioEnhanced > *) SAL_THROW(());

#endif
