#ifndef INCLUDED_COM_SUN_STAR_SHEET_XGLOBALSHEETSETTINGS_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_XGLOBALSHEETSETTINGS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sheet/XGlobalSheetSettings.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theXGlobalSheetSettingsType : public rtl::StaticWithInit< ::css::uno::Type *, theXGlobalSheetSettingsType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.sheet.XGlobalSheetSettings" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[18] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.sheet.XGlobalSheetSettings::MoveSelection" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.sheet.XGlobalSheetSettings::MoveDirection" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.sheet.XGlobalSheetSettings::EnterEdit" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.sheet.XGlobalSheetSettings::ExtendFormat" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.sheet.XGlobalSheetSettings::RangeFinder" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.sheet.XGlobalSheetSettings::ExpandReferences" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.sheet.XGlobalSheetSettings::MarkHeader" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.sheet.XGlobalSheetSettings::UseTabCol" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.sheet.XGlobalSheetSettings::Metric" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.sheet.XGlobalSheetSettings::Scale" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.sheet.XGlobalSheetSettings::DoAutoComplete" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.sheet.XGlobalSheetSettings::StatusBarFunction" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.sheet.XGlobalSheetSettings::UserLists" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );
        ::rtl::OUString sAttributeName13( "com.sun.star.sheet.XGlobalSheetSettings::LinkUpdateMode" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName13.pData );
        ::rtl::OUString sAttributeName14( "com.sun.star.sheet.XGlobalSheetSettings::PrintAllSheets" );
        typelib_typedescriptionreference_new( &pMembers[14],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName14.pData );
        ::rtl::OUString sAttributeName15( "com.sun.star.sheet.XGlobalSheetSettings::PrintEmptyPages" );
        typelib_typedescriptionreference_new( &pMembers[15],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName15.pData );
        ::rtl::OUString sAttributeName16( "com.sun.star.sheet.XGlobalSheetSettings::UsePrinterMetrics" );
        typelib_typedescriptionreference_new( &pMembers[16],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName16.pData );
        ::rtl::OUString sAttributeName17( "com.sun.star.sheet.XGlobalSheetSettings::ReplaceCellsWarning" );
        typelib_typedescriptionreference_new( &pMembers[17],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName17.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            18,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescriptionreference_release( pMembers[14] );
        typelib_typedescriptionreference_release( pMembers[15] );
        typelib_typedescriptionreference_release( pMembers[16] );
        typelib_typedescriptionreference_release( pMembers[17] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XGlobalSheetSettings const *) {
    const ::css::uno::Type &rRet = *detail::theXGlobalSheetSettingsType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "boolean" );
                ::rtl::OUString sAttributeName0( "com.sun.star.sheet.XGlobalSheetSettings::MoveSelection" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    3, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "short" );
                ::rtl::OUString sAttributeName1( "com.sun.star.sheet.XGlobalSheetSettings::MoveDirection" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    4, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "boolean" );
                ::rtl::OUString sAttributeName2( "com.sun.star.sheet.XGlobalSheetSettings::EnterEdit" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    5, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "boolean" );
                ::rtl::OUString sAttributeName3( "com.sun.star.sheet.XGlobalSheetSettings::ExtendFormat" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    6, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "boolean" );
                ::rtl::OUString sAttributeName4( "com.sun.star.sheet.XGlobalSheetSettings::RangeFinder" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "boolean" );
                ::rtl::OUString sAttributeName5( "com.sun.star.sheet.XGlobalSheetSettings::ExpandReferences" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    8, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType5.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "boolean" );
                ::rtl::OUString sAttributeName6( "com.sun.star.sheet.XGlobalSheetSettings::MarkHeader" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    9, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType6.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "boolean" );
                ::rtl::OUString sAttributeName7( "com.sun.star.sheet.XGlobalSheetSettings::UseTabCol" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    10, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType7.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "short" );
                ::rtl::OUString sAttributeName8( "com.sun.star.sheet.XGlobalSheetSettings::Metric" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    11, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType8.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "short" );
                ::rtl::OUString sAttributeName9( "com.sun.star.sheet.XGlobalSheetSettings::Scale" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    12, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType9.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "boolean" );
                ::rtl::OUString sAttributeName10( "com.sun.star.sheet.XGlobalSheetSettings::DoAutoComplete" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    13, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType10.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "short" );
                ::rtl::OUString sAttributeName11( "com.sun.star.sheet.XGlobalSheetSettings::StatusBarFunction" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    14, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType11.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "[]string" );
                ::rtl::OUString sAttributeName12( "com.sun.star.sheet.XGlobalSheetSettings::UserLists" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    15, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType12.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType13( "short" );
                ::rtl::OUString sAttributeName13( "com.sun.star.sheet.XGlobalSheetSettings::LinkUpdateMode" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    16, sAttributeName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType13.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType14( "boolean" );
                ::rtl::OUString sAttributeName14( "com.sun.star.sheet.XGlobalSheetSettings::PrintAllSheets" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    17, sAttributeName14.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType14.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType15( "boolean" );
                ::rtl::OUString sAttributeName15( "com.sun.star.sheet.XGlobalSheetSettings::PrintEmptyPages" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    18, sAttributeName15.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType15.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType16( "boolean" );
                ::rtl::OUString sAttributeName16( "com.sun.star.sheet.XGlobalSheetSettings::UsePrinterMetrics" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    19, sAttributeName16.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType16.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType17( "boolean" );
                ::rtl::OUString sAttributeName17( "com.sun.star.sheet.XGlobalSheetSettings::ReplaceCellsWarning" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    20, sAttributeName17.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType17.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::sheet::XGlobalSheetSettings > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::sheet::XGlobalSheetSettings > >::get();
}

::css::uno::Type const & ::css::sheet::XGlobalSheetSettings::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::sheet::XGlobalSheetSettings > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_XGLOBALSHEETSETTINGS_HPP
