#ifndef INCLUDED_COM_SUN_STAR_SHEET_COMPLEXREFERENCE_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_COMPLEXREFERENCE_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/ComplexReference.hdl"

#include "com/sun/star/sheet/SingleReference.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline ComplexReference::ComplexReference() SAL_THROW(())
    : Reference1()
    , Reference2()
{
}

inline ComplexReference::ComplexReference(const ::css::sheet::SingleReference& Reference1_, const ::css::sheet::SingleReference& Reference2_) SAL_THROW(())
    : Reference1(Reference1_)
    , Reference2(Reference2_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theComplexReferenceType : public rtl::StaticWithInit< ::css::uno::Type *, theComplexReferenceType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.ComplexReference" );
        ::cppu::UnoType< ::css::sheet::SingleReference >::get();
        ::rtl::OUString the_tname0( "com.sun.star.sheet.SingleReference" );
        ::rtl::OUString the_name0( "Reference1" );
        ::rtl::OUString the_name1( "Reference2" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::ComplexReference const *) {
    return *detail::theComplexReferenceType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::ComplexReference const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::ComplexReference >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_COMPLEXREFERENCE_HPP
