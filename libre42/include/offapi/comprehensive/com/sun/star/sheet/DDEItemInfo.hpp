#ifndef INCLUDED_COM_SUN_STAR_SHEET_DDEITEMINFO_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DDEITEMINFO_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DDEItemInfo.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DDEItemInfo::DDEItemInfo() SAL_THROW(())
    : Item()
    , Results()
{
}

inline DDEItemInfo::DDEItemInfo(const ::rtl::OUString& Item_, const ::css::uno::Sequence< ::css::uno::Sequence< ::css::uno::Any > >& Results_) SAL_THROW(())
    : Item(Item_)
    , Results(Results_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDDEItemInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theDDEItemInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DDEItemInfo" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Item" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::cppu::UnoSequenceType< ::css::uno::Any > > >::get();
        ::rtl::OUString the_tname1( "[][]any" );
        ::rtl::OUString the_name1( "Results" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DDEItemInfo const *) {
    return *detail::theDDEItemInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DDEItemInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DDEItemInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DDEITEMINFO_HPP
