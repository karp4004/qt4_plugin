#ifndef INCLUDED_COM_SUN_STAR_SHEET_SOLVERCONSTRAINT_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_SOLVERCONSTRAINT_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/SolverConstraint.hdl"

#include "com/sun/star/sheet/SolverConstraintOperator.hpp"
#include "com/sun/star/table/CellAddress.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline SolverConstraint::SolverConstraint() SAL_THROW(())
    : Left()
    , Operator(::css::sheet::SolverConstraintOperator_LESS_EQUAL)
    , Right()
{
}

inline SolverConstraint::SolverConstraint(const ::css::table::CellAddress& Left_, const ::css::sheet::SolverConstraintOperator& Operator_, const ::css::uno::Any& Right_) SAL_THROW(())
    : Left(Left_)
    , Operator(Operator_)
    , Right(Right_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theSolverConstraintType : public rtl::StaticWithInit< ::css::uno::Type *, theSolverConstraintType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.SolverConstraint" );
        ::cppu::UnoType< ::css::table::CellAddress >::get();
        ::rtl::OUString the_tname0( "com.sun.star.table.CellAddress" );
        ::rtl::OUString the_name0( "Left" );
        ::cppu::UnoType< ::css::sheet::SolverConstraintOperator >::get();
        ::rtl::OUString the_tname1( "com.sun.star.sheet.SolverConstraintOperator" );
        ::rtl::OUString the_name1( "Operator" );
        ::rtl::OUString the_tname2( "any" );
        ::rtl::OUString the_name2( "Right" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::SolverConstraint const *) {
    return *detail::theSolverConstraintType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::SolverConstraint const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::SolverConstraint >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_SOLVERCONSTRAINT_HPP
