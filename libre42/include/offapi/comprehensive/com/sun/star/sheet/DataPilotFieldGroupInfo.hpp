#ifndef INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDGROUPINFO_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDGROUPINFO_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/DataPilotFieldGroupInfo.hdl"

#include "com/sun/star/container/XNameAccess.hpp"
#include "com/sun/star/sheet/XDataPilotField.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline DataPilotFieldGroupInfo::DataPilotFieldGroupInfo() SAL_THROW(())
    : HasAutoStart(false)
    , HasAutoEnd(false)
    , HasDateValues(false)
    , Start(0)
    , End(0)
    , Step(0)
    , GroupBy(0)
    , SourceField()
    , Groups()
{
}

inline DataPilotFieldGroupInfo::DataPilotFieldGroupInfo(const ::sal_Bool& HasAutoStart_, const ::sal_Bool& HasAutoEnd_, const ::sal_Bool& HasDateValues_, const double& Start_, const double& End_, const double& Step_, const ::sal_Int32& GroupBy_, const ::css::uno::Reference< ::css::sheet::XDataPilotField >& SourceField_, const ::css::uno::Reference< ::css::container::XNameAccess >& Groups_) SAL_THROW(())
    : HasAutoStart(HasAutoStart_)
    , HasAutoEnd(HasAutoEnd_)
    , HasDateValues(HasDateValues_)
    , Start(Start_)
    , End(End_)
    , Step(Step_)
    , GroupBy(GroupBy_)
    , SourceField(SourceField_)
    , Groups(Groups_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theDataPilotFieldGroupInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theDataPilotFieldGroupInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.DataPilotFieldGroupInfo" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "HasAutoStart" );
        ::rtl::OUString the_name1( "HasAutoEnd" );
        ::rtl::OUString the_name2( "HasDateValues" );
        ::rtl::OUString the_tname1( "double" );
        ::rtl::OUString the_name3( "Start" );
        ::rtl::OUString the_name4( "End" );
        ::rtl::OUString the_name5( "Step" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name6( "GroupBy" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::sheet::XDataPilotField > >::get();
        ::rtl::OUString the_tname3( "com.sun.star.sheet.XDataPilotField" );
        ::rtl::OUString the_name7( "SourceField" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::container::XNameAccess > >::get();
        ::rtl::OUString the_tname4( "com.sun.star.container.XNameAccess" );
        ::rtl::OUString the_name8( "Groups" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name6.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name7.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname4.pData, the_name8.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 9, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldGroupInfo const *) {
    return *detail::theDataPilotFieldGroupInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::DataPilotFieldGroupInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::DataPilotFieldGroupInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_DATAPILOTFIELDGROUPINFO_HPP
