#ifndef INCLUDED_COM_SUN_STAR_SHEET_GOALRESULT_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_GOALRESULT_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/GoalResult.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet {

inline GoalResult::GoalResult() SAL_THROW(())
    : Divergence(0)
    , Result(0)
{
}

inline GoalResult::GoalResult(const double& Divergence_, const double& Result_) SAL_THROW(())
    : Divergence(Divergence_)
    , Result(Result_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace detail {

struct theGoalResultType : public rtl::StaticWithInit< ::css::uno::Type *, theGoalResultType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.GoalResult" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Divergence" );
        ::rtl::OUString the_name1( "Result" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sheet {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::GoalResult const *) {
    return *detail::theGoalResultType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::GoalResult const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::GoalResult >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_GOALRESULT_HPP
