#ifndef INCLUDED_COM_SUN_STAR_SHEET_XACTIVATIONBROADCASTER_HDL
#define INCLUDED_COM_SUN_STAR_SHEET_XACTIVATIONBROADCASTER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace sheet { class XActivationEventListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sheet {

class SAL_NO_VTABLE XActivationBroadcaster : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addActivationEventListener( const ::css::uno::Reference< ::css::sheet::XActivationEventListener >& aListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeActivationEventListener( const ::css::uno::Reference< ::css::sheet::XActivationEventListener >& aListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XActivationBroadcaster() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::XActivationBroadcaster const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sheet::XActivationBroadcaster > *) SAL_THROW(());

#endif
