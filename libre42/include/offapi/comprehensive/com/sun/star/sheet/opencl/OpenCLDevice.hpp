#ifndef INCLUDED_COM_SUN_STAR_SHEET_OPENCL_OPENCLDEVICE_HPP
#define INCLUDED_COM_SUN_STAR_SHEET_OPENCL_OPENCLDEVICE_HPP

#include "sal/config.h"

#include "com/sun/star/sheet/opencl/OpenCLDevice.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sheet { namespace opencl {

inline OpenCLDevice::OpenCLDevice() SAL_THROW(())
    : Name()
    , Vendor()
    , Driver()
{
}

inline OpenCLDevice::OpenCLDevice(const ::rtl::OUString& Name_, const ::rtl::OUString& Vendor_, const ::rtl::OUString& Driver_) SAL_THROW(())
    : Name(Name_)
    , Vendor(Vendor_)
    , Driver(Driver_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace opencl { namespace detail {

struct theOpenCLDeviceType : public rtl::StaticWithInit< ::css::uno::Type *, theOpenCLDeviceType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sheet.opencl.OpenCLDevice" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::rtl::OUString the_name1( "Vendor" );
        ::rtl::OUString the_name2( "Driver" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace sheet { namespace opencl {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sheet::opencl::OpenCLDevice const *) {
    return *detail::theOpenCLDeviceType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sheet::opencl::OpenCLDevice const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sheet::opencl::OpenCLDevice >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SHEET_OPENCL_OPENCLDEVICE_HPP
