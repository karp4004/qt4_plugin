#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPENDENCYEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPENDENCYEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/DependencyException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/xml/dom/XElement.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

inline DependencyException::DependencyException() SAL_THROW(())
    : ::css::uno::Exception()
    , UnsatisfiedDependencies()
{
    ::cppu::UnoType< ::css::deployment::DependencyException >::get();
}

inline DependencyException::DependencyException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Sequence< ::css::uno::Reference< ::css::xml::dom::XElement > >& UnsatisfiedDependencies_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , UnsatisfiedDependencies(UnsatisfiedDependencies_)
{
    ::cppu::UnoType< ::css::deployment::DependencyException >::get();
}

DependencyException::DependencyException(DependencyException const & the_other): ::css::uno::Exception(the_other), UnsatisfiedDependencies(the_other.UnsatisfiedDependencies) {}

DependencyException::~DependencyException() {}

DependencyException & DependencyException::operator =(DependencyException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    UnsatisfiedDependencies = the_other.UnsatisfiedDependencies;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment { namespace detail {

struct theDependencyExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theDependencyExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.deployment.DependencyException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::uno::Reference< ::css::xml::dom::XElement > > >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "[]com.sun.star.xml.dom.XElement" );
        ::rtl::OUString sMemberName0( "UnsatisfiedDependencies" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::DependencyException const *) {
    return *detail::theDependencyExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::DependencyException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::DependencyException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPENDENCYEXCEPTION_HPP
