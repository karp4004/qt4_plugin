#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_PLATFORMEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_PLATFORMEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/PlatformException.hdl"

#include "com/sun/star/deployment/XPackage.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

inline PlatformException::PlatformException() SAL_THROW(())
    : ::css::uno::Exception()
    , package()
{
    ::cppu::UnoType< ::css::deployment::PlatformException >::get();
}

inline PlatformException::PlatformException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Reference< ::css::deployment::XPackage >& package_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , package(package_)
{
    ::cppu::UnoType< ::css::deployment::PlatformException >::get();
}

PlatformException::PlatformException(PlatformException const & the_other): ::css::uno::Exception(the_other), package(the_other.package) {}

PlatformException::~PlatformException() {}

PlatformException & PlatformException::operator =(PlatformException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    package = the_other.package;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment { namespace detail {

struct thePlatformExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, thePlatformExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.deployment.PlatformException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::deployment::XPackage > >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "com.sun.star.deployment.XPackage" );
        ::rtl::OUString sMemberName0( "package" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::PlatformException const *) {
    return *detail::thePlatformExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::PlatformException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::PlatformException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_PLATFORMEXCEPTION_HPP
