#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_XPACKAGETYPEINFO_HDL
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_XPACKAGETYPEINFO_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/deployment/ExtensionRemovedException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace deployment {

class SAL_NO_VTABLE XPackageTypeInfo : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::rtl::OUString SAL_CALL getMediaType() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getDescription() /* throw (::css::deployment::ExtensionRemovedException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getShortDescription() /* throw (::css::deployment::ExtensionRemovedException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getFileFilter() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getIcon( ::sal_Bool highContrast, ::sal_Bool smallIcon ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPackageTypeInfo() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::XPackageTypeInfo const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::deployment::XPackageTypeInfo > *) SAL_THROW(());

#endif
