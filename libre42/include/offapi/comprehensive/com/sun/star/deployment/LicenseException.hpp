#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_LICENSEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_LICENSEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/LicenseException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

inline LicenseException::LicenseException() SAL_THROW(())
    : ::css::uno::Exception()
    , ExtensionName()
    , Text()
    , AcceptBy()
{
    ::cppu::UnoType< ::css::deployment::LicenseException >::get();
}

inline LicenseException::LicenseException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& ExtensionName_, const ::rtl::OUString& Text_, const ::rtl::OUString& AcceptBy_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ExtensionName(ExtensionName_)
    , Text(Text_)
    , AcceptBy(AcceptBy_)
{
    ::cppu::UnoType< ::css::deployment::LicenseException >::get();
}

LicenseException::LicenseException(LicenseException const & the_other): ::css::uno::Exception(the_other), ExtensionName(the_other.ExtensionName), Text(the_other.Text), AcceptBy(the_other.AcceptBy) {}

LicenseException::~LicenseException() {}

LicenseException & LicenseException::operator =(LicenseException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ExtensionName = the_other.ExtensionName;
    Text = the_other.Text;
    AcceptBy = the_other.AcceptBy;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment { namespace detail {

struct theLicenseExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theLicenseExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.deployment.LicenseException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "ExtensionName" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "Text" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "string" );
        ::rtl::OUString sMemberName2( "AcceptBy" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::LicenseException const *) {
    return *detail::theLicenseExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::LicenseException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::LicenseException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_LICENSEEXCEPTION_HPP
