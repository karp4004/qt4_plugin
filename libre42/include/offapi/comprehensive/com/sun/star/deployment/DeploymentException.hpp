#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPLOYMENTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPLOYMENTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/DeploymentException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

inline DeploymentException::DeploymentException() SAL_THROW(())
    : ::css::uno::Exception()
    , Cause()
{
    ::cppu::UnoType< ::css::deployment::DeploymentException >::get();
}

inline DeploymentException::DeploymentException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& Cause_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Cause(Cause_)
{
    ::cppu::UnoType< ::css::deployment::DeploymentException >::get();
}

DeploymentException::DeploymentException(DeploymentException const & the_other): ::css::uno::Exception(the_other), Cause(the_other.Cause) {}

DeploymentException::~DeploymentException() {}

DeploymentException & DeploymentException::operator =(DeploymentException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Cause = the_other.Cause;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment { namespace detail {

struct theDeploymentExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theDeploymentExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.deployment.DeploymentException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "any" );
        ::rtl::OUString sMemberName0( "Cause" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ANY;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::DeploymentException const *) {
    return *detail::theDeploymentExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::DeploymentException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::DeploymentException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_DEPLOYMENTEXCEPTION_HPP
