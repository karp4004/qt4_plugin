#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_XUPDATEINFORMATIONPROVIDER_HDL
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_XUPDATEINFORMATIONPROVIDER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace container { class XEnumeration; } } } }
namespace com { namespace sun { namespace star { namespace task { class XInteractionHandler; } } } }
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace dom { class XElement; } } } } }
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace deployment {

class SAL_NO_VTABLE XUpdateInformationProvider : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::xml::dom::XElement > > SAL_CALL getUpdateInformation( const ::css::uno::Sequence< ::rtl::OUString >& repositories, const ::rtl::OUString& extensionId ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL cancel() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setInteractionHandler( const ::css::uno::Reference< ::css::task::XInteractionHandler >& handler ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::container::XEnumeration > SAL_CALL getUpdateInformationEnumeration( const ::css::uno::Sequence< ::rtl::OUString >& repositories, const ::rtl::OUString& extensionId ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XUpdateInformationProvider() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::XUpdateInformationProvider const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::deployment::XUpdateInformationProvider > *) SAL_THROW(());

#endif
