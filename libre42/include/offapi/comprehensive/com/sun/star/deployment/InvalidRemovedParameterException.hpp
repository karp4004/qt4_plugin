#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_INVALIDREMOVEDPARAMETEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_INVALIDREMOVEDPARAMETEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/InvalidRemovedParameterException.hdl"

#include "com/sun/star/deployment/XPackage.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace deployment {

inline InvalidRemovedParameterException::InvalidRemovedParameterException() SAL_THROW(())
    : ::css::uno::Exception()
    , PreviousValue(false)
    , Extension()
{
    ::cppu::UnoType< ::css::deployment::InvalidRemovedParameterException >::get();
}

inline InvalidRemovedParameterException::InvalidRemovedParameterException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Bool& PreviousValue_, const ::css::uno::Reference< ::css::deployment::XPackage >& Extension_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , PreviousValue(PreviousValue_)
    , Extension(Extension_)
{
    ::cppu::UnoType< ::css::deployment::InvalidRemovedParameterException >::get();
}

InvalidRemovedParameterException::InvalidRemovedParameterException(InvalidRemovedParameterException const & the_other): ::css::uno::Exception(the_other), PreviousValue(the_other.PreviousValue), Extension(the_other.Extension) {}

InvalidRemovedParameterException::~InvalidRemovedParameterException() {}

InvalidRemovedParameterException & InvalidRemovedParameterException::operator =(InvalidRemovedParameterException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    PreviousValue = the_other.PreviousValue;
    Extension = the_other.Extension;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment { namespace detail {

struct theInvalidRemovedParameterExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInvalidRemovedParameterExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.deployment.InvalidRemovedParameterException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::deployment::XPackage > >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "boolean" );
        ::rtl::OUString sMemberName0( "PreviousValue" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "com.sun.star.deployment.XPackage" );
        ::rtl::OUString sMemberName1( "Extension" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::InvalidRemovedParameterException const *) {
    return *detail::theInvalidRemovedParameterExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::InvalidRemovedParameterException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::InvalidRemovedParameterException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_INVALIDREMOVEDPARAMETEREXCEPTION_HPP
