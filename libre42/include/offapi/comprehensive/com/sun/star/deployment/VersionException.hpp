#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_VERSIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_VERSIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/VersionException.hdl"

#include "com/sun/star/deployment/XPackage.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

inline VersionException::VersionException() SAL_THROW(())
    : ::css::uno::Exception()
    , NewVersion()
    , NewDisplayName()
    , Deployed()
{
    ::cppu::UnoType< ::css::deployment::VersionException >::get();
}

inline VersionException::VersionException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& NewVersion_, const ::rtl::OUString& NewDisplayName_, const ::css::uno::Reference< ::css::deployment::XPackage >& Deployed_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , NewVersion(NewVersion_)
    , NewDisplayName(NewDisplayName_)
    , Deployed(Deployed_)
{
    ::cppu::UnoType< ::css::deployment::VersionException >::get();
}

VersionException::VersionException(VersionException const & the_other): ::css::uno::Exception(the_other), NewVersion(the_other.NewVersion), NewDisplayName(the_other.NewDisplayName), Deployed(the_other.Deployed) {}

VersionException::~VersionException() {}

VersionException & VersionException::operator =(VersionException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    NewVersion = the_other.NewVersion;
    NewDisplayName = the_other.NewDisplayName;
    Deployed = the_other.Deployed;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment { namespace detail {

struct theVersionExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theVersionExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.deployment.VersionException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::deployment::XPackage > >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "NewVersion" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "NewDisplayName" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "com.sun.star.deployment.XPackage" );
        ::rtl::OUString sMemberName2( "Deployed" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::VersionException const *) {
    return *detail::theVersionExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::VersionException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::VersionException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_VERSIONEXCEPTION_HPP
