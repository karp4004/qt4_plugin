#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_EXTENSIONMANAGER_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_EXTENSIONMANAGER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/deployment/XExtensionManager.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace deployment {

class ExtensionManager {
public:
    static ::css::uno::Reference< ::css::deployment::XExtensionManager > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::deployment::XExtensionManager > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.deployment.ExtensionManager" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.deployment.ExtensionManager of type com.sun.star.deployment.XExtensionManager" ), the_context);
        }
        return instance;
    }

private:
    ExtensionManager(); // not implemented
    ExtensionManager(ExtensionManager &); // not implemented
    ~ExtensionManager(); // not implemented
    void operator =(ExtensionManager); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_EXTENSIONMANAGER_HPP
