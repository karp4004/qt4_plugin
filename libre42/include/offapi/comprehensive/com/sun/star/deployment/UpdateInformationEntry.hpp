#ifndef INCLUDED_COM_SUN_STAR_DEPLOYMENT_UPDATEINFORMATIONENTRY_HPP
#define INCLUDED_COM_SUN_STAR_DEPLOYMENT_UPDATEINFORMATIONENTRY_HPP

#include "sal/config.h"

#include "com/sun/star/deployment/UpdateInformationEntry.hdl"

#include "com/sun/star/xml/dom/XElement.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace deployment {

inline UpdateInformationEntry::UpdateInformationEntry() SAL_THROW(())
    : UpdateDocument()
    , Description()
{
}

inline UpdateInformationEntry::UpdateInformationEntry(const ::css::uno::Reference< ::css::xml::dom::XElement >& UpdateDocument_, const ::rtl::OUString& Description_) SAL_THROW(())
    : UpdateDocument(UpdateDocument_)
    , Description(Description_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace deployment { namespace detail {

struct theUpdateInformationEntryType : public rtl::StaticWithInit< ::css::uno::Type *, theUpdateInformationEntryType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.deployment.UpdateInformationEntry" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::xml::dom::XElement > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.xml.dom.XElement" );
        ::rtl::OUString the_name0( "UpdateDocument" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "Description" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace deployment {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::deployment::UpdateInformationEntry const *) {
    return *detail::theUpdateInformationEntryType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::deployment::UpdateInformationEntry const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::deployment::UpdateInformationEntry >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DEPLOYMENT_UPDATEINFORMATIONENTRY_HPP
