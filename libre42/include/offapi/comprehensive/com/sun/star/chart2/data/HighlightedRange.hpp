#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATA_HIGHLIGHTEDRANGE_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_DATA_HIGHLIGHTEDRANGE_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/data/HighlightedRange.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

inline HighlightedRange::HighlightedRange() SAL_THROW(())
    : RangeRepresentation()
    , Index(0)
    , PreferredColor(0)
    , AllowMerginigWithOtherRanges(false)
{
}

inline HighlightedRange::HighlightedRange(const ::rtl::OUString& RangeRepresentation_, const ::sal_Int32& Index_, const ::sal_Int32& PreferredColor_, const ::sal_Bool& AllowMerginigWithOtherRanges_) SAL_THROW(())
    : RangeRepresentation(RangeRepresentation_)
    , Index(Index_)
    , PreferredColor(PreferredColor_)
    , AllowMerginigWithOtherRanges(AllowMerginigWithOtherRanges_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace data { namespace detail {

struct theHighlightedRangeType : public rtl::StaticWithInit< ::css::uno::Type *, theHighlightedRangeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.data.HighlightedRange" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "RangeRepresentation" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "Index" );
        ::rtl::OUString the_name2( "PreferredColor" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name3( "AllowMerginigWithOtherRanges" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::data::HighlightedRange const *) {
    return *detail::theHighlightedRangeType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::data::HighlightedRange const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::data::HighlightedRange >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_DATA_HIGHLIGHTEDRANGE_HPP
