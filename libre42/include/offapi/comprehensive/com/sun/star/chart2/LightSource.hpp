#ifndef INCLUDED_COM_SUN_STAR_CHART2_LIGHTSOURCE_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_LIGHTSOURCE_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/LightSource.hdl"

#include "com/sun/star/drawing/Direction3D.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline LightSource::LightSource() SAL_THROW(())
    : nDiffuseColor(0)
    , aDirection()
    , bIsEnabled(false)
    , bSpecular(false)
{
}

inline LightSource::LightSource(const ::sal_Int32& nDiffuseColor_, const ::css::drawing::Direction3D& aDirection_, const ::sal_Bool& bIsEnabled_, const ::sal_Bool& bSpecular_) SAL_THROW(())
    : nDiffuseColor(nDiffuseColor_)
    , aDirection(aDirection_)
    , bIsEnabled(bIsEnabled_)
    , bSpecular(bSpecular_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theLightSourceType : public rtl::StaticWithInit< ::css::uno::Type *, theLightSourceType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.LightSource" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "nDiffuseColor" );
        ::cppu::UnoType< ::css::drawing::Direction3D >::get();
        ::rtl::OUString the_tname1( "com.sun.star.drawing.Direction3D" );
        ::rtl::OUString the_name1( "aDirection" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name2( "bIsEnabled" );
        ::rtl::OUString the_name3( "bSpecular" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::LightSource const *) {
    return *detail::theLightSourceType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::LightSource const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::LightSource >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_LIGHTSOURCE_HPP
