#ifndef INCLUDED_COM_SUN_STAR_CHART2_INCREMENTDATA_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_INCREMENTDATA_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/IncrementData.hdl"

#include "com/sun/star/chart2/SubIncrement.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline IncrementData::IncrementData() SAL_THROW(())
    : Distance()
    , PostEquidistant()
    , BaseValue()
    , SubIncrements()
{
}

inline IncrementData::IncrementData(const ::css::uno::Any& Distance_, const ::css::uno::Any& PostEquidistant_, const ::css::uno::Any& BaseValue_, const ::css::uno::Sequence< ::css::chart2::SubIncrement >& SubIncrements_) SAL_THROW(())
    : Distance(Distance_)
    , PostEquidistant(PostEquidistant_)
    , BaseValue(BaseValue_)
    , SubIncrements(SubIncrements_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theIncrementDataType : public rtl::StaticWithInit< ::css::uno::Type *, theIncrementDataType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.IncrementData" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "Distance" );
        ::rtl::OUString the_name1( "PostEquidistant" );
        ::rtl::OUString the_name2( "BaseValue" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::chart2::SubIncrement > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.chart2.SubIncrement" );
        ::rtl::OUString the_name3( "SubIncrements" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::IncrementData const *) {
    return *detail::theIncrementDataType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::IncrementData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::IncrementData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_INCREMENTDATA_HPP
