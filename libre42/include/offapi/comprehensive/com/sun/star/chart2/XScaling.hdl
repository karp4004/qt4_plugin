#ifndef INCLUDED_COM_SUN_STAR_CHART2_XSCALING_HDL
#define INCLUDED_COM_SUN_STAR_CHART2_XSCALING_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace chart2 { class XScaling; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

class SAL_NO_VTABLE XScaling : public ::css::uno::XInterface
{
public:

    // Methods
    virtual double SAL_CALL doScaling( double value ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::chart2::XScaling > SAL_CALL getInverseScaling() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XScaling() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::XScaling const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart2::XScaling > *) SAL_THROW(());

#endif
