#ifndef INCLUDED_COM_SUN_STAR_CHART2_SYMBOL_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_SYMBOL_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/Symbol.hdl"

#include "com/sun/star/awt/Size.hpp"
#include "com/sun/star/chart2/SymbolStyle.hpp"
#include "com/sun/star/drawing/PolyPolygonBezierCoords.hpp"
#include "com/sun/star/graphic/XGraphic.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline Symbol::Symbol() SAL_THROW(())
    : Style(::css::chart2::SymbolStyle_NONE)
    , PolygonCoords()
    , StandardSymbol(0)
    , Graphic()
    , Size()
    , BorderColor(0)
    , FillColor(0)
{
}

inline Symbol::Symbol(const ::css::chart2::SymbolStyle& Style_, const ::css::drawing::PolyPolygonBezierCoords& PolygonCoords_, const ::sal_Int32& StandardSymbol_, const ::css::uno::Reference< ::css::graphic::XGraphic >& Graphic_, const ::css::awt::Size& Size_, const ::sal_Int32& BorderColor_, const ::sal_Int32& FillColor_) SAL_THROW(())
    : Style(Style_)
    , PolygonCoords(PolygonCoords_)
    , StandardSymbol(StandardSymbol_)
    , Graphic(Graphic_)
    , Size(Size_)
    , BorderColor(BorderColor_)
    , FillColor(FillColor_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theSymbolType : public rtl::StaticWithInit< ::css::uno::Type *, theSymbolType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.Symbol" );
        ::cppu::UnoType< ::css::chart2::SymbolStyle >::get();
        ::rtl::OUString the_tname0( "com.sun.star.chart2.SymbolStyle" );
        ::rtl::OUString the_name0( "Style" );
        ::cppu::UnoType< ::css::drawing::PolyPolygonBezierCoords >::get();
        ::rtl::OUString the_tname1( "com.sun.star.drawing.PolyPolygonBezierCoords" );
        ::rtl::OUString the_name1( "PolygonCoords" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name2( "StandardSymbol" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::graphic::XGraphic > >::get();
        ::rtl::OUString the_tname3( "com.sun.star.graphic.XGraphic" );
        ::rtl::OUString the_name3( "Graphic" );
        ::cppu::UnoType< ::css::awt::Size >::get();
        ::rtl::OUString the_tname4( "com.sun.star.awt.Size" );
        ::rtl::OUString the_name4( "Size" );
        ::rtl::OUString the_name5( "BorderColor" );
        ::rtl::OUString the_name6( "FillColor" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname4.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::Symbol const *) {
    return *detail::theSymbolType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::Symbol const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::Symbol >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_SYMBOL_HPP
