#ifndef INCLUDED_COM_SUN_STAR_CHART2_SCALEDATA_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_SCALEDATA_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/ScaleData.hdl"

#include "com/sun/star/chart/TimeIncrement.hpp"
#include "com/sun/star/chart2/AxisOrientation.hpp"
#include "com/sun/star/chart2/IncrementData.hpp"
#include "com/sun/star/chart2/XScaling.hpp"
#include "com/sun/star/chart2/data/XLabeledDataSequence.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline ScaleData::ScaleData() SAL_THROW(())
    : Minimum()
    , Maximum()
    , Origin()
    , Orientation(::css::chart2::AxisOrientation_MATHEMATICAL)
    , Scaling()
    , Categories()
    , AxisType(0)
    , AutoDateAxis(false)
    , ShiftedCategoryPosition(false)
    , IncrementData()
    , TimeIncrement()
{
}

inline ScaleData::ScaleData(const ::css::uno::Any& Minimum_, const ::css::uno::Any& Maximum_, const ::css::uno::Any& Origin_, const ::css::chart2::AxisOrientation& Orientation_, const ::css::uno::Reference< ::css::chart2::XScaling >& Scaling_, const ::css::uno::Reference< ::css::chart2::data::XLabeledDataSequence >& Categories_, const ::sal_Int32& AxisType_, const ::sal_Bool& AutoDateAxis_, const ::sal_Bool& ShiftedCategoryPosition_, const ::css::chart2::IncrementData& IncrementData_, const ::css::chart::TimeIncrement& TimeIncrement_) SAL_THROW(())
    : Minimum(Minimum_)
    , Maximum(Maximum_)
    , Origin(Origin_)
    , Orientation(Orientation_)
    , Scaling(Scaling_)
    , Categories(Categories_)
    , AxisType(AxisType_)
    , AutoDateAxis(AutoDateAxis_)
    , ShiftedCategoryPosition(ShiftedCategoryPosition_)
    , IncrementData(IncrementData_)
    , TimeIncrement(TimeIncrement_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theScaleDataType : public rtl::StaticWithInit< ::css::uno::Type *, theScaleDataType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.ScaleData" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "Minimum" );
        ::rtl::OUString the_name1( "Maximum" );
        ::rtl::OUString the_name2( "Origin" );
        ::cppu::UnoType< ::css::chart2::AxisOrientation >::get();
        ::rtl::OUString the_tname1( "com.sun.star.chart2.AxisOrientation" );
        ::rtl::OUString the_name3( "Orientation" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::chart2::XScaling > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.chart2.XScaling" );
        ::rtl::OUString the_name4( "Scaling" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::chart2::data::XLabeledDataSequence > >::get();
        ::rtl::OUString the_tname3( "com.sun.star.chart2.data.XLabeledDataSequence" );
        ::rtl::OUString the_name5( "Categories" );
        ::rtl::OUString the_tname4( "long" );
        ::rtl::OUString the_name6( "AxisType" );
        ::rtl::OUString the_tname5( "boolean" );
        ::rtl::OUString the_name7( "AutoDateAxis" );
        ::rtl::OUString the_name8( "ShiftedCategoryPosition" );
        ::cppu::UnoType< ::css::chart2::IncrementData >::get();
        ::rtl::OUString the_tname6( "com.sun.star.chart2.IncrementData" );
        ::rtl::OUString the_name9( "IncrementData" );
        ::cppu::UnoType< ::css::chart::TimeIncrement >::get();
        ::rtl::OUString the_tname7( "com.sun.star.chart.TimeIncrement" );
        ::rtl::OUString the_name10( "TimeIncrement" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name4.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name5.pData }, false },
            { { typelib_TypeClass_LONG, the_tname4.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname5.pData, the_name7.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname5.pData, the_name8.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname6.pData, the_name9.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname7.pData, the_name10.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 11, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::ScaleData const *) {
    return *detail::theScaleDataType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::ScaleData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::ScaleData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_SCALEDATA_HPP
