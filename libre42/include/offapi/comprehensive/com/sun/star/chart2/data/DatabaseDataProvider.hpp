#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATA_DATABASEDATAPROVIDER_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_DATA_DATABASEDATAPROVIDER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/chart2/data/XDatabaseDataProvider.hpp"
#include "com/sun/star/sdbc/XConnection.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

class DatabaseDataProvider {
public:
    static ::css::uno::Reference< ::css::chart2::data::XDatabaseDataProvider > createWithConnection(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Reference< ::css::sdbc::XConnection >& connection) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= connection;
        ::css::uno::Reference< ::css::chart2::data::XDatabaseDataProvider > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::chart2::data::XDatabaseDataProvider >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.chart2.data.DatabaseDataProvider" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.chart2.data.DatabaseDataProvider of type com.sun.star.chart2.data.XDatabaseDataProvider: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.chart2.data.DatabaseDataProvider of type com.sun.star.chart2.data.XDatabaseDataProvider" ), the_context);
        }
        return the_instance;
    }

private:
    DatabaseDataProvider(); // not implemented
    DatabaseDataProvider(DatabaseDataProvider &); // not implemented
    ~DatabaseDataProvider(); // not implemented
    void operator =(DatabaseDataProvider); // not implemented
};

} } } } }

#endif // INCLUDED_COM_SUN_STAR_CHART2_DATA_DATABASEDATAPROVIDER_HPP
