#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATABASEDATAPROVIDER_HDL
#define INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATABASEDATAPROVIDER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/UnknownPropertyException.hdl"
#include "com/sun/star/beans/XPropertySet.hdl"
#include "com/sun/star/chart2/data/XDataProvider.hdl"
#include "com/sun/star/chart2/data/XRangeXMLConversion.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/XComponent.hdl"
#include "com/sun/star/lang/XInitialization.hdl"
namespace com { namespace sun { namespace star { namespace sdbc { class XConnection; } } } }
#include "com/sun/star/sdbc/XParameters.hdl"
#include "com/sun/star/sdbc/XRowSet.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

class SAL_NO_VTABLE XDatabaseDataProvider : public ::css::chart2::data::XDataProvider, public ::css::chart2::data::XRangeXMLConversion, public ::css::lang::XInitialization, public ::css::lang::XComponent, public ::css::beans::XPropertySet, public ::css::sdbc::XParameters, public ::css::sdbc::XRowSet
{
public:

    // Attributes
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getMasterFields() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMasterFields( const ::css::uno::Sequence< ::rtl::OUString >& _masterfields ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getDetailFields() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setDetailFields( const ::css::uno::Sequence< ::rtl::OUString >& _detailfields ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getCommand() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setCommand( const ::rtl::OUString& _command ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getCommandType() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setCommandType( ::sal_Int32 _commandtype ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getFilter() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setFilter( const ::rtl::OUString& _filter ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getApplyFilter() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setApplyFilter( ::sal_Bool _applyfilter ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getHavingClause() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setHavingClause( const ::rtl::OUString& _havingclause ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getGroupBy() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setGroupBy( const ::rtl::OUString& _groupby ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getOrder() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setOrder( const ::rtl::OUString& _order ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getEscapeProcessing() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setEscapeProcessing( ::sal_Bool _escapeprocessing ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getRowLimit() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRowLimit( ::sal_Int32 _rowlimit ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::sdbc::XConnection > SAL_CALL getActiveConnection() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setActiveConnection( const ::css::uno::Reference< ::css::sdbc::XConnection >& _activeconnection ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getDataSourceName() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setDataSourceName( const ::rtl::OUString& _datasourcename ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDatabaseDataProvider() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::data::XDatabaseDataProvider const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart2::data::XDatabaseDataProvider > *) SAL_THROW(());

#endif
