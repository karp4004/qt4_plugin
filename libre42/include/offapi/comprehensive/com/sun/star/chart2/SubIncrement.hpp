#ifndef INCLUDED_COM_SUN_STAR_CHART2_SUBINCREMENT_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_SUBINCREMENT_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/SubIncrement.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline SubIncrement::SubIncrement() SAL_THROW(())
    : IntervalCount()
    , PostEquidistant()
{
}

inline SubIncrement::SubIncrement(const ::css::uno::Any& IntervalCount_, const ::css::uno::Any& PostEquidistant_) SAL_THROW(())
    : IntervalCount(IntervalCount_)
    , PostEquidistant(PostEquidistant_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theSubIncrementType : public rtl::StaticWithInit< ::css::uno::Type *, theSubIncrementType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.SubIncrement" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "IntervalCount" );
        ::rtl::OUString the_name1( "PostEquidistant" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::SubIncrement const *) {
    return *detail::theSubIncrementType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::SubIncrement const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::SubIncrement >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_SUBINCREMENT_HPP
