#ifndef INCLUDED_COM_SUN_STAR_CHART2_INTERPRETEDDATA_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_INTERPRETEDDATA_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/InterpretedData.hdl"

#include "com/sun/star/chart2/XDataSeries.hpp"
#include "com/sun/star/chart2/data/XLabeledDataSequence.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline InterpretedData::InterpretedData() SAL_THROW(())
    : Series()
    , Categories()
{
}

inline InterpretedData::InterpretedData(const ::css::uno::Sequence< ::css::uno::Sequence< ::css::uno::Reference< ::css::chart2::XDataSeries > > >& Series_, const ::css::uno::Reference< ::css::chart2::data::XLabeledDataSequence >& Categories_) SAL_THROW(())
    : Series(Series_)
    , Categories(Categories_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theInterpretedDataType : public rtl::StaticWithInit< ::css::uno::Type *, theInterpretedDataType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.InterpretedData" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::cppu::UnoSequenceType< ::css::uno::Reference< ::css::chart2::XDataSeries > > > >::get();
        ::rtl::OUString the_tname0( "[][]com.sun.star.chart2.XDataSeries" );
        ::rtl::OUString the_name0( "Series" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::chart2::data::XLabeledDataSequence > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.chart2.data.XLabeledDataSequence" );
        ::rtl::OUString the_name1( "Categories" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::InterpretedData const *) {
    return *detail::theInterpretedDataType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::InterpretedData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::InterpretedData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_INTERPRETEDDATA_HPP
