#ifndef INCLUDED_COM_SUN_STAR_CHART2_RELATIVESIZE_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_RELATIVESIZE_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/RelativeSize.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline RelativeSize::RelativeSize() SAL_THROW(())
    : Primary(0)
    , Secondary(0)
{
}

inline RelativeSize::RelativeSize(const double& Primary_, const double& Secondary_) SAL_THROW(())
    : Primary(Primary_)
    , Secondary(Secondary_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theRelativeSizeType : public rtl::StaticWithInit< ::css::uno::Type *, theRelativeSizeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.RelativeSize" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Primary" );
        ::rtl::OUString the_name1( "Secondary" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::RelativeSize const *) {
    return *detail::theRelativeSizeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::RelativeSize const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::RelativeSize >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_RELATIVESIZE_HPP
