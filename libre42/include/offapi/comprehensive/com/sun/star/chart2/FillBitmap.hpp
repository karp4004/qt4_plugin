#ifndef INCLUDED_COM_SUN_STAR_CHART2_FILLBITMAP_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_FILLBITMAP_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/FillBitmap.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/awt/Size.hpp"
#include "com/sun/star/drawing/BitmapMode.hpp"
#include "com/sun/star/drawing/RectanglePoint.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline FillBitmap::FillBitmap() SAL_THROW(())
    : aURL()
    , aOffset()
    , aPositionOffset()
    , aRectanglePoint(::css::drawing::RectanglePoint_LEFT_TOP)
    , bLogicalSize(false)
    , aSize()
    , aBitmapMode(::css::drawing::BitmapMode_REPEAT)
{
}

inline FillBitmap::FillBitmap(const ::rtl::OUString& aURL_, const ::css::awt::Point& aOffset_, const ::css::awt::Point& aPositionOffset_, const ::css::drawing::RectanglePoint& aRectanglePoint_, const ::sal_Bool& bLogicalSize_, const ::css::awt::Size& aSize_, const ::css::drawing::BitmapMode& aBitmapMode_) SAL_THROW(())
    : aURL(aURL_)
    , aOffset(aOffset_)
    , aPositionOffset(aPositionOffset_)
    , aRectanglePoint(aRectanglePoint_)
    , bLogicalSize(bLogicalSize_)
    , aSize(aSize_)
    , aBitmapMode(aBitmapMode_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theFillBitmapType : public rtl::StaticWithInit< ::css::uno::Type *, theFillBitmapType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.FillBitmap" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "aURL" );
        ::cppu::UnoType< ::css::awt::Point >::get();
        ::rtl::OUString the_tname1( "com.sun.star.awt.Point" );
        ::rtl::OUString the_name1( "aOffset" );
        ::rtl::OUString the_name2( "aPositionOffset" );
        ::cppu::UnoType< ::css::drawing::RectanglePoint >::get();
        ::rtl::OUString the_tname2( "com.sun.star.drawing.RectanglePoint" );
        ::rtl::OUString the_name3( "aRectanglePoint" );
        ::rtl::OUString the_tname3( "boolean" );
        ::rtl::OUString the_name4( "bLogicalSize" );
        ::cppu::UnoType< ::css::awt::Size >::get();
        ::rtl::OUString the_tname4( "com.sun.star.awt.Size" );
        ::rtl::OUString the_name5( "aSize" );
        ::cppu::UnoType< ::css::drawing::BitmapMode >::get();
        ::rtl::OUString the_tname5( "com.sun.star.drawing.BitmapMode" );
        ::rtl::OUString the_name6( "aBitmapMode" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname3.pData, the_name4.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname4.pData, the_name5.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname5.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::FillBitmap const *) {
    return *detail::theFillBitmapType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::FillBitmap const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::FillBitmap >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_FILLBITMAP_HPP
