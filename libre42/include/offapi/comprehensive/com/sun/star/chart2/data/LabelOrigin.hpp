#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATA_LABELORIGIN_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_DATA_LABELORIGIN_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/data/LabelOrigin.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace chart2 { namespace data { namespace detail {

struct theLabelOriginType : public rtl::StaticWithInit< ::css::uno::Type *, theLabelOriginType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.chart2.data.LabelOrigin" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[4];
        ::rtl::OUString sEnumValue0( "SHORT_SIDE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "LONG_SIDE" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "COLUMN" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "ROW" );
        enumValueNames[3] = sEnumValue3.pData;

        sal_Int32 enumValues[4];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::chart2::data::LabelOrigin_SHORT_SIDE,
            4, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::data::LabelOrigin const *) {
    return *detail::theLabelOriginType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::data::LabelOrigin const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::data::LabelOrigin >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_DATA_LABELORIGIN_HPP
