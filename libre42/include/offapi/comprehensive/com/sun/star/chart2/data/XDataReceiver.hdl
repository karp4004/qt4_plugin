#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATARECEIVER_HDL
#define INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATARECEIVER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
namespace com { namespace sun { namespace star { namespace chart2 { namespace data { class XDataProvider; } } } } }
namespace com { namespace sun { namespace star { namespace chart2 { namespace data { class XDataSource; } } } } }
namespace com { namespace sun { namespace star { namespace chart2 { namespace data { class XRangeHighlighter; } } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
namespace com { namespace sun { namespace star { namespace util { class XNumberFormatsSupplier; } } } }
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

class SAL_NO_VTABLE XDataReceiver : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL attachDataProvider( const ::css::uno::Reference< ::css::chart2::data::XDataProvider >& xProvider ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setArguments( const ::css::uno::Sequence< ::css::beans::PropertyValue >& aArguments ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getUsedRangeRepresentations() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::chart2::data::XDataSource > SAL_CALL getUsedData() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL attachNumberFormatsSupplier( const ::css::uno::Reference< ::css::util::XNumberFormatsSupplier >& xSupplier ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::chart2::data::XRangeHighlighter > SAL_CALL getRangeHighlighter() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDataReceiver() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::data::XDataReceiver const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::chart2::data::XDataReceiver > *) SAL_THROW(());

#endif
