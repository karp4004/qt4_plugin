#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATAPOINTLABEL_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_DATAPOINTLABEL_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/DataPointLabel.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace chart2 {

inline DataPointLabel::DataPointLabel() SAL_THROW(())
    : ShowNumber(false)
    , ShowNumberInPercent(false)
    , ShowCategoryName(false)
    , ShowLegendSymbol(false)
{
}

inline DataPointLabel::DataPointLabel(const ::sal_Bool& ShowNumber_, const ::sal_Bool& ShowNumberInPercent_, const ::sal_Bool& ShowCategoryName_, const ::sal_Bool& ShowLegendSymbol_) SAL_THROW(())
    : ShowNumber(ShowNumber_)
    , ShowNumberInPercent(ShowNumberInPercent_)
    , ShowCategoryName(ShowCategoryName_)
    , ShowLegendSymbol(ShowLegendSymbol_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theDataPointLabelType : public rtl::StaticWithInit< ::css::uno::Type *, theDataPointLabelType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.chart2.DataPointLabel" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "ShowNumber" );
        ::rtl::OUString the_name1( "ShowNumberInPercent" );
        ::rtl::OUString the_name2( "ShowCategoryName" );
        ::rtl::OUString the_name3( "ShowLegendSymbol" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::DataPointLabel const *) {
    return *detail::theDataPointLabelType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::DataPointLabel const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::DataPointLabel >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_DATAPOINTLABEL_HPP
