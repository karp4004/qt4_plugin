#ifndef INCLUDED_COM_SUN_STAR_CHART2_CURVESTYLE_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_CURVESTYLE_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/CurveStyle.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theCurveStyleType : public rtl::StaticWithInit< ::css::uno::Type *, theCurveStyleType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.chart2.CurveStyle" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[8];
        ::rtl::OUString sEnumValue0( "LINES" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "CUBIC_SPLINES" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "B_SPLINES" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "NURBS" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "STEP_START" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "STEP_END" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "STEP_CENTER_X" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "STEP_CENTER_Y" );
        enumValueNames[7] = sEnumValue7.pData;

        sal_Int32 enumValues[8];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::chart2::CurveStyle_LINES,
            8, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::CurveStyle const *) {
    return *detail::theCurveStyleType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::CurveStyle const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::CurveStyle >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_CURVESTYLE_HPP
