#ifndef INCLUDED_COM_SUN_STAR_CHART2_AXISORIENTATION_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_AXISORIENTATION_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/AxisOrientation.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct theAxisOrientationType : public rtl::StaticWithInit< ::css::uno::Type *, theAxisOrientationType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.chart2.AxisOrientation" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[2];
        ::rtl::OUString sEnumValue0( "MATHEMATICAL" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "REVERSE" );
        enumValueNames[1] = sEnumValue1.pData;

        sal_Int32 enumValues[2];
        enumValues[0] = 0;
        enumValues[1] = 1;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::chart2::AxisOrientation_MATHEMATICAL,
            2, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::AxisOrientation const *) {
    return *detail::theAxisOrientationType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::AxisOrientation const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::AxisOrientation >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_AXISORIENTATION_HPP
