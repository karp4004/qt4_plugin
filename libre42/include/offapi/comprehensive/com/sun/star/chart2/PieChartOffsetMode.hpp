#ifndef INCLUDED_COM_SUN_STAR_CHART2_PIECHARTOFFSETMODE_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_PIECHARTOFFSETMODE_HPP

#include "sal/config.h"

#include "com/sun/star/chart2/PieChartOffsetMode.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace chart2 { namespace detail {

struct thePieChartOffsetModeType : public rtl::StaticWithInit< ::css::uno::Type *, thePieChartOffsetModeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.chart2.PieChartOffsetMode" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[2];
        ::rtl::OUString sEnumValue0( "NONE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "ALL_EXPLODED" );
        enumValueNames[1] = sEnumValue1.pData;

        sal_Int32 enumValues[2];
        enumValues[0] = 0;
        enumValues[1] = 1;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::chart2::PieChartOffsetMode_NONE,
            2, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace chart2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::PieChartOffsetMode const *) {
    return *detail::thePieChartOffsetModeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::chart2::PieChartOffsetMode const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::chart2::PieChartOffsetMode >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_PIECHARTOFFSETMODE_HPP
