#ifndef INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATABASEDATAPROVIDER_HPP
#define INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATABASEDATAPROVIDER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/chart2/data/XDatabaseDataProvider.hdl"

#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/chart2/data/XDataProvider.hpp"
#include "com/sun/star/chart2/data/XRangeXMLConversion.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/lang/XInitialization.hpp"
#include "com/sun/star/sdbc/XConnection.hpp"
#include "com/sun/star/sdbc/XParameters.hpp"
#include "com/sun/star/sdbc/XRowSet.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace chart2 { namespace data { namespace detail {

struct theXDatabaseDataProviderType : public rtl::StaticWithInit< ::css::uno::Type *, theXDatabaseDataProviderType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.chart2.data.XDatabaseDataProvider" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[7];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::chart2::data::XDataProvider > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::chart2::data::XRangeXMLConversion > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::lang::XInitialization > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< ::css::uno::Reference< ::css::lang::XComponent > >::get().getTypeLibType();
        aSuperTypes[4] = ::cppu::UnoType< ::css::uno::Reference< ::css::beans::XPropertySet > >::get().getTypeLibType();
        aSuperTypes[5] = ::cppu::UnoType< ::css::uno::Reference< ::css::sdbc::XParameters > >::get().getTypeLibType();
        aSuperTypes[6] = ::cppu::UnoType< ::css::uno::Reference< ::css::sdbc::XRowSet > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[13] = { 0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.chart2.data.XDatabaseDataProvider::MasterFields" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.chart2.data.XDatabaseDataProvider::DetailFields" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.chart2.data.XDatabaseDataProvider::Command" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.chart2.data.XDatabaseDataProvider::CommandType" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.chart2.data.XDatabaseDataProvider::Filter" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.chart2.data.XDatabaseDataProvider::ApplyFilter" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.chart2.data.XDatabaseDataProvider::HavingClause" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.chart2.data.XDatabaseDataProvider::GroupBy" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.chart2.data.XDatabaseDataProvider::Order" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.chart2.data.XDatabaseDataProvider::EscapeProcessing" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.chart2.data.XDatabaseDataProvider::RowLimit" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.chart2.data.XDatabaseDataProvider::ActiveConnection" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.chart2.data.XDatabaseDataProvider::DataSourceName" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            7, aSuperTypes,
            13,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace chart2 { namespace data {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::chart2::data::XDatabaseDataProvider const *) {
    const ::css::uno::Type &rRet = *detail::theXDatabaseDataProviderType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
            ::cppu::UnoType< ::css::beans::UnknownPropertyException >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::sdbc::XConnection > >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "[]string" );
                ::rtl::OUString sAttributeName0( "com.sun.star.chart2.data.XDatabaseDataProvider::MasterFields" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    66, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "[]string" );
                ::rtl::OUString sAttributeName1( "com.sun.star.chart2.data.XDatabaseDataProvider::DetailFields" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    67, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "string" );
                ::rtl::OUString sAttributeName2( "com.sun.star.chart2.data.XDatabaseDataProvider::Command" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    68, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "long" );
                ::rtl::OUString sAttributeName3( "com.sun.star.chart2.data.XDatabaseDataProvider::CommandType" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    69, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "string" );
                ::rtl::OUString sAttributeName4( "com.sun.star.chart2.data.XDatabaseDataProvider::Filter" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    70, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "boolean" );
                ::rtl::OUString sAttributeName5( "com.sun.star.chart2.data.XDatabaseDataProvider::ApplyFilter" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    71, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType5.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "string" );
                ::rtl::OUString sAttributeName6( "com.sun.star.chart2.data.XDatabaseDataProvider::HavingClause" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    72, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType6.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "string" );
                ::rtl::OUString sAttributeName7( "com.sun.star.chart2.data.XDatabaseDataProvider::GroupBy" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    73, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType7.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "string" );
                ::rtl::OUString sAttributeName8( "com.sun.star.chart2.data.XDatabaseDataProvider::Order" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    74, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType8.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "boolean" );
                ::rtl::OUString sAttributeName9( "com.sun.star.chart2.data.XDatabaseDataProvider::EscapeProcessing" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    75, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType9.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "long" );
                ::rtl::OUString sAttributeName10( "com.sun.star.chart2.data.XDatabaseDataProvider::RowLimit" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    76, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType10.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "com.sun.star.sdbc.XConnection" );
                ::rtl::OUString sAttributeName11( "com.sun.star.chart2.data.XDatabaseDataProvider::ActiveConnection" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    77, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType11.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "string" );
                ::rtl::OUString sAttributeName12( "com.sun.star.chart2.data.XDatabaseDataProvider::DataSourceName" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    78, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType12.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::chart2::data::XDatabaseDataProvider > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::chart2::data::XDatabaseDataProvider > >::get();
}

::css::uno::Type const & ::css::chart2::data::XDatabaseDataProvider::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::chart2::data::XDatabaseDataProvider > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_CHART2_DATA_XDATABASEDATAPROVIDER_HPP
