#ifndef INCLUDED_COM_SUN_STAR_UTIL_DATETIME_HDL
#define INCLUDED_COM_SUN_STAR_UTIL_DATETIME_HDL

#include "sal/config.h"

#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace util {

#ifdef SAL_W32
#   pragma pack(push, 8)
#endif

struct DateTime {
    inline DateTime() SAL_THROW(());

    inline DateTime(const ::sal_uInt32& NanoSeconds_, const ::sal_uInt16& Seconds_, const ::sal_uInt16& Minutes_, const ::sal_uInt16& Hours_, const ::sal_uInt16& Day_, const ::sal_uInt16& Month_, const ::sal_Int16& Year_, const ::sal_Bool& IsUTC_) SAL_THROW(());

    ::sal_uInt32 NanoSeconds;
    ::sal_uInt16 Seconds;
    ::sal_uInt16 Minutes;
    ::sal_uInt16 Hours;
    ::sal_uInt16 Day;
    ::sal_uInt16 Month;
    ::sal_Int16 Year;
    ::sal_Bool IsUTC;
};

#ifdef SAL_W32
#   pragma pack(pop)
#endif


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::DateTime const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::util::DateTime *) SAL_THROW(());

#endif
