#ifndef INCLUDED_COM_SUN_STAR_UTIL_XLOCALIZEDALIASES_HDL
#define INCLUDED_COM_SUN_STAR_UTIL_XLOCALIZEDALIASES_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/ElementExistException.hdl"
#include "com/sun/star/container/NoSuchElementException.hdl"
#include "com/sun/star/lang/Locale.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/util/AliasProgrammaticPair.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace util {

class SAL_NO_VTABLE XLocalizedAliases : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL bindAlias( const ::rtl::OUString& programmaticName, const ::css::lang::Locale& locale, const ::rtl::OUString& alias ) /* throw (::css::container::ElementExistException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL unbindAlias( const ::css::lang::Locale& locale, const ::rtl::OUString& alias ) /* throw (::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL lookupAlias( const ::css::lang::Locale& locale, const ::rtl::OUString& Alias ) /* throw (::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL lookupProgrammatic( const ::css::lang::Locale& locale, const ::rtl::OUString& programmatic ) /* throw (::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL unbindAliases( const ::rtl::OUString& programmaticName ) /* throw (::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL rebindAliases( const ::rtl::OUString& currentProgrammatic, const ::rtl::OUString& newProgrammatic ) /* throw (::css::container::NoSuchElementException, ::css::container::ElementExistException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL renameAlias( const ::css::lang::Locale& locale, const ::rtl::OUString& oldName, const ::rtl::OUString& aNewName ) /* throw (::css::container::NoSuchElementException, ::css::container::ElementExistException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::util::AliasProgrammaticPair > SAL_CALL listAliases( const ::css::lang::Locale& locale ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XLocalizedAliases() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::XLocalizedAliases const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::util::XLocalizedAliases > *) SAL_THROW(());

#endif
