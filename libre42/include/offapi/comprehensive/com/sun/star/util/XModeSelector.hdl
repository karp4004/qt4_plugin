#ifndef INCLUDED_COM_SUN_STAR_UTIL_XMODESELECTOR_HDL
#define INCLUDED_COM_SUN_STAR_UTIL_XMODESELECTOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/NoSupportException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace util {

class SAL_NO_VTABLE XModeSelector : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setMode( const ::rtl::OUString& aMode ) /* throw (::css::lang::NoSupportException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getMode() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getSupportedModes() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL supportsMode( const ::rtl::OUString& aMode ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XModeSelector() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::XModeSelector const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::util::XModeSelector > *) SAL_THROW(());

#endif
