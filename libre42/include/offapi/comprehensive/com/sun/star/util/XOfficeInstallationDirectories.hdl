#ifndef INCLUDED_COM_SUN_STAR_UTIL_XOFFICEINSTALLATIONDIRECTORIES_HDL
#define INCLUDED_COM_SUN_STAR_UTIL_XOFFICEINSTALLATIONDIRECTORIES_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace util {

class SAL_NO_VTABLE XOfficeInstallationDirectories : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::rtl::OUString SAL_CALL getOfficeInstallationDirectoryURL() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getOfficeUserDataDirectoryURL() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL makeRelocatableURL( const ::rtl::OUString& URL ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL makeAbsoluteURL( const ::rtl::OUString& URL ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XOfficeInstallationDirectories() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::util::XOfficeInstallationDirectories const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::util::XOfficeInstallationDirectories > *) SAL_THROW(());

#endif
