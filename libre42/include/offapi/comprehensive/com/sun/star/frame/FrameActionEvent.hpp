#ifndef INCLUDED_COM_SUN_STAR_FRAME_FRAMEACTIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_FRAMEACTIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/frame/FrameActionEvent.hdl"

#include "com/sun/star/frame/FrameAction.hpp"
#include "com/sun/star/frame/XFrame.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame {

inline FrameActionEvent::FrameActionEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Frame()
    , Action(::css::frame::FrameAction_COMPONENT_ATTACHED)
{
}

inline FrameActionEvent::FrameActionEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::frame::XFrame >& Frame_, const ::css::frame::FrameAction& Action_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Frame(Frame_)
    , Action(Action_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace frame { namespace detail {

struct theFrameActionEventType : public rtl::StaticWithInit< ::css::uno::Type *, theFrameActionEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.FrameActionEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XFrame > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.frame.XFrame" );
        ::rtl::OUString the_name0( "Frame" );
        ::cppu::UnoType< ::css::frame::FrameAction >::get();
        ::rtl::OUString the_tname1( "com.sun.star.frame.FrameAction" );
        ::rtl::OUString the_name1( "Action" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::FrameActionEvent const *) {
    return *detail::theFrameActionEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::FrameActionEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::FrameActionEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_FRAMEACTIONEVENT_HPP
