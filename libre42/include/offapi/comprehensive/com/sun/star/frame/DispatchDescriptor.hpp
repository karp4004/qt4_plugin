#ifndef INCLUDED_COM_SUN_STAR_FRAME_DISPATCHDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_DISPATCHDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/frame/DispatchDescriptor.hdl"

#include "com/sun/star/util/URL.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame {

inline DispatchDescriptor::DispatchDescriptor() SAL_THROW(())
    : FeatureURL()
    , FrameName()
    , SearchFlags(0)
{
}

inline DispatchDescriptor::DispatchDescriptor(const ::css::util::URL& FeatureURL_, const ::rtl::OUString& FrameName_, const ::sal_Int32& SearchFlags_) SAL_THROW(())
    : FeatureURL(FeatureURL_)
    , FrameName(FrameName_)
    , SearchFlags(SearchFlags_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace frame { namespace detail {

struct theDispatchDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, theDispatchDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.DispatchDescriptor" );
        ::cppu::UnoType< ::css::util::URL >::get();
        ::rtl::OUString the_tname0( "com.sun.star.util.URL" );
        ::rtl::OUString the_name0( "FeatureURL" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "FrameName" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name2( "SearchFlags" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::DispatchDescriptor const *) {
    return *detail::theDispatchDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::DispatchDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::DispatchDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_DISPATCHDESCRIPTOR_HPP
