#ifndef INCLUDED_COM_SUN_STAR_FRAME_XTERMINATELISTENER_HDL
#define INCLUDED_COM_SUN_STAR_FRAME_XTERMINATELISTENER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/frame/TerminationVetoException.hdl"
#include "com/sun/star/lang/EventObject.hdl"
#include "com/sun/star/lang/XEventListener.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace frame {

class SAL_NO_VTABLE XTerminateListener : public ::css::lang::XEventListener
{
public:

    // Methods
    virtual void SAL_CALL queryTermination( const ::css::lang::EventObject& Event ) /* throw (::css::frame::TerminationVetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL notifyTermination( const ::css::lang::EventObject& Event ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTerminateListener() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XTerminateListener const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::frame::XTerminateListener > *) SAL_THROW(());

#endif
