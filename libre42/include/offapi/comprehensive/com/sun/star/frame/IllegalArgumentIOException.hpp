#ifndef INCLUDED_COM_SUN_STAR_FRAME_ILLEGALARGUMENTIOEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_ILLEGALARGUMENTIOEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/frame/IllegalArgumentIOException.hdl"

#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace frame {

inline IllegalArgumentIOException::IllegalArgumentIOException() SAL_THROW(())
    : ::css::io::IOException()
{
    ::cppu::UnoType< ::css::frame::IllegalArgumentIOException >::get();
}

inline IllegalArgumentIOException::IllegalArgumentIOException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::io::IOException(Message_, Context_)
{
    ::cppu::UnoType< ::css::frame::IllegalArgumentIOException >::get();
}

IllegalArgumentIOException::IllegalArgumentIOException(IllegalArgumentIOException const & the_other): ::css::io::IOException(the_other) {}

IllegalArgumentIOException::~IllegalArgumentIOException() {}

IllegalArgumentIOException & IllegalArgumentIOException::operator =(IllegalArgumentIOException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::io::IOException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace frame { namespace detail {

struct theIllegalArgumentIOExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theIllegalArgumentIOExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.frame.IllegalArgumentIOException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::io::IOException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::IllegalArgumentIOException const *) {
    return *detail::theIllegalArgumentIOExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::IllegalArgumentIOException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::IllegalArgumentIOException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_ILLEGALARGUMENTIOEXCEPTION_HPP
