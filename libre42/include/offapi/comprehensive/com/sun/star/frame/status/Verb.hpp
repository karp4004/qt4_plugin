#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_VERB_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_VERB_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/Verb.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline Verb::Verb() SAL_THROW(())
    : VerbId(0)
    , VerbName()
    , VerbIsOnMenu(false)
    , VerbIsConst(false)
{
}

inline Verb::Verb(const ::sal_Int32& VerbId_, const ::rtl::OUString& VerbName_, const ::sal_Bool& VerbIsOnMenu_, const ::sal_Bool& VerbIsConst_) SAL_THROW(())
    : VerbId(VerbId_)
    , VerbName(VerbName_)
    , VerbIsOnMenu(VerbIsOnMenu_)
    , VerbIsConst(VerbIsConst_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status { namespace detail {

struct theVerbType : public rtl::StaticWithInit< ::css::uno::Type *, theVerbType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.status.Verb" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "VerbId" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "VerbName" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name2( "VerbIsOnMenu" );
        ::rtl::OUString the_name3( "VerbIsConst" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::Verb const *) {
    return *detail::theVerbType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::Verb const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::Verb >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_VERB_HPP
