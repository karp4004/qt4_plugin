#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_UPPERLOWERMARGINSCALE_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_UPPERLOWERMARGINSCALE_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/UpperLowerMarginScale.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline UpperLowerMarginScale::UpperLowerMarginScale() SAL_THROW(())
    : Upper(0)
    , Lower(0)
    , ScaleUpper(0)
    , ScaleLower(0)
{
}

inline UpperLowerMarginScale::UpperLowerMarginScale(const ::sal_Int32& Upper_, const ::sal_Int32& Lower_, const ::sal_Int16& ScaleUpper_, const ::sal_Int16& ScaleLower_) SAL_THROW(())
    : Upper(Upper_)
    , Lower(Lower_)
    , ScaleUpper(ScaleUpper_)
    , ScaleLower(ScaleLower_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status { namespace detail {

struct theUpperLowerMarginScaleType : public rtl::StaticWithInit< ::css::uno::Type *, theUpperLowerMarginScaleType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.status.UpperLowerMarginScale" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Upper" );
        ::rtl::OUString the_name1( "Lower" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name2( "ScaleUpper" );
        ::rtl::OUString the_name3( "ScaleLower" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::UpperLowerMarginScale const *) {
    return *detail::theUpperLowerMarginScaleType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::UpperLowerMarginScale const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::UpperLowerMarginScale >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_UPPERLOWERMARGINSCALE_HPP
