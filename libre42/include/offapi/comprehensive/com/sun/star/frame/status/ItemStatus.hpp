#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_ITEMSTATUS_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_ITEMSTATUS_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/ItemStatus.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ItemStatus::ItemStatus() SAL_THROW(())
    : State(0)
    , aStateData()
{
}

inline ItemStatus::ItemStatus(const ::sal_Int16& State_, const ::css::uno::Any& aStateData_) SAL_THROW(())
    : State(State_)
    , aStateData(aStateData_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status { namespace detail {

struct theItemStatusType : public rtl::StaticWithInit< ::css::uno::Type *, theItemStatusType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.status.ItemStatus" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "State" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "aStateData" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::ItemStatus const *) {
    return *detail::theItemStatusType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::ItemStatus const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::ItemStatus >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_ITEMSTATUS_HPP
