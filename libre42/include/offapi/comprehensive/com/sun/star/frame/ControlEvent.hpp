#ifndef INCLUDED_COM_SUN_STAR_FRAME_CONTROLEVENT_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_CONTROLEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/frame/ControlEvent.hdl"

#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/util/URL.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame {

inline ControlEvent::ControlEvent() SAL_THROW(())
    : aURL()
    , Event()
    , aInformation()
{
}

inline ControlEvent::ControlEvent(const ::css::util::URL& aURL_, const ::rtl::OUString& Event_, const ::css::uno::Sequence< ::css::beans::NamedValue >& aInformation_) SAL_THROW(())
    : aURL(aURL_)
    , Event(Event_)
    , aInformation(aInformation_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace frame { namespace detail {

struct theControlEventType : public rtl::StaticWithInit< ::css::uno::Type *, theControlEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.ControlEvent" );
        ::cppu::UnoType< ::css::util::URL >::get();
        ::rtl::OUString the_tname0( "com.sun.star.util.URL" );
        ::rtl::OUString the_name0( "aURL" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "Event" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::NamedValue > >::get();
        ::rtl::OUString the_tname2( "[]com.sun.star.beans.NamedValue" );
        ::rtl::OUString the_name2( "aInformation" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::ControlEvent const *) {
    return *detail::theControlEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::ControlEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::ControlEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_CONTROLEVENT_HPP
