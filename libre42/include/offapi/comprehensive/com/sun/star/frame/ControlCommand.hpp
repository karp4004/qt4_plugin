#ifndef INCLUDED_COM_SUN_STAR_FRAME_CONTROLCOMMAND_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_CONTROLCOMMAND_HPP

#include "sal/config.h"

#include "com/sun/star/frame/ControlCommand.hdl"

#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame {

inline ControlCommand::ControlCommand() SAL_THROW(())
    : Command()
    , Arguments()
{
}

inline ControlCommand::ControlCommand(const ::rtl::OUString& Command_, const ::css::uno::Sequence< ::css::beans::NamedValue >& Arguments_) SAL_THROW(())
    : Command(Command_)
    , Arguments(Arguments_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace frame { namespace detail {

struct theControlCommandType : public rtl::StaticWithInit< ::css::uno::Type *, theControlCommandType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.ControlCommand" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Command" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::NamedValue > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.beans.NamedValue" );
        ::rtl::OUString the_name1( "Arguments" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::ControlCommand const *) {
    return *detail::theControlCommandType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::ControlCommand const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::ControlCommand >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_CONTROLCOMMAND_HPP
