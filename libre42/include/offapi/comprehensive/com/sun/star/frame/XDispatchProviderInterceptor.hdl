#ifndef INCLUDED_COM_SUN_STAR_FRAME_XDISPATCHPROVIDERINTERCEPTOR_HDL
#define INCLUDED_COM_SUN_STAR_FRAME_XDISPATCHPROVIDERINTERCEPTOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/frame/XDispatchProvider.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace frame {

class SAL_NO_VTABLE XDispatchProviderInterceptor : public ::css::frame::XDispatchProvider
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::frame::XDispatchProvider > SAL_CALL getSlaveDispatchProvider() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setSlaveDispatchProvider( const ::css::uno::Reference< ::css::frame::XDispatchProvider >& NewDispatchProvider ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::frame::XDispatchProvider > SAL_CALL getMasterDispatchProvider() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMasterDispatchProvider( const ::css::uno::Reference< ::css::frame::XDispatchProvider >& NewSupplier ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDispatchProviderInterceptor() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XDispatchProviderInterceptor const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::frame::XDispatchProviderInterceptor > *) SAL_THROW(());

#endif
