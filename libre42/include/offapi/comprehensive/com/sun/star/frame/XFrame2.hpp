#ifndef INCLUDED_COM_SUN_STAR_FRAME_XFRAME2_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_XFRAME2_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/frame/XFrame2.hdl"

#include "com/sun/star/container/XNameContainer.hpp"
#include "com/sun/star/frame/XDispatchInformationProvider.hpp"
#include "com/sun/star/frame/XDispatchProvider.hpp"
#include "com/sun/star/frame/XDispatchProviderInterception.hpp"
#include "com/sun/star/frame/XDispatchRecorderSupplier.hpp"
#include "com/sun/star/frame/XFramesSupplier.hpp"
#include "com/sun/star/task/XStatusIndicatorFactory.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace frame { namespace detail {

struct theXFrame2Type : public rtl::StaticWithInit< ::css::uno::Type *, theXFrame2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.frame.XFrame2" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[5];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XDispatchProvider > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XDispatchInformationProvider > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XDispatchProviderInterception > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XFramesSupplier > >::get().getTypeLibType();
        aSuperTypes[4] = ::cppu::UnoType< ::css::uno::Reference< ::css::task::XStatusIndicatorFactory > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[4] = { 0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.frame.XFrame2::UserDefinedAttributes" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.frame.XFrame2::Title" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.frame.XFrame2::DispatchRecorderSupplier" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.frame.XFrame2::LayoutManager" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            5, aSuperTypes,
            4,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XFrame2 const *) {
    const ::css::uno::Type &rRet = *detail::theXFrame2Type::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::container::XNameContainer > >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XDispatchRecorderSupplier > >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "com.sun.star.container.XNameContainer" );
                ::rtl::OUString sAttributeName0( "com.sun.star.frame.XFrame2::UserDefinedAttributes" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    33, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType0.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "string" );
                ::rtl::OUString sAttributeName1( "com.sun.star.frame.XFrame2::Title" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    34, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "com.sun.star.frame.XDispatchRecorderSupplier" );
                ::rtl::OUString sAttributeName2( "com.sun.star.frame.XFrame2::DispatchRecorderSupplier" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    35, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "com.sun.star.uno.XInterface" );
                ::rtl::OUString sAttributeName3( "com.sun.star.frame.XFrame2::LayoutManager" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    36, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::frame::XFrame2 > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XFrame2 > >::get();
}

::css::uno::Type const & ::css::frame::XFrame2::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::frame::XFrame2 > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_XFRAME2_HPP
