#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_VISIBILITY_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_VISIBILITY_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/Visibility.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline Visibility::Visibility() SAL_THROW(())
    : bVisible(false)
{
}

inline Visibility::Visibility(const ::sal_Bool& bVisible_) SAL_THROW(())
    : bVisible(bVisible_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status { namespace detail {

struct theVisibilityType : public rtl::StaticWithInit< ::css::uno::Type *, theVisibilityType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.status.Visibility" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "bVisible" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::Visibility const *) {
    return *detail::theVisibilityType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::Visibility const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::Visibility >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_VISIBILITY_HPP
