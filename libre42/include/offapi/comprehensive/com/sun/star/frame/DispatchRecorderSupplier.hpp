#ifndef INCLUDED_COM_SUN_STAR_FRAME_DISPATCHRECORDERSUPPLIER_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_DISPATCHRECORDERSUPPLIER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/frame/XDispatchRecorderSupplier.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace frame {

class DispatchRecorderSupplier {
public:
    static ::css::uno::Reference< ::css::frame::XDispatchRecorderSupplier > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::frame::XDispatchRecorderSupplier > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::frame::XDispatchRecorderSupplier >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.frame.DispatchRecorderSupplier" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.frame.DispatchRecorderSupplier of type com.sun.star.frame.XDispatchRecorderSupplier: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.frame.DispatchRecorderSupplier of type com.sun.star.frame.XDispatchRecorderSupplier" ), the_context);
        }
        return the_instance;
    }

private:
    DispatchRecorderSupplier(); // not implemented
    DispatchRecorderSupplier(DispatchRecorderSupplier &); // not implemented
    ~DispatchRecorderSupplier(); // not implemented
    void operator =(DispatchRecorderSupplier); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_FRAME_DISPATCHRECORDERSUPPLIER_HPP
