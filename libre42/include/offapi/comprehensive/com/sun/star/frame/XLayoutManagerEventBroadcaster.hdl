#ifndef INCLUDED_COM_SUN_STAR_FRAME_XLAYOUTMANAGEREVENTBROADCASTER_HDL
#define INCLUDED_COM_SUN_STAR_FRAME_XLAYOUTMANAGEREVENTBROADCASTER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace frame { class XLayoutManagerListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace frame {

class SAL_NO_VTABLE XLayoutManagerEventBroadcaster : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addLayoutManagerEventListener( const ::css::uno::Reference< ::css::frame::XLayoutManagerListener >& aLayoutManagerListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeLayoutManagerEventListener( const ::css::uno::Reference< ::css::frame::XLayoutManagerListener >& aLayoutManagerListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XLayoutManagerEventBroadcaster() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::XLayoutManagerEventBroadcaster const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::frame::XLayoutManagerEventBroadcaster > *) SAL_THROW(());

#endif
