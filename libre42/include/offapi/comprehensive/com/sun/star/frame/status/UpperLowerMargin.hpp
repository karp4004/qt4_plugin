#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_UPPERLOWERMARGIN_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_UPPERLOWERMARGIN_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/UpperLowerMargin.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline UpperLowerMargin::UpperLowerMargin() SAL_THROW(())
    : Upper(0)
    , Lower(0)
{
}

inline UpperLowerMargin::UpperLowerMargin(const ::sal_Int32& Upper_, const ::sal_Int32& Lower_) SAL_THROW(())
    : Upper(Upper_)
    , Lower(Lower_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status { namespace detail {

struct theUpperLowerMarginType : public rtl::StaticWithInit< ::css::uno::Type *, theUpperLowerMarginType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.status.UpperLowerMargin" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Upper" );
        ::rtl::OUString the_name1( "Lower" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::UpperLowerMargin const *) {
    return *detail::theUpperLowerMarginType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::UpperLowerMargin const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::UpperLowerMargin >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_UPPERLOWERMARGIN_HPP
