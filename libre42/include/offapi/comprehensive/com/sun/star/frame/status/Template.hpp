#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_TEMPLATE_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_TEMPLATE_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/Template.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline Template::Template() SAL_THROW(())
    : StyleName()
    , Value(0)
{
}

inline Template::Template(const ::rtl::OUString& StyleName_, const ::sal_Int32& Value_) SAL_THROW(())
    : StyleName(StyleName_)
    , Value(Value_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status { namespace detail {

struct theTemplateType : public rtl::StaticWithInit< ::css::uno::Type *, theTemplateType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.status.Template" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "StyleName" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "Value" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::Template const *) {
    return *detail::theTemplateType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::Template const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::Template >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_TEMPLATE_HPP
