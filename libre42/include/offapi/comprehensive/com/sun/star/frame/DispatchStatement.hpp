#ifndef INCLUDED_COM_SUN_STAR_FRAME_DISPATCHSTATEMENT_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_DISPATCHSTATEMENT_HPP

#include "sal/config.h"

#include "com/sun/star/frame/DispatchStatement.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame {

inline DispatchStatement::DispatchStatement() SAL_THROW(())
    : aCommand()
    , aTarget()
    , aArgs()
    , nFlags(0)
    , bIsComment(false)
{
}

inline DispatchStatement::DispatchStatement(const ::rtl::OUString& aCommand_, const ::rtl::OUString& aTarget_, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aArgs_, const ::sal_Int32& nFlags_, const ::sal_Bool& bIsComment_) SAL_THROW(())
    : aCommand(aCommand_)
    , aTarget(aTarget_)
    , aArgs(aArgs_)
    , nFlags(nFlags_)
    , bIsComment(bIsComment_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace frame { namespace detail {

struct theDispatchStatementType : public rtl::StaticWithInit< ::css::uno::Type *, theDispatchStatementType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.DispatchStatement" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "aCommand" );
        ::rtl::OUString the_name1( "aTarget" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::PropertyValue > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.beans.PropertyValue" );
        ::rtl::OUString the_name2( "aArgs" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name3( "nFlags" );
        ::rtl::OUString the_tname3( "boolean" );
        ::rtl::OUString the_name4( "bIsComment" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname3.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::DispatchStatement const *) {
    return *detail::theDispatchStatementType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::DispatchStatement const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::DispatchStatement >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_DISPATCHSTATEMENT_HPP
