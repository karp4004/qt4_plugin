#ifndef INCLUDED_COM_SUN_STAR_FRAME_FEATURESTATEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_FEATURESTATEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/frame/FeatureStateEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/util/URL.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame {

inline FeatureStateEvent::FeatureStateEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , FeatureURL()
    , FeatureDescriptor()
    , IsEnabled(false)
    , Requery(false)
    , State()
{
}

inline FeatureStateEvent::FeatureStateEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::util::URL& FeatureURL_, const ::rtl::OUString& FeatureDescriptor_, const ::sal_Bool& IsEnabled_, const ::sal_Bool& Requery_, const ::css::uno::Any& State_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , FeatureURL(FeatureURL_)
    , FeatureDescriptor(FeatureDescriptor_)
    , IsEnabled(IsEnabled_)
    , Requery(Requery_)
    , State(State_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace frame { namespace detail {

struct theFeatureStateEventType : public rtl::StaticWithInit< ::css::uno::Type *, theFeatureStateEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.FeatureStateEvent" );
        ::cppu::UnoType< ::css::util::URL >::get();
        ::rtl::OUString the_tname0( "com.sun.star.util.URL" );
        ::rtl::OUString the_name0( "FeatureURL" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "FeatureDescriptor" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name2( "IsEnabled" );
        ::rtl::OUString the_name3( "Requery" );
        ::rtl::OUString the_tname3( "any" );
        ::rtl::OUString the_name4( "State" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_ANY, the_tname3.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace frame {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::FeatureStateEvent const *) {
    return *detail::theFeatureStateEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::FeatureStateEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::FeatureStateEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_FEATURESTATEEVENT_HPP
