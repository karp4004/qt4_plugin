#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_FONTHEIGHT_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_FONTHEIGHT_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/FontHeight.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline FontHeight::FontHeight() SAL_THROW(())
    : Height(0)
    , Prop(0)
    , Diff(0)
{
}

inline FontHeight::FontHeight(const float& Height_, const ::sal_Int16& Prop_, const float& Diff_) SAL_THROW(())
    : Height(Height_)
    , Prop(Prop_)
    , Diff(Diff_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status { namespace detail {

struct theFontHeightType : public rtl::StaticWithInit< ::css::uno::Type *, theFontHeightType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.status.FontHeight" );
        ::rtl::OUString the_tname0( "float" );
        ::rtl::OUString the_name0( "Height" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "Prop" );
        ::rtl::OUString the_name2( "Diff" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_FLOAT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_FLOAT, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::FontHeight const *) {
    return *detail::theFontHeightType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::FontHeight const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::FontHeight >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_FONTHEIGHT_HPP
