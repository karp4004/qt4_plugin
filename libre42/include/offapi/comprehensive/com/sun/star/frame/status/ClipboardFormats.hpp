#ifndef INCLUDED_COM_SUN_STAR_FRAME_STATUS_CLIPBOARDFORMATS_HPP
#define INCLUDED_COM_SUN_STAR_FRAME_STATUS_CLIPBOARDFORMATS_HPP

#include "sal/config.h"

#include "com/sun/star/frame/status/ClipboardFormats.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ClipboardFormats::ClipboardFormats() SAL_THROW(())
    : Identifiers()
    , Names()
{
}

inline ClipboardFormats::ClipboardFormats(const ::css::uno::Sequence< ::sal_Int64 >& Identifiers_, const ::css::uno::Sequence< ::rtl::OUString >& Names_) SAL_THROW(())
    : Identifiers(Identifiers_)
    , Names(Names_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status { namespace detail {

struct theClipboardFormatsType : public rtl::StaticWithInit< ::css::uno::Type *, theClipboardFormatsType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.frame.status.ClipboardFormats" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::sal_Int64 > >::get();
        ::rtl::OUString the_tname0( "[]hyper" );
        ::rtl::OUString the_name0( "Identifiers" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
        ::rtl::OUString the_tname1( "[]string" );
        ::rtl::OUString the_name1( "Names" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace frame { namespace status {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::frame::status::ClipboardFormats const *) {
    return *detail::theClipboardFormatsType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::frame::status::ClipboardFormats const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::frame::status::ClipboardFormats >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FRAME_STATUS_CLIPBOARDFORMATS_HPP
