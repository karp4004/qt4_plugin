#ifndef INCLUDED_COM_SUN_STAR_MEDIA_ZOOMLEVEL_HPP
#define INCLUDED_COM_SUN_STAR_MEDIA_ZOOMLEVEL_HPP

#include "sal/config.h"

#include "com/sun/star/media/ZoomLevel.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace media { namespace detail {

struct theZoomLevelType : public rtl::StaticWithInit< ::css::uno::Type *, theZoomLevelType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.media.ZoomLevel" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[9];
        ::rtl::OUString sEnumValue0( "NOT_AVAILABLE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "ORIGINAL" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "FIT_TO_WINDOW" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "FIT_TO_WINDOW_FIXED_ASPECT" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "FULLSCREEN" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "ZOOM_1_TO_4" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "ZOOM_1_TO_2" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "ZOOM_2_TO_1" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "ZOOM_4_TO_1" );
        enumValueNames[8] = sEnumValue8.pData;

        sal_Int32 enumValues[9];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::media::ZoomLevel_NOT_AVAILABLE,
            9, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace media {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::media::ZoomLevel const *) {
    return *detail::theZoomLevelType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::media::ZoomLevel const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::media::ZoomLevel >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MEDIA_ZOOMLEVEL_HPP
