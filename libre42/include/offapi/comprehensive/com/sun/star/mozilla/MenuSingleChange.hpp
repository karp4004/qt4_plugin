#ifndef INCLUDED_COM_SUN_STAR_MOZILLA_MENUSINGLECHANGE_HPP
#define INCLUDED_COM_SUN_STAR_MOZILLA_MENUSINGLECHANGE_HPP

#include "sal/config.h"

#include "com/sun/star/mozilla/MenuSingleChange.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace mozilla {

inline MenuSingleChange::MenuSingleChange() SAL_THROW(())
    : ID(0)
    , ChangeID(0)
    , Change()
{
}

inline MenuSingleChange::MenuSingleChange(const ::sal_Int16& ID_, const ::sal_Int16& ChangeID_, const ::css::uno::Any& Change_) SAL_THROW(())
    : ID(ID_)
    , ChangeID(ChangeID_)
    , Change(Change_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace mozilla { namespace detail {

struct theMenuSingleChangeType : public rtl::StaticWithInit< ::css::uno::Type *, theMenuSingleChangeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.mozilla.MenuSingleChange" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "ID" );
        ::rtl::OUString the_name1( "ChangeID" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name2( "Change" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace mozilla {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mozilla::MenuSingleChange const *) {
    return *detail::theMenuSingleChangeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mozilla::MenuSingleChange const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mozilla::MenuSingleChange >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MOZILLA_MENUSINGLECHANGE_HPP
