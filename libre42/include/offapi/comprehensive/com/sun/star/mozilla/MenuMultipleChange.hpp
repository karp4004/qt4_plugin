#ifndef INCLUDED_COM_SUN_STAR_MOZILLA_MENUMULTIPLECHANGE_HPP
#define INCLUDED_COM_SUN_STAR_MOZILLA_MENUMULTIPLECHANGE_HPP

#include "sal/config.h"

#include "com/sun/star/mozilla/MenuMultipleChange.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace mozilla {

inline MenuMultipleChange::MenuMultipleChange() SAL_THROW(())
    : ID(0)
    , GroupID(0)
    , PreItemID(0)
    , ItemText()
    , IsVisible(false)
    , IsActive(false)
    , IsCheckable(false)
    , IsChecked(false)
    , Image()
{
}

inline MenuMultipleChange::MenuMultipleChange(const ::sal_Int16& ID_, const ::sal_Int16& GroupID_, const ::sal_Int16& PreItemID_, const ::rtl::OUString& ItemText_, const ::sal_Bool& IsVisible_, const ::sal_Bool& IsActive_, const ::sal_Bool& IsCheckable_, const ::sal_Bool& IsChecked_, const ::css::uno::Sequence< ::sal_Int8 >& Image_) SAL_THROW(())
    : ID(ID_)
    , GroupID(GroupID_)
    , PreItemID(PreItemID_)
    , ItemText(ItemText_)
    , IsVisible(IsVisible_)
    , IsActive(IsActive_)
    , IsCheckable(IsCheckable_)
    , IsChecked(IsChecked_)
    , Image(Image_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace mozilla { namespace detail {

struct theMenuMultipleChangeType : public rtl::StaticWithInit< ::css::uno::Type *, theMenuMultipleChangeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.mozilla.MenuMultipleChange" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "ID" );
        ::rtl::OUString the_name1( "GroupID" );
        ::rtl::OUString the_name2( "PreItemID" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name3( "ItemText" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name4( "IsVisible" );
        ::rtl::OUString the_name5( "IsActive" );
        ::rtl::OUString the_name6( "IsCheckable" );
        ::rtl::OUString the_name7( "IsChecked" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::sal_Int8 > >::get();
        ::rtl::OUString the_tname3( "[]byte" );
        ::rtl::OUString the_name8( "Image" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name7.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname3.pData, the_name8.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 9, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace mozilla {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mozilla::MenuMultipleChange const *) {
    return *detail::theMenuMultipleChangeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mozilla::MenuMultipleChange const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mozilla::MenuMultipleChange >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MOZILLA_MENUMULTIPLECHANGE_HPP
