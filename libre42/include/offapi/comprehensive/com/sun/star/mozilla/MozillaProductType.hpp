#ifndef INCLUDED_COM_SUN_STAR_MOZILLA_MOZILLAPRODUCTTYPE_HPP
#define INCLUDED_COM_SUN_STAR_MOZILLA_MOZILLAPRODUCTTYPE_HPP

#include "sal/config.h"

#include "com/sun/star/mozilla/MozillaProductType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace mozilla { namespace detail {

struct theMozillaProductTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theMozillaProductTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.mozilla.MozillaProductType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[4];
        ::rtl::OUString sEnumValue0( "Default" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "Mozilla" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "Firefox" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "Thunderbird" );
        enumValueNames[3] = sEnumValue3.pData;

        sal_Int32 enumValues[4];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::mozilla::MozillaProductType_Default,
            4, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace mozilla {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mozilla::MozillaProductType const *) {
    return *detail::theMozillaProductTypeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mozilla::MozillaProductType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mozilla::MozillaProductType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MOZILLA_MOZILLAPRODUCTTYPE_HPP
