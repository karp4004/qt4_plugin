#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_CORRUPTEDUICONFIGURATIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_CORRUPTEDUICONFIGURATIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/CorruptedUIConfigurationException.hdl"

#include "com/sun/star/configuration/CorruptedConfigurationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

inline CorruptedUIConfigurationException::CorruptedUIConfigurationException() SAL_THROW(())
    : ::css::configuration::CorruptedConfigurationException()
{
    ::cppu::UnoType< ::css::configuration::CorruptedUIConfigurationException >::get();
}

inline CorruptedUIConfigurationException::CorruptedUIConfigurationException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& Details_) SAL_THROW(())
    : ::css::configuration::CorruptedConfigurationException(Message_, Context_, Details_)
{
    ::cppu::UnoType< ::css::configuration::CorruptedUIConfigurationException >::get();
}

CorruptedUIConfigurationException::CorruptedUIConfigurationException(CorruptedUIConfigurationException const & the_other): ::css::configuration::CorruptedConfigurationException(the_other) {}

CorruptedUIConfigurationException::~CorruptedUIConfigurationException() {}

CorruptedUIConfigurationException & CorruptedUIConfigurationException::operator =(CorruptedUIConfigurationException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::CorruptedConfigurationException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace detail {

struct theCorruptedUIConfigurationExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theCorruptedUIConfigurationExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.CorruptedUIConfigurationException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::configuration::CorruptedConfigurationException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace configuration {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::CorruptedUIConfigurationException const *) {
    return *detail::theCorruptedUIConfigurationExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::CorruptedUIConfigurationException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::CorruptedUIConfigurationException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_CORRUPTEDUICONFIGURATIONEXCEPTION_HPP
