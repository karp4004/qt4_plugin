#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_INSUFFICIENTACCESSRIGHTSEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_INSUFFICIENTACCESSRIGHTSEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/InsufficientAccessRightsException.hdl"

#include "com/sun/star/configuration/backend/BackendAccessException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline InsufficientAccessRightsException::InsufficientAccessRightsException() SAL_THROW(())
    : ::css::configuration::backend::BackendAccessException()
{
    ::cppu::UnoType< ::css::configuration::backend::InsufficientAccessRightsException >::get();
}

inline InsufficientAccessRightsException::InsufficientAccessRightsException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& TargetException_) SAL_THROW(())
    : ::css::configuration::backend::BackendAccessException(Message_, Context_, TargetException_)
{
    ::cppu::UnoType< ::css::configuration::backend::InsufficientAccessRightsException >::get();
}

InsufficientAccessRightsException::InsufficientAccessRightsException(InsufficientAccessRightsException const & the_other): ::css::configuration::backend::BackendAccessException(the_other) {}

InsufficientAccessRightsException::~InsufficientAccessRightsException() {}

InsufficientAccessRightsException & InsufficientAccessRightsException::operator =(InsufficientAccessRightsException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::backend::BackendAccessException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend { namespace detail {

struct theInsufficientAccessRightsExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInsufficientAccessRightsExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.backend.InsufficientAccessRightsException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::configuration::backend::BackendAccessException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::InsufficientAccessRightsException const *) {
    return *detail::theInsufficientAccessRightsExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::InsufficientAccessRightsException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::InsufficientAccessRightsException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_INSUFFICIENTACCESSRIGHTSEXCEPTION_HPP
