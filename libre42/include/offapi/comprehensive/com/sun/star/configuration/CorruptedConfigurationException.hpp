#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_CORRUPTEDCONFIGURATIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_CORRUPTEDCONFIGURATIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/CorruptedConfigurationException.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

inline CorruptedConfigurationException::CorruptedConfigurationException() SAL_THROW(())
    : ::css::uno::RuntimeException()
    , Details()
{
    ::cppu::UnoType< ::css::configuration::CorruptedConfigurationException >::get();
}

inline CorruptedConfigurationException::CorruptedConfigurationException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& Details_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
    , Details(Details_)
{
    ::cppu::UnoType< ::css::configuration::CorruptedConfigurationException >::get();
}

CorruptedConfigurationException::CorruptedConfigurationException(CorruptedConfigurationException const & the_other): ::css::uno::RuntimeException(the_other), Details(the_other.Details) {}

CorruptedConfigurationException::~CorruptedConfigurationException() {}

CorruptedConfigurationException & CorruptedConfigurationException::operator =(CorruptedConfigurationException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    Details = the_other.Details;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace detail {

struct theCorruptedConfigurationExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theCorruptedConfigurationExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.CorruptedConfigurationException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::RuntimeException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "Details" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace configuration {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::CorruptedConfigurationException const *) {
    return *detail::theCorruptedConfigurationExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::CorruptedConfigurationException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::CorruptedConfigurationException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_CORRUPTEDCONFIGURATIONEXCEPTION_HPP
