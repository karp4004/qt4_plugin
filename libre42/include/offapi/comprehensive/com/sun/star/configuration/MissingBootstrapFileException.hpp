#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_MISSINGBOOTSTRAPFILEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_MISSINGBOOTSTRAPFILEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/MissingBootstrapFileException.hdl"

#include "com/sun/star/configuration/CannotLoadConfigurationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

inline MissingBootstrapFileException::MissingBootstrapFileException() SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException()
    , BootstrapFileURL()
{
    ::cppu::UnoType< ::css::configuration::MissingBootstrapFileException >::get();
}

inline MissingBootstrapFileException::MissingBootstrapFileException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& BootstrapFileURL_) SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException(Message_, Context_)
    , BootstrapFileURL(BootstrapFileURL_)
{
    ::cppu::UnoType< ::css::configuration::MissingBootstrapFileException >::get();
}

MissingBootstrapFileException::MissingBootstrapFileException(MissingBootstrapFileException const & the_other): ::css::configuration::CannotLoadConfigurationException(the_other), BootstrapFileURL(the_other.BootstrapFileURL) {}

MissingBootstrapFileException::~MissingBootstrapFileException() {}

MissingBootstrapFileException & MissingBootstrapFileException::operator =(MissingBootstrapFileException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::CannotLoadConfigurationException::operator =(the_other);
    BootstrapFileURL = the_other.BootstrapFileURL;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace detail {

struct theMissingBootstrapFileExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theMissingBootstrapFileExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.MissingBootstrapFileException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::configuration::CannotLoadConfigurationException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "BootstrapFileURL" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace configuration {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::MissingBootstrapFileException const *) {
    return *detail::theMissingBootstrapFileExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::MissingBootstrapFileException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::MissingBootstrapFileException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_MISSINGBOOTSTRAPFILEEXCEPTION_HPP
