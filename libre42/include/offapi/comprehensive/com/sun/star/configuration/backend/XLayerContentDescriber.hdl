#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_XLAYERCONTENTDESCRIBER_HDL
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_XLAYERCONTENTDESCRIBER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/configuration/backend/MalformedDataException.hdl"
#include "com/sun/star/configuration/backend/PropertyInfo.hdl"
namespace com { namespace sun { namespace star { namespace configuration { namespace backend { class XLayerHandler; } } } } }
#include "com/sun/star/lang/NullPointerException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

class SAL_NO_VTABLE XLayerContentDescriber : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL describeLayer( const ::css::uno::Reference< ::css::configuration::backend::XLayerHandler >& aHandler, const ::css::uno::Sequence< ::css::configuration::backend::PropertyInfo >& aPropertyInfos ) /* throw (::css::lang::NullPointerException, ::css::lang::WrappedTargetException, ::css::configuration::backend::MalformedDataException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XLayerContentDescriber() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::XLayerContentDescriber const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::configuration::backend::XLayerContentDescriber > *) SAL_THROW(());

#endif
