#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_STRATUMCREATIONEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_STRATUMCREATIONEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/StratumCreationException.hdl"

#include "com/sun/star/configuration/backend/BackendSetupException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline StratumCreationException::StratumCreationException() SAL_THROW(())
    : ::css::configuration::backend::BackendSetupException()
    , StratumService()
    , StratumData()
{
    ::cppu::UnoType< ::css::configuration::backend::StratumCreationException >::get();
}

inline StratumCreationException::StratumCreationException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& BackendException_, const ::rtl::OUString& StratumService_, const ::rtl::OUString& StratumData_) SAL_THROW(())
    : ::css::configuration::backend::BackendSetupException(Message_, Context_, BackendException_)
    , StratumService(StratumService_)
    , StratumData(StratumData_)
{
    ::cppu::UnoType< ::css::configuration::backend::StratumCreationException >::get();
}

StratumCreationException::StratumCreationException(StratumCreationException const & the_other): ::css::configuration::backend::BackendSetupException(the_other), StratumService(the_other.StratumService), StratumData(the_other.StratumData) {}

StratumCreationException::~StratumCreationException() {}

StratumCreationException & StratumCreationException::operator =(StratumCreationException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::backend::BackendSetupException::operator =(the_other);
    StratumService = the_other.StratumService;
    StratumData = the_other.StratumData;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend { namespace detail {

struct theStratumCreationExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theStratumCreationExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.backend.StratumCreationException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::configuration::backend::BackendSetupException >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "StratumService" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "StratumData" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::StratumCreationException const *) {
    return *detail::theStratumCreationExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::StratumCreationException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::StratumCreationException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_STRATUMCREATIONEXCEPTION_HPP
