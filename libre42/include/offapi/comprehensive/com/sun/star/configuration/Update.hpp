#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_UPDATE_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_UPDATE_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/configuration/XUpdate.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

class Update {
public:
    static ::css::uno::Reference< ::css::configuration::XUpdate > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::configuration::XUpdate > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.configuration.Update" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.configuration.Update of type com.sun.star.configuration.XUpdate" ), the_context);
        }
        return instance;
    }

private:
    Update(); // not implemented
    Update(Update &); // not implemented
    ~Update(); // not implemented
    void operator =(Update); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_UPDATE_HPP
