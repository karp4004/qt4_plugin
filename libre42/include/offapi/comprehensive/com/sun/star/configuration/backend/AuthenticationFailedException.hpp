#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_AUTHENTICATIONFAILEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_AUTHENTICATIONFAILEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/AuthenticationFailedException.hdl"

#include "com/sun/star/configuration/backend/BackendSetupException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline AuthenticationFailedException::AuthenticationFailedException() SAL_THROW(())
    : ::css::configuration::backend::BackendSetupException()
{
    ::cppu::UnoType< ::css::configuration::backend::AuthenticationFailedException >::get();
}

inline AuthenticationFailedException::AuthenticationFailedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& BackendException_) SAL_THROW(())
    : ::css::configuration::backend::BackendSetupException(Message_, Context_, BackendException_)
{
    ::cppu::UnoType< ::css::configuration::backend::AuthenticationFailedException >::get();
}

AuthenticationFailedException::AuthenticationFailedException(AuthenticationFailedException const & the_other): ::css::configuration::backend::BackendSetupException(the_other) {}

AuthenticationFailedException::~AuthenticationFailedException() {}

AuthenticationFailedException & AuthenticationFailedException::operator =(AuthenticationFailedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::backend::BackendSetupException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend { namespace detail {

struct theAuthenticationFailedExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theAuthenticationFailedExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.backend.AuthenticationFailedException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::configuration::backend::BackendSetupException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::AuthenticationFailedException const *) {
    return *detail::theAuthenticationFailedExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::AuthenticationFailedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::AuthenticationFailedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_AUTHENTICATIONFAILEDEXCEPTION_HPP
