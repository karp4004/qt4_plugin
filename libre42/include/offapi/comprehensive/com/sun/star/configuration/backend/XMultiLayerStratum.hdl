#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_XMULTILAYERSTRATUM_HDL
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_XMULTILAYERSTRATUM_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/configuration/backend/BackendAccessException.hdl"
namespace com { namespace sun { namespace star { namespace configuration { namespace backend { class XLayer; } } } } }
namespace com { namespace sun { namespace star { namespace configuration { namespace backend { class XUpdatableLayer; } } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/NoSupportException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

class SAL_NO_VTABLE XMultiLayerStratum : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL listLayerIds( const ::rtl::OUString& aComponent, const ::rtl::OUString& aEntity ) /* throw (::css::configuration::backend::BackendAccessException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getUpdateLayerId( const ::rtl::OUString& aComponent, const ::rtl::OUString& aEntity ) /* throw (::css::configuration::backend::BackendAccessException, ::css::lang::NoSupportException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::configuration::backend::XLayer > SAL_CALL getLayer( const ::rtl::OUString& aLayerId, const ::rtl::OUString& aTimestamp ) /* throw (::css::configuration::backend::BackendAccessException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::configuration::backend::XLayer > > SAL_CALL getLayers( const ::css::uno::Sequence< ::rtl::OUString >& aLayerIds, const ::rtl::OUString& aTimestamp ) /* throw (::css::configuration::backend::BackendAccessException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::configuration::backend::XLayer > > SAL_CALL getMultipleLayers( const ::css::uno::Sequence< ::rtl::OUString >& aLayerIds, const ::css::uno::Sequence< ::rtl::OUString >& aTimestamps ) /* throw (::css::configuration::backend::BackendAccessException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::configuration::backend::XUpdatableLayer > SAL_CALL getUpdatableLayer( const ::rtl::OUString& aLayerId ) /* throw (::css::configuration::backend::BackendAccessException, ::css::lang::NoSupportException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XMultiLayerStratum() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::XMultiLayerStratum const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::configuration::backend::XMultiLayerStratum > *) SAL_THROW(());

#endif
