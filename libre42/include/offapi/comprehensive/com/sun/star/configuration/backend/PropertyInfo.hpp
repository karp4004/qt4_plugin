#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_PROPERTYINFO_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_PROPERTYINFO_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/PropertyInfo.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline PropertyInfo::PropertyInfo() SAL_THROW(())
    : Name()
    , Type()
    , Value()
    , Protected(false)
{
}

inline PropertyInfo::PropertyInfo(const ::rtl::OUString& Name_, const ::rtl::OUString& Type_, const ::css::uno::Any& Value_, const ::sal_Bool& Protected_) SAL_THROW(())
    : Name(Name_)
    , Type(Type_)
    , Value(Value_)
    , Protected(Protected_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend { namespace detail {

struct thePropertyInfoType : public rtl::StaticWithInit< ::css::uno::Type *, thePropertyInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.configuration.backend.PropertyInfo" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::rtl::OUString the_name1( "Type" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name2( "Value" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name3( "Protected" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::PropertyInfo const *) {
    return *detail::thePropertyInfoType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::PropertyInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::PropertyInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_PROPERTYINFO_HPP
