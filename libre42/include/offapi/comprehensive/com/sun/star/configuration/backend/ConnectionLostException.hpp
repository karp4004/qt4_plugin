#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_CONNECTIONLOSTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_CONNECTIONLOSTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/ConnectionLostException.hdl"

#include "com/sun/star/configuration/backend/BackendAccessException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ConnectionLostException::ConnectionLostException() SAL_THROW(())
    : ::css::configuration::backend::BackendAccessException()
{
    ::cppu::UnoType< ::css::configuration::backend::ConnectionLostException >::get();
}

inline ConnectionLostException::ConnectionLostException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& TargetException_) SAL_THROW(())
    : ::css::configuration::backend::BackendAccessException(Message_, Context_, TargetException_)
{
    ::cppu::UnoType< ::css::configuration::backend::ConnectionLostException >::get();
}

ConnectionLostException::ConnectionLostException(ConnectionLostException const & the_other): ::css::configuration::backend::BackendAccessException(the_other) {}

ConnectionLostException::~ConnectionLostException() {}

ConnectionLostException & ConnectionLostException::operator =(ConnectionLostException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::backend::BackendAccessException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend { namespace detail {

struct theConnectionLostExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theConnectionLostExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.backend.ConnectionLostException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::configuration::backend::BackendAccessException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::ConnectionLostException const *) {
    return *detail::theConnectionLostExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::ConnectionLostException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::ConnectionLostException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_CONNECTIONLOSTEXCEPTION_HPP
