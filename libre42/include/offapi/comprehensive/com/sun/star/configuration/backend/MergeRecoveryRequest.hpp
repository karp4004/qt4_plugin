#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_MERGERECOVERYREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_MERGERECOVERYREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/MergeRecoveryRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline MergeRecoveryRequest::MergeRecoveryRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , ErrorDetails()
    , ErrorLayerId()
    , IsRemovalRequest(false)
{
    ::cppu::UnoType< ::css::configuration::backend::MergeRecoveryRequest >::get();
}

inline MergeRecoveryRequest::MergeRecoveryRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& ErrorDetails_, const ::rtl::OUString& ErrorLayerId_, const ::sal_Bool& IsRemovalRequest_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ErrorDetails(ErrorDetails_)
    , ErrorLayerId(ErrorLayerId_)
    , IsRemovalRequest(IsRemovalRequest_)
{
    ::cppu::UnoType< ::css::configuration::backend::MergeRecoveryRequest >::get();
}

MergeRecoveryRequest::MergeRecoveryRequest(MergeRecoveryRequest const & the_other): ::css::uno::Exception(the_other), ErrorDetails(the_other.ErrorDetails), ErrorLayerId(the_other.ErrorLayerId), IsRemovalRequest(the_other.IsRemovalRequest) {}

MergeRecoveryRequest::~MergeRecoveryRequest() {}

MergeRecoveryRequest & MergeRecoveryRequest::operator =(MergeRecoveryRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ErrorDetails = the_other.ErrorDetails;
    ErrorLayerId = the_other.ErrorLayerId;
    IsRemovalRequest = the_other.IsRemovalRequest;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend { namespace detail {

struct theMergeRecoveryRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theMergeRecoveryRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.backend.MergeRecoveryRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "any" );
        ::rtl::OUString sMemberName0( "ErrorDetails" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ANY;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "ErrorLayerId" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "boolean" );
        ::rtl::OUString sMemberName2( "IsRemovalRequest" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::MergeRecoveryRequest const *) {
    return *detail::theMergeRecoveryRequestType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::MergeRecoveryRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::MergeRecoveryRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_MERGERECOVERYREQUEST_HPP
