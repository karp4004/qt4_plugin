#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDSETUPEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDSETUPEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/backend/BackendSetupException.hdl"

#include "com/sun/star/configuration/CannotLoadConfigurationException.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline BackendSetupException::BackendSetupException() SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException()
    , BackendException()
{
    ::cppu::UnoType< ::css::configuration::backend::BackendSetupException >::get();
}

inline BackendSetupException::BackendSetupException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& BackendException_) SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException(Message_, Context_)
    , BackendException(BackendException_)
{
    ::cppu::UnoType< ::css::configuration::backend::BackendSetupException >::get();
}

BackendSetupException::BackendSetupException(BackendSetupException const & the_other): ::css::configuration::CannotLoadConfigurationException(the_other), BackendException(the_other.BackendException) {}

BackendSetupException::~BackendSetupException() {}

BackendSetupException & BackendSetupException::operator =(BackendSetupException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::CannotLoadConfigurationException::operator =(the_other);
    BackendException = the_other.BackendException;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend { namespace detail {

struct theBackendSetupExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theBackendSetupExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.backend.BackendSetupException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::configuration::CannotLoadConfigurationException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "any" );
        ::rtl::OUString sMemberName0( "BackendException" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ANY;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace backend {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::backend::BackendSetupException const *) {
    return *detail::theBackendSetupExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::backend::BackendSetupException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::backend::BackendSetupException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_BACKEND_BACKENDSETUPEXCEPTION_HPP
