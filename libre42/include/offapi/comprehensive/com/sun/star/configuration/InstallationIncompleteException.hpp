#ifndef INCLUDED_COM_SUN_STAR_CONFIGURATION_INSTALLATIONINCOMPLETEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_CONFIGURATION_INSTALLATIONINCOMPLETEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/configuration/InstallationIncompleteException.hdl"

#include "com/sun/star/configuration/CannotLoadConfigurationException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace configuration {

inline InstallationIncompleteException::InstallationIncompleteException() SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException()
{
    ::cppu::UnoType< ::css::configuration::InstallationIncompleteException >::get();
}

inline InstallationIncompleteException::InstallationIncompleteException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::configuration::CannotLoadConfigurationException(Message_, Context_)
{
    ::cppu::UnoType< ::css::configuration::InstallationIncompleteException >::get();
}

InstallationIncompleteException::InstallationIncompleteException(InstallationIncompleteException const & the_other): ::css::configuration::CannotLoadConfigurationException(the_other) {}

InstallationIncompleteException::~InstallationIncompleteException() {}

InstallationIncompleteException & InstallationIncompleteException::operator =(InstallationIncompleteException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::configuration::CannotLoadConfigurationException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace configuration { namespace detail {

struct theInstallationIncompleteExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInstallationIncompleteExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.configuration.InstallationIncompleteException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::configuration::CannotLoadConfigurationException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace configuration {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::configuration::InstallationIncompleteException const *) {
    return *detail::theInstallationIncompleteExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::configuration::InstallationIncompleteException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::configuration::InstallationIncompleteException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_CONFIGURATION_INSTALLATIONINCOMPLETEEXCEPTION_HPP
