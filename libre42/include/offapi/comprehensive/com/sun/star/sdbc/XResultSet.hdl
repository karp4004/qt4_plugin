#ifndef INCLUDED_COM_SUN_STAR_SDBC_XRESULTSET_HDL
#define INCLUDED_COM_SUN_STAR_SDBC_XRESULTSET_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sdbc/SQLException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace sdbc {

class SAL_NO_VTABLE XResultSet : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL next() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isBeforeFirst() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isAfterLast() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isFirst() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isLast() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL beforeFirst() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL afterLast() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL first() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL last() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getRow() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL absolute( ::sal_Int32 row ) /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL relative( ::sal_Int32 rows ) /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL previous() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL refreshRow() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL rowUpdated() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL rowInserted() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL rowDeleted() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::uno::XInterface > SAL_CALL getStatement() /* throw (::css::sdbc::SQLException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XResultSet() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdbc::XResultSet const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::sdbc::XResultSet > *) SAL_THROW(());

#endif
