#ifndef INCLUDED_COM_SUN_STAR_MAIL_NOMAILSERVICEPROVIDEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_NOMAILSERVICEPROVIDEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/mail/NoMailServiceProviderException.hdl"

#include "com/sun/star/mail/MailException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace mail {

inline NoMailServiceProviderException::NoMailServiceProviderException() SAL_THROW(())
    : ::css::mail::MailException()
{
    ::cppu::UnoType< ::css::mail::NoMailServiceProviderException >::get();
}

inline NoMailServiceProviderException::NoMailServiceProviderException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::mail::MailException(Message_, Context_)
{
    ::cppu::UnoType< ::css::mail::NoMailServiceProviderException >::get();
}

NoMailServiceProviderException::NoMailServiceProviderException(NoMailServiceProviderException const & the_other): ::css::mail::MailException(the_other) {}

NoMailServiceProviderException::~NoMailServiceProviderException() {}

NoMailServiceProviderException & NoMailServiceProviderException::operator =(NoMailServiceProviderException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::mail::MailException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace mail { namespace detail {

struct theNoMailServiceProviderExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theNoMailServiceProviderExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.mail.NoMailServiceProviderException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::mail::MailException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::NoMailServiceProviderException const *) {
    return *detail::theNoMailServiceProviderExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mail::NoMailServiceProviderException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mail::NoMailServiceProviderException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_NOMAILSERVICEPROVIDEREXCEPTION_HPP
