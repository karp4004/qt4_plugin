#ifndef INCLUDED_COM_SUN_STAR_MAIL_MAILATTACHMENT_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_MAILATTACHMENT_HPP

#include "sal/config.h"

#include "com/sun/star/mail/MailAttachment.hdl"

#include "com/sun/star/datatransfer/XTransferable.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace mail {

inline MailAttachment::MailAttachment() SAL_THROW(())
    : Data()
    , ReadableName()
{
}

inline MailAttachment::MailAttachment(const ::css::uno::Reference< ::css::datatransfer::XTransferable >& Data_, const ::rtl::OUString& ReadableName_) SAL_THROW(())
    : Data(Data_)
    , ReadableName(ReadableName_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace mail { namespace detail {

struct theMailAttachmentType : public rtl::StaticWithInit< ::css::uno::Type *, theMailAttachmentType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.mail.MailAttachment" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::datatransfer::XTransferable > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.datatransfer.XTransferable" );
        ::rtl::OUString the_name0( "Data" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "ReadableName" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::MailAttachment const *) {
    return *detail::theMailAttachmentType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mail::MailAttachment const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mail::MailAttachment >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_MAILATTACHMENT_HPP
