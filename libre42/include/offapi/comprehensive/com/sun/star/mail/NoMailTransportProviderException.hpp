#ifndef INCLUDED_COM_SUN_STAR_MAIL_NOMAILTRANSPORTPROVIDEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_NOMAILTRANSPORTPROVIDEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/mail/NoMailTransportProviderException.hdl"

#include "com/sun/star/mail/MailException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace mail {

inline NoMailTransportProviderException::NoMailTransportProviderException() SAL_THROW(())
    : ::css::mail::MailException()
{
    ::cppu::UnoType< ::css::mail::NoMailTransportProviderException >::get();
}

inline NoMailTransportProviderException::NoMailTransportProviderException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::mail::MailException(Message_, Context_)
{
    ::cppu::UnoType< ::css::mail::NoMailTransportProviderException >::get();
}

NoMailTransportProviderException::NoMailTransportProviderException(NoMailTransportProviderException const & the_other): ::css::mail::MailException(the_other) {}

NoMailTransportProviderException::~NoMailTransportProviderException() {}

NoMailTransportProviderException & NoMailTransportProviderException::operator =(NoMailTransportProviderException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::mail::MailException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace mail { namespace detail {

struct theNoMailTransportProviderExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theNoMailTransportProviderExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.mail.NoMailTransportProviderException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::mail::MailException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::NoMailTransportProviderException const *) {
    return *detail::theNoMailTransportProviderExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mail::NoMailTransportProviderException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mail::NoMailTransportProviderException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_NOMAILTRANSPORTPROVIDEREXCEPTION_HPP
