#ifndef INCLUDED_COM_SUN_STAR_MAIL_SENDMAILMESSAGEFAILEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_MAIL_SENDMAILMESSAGEFAILEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/mail/SendMailMessageFailedException.hdl"

#include "com/sun/star/mail/MailException.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace mail {

inline SendMailMessageFailedException::SendMailMessageFailedException() SAL_THROW(())
    : ::css::mail::MailException()
    , InvalidAddresses()
    , ValidSentAddresses()
    , ValidUnsentAddresses()
{
    ::cppu::UnoType< ::css::mail::SendMailMessageFailedException >::get();
}

inline SendMailMessageFailedException::SendMailMessageFailedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Sequence< ::rtl::OUString >& InvalidAddresses_, const ::css::uno::Sequence< ::rtl::OUString >& ValidSentAddresses_, const ::css::uno::Sequence< ::rtl::OUString >& ValidUnsentAddresses_) SAL_THROW(())
    : ::css::mail::MailException(Message_, Context_)
    , InvalidAddresses(InvalidAddresses_)
    , ValidSentAddresses(ValidSentAddresses_)
    , ValidUnsentAddresses(ValidUnsentAddresses_)
{
    ::cppu::UnoType< ::css::mail::SendMailMessageFailedException >::get();
}

SendMailMessageFailedException::SendMailMessageFailedException(SendMailMessageFailedException const & the_other): ::css::mail::MailException(the_other), InvalidAddresses(the_other.InvalidAddresses), ValidSentAddresses(the_other.ValidSentAddresses), ValidUnsentAddresses(the_other.ValidUnsentAddresses) {}

SendMailMessageFailedException::~SendMailMessageFailedException() {}

SendMailMessageFailedException & SendMailMessageFailedException::operator =(SendMailMessageFailedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::mail::MailException::operator =(the_other);
    InvalidAddresses = the_other.InvalidAddresses;
    ValidSentAddresses = the_other.ValidSentAddresses;
    ValidUnsentAddresses = the_other.ValidUnsentAddresses;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace mail { namespace detail {

struct theSendMailMessageFailedExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theSendMailMessageFailedExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.mail.SendMailMessageFailedException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::mail::MailException >::get();
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "[]string" );
        ::rtl::OUString sMemberName0( "InvalidAddresses" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "[]string" );
        ::rtl::OUString sMemberName1( "ValidSentAddresses" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "[]string" );
        ::rtl::OUString sMemberName2( "ValidUnsentAddresses" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace mail {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::mail::SendMailMessageFailedException const *) {
    return *detail::theSendMailMessageFailedExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::mail::SendMailMessageFailedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::mail::SendMailMessageFailedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_MAIL_SENDMAILMESSAGEFAILEDEXCEPTION_HPP
