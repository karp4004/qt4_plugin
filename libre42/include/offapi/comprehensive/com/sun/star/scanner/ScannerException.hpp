#ifndef INCLUDED_COM_SUN_STAR_SCANNER_SCANNEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SCANNER_SCANNEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/scanner/ScannerException.hdl"

#include "com/sun/star/scanner/ScanError.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace scanner {

inline ScannerException::ScannerException() SAL_THROW(())
    : ::css::uno::Exception()
    , Error(::css::scanner::ScanError_ScanErrorNone)
{
    ::cppu::UnoType< ::css::scanner::ScannerException >::get();
}

inline ScannerException::ScannerException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::scanner::ScanError& Error_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Error(Error_)
{
    ::cppu::UnoType< ::css::scanner::ScannerException >::get();
}

ScannerException::ScannerException(ScannerException const & the_other): ::css::uno::Exception(the_other), Error(the_other.Error) {}

ScannerException::~ScannerException() {}

ScannerException & ScannerException::operator =(ScannerException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Error = the_other.Error;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace scanner { namespace detail {

struct theScannerExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theScannerExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.scanner.ScannerException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::scanner::ScanError >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "com.sun.star.scanner.ScanError" );
        ::rtl::OUString sMemberName0( "Error" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ENUM;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace scanner {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::scanner::ScannerException const *) {
    return *detail::theScannerExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::scanner::ScannerException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::scanner::ScannerException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCANNER_SCANNEREXCEPTION_HPP
