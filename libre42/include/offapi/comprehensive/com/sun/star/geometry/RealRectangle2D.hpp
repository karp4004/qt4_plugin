#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_REALRECTANGLE2D_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_REALRECTANGLE2D_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/RealRectangle2D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline RealRectangle2D::RealRectangle2D() SAL_THROW(())
    : X1(0)
    , Y1(0)
    , X2(0)
    , Y2(0)
{
}

inline RealRectangle2D::RealRectangle2D(const double& X1_, const double& Y1_, const double& X2_, const double& Y2_) SAL_THROW(())
    : X1(X1_)
    , Y1(Y1_)
    , X2(X2_)
    , Y2(Y2_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry { namespace detail {

struct theRealRectangle2DType : public rtl::StaticWithInit< ::css::uno::Type *, theRealRectangle2DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.geometry.RealRectangle2D" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "X1" );
        ::rtl::OUString the_name1( "Y1" );
        ::rtl::OUString the_name2( "X2" );
        ::rtl::OUString the_name3( "Y2" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::RealRectangle2D const *) {
    return *detail::theRealRectangle2DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::RealRectangle2D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::RealRectangle2D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_REALRECTANGLE2D_HPP
