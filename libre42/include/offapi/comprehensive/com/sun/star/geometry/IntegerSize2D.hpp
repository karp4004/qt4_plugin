#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERSIZE2D_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERSIZE2D_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/IntegerSize2D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline IntegerSize2D::IntegerSize2D() SAL_THROW(())
    : Width(0)
    , Height(0)
{
}

inline IntegerSize2D::IntegerSize2D(const ::sal_Int32& Width_, const ::sal_Int32& Height_) SAL_THROW(())
    : Width(Width_)
    , Height(Height_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry { namespace detail {

struct theIntegerSize2DType : public rtl::StaticWithInit< ::css::uno::Type *, theIntegerSize2DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.geometry.IntegerSize2D" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Width" );
        ::rtl::OUString the_name1( "Height" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::IntegerSize2D const *) {
    return *detail::theIntegerSize2DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::IntegerSize2D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::IntegerSize2D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERSIZE2D_HPP
