#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_ELLIPTICALARC_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_ELLIPTICALARC_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/EllipticalArc.hdl"

#include "com/sun/star/geometry/RealPoint2D.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline EllipticalArc::EllipticalArc() SAL_THROW(())
    : StartPosition()
    , EndPosition()
    , RadiusX(0)
    , RadiusY(0)
    , XAxisRotation(0)
    , IsLargeArc(false)
    , IsClockwiseSweep(false)
{
}

inline EllipticalArc::EllipticalArc(const ::css::geometry::RealPoint2D& StartPosition_, const ::css::geometry::RealPoint2D& EndPosition_, const double& RadiusX_, const double& RadiusY_, const double& XAxisRotation_, const ::sal_Bool& IsLargeArc_, const ::sal_Bool& IsClockwiseSweep_) SAL_THROW(())
    : StartPosition(StartPosition_)
    , EndPosition(EndPosition_)
    , RadiusX(RadiusX_)
    , RadiusY(RadiusY_)
    , XAxisRotation(XAxisRotation_)
    , IsLargeArc(IsLargeArc_)
    , IsClockwiseSweep(IsClockwiseSweep_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry { namespace detail {

struct theEllipticalArcType : public rtl::StaticWithInit< ::css::uno::Type *, theEllipticalArcType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.geometry.EllipticalArc" );
        ::cppu::UnoType< ::css::geometry::RealPoint2D >::get();
        ::rtl::OUString the_tname0( "com.sun.star.geometry.RealPoint2D" );
        ::rtl::OUString the_name0( "StartPosition" );
        ::rtl::OUString the_name1( "EndPosition" );
        ::rtl::OUString the_tname1( "double" );
        ::rtl::OUString the_name2( "RadiusX" );
        ::rtl::OUString the_name3( "RadiusY" );
        ::rtl::OUString the_name4( "XAxisRotation" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name5( "IsLargeArc" );
        ::rtl::OUString the_name6( "IsClockwiseSweep" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::EllipticalArc const *) {
    return *detail::theEllipticalArcType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::EllipticalArc const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::EllipticalArc >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_ELLIPTICALARC_HPP
