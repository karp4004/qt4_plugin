#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_REALRECTANGLE3D_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_REALRECTANGLE3D_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/RealRectangle3D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline RealRectangle3D::RealRectangle3D() SAL_THROW(())
    : X1(0)
    , Y1(0)
    , Z1(0)
    , X2(0)
    , Y2(0)
    , Z2(0)
{
}

inline RealRectangle3D::RealRectangle3D(const double& X1_, const double& Y1_, const double& Z1_, const double& X2_, const double& Y2_, const double& Z2_) SAL_THROW(())
    : X1(X1_)
    , Y1(Y1_)
    , Z1(Z1_)
    , X2(X2_)
    , Y2(Y2_)
    , Z2(Z2_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry { namespace detail {

struct theRealRectangle3DType : public rtl::StaticWithInit< ::css::uno::Type *, theRealRectangle3DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.geometry.RealRectangle3D" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "X1" );
        ::rtl::OUString the_name1( "Y1" );
        ::rtl::OUString the_name2( "Z1" );
        ::rtl::OUString the_name3( "X2" );
        ::rtl::OUString the_name4( "Y2" );
        ::rtl::OUString the_name5( "Z2" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::RealRectangle3D const *) {
    return *detail::theRealRectangle3DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::RealRectangle3D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::RealRectangle3D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_REALRECTANGLE3D_HPP
