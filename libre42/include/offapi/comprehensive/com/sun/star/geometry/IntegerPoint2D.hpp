#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERPOINT2D_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERPOINT2D_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/IntegerPoint2D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline IntegerPoint2D::IntegerPoint2D() SAL_THROW(())
    : X(0)
    , Y(0)
{
}

inline IntegerPoint2D::IntegerPoint2D(const ::sal_Int32& X_, const ::sal_Int32& Y_) SAL_THROW(())
    : X(X_)
    , Y(Y_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry { namespace detail {

struct theIntegerPoint2DType : public rtl::StaticWithInit< ::css::uno::Type *, theIntegerPoint2DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.geometry.IntegerPoint2D" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "X" );
        ::rtl::OUString the_name1( "Y" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::IntegerPoint2D const *) {
    return *detail::theIntegerPoint2DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::IntegerPoint2D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::IntegerPoint2D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_INTEGERPOINT2D_HPP
