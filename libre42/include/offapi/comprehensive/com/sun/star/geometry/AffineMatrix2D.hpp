#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_AFFINEMATRIX2D_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_AFFINEMATRIX2D_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/AffineMatrix2D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline AffineMatrix2D::AffineMatrix2D() SAL_THROW(())
    : m00(0)
    , m01(0)
    , m02(0)
    , m10(0)
    , m11(0)
    , m12(0)
{
}

inline AffineMatrix2D::AffineMatrix2D(const double& m00_, const double& m01_, const double& m02_, const double& m10_, const double& m11_, const double& m12_) SAL_THROW(())
    : m00(m00_)
    , m01(m01_)
    , m02(m02_)
    , m10(m10_)
    , m11(m11_)
    , m12(m12_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry { namespace detail {

struct theAffineMatrix2DType : public rtl::StaticWithInit< ::css::uno::Type *, theAffineMatrix2DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.geometry.AffineMatrix2D" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "m00" );
        ::rtl::OUString the_name1( "m01" );
        ::rtl::OUString the_name2( "m02" );
        ::rtl::OUString the_name3( "m10" );
        ::rtl::OUString the_name4( "m11" );
        ::rtl::OUString the_name5( "m12" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::AffineMatrix2D const *) {
    return *detail::theAffineMatrix2DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::AffineMatrix2D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::AffineMatrix2D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_AFFINEMATRIX2D_HPP
