#ifndef INCLUDED_COM_SUN_STAR_GEOMETRY_REALBEZIERSEGMENT2D_HPP
#define INCLUDED_COM_SUN_STAR_GEOMETRY_REALBEZIERSEGMENT2D_HPP

#include "sal/config.h"

#include "com/sun/star/geometry/RealBezierSegment2D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace geometry {

inline RealBezierSegment2D::RealBezierSegment2D() SAL_THROW(())
    : Px(0)
    , Py(0)
    , C1x(0)
    , C1y(0)
    , C2x(0)
    , C2y(0)
{
}

inline RealBezierSegment2D::RealBezierSegment2D(const double& Px_, const double& Py_, const double& C1x_, const double& C1y_, const double& C2x_, const double& C2y_) SAL_THROW(())
    : Px(Px_)
    , Py(Py_)
    , C1x(C1x_)
    , C1y(C1y_)
    , C2x(C2x_)
    , C2y(C2y_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace geometry { namespace detail {

struct theRealBezierSegment2DType : public rtl::StaticWithInit< ::css::uno::Type *, theRealBezierSegment2DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.geometry.RealBezierSegment2D" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Px" );
        ::rtl::OUString the_name1( "Py" );
        ::rtl::OUString the_name2( "C1x" );
        ::rtl::OUString the_name3( "C1y" );
        ::rtl::OUString the_name4( "C2x" );
        ::rtl::OUString the_name5( "C2y" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace geometry {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::geometry::RealBezierSegment2D const *) {
    return *detail::theRealBezierSegment2DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::geometry::RealBezierSegment2D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::geometry::RealBezierSegment2D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_GEOMETRY_REALBEZIERSEGMENT2D_HPP
