#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_CMISVERSION_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_CMISVERSION_HPP

#include "sal/config.h"

#include "com/sun/star/document/CmisVersion.hdl"

#include "com/sun/star/util/DateTime.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace document {

inline CmisVersion::CmisVersion() SAL_THROW(())
    : Id()
    , TimeStamp()
    , Author()
    , Comment()
{
}

inline CmisVersion::CmisVersion(const ::rtl::OUString& Id_, const ::css::util::DateTime& TimeStamp_, const ::rtl::OUString& Author_, const ::rtl::OUString& Comment_) SAL_THROW(())
    : Id(Id_)
    , TimeStamp(TimeStamp_)
    , Author(Author_)
    , Comment(Comment_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theCmisVersionType : public rtl::StaticWithInit< ::css::uno::Type *, theCmisVersionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.document.CmisVersion" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Id" );
        ::cppu::UnoType< ::css::util::DateTime >::get();
        ::rtl::OUString the_tname1( "com.sun.star.util.DateTime" );
        ::rtl::OUString the_name1( "TimeStamp" );
        ::rtl::OUString the_name2( "Author" );
        ::rtl::OUString the_name3( "Comment" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::CmisVersion const *) {
    return *detail::theCmisVersionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::CmisVersion const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::CmisVersion >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_CMISVERSION_HPP
