#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_EVENTOBJECT_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_EVENTOBJECT_HPP

#include "sal/config.h"

#include "com/sun/star/document/EventObject.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace document {

inline EventObject::EventObject() SAL_THROW(())
    : ::css::lang::EventObject()
    , EventName()
{
}

inline EventObject::EventObject(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& EventName_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , EventName(EventName_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theEventObjectType : public rtl::StaticWithInit< ::css::uno::Type *, theEventObjectType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.document.EventObject" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "EventName" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::EventObject const *) {
    return *detail::theEventObjectType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::EventObject const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::EventObject >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_EVENTOBJECT_HPP
