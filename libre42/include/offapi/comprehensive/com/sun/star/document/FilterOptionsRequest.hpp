#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_FILTEROPTIONSREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_FILTEROPTIONSREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/document/FilterOptionsRequest.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/frame/XModel.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline FilterOptionsRequest::FilterOptionsRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , rModel()
    , rProperties()
{
    ::cppu::UnoType< ::css::document::FilterOptionsRequest >::get();
}

inline FilterOptionsRequest::FilterOptionsRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Reference< ::css::frame::XModel >& rModel_, const ::css::uno::Sequence< ::css::beans::PropertyValue >& rProperties_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , rModel(rModel_)
    , rProperties(rProperties_)
{
    ::cppu::UnoType< ::css::document::FilterOptionsRequest >::get();
}

FilterOptionsRequest::FilterOptionsRequest(FilterOptionsRequest const & the_other): ::css::uno::Exception(the_other), rModel(the_other.rModel), rProperties(the_other.rProperties) {}

FilterOptionsRequest::~FilterOptionsRequest() {}

FilterOptionsRequest & FilterOptionsRequest::operator =(FilterOptionsRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    rModel = the_other.rModel;
    rProperties = the_other.rProperties;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theFilterOptionsRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theFilterOptionsRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.document.FilterOptionsRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XModel > >::get();
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::PropertyValue > >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "com.sun.star.frame.XModel" );
        ::rtl::OUString sMemberName0( "rModel" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "[]com.sun.star.beans.PropertyValue" );
        ::rtl::OUString sMemberName1( "rProperties" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::FilterOptionsRequest const *) {
    return *detail::theFilterOptionsRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::FilterOptionsRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::FilterOptionsRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_FILTEROPTIONSREQUEST_HPP
