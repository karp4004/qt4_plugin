#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_UNDOMANAGEREVENT_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_UNDOMANAGEREVENT_HPP

#include "sal/config.h"

#include "com/sun/star/document/UndoManagerEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace document {

inline UndoManagerEvent::UndoManagerEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , UndoActionTitle()
    , UndoContextDepth(0)
{
}

inline UndoManagerEvent::UndoManagerEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& UndoActionTitle_, const ::sal_Int32& UndoContextDepth_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , UndoActionTitle(UndoActionTitle_)
    , UndoContextDepth(UndoContextDepth_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theUndoManagerEventType : public rtl::StaticWithInit< ::css::uno::Type *, theUndoManagerEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.document.UndoManagerEvent" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "UndoActionTitle" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "UndoContextDepth" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::UndoManagerEvent const *) {
    return *detail::theUndoManagerEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::UndoManagerEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::UndoManagerEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_UNDOMANAGEREVENT_HPP
