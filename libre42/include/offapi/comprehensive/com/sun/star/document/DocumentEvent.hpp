#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_DOCUMENTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_DOCUMENTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/document/DocumentEvent.hdl"

#include "com/sun/star/frame/XController2.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace document {

inline DocumentEvent::DocumentEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , EventName()
    , ViewController()
    , Supplement()
{
}

inline DocumentEvent::DocumentEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& EventName_, const ::css::uno::Reference< ::css::frame::XController2 >& ViewController_, const ::css::uno::Any& Supplement_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , EventName(EventName_)
    , ViewController(ViewController_)
    , Supplement(Supplement_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theDocumentEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDocumentEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.document.DocumentEvent" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "EventName" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XController2 > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.frame.XController2" );
        ::rtl::OUString the_name1( "ViewController" );
        ::rtl::OUString the_tname2( "any" );
        ::rtl::OUString the_name2( "Supplement" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::DocumentEvent const *) {
    return *detail::theDocumentEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::DocumentEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::DocumentEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_DOCUMENTEVENT_HPP
