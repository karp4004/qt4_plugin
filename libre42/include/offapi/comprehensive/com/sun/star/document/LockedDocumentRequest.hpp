#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_LOCKEDDOCUMENTREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_LOCKEDDOCUMENTREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/document/LockedDocumentRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline LockedDocumentRequest::LockedDocumentRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , DocumentURL()
    , UserInfo()
{
    ::cppu::UnoType< ::css::document::LockedDocumentRequest >::get();
}

inline LockedDocumentRequest::LockedDocumentRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& DocumentURL_, const ::rtl::OUString& UserInfo_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , DocumentURL(DocumentURL_)
    , UserInfo(UserInfo_)
{
    ::cppu::UnoType< ::css::document::LockedDocumentRequest >::get();
}

LockedDocumentRequest::LockedDocumentRequest(LockedDocumentRequest const & the_other): ::css::uno::Exception(the_other), DocumentURL(the_other.DocumentURL), UserInfo(the_other.UserInfo) {}

LockedDocumentRequest::~LockedDocumentRequest() {}

LockedDocumentRequest & LockedDocumentRequest::operator =(LockedDocumentRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    DocumentURL = the_other.DocumentURL;
    UserInfo = the_other.UserInfo;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theLockedDocumentRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theLockedDocumentRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.document.LockedDocumentRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "DocumentURL" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "UserInfo" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::LockedDocumentRequest const *) {
    return *detail::theLockedDocumentRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::LockedDocumentRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::LockedDocumentRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_LOCKEDDOCUMENTREQUEST_HPP
