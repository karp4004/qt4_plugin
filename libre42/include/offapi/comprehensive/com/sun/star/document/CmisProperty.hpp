#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_CMISPROPERTY_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_CMISPROPERTY_HPP

#include "sal/config.h"

#include "com/sun/star/document/CmisProperty.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace document {

inline CmisProperty::CmisProperty() SAL_THROW(())
    : Id()
    , Name()
    , Type()
    , Updatable(false)
    , Required(false)
    , MultiValued(false)
    , OpenChoice(false)
    , Choices()
    , Value()
{
}

inline CmisProperty::CmisProperty(const ::rtl::OUString& Id_, const ::rtl::OUString& Name_, const ::rtl::OUString& Type_, const ::sal_Bool& Updatable_, const ::sal_Bool& Required_, const ::sal_Bool& MultiValued_, const ::sal_Bool& OpenChoice_, const ::css::uno::Any& Choices_, const ::css::uno::Any& Value_) SAL_THROW(())
    : Id(Id_)
    , Name(Name_)
    , Type(Type_)
    , Updatable(Updatable_)
    , Required(Required_)
    , MultiValued(MultiValued_)
    , OpenChoice(OpenChoice_)
    , Choices(Choices_)
    , Value(Value_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theCmisPropertyType : public rtl::StaticWithInit< ::css::uno::Type *, theCmisPropertyType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.document.CmisProperty" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Id" );
        ::rtl::OUString the_name1( "Name" );
        ::rtl::OUString the_name2( "Type" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name3( "Updatable" );
        ::rtl::OUString the_name4( "Required" );
        ::rtl::OUString the_name5( "MultiValued" );
        ::rtl::OUString the_name6( "OpenChoice" );
        ::rtl::OUString the_tname2( "any" );
        ::rtl::OUString the_name7( "Choices" );
        ::rtl::OUString the_name8( "Value" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name6.pData }, false },
            { { typelib_TypeClass_ANY, the_tname2.pData, the_name7.pData }, false },
            { { typelib_TypeClass_ANY, the_tname2.pData, the_name8.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 9, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::CmisProperty const *) {
    return *detail::theCmisPropertyType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::CmisProperty const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::CmisProperty >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_CMISPROPERTY_HPP
