#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_BROKENPACKAGEREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_BROKENPACKAGEREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/document/BrokenPackageRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline BrokenPackageRequest::BrokenPackageRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , aName()
{
    ::cppu::UnoType< ::css::document::BrokenPackageRequest >::get();
}

inline BrokenPackageRequest::BrokenPackageRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& aName_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , aName(aName_)
{
    ::cppu::UnoType< ::css::document::BrokenPackageRequest >::get();
}

BrokenPackageRequest::BrokenPackageRequest(BrokenPackageRequest const & the_other): ::css::uno::Exception(the_other), aName(the_other.aName) {}

BrokenPackageRequest::~BrokenPackageRequest() {}

BrokenPackageRequest & BrokenPackageRequest::operator =(BrokenPackageRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    aName = the_other.aName;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theBrokenPackageRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theBrokenPackageRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.document.BrokenPackageRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "aName" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::BrokenPackageRequest const *) {
    return *detail::theBrokenPackageRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::BrokenPackageRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::BrokenPackageRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_BROKENPACKAGEREQUEST_HPP
