#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_XEVENTLISTENER_HDL
#define INCLUDED_COM_SUN_STAR_DOCUMENT_XEVENTLISTENER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/document/EventObject.hdl"
#include "com/sun/star/lang/XEventListener.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace document {

class SAL_NO_VTABLE XEventListener : public ::css::lang::XEventListener
{
public:

    // Methods
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual void SAL_CALL notifyEvent( const ::css::document::EventObject& Event ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XEventListener() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::XEventListener const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::document::XEventListener > *) SAL_THROW(());

#endif
