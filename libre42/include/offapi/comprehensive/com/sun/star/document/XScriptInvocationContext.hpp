#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_XSCRIPTINVOCATIONCONTEXT_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_XSCRIPTINVOCATIONCONTEXT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/document/XScriptInvocationContext.hdl"

#include "com/sun/star/document/XEmbeddedScripts.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theXScriptInvocationContextType : public rtl::StaticWithInit< ::css::uno::Type *, theXScriptInvocationContextType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.document.XScriptInvocationContext" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[1] = { 0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.document.XScriptInvocationContext::ScriptContainer" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            1,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::XScriptInvocationContext const *) {
    const ::css::uno::Type &rRet = *detail::theXScriptInvocationContextType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::document::XEmbeddedScripts > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "com.sun.star.document.XEmbeddedScripts" );
                ::rtl::OUString sAttributeName0( "com.sun.star.document.XScriptInvocationContext::ScriptContainer" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    3, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType0.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::document::XScriptInvocationContext > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::document::XScriptInvocationContext > >::get();
}

::css::uno::Type const & ::css::document::XScriptInvocationContext::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::document::XScriptInvocationContext > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_XSCRIPTINVOCATIONCONTEXT_HPP
