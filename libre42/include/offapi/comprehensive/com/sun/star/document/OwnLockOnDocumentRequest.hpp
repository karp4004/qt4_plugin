#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_OWNLOCKONDOCUMENTREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_OWNLOCKONDOCUMENTREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/document/OwnLockOnDocumentRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace document {

inline OwnLockOnDocumentRequest::OwnLockOnDocumentRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , DocumentURL()
    , TimeInfo()
    , IsStoring(false)
{
    ::cppu::UnoType< ::css::document::OwnLockOnDocumentRequest >::get();
}

inline OwnLockOnDocumentRequest::OwnLockOnDocumentRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& DocumentURL_, const ::rtl::OUString& TimeInfo_, const ::sal_Bool& IsStoring_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , DocumentURL(DocumentURL_)
    , TimeInfo(TimeInfo_)
    , IsStoring(IsStoring_)
{
    ::cppu::UnoType< ::css::document::OwnLockOnDocumentRequest >::get();
}

OwnLockOnDocumentRequest::OwnLockOnDocumentRequest(OwnLockOnDocumentRequest const & the_other): ::css::uno::Exception(the_other), DocumentURL(the_other.DocumentURL), TimeInfo(the_other.TimeInfo), IsStoring(the_other.IsStoring) {}

OwnLockOnDocumentRequest::~OwnLockOnDocumentRequest() {}

OwnLockOnDocumentRequest & OwnLockOnDocumentRequest::operator =(OwnLockOnDocumentRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    DocumentURL = the_other.DocumentURL;
    TimeInfo = the_other.TimeInfo;
    IsStoring = the_other.IsStoring;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theOwnLockOnDocumentRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theOwnLockOnDocumentRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.document.OwnLockOnDocumentRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "DocumentURL" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "TimeInfo" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "boolean" );
        ::rtl::OUString sMemberName2( "IsStoring" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::OwnLockOnDocumentRequest const *) {
    return *detail::theOwnLockOnDocumentRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::OwnLockOnDocumentRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::OwnLockOnDocumentRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_OWNLOCKONDOCUMENTREQUEST_HPP
