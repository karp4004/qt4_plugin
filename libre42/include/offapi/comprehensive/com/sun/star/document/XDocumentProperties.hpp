#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_XDOCUMENTPROPERTIES_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_XDOCUMENTPROPERTIES_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/document/XDocumentProperties.hdl"

#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/beans/XPropertyContainer.hpp"
#include "com/sun/star/embed/XStorage.hpp"
#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/io/WrongFormatException.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/lang/WrappedTargetException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/util/DateTime.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theXDocumentPropertiesType : public rtl::StaticWithInit< ::css::uno::Type *, theXDocumentPropertiesType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.document.XDocumentProperties" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[27] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.document.XDocumentProperties::Author" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.document.XDocumentProperties::Generator" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.document.XDocumentProperties::CreationDate" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.document.XDocumentProperties::Title" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.document.XDocumentProperties::Subject" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.document.XDocumentProperties::Description" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.document.XDocumentProperties::Keywords" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.document.XDocumentProperties::Language" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.document.XDocumentProperties::ModifiedBy" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.document.XDocumentProperties::ModificationDate" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.document.XDocumentProperties::PrintedBy" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.document.XDocumentProperties::PrintDate" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.document.XDocumentProperties::TemplateName" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );
        ::rtl::OUString sAttributeName13( "com.sun.star.document.XDocumentProperties::TemplateURL" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName13.pData );
        ::rtl::OUString sAttributeName14( "com.sun.star.document.XDocumentProperties::TemplateDate" );
        typelib_typedescriptionreference_new( &pMembers[14],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName14.pData );
        ::rtl::OUString sAttributeName15( "com.sun.star.document.XDocumentProperties::AutoloadURL" );
        typelib_typedescriptionreference_new( &pMembers[15],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName15.pData );
        ::rtl::OUString sAttributeName16( "com.sun.star.document.XDocumentProperties::AutoloadSecs" );
        typelib_typedescriptionreference_new( &pMembers[16],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName16.pData );
        ::rtl::OUString sAttributeName17( "com.sun.star.document.XDocumentProperties::DefaultTarget" );
        typelib_typedescriptionreference_new( &pMembers[17],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName17.pData );
        ::rtl::OUString sAttributeName18( "com.sun.star.document.XDocumentProperties::DocumentStatistics" );
        typelib_typedescriptionreference_new( &pMembers[18],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName18.pData );
        ::rtl::OUString sAttributeName19( "com.sun.star.document.XDocumentProperties::EditingCycles" );
        typelib_typedescriptionreference_new( &pMembers[19],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName19.pData );
        ::rtl::OUString sAttributeName20( "com.sun.star.document.XDocumentProperties::EditingDuration" );
        typelib_typedescriptionreference_new( &pMembers[20],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName20.pData );
        ::rtl::OUString sMethodName0( "com.sun.star.document.XDocumentProperties::resetUserData" );
        typelib_typedescriptionreference_new( &pMembers[21],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );
        ::rtl::OUString sMethodName1( "com.sun.star.document.XDocumentProperties::getUserDefinedProperties" );
        typelib_typedescriptionreference_new( &pMembers[22],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName1.pData );
        ::rtl::OUString sMethodName2( "com.sun.star.document.XDocumentProperties::loadFromStorage" );
        typelib_typedescriptionreference_new( &pMembers[23],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName2.pData );
        ::rtl::OUString sMethodName3( "com.sun.star.document.XDocumentProperties::loadFromMedium" );
        typelib_typedescriptionreference_new( &pMembers[24],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName3.pData );
        ::rtl::OUString sMethodName4( "com.sun.star.document.XDocumentProperties::storeToStorage" );
        typelib_typedescriptionreference_new( &pMembers[25],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName4.pData );
        ::rtl::OUString sMethodName5( "com.sun.star.document.XDocumentProperties::storeToMedium" );
        typelib_typedescriptionreference_new( &pMembers[26],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName5.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            27,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescriptionreference_release( pMembers[14] );
        typelib_typedescriptionreference_release( pMembers[15] );
        typelib_typedescriptionreference_release( pMembers[16] );
        typelib_typedescriptionreference_release( pMembers[17] );
        typelib_typedescriptionreference_release( pMembers[18] );
        typelib_typedescriptionreference_release( pMembers[19] );
        typelib_typedescriptionreference_release( pMembers[20] );
        typelib_typedescriptionreference_release( pMembers[21] );
        typelib_typedescriptionreference_release( pMembers[22] );
        typelib_typedescriptionreference_release( pMembers[23] );
        typelib_typedescriptionreference_release( pMembers[24] );
        typelib_typedescriptionreference_release( pMembers[25] );
        typelib_typedescriptionreference_release( pMembers[26] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::XDocumentProperties const *) {
    const ::css::uno::Type &rRet = *detail::theXDocumentPropertiesType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::util::DateTime >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
            ::cppu::UnoType< ::css::lang::Locale >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::NamedValue > >::get();
            ::cppu::UnoType< ::css::io::WrongFormatException >::get();
            ::cppu::UnoType< ::css::lang::WrappedTargetException >::get();
            ::cppu::UnoType< ::css::io::IOException >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "string" );
                ::rtl::OUString sAttributeName0( "com.sun.star.document.XDocumentProperties::Author" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    3, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "string" );
                ::rtl::OUString sAttributeName1( "com.sun.star.document.XDocumentProperties::Generator" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    4, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "com.sun.star.util.DateTime" );
                ::rtl::OUString sAttributeName2( "com.sun.star.document.XDocumentProperties::CreationDate" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    5, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "string" );
                ::rtl::OUString sAttributeName3( "com.sun.star.document.XDocumentProperties::Title" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    6, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "string" );
                ::rtl::OUString sAttributeName4( "com.sun.star.document.XDocumentProperties::Subject" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "string" );
                ::rtl::OUString sAttributeName5( "com.sun.star.document.XDocumentProperties::Description" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    8, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType5.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "[]string" );
                ::rtl::OUString sAttributeName6( "com.sun.star.document.XDocumentProperties::Keywords" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    9, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType6.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "com.sun.star.lang.Locale" );
                ::rtl::OUString sAttributeName7( "com.sun.star.document.XDocumentProperties::Language" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    10, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType7.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "string" );
                ::rtl::OUString sAttributeName8( "com.sun.star.document.XDocumentProperties::ModifiedBy" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    11, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType8.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "com.sun.star.util.DateTime" );
                ::rtl::OUString sAttributeName9( "com.sun.star.document.XDocumentProperties::ModificationDate" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    12, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType9.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "string" );
                ::rtl::OUString sAttributeName10( "com.sun.star.document.XDocumentProperties::PrintedBy" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    13, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType10.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "com.sun.star.util.DateTime" );
                ::rtl::OUString sAttributeName11( "com.sun.star.document.XDocumentProperties::PrintDate" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    14, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType11.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "string" );
                ::rtl::OUString sAttributeName12( "com.sun.star.document.XDocumentProperties::TemplateName" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    15, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType12.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType13( "string" );
                ::rtl::OUString sAttributeName13( "com.sun.star.document.XDocumentProperties::TemplateURL" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    16, sAttributeName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType13.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType14( "com.sun.star.util.DateTime" );
                ::rtl::OUString sAttributeName14( "com.sun.star.document.XDocumentProperties::TemplateDate" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    17, sAttributeName14.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType14.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType15( "string" );
                ::rtl::OUString sAttributeName15( "com.sun.star.document.XDocumentProperties::AutoloadURL" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    18, sAttributeName15.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType15.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType16( "long" );
                ::rtl::OUString sAttributeName16( "com.sun.star.document.XDocumentProperties::AutoloadSecs" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    19, sAttributeName16.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType16.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType17( "string" );
                ::rtl::OUString sAttributeName17( "com.sun.star.document.XDocumentProperties::DefaultTarget" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    20, sAttributeName17.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType17.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType18( "[]com.sun.star.beans.NamedValue" );
                ::rtl::OUString sAttributeName18( "com.sun.star.document.XDocumentProperties::DocumentStatistics" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    21, sAttributeName18.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType18.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType19( "short" );
                ::rtl::OUString sAttributeName19( "com.sun.star.document.XDocumentProperties::EditingCycles" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    22, sAttributeName19.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType19.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType20( "long" );
                ::rtl::OUString sAttributeName20( "com.sun.star.document.XDocumentProperties::EditingDuration" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    23, sAttributeName20.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType20.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Author" );
                ::rtl::OUString sParamType0( "string" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType0( "void" );
                ::rtl::OUString sMethodName0( "com.sun.star.document.XDocumentProperties::resetUserData" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    24, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType0.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType1( "com.sun.star.beans.XPropertyContainer" );
                ::rtl::OUString sMethodName1( "com.sun.star.document.XDocumentProperties::getUserDefinedProperties" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    25, sal_False,
                    sMethodName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sReturnType1.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[2];
                ::rtl::OUString sParamName0( "Storage" );
                ::rtl::OUString sParamType0( "com.sun.star.embed.XStorage" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString sParamName1( "Medium" );
                ::rtl::OUString sParamType1( "[]com.sun.star.beans.PropertyValue" );
                aParameters[1].pParamName = sParamName1.pData;
                aParameters[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
                aParameters[1].pTypeName = sParamType1.pData;
                aParameters[1].bIn = sal_True;
                aParameters[1].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.io.WrongFormatException" );
                ::rtl::OUString the_ExceptionName2( "com.sun.star.lang.WrappedTargetException" );
                ::rtl::OUString the_ExceptionName3( "com.sun.star.io.IOException" );
                ::rtl::OUString the_ExceptionName4( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData, the_ExceptionName2.pData, the_ExceptionName3.pData, the_ExceptionName4.pData };
                ::rtl::OUString sReturnType2( "void" );
                ::rtl::OUString sMethodName2( "com.sun.star.document.XDocumentProperties::loadFromStorage" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    26, sal_False,
                    sMethodName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType2.pData,
                    2, aParameters,
                    5, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[2];
                ::rtl::OUString sParamName0( "URL" );
                ::rtl::OUString sParamType0( "string" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString sParamName1( "Medium" );
                ::rtl::OUString sParamType1( "[]com.sun.star.beans.PropertyValue" );
                aParameters[1].pParamName = sParamName1.pData;
                aParameters[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
                aParameters[1].pTypeName = sParamType1.pData;
                aParameters[1].bIn = sal_True;
                aParameters[1].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.io.WrongFormatException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.lang.WrappedTargetException" );
                ::rtl::OUString the_ExceptionName2( "com.sun.star.io.IOException" );
                ::rtl::OUString the_ExceptionName3( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData, the_ExceptionName2.pData, the_ExceptionName3.pData };
                ::rtl::OUString sReturnType3( "void" );
                ::rtl::OUString sMethodName3( "com.sun.star.document.XDocumentProperties::loadFromMedium" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    27, sal_False,
                    sMethodName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType3.pData,
                    2, aParameters,
                    4, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[2];
                ::rtl::OUString sParamName0( "Storage" );
                ::rtl::OUString sParamType0( "com.sun.star.embed.XStorage" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString sParamName1( "Medium" );
                ::rtl::OUString sParamType1( "[]com.sun.star.beans.PropertyValue" );
                aParameters[1].pParamName = sParamName1.pData;
                aParameters[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
                aParameters[1].pTypeName = sParamType1.pData;
                aParameters[1].bIn = sal_True;
                aParameters[1].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.lang.WrappedTargetException" );
                ::rtl::OUString the_ExceptionName2( "com.sun.star.io.IOException" );
                ::rtl::OUString the_ExceptionName3( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData, the_ExceptionName2.pData, the_ExceptionName3.pData };
                ::rtl::OUString sReturnType4( "void" );
                ::rtl::OUString sMethodName4( "com.sun.star.document.XDocumentProperties::storeToStorage" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    28, sal_False,
                    sMethodName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType4.pData,
                    2, aParameters,
                    4, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[2];
                ::rtl::OUString sParamName0( "URL" );
                ::rtl::OUString sParamType0( "string" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString sParamName1( "Medium" );
                ::rtl::OUString sParamType1( "[]com.sun.star.beans.PropertyValue" );
                aParameters[1].pParamName = sParamName1.pData;
                aParameters[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
                aParameters[1].pTypeName = sParamType1.pData;
                aParameters[1].bIn = sal_True;
                aParameters[1].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.lang.WrappedTargetException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.io.IOException" );
                ::rtl::OUString the_ExceptionName2( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData, the_ExceptionName2.pData };
                ::rtl::OUString sReturnType5( "void" );
                ::rtl::OUString sMethodName5( "com.sun.star.document.XDocumentProperties::storeToMedium" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    29, sal_False,
                    sMethodName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType5.pData,
                    2, aParameters,
                    3, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::document::XDocumentProperties > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::document::XDocumentProperties > >::get();
}

::css::uno::Type const & ::css::document::XDocumentProperties::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::document::XDocumentProperties > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_XDOCUMENTPROPERTIES_HPP
