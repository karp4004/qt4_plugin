#ifndef INCLUDED_COM_SUN_STAR_DOCUMENT_AMBIGOUSFILTERREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_DOCUMENT_AMBIGOUSFILTERREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/document/AmbigousFilterRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace document {

inline AmbigousFilterRequest::AmbigousFilterRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , URL()
    , SelectedFilter()
    , DetectedFilter()
{
    ::cppu::UnoType< ::css::document::AmbigousFilterRequest >::get();
}

inline AmbigousFilterRequest::AmbigousFilterRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& URL_, const ::rtl::OUString& SelectedFilter_, const ::rtl::OUString& DetectedFilter_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , URL(URL_)
    , SelectedFilter(SelectedFilter_)
    , DetectedFilter(DetectedFilter_)
{
    ::cppu::UnoType< ::css::document::AmbigousFilterRequest >::get();
}

AmbigousFilterRequest::AmbigousFilterRequest(AmbigousFilterRequest const & the_other): ::css::uno::Exception(the_other), URL(the_other.URL), SelectedFilter(the_other.SelectedFilter), DetectedFilter(the_other.DetectedFilter) {}

AmbigousFilterRequest::~AmbigousFilterRequest() {}

AmbigousFilterRequest & AmbigousFilterRequest::operator =(AmbigousFilterRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    URL = the_other.URL;
    SelectedFilter = the_other.SelectedFilter;
    DetectedFilter = the_other.DetectedFilter;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace document { namespace detail {

struct theAmbigousFilterRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theAmbigousFilterRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.document.AmbigousFilterRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "URL" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "SelectedFilter" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "string" );
        ::rtl::OUString sMemberName2( "DetectedFilter" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace document {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::document::AmbigousFilterRequest const *) {
    return *detail::theAmbigousFilterRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::document::AmbigousFilterRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::document::AmbigousFilterRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DOCUMENT_AMBIGOUSFILTERREQUEST_HPP
