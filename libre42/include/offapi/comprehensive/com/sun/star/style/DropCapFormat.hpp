#ifndef INCLUDED_COM_SUN_STAR_STYLE_DROPCAPFORMAT_HPP
#define INCLUDED_COM_SUN_STAR_STYLE_DROPCAPFORMAT_HPP

#include "sal/config.h"

#include "com/sun/star/style/DropCapFormat.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace style {

inline DropCapFormat::DropCapFormat() SAL_THROW(())
    : Lines(0)
    , Count(0)
    , Distance(0)
{
}

inline DropCapFormat::DropCapFormat(const ::sal_Int8& Lines_, const ::sal_Int8& Count_, const ::sal_Int16& Distance_) SAL_THROW(())
    : Lines(Lines_)
    , Count(Count_)
    , Distance(Distance_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace style { namespace detail {

struct theDropCapFormatType : public rtl::StaticWithInit< ::css::uno::Type *, theDropCapFormatType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.style.DropCapFormat" );
        ::rtl::OUString the_tname0( "byte" );
        ::rtl::OUString the_name0( "Lines" );
        ::rtl::OUString the_name1( "Count" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name2( "Distance" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace style {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::DropCapFormat const *) {
    return *detail::theDropCapFormatType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::style::DropCapFormat const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::style::DropCapFormat >::get();
}

#endif // INCLUDED_COM_SUN_STAR_STYLE_DROPCAPFORMAT_HPP
