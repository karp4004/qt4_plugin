#ifndef INCLUDED_COM_SUN_STAR_STYLE_LINESPACING_HPP
#define INCLUDED_COM_SUN_STAR_STYLE_LINESPACING_HPP

#include "sal/config.h"

#include "com/sun/star/style/LineSpacing.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace style {

inline LineSpacing::LineSpacing() SAL_THROW(())
    : Mode(0)
    , Height(0)
{
}

inline LineSpacing::LineSpacing(const ::sal_Int16& Mode_, const ::sal_Int16& Height_) SAL_THROW(())
    : Mode(Mode_)
    , Height(Height_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace style { namespace detail {

struct theLineSpacingType : public rtl::StaticWithInit< ::css::uno::Type *, theLineSpacingType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.style.LineSpacing" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "Mode" );
        ::rtl::OUString the_name1( "Height" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace style {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::LineSpacing const *) {
    return *detail::theLineSpacingType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::style::LineSpacing const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::style::LineSpacing >::get();
}

#endif // INCLUDED_COM_SUN_STAR_STYLE_LINESPACING_HPP
