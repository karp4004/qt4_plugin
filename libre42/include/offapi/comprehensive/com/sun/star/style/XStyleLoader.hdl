#ifndef INCLUDED_COM_SUN_STAR_STYLE_XSTYLELOADER_HDL
#define INCLUDED_COM_SUN_STAR_STYLE_XSTYLELOADER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
#include "com/sun/star/io/IOException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace style {

class SAL_NO_VTABLE XStyleLoader : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL loadStylesFromURL( const ::rtl::OUString& URL, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aOptions ) /* throw (::css::io::IOException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::beans::PropertyValue > SAL_CALL getStyleLoaderOptions() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XStyleLoader() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::XStyleLoader const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::style::XStyleLoader > *) SAL_THROW(());

#endif
