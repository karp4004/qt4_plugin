#ifndef INCLUDED_COM_SUN_STAR_STYLE_BREAKTYPE_HPP
#define INCLUDED_COM_SUN_STAR_STYLE_BREAKTYPE_HPP

#include "sal/config.h"

#include "com/sun/star/style/BreakType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace style { namespace detail {

struct theBreakTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theBreakTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.style.BreakType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[7];
        ::rtl::OUString sEnumValue0( "NONE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "COLUMN_BEFORE" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "COLUMN_AFTER" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "COLUMN_BOTH" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "PAGE_BEFORE" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "PAGE_AFTER" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "PAGE_BOTH" );
        enumValueNames[6] = sEnumValue6.pData;

        sal_Int32 enumValues[7];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::style::BreakType_NONE,
            7, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace style {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::BreakType const *) {
    return *detail::theBreakTypeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::style::BreakType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::style::BreakType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_STYLE_BREAKTYPE_HPP
