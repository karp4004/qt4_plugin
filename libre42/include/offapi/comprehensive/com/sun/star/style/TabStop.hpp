#ifndef INCLUDED_COM_SUN_STAR_STYLE_TABSTOP_HPP
#define INCLUDED_COM_SUN_STAR_STYLE_TABSTOP_HPP

#include "sal/config.h"

#include "com/sun/star/style/TabStop.hdl"

#include "com/sun/star/style/TabAlign.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace style {

inline TabStop::TabStop() SAL_THROW(())
    : Position(0)
    , Alignment(::css::style::TabAlign_LEFT)
    , DecimalChar(0)
    , FillChar(0)
{
}

inline TabStop::TabStop(const ::sal_Int32& Position_, const ::css::style::TabAlign& Alignment_, const ::sal_Unicode& DecimalChar_, const ::sal_Unicode& FillChar_) SAL_THROW(())
    : Position(Position_)
    , Alignment(Alignment_)
    , DecimalChar(DecimalChar_)
    , FillChar(FillChar_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace style { namespace detail {

struct theTabStopType : public rtl::StaticWithInit< ::css::uno::Type *, theTabStopType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.style.TabStop" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Position" );
        ::cppu::UnoType< ::css::style::TabAlign >::get();
        ::rtl::OUString the_tname1( "com.sun.star.style.TabAlign" );
        ::rtl::OUString the_name1( "Alignment" );
        ::rtl::OUString the_tname2( "char" );
        ::rtl::OUString the_name2( "DecimalChar" );
        ::rtl::OUString the_name3( "FillChar" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_CHAR, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_CHAR, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace style {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::style::TabStop const *) {
    return *detail::theTabStopType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::style::TabStop const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::style::TabStop >::get();
}

#endif // INCLUDED_COM_SUN_STAR_STYLE_TABSTOP_HPP
