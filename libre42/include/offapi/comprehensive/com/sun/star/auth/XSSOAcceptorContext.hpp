#ifndef INCLUDED_COM_SUN_STAR_AUTH_XSSOACCEPTORCONTEXT_HPP
#define INCLUDED_COM_SUN_STAR_AUTH_XSSOACCEPTORCONTEXT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/auth/XSSOAcceptorContext.hdl"

#include "com/sun/star/auth/AuthenticationFailedException.hpp"
#include "com/sun/star/auth/InvalidArgumentException.hpp"
#include "com/sun/star/auth/InvalidContextException.hpp"
#include "com/sun/star/auth/InvalidCredentialException.hpp"
#include "com/sun/star/auth/XSSOContext.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace auth { namespace detail {

struct theXSSOAcceptorContextType : public rtl::StaticWithInit< ::css::uno::Type *, theXSSOAcceptorContextType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.auth.XSSOAcceptorContext" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::auth::XSSOContext > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[1] = { 0 };
        ::rtl::OUString sMethodName0( "com.sun.star.auth.XSSOAcceptorContext::accept" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            1,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace auth {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::auth::XSSOAcceptorContext const *) {
    const ::css::uno::Type &rRet = *detail::theXSSOAcceptorContextType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::auth::InvalidArgumentException >::get();
            ::cppu::UnoType< ::css::auth::InvalidCredentialException >::get();
            ::cppu::UnoType< ::css::auth::InvalidContextException >::get();
            ::cppu::UnoType< ::css::auth::AuthenticationFailedException >::get();

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Token" );
                ::rtl::OUString sParamType0( "[]byte" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.auth.InvalidArgumentException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.auth.InvalidCredentialException" );
                ::rtl::OUString the_ExceptionName2( "com.sun.star.auth.InvalidContextException" );
                ::rtl::OUString the_ExceptionName3( "com.sun.star.auth.AuthenticationFailedException" );
                ::rtl::OUString the_ExceptionName4( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData, the_ExceptionName2.pData, the_ExceptionName3.pData, the_ExceptionName4.pData };
                ::rtl::OUString sReturnType0( "[]byte" );
                ::rtl::OUString sMethodName0( "com.sun.star.auth.XSSOAcceptorContext::accept" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    7, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sReturnType0.pData,
                    1, aParameters,
                    5, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::auth::XSSOAcceptorContext > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::auth::XSSOAcceptorContext > >::get();
}

::css::uno::Type const & ::css::auth::XSSOAcceptorContext::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::auth::XSSOAcceptorContext > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_AUTH_XSSOACCEPTORCONTEXT_HPP
