#ifndef INCLUDED_COM_SUN_STAR_AUTH_XSSOMANAGERFACTORY_HDL
#define INCLUDED_COM_SUN_STAR_AUTH_XSSOMANAGERFACTORY_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace auth { class XSSOManager; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace auth {

class SAL_NO_VTABLE XSSOManagerFactory : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::auth::XSSOManager > SAL_CALL getSSOManager() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSSOManagerFactory() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::auth::XSSOManagerFactory const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::auth::XSSOManagerFactory > *) SAL_THROW(());

#endif
