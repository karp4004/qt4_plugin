#ifndef INCLUDED_COM_SUN_STAR_PRESENTATION_XSLIDESHOWCONTROLLER_HPP
#define INCLUDED_COM_SUN_STAR_PRESENTATION_XSLIDESHOWCONTROLLER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/presentation/XSlideShowController.hdl"

#include "com/sun/star/drawing/XDrawPage.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/IndexOutOfBoundsException.hpp"
#include "com/sun/star/presentation/XSlideShow.hpp"
#include "com/sun/star/presentation/XSlideShowListener.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace presentation { namespace detail {

struct theXSlideShowControllerType : public rtl::StaticWithInit< ::css::uno::Type *, theXSlideShowControllerType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.presentation.XSlideShowController" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[33] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.presentation.XSlideShowController::AlwaysOnTop" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.presentation.XSlideShowController::MouseVisible" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.presentation.XSlideShowController::UsePen" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.presentation.XSlideShowController::PenColor" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.presentation.XSlideShowController::PenWidth" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sMethodName0( "com.sun.star.presentation.XSlideShowController::isRunning" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );
        ::rtl::OUString sMethodName1( "com.sun.star.presentation.XSlideShowController::getSlideCount" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName1.pData );
        ::rtl::OUString sMethodName2( "com.sun.star.presentation.XSlideShowController::getSlideByIndex" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName2.pData );
        ::rtl::OUString sMethodName3( "com.sun.star.presentation.XSlideShowController::addSlideShowListener" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName3.pData );
        ::rtl::OUString sMethodName4( "com.sun.star.presentation.XSlideShowController::removeSlideShowListener" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName4.pData );
        ::rtl::OUString sMethodName5( "com.sun.star.presentation.XSlideShowController::gotoNextEffect" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName5.pData );
        ::rtl::OUString sMethodName6( "com.sun.star.presentation.XSlideShowController::gotoPreviousEffect" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName6.pData );
        ::rtl::OUString sMethodName7( "com.sun.star.presentation.XSlideShowController::gotoFirstSlide" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName7.pData );
        ::rtl::OUString sMethodName8( "com.sun.star.presentation.XSlideShowController::gotoNextSlide" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName8.pData );
        ::rtl::OUString sMethodName9( "com.sun.star.presentation.XSlideShowController::gotoPreviousSlide" );
        typelib_typedescriptionreference_new( &pMembers[14],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName9.pData );
        ::rtl::OUString sMethodName10( "com.sun.star.presentation.XSlideShowController::gotoLastSlide" );
        typelib_typedescriptionreference_new( &pMembers[15],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName10.pData );
        ::rtl::OUString sMethodName11( "com.sun.star.presentation.XSlideShowController::gotoBookmark" );
        typelib_typedescriptionreference_new( &pMembers[16],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName11.pData );
        ::rtl::OUString sMethodName12( "com.sun.star.presentation.XSlideShowController::gotoSlide" );
        typelib_typedescriptionreference_new( &pMembers[17],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName12.pData );
        ::rtl::OUString sMethodName13( "com.sun.star.presentation.XSlideShowController::gotoSlideIndex" );
        typelib_typedescriptionreference_new( &pMembers[18],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName13.pData );
        ::rtl::OUString sMethodName14( "com.sun.star.presentation.XSlideShowController::stopSound" );
        typelib_typedescriptionreference_new( &pMembers[19],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName14.pData );
        ::rtl::OUString sMethodName15( "com.sun.star.presentation.XSlideShowController::pause" );
        typelib_typedescriptionreference_new( &pMembers[20],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName15.pData );
        ::rtl::OUString sMethodName16( "com.sun.star.presentation.XSlideShowController::resume" );
        typelib_typedescriptionreference_new( &pMembers[21],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName16.pData );
        ::rtl::OUString sMethodName17( "com.sun.star.presentation.XSlideShowController::isPaused" );
        typelib_typedescriptionreference_new( &pMembers[22],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName17.pData );
        ::rtl::OUString sMethodName18( "com.sun.star.presentation.XSlideShowController::blankScreen" );
        typelib_typedescriptionreference_new( &pMembers[23],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName18.pData );
        ::rtl::OUString sMethodName19( "com.sun.star.presentation.XSlideShowController::activate" );
        typelib_typedescriptionreference_new( &pMembers[24],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName19.pData );
        ::rtl::OUString sMethodName20( "com.sun.star.presentation.XSlideShowController::deactivate" );
        typelib_typedescriptionreference_new( &pMembers[25],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName20.pData );
        ::rtl::OUString sMethodName21( "com.sun.star.presentation.XSlideShowController::isActive" );
        typelib_typedescriptionreference_new( &pMembers[26],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName21.pData );
        ::rtl::OUString sMethodName22( "com.sun.star.presentation.XSlideShowController::getCurrentSlide" );
        typelib_typedescriptionreference_new( &pMembers[27],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName22.pData );
        ::rtl::OUString sMethodName23( "com.sun.star.presentation.XSlideShowController::getCurrentSlideIndex" );
        typelib_typedescriptionreference_new( &pMembers[28],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName23.pData );
        ::rtl::OUString sMethodName24( "com.sun.star.presentation.XSlideShowController::getNextSlideIndex" );
        typelib_typedescriptionreference_new( &pMembers[29],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName24.pData );
        ::rtl::OUString sMethodName25( "com.sun.star.presentation.XSlideShowController::isEndless" );
        typelib_typedescriptionreference_new( &pMembers[30],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName25.pData );
        ::rtl::OUString sMethodName26( "com.sun.star.presentation.XSlideShowController::isFullScreen" );
        typelib_typedescriptionreference_new( &pMembers[31],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName26.pData );
        ::rtl::OUString sMethodName27( "com.sun.star.presentation.XSlideShowController::getSlideShow" );
        typelib_typedescriptionreference_new( &pMembers[32],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName27.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            33,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescriptionreference_release( pMembers[14] );
        typelib_typedescriptionreference_release( pMembers[15] );
        typelib_typedescriptionreference_release( pMembers[16] );
        typelib_typedescriptionreference_release( pMembers[17] );
        typelib_typedescriptionreference_release( pMembers[18] );
        typelib_typedescriptionreference_release( pMembers[19] );
        typelib_typedescriptionreference_release( pMembers[20] );
        typelib_typedescriptionreference_release( pMembers[21] );
        typelib_typedescriptionreference_release( pMembers[22] );
        typelib_typedescriptionreference_release( pMembers[23] );
        typelib_typedescriptionreference_release( pMembers[24] );
        typelib_typedescriptionreference_release( pMembers[25] );
        typelib_typedescriptionreference_release( pMembers[26] );
        typelib_typedescriptionreference_release( pMembers[27] );
        typelib_typedescriptionreference_release( pMembers[28] );
        typelib_typedescriptionreference_release( pMembers[29] );
        typelib_typedescriptionreference_release( pMembers[30] );
        typelib_typedescriptionreference_release( pMembers[31] );
        typelib_typedescriptionreference_release( pMembers[32] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace presentation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::presentation::XSlideShowController const *) {
    const ::css::uno::Type &rRet = *detail::theXSlideShowControllerType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::lang::IndexOutOfBoundsException >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "boolean" );
                ::rtl::OUString sAttributeName0( "com.sun.star.presentation.XSlideShowController::AlwaysOnTop" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    3, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "boolean" );
                ::rtl::OUString sAttributeName1( "com.sun.star.presentation.XSlideShowController::MouseVisible" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    4, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "boolean" );
                ::rtl::OUString sAttributeName2( "com.sun.star.presentation.XSlideShowController::UsePen" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    5, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "long" );
                ::rtl::OUString sAttributeName3( "com.sun.star.presentation.XSlideShowController::PenColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    6, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "double" );
                ::rtl::OUString sAttributeName4( "com.sun.star.presentation.XSlideShowController::PenWidth" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_DOUBLE, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType0( "boolean" );
                ::rtl::OUString sMethodName0( "com.sun.star.presentation.XSlideShowController::isRunning" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    8, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType0.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType1( "long" );
                ::rtl::OUString sMethodName1( "com.sun.star.presentation.XSlideShowController::getSlideCount" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    9, sal_False,
                    sMethodName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sReturnType1.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Index" );
                ::rtl::OUString sParamType0( "long" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.lang.IndexOutOfBoundsException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData };
                ::rtl::OUString sReturnType2( "com.sun.star.drawing.XDrawPage" );
                ::rtl::OUString sMethodName2( "com.sun.star.presentation.XSlideShowController::getSlideByIndex" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    10, sal_False,
                    sMethodName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sReturnType2.pData,
                    1, aParameters,
                    2, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Listener" );
                ::rtl::OUString sParamType0( "com.sun.star.presentation.XSlideShowListener" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType3( "void" );
                ::rtl::OUString sMethodName3( "com.sun.star.presentation.XSlideShowController::addSlideShowListener" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    11, sal_False,
                    sMethodName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType3.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Listener" );
                ::rtl::OUString sParamType0( "com.sun.star.presentation.XSlideShowListener" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType4( "void" );
                ::rtl::OUString sMethodName4( "com.sun.star.presentation.XSlideShowController::removeSlideShowListener" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    12, sal_False,
                    sMethodName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType4.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType5( "void" );
                ::rtl::OUString sMethodName5( "com.sun.star.presentation.XSlideShowController::gotoNextEffect" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    13, sal_False,
                    sMethodName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType5.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType6( "void" );
                ::rtl::OUString sMethodName6( "com.sun.star.presentation.XSlideShowController::gotoPreviousEffect" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    14, sal_False,
                    sMethodName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType6.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType7( "void" );
                ::rtl::OUString sMethodName7( "com.sun.star.presentation.XSlideShowController::gotoFirstSlide" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    15, sal_False,
                    sMethodName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType7.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType8( "void" );
                ::rtl::OUString sMethodName8( "com.sun.star.presentation.XSlideShowController::gotoNextSlide" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    16, sal_False,
                    sMethodName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType8.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType9( "void" );
                ::rtl::OUString sMethodName9( "com.sun.star.presentation.XSlideShowController::gotoPreviousSlide" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    17, sal_False,
                    sMethodName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType9.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType10( "void" );
                ::rtl::OUString sMethodName10( "com.sun.star.presentation.XSlideShowController::gotoLastSlide" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    18, sal_False,
                    sMethodName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType10.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Bookmark" );
                ::rtl::OUString sParamType0( "string" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType11( "void" );
                ::rtl::OUString sMethodName11( "com.sun.star.presentation.XSlideShowController::gotoBookmark" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    19, sal_False,
                    sMethodName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType11.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Page" );
                ::rtl::OUString sParamType0( "com.sun.star.drawing.XDrawPage" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData };
                ::rtl::OUString sReturnType12( "void" );
                ::rtl::OUString sMethodName12( "com.sun.star.presentation.XSlideShowController::gotoSlide" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    20, sal_False,
                    sMethodName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType12.pData,
                    1, aParameters,
                    2, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Index" );
                ::rtl::OUString sParamType0( "long" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType13( "void" );
                ::rtl::OUString sMethodName13( "com.sun.star.presentation.XSlideShowController::gotoSlideIndex" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    21, sal_False,
                    sMethodName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType13.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType14( "void" );
                ::rtl::OUString sMethodName14( "com.sun.star.presentation.XSlideShowController::stopSound" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    22, sal_False,
                    sMethodName14.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType14.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType15( "void" );
                ::rtl::OUString sMethodName15( "com.sun.star.presentation.XSlideShowController::pause" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    23, sal_False,
                    sMethodName15.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType15.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType16( "void" );
                ::rtl::OUString sMethodName16( "com.sun.star.presentation.XSlideShowController::resume" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    24, sal_False,
                    sMethodName16.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType16.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType17( "boolean" );
                ::rtl::OUString sMethodName17( "com.sun.star.presentation.XSlideShowController::isPaused" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    25, sal_False,
                    sMethodName17.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType17.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Color" );
                ::rtl::OUString sParamType0( "long" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType18( "void" );
                ::rtl::OUString sMethodName18( "com.sun.star.presentation.XSlideShowController::blankScreen" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    26, sal_False,
                    sMethodName18.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType18.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType19( "void" );
                ::rtl::OUString sMethodName19( "com.sun.star.presentation.XSlideShowController::activate" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    27, sal_False,
                    sMethodName19.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType19.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType20( "void" );
                ::rtl::OUString sMethodName20( "com.sun.star.presentation.XSlideShowController::deactivate" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    28, sal_False,
                    sMethodName20.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType20.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType21( "boolean" );
                ::rtl::OUString sMethodName21( "com.sun.star.presentation.XSlideShowController::isActive" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    29, sal_False,
                    sMethodName21.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType21.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType22( "com.sun.star.drawing.XDrawPage" );
                ::rtl::OUString sMethodName22( "com.sun.star.presentation.XSlideShowController::getCurrentSlide" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    30, sal_False,
                    sMethodName22.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sReturnType22.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType23( "long" );
                ::rtl::OUString sMethodName23( "com.sun.star.presentation.XSlideShowController::getCurrentSlideIndex" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    31, sal_False,
                    sMethodName23.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sReturnType23.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType24( "long" );
                ::rtl::OUString sMethodName24( "com.sun.star.presentation.XSlideShowController::getNextSlideIndex" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    32, sal_False,
                    sMethodName24.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sReturnType24.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType25( "boolean" );
                ::rtl::OUString sMethodName25( "com.sun.star.presentation.XSlideShowController::isEndless" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    33, sal_False,
                    sMethodName25.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType25.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType26( "boolean" );
                ::rtl::OUString sMethodName26( "com.sun.star.presentation.XSlideShowController::isFullScreen" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    34, sal_False,
                    sMethodName26.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType26.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType27( "com.sun.star.presentation.XSlideShow" );
                ::rtl::OUString sMethodName27( "com.sun.star.presentation.XSlideShowController::getSlideShow" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    35, sal_False,
                    sMethodName27.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sReturnType27.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::presentation::XSlideShowController > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::presentation::XSlideShowController > >::get();
}

::css::uno::Type const & ::css::presentation::XSlideShowController::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::presentation::XSlideShowController > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_PRESENTATION_XSLIDESHOWCONTROLLER_HPP
