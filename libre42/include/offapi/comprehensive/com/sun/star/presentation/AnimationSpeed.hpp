#ifndef INCLUDED_COM_SUN_STAR_PRESENTATION_ANIMATIONSPEED_HPP
#define INCLUDED_COM_SUN_STAR_PRESENTATION_ANIMATIONSPEED_HPP

#include "sal/config.h"

#include "com/sun/star/presentation/AnimationSpeed.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace presentation { namespace detail {

struct theAnimationSpeedType : public rtl::StaticWithInit< ::css::uno::Type *, theAnimationSpeedType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.presentation.AnimationSpeed" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[3];
        ::rtl::OUString sEnumValue0( "SLOW" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "MEDIUM" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "FAST" );
        enumValueNames[2] = sEnumValue2.pData;

        sal_Int32 enumValues[3];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::presentation::AnimationSpeed_SLOW,
            3, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace presentation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::presentation::AnimationSpeed const *) {
    return *detail::theAnimationSpeedType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::presentation::AnimationSpeed const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::presentation::AnimationSpeed >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PRESENTATION_ANIMATIONSPEED_HPP
