#ifndef INCLUDED_COM_SUN_STAR_PRESENTATION_PARAGRAPHTARGET_HPP
#define INCLUDED_COM_SUN_STAR_PRESENTATION_PARAGRAPHTARGET_HPP

#include "sal/config.h"

#include "com/sun/star/presentation/ParagraphTarget.hdl"

#include "com/sun/star/drawing/XShape.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace presentation {

inline ParagraphTarget::ParagraphTarget() SAL_THROW(())
    : Shape()
    , Paragraph(0)
{
}

inline ParagraphTarget::ParagraphTarget(const ::css::uno::Reference< ::css::drawing::XShape >& Shape_, const ::sal_Int16& Paragraph_) SAL_THROW(())
    : Shape(Shape_)
    , Paragraph(Paragraph_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace presentation { namespace detail {

struct theParagraphTargetType : public rtl::StaticWithInit< ::css::uno::Type *, theParagraphTargetType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.presentation.ParagraphTarget" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::drawing::XShape > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.XShape" );
        ::rtl::OUString the_name0( "Shape" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "Paragraph" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace presentation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::presentation::ParagraphTarget const *) {
    return *detail::theParagraphTargetType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::presentation::ParagraphTarget const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::presentation::ParagraphTarget >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PRESENTATION_PARAGRAPHTARGET_HPP
