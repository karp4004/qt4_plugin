#ifndef INCLUDED_COM_SUN_STAR_PRESENTATION_ANIMATIONEFFECT_HPP
#define INCLUDED_COM_SUN_STAR_PRESENTATION_ANIMATIONEFFECT_HPP

#include "sal/config.h"

#include "com/sun/star/presentation/AnimationEffect.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace presentation { namespace detail {

struct theAnimationEffectType : public rtl::StaticWithInit< ::css::uno::Type *, theAnimationEffectType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.presentation.AnimationEffect" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[112];
        ::rtl::OUString sEnumValue0( "NONE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "FADE_FROM_LEFT" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "FADE_FROM_TOP" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "FADE_FROM_RIGHT" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "FADE_FROM_BOTTOM" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "FADE_TO_CENTER" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "FADE_FROM_CENTER" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "MOVE_FROM_LEFT" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "MOVE_FROM_TOP" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "MOVE_FROM_RIGHT" );
        enumValueNames[9] = sEnumValue9.pData;
        ::rtl::OUString sEnumValue10( "MOVE_FROM_BOTTOM" );
        enumValueNames[10] = sEnumValue10.pData;
        ::rtl::OUString sEnumValue11( "VERTICAL_STRIPES" );
        enumValueNames[11] = sEnumValue11.pData;
        ::rtl::OUString sEnumValue12( "HORIZONTAL_STRIPES" );
        enumValueNames[12] = sEnumValue12.pData;
        ::rtl::OUString sEnumValue13( "CLOCKWISE" );
        enumValueNames[13] = sEnumValue13.pData;
        ::rtl::OUString sEnumValue14( "COUNTERCLOCKWISE" );
        enumValueNames[14] = sEnumValue14.pData;
        ::rtl::OUString sEnumValue15( "FADE_FROM_UPPERLEFT" );
        enumValueNames[15] = sEnumValue15.pData;
        ::rtl::OUString sEnumValue16( "FADE_FROM_UPPERRIGHT" );
        enumValueNames[16] = sEnumValue16.pData;
        ::rtl::OUString sEnumValue17( "FADE_FROM_LOWERLEFT" );
        enumValueNames[17] = sEnumValue17.pData;
        ::rtl::OUString sEnumValue18( "FADE_FROM_LOWERRIGHT" );
        enumValueNames[18] = sEnumValue18.pData;
        ::rtl::OUString sEnumValue19( "CLOSE_VERTICAL" );
        enumValueNames[19] = sEnumValue19.pData;
        ::rtl::OUString sEnumValue20( "CLOSE_HORIZONTAL" );
        enumValueNames[20] = sEnumValue20.pData;
        ::rtl::OUString sEnumValue21( "OPEN_VERTICAL" );
        enumValueNames[21] = sEnumValue21.pData;
        ::rtl::OUString sEnumValue22( "OPEN_HORIZONTAL" );
        enumValueNames[22] = sEnumValue22.pData;
        ::rtl::OUString sEnumValue23( "PATH" );
        enumValueNames[23] = sEnumValue23.pData;
        ::rtl::OUString sEnumValue24( "MOVE_TO_LEFT" );
        enumValueNames[24] = sEnumValue24.pData;
        ::rtl::OUString sEnumValue25( "MOVE_TO_TOP" );
        enumValueNames[25] = sEnumValue25.pData;
        ::rtl::OUString sEnumValue26( "MOVE_TO_RIGHT" );
        enumValueNames[26] = sEnumValue26.pData;
        ::rtl::OUString sEnumValue27( "MOVE_TO_BOTTOM" );
        enumValueNames[27] = sEnumValue27.pData;
        ::rtl::OUString sEnumValue28( "SPIRALIN_LEFT" );
        enumValueNames[28] = sEnumValue28.pData;
        ::rtl::OUString sEnumValue29( "SPIRALIN_RIGHT" );
        enumValueNames[29] = sEnumValue29.pData;
        ::rtl::OUString sEnumValue30( "SPIRALOUT_LEFT" );
        enumValueNames[30] = sEnumValue30.pData;
        ::rtl::OUString sEnumValue31( "SPIRALOUT_RIGHT" );
        enumValueNames[31] = sEnumValue31.pData;
        ::rtl::OUString sEnumValue32( "DISSOLVE" );
        enumValueNames[32] = sEnumValue32.pData;
        ::rtl::OUString sEnumValue33( "WAVYLINE_FROM_LEFT" );
        enumValueNames[33] = sEnumValue33.pData;
        ::rtl::OUString sEnumValue34( "WAVYLINE_FROM_TOP" );
        enumValueNames[34] = sEnumValue34.pData;
        ::rtl::OUString sEnumValue35( "WAVYLINE_FROM_RIGHT" );
        enumValueNames[35] = sEnumValue35.pData;
        ::rtl::OUString sEnumValue36( "WAVYLINE_FROM_BOTTOM" );
        enumValueNames[36] = sEnumValue36.pData;
        ::rtl::OUString sEnumValue37( "RANDOM" );
        enumValueNames[37] = sEnumValue37.pData;
        ::rtl::OUString sEnumValue38( "VERTICAL_LINES" );
        enumValueNames[38] = sEnumValue38.pData;
        ::rtl::OUString sEnumValue39( "HORIZONTAL_LINES" );
        enumValueNames[39] = sEnumValue39.pData;
        ::rtl::OUString sEnumValue40( "LASER_FROM_LEFT" );
        enumValueNames[40] = sEnumValue40.pData;
        ::rtl::OUString sEnumValue41( "LASER_FROM_TOP" );
        enumValueNames[41] = sEnumValue41.pData;
        ::rtl::OUString sEnumValue42( "LASER_FROM_RIGHT" );
        enumValueNames[42] = sEnumValue42.pData;
        ::rtl::OUString sEnumValue43( "LASER_FROM_BOTTOM" );
        enumValueNames[43] = sEnumValue43.pData;
        ::rtl::OUString sEnumValue44( "LASER_FROM_UPPERLEFT" );
        enumValueNames[44] = sEnumValue44.pData;
        ::rtl::OUString sEnumValue45( "LASER_FROM_UPPERRIGHT" );
        enumValueNames[45] = sEnumValue45.pData;
        ::rtl::OUString sEnumValue46( "LASER_FROM_LOWERLEFT" );
        enumValueNames[46] = sEnumValue46.pData;
        ::rtl::OUString sEnumValue47( "LASER_FROM_LOWERRIGHT" );
        enumValueNames[47] = sEnumValue47.pData;
        ::rtl::OUString sEnumValue48( "APPEAR" );
        enumValueNames[48] = sEnumValue48.pData;
        ::rtl::OUString sEnumValue49( "HIDE" );
        enumValueNames[49] = sEnumValue49.pData;
        ::rtl::OUString sEnumValue50( "MOVE_FROM_UPPERLEFT" );
        enumValueNames[50] = sEnumValue50.pData;
        ::rtl::OUString sEnumValue51( "MOVE_FROM_UPPERRIGHT" );
        enumValueNames[51] = sEnumValue51.pData;
        ::rtl::OUString sEnumValue52( "MOVE_FROM_LOWERRIGHT" );
        enumValueNames[52] = sEnumValue52.pData;
        ::rtl::OUString sEnumValue53( "MOVE_FROM_LOWERLEFT" );
        enumValueNames[53] = sEnumValue53.pData;
        ::rtl::OUString sEnumValue54( "MOVE_TO_UPPERLEFT" );
        enumValueNames[54] = sEnumValue54.pData;
        ::rtl::OUString sEnumValue55( "MOVE_TO_UPPERRIGHT" );
        enumValueNames[55] = sEnumValue55.pData;
        ::rtl::OUString sEnumValue56( "MOVE_TO_LOWERRIGHT" );
        enumValueNames[56] = sEnumValue56.pData;
        ::rtl::OUString sEnumValue57( "MOVE_TO_LOWERLEFT" );
        enumValueNames[57] = sEnumValue57.pData;
        ::rtl::OUString sEnumValue58( "MOVE_SHORT_FROM_LEFT" );
        enumValueNames[58] = sEnumValue58.pData;
        ::rtl::OUString sEnumValue59( "MOVE_SHORT_FROM_UPPERLEFT" );
        enumValueNames[59] = sEnumValue59.pData;
        ::rtl::OUString sEnumValue60( "MOVE_SHORT_FROM_TOP" );
        enumValueNames[60] = sEnumValue60.pData;
        ::rtl::OUString sEnumValue61( "MOVE_SHORT_FROM_UPPERRIGHT" );
        enumValueNames[61] = sEnumValue61.pData;
        ::rtl::OUString sEnumValue62( "MOVE_SHORT_FROM_RIGHT" );
        enumValueNames[62] = sEnumValue62.pData;
        ::rtl::OUString sEnumValue63( "MOVE_SHORT_FROM_LOWERRIGHT" );
        enumValueNames[63] = sEnumValue63.pData;
        ::rtl::OUString sEnumValue64( "MOVE_SHORT_FROM_BOTTOM" );
        enumValueNames[64] = sEnumValue64.pData;
        ::rtl::OUString sEnumValue65( "MOVE_SHORT_FROM_LOWERLEFT" );
        enumValueNames[65] = sEnumValue65.pData;
        ::rtl::OUString sEnumValue66( "MOVE_SHORT_TO_LEFT" );
        enumValueNames[66] = sEnumValue66.pData;
        ::rtl::OUString sEnumValue67( "MOVE_SHORT_TO_UPPERLEFT" );
        enumValueNames[67] = sEnumValue67.pData;
        ::rtl::OUString sEnumValue68( "MOVE_SHORT_TO_TOP" );
        enumValueNames[68] = sEnumValue68.pData;
        ::rtl::OUString sEnumValue69( "MOVE_SHORT_TO_UPPERRIGHT" );
        enumValueNames[69] = sEnumValue69.pData;
        ::rtl::OUString sEnumValue70( "MOVE_SHORT_TO_RIGHT" );
        enumValueNames[70] = sEnumValue70.pData;
        ::rtl::OUString sEnumValue71( "MOVE_SHORT_TO_LOWERRIGHT" );
        enumValueNames[71] = sEnumValue71.pData;
        ::rtl::OUString sEnumValue72( "MOVE_SHORT_TO_BOTTOM" );
        enumValueNames[72] = sEnumValue72.pData;
        ::rtl::OUString sEnumValue73( "MOVE_SHORT_TO_LOWERLEFT" );
        enumValueNames[73] = sEnumValue73.pData;
        ::rtl::OUString sEnumValue74( "VERTICAL_CHECKERBOARD" );
        enumValueNames[74] = sEnumValue74.pData;
        ::rtl::OUString sEnumValue75( "HORIZONTAL_CHECKERBOARD" );
        enumValueNames[75] = sEnumValue75.pData;
        ::rtl::OUString sEnumValue76( "HORIZONTAL_ROTATE" );
        enumValueNames[76] = sEnumValue76.pData;
        ::rtl::OUString sEnumValue77( "VERTICAL_ROTATE" );
        enumValueNames[77] = sEnumValue77.pData;
        ::rtl::OUString sEnumValue78( "HORIZONTAL_STRETCH" );
        enumValueNames[78] = sEnumValue78.pData;
        ::rtl::OUString sEnumValue79( "VERTICAL_STRETCH" );
        enumValueNames[79] = sEnumValue79.pData;
        ::rtl::OUString sEnumValue80( "STRETCH_FROM_LEFT" );
        enumValueNames[80] = sEnumValue80.pData;
        ::rtl::OUString sEnumValue81( "STRETCH_FROM_UPPERLEFT" );
        enumValueNames[81] = sEnumValue81.pData;
        ::rtl::OUString sEnumValue82( "STRETCH_FROM_TOP" );
        enumValueNames[82] = sEnumValue82.pData;
        ::rtl::OUString sEnumValue83( "STRETCH_FROM_UPPERRIGHT" );
        enumValueNames[83] = sEnumValue83.pData;
        ::rtl::OUString sEnumValue84( "STRETCH_FROM_RIGHT" );
        enumValueNames[84] = sEnumValue84.pData;
        ::rtl::OUString sEnumValue85( "STRETCH_FROM_LOWERRIGHT" );
        enumValueNames[85] = sEnumValue85.pData;
        ::rtl::OUString sEnumValue86( "STRETCH_FROM_BOTTOM" );
        enumValueNames[86] = sEnumValue86.pData;
        ::rtl::OUString sEnumValue87( "STRETCH_FROM_LOWERLEFT" );
        enumValueNames[87] = sEnumValue87.pData;
        ::rtl::OUString sEnumValue88( "ZOOM_IN" );
        enumValueNames[88] = sEnumValue88.pData;
        ::rtl::OUString sEnumValue89( "ZOOM_IN_SMALL" );
        enumValueNames[89] = sEnumValue89.pData;
        ::rtl::OUString sEnumValue90( "ZOOM_IN_SPIRAL" );
        enumValueNames[90] = sEnumValue90.pData;
        ::rtl::OUString sEnumValue91( "ZOOM_OUT" );
        enumValueNames[91] = sEnumValue91.pData;
        ::rtl::OUString sEnumValue92( "ZOOM_OUT_SMALL" );
        enumValueNames[92] = sEnumValue92.pData;
        ::rtl::OUString sEnumValue93( "ZOOM_OUT_SPIRAL" );
        enumValueNames[93] = sEnumValue93.pData;
        ::rtl::OUString sEnumValue94( "ZOOM_IN_FROM_LEFT" );
        enumValueNames[94] = sEnumValue94.pData;
        ::rtl::OUString sEnumValue95( "ZOOM_IN_FROM_UPPERLEFT" );
        enumValueNames[95] = sEnumValue95.pData;
        ::rtl::OUString sEnumValue96( "ZOOM_IN_FROM_TOP" );
        enumValueNames[96] = sEnumValue96.pData;
        ::rtl::OUString sEnumValue97( "ZOOM_IN_FROM_UPPERRIGHT" );
        enumValueNames[97] = sEnumValue97.pData;
        ::rtl::OUString sEnumValue98( "ZOOM_IN_FROM_RIGHT" );
        enumValueNames[98] = sEnumValue98.pData;
        ::rtl::OUString sEnumValue99( "ZOOM_IN_FROM_LOWERRIGHT" );
        enumValueNames[99] = sEnumValue99.pData;
        ::rtl::OUString sEnumValue100( "ZOOM_IN_FROM_BOTTOM" );
        enumValueNames[100] = sEnumValue100.pData;
        ::rtl::OUString sEnumValue101( "ZOOM_IN_FROM_LOWERLEFT" );
        enumValueNames[101] = sEnumValue101.pData;
        ::rtl::OUString sEnumValue102( "ZOOM_IN_FROM_CENTER" );
        enumValueNames[102] = sEnumValue102.pData;
        ::rtl::OUString sEnumValue103( "ZOOM_OUT_FROM_LEFT" );
        enumValueNames[103] = sEnumValue103.pData;
        ::rtl::OUString sEnumValue104( "ZOOM_OUT_FROM_UPPERLEFT" );
        enumValueNames[104] = sEnumValue104.pData;
        ::rtl::OUString sEnumValue105( "ZOOM_OUT_FROM_TOP" );
        enumValueNames[105] = sEnumValue105.pData;
        ::rtl::OUString sEnumValue106( "ZOOM_OUT_FROM_UPPERRIGHT" );
        enumValueNames[106] = sEnumValue106.pData;
        ::rtl::OUString sEnumValue107( "ZOOM_OUT_FROM_RIGHT" );
        enumValueNames[107] = sEnumValue107.pData;
        ::rtl::OUString sEnumValue108( "ZOOM_OUT_FROM_LOWERRIGHT" );
        enumValueNames[108] = sEnumValue108.pData;
        ::rtl::OUString sEnumValue109( "ZOOM_OUT_FROM_BOTTOM" );
        enumValueNames[109] = sEnumValue109.pData;
        ::rtl::OUString sEnumValue110( "ZOOM_OUT_FROM_LOWERLEFT" );
        enumValueNames[110] = sEnumValue110.pData;
        ::rtl::OUString sEnumValue111( "ZOOM_OUT_FROM_CENTER" );
        enumValueNames[111] = sEnumValue111.pData;

        sal_Int32 enumValues[112];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;
        enumValues[9] = 9;
        enumValues[10] = 10;
        enumValues[11] = 11;
        enumValues[12] = 12;
        enumValues[13] = 13;
        enumValues[14] = 14;
        enumValues[15] = 15;
        enumValues[16] = 16;
        enumValues[17] = 17;
        enumValues[18] = 18;
        enumValues[19] = 19;
        enumValues[20] = 20;
        enumValues[21] = 21;
        enumValues[22] = 22;
        enumValues[23] = 23;
        enumValues[24] = 24;
        enumValues[25] = 25;
        enumValues[26] = 26;
        enumValues[27] = 27;
        enumValues[28] = 28;
        enumValues[29] = 29;
        enumValues[30] = 30;
        enumValues[31] = 31;
        enumValues[32] = 32;
        enumValues[33] = 33;
        enumValues[34] = 34;
        enumValues[35] = 35;
        enumValues[36] = 36;
        enumValues[37] = 37;
        enumValues[38] = 38;
        enumValues[39] = 39;
        enumValues[40] = 40;
        enumValues[41] = 41;
        enumValues[42] = 42;
        enumValues[43] = 43;
        enumValues[44] = 44;
        enumValues[45] = 45;
        enumValues[46] = 46;
        enumValues[47] = 47;
        enumValues[48] = 48;
        enumValues[49] = 49;
        enumValues[50] = 50;
        enumValues[51] = 51;
        enumValues[52] = 52;
        enumValues[53] = 53;
        enumValues[54] = 54;
        enumValues[55] = 55;
        enumValues[56] = 56;
        enumValues[57] = 57;
        enumValues[58] = 58;
        enumValues[59] = 59;
        enumValues[60] = 60;
        enumValues[61] = 61;
        enumValues[62] = 62;
        enumValues[63] = 63;
        enumValues[64] = 64;
        enumValues[65] = 65;
        enumValues[66] = 66;
        enumValues[67] = 67;
        enumValues[68] = 68;
        enumValues[69] = 69;
        enumValues[70] = 70;
        enumValues[71] = 71;
        enumValues[72] = 72;
        enumValues[73] = 73;
        enumValues[74] = 74;
        enumValues[75] = 75;
        enumValues[76] = 76;
        enumValues[77] = 77;
        enumValues[78] = 78;
        enumValues[79] = 79;
        enumValues[80] = 80;
        enumValues[81] = 81;
        enumValues[82] = 82;
        enumValues[83] = 83;
        enumValues[84] = 84;
        enumValues[85] = 85;
        enumValues[86] = 86;
        enumValues[87] = 87;
        enumValues[88] = 88;
        enumValues[89] = 89;
        enumValues[90] = 90;
        enumValues[91] = 91;
        enumValues[92] = 92;
        enumValues[93] = 93;
        enumValues[94] = 94;
        enumValues[95] = 95;
        enumValues[96] = 96;
        enumValues[97] = 97;
        enumValues[98] = 98;
        enumValues[99] = 99;
        enumValues[100] = 100;
        enumValues[101] = 101;
        enumValues[102] = 102;
        enumValues[103] = 103;
        enumValues[104] = 104;
        enumValues[105] = 105;
        enumValues[106] = 106;
        enumValues[107] = 107;
        enumValues[108] = 108;
        enumValues[109] = 109;
        enumValues[110] = 110;
        enumValues[111] = 111;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::presentation::AnimationEffect_NONE,
            112, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace presentation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::presentation::AnimationEffect const *) {
    return *detail::theAnimationEffectType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::presentation::AnimationEffect const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::presentation::AnimationEffect >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PRESENTATION_ANIMATIONEFFECT_HPP
