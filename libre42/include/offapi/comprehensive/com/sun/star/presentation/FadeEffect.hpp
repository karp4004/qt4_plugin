#ifndef INCLUDED_COM_SUN_STAR_PRESENTATION_FADEEFFECT_HPP
#define INCLUDED_COM_SUN_STAR_PRESENTATION_FADEEFFECT_HPP

#include "sal/config.h"

#include "com/sun/star/presentation/FadeEffect.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace presentation { namespace detail {

struct theFadeEffectType : public rtl::StaticWithInit< ::css::uno::Type *, theFadeEffectType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.presentation.FadeEffect" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[57];
        ::rtl::OUString sEnumValue0( "NONE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "FADE_FROM_LEFT" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "FADE_FROM_TOP" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "FADE_FROM_RIGHT" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "FADE_FROM_BOTTOM" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "FADE_TO_CENTER" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "FADE_FROM_CENTER" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "MOVE_FROM_LEFT" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "MOVE_FROM_TOP" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "MOVE_FROM_RIGHT" );
        enumValueNames[9] = sEnumValue9.pData;
        ::rtl::OUString sEnumValue10( "MOVE_FROM_BOTTOM" );
        enumValueNames[10] = sEnumValue10.pData;
        ::rtl::OUString sEnumValue11( "ROLL_FROM_LEFT" );
        enumValueNames[11] = sEnumValue11.pData;
        ::rtl::OUString sEnumValue12( "ROLL_FROM_TOP" );
        enumValueNames[12] = sEnumValue12.pData;
        ::rtl::OUString sEnumValue13( "ROLL_FROM_RIGHT" );
        enumValueNames[13] = sEnumValue13.pData;
        ::rtl::OUString sEnumValue14( "ROLL_FROM_BOTTOM" );
        enumValueNames[14] = sEnumValue14.pData;
        ::rtl::OUString sEnumValue15( "VERTICAL_STRIPES" );
        enumValueNames[15] = sEnumValue15.pData;
        ::rtl::OUString sEnumValue16( "HORIZONTAL_STRIPES" );
        enumValueNames[16] = sEnumValue16.pData;
        ::rtl::OUString sEnumValue17( "CLOCKWISE" );
        enumValueNames[17] = sEnumValue17.pData;
        ::rtl::OUString sEnumValue18( "COUNTERCLOCKWISE" );
        enumValueNames[18] = sEnumValue18.pData;
        ::rtl::OUString sEnumValue19( "FADE_FROM_UPPERLEFT" );
        enumValueNames[19] = sEnumValue19.pData;
        ::rtl::OUString sEnumValue20( "FADE_FROM_UPPERRIGHT" );
        enumValueNames[20] = sEnumValue20.pData;
        ::rtl::OUString sEnumValue21( "FADE_FROM_LOWERLEFT" );
        enumValueNames[21] = sEnumValue21.pData;
        ::rtl::OUString sEnumValue22( "FADE_FROM_LOWERRIGHT" );
        enumValueNames[22] = sEnumValue22.pData;
        ::rtl::OUString sEnumValue23( "CLOSE_VERTICAL" );
        enumValueNames[23] = sEnumValue23.pData;
        ::rtl::OUString sEnumValue24( "CLOSE_HORIZONTAL" );
        enumValueNames[24] = sEnumValue24.pData;
        ::rtl::OUString sEnumValue25( "OPEN_VERTICAL" );
        enumValueNames[25] = sEnumValue25.pData;
        ::rtl::OUString sEnumValue26( "OPEN_HORIZONTAL" );
        enumValueNames[26] = sEnumValue26.pData;
        ::rtl::OUString sEnumValue27( "SPIRALIN_LEFT" );
        enumValueNames[27] = sEnumValue27.pData;
        ::rtl::OUString sEnumValue28( "SPIRALIN_RIGHT" );
        enumValueNames[28] = sEnumValue28.pData;
        ::rtl::OUString sEnumValue29( "SPIRALOUT_LEFT" );
        enumValueNames[29] = sEnumValue29.pData;
        ::rtl::OUString sEnumValue30( "SPIRALOUT_RIGHT" );
        enumValueNames[30] = sEnumValue30.pData;
        ::rtl::OUString sEnumValue31( "DISSOLVE" );
        enumValueNames[31] = sEnumValue31.pData;
        ::rtl::OUString sEnumValue32( "WAVYLINE_FROM_LEFT" );
        enumValueNames[32] = sEnumValue32.pData;
        ::rtl::OUString sEnumValue33( "WAVYLINE_FROM_TOP" );
        enumValueNames[33] = sEnumValue33.pData;
        ::rtl::OUString sEnumValue34( "WAVYLINE_FROM_RIGHT" );
        enumValueNames[34] = sEnumValue34.pData;
        ::rtl::OUString sEnumValue35( "WAVYLINE_FROM_BOTTOM" );
        enumValueNames[35] = sEnumValue35.pData;
        ::rtl::OUString sEnumValue36( "RANDOM" );
        enumValueNames[36] = sEnumValue36.pData;
        ::rtl::OUString sEnumValue37( "STRETCH_FROM_LEFT" );
        enumValueNames[37] = sEnumValue37.pData;
        ::rtl::OUString sEnumValue38( "STRETCH_FROM_TOP" );
        enumValueNames[38] = sEnumValue38.pData;
        ::rtl::OUString sEnumValue39( "STRETCH_FROM_RIGHT" );
        enumValueNames[39] = sEnumValue39.pData;
        ::rtl::OUString sEnumValue40( "STRETCH_FROM_BOTTOM" );
        enumValueNames[40] = sEnumValue40.pData;
        ::rtl::OUString sEnumValue41( "VERTICAL_LINES" );
        enumValueNames[41] = sEnumValue41.pData;
        ::rtl::OUString sEnumValue42( "HORIZONTAL_LINES" );
        enumValueNames[42] = sEnumValue42.pData;
        ::rtl::OUString sEnumValue43( "MOVE_FROM_UPPERLEFT" );
        enumValueNames[43] = sEnumValue43.pData;
        ::rtl::OUString sEnumValue44( "MOVE_FROM_UPPERRIGHT" );
        enumValueNames[44] = sEnumValue44.pData;
        ::rtl::OUString sEnumValue45( "MOVE_FROM_LOWERRIGHT" );
        enumValueNames[45] = sEnumValue45.pData;
        ::rtl::OUString sEnumValue46( "MOVE_FROM_LOWERLEFT" );
        enumValueNames[46] = sEnumValue46.pData;
        ::rtl::OUString sEnumValue47( "UNCOVER_TO_LEFT" );
        enumValueNames[47] = sEnumValue47.pData;
        ::rtl::OUString sEnumValue48( "UNCOVER_TO_UPPERLEFT" );
        enumValueNames[48] = sEnumValue48.pData;
        ::rtl::OUString sEnumValue49( "UNCOVER_TO_TOP" );
        enumValueNames[49] = sEnumValue49.pData;
        ::rtl::OUString sEnumValue50( "UNCOVER_TO_UPPERRIGHT" );
        enumValueNames[50] = sEnumValue50.pData;
        ::rtl::OUString sEnumValue51( "UNCOVER_TO_RIGHT" );
        enumValueNames[51] = sEnumValue51.pData;
        ::rtl::OUString sEnumValue52( "UNCOVER_TO_LOWERRIGHT" );
        enumValueNames[52] = sEnumValue52.pData;
        ::rtl::OUString sEnumValue53( "UNCOVER_TO_BOTTOM" );
        enumValueNames[53] = sEnumValue53.pData;
        ::rtl::OUString sEnumValue54( "UNCOVER_TO_LOWERLEFT" );
        enumValueNames[54] = sEnumValue54.pData;
        ::rtl::OUString sEnumValue55( "VERTICAL_CHECKERBOARD" );
        enumValueNames[55] = sEnumValue55.pData;
        ::rtl::OUString sEnumValue56( "HORIZONTAL_CHECKERBOARD" );
        enumValueNames[56] = sEnumValue56.pData;

        sal_Int32 enumValues[57];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;
        enumValues[9] = 9;
        enumValues[10] = 10;
        enumValues[11] = 11;
        enumValues[12] = 12;
        enumValues[13] = 13;
        enumValues[14] = 14;
        enumValues[15] = 15;
        enumValues[16] = 16;
        enumValues[17] = 17;
        enumValues[18] = 18;
        enumValues[19] = 19;
        enumValues[20] = 20;
        enumValues[21] = 21;
        enumValues[22] = 22;
        enumValues[23] = 23;
        enumValues[24] = 24;
        enumValues[25] = 25;
        enumValues[26] = 26;
        enumValues[27] = 27;
        enumValues[28] = 28;
        enumValues[29] = 29;
        enumValues[30] = 30;
        enumValues[31] = 31;
        enumValues[32] = 32;
        enumValues[33] = 33;
        enumValues[34] = 34;
        enumValues[35] = 35;
        enumValues[36] = 36;
        enumValues[37] = 37;
        enumValues[38] = 38;
        enumValues[39] = 39;
        enumValues[40] = 40;
        enumValues[41] = 41;
        enumValues[42] = 42;
        enumValues[43] = 43;
        enumValues[44] = 44;
        enumValues[45] = 45;
        enumValues[46] = 46;
        enumValues[47] = 47;
        enumValues[48] = 48;
        enumValues[49] = 49;
        enumValues[50] = 50;
        enumValues[51] = 51;
        enumValues[52] = 52;
        enumValues[53] = 53;
        enumValues[54] = 54;
        enumValues[55] = 55;
        enumValues[56] = 56;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::presentation::FadeEffect_NONE,
            57, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace presentation {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::presentation::FadeEffect const *) {
    return *detail::theFadeEffectType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::presentation::FadeEffect const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::presentation::FadeEffect >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PRESENTATION_FADEEFFECT_HPP
