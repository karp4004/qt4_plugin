#ifndef INCLUDED_COM_SUN_STAR_INSPECTION_LINEDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_INSPECTION_LINEDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/inspection/LineDescriptor.hdl"

#include "com/sun/star/graphic/XGraphic.hpp"
#include "com/sun/star/inspection/XPropertyControl.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace inspection {

inline LineDescriptor::LineDescriptor() SAL_THROW(())
    : DisplayName()
    , Control()
    , HelpURL()
    , HasPrimaryButton(false)
    , PrimaryButtonId()
    , PrimaryButtonImageURL()
    , PrimaryButtonImage()
    , HasSecondaryButton(false)
    , SecondaryButtonId()
    , SecondaryButtonImageURL()
    , SecondaryButtonImage()
    , IndentLevel(0)
    , Category()
{
}

inline LineDescriptor::LineDescriptor(const ::rtl::OUString& DisplayName_, const ::css::uno::Reference< ::css::inspection::XPropertyControl >& Control_, const ::rtl::OUString& HelpURL_, const ::sal_Bool& HasPrimaryButton_, const ::rtl::OUString& PrimaryButtonId_, const ::rtl::OUString& PrimaryButtonImageURL_, const ::css::uno::Reference< ::css::graphic::XGraphic >& PrimaryButtonImage_, const ::sal_Bool& HasSecondaryButton_, const ::rtl::OUString& SecondaryButtonId_, const ::rtl::OUString& SecondaryButtonImageURL_, const ::css::uno::Reference< ::css::graphic::XGraphic >& SecondaryButtonImage_, const ::sal_Int16& IndentLevel_, const ::rtl::OUString& Category_) SAL_THROW(())
    : DisplayName(DisplayName_)
    , Control(Control_)
    , HelpURL(HelpURL_)
    , HasPrimaryButton(HasPrimaryButton_)
    , PrimaryButtonId(PrimaryButtonId_)
    , PrimaryButtonImageURL(PrimaryButtonImageURL_)
    , PrimaryButtonImage(PrimaryButtonImage_)
    , HasSecondaryButton(HasSecondaryButton_)
    , SecondaryButtonId(SecondaryButtonId_)
    , SecondaryButtonImageURL(SecondaryButtonImageURL_)
    , SecondaryButtonImage(SecondaryButtonImage_)
    , IndentLevel(IndentLevel_)
    , Category(Category_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace inspection { namespace detail {

struct theLineDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, theLineDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.inspection.LineDescriptor" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "DisplayName" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::inspection::XPropertyControl > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.inspection.XPropertyControl" );
        ::rtl::OUString the_name1( "Control" );
        ::rtl::OUString the_name2( "HelpURL" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name3( "HasPrimaryButton" );
        ::rtl::OUString the_name4( "PrimaryButtonId" );
        ::rtl::OUString the_name5( "PrimaryButtonImageURL" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::graphic::XGraphic > >::get();
        ::rtl::OUString the_tname3( "com.sun.star.graphic.XGraphic" );
        ::rtl::OUString the_name6( "PrimaryButtonImage" );
        ::rtl::OUString the_name7( "HasSecondaryButton" );
        ::rtl::OUString the_name8( "SecondaryButtonId" );
        ::rtl::OUString the_name9( "SecondaryButtonImageURL" );
        ::rtl::OUString the_name10( "SecondaryButtonImage" );
        ::rtl::OUString the_tname4( "short" );
        ::rtl::OUString the_name11( "IndentLevel" );
        ::rtl::OUString the_name12( "Category" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name7.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name8.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name9.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name10.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname4.pData, the_name11.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name12.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 13, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace inspection {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::inspection::LineDescriptor const *) {
    return *detail::theLineDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::inspection::LineDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::inspection::LineDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_INSPECTION_LINEDESCRIPTOR_HPP
