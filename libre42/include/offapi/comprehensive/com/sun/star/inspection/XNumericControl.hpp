#ifndef INCLUDED_COM_SUN_STAR_INSPECTION_XNUMERICCONTROL_HPP
#define INCLUDED_COM_SUN_STAR_INSPECTION_XNUMERICCONTROL_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/inspection/XNumericControl.hdl"

#include "com/sun/star/beans/Optional.hpp"
#include "com/sun/star/inspection/XPropertyControl.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace inspection { namespace detail {

struct theXNumericControlType : public rtl::StaticWithInit< ::css::uno::Type *, theXNumericControlType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.inspection.XNumericControl" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::inspection::XPropertyControl > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[5] = { 0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.inspection.XNumericControl::DecimalDigits" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.inspection.XNumericControl::MinValue" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.inspection.XNumericControl::MaxValue" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.inspection.XNumericControl::DisplayUnit" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.inspection.XNumericControl::ValueUnit" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            5,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace inspection {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::inspection::XNumericControl const *) {
    const ::css::uno::Type &rRet = *detail::theXNumericControlType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::beans::Optional< double > >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "short" );
                ::rtl::OUString sAttributeName0( "com.sun.star.inspection.XNumericControl::DecimalDigits" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    10, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "com.sun.star.beans.Optional<double>" );
                ::rtl::OUString sAttributeName1( "com.sun.star.inspection.XNumericControl::MinValue" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    11, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "com.sun.star.beans.Optional<double>" );
                ::rtl::OUString sAttributeName2( "com.sun.star.inspection.XNumericControl::MaxValue" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    12, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "short" );
                ::rtl::OUString sAttributeName3( "com.sun.star.inspection.XNumericControl::DisplayUnit" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    13, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType3.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "short" );
                ::rtl::OUString sAttributeName4( "com.sun.star.inspection.XNumericControl::ValueUnit" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    14, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::inspection::XNumericControl > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::inspection::XNumericControl > >::get();
}

::css::uno::Type const & ::css::inspection::XNumericControl::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::inspection::XNumericControl > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_INSPECTION_XNUMERICCONTROL_HPP
