#ifndef INCLUDED_COM_SUN_STAR_INSPECTION_XSTRINGREPRESENTATION_HDL
#define INCLUDED_COM_SUN_STAR_INSPECTION_XSTRINGREPRESENTATION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Type.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace inspection {

class SAL_NO_VTABLE XStringRepresentation : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::rtl::OUString SAL_CALL convertToControlValue( const ::css::uno::Any& PropertyValue ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL convertToPropertyValue( const ::rtl::OUString& ControlValue, const ::css::uno::Type& ControlValueType ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XStringRepresentation() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::inspection::XStringRepresentation const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::inspection::XStringRepresentation > *) SAL_THROW(());

#endif
