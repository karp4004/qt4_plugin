#ifndef INCLUDED_COM_SUN_STAR_INSPECTION_XPROPERTYHANDLER_HDL
#define INCLUDED_COM_SUN_STAR_INSPECTION_XPROPERTYHANDLER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/Property.hdl"
#include "com/sun/star/beans/PropertyState.hdl"
#include "com/sun/star/beans/PropertyVetoException.hdl"
#include "com/sun/star/beans/UnknownPropertyException.hdl"
namespace com { namespace sun { namespace star { namespace beans { class XPropertyChangeListener; } } } }
#include "com/sun/star/inspection/InteractiveSelectionResult.hdl"
#include "com/sun/star/inspection/LineDescriptor.hdl"
namespace com { namespace sun { namespace star { namespace inspection { class XObjectInspectorUI; } } } }
namespace com { namespace sun { namespace star { namespace inspection { class XPropertyControlFactory; } } } }
#include "com/sun/star/lang/NullPointerException.hdl"
#include "com/sun/star/lang/XComponent.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
namespace com { namespace sun { namespace star { namespace uno { class XInterface; } } } }
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "com/sun/star/uno/Type.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace inspection {

class SAL_NO_VTABLE XPropertyHandler : public ::css::lang::XComponent
{
public:

    // Methods
    virtual void SAL_CALL inspect( const ::css::uno::Reference< ::css::uno::XInterface >& Component ) /* throw (::css::lang::NullPointerException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL getPropertyValue( const ::rtl::OUString& PropertyName ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPropertyValue( const ::rtl::OUString& PropertyName, const ::css::uno::Any& Value ) /* throw (::css::beans::UnknownPropertyException, ::css::beans::PropertyVetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::beans::PropertyState SAL_CALL getPropertyState( const ::rtl::OUString& PropertyName ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::inspection::LineDescriptor SAL_CALL describePropertyLine( const ::rtl::OUString& PropertyName, const ::css::uno::Reference< ::css::inspection::XPropertyControlFactory >& ControlFactory ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::NullPointerException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL convertToPropertyValue( const ::rtl::OUString& PropertyName, const ::css::uno::Any& ControlValue ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL convertToControlValue( const ::rtl::OUString& PropertyName, const ::css::uno::Any& PropertyValue, const ::css::uno::Type& ControlValueType ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addPropertyChangeListener( const ::css::uno::Reference< ::css::beans::XPropertyChangeListener >& Listener ) /* throw (::css::lang::NullPointerException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removePropertyChangeListener( const ::css::uno::Reference< ::css::beans::XPropertyChangeListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::beans::Property > SAL_CALL getSupportedProperties() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getSupersededProperties() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getActuatingProperties() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isComposable( const ::rtl::OUString& PropertyName ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::inspection::InteractiveSelectionResult SAL_CALL onInteractivePropertySelection( const ::rtl::OUString& PropertyName, ::sal_Bool Primary, ::css::uno::Any& outData, const ::css::uno::Reference< ::css::inspection::XObjectInspectorUI >& InspectorUI ) /* throw (::css::beans::UnknownPropertyException, ::css::lang::NullPointerException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL actuatingPropertyChanged( const ::rtl::OUString& ActuatingPropertyName, const ::css::uno::Any& NewValue, const ::css::uno::Any& OldValue, const ::css::uno::Reference< ::css::inspection::XObjectInspectorUI >& InspectorUI, ::sal_Bool FirstTimeInit ) /* throw (::css::lang::NullPointerException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL suspend( ::sal_Bool Suspend ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPropertyHandler() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::inspection::XPropertyHandler const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::inspection::XPropertyHandler > *) SAL_THROW(());

#endif
