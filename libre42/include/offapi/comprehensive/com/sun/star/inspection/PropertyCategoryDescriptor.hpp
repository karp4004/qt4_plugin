#ifndef INCLUDED_COM_SUN_STAR_INSPECTION_PROPERTYCATEGORYDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_INSPECTION_PROPERTYCATEGORYDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/inspection/PropertyCategoryDescriptor.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace inspection {

inline PropertyCategoryDescriptor::PropertyCategoryDescriptor() SAL_THROW(())
    : ProgrammaticName()
    , UIName()
    , HelpURL()
{
}

inline PropertyCategoryDescriptor::PropertyCategoryDescriptor(const ::rtl::OUString& ProgrammaticName_, const ::rtl::OUString& UIName_, const ::rtl::OUString& HelpURL_) SAL_THROW(())
    : ProgrammaticName(ProgrammaticName_)
    , UIName(UIName_)
    , HelpURL(HelpURL_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace inspection { namespace detail {

struct thePropertyCategoryDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, thePropertyCategoryDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.inspection.PropertyCategoryDescriptor" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "ProgrammaticName" );
        ::rtl::OUString the_name1( "UIName" );
        ::rtl::OUString the_name2( "HelpURL" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace inspection {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::inspection::PropertyCategoryDescriptor const *) {
    return *detail::thePropertyCategoryDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::inspection::PropertyCategoryDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::inspection::PropertyCategoryDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_INSPECTION_PROPERTYCATEGORYDESCRIPTOR_HPP
