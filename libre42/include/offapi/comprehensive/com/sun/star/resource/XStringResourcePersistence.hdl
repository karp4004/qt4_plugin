#ifndef INCLUDED_COM_SUN_STAR_RESOURCE_XSTRINGRESOURCEPERSISTENCE_HDL
#define INCLUDED_COM_SUN_STAR_RESOURCE_XSTRINGRESOURCEPERSISTENCE_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace embed { class XStorage; } } } }
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/NoSupportException.hdl"
#include "com/sun/star/resource/XStringResourceManager.hdl"
namespace com { namespace sun { namespace star { namespace task { class XInteractionHandler; } } } }
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace resource {

class SAL_NO_VTABLE XStringResourcePersistence : public ::css::resource::XStringResourceManager
{
public:

    // Methods
    virtual void SAL_CALL store() /* throw (::css::lang::NoSupportException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isModified() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setComment( const ::rtl::OUString& Comment ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL storeToStorage( const ::css::uno::Reference< ::css::embed::XStorage >& Storage, const ::rtl::OUString& BaseName, const ::rtl::OUString& Comment ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL storeToURL( const ::rtl::OUString& URL, const ::rtl::OUString& BaseName, const ::rtl::OUString& Comment, const ::css::uno::Reference< ::css::task::XInteractionHandler >& Handler ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int8 > SAL_CALL exportBinary() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL importBinary( const ::css::uno::Sequence< ::sal_Int8 >& Data ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XStringResourcePersistence() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::resource::XStringResourcePersistence const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::resource::XStringResourcePersistence > *) SAL_THROW(());

#endif
