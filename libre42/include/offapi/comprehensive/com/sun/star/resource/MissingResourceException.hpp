#ifndef INCLUDED_COM_SUN_STAR_RESOURCE_MISSINGRESOURCEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_RESOURCE_MISSINGRESOURCEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/resource/MissingResourceException.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace resource {

inline MissingResourceException::MissingResourceException() SAL_THROW(())
    : ::css::uno::RuntimeException()
{
    ::cppu::UnoType< ::css::resource::MissingResourceException >::get();
}

inline MissingResourceException::MissingResourceException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
{
    ::cppu::UnoType< ::css::resource::MissingResourceException >::get();
}

MissingResourceException::MissingResourceException(MissingResourceException const & the_other): ::css::uno::RuntimeException(the_other) {}

MissingResourceException::~MissingResourceException() {}

MissingResourceException & MissingResourceException::operator =(MissingResourceException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace resource { namespace detail {

struct theMissingResourceExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theMissingResourceExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.resource.MissingResourceException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::RuntimeException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace resource {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::resource::MissingResourceException const *) {
    return *detail::theMissingResourceExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::resource::MissingResourceException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::resource::MissingResourceException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RESOURCE_MISSINGRESOURCEEXCEPTION_HPP
