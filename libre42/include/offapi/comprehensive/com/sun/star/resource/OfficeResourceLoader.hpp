#ifndef INCLUDED_COM_SUN_STAR_RESOURCE_OFFICERESOURCELOADER_HPP
#define INCLUDED_COM_SUN_STAR_RESOURCE_OFFICERESOURCELOADER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/resource/XResourceBundleLoader.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace resource {

class OfficeResourceLoader {
public:
    static ::css::uno::Reference< ::css::resource::XResourceBundleLoader > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::resource::XResourceBundleLoader > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.resource.OfficeResourceLoader" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.resource.OfficeResourceLoader of type com.sun.star.resource.XResourceBundleLoader" ), the_context);
        }
        return instance;
    }

private:
    OfficeResourceLoader(); // not implemented
    OfficeResourceLoader(OfficeResourceLoader &); // not implemented
    ~OfficeResourceLoader(); // not implemented
    void operator =(OfficeResourceLoader); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_RESOURCE_OFFICERESOURCELOADER_HPP
