#ifndef INCLUDED_COM_SUN_STAR_XFORMS_INVALIDDATAONSUBMITEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XFORMS_INVALIDDATAONSUBMITEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xforms/InvalidDataOnSubmitException.hdl"

#include "com/sun/star/util/VetoException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xforms {

inline InvalidDataOnSubmitException::InvalidDataOnSubmitException() SAL_THROW(())
    : ::css::util::VetoException()
{
    ::cppu::UnoType< ::css::xforms::InvalidDataOnSubmitException >::get();
}

inline InvalidDataOnSubmitException::InvalidDataOnSubmitException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::util::VetoException(Message_, Context_)
{
    ::cppu::UnoType< ::css::xforms::InvalidDataOnSubmitException >::get();
}

InvalidDataOnSubmitException::InvalidDataOnSubmitException(InvalidDataOnSubmitException const & the_other): ::css::util::VetoException(the_other) {}

InvalidDataOnSubmitException::~InvalidDataOnSubmitException() {}

InvalidDataOnSubmitException & InvalidDataOnSubmitException::operator =(InvalidDataOnSubmitException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::util::VetoException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace xforms { namespace detail {

struct theInvalidDataOnSubmitExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInvalidDataOnSubmitExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xforms.InvalidDataOnSubmitException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::util::VetoException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace xforms {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xforms::InvalidDataOnSubmitException const *) {
    return *detail::theInvalidDataOnSubmitExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xforms::InvalidDataOnSubmitException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xforms::InvalidDataOnSubmitException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XFORMS_INVALIDDATAONSUBMITEXCEPTION_HPP
