#ifndef INCLUDED_COM_SUN_STAR_XFORMS_XDATATYPEREPOSITORY_HDL
#define INCLUDED_COM_SUN_STAR_XFORMS_XDATATYPEREPOSITORY_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/ElementExistException.hdl"
#include "com/sun/star/container/NoSuchElementException.hdl"
#include "com/sun/star/container/XEnumerationAccess.hdl"
#include "com/sun/star/container/XNameAccess.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/util/VetoException.hdl"
namespace com { namespace sun { namespace star { namespace xsd { class XDataType; } } } }
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xforms {

class SAL_NO_VTABLE XDataTypeRepository : public ::css::container::XEnumerationAccess, public ::css::container::XNameAccess
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::xsd::XDataType > SAL_CALL getBasicDataType( ::sal_Int16 dataTypeClass ) /* throw (::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xsd::XDataType > SAL_CALL cloneDataType( const ::rtl::OUString& sourceName, const ::rtl::OUString& newName ) /* throw (::css::container::NoSuchElementException, ::css::container::ElementExistException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL revokeDataType( const ::rtl::OUString& typeName ) /* throw (::css::container::NoSuchElementException, ::css::util::VetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xsd::XDataType > SAL_CALL getDataType( const ::rtl::OUString& typeName ) /* throw (::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDataTypeRepository() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xforms::XDataTypeRepository const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xforms::XDataTypeRepository > *) SAL_THROW(());

#endif
