#ifndef INCLUDED_COM_SUN_STAR_RENDERING_XCOLORSPACE_HDL
#define INCLUDED_COM_SUN_STAR_RENDERING_XCOLORSPACE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/rendering/ARGBColor.hdl"
#include "com/sun/star/rendering/ColorComponent.hdl"
#include "com/sun/star/rendering/RGBColor.hdl"
namespace com { namespace sun { namespace star { namespace rendering { class XColorSpace; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace rendering {

class SAL_NO_VTABLE XColorSpace : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Int8 SAL_CALL getType() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int8 > SAL_CALL getComponentTags() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int8 SAL_CALL getRenderingIntent() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::beans::PropertyValue > SAL_CALL getProperties() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< double > SAL_CALL convertColorSpace( const ::css::uno::Sequence< double >& deviceColor, const ::css::uno::Reference< ::css::rendering::XColorSpace >& targetColorSpace ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::rendering::RGBColor > SAL_CALL convertToRGB( const ::css::uno::Sequence< double >& deviceColor ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::rendering::ARGBColor > SAL_CALL convertToARGB( const ::css::uno::Sequence< double >& deviceColor ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::rendering::ARGBColor > SAL_CALL convertToPARGB( const ::css::uno::Sequence< double >& deviceColor ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< double > SAL_CALL convertFromRGB( const ::css::uno::Sequence< ::css::rendering::RGBColor >& rgbColor ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< double > SAL_CALL convertFromARGB( const ::css::uno::Sequence< ::css::rendering::ARGBColor >& rgbColor ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< double > SAL_CALL convertFromPARGB( const ::css::uno::Sequence< ::css::rendering::ARGBColor >& rgbColor ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XColorSpace() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::XColorSpace const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::rendering::XColorSpace > *) SAL_THROW(());

#endif
