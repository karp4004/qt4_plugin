#ifndef INCLUDED_COM_SUN_STAR_RENDERING_ARGBCOLOR_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_ARGBCOLOR_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/ARGBColor.hdl"

#include "com/sun/star/rendering/ColorComponent.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline ARGBColor::ARGBColor() SAL_THROW(())
    : Alpha(0)
    , Red(0)
    , Green(0)
    , Blue(0)
{
}

inline ARGBColor::ARGBColor(const double& Alpha_, const double& Red_, const double& Green_, const double& Blue_) SAL_THROW(())
    : Alpha(Alpha_)
    , Red(Red_)
    , Green(Green_)
    , Blue(Blue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theARGBColorType : public rtl::StaticWithInit< ::css::uno::Type *, theARGBColorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.ARGBColor" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Alpha" );
        ::rtl::OUString the_name1( "Red" );
        ::rtl::OUString the_name2( "Green" );
        ::rtl::OUString the_name3( "Blue" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::ARGBColor const *) {
    return *detail::theARGBColorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::ARGBColor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::ARGBColor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_ARGBCOLOR_HPP
