#ifndef INCLUDED_COM_SUN_STAR_RENDERING_FONTREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_FONTREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/FontRequest.hdl"

#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/rendering/FontInfo.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline FontRequest::FontRequest() SAL_THROW(())
    : FontDescription()
    , CellSize(0)
    , ReferenceAdvancement(0)
    , Locale()
{
}

inline FontRequest::FontRequest(const ::css::rendering::FontInfo& FontDescription_, const double& CellSize_, const double& ReferenceAdvancement_, const ::css::lang::Locale& Locale_) SAL_THROW(())
    : FontDescription(FontDescription_)
    , CellSize(CellSize_)
    , ReferenceAdvancement(ReferenceAdvancement_)
    , Locale(Locale_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theFontRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theFontRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.FontRequest" );
        ::cppu::UnoType< ::css::rendering::FontInfo >::get();
        ::rtl::OUString the_tname0( "com.sun.star.rendering.FontInfo" );
        ::rtl::OUString the_name0( "FontDescription" );
        ::rtl::OUString the_tname1( "double" );
        ::rtl::OUString the_name1( "CellSize" );
        ::rtl::OUString the_name2( "ReferenceAdvancement" );
        ::cppu::UnoType< ::css::lang::Locale >::get();
        ::rtl::OUString the_tname2( "com.sun.star.lang.Locale" );
        ::rtl::OUString the_name3( "Locale" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::FontRequest const *) {
    return *detail::theFontRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::FontRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::FontRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_FONTREQUEST_HPP
