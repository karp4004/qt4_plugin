#ifndef INCLUDED_COM_SUN_STAR_RENDERING_CARET_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_CARET_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/Caret.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline Caret::Caret() SAL_THROW(())
    : MainCaretIndex(0)
    , SecondaryCaretIndex(0)
    , CaretAngle(0)
{
}

inline Caret::Caret(const ::sal_Int32& MainCaretIndex_, const ::sal_Int32& SecondaryCaretIndex_, const double& CaretAngle_) SAL_THROW(())
    : MainCaretIndex(MainCaretIndex_)
    , SecondaryCaretIndex(SecondaryCaretIndex_)
    , CaretAngle(CaretAngle_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theCaretType : public rtl::StaticWithInit< ::css::uno::Type *, theCaretType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.Caret" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "MainCaretIndex" );
        ::rtl::OUString the_name1( "SecondaryCaretIndex" );
        ::rtl::OUString the_tname1( "double" );
        ::rtl::OUString the_name2( "CaretAngle" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::Caret const *) {
    return *detail::theCaretType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::Caret const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::Caret >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_CARET_HPP
