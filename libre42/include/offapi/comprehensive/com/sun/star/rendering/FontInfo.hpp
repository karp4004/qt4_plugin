#ifndef INCLUDED_COM_SUN_STAR_RENDERING_FONTINFO_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_FONTINFO_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/FontInfo.hdl"

#include "com/sun/star/rendering/Panose.hpp"
#include "com/sun/star/util/TriState.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline FontInfo::FontInfo() SAL_THROW(())
    : FontDescription()
    , FamilyName()
    , StyleName()
    , UnicodeRanges0(0)
    , UnicodeRanges1(0)
    , UnicodeRanges2(0)
    , UnicodeRanges3(0)
    , IsSymbolFont(::css::util::TriState_NO)
    , IsVertical(::css::util::TriState_NO)
{
}

inline FontInfo::FontInfo(const ::css::rendering::Panose& FontDescription_, const ::rtl::OUString& FamilyName_, const ::rtl::OUString& StyleName_, const ::sal_Int32& UnicodeRanges0_, const ::sal_Int32& UnicodeRanges1_, const ::sal_Int32& UnicodeRanges2_, const ::sal_Int32& UnicodeRanges3_, const ::css::util::TriState& IsSymbolFont_, const ::css::util::TriState& IsVertical_) SAL_THROW(())
    : FontDescription(FontDescription_)
    , FamilyName(FamilyName_)
    , StyleName(StyleName_)
    , UnicodeRanges0(UnicodeRanges0_)
    , UnicodeRanges1(UnicodeRanges1_)
    , UnicodeRanges2(UnicodeRanges2_)
    , UnicodeRanges3(UnicodeRanges3_)
    , IsSymbolFont(IsSymbolFont_)
    , IsVertical(IsVertical_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theFontInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theFontInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.FontInfo" );
        ::cppu::UnoType< ::css::rendering::Panose >::get();
        ::rtl::OUString the_tname0( "com.sun.star.rendering.Panose" );
        ::rtl::OUString the_name0( "FontDescription" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "FamilyName" );
        ::rtl::OUString the_name2( "StyleName" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name3( "UnicodeRanges0" );
        ::rtl::OUString the_name4( "UnicodeRanges1" );
        ::rtl::OUString the_name5( "UnicodeRanges2" );
        ::rtl::OUString the_name6( "UnicodeRanges3" );
        ::cppu::UnoType< ::css::util::TriState >::get();
        ::rtl::OUString the_tname3( "com.sun.star.util.TriState" );
        ::rtl::OUString the_name7( "IsSymbolFont" );
        ::rtl::OUString the_name8( "IsVertical" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name6.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname3.pData, the_name7.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname3.pData, the_name8.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 9, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::FontInfo const *) {
    return *detail::theFontInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::FontInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::FontInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_FONTINFO_HPP
