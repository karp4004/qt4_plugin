#ifndef INCLUDED_COM_SUN_STAR_RENDERING_STRINGCONTEXT_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_STRINGCONTEXT_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/StringContext.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline StringContext::StringContext() SAL_THROW(())
    : Text()
    , StartPosition(0)
    , Length(0)
{
}

inline StringContext::StringContext(const ::rtl::OUString& Text_, const ::sal_Int32& StartPosition_, const ::sal_Int32& Length_) SAL_THROW(())
    : Text(Text_)
    , StartPosition(StartPosition_)
    , Length(Length_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theStringContextType : public rtl::StaticWithInit< ::css::uno::Type *, theStringContextType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.StringContext" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Text" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "StartPosition" );
        ::rtl::OUString the_name2( "Length" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::StringContext const *) {
    return *detail::theStringContextType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::StringContext const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::StringContext >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_STRINGCONTEXT_HPP
