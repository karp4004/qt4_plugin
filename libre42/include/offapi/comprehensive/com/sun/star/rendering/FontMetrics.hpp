#ifndef INCLUDED_COM_SUN_STAR_RENDERING_FONTMETRICS_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_FONTMETRICS_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/FontMetrics.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline FontMetrics::FontMetrics() SAL_THROW(())
    : Ascent(0)
    , Descent(0)
    , InternalLeading(0)
    , ExternalLeading(0)
    , ReferenceCharSize(0)
    , UnderlineOffset(0)
    , StrikeThroughOffset(0)
{
}

inline FontMetrics::FontMetrics(const double& Ascent_, const double& Descent_, const double& InternalLeading_, const double& ExternalLeading_, const double& ReferenceCharSize_, const double& UnderlineOffset_, const double& StrikeThroughOffset_) SAL_THROW(())
    : Ascent(Ascent_)
    , Descent(Descent_)
    , InternalLeading(InternalLeading_)
    , ExternalLeading(ExternalLeading_)
    , ReferenceCharSize(ReferenceCharSize_)
    , UnderlineOffset(UnderlineOffset_)
    , StrikeThroughOffset(StrikeThroughOffset_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theFontMetricsType : public rtl::StaticWithInit< ::css::uno::Type *, theFontMetricsType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.FontMetrics" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Ascent" );
        ::rtl::OUString the_name1( "Descent" );
        ::rtl::OUString the_name2( "InternalLeading" );
        ::rtl::OUString the_name3( "ExternalLeading" );
        ::rtl::OUString the_name4( "ReferenceCharSize" );
        ::rtl::OUString the_name5( "UnderlineOffset" );
        ::rtl::OUString the_name6( "StrikeThroughOffset" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::FontMetrics const *) {
    return *detail::theFontMetricsType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::FontMetrics const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::FontMetrics >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_FONTMETRICS_HPP
