#ifndef INCLUDED_COM_SUN_STAR_RENDERING_PANOSE_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_PANOSE_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/Panose.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline Panose::Panose() SAL_THROW(())
    : FamilyType(0)
    , SerifStyle(0)
    , Weight(0)
    , Proportion(0)
    , Contrast(0)
    , StrokeVariation(0)
    , ArmStyle(0)
    , Letterform(0)
    , Midline(0)
    , XHeight(0)
{
}

inline Panose::Panose(const ::sal_Int8& FamilyType_, const ::sal_Int8& SerifStyle_, const ::sal_Int8& Weight_, const ::sal_Int8& Proportion_, const ::sal_Int8& Contrast_, const ::sal_Int8& StrokeVariation_, const ::sal_Int8& ArmStyle_, const ::sal_Int8& Letterform_, const ::sal_Int8& Midline_, const ::sal_Int8& XHeight_) SAL_THROW(())
    : FamilyType(FamilyType_)
    , SerifStyle(SerifStyle_)
    , Weight(Weight_)
    , Proportion(Proportion_)
    , Contrast(Contrast_)
    , StrokeVariation(StrokeVariation_)
    , ArmStyle(ArmStyle_)
    , Letterform(Letterform_)
    , Midline(Midline_)
    , XHeight(XHeight_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct thePanoseType : public rtl::StaticWithInit< ::css::uno::Type *, thePanoseType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.Panose" );
        ::rtl::OUString the_tname0( "byte" );
        ::rtl::OUString the_name0( "FamilyType" );
        ::rtl::OUString the_name1( "SerifStyle" );
        ::rtl::OUString the_name2( "Weight" );
        ::rtl::OUString the_name3( "Proportion" );
        ::rtl::OUString the_name4( "Contrast" );
        ::rtl::OUString the_name5( "StrokeVariation" );
        ::rtl::OUString the_name6( "ArmStyle" );
        ::rtl::OUString the_name7( "Letterform" );
        ::rtl::OUString the_name8( "Midline" );
        ::rtl::OUString the_name9( "XHeight" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name7.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name8.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name9.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 10, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::Panose const *) {
    return *detail::thePanoseType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::Panose const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::Panose >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_PANOSE_HPP
