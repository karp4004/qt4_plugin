#ifndef INCLUDED_COM_SUN_STAR_RENDERING_TEXTURE_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_TEXTURE_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/Texture.hdl"

#include "com/sun/star/geometry/AffineMatrix2D.hpp"
#include "com/sun/star/rendering/StrokeAttributes.hpp"
#include "com/sun/star/rendering/XBitmap.hpp"
#include "com/sun/star/rendering/XParametricPolyPolygon2D.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline Texture::Texture() SAL_THROW(())
    : AffineTransform()
    , Alpha(0)
    , NumberOfHatchPolygons(0)
    , Bitmap()
    , Gradient()
    , Hatching()
    , HatchAttributes()
    , RepeatModeX(0)
    , RepeatModeY(0)
{
}

inline Texture::Texture(const ::css::geometry::AffineMatrix2D& AffineTransform_, const double& Alpha_, const ::sal_Int32& NumberOfHatchPolygons_, const ::css::uno::Reference< ::css::rendering::XBitmap >& Bitmap_, const ::css::uno::Reference< ::css::rendering::XParametricPolyPolygon2D >& Gradient_, const ::css::uno::Reference< ::css::rendering::XParametricPolyPolygon2D >& Hatching_, const ::css::rendering::StrokeAttributes& HatchAttributes_, const ::sal_Int8& RepeatModeX_, const ::sal_Int8& RepeatModeY_) SAL_THROW(())
    : AffineTransform(AffineTransform_)
    , Alpha(Alpha_)
    , NumberOfHatchPolygons(NumberOfHatchPolygons_)
    , Bitmap(Bitmap_)
    , Gradient(Gradient_)
    , Hatching(Hatching_)
    , HatchAttributes(HatchAttributes_)
    , RepeatModeX(RepeatModeX_)
    , RepeatModeY(RepeatModeY_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theTextureType : public rtl::StaticWithInit< ::css::uno::Type *, theTextureType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.Texture" );
        ::cppu::UnoType< ::css::geometry::AffineMatrix2D >::get();
        ::rtl::OUString the_tname0( "com.sun.star.geometry.AffineMatrix2D" );
        ::rtl::OUString the_name0( "AffineTransform" );
        ::rtl::OUString the_tname1( "double" );
        ::rtl::OUString the_name1( "Alpha" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name2( "NumberOfHatchPolygons" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XBitmap > >::get();
        ::rtl::OUString the_tname3( "com.sun.star.rendering.XBitmap" );
        ::rtl::OUString the_name3( "Bitmap" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XParametricPolyPolygon2D > >::get();
        ::rtl::OUString the_tname4( "com.sun.star.rendering.XParametricPolyPolygon2D" );
        ::rtl::OUString the_name4( "Gradient" );
        ::rtl::OUString the_name5( "Hatching" );
        ::cppu::UnoType< ::css::rendering::StrokeAttributes >::get();
        ::rtl::OUString the_tname5( "com.sun.star.rendering.StrokeAttributes" );
        ::rtl::OUString the_name6( "HatchAttributes" );
        ::rtl::OUString the_tname6( "byte" );
        ::rtl::OUString the_name7( "RepeatModeX" );
        ::rtl::OUString the_name8( "RepeatModeY" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name3.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname4.pData, the_name4.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname4.pData, the_name5.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname5.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname6.pData, the_name7.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname6.pData, the_name8.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 9, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::Texture const *) {
    return *detail::theTextureType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::Texture const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::Texture >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_TEXTURE_HPP
