#ifndef INCLUDED_COM_SUN_STAR_RENDERING_RGBCOLOR_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_RGBCOLOR_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/RGBColor.hdl"

#include "com/sun/star/rendering/ColorComponent.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline RGBColor::RGBColor() SAL_THROW(())
    : Red(0)
    , Green(0)
    , Blue(0)
{
}

inline RGBColor::RGBColor(const double& Red_, const double& Green_, const double& Blue_) SAL_THROW(())
    : Red(Red_)
    , Green(Green_)
    , Blue(Blue_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theRGBColorType : public rtl::StaticWithInit< ::css::uno::Type *, theRGBColorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.RGBColor" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Red" );
        ::rtl::OUString the_name1( "Green" );
        ::rtl::OUString the_name2( "Blue" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::RGBColor const *) {
    return *detail::theRGBColorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::RGBColor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::RGBColor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_RGBCOLOR_HPP
