#ifndef INCLUDED_COM_SUN_STAR_RENDERING_COLORPROFILE_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_COLORPROFILE_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/ColorProfile.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline ColorProfile::ColorProfile() SAL_THROW(())
    : dummy(0)
{
}

inline ColorProfile::ColorProfile(const ::sal_Int8& dummy_) SAL_THROW(())
    : dummy(dummy_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theColorProfileType : public rtl::StaticWithInit< ::css::uno::Type *, theColorProfileType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.ColorProfile" );
        ::rtl::OUString the_tname0( "byte" );
        ::rtl::OUString the_name0( "dummy" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::ColorProfile const *) {
    return *detail::theColorProfileType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::ColorProfile const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::ColorProfile >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_COLORPROFILE_HPP
