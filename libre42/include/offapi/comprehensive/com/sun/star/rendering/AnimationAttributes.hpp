#ifndef INCLUDED_COM_SUN_STAR_RENDERING_ANIMATIONATTRIBUTES_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_ANIMATIONATTRIBUTES_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/AnimationAttributes.hdl"

#include "com/sun/star/geometry/RealSize2D.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline AnimationAttributes::AnimationAttributes() SAL_THROW(())
    : Duration(0)
    , RepeatMode(0)
    , UntransformedSize()
{
}

inline AnimationAttributes::AnimationAttributes(const double& Duration_, const ::sal_Int8& RepeatMode_, const ::css::geometry::RealSize2D& UntransformedSize_) SAL_THROW(())
    : Duration(Duration_)
    , RepeatMode(RepeatMode_)
    , UntransformedSize(UntransformedSize_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theAnimationAttributesType : public rtl::StaticWithInit< ::css::uno::Type *, theAnimationAttributesType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.AnimationAttributes" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Duration" );
        ::rtl::OUString the_tname1( "byte" );
        ::rtl::OUString the_name1( "RepeatMode" );
        ::cppu::UnoType< ::css::geometry::RealSize2D >::get();
        ::rtl::OUString the_tname2( "com.sun.star.geometry.RealSize2D" );
        ::rtl::OUString the_name2( "UntransformedSize" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::AnimationAttributes const *) {
    return *detail::theAnimationAttributesType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::AnimationAttributes const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::AnimationAttributes >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_ANIMATIONATTRIBUTES_HPP
