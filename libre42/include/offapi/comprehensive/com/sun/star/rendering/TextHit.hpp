#ifndef INCLUDED_COM_SUN_STAR_RENDERING_TEXTHIT_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_TEXTHIT_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/TextHit.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline TextHit::TextHit() SAL_THROW(())
    : EntryIndex(0)
    , IsLeadingEdge(false)
{
}

inline TextHit::TextHit(const ::sal_Int32& EntryIndex_, const ::sal_Bool& IsLeadingEdge_) SAL_THROW(())
    : EntryIndex(EntryIndex_)
    , IsLeadingEdge(IsLeadingEdge_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theTextHitType : public rtl::StaticWithInit< ::css::uno::Type *, theTextHitType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.TextHit" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "EntryIndex" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "IsLeadingEdge" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::TextHit const *) {
    return *detail::theTextHitType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::TextHit const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::TextHit >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_TEXTHIT_HPP
