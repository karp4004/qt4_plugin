#ifndef INCLUDED_COM_SUN_STAR_RENDERING_INTEGERBITMAPLAYOUT_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_INTEGERBITMAPLAYOUT_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/IntegerBitmapLayout.hdl"

#include "com/sun/star/rendering/XBitmapPalette.hpp"
#include "com/sun/star/rendering/XIntegerBitmapColorSpace.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline IntegerBitmapLayout::IntegerBitmapLayout() SAL_THROW(())
    : ScanLines(0)
    , ScanLineBytes(0)
    , ScanLineStride(0)
    , PlaneStride(0)
    , ColorSpace()
    , Palette()
    , IsMsbFirst(false)
{
}

inline IntegerBitmapLayout::IntegerBitmapLayout(const ::sal_Int32& ScanLines_, const ::sal_Int32& ScanLineBytes_, const ::sal_Int32& ScanLineStride_, const ::sal_Int32& PlaneStride_, const ::css::uno::Reference< ::css::rendering::XIntegerBitmapColorSpace >& ColorSpace_, const ::css::uno::Reference< ::css::rendering::XBitmapPalette >& Palette_, const ::sal_Bool& IsMsbFirst_) SAL_THROW(())
    : ScanLines(ScanLines_)
    , ScanLineBytes(ScanLineBytes_)
    , ScanLineStride(ScanLineStride_)
    , PlaneStride(PlaneStride_)
    , ColorSpace(ColorSpace_)
    , Palette(Palette_)
    , IsMsbFirst(IsMsbFirst_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theIntegerBitmapLayoutType : public rtl::StaticWithInit< ::css::uno::Type *, theIntegerBitmapLayoutType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.IntegerBitmapLayout" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "ScanLines" );
        ::rtl::OUString the_name1( "ScanLineBytes" );
        ::rtl::OUString the_name2( "ScanLineStride" );
        ::rtl::OUString the_name3( "PlaneStride" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XIntegerBitmapColorSpace > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.rendering.XIntegerBitmapColorSpace" );
        ::rtl::OUString the_name4( "ColorSpace" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XBitmapPalette > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.rendering.XBitmapPalette" );
        ::rtl::OUString the_name5( "Palette" );
        ::rtl::OUString the_tname3( "boolean" );
        ::rtl::OUString the_name6( "IsMsbFirst" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname3.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::IntegerBitmapLayout const *) {
    return *detail::theIntegerBitmapLayoutType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::IntegerBitmapLayout const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::IntegerBitmapLayout >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_INTEGERBITMAPLAYOUT_HPP
