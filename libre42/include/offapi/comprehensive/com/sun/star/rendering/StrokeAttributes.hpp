#ifndef INCLUDED_COM_SUN_STAR_RENDERING_STROKEATTRIBUTES_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_STROKEATTRIBUTES_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/StrokeAttributes.hdl"

#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline StrokeAttributes::StrokeAttributes() SAL_THROW(())
    : StrokeWidth(0)
    , MiterLimit(0)
    , DashArray()
    , LineArray()
    , StartCapType(0)
    , EndCapType(0)
    , JoinType(0)
{
}

inline StrokeAttributes::StrokeAttributes(const double& StrokeWidth_, const double& MiterLimit_, const ::css::uno::Sequence< double >& DashArray_, const ::css::uno::Sequence< double >& LineArray_, const ::sal_Int8& StartCapType_, const ::sal_Int8& EndCapType_, const ::sal_Int8& JoinType_) SAL_THROW(())
    : StrokeWidth(StrokeWidth_)
    , MiterLimit(MiterLimit_)
    , DashArray(DashArray_)
    , LineArray(LineArray_)
    , StartCapType(StartCapType_)
    , EndCapType(EndCapType_)
    , JoinType(JoinType_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theStrokeAttributesType : public rtl::StaticWithInit< ::css::uno::Type *, theStrokeAttributesType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.StrokeAttributes" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "StrokeWidth" );
        ::rtl::OUString the_name1( "MiterLimit" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< double > >::get();
        ::rtl::OUString the_tname1( "[]double" );
        ::rtl::OUString the_name2( "DashArray" );
        ::rtl::OUString the_name3( "LineArray" );
        ::rtl::OUString the_tname2( "byte" );
        ::rtl::OUString the_name4( "StartCapType" );
        ::rtl::OUString the_name5( "EndCapType" );
        ::rtl::OUString the_name6( "JoinType" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname2.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname2.pData, the_name6.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 7, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::StrokeAttributes const *) {
    return *detail::theStrokeAttributesType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::StrokeAttributes const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::StrokeAttributes >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_STROKEATTRIBUTES_HPP
