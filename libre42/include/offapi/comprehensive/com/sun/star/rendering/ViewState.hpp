#ifndef INCLUDED_COM_SUN_STAR_RENDERING_VIEWSTATE_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_VIEWSTATE_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/ViewState.hdl"

#include "com/sun/star/geometry/AffineMatrix2D.hpp"
#include "com/sun/star/rendering/XPolyPolygon2D.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline ViewState::ViewState() SAL_THROW(())
    : AffineTransform()
    , Clip()
{
}

inline ViewState::ViewState(const ::css::geometry::AffineMatrix2D& AffineTransform_, const ::css::uno::Reference< ::css::rendering::XPolyPolygon2D >& Clip_) SAL_THROW(())
    : AffineTransform(AffineTransform_)
    , Clip(Clip_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theViewStateType : public rtl::StaticWithInit< ::css::uno::Type *, theViewStateType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.ViewState" );
        ::cppu::UnoType< ::css::geometry::AffineMatrix2D >::get();
        ::rtl::OUString the_tname0( "com.sun.star.geometry.AffineMatrix2D" );
        ::rtl::OUString the_name0( "AffineTransform" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XPolyPolygon2D > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.rendering.XPolyPolygon2D" );
        ::rtl::OUString the_name1( "Clip" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::ViewState const *) {
    return *detail::theViewStateType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::ViewState const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::ViewState >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_VIEWSTATE_HPP
