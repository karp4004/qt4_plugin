#ifndef INCLUDED_COM_SUN_STAR_RENDERING_FLOATINGPOINTBITMAPLAYOUT_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_FLOATINGPOINTBITMAPLAYOUT_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/FloatingPointBitmapLayout.hdl"

#include "com/sun/star/rendering/XColorSpace.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline FloatingPointBitmapLayout::FloatingPointBitmapLayout() SAL_THROW(())
    : ScanLines(0)
    , ScanLineBytes(0)
    , ScanLineStride(0)
    , PlaneStride(0)
    , ColorSpace()
    , NumComponents(0)
    , Endianness(0)
    , Format(0)
{
}

inline FloatingPointBitmapLayout::FloatingPointBitmapLayout(const ::sal_Int32& ScanLines_, const ::sal_Int32& ScanLineBytes_, const ::sal_Int32& ScanLineStride_, const ::sal_Int32& PlaneStride_, const ::css::uno::Reference< ::css::rendering::XColorSpace >& ColorSpace_, const ::sal_Int32& NumComponents_, const ::sal_Int8& Endianness_, const ::sal_Int8& Format_) SAL_THROW(())
    : ScanLines(ScanLines_)
    , ScanLineBytes(ScanLineBytes_)
    , ScanLineStride(ScanLineStride_)
    , PlaneStride(PlaneStride_)
    , ColorSpace(ColorSpace_)
    , NumComponents(NumComponents_)
    , Endianness(Endianness_)
    , Format(Format_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theFloatingPointBitmapLayoutType : public rtl::StaticWithInit< ::css::uno::Type *, theFloatingPointBitmapLayoutType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.FloatingPointBitmapLayout" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "ScanLines" );
        ::rtl::OUString the_name1( "ScanLineBytes" );
        ::rtl::OUString the_name2( "ScanLineStride" );
        ::rtl::OUString the_name3( "PlaneStride" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XColorSpace > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.rendering.XColorSpace" );
        ::rtl::OUString the_name4( "ColorSpace" );
        ::rtl::OUString the_name5( "NumComponents" );
        ::rtl::OUString the_tname2( "byte" );
        ::rtl::OUString the_name6( "Endianness" );
        ::rtl::OUString the_name7( "Format" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname2.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname2.pData, the_name7.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 8, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::FloatingPointBitmapLayout const *) {
    return *detail::theFloatingPointBitmapLayoutType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::FloatingPointBitmapLayout const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::FloatingPointBitmapLayout >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_FLOATINGPOINTBITMAPLAYOUT_HPP
