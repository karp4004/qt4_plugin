#ifndef INCLUDED_COM_SUN_STAR_RENDERING_RENDERSTATE_HPP
#define INCLUDED_COM_SUN_STAR_RENDERING_RENDERSTATE_HPP

#include "sal/config.h"

#include "com/sun/star/rendering/RenderState.hdl"

#include "com/sun/star/geometry/AffineMatrix2D.hpp"
#include "com/sun/star/rendering/ColorComponent.hpp"
#include "com/sun/star/rendering/XPolyPolygon2D.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rendering {

inline RenderState::RenderState() SAL_THROW(())
    : AffineTransform()
    , Clip()
    , DeviceColor()
    , CompositeOperation(0)
{
}

inline RenderState::RenderState(const ::css::geometry::AffineMatrix2D& AffineTransform_, const ::css::uno::Reference< ::css::rendering::XPolyPolygon2D >& Clip_, const ::css::uno::Sequence< double >& DeviceColor_, const ::sal_Int8& CompositeOperation_) SAL_THROW(())
    : AffineTransform(AffineTransform_)
    , Clip(Clip_)
    , DeviceColor(DeviceColor_)
    , CompositeOperation(CompositeOperation_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rendering { namespace detail {

struct theRenderStateType : public rtl::StaticWithInit< ::css::uno::Type *, theRenderStateType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rendering.RenderState" );
        ::cppu::UnoType< ::css::geometry::AffineMatrix2D >::get();
        ::rtl::OUString the_tname0( "com.sun.star.geometry.AffineMatrix2D" );
        ::rtl::OUString the_name0( "AffineTransform" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rendering::XPolyPolygon2D > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.rendering.XPolyPolygon2D" );
        ::rtl::OUString the_name1( "Clip" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< double > >::get();
        ::rtl::OUString the_tname2( "[]double" );
        ::rtl::OUString the_name2( "DeviceColor" );
        ::rtl::OUString the_tname3( "byte" );
        ::rtl::OUString the_name3( "CompositeOperation" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname3.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rendering {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rendering::RenderState const *) {
    return *detail::theRenderStateType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rendering::RenderState const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rendering::RenderState >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RENDERING_RENDERSTATE_HPP
