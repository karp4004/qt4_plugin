#ifndef INCLUDED_COM_SUN_STAR_RDF_STATEMENT_HPP
#define INCLUDED_COM_SUN_STAR_RDF_STATEMENT_HPP

#include "sal/config.h"

#include "com/sun/star/rdf/Statement.hdl"

#include "com/sun/star/rdf/XNode.hpp"
#include "com/sun/star/rdf/XResource.hpp"
#include "com/sun/star/rdf/XURI.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace rdf {

inline Statement::Statement() SAL_THROW(())
    : Subject()
    , Predicate()
    , Object()
    , Graph()
{
}

inline Statement::Statement(const ::css::uno::Reference< ::css::rdf::XResource >& Subject_, const ::css::uno::Reference< ::css::rdf::XURI >& Predicate_, const ::css::uno::Reference< ::css::rdf::XNode >& Object_, const ::css::uno::Reference< ::css::rdf::XURI >& Graph_) SAL_THROW(())
    : Subject(Subject_)
    , Predicate(Predicate_)
    , Object(Object_)
    , Graph(Graph_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace rdf { namespace detail {

struct theStatementType : public rtl::StaticWithInit< ::css::uno::Type *, theStatementType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.rdf.Statement" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rdf::XResource > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.rdf.XResource" );
        ::rtl::OUString the_name0( "Subject" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rdf::XURI > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.rdf.XURI" );
        ::rtl::OUString the_name1( "Predicate" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::rdf::XNode > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.rdf.XNode" );
        ::rtl::OUString the_name2( "Object" );
        ::rtl::OUString the_name3( "Graph" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace rdf {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::rdf::Statement const *) {
    return *detail::theStatementType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::rdf::Statement const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::rdf::Statement >::get();
}

#endif // INCLUDED_COM_SUN_STAR_RDF_STATEMENT_HPP
