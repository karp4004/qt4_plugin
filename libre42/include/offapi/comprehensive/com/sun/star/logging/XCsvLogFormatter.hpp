#ifndef INCLUDED_COM_SUN_STAR_LOGGING_XCSVLOGFORMATTER_HPP
#define INCLUDED_COM_SUN_STAR_LOGGING_XCSVLOGFORMATTER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/logging/XCsvLogFormatter.hdl"

#include "com/sun/star/logging/XLogFormatter.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace logging { namespace detail {

struct theXCsvLogFormatterType : public rtl::StaticWithInit< ::css::uno::Type *, theXCsvLogFormatterType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.logging.XCsvLogFormatter" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::logging::XLogFormatter > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[6] = { 0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.logging.XCsvLogFormatter::LogEventNo" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.logging.XCsvLogFormatter::LogThread" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.logging.XCsvLogFormatter::LogTimestamp" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.logging.XCsvLogFormatter::LogSource" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.logging.XCsvLogFormatter::Columnnames" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sMethodName0( "com.sun.star.logging.XCsvLogFormatter::formatMultiColumn" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            6,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace logging {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::logging::XCsvLogFormatter const *) {
    const ::css::uno::Type &rRet = *detail::theXCsvLogFormatterType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "boolean" );
                ::rtl::OUString sAttributeName0( "com.sun.star.logging.XCsvLogFormatter::LogEventNo" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    6, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "boolean" );
                ::rtl::OUString sAttributeName1( "com.sun.star.logging.XCsvLogFormatter::LogThread" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "boolean" );
                ::rtl::OUString sAttributeName2( "com.sun.star.logging.XCsvLogFormatter::LogTimestamp" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    8, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "boolean" );
                ::rtl::OUString sAttributeName3( "com.sun.star.logging.XCsvLogFormatter::LogSource" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    9, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "[]string" );
                ::rtl::OUString sAttributeName4( "com.sun.star.logging.XCsvLogFormatter::Columnnames" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    10, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "columnData" );
                ::rtl::OUString sParamType0( "[]string" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType0( "string" );
                ::rtl::OUString sMethodName0( "com.sun.star.logging.XCsvLogFormatter::formatMultiColumn" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    11, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sReturnType0.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::logging::XCsvLogFormatter > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::logging::XCsvLogFormatter > >::get();
}

::css::uno::Type const & ::css::logging::XCsvLogFormatter::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::logging::XCsvLogFormatter > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_LOGGING_XCSVLOGFORMATTER_HPP
