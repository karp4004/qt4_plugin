#ifndef INCLUDED_COM_SUN_STAR_LOGGING_LOGRECORD_HPP
#define INCLUDED_COM_SUN_STAR_LOGGING_LOGRECORD_HPP

#include "sal/config.h"

#include "com/sun/star/logging/LogRecord.hdl"

#include "com/sun/star/util/DateTime.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace logging {

inline LogRecord::LogRecord() SAL_THROW(())
    : LoggerName()
    , SourceClassName()
    , SourceMethodName()
    , Message()
    , LogTime()
    , SequenceNumber(0)
    , ThreadID()
    , Level(0)
{
}

inline LogRecord::LogRecord(const ::rtl::OUString& LoggerName_, const ::rtl::OUString& SourceClassName_, const ::rtl::OUString& SourceMethodName_, const ::rtl::OUString& Message_, const ::css::util::DateTime& LogTime_, const ::sal_Int64& SequenceNumber_, const ::rtl::OUString& ThreadID_, const ::sal_Int32& Level_) SAL_THROW(())
    : LoggerName(LoggerName_)
    , SourceClassName(SourceClassName_)
    , SourceMethodName(SourceMethodName_)
    , Message(Message_)
    , LogTime(LogTime_)
    , SequenceNumber(SequenceNumber_)
    , ThreadID(ThreadID_)
    , Level(Level_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace logging { namespace detail {

struct theLogRecordType : public rtl::StaticWithInit< ::css::uno::Type *, theLogRecordType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.logging.LogRecord" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "LoggerName" );
        ::rtl::OUString the_name1( "SourceClassName" );
        ::rtl::OUString the_name2( "SourceMethodName" );
        ::rtl::OUString the_name3( "Message" );
        ::cppu::UnoType< ::css::util::DateTime >::get();
        ::rtl::OUString the_tname1( "com.sun.star.util.DateTime" );
        ::rtl::OUString the_name4( "LogTime" );
        ::rtl::OUString the_tname2( "hyper" );
        ::rtl::OUString the_name5( "SequenceNumber" );
        ::rtl::OUString the_name6( "ThreadID" );
        ::rtl::OUString the_tname3( "long" );
        ::rtl::OUString the_name7( "Level" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_HYPER, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name6.pData }, false },
            { { typelib_TypeClass_LONG, the_tname3.pData, the_name7.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 8, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace logging {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::logging::LogRecord const *) {
    return *detail::theLogRecordType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::logging::LogRecord const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::logging::LogRecord >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LOGGING_LOGRECORD_HPP
