#ifndef INCLUDED_COM_SUN_STAR_LOGGING_SIMPLELOGRING_HPP
#define INCLUDED_COM_SUN_STAR_LOGGING_SIMPLELOGRING_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/logging/XSimpleLogRing.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace logging {

class SimpleLogRing {
public:
    static ::css::uno::Reference< ::css::logging::XSimpleLogRing > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::logging::XSimpleLogRing > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::logging::XSimpleLogRing >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.logging.SimpleLogRing" ), ::css::uno::Sequence< ::css::uno::Any >(), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.logging.SimpleLogRing of type com.sun.star.logging.XSimpleLogRing: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.logging.SimpleLogRing of type com.sun.star.logging.XSimpleLogRing" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::logging::XSimpleLogRing > createWithSize(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, ::sal_Int32 nSize) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= nSize;
        ::css::uno::Reference< ::css::logging::XSimpleLogRing > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::logging::XSimpleLogRing >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.logging.SimpleLogRing" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.logging.SimpleLogRing of type com.sun.star.logging.XSimpleLogRing: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.logging.SimpleLogRing of type com.sun.star.logging.XSimpleLogRing" ), the_context);
        }
        return the_instance;
    }

private:
    SimpleLogRing(); // not implemented
    SimpleLogRing(SimpleLogRing &); // not implemented
    ~SimpleLogRing(); // not implemented
    void operator =(SimpleLogRing); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_LOGGING_SIMPLELOGRING_HPP
