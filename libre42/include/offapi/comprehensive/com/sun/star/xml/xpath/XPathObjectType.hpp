#ifndef INCLUDED_COM_SUN_STAR_XML_XPATH_XPATHOBJECTTYPE_HPP
#define INCLUDED_COM_SUN_STAR_XML_XPATH_XPATHOBJECTTYPE_HPP

#include "sal/config.h"

#include "com/sun/star/xml/xpath/XPathObjectType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace xpath { namespace detail {

struct theXPathObjectTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theXPathObjectTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.xpath.XPathObjectType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[10];
        ::rtl::OUString sEnumValue0( "XPATH_UNDEFINED" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "XPATH_NODESET" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "XPATH_BOOLEAN" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "XPATH_NUMBER" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "XPATH_STRING" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "XPATH_POINT" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "XPATH_RANGE" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "XPATH_LOCATIONSET" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "XPATH_USERS" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "XPATH_XSLT_TREE" );
        enumValueNames[9] = sEnumValue9.pData;

        sal_Int32 enumValues[10];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;
        enumValues[9] = 9;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::xml::xpath::XPathObjectType_XPATH_UNDEFINED,
            10, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace xpath {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::xpath::XPathObjectType const *) {
    return *detail::theXPathObjectTypeType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::xpath::XPathObjectType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::xpath::XPathObjectType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_XPATH_XPATHOBJECTTYPE_HPP
