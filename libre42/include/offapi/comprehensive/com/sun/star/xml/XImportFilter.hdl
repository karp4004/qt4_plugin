#ifndef INCLUDED_COM_SUN_STAR_XML_XIMPORTFILTER_HDL
#define INCLUDED_COM_SUN_STAR_XML_XIMPORTFILTER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace sax { class XDocumentHandler; } } } } }
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml {

class SAL_NO_VTABLE XImportFilter : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Bool SAL_CALL importer( const ::css::uno::Sequence< ::css::beans::PropertyValue >& aSourceData, const ::css::uno::Reference< ::css::xml::sax::XDocumentHandler >& xDocHandler, const ::css::uno::Sequence< ::rtl::OUString >& msUserData ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XImportFilter() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::XImportFilter const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::XImportFilter > *) SAL_THROW(());

#endif
