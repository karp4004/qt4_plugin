#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_XDOCUMENTFRAGMENT_HPP
#define INCLUDED_COM_SUN_STAR_XML_DOM_XDOCUMENTFRAGMENT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xml/dom/XDocumentFragment.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/xml/dom/XNode.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace detail {

struct theXDocumentFragmentType : public rtl::StaticWithInit< ::css::uno::Type *, theXDocumentFragmentType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.dom.XDocumentFragment" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::xml::dom::XNode > >::get().getTypeLibType();

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::XDocumentFragment const *) {
    const ::css::uno::Type &rRet = *detail::theXDocumentFragmentType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xml::dom::XDocumentFragment > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xml::dom::XDocumentFragment > >::get();
}

::css::uno::Type const & ::css::xml::dom::XDocumentFragment::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xml::dom::XDocumentFragment > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XML_DOM_XDOCUMENTFRAGMENT_HPP
