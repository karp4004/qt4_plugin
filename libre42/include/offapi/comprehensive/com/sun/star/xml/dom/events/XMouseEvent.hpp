#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_XMOUSEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_XMOUSEEVENT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xml/dom/events/XMouseEvent.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/xml/dom/events/XEventTarget.hpp"
#include "com/sun/star/xml/dom/events/XUIEvent.hpp"
#include "com/sun/star/xml/dom/views/XAbstractView.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace events { namespace detail {

struct theXMouseEventType : public rtl::StaticWithInit< ::css::uno::Type *, theXMouseEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.dom.events.XMouseEvent" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::xml::dom::events::XUIEvent > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[11] = { 0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sMethodName0( "com.sun.star.xml.dom.events.XMouseEvent::getScreenX" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );
        ::rtl::OUString sMethodName1( "com.sun.star.xml.dom.events.XMouseEvent::getScreenY" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName1.pData );
        ::rtl::OUString sMethodName2( "com.sun.star.xml.dom.events.XMouseEvent::getClientX" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName2.pData );
        ::rtl::OUString sMethodName3( "com.sun.star.xml.dom.events.XMouseEvent::getClientY" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName3.pData );
        ::rtl::OUString sMethodName4( "com.sun.star.xml.dom.events.XMouseEvent::getCtrlKey" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName4.pData );
        ::rtl::OUString sMethodName5( "com.sun.star.xml.dom.events.XMouseEvent::getShiftKey" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName5.pData );
        ::rtl::OUString sMethodName6( "com.sun.star.xml.dom.events.XMouseEvent::getAltKey" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName6.pData );
        ::rtl::OUString sMethodName7( "com.sun.star.xml.dom.events.XMouseEvent::getMetaKey" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName7.pData );
        ::rtl::OUString sMethodName8( "com.sun.star.xml.dom.events.XMouseEvent::getButton" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName8.pData );
        ::rtl::OUString sMethodName9( "com.sun.star.xml.dom.events.XMouseEvent::getRelatedTarget" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName9.pData );
        ::rtl::OUString sMethodName10( "com.sun.star.xml.dom.events.XMouseEvent::initMouseEvent" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName10.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            11,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace events {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::events::XMouseEvent const *) {
    const ::css::uno::Type &rRet = *detail::theXMouseEventType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType0( "long" );
                ::rtl::OUString sMethodName0( "com.sun.star.xml.dom.events.XMouseEvent::getScreenX" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    16, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sReturnType0.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType1( "long" );
                ::rtl::OUString sMethodName1( "com.sun.star.xml.dom.events.XMouseEvent::getScreenY" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    17, sal_False,
                    sMethodName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sReturnType1.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType2( "long" );
                ::rtl::OUString sMethodName2( "com.sun.star.xml.dom.events.XMouseEvent::getClientX" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    18, sal_False,
                    sMethodName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sReturnType2.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType3( "long" );
                ::rtl::OUString sMethodName3( "com.sun.star.xml.dom.events.XMouseEvent::getClientY" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    19, sal_False,
                    sMethodName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sReturnType3.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType4( "boolean" );
                ::rtl::OUString sMethodName4( "com.sun.star.xml.dom.events.XMouseEvent::getCtrlKey" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    20, sal_False,
                    sMethodName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType4.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType5( "boolean" );
                ::rtl::OUString sMethodName5( "com.sun.star.xml.dom.events.XMouseEvent::getShiftKey" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    21, sal_False,
                    sMethodName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType5.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType6( "boolean" );
                ::rtl::OUString sMethodName6( "com.sun.star.xml.dom.events.XMouseEvent::getAltKey" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    22, sal_False,
                    sMethodName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType6.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType7( "boolean" );
                ::rtl::OUString sMethodName7( "com.sun.star.xml.dom.events.XMouseEvent::getMetaKey" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    23, sal_False,
                    sMethodName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sReturnType7.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType8( "short" );
                ::rtl::OUString sMethodName8( "com.sun.star.xml.dom.events.XMouseEvent::getButton" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    24, sal_False,
                    sMethodName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sReturnType8.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType9( "com.sun.star.xml.dom.events.XEventTarget" );
                ::rtl::OUString sMethodName9( "com.sun.star.xml.dom.events.XMouseEvent::getRelatedTarget" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    25, sal_False,
                    sMethodName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sReturnType9.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[15];
                ::rtl::OUString sParamName0( "typeArg" );
                ::rtl::OUString sParamType0( "string" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString sParamName1( "canBubbleArg" );
                ::rtl::OUString sParamType1( "boolean" );
                aParameters[1].pParamName = sParamName1.pData;
                aParameters[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
                aParameters[1].pTypeName = sParamType1.pData;
                aParameters[1].bIn = sal_True;
                aParameters[1].bOut = sal_False;
                ::rtl::OUString sParamName2( "cancelableArg" );
                ::rtl::OUString sParamType2( "boolean" );
                aParameters[2].pParamName = sParamName2.pData;
                aParameters[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
                aParameters[2].pTypeName = sParamType2.pData;
                aParameters[2].bIn = sal_True;
                aParameters[2].bOut = sal_False;
                ::rtl::OUString sParamName3( "viewArg" );
                ::rtl::OUString sParamType3( "com.sun.star.xml.dom.views.XAbstractView" );
                aParameters[3].pParamName = sParamName3.pData;
                aParameters[3].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[3].pTypeName = sParamType3.pData;
                aParameters[3].bIn = sal_True;
                aParameters[3].bOut = sal_False;
                ::rtl::OUString sParamName4( "detailArg" );
                ::rtl::OUString sParamType4( "long" );
                aParameters[4].pParamName = sParamName4.pData;
                aParameters[4].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
                aParameters[4].pTypeName = sParamType4.pData;
                aParameters[4].bIn = sal_True;
                aParameters[4].bOut = sal_False;
                ::rtl::OUString sParamName5( "screenXArg" );
                ::rtl::OUString sParamType5( "long" );
                aParameters[5].pParamName = sParamName5.pData;
                aParameters[5].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
                aParameters[5].pTypeName = sParamType5.pData;
                aParameters[5].bIn = sal_True;
                aParameters[5].bOut = sal_False;
                ::rtl::OUString sParamName6( "screenYArg" );
                ::rtl::OUString sParamType6( "long" );
                aParameters[6].pParamName = sParamName6.pData;
                aParameters[6].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
                aParameters[6].pTypeName = sParamType6.pData;
                aParameters[6].bIn = sal_True;
                aParameters[6].bOut = sal_False;
                ::rtl::OUString sParamName7( "clientXArg" );
                ::rtl::OUString sParamType7( "long" );
                aParameters[7].pParamName = sParamName7.pData;
                aParameters[7].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
                aParameters[7].pTypeName = sParamType7.pData;
                aParameters[7].bIn = sal_True;
                aParameters[7].bOut = sal_False;
                ::rtl::OUString sParamName8( "clientYArg" );
                ::rtl::OUString sParamType8( "long" );
                aParameters[8].pParamName = sParamName8.pData;
                aParameters[8].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
                aParameters[8].pTypeName = sParamType8.pData;
                aParameters[8].bIn = sal_True;
                aParameters[8].bOut = sal_False;
                ::rtl::OUString sParamName9( "ctrlKeyArg" );
                ::rtl::OUString sParamType9( "boolean" );
                aParameters[9].pParamName = sParamName9.pData;
                aParameters[9].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
                aParameters[9].pTypeName = sParamType9.pData;
                aParameters[9].bIn = sal_True;
                aParameters[9].bOut = sal_False;
                ::rtl::OUString sParamName10( "altKeyArg" );
                ::rtl::OUString sParamType10( "boolean" );
                aParameters[10].pParamName = sParamName10.pData;
                aParameters[10].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
                aParameters[10].pTypeName = sParamType10.pData;
                aParameters[10].bIn = sal_True;
                aParameters[10].bOut = sal_False;
                ::rtl::OUString sParamName11( "shiftKeyArg" );
                ::rtl::OUString sParamType11( "boolean" );
                aParameters[11].pParamName = sParamName11.pData;
                aParameters[11].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
                aParameters[11].pTypeName = sParamType11.pData;
                aParameters[11].bIn = sal_True;
                aParameters[11].bOut = sal_False;
                ::rtl::OUString sParamName12( "metaKeyArg" );
                ::rtl::OUString sParamType12( "boolean" );
                aParameters[12].pParamName = sParamName12.pData;
                aParameters[12].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN;
                aParameters[12].pTypeName = sParamType12.pData;
                aParameters[12].bIn = sal_True;
                aParameters[12].bOut = sal_False;
                ::rtl::OUString sParamName13( "buttonArg" );
                ::rtl::OUString sParamType13( "short" );
                aParameters[13].pParamName = sParamName13.pData;
                aParameters[13].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SHORT;
                aParameters[13].pTypeName = sParamType13.pData;
                aParameters[13].bIn = sal_True;
                aParameters[13].bOut = sal_False;
                ::rtl::OUString sParamName14( "relatedTargetArg" );
                ::rtl::OUString sParamType14( "com.sun.star.xml.dom.events.XEventTarget" );
                aParameters[14].pParamName = sParamName14.pData;
                aParameters[14].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[14].pTypeName = sParamType14.pData;
                aParameters[14].bIn = sal_True;
                aParameters[14].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType10( "void" );
                ::rtl::OUString sMethodName10( "com.sun.star.xml.dom.events.XMouseEvent::initMouseEvent" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    26, sal_False,
                    sMethodName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType10.pData,
                    15, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xml::dom::events::XMouseEvent > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xml::dom::events::XMouseEvent > >::get();
}

::css::uno::Type const & ::css::xml::dom::events::XMouseEvent::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xml::dom::events::XMouseEvent > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_XMOUSEEVENT_HPP
