#ifndef INCLUDED_COM_SUN_STAR_XML_CSAX_XMLATTRIBUTE_HPP
#define INCLUDED_COM_SUN_STAR_XML_CSAX_XMLATTRIBUTE_HPP

#include "sal/config.h"

#include "com/sun/star/xml/csax/XMLAttribute.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace xml { namespace csax {

inline XMLAttribute::XMLAttribute() SAL_THROW(())
    : sName()
    , sValue()
{
}

inline XMLAttribute::XMLAttribute(const ::rtl::OUString& sName_, const ::rtl::OUString& sValue_) SAL_THROW(())
    : sName(sName_)
    , sValue(sValue_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace csax { namespace detail {

struct theXMLAttributeType : public rtl::StaticWithInit< ::css::uno::Type *, theXMLAttributeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.xml.csax.XMLAttribute" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "sName" );
        ::rtl::OUString the_name1( "sValue" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace csax {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::csax::XMLAttribute const *) {
    return *detail::theXMLAttributeType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::csax::XMLAttribute const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::csax::XMLAttribute >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_CSAX_XMLATTRIBUTE_HPP
