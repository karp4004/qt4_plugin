#ifndef INCLUDED_COM_SUN_STAR_XML_SAX_SAXINVALIDCHARACTEREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XML_SAX_SAXINVALIDCHARACTEREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xml/sax/SAXInvalidCharacterException.hdl"

#include "com/sun/star/xml/sax/SAXException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

inline SAXInvalidCharacterException::SAXInvalidCharacterException() SAL_THROW(())
    : ::css::xml::sax::SAXException()
{
    ::cppu::UnoType< ::css::xml::sax::SAXInvalidCharacterException >::get();
}

inline SAXInvalidCharacterException::SAXInvalidCharacterException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& WrappedException_) SAL_THROW(())
    : ::css::xml::sax::SAXException(Message_, Context_, WrappedException_)
{
    ::cppu::UnoType< ::css::xml::sax::SAXInvalidCharacterException >::get();
}

SAXInvalidCharacterException::SAXInvalidCharacterException(SAXInvalidCharacterException const & the_other): ::css::xml::sax::SAXException(the_other) {}

SAXInvalidCharacterException::~SAXInvalidCharacterException() {}

SAXInvalidCharacterException & SAXInvalidCharacterException::operator =(SAXInvalidCharacterException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::xml::sax::SAXException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax { namespace detail {

struct theSAXInvalidCharacterExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theSAXInvalidCharacterExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.sax.SAXInvalidCharacterException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::xml::sax::SAXException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::sax::SAXInvalidCharacterException const *) {
    return *detail::theSAXInvalidCharacterExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::sax::SAXInvalidCharacterException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::sax::SAXInvalidCharacterException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_SAX_SAXINVALIDCHARACTEREXCEPTION_HPP
