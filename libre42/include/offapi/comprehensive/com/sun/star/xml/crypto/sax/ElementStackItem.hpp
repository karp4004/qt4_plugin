#ifndef INCLUDED_COM_SUN_STAR_XML_CRYPTO_SAX_ELEMENTSTACKITEM_HPP
#define INCLUDED_COM_SUN_STAR_XML_CRYPTO_SAX_ELEMENTSTACKITEM_HPP

#include "sal/config.h"

#include "com/sun/star/xml/crypto/sax/ElementStackItem.hdl"

#include "com/sun/star/xml/sax/XAttributeList.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace xml { namespace crypto { namespace sax {

inline ElementStackItem::ElementStackItem() SAL_THROW(())
    : isStartElementEvent(false)
    , elementName()
    , xAttributes()
{
}

inline ElementStackItem::ElementStackItem(const ::sal_Bool& isStartElementEvent_, const ::rtl::OUString& elementName_, const ::css::uno::Reference< ::css::xml::sax::XAttributeList >& xAttributes_) SAL_THROW(())
    : isStartElementEvent(isStartElementEvent_)
    , elementName(elementName_)
    , xAttributes(xAttributes_)
{
}

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace crypto { namespace sax { namespace detail {

struct theElementStackItemType : public rtl::StaticWithInit< ::css::uno::Type *, theElementStackItemType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.xml.crypto.sax.ElementStackItem" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "isStartElementEvent" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "elementName" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::xml::sax::XAttributeList > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.xml.sax.XAttributeList" );
        ::rtl::OUString the_name2( "xAttributes" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace crypto { namespace sax {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::crypto::sax::ElementStackItem const *) {
    return *detail::theElementStackItemType::get();
}

} } } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::crypto::sax::ElementStackItem const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::crypto::sax::ElementStackItem >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_CRYPTO_SAX_ELEMENTSTACKITEM_HPP
