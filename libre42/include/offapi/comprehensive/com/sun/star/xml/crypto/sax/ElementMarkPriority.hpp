#ifndef INCLUDED_COM_SUN_STAR_XML_CRYPTO_SAX_ELEMENTMARKPRIORITY_HPP
#define INCLUDED_COM_SUN_STAR_XML_CRYPTO_SAX_ELEMENTMARKPRIORITY_HPP

#include "sal/config.h"

#include "com/sun/star/xml/crypto/sax/ElementMarkPriority.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace crypto { namespace sax { namespace detail {

struct theElementMarkPriorityType : public rtl::StaticWithInit< ::css::uno::Type *, theElementMarkPriorityType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.crypto.sax.ElementMarkPriority" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[3];
        ::rtl::OUString sEnumValue0( "MINIMUM" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "AFTERMODIFY" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "BEFOREMODIFY" );
        enumValueNames[2] = sEnumValue2.pData;

        sal_Int32 enumValues[3];
        enumValues[0] = 1;
        enumValues[1] = 2;
        enumValues[2] = 3;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::xml::crypto::sax::ElementMarkPriority_MINIMUM,
            3, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace crypto { namespace sax {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::crypto::sax::ElementMarkPriority const *) {
    return *detail::theElementMarkPriorityType::get();
}

} } } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::crypto::sax::ElementMarkPriority const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::crypto::sax::ElementMarkPriority >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_CRYPTO_SAX_ELEMENTMARKPRIORITY_HPP
