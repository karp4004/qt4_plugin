#ifndef INCLUDED_COM_SUN_STAR_XML_CRYPTO_XNSSINITIALIZER_HPP
#define INCLUDED_COM_SUN_STAR_XML_CRYPTO_XNSSINITIALIZER_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/xml/crypto/XNSSInitializer.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/xml/crypto/XCipherContextSupplier.hpp"
#include "com/sun/star/xml/crypto/XDigestContextSupplier.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace crypto { namespace detail {

struct theXNSSInitializerType : public rtl::StaticWithInit< ::css::uno::Type *, theXNSSInitializerType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.crypto.XNSSInitializer" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[2];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::xml::crypto::XDigestContextSupplier > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::xml::crypto::XCipherContextSupplier > >::get().getTypeLibType();

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            2, aSuperTypes,
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace crypto {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::crypto::XNSSInitializer const *) {
    const ::css::uno::Type &rRet = *detail::theXNSSInitializerType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::xml::crypto::XNSSInitializer > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::xml::crypto::XNSSInitializer > >::get();
}

::css::uno::Type const & ::css::xml::crypto::XNSSInitializer::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::xml::crypto::XNSSInitializer > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_XML_CRYPTO_XNSSINITIALIZER_HPP
