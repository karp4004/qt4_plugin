#ifndef INCLUDED_COM_SUN_STAR_XML_WRAPPER_XXMLDOCUMENTWRAPPER_HDL
#define INCLUDED_COM_SUN_STAR_XML_WRAPPER_XXMLDOCUMENTWRAPPER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/xml/sax/SAXException.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace sax { class XDocumentHandler; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace wrapper { class XXMLElementWrapper; } } } } }
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace wrapper {

class SAL_NO_VTABLE XXMLDocumentWrapper : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper > SAL_CALL getCurrentElement() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setCurrentElement( const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& element ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeCurrentElement() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isCurrent( const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& node ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isCurrentElementEmpty() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getNodeName( const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& node ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL clearUselessData( const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& node, const ::css::uno::Sequence< ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper > >& reservedDescendants, const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& stopAtNode ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL collapse( const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& node ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL generateSAXEvents( const ::css::uno::Reference< ::css::xml::sax::XDocumentHandler >& handler, const ::css::uno::Reference< ::css::xml::sax::XDocumentHandler >& saxEventKeeperHandler, const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& startNode, const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& endNode ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL getTree( const ::css::uno::Reference< ::css::xml::sax::XDocumentHandler >& handler ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL rebuildIDLink( const ::css::uno::Reference< ::css::xml::wrapper::XXMLElementWrapper >& node ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XXMLDocumentWrapper() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::wrapper::XXMLDocumentWrapper const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::wrapper::XXMLDocumentWrapper > *) SAL_THROW(());

#endif
