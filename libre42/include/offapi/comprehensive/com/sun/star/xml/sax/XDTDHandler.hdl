#ifndef INCLUDED_COM_SUN_STAR_XML_SAX_XDTDHANDLER_HDL
#define INCLUDED_COM_SUN_STAR_XML_SAX_XDTDHANDLER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

class SAL_NO_VTABLE XDTDHandler : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL notationDecl( const ::rtl::OUString& sName, const ::rtl::OUString& sPublicId, const ::rtl::OUString& sSystemId ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL unparsedEntityDecl( const ::rtl::OUString& sName, const ::rtl::OUString& sPublicId, const ::rtl::OUString& sSystemId, const ::rtl::OUString& sNotationName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDTDHandler() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::sax::XDTDHandler const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::sax::XDTDHandler > *) SAL_THROW(());

#endif
