#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_EVENTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_EVENTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xml/dom/events/EventException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace events {

inline EventException::EventException() SAL_THROW(())
    : ::css::uno::Exception()
    , code(0)
{
    ::cppu::UnoType< ::css::xml::dom::events::EventException >::get();
}

inline EventException::EventException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int16& code_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , code(code_)
{
    ::cppu::UnoType< ::css::xml::dom::events::EventException >::get();
}

EventException::EventException(EventException const & the_other): ::css::uno::Exception(the_other), code(the_other.code) {}

EventException::~EventException() {}

EventException & EventException::operator =(EventException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    code = the_other.code;
    return *this;
}

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace events { namespace detail {

struct theEventExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theEventExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.dom.events.EventException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "short" );
        ::rtl::OUString sMemberName0( "code" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SHORT;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace events {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::events::EventException const *) {
    return *detail::theEventExceptionType::get();
}

} } } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::dom::events::EventException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::dom::events::EventException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_DOM_EVENTS_EVENTEXCEPTION_HPP
