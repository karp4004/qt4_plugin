#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_DOMEXCEPTIONTYPE_HPP
#define INCLUDED_COM_SUN_STAR_XML_DOM_DOMEXCEPTIONTYPE_HPP

#include "sal/config.h"

#include "com/sun/star/xml/dom/DOMExceptionType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace detail {

struct theDOMExceptionTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theDOMExceptionTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.dom.DOMExceptionType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[15];
        ::rtl::OUString sEnumValue0( "DOMSTRING_SIZE_ERR" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "HIERARCHY_REQUEST_ERR" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "INDEX_SIZE_ERR" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "INUSE_ATTRIBUTE_ERR" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "INVALID_ACCESS_ERR" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "INVALID_CHARACTER_ERR" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "INVALID_MODIFICATION_ERR" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "INVALID_STATE_ERR" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "NAMESPACE_ERR" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "NO_DATA_ALLOWED_ERR" );
        enumValueNames[9] = sEnumValue9.pData;
        ::rtl::OUString sEnumValue10( "NO_MODIFICATION_ALLOWED_ERR" );
        enumValueNames[10] = sEnumValue10.pData;
        ::rtl::OUString sEnumValue11( "NOT_FOUND_ERR" );
        enumValueNames[11] = sEnumValue11.pData;
        ::rtl::OUString sEnumValue12( "NOT_SUPPORTED_ERR" );
        enumValueNames[12] = sEnumValue12.pData;
        ::rtl::OUString sEnumValue13( "SYNTAX_ERR" );
        enumValueNames[13] = sEnumValue13.pData;
        ::rtl::OUString sEnumValue14( "WRONG_DOCUMENT_ERR" );
        enumValueNames[14] = sEnumValue14.pData;

        sal_Int32 enumValues[15];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;
        enumValues[9] = 9;
        enumValues[10] = 10;
        enumValues[11] = 11;
        enumValues[12] = 12;
        enumValues[13] = 13;
        enumValues[14] = 14;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::xml::dom::DOMExceptionType_DOMSTRING_SIZE_ERR,
            15, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::DOMExceptionType const *) {
    return *detail::theDOMExceptionTypeType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::dom::DOMExceptionType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::dom::DOMExceptionType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_DOM_DOMEXCEPTIONTYPE_HPP
