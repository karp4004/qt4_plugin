#ifndef INCLUDED_COM_SUN_STAR_XML_CRYPTO_XMLSIGNATUREEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XML_CRYPTO_XMLSIGNATUREEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xml/crypto/XMLSignatureException.hdl"

#include "com/sun/star/security/SignatureException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace crypto {

inline XMLSignatureException::XMLSignatureException() SAL_THROW(())
    : ::css::security::SignatureException()
{
    ::cppu::UnoType< ::css::xml::crypto::XMLSignatureException >::get();
}

inline XMLSignatureException::XMLSignatureException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::security::SignatureException(Message_, Context_)
{
    ::cppu::UnoType< ::css::xml::crypto::XMLSignatureException >::get();
}

XMLSignatureException::XMLSignatureException(XMLSignatureException const & the_other): ::css::security::SignatureException(the_other) {}

XMLSignatureException::~XMLSignatureException() {}

XMLSignatureException & XMLSignatureException::operator =(XMLSignatureException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::security::SignatureException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace crypto { namespace detail {

struct theXMLSignatureExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theXMLSignatureExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.crypto.XMLSignatureException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::security::SignatureException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace crypto {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::crypto::XMLSignatureException const *) {
    return *detail::theXMLSignatureExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::crypto::XMLSignatureException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::crypto::XMLSignatureException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_CRYPTO_XMLSIGNATUREEXCEPTION_HPP
