#ifndef INCLUDED_COM_SUN_STAR_XML_DOM_DOMEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XML_DOM_DOMEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xml/dom/DOMException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/xml/dom/DOMExceptionType.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace dom {

inline DOMException::DOMException() SAL_THROW(())
    : ::css::uno::Exception()
    , Code(::css::xml::dom::DOMExceptionType_DOMSTRING_SIZE_ERR)
{
    ::cppu::UnoType< ::css::xml::dom::DOMException >::get();
}

inline DOMException::DOMException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::xml::dom::DOMExceptionType& Code_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Code(Code_)
{
    ::cppu::UnoType< ::css::xml::dom::DOMException >::get();
}

DOMException::DOMException(DOMException const & the_other): ::css::uno::Exception(the_other), Code(the_other.Code) {}

DOMException::~DOMException() {}

DOMException & DOMException::operator =(DOMException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Code = the_other.Code;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom { namespace detail {

struct theDOMExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theDOMExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.dom.DOMException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::xml::dom::DOMExceptionType >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "com.sun.star.xml.dom.DOMExceptionType" );
        ::rtl::OUString sMemberName0( "Code" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_ENUM;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace dom {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::dom::DOMException const *) {
    return *detail::theDOMExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::dom::DOMException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::dom::DOMException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_DOM_DOMEXCEPTION_HPP
