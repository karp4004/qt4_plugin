#ifndef INCLUDED_COM_SUN_STAR_XML_SAX_INPUTSOURCE_HPP
#define INCLUDED_COM_SUN_STAR_XML_SAX_INPUTSOURCE_HPP

#include "sal/config.h"

#include "com/sun/star/xml/sax/InputSource.hdl"

#include "com/sun/star/io/XInputStream.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

inline InputSource::InputSource() SAL_THROW(())
    : aInputStream()
    , sEncoding()
    , sPublicId()
    , sSystemId()
{
}

inline InputSource::InputSource(const ::css::uno::Reference< ::css::io::XInputStream >& aInputStream_, const ::rtl::OUString& sEncoding_, const ::rtl::OUString& sPublicId_, const ::rtl::OUString& sSystemId_) SAL_THROW(())
    : aInputStream(aInputStream_)
    , sEncoding(sEncoding_)
    , sPublicId(sPublicId_)
    , sSystemId(sSystemId_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax { namespace detail {

struct theInputSourceType : public rtl::StaticWithInit< ::css::uno::Type *, theInputSourceType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.xml.sax.InputSource" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::io::XInputStream > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.io.XInputStream" );
        ::rtl::OUString the_name0( "aInputStream" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "sEncoding" );
        ::rtl::OUString the_name2( "sPublicId" );
        ::rtl::OUString the_name3( "sSystemId" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::sax::InputSource const *) {
    return *detail::theInputSourceType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::sax::InputSource const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::sax::InputSource >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_SAX_INPUTSOURCE_HPP
