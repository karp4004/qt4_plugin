#ifndef INCLUDED_COM_SUN_STAR_XML_SAX_XEXTENDEDDOCUMENTHANDLER_HDL
#define INCLUDED_COM_SUN_STAR_XML_SAX_XEXTENDEDDOCUMENTHANDLER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/xml/sax/SAXException.hdl"
#include "com/sun/star/xml/sax/XDocumentHandler.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

class SAL_NO_VTABLE XExtendedDocumentHandler : public ::css::xml::sax::XDocumentHandler
{
public:

    // Methods
    virtual void SAL_CALL startCDATA() /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL endCDATA() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL comment( const ::rtl::OUString& sComment ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL allowLineBreak() /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL unknown( const ::rtl::OUString& sString ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XExtendedDocumentHandler() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::sax::XExtendedDocumentHandler const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::sax::XExtendedDocumentHandler > *) SAL_THROW(());

#endif
