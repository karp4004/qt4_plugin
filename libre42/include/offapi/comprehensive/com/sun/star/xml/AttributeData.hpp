#ifndef INCLUDED_COM_SUN_STAR_XML_ATTRIBUTEDATA_HPP
#define INCLUDED_COM_SUN_STAR_XML_ATTRIBUTEDATA_HPP

#include "sal/config.h"

#include "com/sun/star/xml/AttributeData.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace xml {

inline AttributeData::AttributeData() SAL_THROW(())
    : Namespace()
    , Type()
    , Value()
{
}

inline AttributeData::AttributeData(const ::rtl::OUString& Namespace_, const ::rtl::OUString& Type_, const ::rtl::OUString& Value_) SAL_THROW(())
    : Namespace(Namespace_)
    , Type(Type_)
    , Value(Value_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace xml { namespace detail {

struct theAttributeDataType : public rtl::StaticWithInit< ::css::uno::Type *, theAttributeDataType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.xml.AttributeData" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Namespace" );
        ::rtl::OUString the_name1( "Type" );
        ::rtl::OUString the_name2( "Value" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace xml {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::AttributeData const *) {
    return *detail::theAttributeDataType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::AttributeData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::AttributeData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_ATTRIBUTEDATA_HPP
