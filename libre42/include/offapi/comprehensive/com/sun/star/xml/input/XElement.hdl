#ifndef INCLUDED_COM_SUN_STAR_XML_INPUT_XELEMENT_HDL
#define INCLUDED_COM_SUN_STAR_XML_INPUT_XELEMENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
namespace com { namespace sun { namespace star { namespace xml { namespace input { class XAttributes; } } } } }
namespace com { namespace sun { namespace star { namespace xml { namespace input { class XElement; } } } } }
#include "com/sun/star/xml/sax/SAXException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace input {

class SAL_NO_VTABLE XElement : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::xml::input::XElement > SAL_CALL getParent() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getLocalName() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getUid() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::input::XAttributes > SAL_CALL getAttributes() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::xml::input::XElement > SAL_CALL startChildElement( ::sal_Int32 uid, const ::rtl::OUString& localName, const ::css::uno::Reference< ::css::xml::input::XAttributes >& xAttributes ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL characters( const ::rtl::OUString& chars ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL ignorableWhitespace( const ::rtl::OUString& whitespace ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL processingInstruction( const ::rtl::OUString& target, const ::rtl::OUString& data ) /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL endElement() /* throw (::css::xml::sax::SAXException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XElement() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::input::XElement const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::input::XElement > *) SAL_THROW(());

#endif
