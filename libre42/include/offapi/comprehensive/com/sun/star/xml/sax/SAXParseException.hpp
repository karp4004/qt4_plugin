#ifndef INCLUDED_COM_SUN_STAR_XML_SAX_SAXPARSEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XML_SAX_SAXPARSEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xml/sax/SAXParseException.hdl"

#include "com/sun/star/xml/sax/SAXException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

inline SAXParseException::SAXParseException() SAL_THROW(())
    : ::css::xml::sax::SAXException()
    , PublicId()
    , SystemId()
    , LineNumber(0)
    , ColumnNumber(0)
{
    ::cppu::UnoType< ::css::xml::sax::SAXParseException >::get();
}

inline SAXParseException::SAXParseException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Any& WrappedException_, const ::rtl::OUString& PublicId_, const ::rtl::OUString& SystemId_, const ::sal_Int32& LineNumber_, const ::sal_Int32& ColumnNumber_) SAL_THROW(())
    : ::css::xml::sax::SAXException(Message_, Context_, WrappedException_)
    , PublicId(PublicId_)
    , SystemId(SystemId_)
    , LineNumber(LineNumber_)
    , ColumnNumber(ColumnNumber_)
{
    ::cppu::UnoType< ::css::xml::sax::SAXParseException >::get();
}

SAXParseException::SAXParseException(SAXParseException const & the_other): ::css::xml::sax::SAXException(the_other), PublicId(the_other.PublicId), SystemId(the_other.SystemId), LineNumber(the_other.LineNumber), ColumnNumber(the_other.ColumnNumber) {}

SAXParseException::~SAXParseException() {}

SAXParseException & SAXParseException::operator =(SAXParseException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::xml::sax::SAXException::operator =(the_other);
    PublicId = the_other.PublicId;
    SystemId = the_other.SystemId;
    LineNumber = the_other.LineNumber;
    ColumnNumber = the_other.ColumnNumber;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax { namespace detail {

struct theSAXParseExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theSAXParseExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.sax.SAXParseException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::xml::sax::SAXException >::get();

        typelib_CompoundMember_Init aMembers[4];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "PublicId" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "SystemId" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "long" );
        ::rtl::OUString sMemberName2( "LineNumber" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;
        ::rtl::OUString sMemberType3( "long" );
        ::rtl::OUString sMemberName3( "ColumnNumber" );
        aMembers[3].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[3].pTypeName = sMemberType3.pData;
        aMembers[3].pMemberName = sMemberName3.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            4,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace sax {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::sax::SAXParseException const *) {
    return *detail::theSAXParseExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::sax::SAXParseException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::sax::SAXParseException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_SAX_SAXPARSEEXCEPTION_HPP
