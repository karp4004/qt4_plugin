#ifndef INCLUDED_COM_SUN_STAR_XML_XPATH_LIBXML2EXTENSIONHANDLE_HPP
#define INCLUDED_COM_SUN_STAR_XML_XPATH_LIBXML2EXTENSIONHANDLE_HPP

#include "sal/config.h"

#include "com/sun/star/xml/xpath/Libxml2ExtensionHandle.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace xml { namespace xpath {

inline Libxml2ExtensionHandle::Libxml2ExtensionHandle() SAL_THROW(())
    : functionLookupFunction(0)
    , functionData(0)
    , variableLookupFunction(0)
    , variableData(0)
{
}

inline Libxml2ExtensionHandle::Libxml2ExtensionHandle(const ::sal_Int64& functionLookupFunction_, const ::sal_Int64& functionData_, const ::sal_Int64& variableLookupFunction_, const ::sal_Int64& variableData_) SAL_THROW(())
    : functionLookupFunction(functionLookupFunction_)
    , functionData(functionData_)
    , variableLookupFunction(variableLookupFunction_)
    , variableData(variableData_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace xpath { namespace detail {

struct theLibxml2ExtensionHandleType : public rtl::StaticWithInit< ::css::uno::Type *, theLibxml2ExtensionHandleType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.xml.xpath.Libxml2ExtensionHandle" );
        ::rtl::OUString the_tname0( "hyper" );
        ::rtl::OUString the_name0( "functionLookupFunction" );
        ::rtl::OUString the_name1( "functionData" );
        ::rtl::OUString the_name2( "variableLookupFunction" );
        ::rtl::OUString the_name3( "variableData" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_HYPER, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_HYPER, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_HYPER, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_HYPER, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace xpath {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::xpath::Libxml2ExtensionHandle const *) {
    return *detail::theLibxml2ExtensionHandleType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::xpath::Libxml2ExtensionHandle const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::xpath::Libxml2ExtensionHandle >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_XPATH_LIBXML2EXTENSIONHANDLE_HPP
