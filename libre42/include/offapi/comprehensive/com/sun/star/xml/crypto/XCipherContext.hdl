#ifndef INCLUDED_COM_SUN_STAR_XML_CRYPTO_XCIPHERCONTEXT_HDL
#define INCLUDED_COM_SUN_STAR_XML_CRYPTO_XCIPHERCONTEXT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/DisposedException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace crypto {

class SAL_NO_VTABLE XCipherContext : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::sal_Int8 > SAL_CALL convertWithCipherContext( const ::css::uno::Sequence< ::sal_Int8 >& aData ) /* throw (::css::lang::IllegalArgumentException, ::css::lang::DisposedException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int8 > SAL_CALL finalizeCipherContextAndDispose() /* throw (::css::lang::DisposedException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XCipherContext() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::crypto::XCipherContext const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::xml::crypto::XCipherContext > *) SAL_THROW(());

#endif
