#ifndef INCLUDED_COM_SUN_STAR_XML_CRYPTO_SECURITYOPERATIONSTATUS_HPP
#define INCLUDED_COM_SUN_STAR_XML_CRYPTO_SECURITYOPERATIONSTATUS_HPP

#include "sal/config.h"

#include "com/sun/star/xml/crypto/SecurityOperationStatus.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace crypto { namespace detail {

struct theSecurityOperationStatusType : public rtl::StaticWithInit< ::css::uno::Type *, theSecurityOperationStatusType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.crypto.SecurityOperationStatus" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[50];
        ::rtl::OUString sEnumValue0( "UNKNOWN" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "OPERATION_SUCCEEDED" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "RUNTIMEERROR_FAILED" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "ENGINE_FAILED" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "MALLOC_FAILED" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "STRDUP_FAILED" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "CRYPTO_FAILED" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "XML_FAILED" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "XSLT_FAILED" );
        enumValueNames[8] = sEnumValue8.pData;
        ::rtl::OUString sEnumValue9( "IO_FAILED" );
        enumValueNames[9] = sEnumValue9.pData;
        ::rtl::OUString sEnumValue10( "DISABLED" );
        enumValueNames[10] = sEnumValue10.pData;
        ::rtl::OUString sEnumValue11( "NOT_IMPLEMENTED" );
        enumValueNames[11] = sEnumValue11.pData;
        ::rtl::OUString sEnumValue12( "INVALID_SIZE" );
        enumValueNames[12] = sEnumValue12.pData;
        ::rtl::OUString sEnumValue13( "INVALID_DATA" );
        enumValueNames[13] = sEnumValue13.pData;
        ::rtl::OUString sEnumValue14( "INVALID_RESULT" );
        enumValueNames[14] = sEnumValue14.pData;
        ::rtl::OUString sEnumValue15( "INVALID_TYPE" );
        enumValueNames[15] = sEnumValue15.pData;
        ::rtl::OUString sEnumValue16( "INVALID_OPERATION" );
        enumValueNames[16] = sEnumValue16.pData;
        ::rtl::OUString sEnumValue17( "INVALID_STATUS" );
        enumValueNames[17] = sEnumValue17.pData;
        ::rtl::OUString sEnumValue18( "INVALID_FORMAT" );
        enumValueNames[18] = sEnumValue18.pData;
        ::rtl::OUString sEnumValue19( "DATA_NOT_MATCH" );
        enumValueNames[19] = sEnumValue19.pData;
        ::rtl::OUString sEnumValue20( "INVALID_NODE" );
        enumValueNames[20] = sEnumValue20.pData;
        ::rtl::OUString sEnumValue21( "INVALID_NODE_CONTENT" );
        enumValueNames[21] = sEnumValue21.pData;
        ::rtl::OUString sEnumValue22( "INVALID_NODE_ATTRIBUTE" );
        enumValueNames[22] = sEnumValue22.pData;
        ::rtl::OUString sEnumValue23( "MISSING_NODE_ATTRIBUTE" );
        enumValueNames[23] = sEnumValue23.pData;
        ::rtl::OUString sEnumValue24( "NODE_ALREADY_PRESENT" );
        enumValueNames[24] = sEnumValue24.pData;
        ::rtl::OUString sEnumValue25( "UNEXPECTED_NODE" );
        enumValueNames[25] = sEnumValue25.pData;
        ::rtl::OUString sEnumValue26( "NODE_NOT_FOUND" );
        enumValueNames[26] = sEnumValue26.pData;
        ::rtl::OUString sEnumValue27( "INVALID_TRANSFORM" );
        enumValueNames[27] = sEnumValue27.pData;
        ::rtl::OUString sEnumValue28( "INVALID_TRANSFORM_KEY" );
        enumValueNames[28] = sEnumValue28.pData;
        ::rtl::OUString sEnumValue29( "INVALID_URI_TYPE" );
        enumValueNames[29] = sEnumValue29.pData;
        ::rtl::OUString sEnumValue30( "TRANSFORM_SAME_DOCUMENT_REQUIRED" );
        enumValueNames[30] = sEnumValue30.pData;
        ::rtl::OUString sEnumValue31( "TRANSFORM_DISABLED" );
        enumValueNames[31] = sEnumValue31.pData;
        ::rtl::OUString sEnumValue32( "INVALID_KEY_DATA" );
        enumValueNames[32] = sEnumValue32.pData;
        ::rtl::OUString sEnumValue33( "KEY_DATA_NOT_FOUND" );
        enumValueNames[33] = sEnumValue33.pData;
        ::rtl::OUString sEnumValue34( "KEY_DATA_ALREADY_EXIST" );
        enumValueNames[34] = sEnumValue34.pData;
        ::rtl::OUString sEnumValue35( "INVALID_KEY_DATA_SIZE" );
        enumValueNames[35] = sEnumValue35.pData;
        ::rtl::OUString sEnumValue36( "KEY_NOT_FOUND" );
        enumValueNames[36] = sEnumValue36.pData;
        ::rtl::OUString sEnumValue37( "KEYDATA_DISABLED" );
        enumValueNames[37] = sEnumValue37.pData;
        ::rtl::OUString sEnumValue38( "MAX_RETRIEVALS_LEVEL" );
        enumValueNames[38] = sEnumValue38.pData;
        ::rtl::OUString sEnumValue39( "MAX_RETRIEVAL_TYPE_MISMATCH" );
        enumValueNames[39] = sEnumValue39.pData;
        ::rtl::OUString sEnumValue40( "MAX_ENCKEY_LEVEL" );
        enumValueNames[40] = sEnumValue40.pData;
        ::rtl::OUString sEnumValue41( "CERT_VERIFY_FAILED" );
        enumValueNames[41] = sEnumValue41.pData;
        ::rtl::OUString sEnumValue42( "CERT_NOT_FOUND" );
        enumValueNames[42] = sEnumValue42.pData;
        ::rtl::OUString sEnumValue43( "CERT_REVOKED" );
        enumValueNames[43] = sEnumValue43.pData;
        ::rtl::OUString sEnumValue44( "CERT_ISSUER_FAILED" );
        enumValueNames[44] = sEnumValue44.pData;
        ::rtl::OUString sEnumValue45( "CERT_NOT_YET_VALID" );
        enumValueNames[45] = sEnumValue45.pData;
        ::rtl::OUString sEnumValue46( "CERT_HAS_EXPIRED" );
        enumValueNames[46] = sEnumValue46.pData;
        ::rtl::OUString sEnumValue47( "DSIG_NO_REFERENCES" );
        enumValueNames[47] = sEnumValue47.pData;
        ::rtl::OUString sEnumValue48( "DSIG_INVALID_REFERENCE" );
        enumValueNames[48] = sEnumValue48.pData;
        ::rtl::OUString sEnumValue49( "ASSERTION" );
        enumValueNames[49] = sEnumValue49.pData;

        sal_Int32 enumValues[50];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;
        enumValues[9] = 9;
        enumValues[10] = 10;
        enumValues[11] = 11;
        enumValues[12] = 12;
        enumValues[13] = 13;
        enumValues[14] = 14;
        enumValues[15] = 15;
        enumValues[16] = 16;
        enumValues[17] = 17;
        enumValues[18] = 18;
        enumValues[19] = 19;
        enumValues[20] = 20;
        enumValues[21] = 21;
        enumValues[22] = 22;
        enumValues[23] = 23;
        enumValues[24] = 24;
        enumValues[25] = 25;
        enumValues[26] = 26;
        enumValues[27] = 27;
        enumValues[28] = 28;
        enumValues[29] = 29;
        enumValues[30] = 30;
        enumValues[31] = 31;
        enumValues[32] = 32;
        enumValues[33] = 33;
        enumValues[34] = 34;
        enumValues[35] = 35;
        enumValues[36] = 36;
        enumValues[37] = 37;
        enumValues[38] = 38;
        enumValues[39] = 39;
        enumValues[40] = 40;
        enumValues[41] = 41;
        enumValues[42] = 42;
        enumValues[43] = 43;
        enumValues[44] = 44;
        enumValues[45] = 45;
        enumValues[46] = 46;
        enumValues[47] = 47;
        enumValues[48] = 48;
        enumValues[49] = 49;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::xml::crypto::SecurityOperationStatus_UNKNOWN,
            50, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace crypto {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::crypto::SecurityOperationStatus const *) {
    return *detail::theSecurityOperationStatusType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::crypto::SecurityOperationStatus const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::crypto::SecurityOperationStatus >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_CRYPTO_SECURITYOPERATIONSTATUS_HPP
