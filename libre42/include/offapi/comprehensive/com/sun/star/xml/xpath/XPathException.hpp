#ifndef INCLUDED_COM_SUN_STAR_XML_XPATH_XPATHEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_XML_XPATH_XPATHEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/xml/xpath/XPathException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace xml { namespace xpath {

inline XPathException::XPathException() SAL_THROW(())
    : ::css::uno::Exception()
{
    ::cppu::UnoType< ::css::xml::xpath::XPathException >::get();
}

inline XPathException::XPathException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{
    ::cppu::UnoType< ::css::xml::xpath::XPathException >::get();
}

XPathException::XPathException(XPathException const & the_other): ::css::uno::Exception(the_other) {}

XPathException::~XPathException() {}

XPathException & XPathException::operator =(XPathException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace xpath { namespace detail {

struct theXPathExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theXPathExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.xml.xpath.XPathException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace xml { namespace xpath {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::xml::xpath::XPathException const *) {
    return *detail::theXPathExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::xml::xpath::XPathException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::xml::xpath::XPathException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_XML_XPATH_XPATHEXCEPTION_HPP
