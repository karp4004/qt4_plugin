#ifndef INCLUDED_COM_SUN_STAR_AWT_VCLCONTAINEREVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_VCLCONTAINEREVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/VclContainerEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline VclContainerEvent::VclContainerEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Child()
{
}

inline VclContainerEvent::VclContainerEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::uno::XInterface >& Child_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Child(Child_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theVclContainerEventType : public rtl::StaticWithInit< ::css::uno::Type *, theVclContainerEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.VclContainerEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.uno.XInterface" );
        ::rtl::OUString the_name0( "Child" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::VclContainerEvent const *) {
    return *detail::theVclContainerEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::VclContainerEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::VclContainerEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_VCLCONTAINEREVENT_HPP
