#ifndef INCLUDED_COM_SUN_STAR_AWT_GRADIENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRADIENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/Gradient.hdl"

#include "com/sun/star/awt/GradientStyle.hpp"
#include "com/sun/star/util/Color.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline Gradient::Gradient() SAL_THROW(())
    : Style(::css::awt::GradientStyle_LINEAR)
    , StartColor(0)
    , EndColor(0)
    , Angle(0)
    , Border(0)
    , XOffset(0)
    , YOffset(0)
    , StartIntensity(0)
    , EndIntensity(0)
    , StepCount(0)
{
}

inline Gradient::Gradient(const ::css::awt::GradientStyle& Style_, const ::sal_Int32& StartColor_, const ::sal_Int32& EndColor_, const ::sal_Int16& Angle_, const ::sal_Int16& Border_, const ::sal_Int16& XOffset_, const ::sal_Int16& YOffset_, const ::sal_Int16& StartIntensity_, const ::sal_Int16& EndIntensity_, const ::sal_Int16& StepCount_) SAL_THROW(())
    : Style(Style_)
    , StartColor(StartColor_)
    , EndColor(EndColor_)
    , Angle(Angle_)
    , Border(Border_)
    , XOffset(XOffset_)
    , YOffset(YOffset_)
    , StartIntensity(StartIntensity_)
    , EndIntensity(EndIntensity_)
    , StepCount(StepCount_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theGradientType : public rtl::StaticWithInit< ::css::uno::Type *, theGradientType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.Gradient" );
        ::cppu::UnoType< ::css::awt::GradientStyle >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.GradientStyle" );
        ::rtl::OUString the_name0( "Style" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "StartColor" );
        ::rtl::OUString the_name2( "EndColor" );
        ::rtl::OUString the_tname2( "short" );
        ::rtl::OUString the_name3( "Angle" );
        ::rtl::OUString the_name4( "Border" );
        ::rtl::OUString the_name5( "XOffset" );
        ::rtl::OUString the_name6( "YOffset" );
        ::rtl::OUString the_name7( "StartIntensity" );
        ::rtl::OUString the_name8( "EndIntensity" );
        ::rtl::OUString the_name9( "StepCount" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name4.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name6.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name7.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name8.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name9.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 10, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::Gradient const *) {
    return *detail::theGradientType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::Gradient const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::Gradient >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRADIENT_HPP
