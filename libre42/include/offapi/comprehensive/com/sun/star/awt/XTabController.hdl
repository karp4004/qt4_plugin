#ifndef INCLUDED_COM_SUN_STAR_AWT_XTABCONTROLLER_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XTABCONTROLLER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { class XControl; } } } }
namespace com { namespace sun { namespace star { namespace awt { class XControlContainer; } } } }
namespace com { namespace sun { namespace star { namespace awt { class XTabControllerModel; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XTabController : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setModel( const ::css::uno::Reference< ::css::awt::XTabControllerModel >& Model ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::XTabControllerModel > SAL_CALL getModel() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setContainer( const ::css::uno::Reference< ::css::awt::XControlContainer >& Container ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::XControlContainer > SAL_CALL getContainer() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::awt::XControl > > SAL_CALL getControls() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL autoTabOrder() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL activateTabOrder() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL activateFirst() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL activateLast() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTabController() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XTabController const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XTabController > *) SAL_THROW(());

#endif
