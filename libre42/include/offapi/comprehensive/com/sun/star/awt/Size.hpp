#ifndef INCLUDED_COM_SUN_STAR_AWT_SIZE_HPP
#define INCLUDED_COM_SUN_STAR_AWT_SIZE_HPP

#include "sal/config.h"

#include "com/sun/star/awt/Size.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline Size::Size() SAL_THROW(())
    : Width(0)
    , Height(0)
{
}

inline Size::Size(const ::sal_Int32& Width_, const ::sal_Int32& Height_) SAL_THROW(())
    : Width(Width_)
    , Height(Height_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theSizeType : public rtl::StaticWithInit< ::css::uno::Type *, theSizeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.Size" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Width" );
        ::rtl::OUString the_name1( "Height" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::Size const *) {
    return *detail::theSizeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::Size const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::Size >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_SIZE_HPP
