#ifndef INCLUDED_COM_SUN_STAR_AWT_XFONT_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XFONT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/FontDescriptor.hdl"
#include "com/sun/star/awt/SimpleFontMetric.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XFont : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::awt::FontDescriptor SAL_CALL getFontDescriptor() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::awt::SimpleFontMetric SAL_CALL getFontMetric() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getCharWidth( ::sal_Unicode c ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int16 > SAL_CALL getCharWidths( ::sal_Unicode nFirst, ::sal_Unicode nLast ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getStringWidth( const ::rtl::OUString& str ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getStringWidthArray( const ::rtl::OUString& str, ::css::uno::Sequence< ::sal_Int32 >& aDXArray ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL getKernPairs( ::css::uno::Sequence< ::sal_Unicode >& Chars1, ::css::uno::Sequence< ::sal_Unicode >& Chars2, ::css::uno::Sequence< ::sal_Int16 >& Kerns ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFont() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XFont const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XFont > *) SAL_THROW(());

#endif
