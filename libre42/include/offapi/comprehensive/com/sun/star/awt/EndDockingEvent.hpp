#ifndef INCLUDED_COM_SUN_STAR_AWT_ENDDOCKINGEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_ENDDOCKINGEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/EndDockingEvent.hdl"

#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline EndDockingEvent::EndDockingEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , WindowRectangle()
    , bFloating(false)
    , bCancelled(false)
{
}

inline EndDockingEvent::EndDockingEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::awt::Rectangle& WindowRectangle_, const ::sal_Bool& bFloating_, const ::sal_Bool& bCancelled_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , WindowRectangle(WindowRectangle_)
    , bFloating(bFloating_)
    , bCancelled(bCancelled_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theEndDockingEventType : public rtl::StaticWithInit< ::css::uno::Type *, theEndDockingEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.EndDockingEvent" );
        ::cppu::UnoType< ::css::awt::Rectangle >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.Rectangle" );
        ::rtl::OUString the_name0( "WindowRectangle" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "bFloating" );
        ::rtl::OUString the_name2( "bCancelled" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::EndDockingEvent const *) {
    return *detail::theEndDockingEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::EndDockingEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::EndDockingEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_ENDDOCKINGEVENT_HPP
