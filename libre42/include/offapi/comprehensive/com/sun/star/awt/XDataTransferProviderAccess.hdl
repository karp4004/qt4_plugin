#ifndef INCLUDED_COM_SUN_STAR_AWT_XDATATRANSFERPROVIDERACCESS_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XDATATRANSFERPROVIDERACCESS_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { class XWindow; } } } }
namespace com { namespace sun { namespace star { namespace datatransfer { namespace clipboard { class XClipboard; } } } } }
namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd { class XDragGestureRecognizer; } } } } }
namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd { class XDragSource; } } } } }
namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd { class XDropTarget; } } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XDataTransferProviderAccess : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::datatransfer::dnd::XDragGestureRecognizer > SAL_CALL getDragGestureRecognizer( const ::css::uno::Reference< ::css::awt::XWindow >& window ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::datatransfer::dnd::XDragSource > SAL_CALL getDragSource( const ::css::uno::Reference< ::css::awt::XWindow >& window ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::datatransfer::dnd::XDropTarget > SAL_CALL getDropTarget( const ::css::uno::Reference< ::css::awt::XWindow >& window ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::datatransfer::clipboard::XClipboard > SAL_CALL getClipboard( const ::rtl::OUString& clipboardName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDataTransferProviderAccess() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XDataTransferProviderAccess const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XDataTransferProviderAccess > *) SAL_THROW(());

#endif
