#ifndef INCLUDED_COM_SUN_STAR_AWT_XFIXEDTEXT_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XFIXEDTEXT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XFixedText : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setText( const ::rtl::OUString& Text ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getText() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setAlignment( ::sal_Int16 nAlign ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getAlignment() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFixedText() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XFixedText const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XFixedText > *) SAL_THROW(());

#endif
