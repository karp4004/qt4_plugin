#ifndef INCLUDED_COM_SUN_STAR_AWT_XSTYLESETTINGS_HPP
#define INCLUDED_COM_SUN_STAR_AWT_XSTYLESETTINGS_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/XStyleSettings.hdl"

#include "com/sun/star/awt/FontDescriptor.hpp"
#include "com/sun/star/awt/XStyleChangeListener.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/util/Color.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theXStyleSettingsType : public rtl::StaticWithInit< ::css::uno::Type *, theXStyleSettingsType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.awt.XStyleSettings" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[57] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.awt.XStyleSettings::ActiveBorderColor" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.awt.XStyleSettings::ActiveColor" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.awt.XStyleSettings::ActiveTabColor" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.awt.XStyleSettings::ActiveTextColor" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.awt.XStyleSettings::ButtonRolloverTextColor" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.awt.XStyleSettings::ButtonTextColor" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.awt.XStyleSettings::CheckedColor" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.awt.XStyleSettings::DarkShadowColor" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.awt.XStyleSettings::DeactiveBorderColor" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.awt.XStyleSettings::DeactiveColor" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.awt.XStyleSettings::DeactiveTextColor" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.awt.XStyleSettings::DialogColor" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.awt.XStyleSettings::DialogTextColor" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );
        ::rtl::OUString sAttributeName13( "com.sun.star.awt.XStyleSettings::DisableColor" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName13.pData );
        ::rtl::OUString sAttributeName14( "com.sun.star.awt.XStyleSettings::FaceColor" );
        typelib_typedescriptionreference_new( &pMembers[14],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName14.pData );
        ::rtl::OUString sAttributeName15( "com.sun.star.awt.XStyleSettings::FaceGradientColor" );
        typelib_typedescriptionreference_new( &pMembers[15],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName15.pData );
        ::rtl::OUString sAttributeName16( "com.sun.star.awt.XStyleSettings::FieldColor" );
        typelib_typedescriptionreference_new( &pMembers[16],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName16.pData );
        ::rtl::OUString sAttributeName17( "com.sun.star.awt.XStyleSettings::FieldRolloverTextColor" );
        typelib_typedescriptionreference_new( &pMembers[17],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName17.pData );
        ::rtl::OUString sAttributeName18( "com.sun.star.awt.XStyleSettings::FieldTextColor" );
        typelib_typedescriptionreference_new( &pMembers[18],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName18.pData );
        ::rtl::OUString sAttributeName19( "com.sun.star.awt.XStyleSettings::GroupTextColor" );
        typelib_typedescriptionreference_new( &pMembers[19],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName19.pData );
        ::rtl::OUString sAttributeName20( "com.sun.star.awt.XStyleSettings::HelpColor" );
        typelib_typedescriptionreference_new( &pMembers[20],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName20.pData );
        ::rtl::OUString sAttributeName21( "com.sun.star.awt.XStyleSettings::HelpTextColor" );
        typelib_typedescriptionreference_new( &pMembers[21],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName21.pData );
        ::rtl::OUString sAttributeName22( "com.sun.star.awt.XStyleSettings::HighlightColor" );
        typelib_typedescriptionreference_new( &pMembers[22],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName22.pData );
        ::rtl::OUString sAttributeName23( "com.sun.star.awt.XStyleSettings::HighlightTextColor" );
        typelib_typedescriptionreference_new( &pMembers[23],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName23.pData );
        ::rtl::OUString sAttributeName24( "com.sun.star.awt.XStyleSettings::InactiveTabColor" );
        typelib_typedescriptionreference_new( &pMembers[24],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName24.pData );
        ::rtl::OUString sAttributeName25( "com.sun.star.awt.XStyleSettings::InfoTextColor" );
        typelib_typedescriptionreference_new( &pMembers[25],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName25.pData );
        ::rtl::OUString sAttributeName26( "com.sun.star.awt.XStyleSettings::LabelTextColor" );
        typelib_typedescriptionreference_new( &pMembers[26],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName26.pData );
        ::rtl::OUString sAttributeName27( "com.sun.star.awt.XStyleSettings::LightColor" );
        typelib_typedescriptionreference_new( &pMembers[27],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName27.pData );
        ::rtl::OUString sAttributeName28( "com.sun.star.awt.XStyleSettings::MenuBarColor" );
        typelib_typedescriptionreference_new( &pMembers[28],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName28.pData );
        ::rtl::OUString sAttributeName29( "com.sun.star.awt.XStyleSettings::MenuBarTextColor" );
        typelib_typedescriptionreference_new( &pMembers[29],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName29.pData );
        ::rtl::OUString sAttributeName30( "com.sun.star.awt.XStyleSettings::MenuBorderColor" );
        typelib_typedescriptionreference_new( &pMembers[30],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName30.pData );
        ::rtl::OUString sAttributeName31( "com.sun.star.awt.XStyleSettings::MenuColor" );
        typelib_typedescriptionreference_new( &pMembers[31],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName31.pData );
        ::rtl::OUString sAttributeName32( "com.sun.star.awt.XStyleSettings::MenuHighlightColor" );
        typelib_typedescriptionreference_new( &pMembers[32],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName32.pData );
        ::rtl::OUString sAttributeName33( "com.sun.star.awt.XStyleSettings::MenuHighlightTextColor" );
        typelib_typedescriptionreference_new( &pMembers[33],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName33.pData );
        ::rtl::OUString sAttributeName34( "com.sun.star.awt.XStyleSettings::MenuTextColor" );
        typelib_typedescriptionreference_new( &pMembers[34],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName34.pData );
        ::rtl::OUString sAttributeName35( "com.sun.star.awt.XStyleSettings::MonoColor" );
        typelib_typedescriptionreference_new( &pMembers[35],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName35.pData );
        ::rtl::OUString sAttributeName36( "com.sun.star.awt.XStyleSettings::RadioCheckTextColor" );
        typelib_typedescriptionreference_new( &pMembers[36],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName36.pData );
        ::rtl::OUString sAttributeName37( "com.sun.star.awt.XStyleSettings::SeparatorColor" );
        typelib_typedescriptionreference_new( &pMembers[37],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName37.pData );
        ::rtl::OUString sAttributeName38( "com.sun.star.awt.XStyleSettings::ShadowColor" );
        typelib_typedescriptionreference_new( &pMembers[38],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName38.pData );
        ::rtl::OUString sAttributeName39( "com.sun.star.awt.XStyleSettings::WindowColor" );
        typelib_typedescriptionreference_new( &pMembers[39],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName39.pData );
        ::rtl::OUString sAttributeName40( "com.sun.star.awt.XStyleSettings::WindowTextColor" );
        typelib_typedescriptionreference_new( &pMembers[40],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName40.pData );
        ::rtl::OUString sAttributeName41( "com.sun.star.awt.XStyleSettings::WorkspaceColor" );
        typelib_typedescriptionreference_new( &pMembers[41],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName41.pData );
        ::rtl::OUString sAttributeName42( "com.sun.star.awt.XStyleSettings::HighContrastMode" );
        typelib_typedescriptionreference_new( &pMembers[42],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName42.pData );
        ::rtl::OUString sAttributeName43( "com.sun.star.awt.XStyleSettings::ApplicationFont" );
        typelib_typedescriptionreference_new( &pMembers[43],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName43.pData );
        ::rtl::OUString sAttributeName44( "com.sun.star.awt.XStyleSettings::HelpFont" );
        typelib_typedescriptionreference_new( &pMembers[44],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName44.pData );
        ::rtl::OUString sAttributeName45( "com.sun.star.awt.XStyleSettings::TitleFont" );
        typelib_typedescriptionreference_new( &pMembers[45],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName45.pData );
        ::rtl::OUString sAttributeName46( "com.sun.star.awt.XStyleSettings::FloatTitleFont" );
        typelib_typedescriptionreference_new( &pMembers[46],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName46.pData );
        ::rtl::OUString sAttributeName47( "com.sun.star.awt.XStyleSettings::MenuFont" );
        typelib_typedescriptionreference_new( &pMembers[47],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName47.pData );
        ::rtl::OUString sAttributeName48( "com.sun.star.awt.XStyleSettings::ToolFont" );
        typelib_typedescriptionreference_new( &pMembers[48],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName48.pData );
        ::rtl::OUString sAttributeName49( "com.sun.star.awt.XStyleSettings::GroupFont" );
        typelib_typedescriptionreference_new( &pMembers[49],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName49.pData );
        ::rtl::OUString sAttributeName50( "com.sun.star.awt.XStyleSettings::LabelFont" );
        typelib_typedescriptionreference_new( &pMembers[50],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName50.pData );
        ::rtl::OUString sAttributeName51( "com.sun.star.awt.XStyleSettings::InfoFont" );
        typelib_typedescriptionreference_new( &pMembers[51],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName51.pData );
        ::rtl::OUString sAttributeName52( "com.sun.star.awt.XStyleSettings::RadioCheckFont" );
        typelib_typedescriptionreference_new( &pMembers[52],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName52.pData );
        ::rtl::OUString sAttributeName53( "com.sun.star.awt.XStyleSettings::PushButtonFont" );
        typelib_typedescriptionreference_new( &pMembers[53],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName53.pData );
        ::rtl::OUString sAttributeName54( "com.sun.star.awt.XStyleSettings::FieldFont" );
        typelib_typedescriptionreference_new( &pMembers[54],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName54.pData );
        ::rtl::OUString sMethodName0( "com.sun.star.awt.XStyleSettings::addStyleChangeListener" );
        typelib_typedescriptionreference_new( &pMembers[55],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );
        ::rtl::OUString sMethodName1( "com.sun.star.awt.XStyleSettings::removeStyleChangeListener" );
        typelib_typedescriptionreference_new( &pMembers[56],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName1.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            57,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescriptionreference_release( pMembers[14] );
        typelib_typedescriptionreference_release( pMembers[15] );
        typelib_typedescriptionreference_release( pMembers[16] );
        typelib_typedescriptionreference_release( pMembers[17] );
        typelib_typedescriptionreference_release( pMembers[18] );
        typelib_typedescriptionreference_release( pMembers[19] );
        typelib_typedescriptionreference_release( pMembers[20] );
        typelib_typedescriptionreference_release( pMembers[21] );
        typelib_typedescriptionreference_release( pMembers[22] );
        typelib_typedescriptionreference_release( pMembers[23] );
        typelib_typedescriptionreference_release( pMembers[24] );
        typelib_typedescriptionreference_release( pMembers[25] );
        typelib_typedescriptionreference_release( pMembers[26] );
        typelib_typedescriptionreference_release( pMembers[27] );
        typelib_typedescriptionreference_release( pMembers[28] );
        typelib_typedescriptionreference_release( pMembers[29] );
        typelib_typedescriptionreference_release( pMembers[30] );
        typelib_typedescriptionreference_release( pMembers[31] );
        typelib_typedescriptionreference_release( pMembers[32] );
        typelib_typedescriptionreference_release( pMembers[33] );
        typelib_typedescriptionreference_release( pMembers[34] );
        typelib_typedescriptionreference_release( pMembers[35] );
        typelib_typedescriptionreference_release( pMembers[36] );
        typelib_typedescriptionreference_release( pMembers[37] );
        typelib_typedescriptionreference_release( pMembers[38] );
        typelib_typedescriptionreference_release( pMembers[39] );
        typelib_typedescriptionreference_release( pMembers[40] );
        typelib_typedescriptionreference_release( pMembers[41] );
        typelib_typedescriptionreference_release( pMembers[42] );
        typelib_typedescriptionreference_release( pMembers[43] );
        typelib_typedescriptionreference_release( pMembers[44] );
        typelib_typedescriptionreference_release( pMembers[45] );
        typelib_typedescriptionreference_release( pMembers[46] );
        typelib_typedescriptionreference_release( pMembers[47] );
        typelib_typedescriptionreference_release( pMembers[48] );
        typelib_typedescriptionreference_release( pMembers[49] );
        typelib_typedescriptionreference_release( pMembers[50] );
        typelib_typedescriptionreference_release( pMembers[51] );
        typelib_typedescriptionreference_release( pMembers[52] );
        typelib_typedescriptionreference_release( pMembers[53] );
        typelib_typedescriptionreference_release( pMembers[54] );
        typelib_typedescriptionreference_release( pMembers[55] );
        typelib_typedescriptionreference_release( pMembers[56] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XStyleSettings const *) {
    const ::css::uno::Type &rRet = *detail::theXStyleSettingsType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::awt::FontDescriptor >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "long" );
                ::rtl::OUString sAttributeName0( "com.sun.star.awt.XStyleSettings::ActiveBorderColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    3, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "long" );
                ::rtl::OUString sAttributeName1( "com.sun.star.awt.XStyleSettings::ActiveColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    4, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "long" );
                ::rtl::OUString sAttributeName2( "com.sun.star.awt.XStyleSettings::ActiveTabColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    5, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "long" );
                ::rtl::OUString sAttributeName3( "com.sun.star.awt.XStyleSettings::ActiveTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    6, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "long" );
                ::rtl::OUString sAttributeName4( "com.sun.star.awt.XStyleSettings::ButtonRolloverTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "long" );
                ::rtl::OUString sAttributeName5( "com.sun.star.awt.XStyleSettings::ButtonTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    8, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType5.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "long" );
                ::rtl::OUString sAttributeName6( "com.sun.star.awt.XStyleSettings::CheckedColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    9, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType6.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "long" );
                ::rtl::OUString sAttributeName7( "com.sun.star.awt.XStyleSettings::DarkShadowColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    10, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType7.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "long" );
                ::rtl::OUString sAttributeName8( "com.sun.star.awt.XStyleSettings::DeactiveBorderColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    11, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType8.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "long" );
                ::rtl::OUString sAttributeName9( "com.sun.star.awt.XStyleSettings::DeactiveColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    12, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType9.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "long" );
                ::rtl::OUString sAttributeName10( "com.sun.star.awt.XStyleSettings::DeactiveTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    13, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType10.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "long" );
                ::rtl::OUString sAttributeName11( "com.sun.star.awt.XStyleSettings::DialogColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    14, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType11.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "long" );
                ::rtl::OUString sAttributeName12( "com.sun.star.awt.XStyleSettings::DialogTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    15, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType12.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType13( "long" );
                ::rtl::OUString sAttributeName13( "com.sun.star.awt.XStyleSettings::DisableColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    16, sAttributeName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType13.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType14( "long" );
                ::rtl::OUString sAttributeName14( "com.sun.star.awt.XStyleSettings::FaceColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    17, sAttributeName14.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType14.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType15( "long" );
                ::rtl::OUString sAttributeName15( "com.sun.star.awt.XStyleSettings::FaceGradientColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    18, sAttributeName15.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType15.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType16( "long" );
                ::rtl::OUString sAttributeName16( "com.sun.star.awt.XStyleSettings::FieldColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    19, sAttributeName16.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType16.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType17( "long" );
                ::rtl::OUString sAttributeName17( "com.sun.star.awt.XStyleSettings::FieldRolloverTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    20, sAttributeName17.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType17.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType18( "long" );
                ::rtl::OUString sAttributeName18( "com.sun.star.awt.XStyleSettings::FieldTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    21, sAttributeName18.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType18.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType19( "long" );
                ::rtl::OUString sAttributeName19( "com.sun.star.awt.XStyleSettings::GroupTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    22, sAttributeName19.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType19.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType20( "long" );
                ::rtl::OUString sAttributeName20( "com.sun.star.awt.XStyleSettings::HelpColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    23, sAttributeName20.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType20.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType21( "long" );
                ::rtl::OUString sAttributeName21( "com.sun.star.awt.XStyleSettings::HelpTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    24, sAttributeName21.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType21.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType22( "long" );
                ::rtl::OUString sAttributeName22( "com.sun.star.awt.XStyleSettings::HighlightColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    25, sAttributeName22.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType22.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType23( "long" );
                ::rtl::OUString sAttributeName23( "com.sun.star.awt.XStyleSettings::HighlightTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    26, sAttributeName23.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType23.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType24( "long" );
                ::rtl::OUString sAttributeName24( "com.sun.star.awt.XStyleSettings::InactiveTabColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    27, sAttributeName24.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType24.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType25( "long" );
                ::rtl::OUString sAttributeName25( "com.sun.star.awt.XStyleSettings::InfoTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    28, sAttributeName25.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType25.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType26( "long" );
                ::rtl::OUString sAttributeName26( "com.sun.star.awt.XStyleSettings::LabelTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    29, sAttributeName26.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType26.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType27( "long" );
                ::rtl::OUString sAttributeName27( "com.sun.star.awt.XStyleSettings::LightColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    30, sAttributeName27.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType27.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType28( "long" );
                ::rtl::OUString sAttributeName28( "com.sun.star.awt.XStyleSettings::MenuBarColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    31, sAttributeName28.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType28.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType29( "long" );
                ::rtl::OUString sAttributeName29( "com.sun.star.awt.XStyleSettings::MenuBarTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    32, sAttributeName29.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType29.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType30( "long" );
                ::rtl::OUString sAttributeName30( "com.sun.star.awt.XStyleSettings::MenuBorderColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    33, sAttributeName30.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType30.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType31( "long" );
                ::rtl::OUString sAttributeName31( "com.sun.star.awt.XStyleSettings::MenuColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    34, sAttributeName31.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType31.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType32( "long" );
                ::rtl::OUString sAttributeName32( "com.sun.star.awt.XStyleSettings::MenuHighlightColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    35, sAttributeName32.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType32.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType33( "long" );
                ::rtl::OUString sAttributeName33( "com.sun.star.awt.XStyleSettings::MenuHighlightTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    36, sAttributeName33.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType33.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType34( "long" );
                ::rtl::OUString sAttributeName34( "com.sun.star.awt.XStyleSettings::MenuTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    37, sAttributeName34.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType34.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType35( "long" );
                ::rtl::OUString sAttributeName35( "com.sun.star.awt.XStyleSettings::MonoColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    38, sAttributeName35.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType35.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType36( "long" );
                ::rtl::OUString sAttributeName36( "com.sun.star.awt.XStyleSettings::RadioCheckTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    39, sAttributeName36.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType36.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType37( "long" );
                ::rtl::OUString sAttributeName37( "com.sun.star.awt.XStyleSettings::SeparatorColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    40, sAttributeName37.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType37.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType38( "long" );
                ::rtl::OUString sAttributeName38( "com.sun.star.awt.XStyleSettings::ShadowColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    41, sAttributeName38.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType38.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType39( "long" );
                ::rtl::OUString sAttributeName39( "com.sun.star.awt.XStyleSettings::WindowColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    42, sAttributeName39.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType39.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType40( "long" );
                ::rtl::OUString sAttributeName40( "com.sun.star.awt.XStyleSettings::WindowTextColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    43, sAttributeName40.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType40.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType41( "long" );
                ::rtl::OUString sAttributeName41( "com.sun.star.awt.XStyleSettings::WorkspaceColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    44, sAttributeName41.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType41.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType42( "boolean" );
                ::rtl::OUString sAttributeName42( "com.sun.star.awt.XStyleSettings::HighContrastMode" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    45, sAttributeName42.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType42.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType43( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName43( "com.sun.star.awt.XStyleSettings::ApplicationFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    46, sAttributeName43.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType43.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType44( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName44( "com.sun.star.awt.XStyleSettings::HelpFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    47, sAttributeName44.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType44.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType45( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName45( "com.sun.star.awt.XStyleSettings::TitleFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    48, sAttributeName45.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType45.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType46( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName46( "com.sun.star.awt.XStyleSettings::FloatTitleFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    49, sAttributeName46.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType46.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType47( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName47( "com.sun.star.awt.XStyleSettings::MenuFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    50, sAttributeName47.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType47.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType48( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName48( "com.sun.star.awt.XStyleSettings::ToolFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    51, sAttributeName48.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType48.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType49( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName49( "com.sun.star.awt.XStyleSettings::GroupFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    52, sAttributeName49.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType49.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType50( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName50( "com.sun.star.awt.XStyleSettings::LabelFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    53, sAttributeName50.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType50.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType51( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName51( "com.sun.star.awt.XStyleSettings::InfoFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    54, sAttributeName51.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType51.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType52( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName52( "com.sun.star.awt.XStyleSettings::RadioCheckFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    55, sAttributeName52.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType52.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType53( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName53( "com.sun.star.awt.XStyleSettings::PushButtonFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    56, sAttributeName53.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType53.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType54( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName54( "com.sun.star.awt.XStyleSettings::FieldFont" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    57, sAttributeName54.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType54.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Listener" );
                ::rtl::OUString sParamType0( "com.sun.star.awt.XStyleChangeListener" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType0( "void" );
                ::rtl::OUString sMethodName0( "com.sun.star.awt.XStyleSettings::addStyleChangeListener" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    58, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType0.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "Listener" );
                ::rtl::OUString sParamType0( "com.sun.star.awt.XStyleChangeListener" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType1( "void" );
                ::rtl::OUString sMethodName1( "com.sun.star.awt.XStyleSettings::removeStyleChangeListener" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    59, sal_False,
                    sMethodName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_VOID, sReturnType1.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::awt::XStyleSettings > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::awt::XStyleSettings > >::get();
}

::css::uno::Type const & ::css::awt::XStyleSettings::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::awt::XStyleSettings > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_AWT_XSTYLESETTINGS_HPP
