#ifndef INCLUDED_COM_SUN_STAR_AWT_TREE_EXPANDVETOEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_AWT_TREE_EXPANDVETOEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/awt/tree/ExpandVetoException.hdl"

#include "com/sun/star/awt/tree/TreeExpansionEvent.hpp"
#include "com/sun/star/util/VetoException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline ExpandVetoException::ExpandVetoException() SAL_THROW(())
    : ::css::util::VetoException()
    , Event()
{
    ::cppu::UnoType< ::css::awt::tree::ExpandVetoException >::get();
}

inline ExpandVetoException::ExpandVetoException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::awt::tree::TreeExpansionEvent& Event_) SAL_THROW(())
    : ::css::util::VetoException(Message_, Context_)
    , Event(Event_)
{
    ::cppu::UnoType< ::css::awt::tree::ExpandVetoException >::get();
}

ExpandVetoException::ExpandVetoException(ExpandVetoException const & the_other): ::css::util::VetoException(the_other), Event(the_other.Event) {}

ExpandVetoException::~ExpandVetoException() {}

ExpandVetoException & ExpandVetoException::operator =(ExpandVetoException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::util::VetoException::operator =(the_other);
    Event = the_other.Event;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tree { namespace detail {

struct theExpandVetoExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theExpandVetoExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.awt.tree.ExpandVetoException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::util::VetoException >::get();
        ::cppu::UnoType< ::css::awt::tree::TreeExpansionEvent >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "com.sun.star.awt.tree.TreeExpansionEvent" );
        ::rtl::OUString sMemberName0( "Event" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRUCT;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::tree::ExpandVetoException const *) {
    return *detail::theExpandVetoExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::tree::ExpandVetoException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::tree::ExpandVetoException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_TREE_EXPANDVETOEXCEPTION_HPP
