#ifndef INCLUDED_COM_SUN_STAR_AWT_FOCUSEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_FOCUSEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/FocusEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline FocusEvent::FocusEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , FocusFlags(0)
    , NextFocus()
    , Temporary(false)
{
}

inline FocusEvent::FocusEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& FocusFlags_, const ::css::uno::Reference< ::css::uno::XInterface >& NextFocus_, const ::sal_Bool& Temporary_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , FocusFlags(FocusFlags_)
    , NextFocus(NextFocus_)
    , Temporary(Temporary_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theFocusEventType : public rtl::StaticWithInit< ::css::uno::Type *, theFocusEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.FocusEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "FocusFlags" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.uno.XInterface" );
        ::rtl::OUString the_name1( "NextFocus" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name2( "Temporary" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::FocusEvent const *) {
    return *detail::theFocusEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::FocusEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::FocusEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_FOCUSEVENT_HPP
