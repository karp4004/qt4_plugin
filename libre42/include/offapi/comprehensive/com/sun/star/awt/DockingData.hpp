#ifndef INCLUDED_COM_SUN_STAR_AWT_DOCKINGDATA_HPP
#define INCLUDED_COM_SUN_STAR_AWT_DOCKINGDATA_HPP

#include "sal/config.h"

#include "com/sun/star/awt/DockingData.hdl"

#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline DockingData::DockingData() SAL_THROW(())
    : TrackingRectangle()
    , bFloating(false)
{
}

inline DockingData::DockingData(const ::css::awt::Rectangle& TrackingRectangle_, const ::sal_Bool& bFloating_) SAL_THROW(())
    : TrackingRectangle(TrackingRectangle_)
    , bFloating(bFloating_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theDockingDataType : public rtl::StaticWithInit< ::css::uno::Type *, theDockingDataType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.DockingData" );
        ::cppu::UnoType< ::css::awt::Rectangle >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.Rectangle" );
        ::rtl::OUString the_name0( "TrackingRectangle" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "bFloating" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::DockingData const *) {
    return *detail::theDockingDataType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::DockingData const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::DockingData >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_DOCKINGDATA_HPP
