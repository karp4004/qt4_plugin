#ifndef INCLUDED_COM_SUN_STAR_AWT_FONTDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_AWT_FONTDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/awt/FontDescriptor.hdl"

#include "com/sun/star/awt/FontSlant.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline FontDescriptor::FontDescriptor() SAL_THROW(())
    : Name()
    , Height(0)
    , Width(0)
    , StyleName()
    , Family(0)
    , CharSet(0)
    , Pitch(0)
    , CharacterWidth(0)
    , Weight(0)
    , Slant(::css::awt::FontSlant_NONE)
    , Underline(0)
    , Strikeout(0)
    , Orientation(0)
    , Kerning(false)
    , WordLineMode(false)
    , Type(0)
{
}

inline FontDescriptor::FontDescriptor(const ::rtl::OUString& Name_, const ::sal_Int16& Height_, const ::sal_Int16& Width_, const ::rtl::OUString& StyleName_, const ::sal_Int16& Family_, const ::sal_Int16& CharSet_, const ::sal_Int16& Pitch_, const float& CharacterWidth_, const float& Weight_, const ::css::awt::FontSlant& Slant_, const ::sal_Int16& Underline_, const ::sal_Int16& Strikeout_, const float& Orientation_, const ::sal_Bool& Kerning_, const ::sal_Bool& WordLineMode_, const ::sal_Int16& Type_) SAL_THROW(())
    : Name(Name_)
    , Height(Height_)
    , Width(Width_)
    , StyleName(StyleName_)
    , Family(Family_)
    , CharSet(CharSet_)
    , Pitch(Pitch_)
    , CharacterWidth(CharacterWidth_)
    , Weight(Weight_)
    , Slant(Slant_)
    , Underline(Underline_)
    , Strikeout(Strikeout_)
    , Orientation(Orientation_)
    , Kerning(Kerning_)
    , WordLineMode(WordLineMode_)
    , Type(Type_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theFontDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, theFontDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.FontDescriptor" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "Height" );
        ::rtl::OUString the_name2( "Width" );
        ::rtl::OUString the_name3( "StyleName" );
        ::rtl::OUString the_name4( "Family" );
        ::rtl::OUString the_name5( "CharSet" );
        ::rtl::OUString the_name6( "Pitch" );
        ::rtl::OUString the_tname2( "float" );
        ::rtl::OUString the_name7( "CharacterWidth" );
        ::rtl::OUString the_name8( "Weight" );
        ::cppu::UnoType< ::css::awt::FontSlant >::get();
        ::rtl::OUString the_tname3( "com.sun.star.awt.FontSlant" );
        ::rtl::OUString the_name9( "Slant" );
        ::rtl::OUString the_name10( "Underline" );
        ::rtl::OUString the_name11( "Strikeout" );
        ::rtl::OUString the_name12( "Orientation" );
        ::rtl::OUString the_tname4( "boolean" );
        ::rtl::OUString the_name13( "Kerning" );
        ::rtl::OUString the_name14( "WordLineMode" );
        ::rtl::OUString the_name15( "Type" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name6.pData }, false },
            { { typelib_TypeClass_FLOAT, the_tname2.pData, the_name7.pData }, false },
            { { typelib_TypeClass_FLOAT, the_tname2.pData, the_name8.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname3.pData, the_name9.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name10.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name11.pData }, false },
            { { typelib_TypeClass_FLOAT, the_tname2.pData, the_name12.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname4.pData, the_name13.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname4.pData, the_name14.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name15.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 16, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::FontDescriptor const *) {
    return *detail::theFontDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::FontDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::FontDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_FONTDESCRIPTOR_HPP
