#ifndef INCLUDED_COM_SUN_STAR_AWT_XTIMEFIELD_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XTIMEFIELD_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/util/Time.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XTimeField : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL setTime( const ::css::util::Time& Time ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::util::Time SAL_CALL getTime() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMin( const ::css::util::Time& Time ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::util::Time SAL_CALL getMin() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMax( const ::css::util::Time& Time ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::util::Time SAL_CALL getMax() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setFirst( const ::css::util::Time& Time ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::util::Time SAL_CALL getFirst() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setLast( const ::css::util::Time& Time ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::util::Time SAL_CALL getLast() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setEmpty() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isEmpty() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setStrictFormat( ::sal_Bool bStrict ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isStrictFormat() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTimeField() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XTimeField const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XTimeField > *) SAL_THROW(());

#endif
