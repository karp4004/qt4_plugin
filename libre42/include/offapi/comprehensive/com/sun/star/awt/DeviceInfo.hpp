#ifndef INCLUDED_COM_SUN_STAR_AWT_DEVICEINFO_HPP
#define INCLUDED_COM_SUN_STAR_AWT_DEVICEINFO_HPP

#include "sal/config.h"

#include "com/sun/star/awt/DeviceInfo.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline DeviceInfo::DeviceInfo() SAL_THROW(())
    : Width(0)
    , Height(0)
    , LeftInset(0)
    , TopInset(0)
    , RightInset(0)
    , BottomInset(0)
    , PixelPerMeterX(0)
    , PixelPerMeterY(0)
    , BitsPerPixel(0)
    , Capabilities(0)
{
}

inline DeviceInfo::DeviceInfo(const ::sal_Int32& Width_, const ::sal_Int32& Height_, const ::sal_Int32& LeftInset_, const ::sal_Int32& TopInset_, const ::sal_Int32& RightInset_, const ::sal_Int32& BottomInset_, const double& PixelPerMeterX_, const double& PixelPerMeterY_, const ::sal_Int16& BitsPerPixel_, const ::sal_Int32& Capabilities_) SAL_THROW(())
    : Width(Width_)
    , Height(Height_)
    , LeftInset(LeftInset_)
    , TopInset(TopInset_)
    , RightInset(RightInset_)
    , BottomInset(BottomInset_)
    , PixelPerMeterX(PixelPerMeterX_)
    , PixelPerMeterY(PixelPerMeterY_)
    , BitsPerPixel(BitsPerPixel_)
    , Capabilities(Capabilities_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theDeviceInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theDeviceInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.DeviceInfo" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Width" );
        ::rtl::OUString the_name1( "Height" );
        ::rtl::OUString the_name2( "LeftInset" );
        ::rtl::OUString the_name3( "TopInset" );
        ::rtl::OUString the_name4( "RightInset" );
        ::rtl::OUString the_name5( "BottomInset" );
        ::rtl::OUString the_tname1( "double" );
        ::rtl::OUString the_name6( "PixelPerMeterX" );
        ::rtl::OUString the_name7( "PixelPerMeterY" );
        ::rtl::OUString the_tname2( "short" );
        ::rtl::OUString the_name8( "BitsPerPixel" );
        ::rtl::OUString the_name9( "Capabilities" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name6.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname1.pData, the_name7.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name8.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name9.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 10, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::DeviceInfo const *) {
    return *detail::theDeviceInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::DeviceInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::DeviceInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_DEVICEINFO_HPP
