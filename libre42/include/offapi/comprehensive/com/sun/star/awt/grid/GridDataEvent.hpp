#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDDATAEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDDATAEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/grid/GridDataEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline GridDataEvent::GridDataEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , FirstColumn(0)
    , LastColumn(0)
    , FirstRow(0)
    , LastRow(0)
{
}

inline GridDataEvent::GridDataEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& FirstColumn_, const ::sal_Int32& LastColumn_, const ::sal_Int32& FirstRow_, const ::sal_Int32& LastRow_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , FirstColumn(FirstColumn_)
    , LastColumn(LastColumn_)
    , FirstRow(FirstRow_)
    , LastRow(LastRow_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid { namespace detail {

struct theGridDataEventType : public rtl::StaticWithInit< ::css::uno::Type *, theGridDataEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.grid.GridDataEvent" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "FirstColumn" );
        ::rtl::OUString the_name1( "LastColumn" );
        ::rtl::OUString the_name2( "FirstRow" );
        ::rtl::OUString the_name3( "LastRow" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridDataEvent const *) {
    return *detail::theGridDataEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridDataEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::grid::GridDataEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDDATAEVENT_HPP
