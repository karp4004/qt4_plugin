#ifndef INCLUDED_COM_SUN_STAR_AWT_XUSERINPUTINTERCEPTION_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XUSERINPUTINTERCEPTION_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { class XKeyHandler; } } } }
namespace com { namespace sun { namespace star { namespace awt { class XMouseClickHandler; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XUserInputInterception : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addKeyHandler( const ::css::uno::Reference< ::css::awt::XKeyHandler >& xHandler ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeKeyHandler( const ::css::uno::Reference< ::css::awt::XKeyHandler >& xHandler ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addMouseClickHandler( const ::css::uno::Reference< ::css::awt::XMouseClickHandler >& xHandler ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeMouseClickHandler( const ::css::uno::Reference< ::css::awt::XMouseClickHandler >& xHandler ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XUserInputInterception() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XUserInputInterception const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XUserInputInterception > *) SAL_THROW(());

#endif
