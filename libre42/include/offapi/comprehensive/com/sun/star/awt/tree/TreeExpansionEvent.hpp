#ifndef INCLUDED_COM_SUN_STAR_AWT_TREE_TREEEXPANSIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_TREE_TREEEXPANSIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/tree/TreeExpansionEvent.hdl"

#include "com/sun/star/awt/tree/XTreeNode.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline TreeExpansionEvent::TreeExpansionEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Node()
{
}

inline TreeExpansionEvent::TreeExpansionEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::awt::tree::XTreeNode >& Node_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Node(Node_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tree { namespace detail {

struct theTreeExpansionEventType : public rtl::StaticWithInit< ::css::uno::Type *, theTreeExpansionEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.tree.TreeExpansionEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::awt::tree::XTreeNode > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.tree.XTreeNode" );
        ::rtl::OUString the_name0( "Node" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::tree::TreeExpansionEvent const *) {
    return *detail::theTreeExpansionEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::tree::TreeExpansionEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::tree::TreeExpansionEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_TREE_TREEEXPANSIONEVENT_HPP
