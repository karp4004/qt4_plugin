#ifndef INCLUDED_COM_SUN_STAR_AWT_TAB_XTABPAGECONTAINER_HDL
#define INCLUDED_COM_SUN_STAR_AWT_TAB_XTABPAGECONTAINER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { namespace tab { class XTabPage; } } } } }
namespace com { namespace sun { namespace star { namespace awt { namespace tab { class XTabPageContainerListener; } } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tab {

class SAL_NO_VTABLE XTabPageContainer : public ::css::uno::XInterface
{
public:

    // Attributes
    virtual ::sal_Int16 SAL_CALL getActiveTabPageID() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setActiveTabPageID( ::sal_Int16 _activetabpageid ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual ::sal_Int16 SAL_CALL getTabPageCount() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isTabPageActive( ::sal_Int16 tabPageIndex ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::tab::XTabPage > SAL_CALL getTabPage( ::sal_Int16 tabPageIndex ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::awt::tab::XTabPage > SAL_CALL getTabPageByID( ::sal_Int16 tabPageID ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addTabPageContainerListener( const ::css::uno::Reference< ::css::awt::tab::XTabPageContainerListener >& listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeTabPageContainerListener( const ::css::uno::Reference< ::css::awt::tab::XTabPageContainerListener >& listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTabPageContainer() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::tab::XTabPageContainer const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::tab::XTabPageContainer > *) SAL_THROW(());

#endif
