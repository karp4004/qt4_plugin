#ifndef INCLUDED_COM_SUN_STAR_AWT_ADJUSTMENTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_ADJUSTMENTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/AdjustmentEvent.hdl"

#include "com/sun/star/awt/AdjustmentType.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline AdjustmentEvent::AdjustmentEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Value(0)
    , Type(::css::awt::AdjustmentType_ADJUST_LINE)
{
}

inline AdjustmentEvent::AdjustmentEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& Value_, const ::css::awt::AdjustmentType& Type_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Value(Value_)
    , Type(Type_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theAdjustmentEventType : public rtl::StaticWithInit< ::css::uno::Type *, theAdjustmentEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.AdjustmentEvent" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Value" );
        ::cppu::UnoType< ::css::awt::AdjustmentType >::get();
        ::rtl::OUString the_tname1( "com.sun.star.awt.AdjustmentType" );
        ::rtl::OUString the_name1( "Type" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::AdjustmentEvent const *) {
    return *detail::theAdjustmentEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::AdjustmentEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::AdjustmentEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_ADJUSTMENTEVENT_HPP
