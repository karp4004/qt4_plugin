#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDDATAEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDDATAEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/awt/grid/GridInvalidDataException.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline GridInvalidDataException::GridInvalidDataException() SAL_THROW(())
    : ::css::uno::RuntimeException()
{
    ::cppu::UnoType< ::css::awt::grid::GridInvalidDataException >::get();
}

inline GridInvalidDataException::GridInvalidDataException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
{
    ::cppu::UnoType< ::css::awt::grid::GridInvalidDataException >::get();
}

GridInvalidDataException::GridInvalidDataException(GridInvalidDataException const & the_other): ::css::uno::RuntimeException(the_other) {}

GridInvalidDataException::~GridInvalidDataException() {}

GridInvalidDataException & GridInvalidDataException::operator =(GridInvalidDataException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid { namespace detail {

struct theGridInvalidDataExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theGridInvalidDataExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.awt.grid.GridInvalidDataException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::RuntimeException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridInvalidDataException const *) {
    return *detail::theGridInvalidDataExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridInvalidDataException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::grid::GridInvalidDataException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDDATAEXCEPTION_HPP
