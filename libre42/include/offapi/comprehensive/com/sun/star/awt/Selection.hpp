#ifndef INCLUDED_COM_SUN_STAR_AWT_SELECTION_HPP
#define INCLUDED_COM_SUN_STAR_AWT_SELECTION_HPP

#include "sal/config.h"

#include "com/sun/star/awt/Selection.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline Selection::Selection() SAL_THROW(())
    : Min(0)
    , Max(0)
{
}

inline Selection::Selection(const ::sal_Int32& Min_, const ::sal_Int32& Max_) SAL_THROW(())
    : Min(Min_)
    , Max(Max_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theSelectionType : public rtl::StaticWithInit< ::css::uno::Type *, theSelectionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.Selection" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Min" );
        ::rtl::OUString the_name1( "Max" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::Selection const *) {
    return *detail::theSelectionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::Selection const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::Selection >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_SELECTION_HPP
