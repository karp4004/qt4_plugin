#ifndef INCLUDED_COM_SUN_STAR_AWT_TREE_TREEDATAMODELEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_TREE_TREEDATAMODELEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/tree/TreeDataModelEvent.hdl"

#include "com/sun/star/awt/tree/XTreeNode.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline TreeDataModelEvent::TreeDataModelEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Nodes()
    , ParentNode()
{
}

inline TreeDataModelEvent::TreeDataModelEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Sequence< ::css::uno::Reference< ::css::awt::tree::XTreeNode > >& Nodes_, const ::css::uno::Reference< ::css::awt::tree::XTreeNode >& ParentNode_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Nodes(Nodes_)
    , ParentNode(ParentNode_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tree { namespace detail {

struct theTreeDataModelEventType : public rtl::StaticWithInit< ::css::uno::Type *, theTreeDataModelEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.tree.TreeDataModelEvent" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::uno::Reference< ::css::awt::tree::XTreeNode > > >::get();
        ::rtl::OUString the_tname0( "[]com.sun.star.awt.tree.XTreeNode" );
        ::rtl::OUString the_name0( "Nodes" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::awt::tree::XTreeNode > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.awt.tree.XTreeNode" );
        ::rtl::OUString the_name1( "ParentNode" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tree {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::tree::TreeDataModelEvent const *) {
    return *detail::theTreeDataModelEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::tree::TreeDataModelEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::tree::TreeDataModelEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_TREE_TREEDATAMODELEVENT_HPP
