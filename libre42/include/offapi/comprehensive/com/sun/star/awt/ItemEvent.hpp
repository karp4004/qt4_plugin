#ifndef INCLUDED_COM_SUN_STAR_AWT_ITEMEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_ITEMEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/ItemEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline ItemEvent::ItemEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Selected(0)
    , Highlighted(0)
    , ItemId(0)
{
}

inline ItemEvent::ItemEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& Selected_, const ::sal_Int32& Highlighted_, const ::sal_Int32& ItemId_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Selected(Selected_)
    , Highlighted(Highlighted_)
    , ItemId(ItemId_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theItemEventType : public rtl::StaticWithInit< ::css::uno::Type *, theItemEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.ItemEvent" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Selected" );
        ::rtl::OUString the_name1( "Highlighted" );
        ::rtl::OUString the_name2( "ItemId" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::ItemEvent const *) {
    return *detail::theItemEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::ItemEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::ItemEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_ITEMEVENT_HPP
