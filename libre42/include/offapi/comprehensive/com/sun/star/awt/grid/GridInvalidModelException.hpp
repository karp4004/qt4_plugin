#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDMODELEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDMODELEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/awt/grid/GridInvalidModelException.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline GridInvalidModelException::GridInvalidModelException() SAL_THROW(())
    : ::css::uno::RuntimeException()
{
    ::cppu::UnoType< ::css::awt::grid::GridInvalidModelException >::get();
}

inline GridInvalidModelException::GridInvalidModelException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::RuntimeException(Message_, Context_)
{
    ::cppu::UnoType< ::css::awt::grid::GridInvalidModelException >::get();
}

GridInvalidModelException::GridInvalidModelException(GridInvalidModelException const & the_other): ::css::uno::RuntimeException(the_other) {}

GridInvalidModelException::~GridInvalidModelException() {}

GridInvalidModelException & GridInvalidModelException::operator =(GridInvalidModelException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::RuntimeException::operator =(the_other);
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid { namespace detail {

struct theGridInvalidModelExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theGridInvalidModelExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.awt.grid.GridInvalidModelException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::RuntimeException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridInvalidModelException const *) {
    return *detail::theGridInvalidModelExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridInvalidModelException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::grid::GridInvalidModelException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDINVALIDMODELEXCEPTION_HPP
