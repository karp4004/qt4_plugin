#ifndef INCLUDED_COM_SUN_STAR_AWT_PAINTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_PAINTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/PaintEvent.hdl"

#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline PaintEvent::PaintEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , UpdateRect()
    , Count(0)
{
}

inline PaintEvent::PaintEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::awt::Rectangle& UpdateRect_, const ::sal_Int16& Count_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , UpdateRect(UpdateRect_)
    , Count(Count_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct thePaintEventType : public rtl::StaticWithInit< ::css::uno::Type *, thePaintEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.PaintEvent" );
        ::cppu::UnoType< ::css::awt::Rectangle >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.Rectangle" );
        ::rtl::OUString the_name0( "UpdateRect" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "Count" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::PaintEvent const *) {
    return *detail::thePaintEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::PaintEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::PaintEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_PAINTEVENT_HPP
