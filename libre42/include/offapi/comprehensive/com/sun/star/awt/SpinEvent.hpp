#ifndef INCLUDED_COM_SUN_STAR_AWT_SPINEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_SPINEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/SpinEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline SpinEvent::SpinEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , dummy1(0)
{
}

inline SpinEvent::SpinEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& dummy1_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , dummy1(dummy1_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theSpinEventType : public rtl::StaticWithInit< ::css::uno::Type *, theSpinEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.SpinEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "dummy1" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::SpinEvent const *) {
    return *detail::theSpinEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::SpinEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::SpinEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_SPINEVENT_HPP
