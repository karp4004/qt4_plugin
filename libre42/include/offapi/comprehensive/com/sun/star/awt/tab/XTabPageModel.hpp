#ifndef INCLUDED_COM_SUN_STAR_AWT_TAB_XTABPAGEMODEL_HPP
#define INCLUDED_COM_SUN_STAR_AWT_TAB_XTABPAGEMODEL_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/tab/XTabPageModel.hdl"

#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace awt { namespace tab { namespace detail {

struct theXTabPageModelType : public rtl::StaticWithInit< ::css::uno::Type *, theXTabPageModelType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.awt.tab.XTabPageModel" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[5] = { 0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.awt.tab.XTabPageModel::TabPageID" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.awt.tab.XTabPageModel::Enabled" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.awt.tab.XTabPageModel::Title" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.awt.tab.XTabPageModel::ImageURL" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.awt.tab.XTabPageModel::ToolTip" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            5,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace tab {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::tab::XTabPageModel const *) {
    const ::css::uno::Type &rRet = *detail::theXTabPageModelType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "short" );
                ::rtl::OUString sAttributeName0( "com.sun.star.awt.tab.XTabPageModel::TabPageID" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    3, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType0.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "boolean" );
                ::rtl::OUString sAttributeName1( "com.sun.star.awt.tab.XTabPageModel::Enabled" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    4, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "string" );
                ::rtl::OUString sAttributeName2( "com.sun.star.awt.tab.XTabPageModel::Title" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    5, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "string" );
                ::rtl::OUString sAttributeName3( "com.sun.star.awt.tab.XTabPageModel::ImageURL" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    6, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "string" );
                ::rtl::OUString sAttributeName4( "com.sun.star.awt.tab.XTabPageModel::ToolTip" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::awt::tab::XTabPageModel > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::awt::tab::XTabPageModel > >::get();
}

::css::uno::Type const & ::css::awt::tab::XTabPageModel::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::awt::tab::XTabPageModel > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_AWT_TAB_XTABPAGEMODEL_HPP
