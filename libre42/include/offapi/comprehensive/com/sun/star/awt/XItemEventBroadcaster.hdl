#ifndef INCLUDED_COM_SUN_STAR_AWT_XITEMEVENTBROADCASTER_HDL
#define INCLUDED_COM_SUN_STAR_AWT_XITEMEVENTBROADCASTER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace awt { class XItemListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace awt {

class SAL_NO_VTABLE XItemEventBroadcaster : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL addItemListener( const ::css::uno::Reference< ::css::awt::XItemListener >& l ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeItemListener( const ::css::uno::Reference< ::css::awt::XItemListener >& l ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XItemEventBroadcaster() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::XItemEventBroadcaster const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::awt::XItemEventBroadcaster > *) SAL_THROW(());

#endif
