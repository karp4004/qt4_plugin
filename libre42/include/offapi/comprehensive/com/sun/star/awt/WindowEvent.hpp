#ifndef INCLUDED_COM_SUN_STAR_AWT_WINDOWEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_WINDOWEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/WindowEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline WindowEvent::WindowEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , X(0)
    , Y(0)
    , Width(0)
    , Height(0)
    , LeftInset(0)
    , TopInset(0)
    , RightInset(0)
    , BottomInset(0)
{
}

inline WindowEvent::WindowEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& X_, const ::sal_Int32& Y_, const ::sal_Int32& Width_, const ::sal_Int32& Height_, const ::sal_Int32& LeftInset_, const ::sal_Int32& TopInset_, const ::sal_Int32& RightInset_, const ::sal_Int32& BottomInset_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , X(X_)
    , Y(Y_)
    , Width(Width_)
    , Height(Height_)
    , LeftInset(LeftInset_)
    , TopInset(TopInset_)
    , RightInset(RightInset_)
    , BottomInset(BottomInset_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theWindowEventType : public rtl::StaticWithInit< ::css::uno::Type *, theWindowEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.WindowEvent" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "X" );
        ::rtl::OUString the_name1( "Y" );
        ::rtl::OUString the_name2( "Width" );
        ::rtl::OUString the_name3( "Height" );
        ::rtl::OUString the_name4( "LeftInset" );
        ::rtl::OUString the_name5( "TopInset" );
        ::rtl::OUString the_name6( "RightInset" );
        ::rtl::OUString the_name7( "BottomInset" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name5.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name6.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name7.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 8, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::WindowEvent const *) {
    return *detail::theWindowEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::WindowEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::WindowEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_WINDOWEVENT_HPP
