#ifndef INCLUDED_COM_SUN_STAR_AWT_KEYEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_KEYEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/KeyEvent.hdl"

#include "com/sun/star/awt/InputEvent.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline KeyEvent::KeyEvent() SAL_THROW(())
    : ::css::awt::InputEvent()
    , KeyCode(0)
    , KeyChar(0)
    , KeyFunc(0)
{
}

inline KeyEvent::KeyEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& Modifiers_, const ::sal_Int16& KeyCode_, const ::sal_Unicode& KeyChar_, const ::sal_Int16& KeyFunc_) SAL_THROW(())
    : ::css::awt::InputEvent(Source_, Modifiers_)
    , KeyCode(KeyCode_)
    , KeyChar(KeyChar_)
    , KeyFunc(KeyFunc_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theKeyEventType : public rtl::StaticWithInit< ::css::uno::Type *, theKeyEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.KeyEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "KeyCode" );
        ::rtl::OUString the_tname1( "char" );
        ::rtl::OUString the_name1( "KeyChar" );
        ::rtl::OUString the_name2( "KeyFunc" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_CHAR, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::awt::InputEvent >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::KeyEvent const *) {
    return *detail::theKeyEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::KeyEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::KeyEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_KEYEVENT_HPP
