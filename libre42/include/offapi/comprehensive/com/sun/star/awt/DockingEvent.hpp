#ifndef INCLUDED_COM_SUN_STAR_AWT_DOCKINGEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_DOCKINGEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/DockingEvent.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline DockingEvent::DockingEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , TrackingRectangle()
    , MousePos()
    , bLiveMode(false)
    , bInteractive(false)
{
}

inline DockingEvent::DockingEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::awt::Rectangle& TrackingRectangle_, const ::css::awt::Point& MousePos_, const ::sal_Bool& bLiveMode_, const ::sal_Bool& bInteractive_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , TrackingRectangle(TrackingRectangle_)
    , MousePos(MousePos_)
    , bLiveMode(bLiveMode_)
    , bInteractive(bInteractive_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theDockingEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDockingEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.DockingEvent" );
        ::cppu::UnoType< ::css::awt::Rectangle >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.Rectangle" );
        ::rtl::OUString the_name0( "TrackingRectangle" );
        ::cppu::UnoType< ::css::awt::Point >::get();
        ::rtl::OUString the_tname1( "com.sun.star.awt.Point" );
        ::rtl::OUString the_name1( "MousePos" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name2( "bLiveMode" );
        ::rtl::OUString the_name3( "bInteractive" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::DockingEvent const *) {
    return *detail::theDockingEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::DockingEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::DockingEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_DOCKINGEVENT_HPP
