#ifndef INCLUDED_COM_SUN_STAR_AWT_ENDPOPUPMODEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_ENDPOPUPMODEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/EndPopupModeEvent.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline EndPopupModeEvent::EndPopupModeEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , bTearoff(false)
    , FloatingPosition()
{
}

inline EndPopupModeEvent::EndPopupModeEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Bool& bTearoff_, const ::css::awt::Point& FloatingPosition_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , bTearoff(bTearoff_)
    , FloatingPosition(FloatingPosition_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theEndPopupModeEventType : public rtl::StaticWithInit< ::css::uno::Type *, theEndPopupModeEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.EndPopupModeEvent" );
        ::rtl::OUString the_tname0( "boolean" );
        ::rtl::OUString the_name0( "bTearoff" );
        ::cppu::UnoType< ::css::awt::Point >::get();
        ::rtl::OUString the_tname1( "com.sun.star.awt.Point" );
        ::rtl::OUString the_name1( "FloatingPosition" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BOOLEAN, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::EndPopupModeEvent const *) {
    return *detail::theEndPopupModeEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::EndPopupModeEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::EndPopupModeEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_ENDPOPUPMODEEVENT_HPP
