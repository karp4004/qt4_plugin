#ifndef INCLUDED_COM_SUN_STAR_AWT_ITEMLISTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_ITEMLISTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/ItemListEvent.hdl"

#include "com/sun/star/beans/Optional.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline ItemListEvent::ItemListEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , ItemPosition(0)
    , ItemText()
    , ItemImageURL()
{
}

inline ItemListEvent::ItemListEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& ItemPosition_, const ::css::beans::Optional< ::rtl::OUString >& ItemText_, const ::css::beans::Optional< ::rtl::OUString >& ItemImageURL_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , ItemPosition(ItemPosition_)
    , ItemText(ItemText_)
    , ItemImageURL(ItemImageURL_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theItemListEventType : public rtl::StaticWithInit< ::css::uno::Type *, theItemListEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.ItemListEvent" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "ItemPosition" );
        ::cppu::UnoType< ::css::beans::Optional< ::rtl::OUString > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.beans.Optional<string>" );
        ::rtl::OUString the_name1( "ItemText" );
        ::rtl::OUString the_name2( "ItemImageURL" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::ItemListEvent const *) {
    return *detail::theItemListEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::ItemListEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::ItemListEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_ITEMLISTEVENT_HPP
