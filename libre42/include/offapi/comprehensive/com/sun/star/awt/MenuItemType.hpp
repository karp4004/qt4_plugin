#ifndef INCLUDED_COM_SUN_STAR_AWT_MENUITEMTYPE_HPP
#define INCLUDED_COM_SUN_STAR_AWT_MENUITEMTYPE_HPP

#include "sal/config.h"

#include "com/sun/star/awt/MenuItemType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theMenuItemTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theMenuItemTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.awt.MenuItemType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[5];
        ::rtl::OUString sEnumValue0( "DONTKNOW" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "STRING" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "IMAGE" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "STRINGIMAGE" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "SEPARATOR" );
        enumValueNames[4] = sEnumValue4.pData;

        sal_Int32 enumValues[5];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::awt::MenuItemType_DONTKNOW,
            5, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::MenuItemType const *) {
    return *detail::theMenuItemTypeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::MenuItemType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::MenuItemType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_MENUITEMTYPE_HPP
