#ifndef INCLUDED_COM_SUN_STAR_AWT_KEYSTROKE_HPP
#define INCLUDED_COM_SUN_STAR_AWT_KEYSTROKE_HPP

#include "sal/config.h"

#include "com/sun/star/awt/KeyStroke.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline KeyStroke::KeyStroke() SAL_THROW(())
    : Modifiers(0)
    , KeyCode(0)
    , KeyChar(0)
    , KeyFunc(0)
{
}

inline KeyStroke::KeyStroke(const ::sal_Int16& Modifiers_, const ::sal_Int16& KeyCode_, const ::sal_Unicode& KeyChar_, const ::sal_Int16& KeyFunc_) SAL_THROW(())
    : Modifiers(Modifiers_)
    , KeyCode(KeyCode_)
    , KeyChar(KeyChar_)
    , KeyFunc(KeyFunc_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theKeyStrokeType : public rtl::StaticWithInit< ::css::uno::Type *, theKeyStrokeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.KeyStroke" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "Modifiers" );
        ::rtl::OUString the_name1( "KeyCode" );
        ::rtl::OUString the_tname1( "char" );
        ::rtl::OUString the_name2( "KeyChar" );
        ::rtl::OUString the_name3( "KeyFunc" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_CHAR, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::KeyStroke const *) {
    return *detail::theKeyStrokeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::KeyStroke const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::KeyStroke >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_KEYSTROKE_HPP
