#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDCOLUMNEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDCOLUMNEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/grid/GridColumnEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline GridColumnEvent::GridColumnEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , AttributeName()
    , OldValue()
    , NewValue()
    , ColumnIndex(0)
{
}

inline GridColumnEvent::GridColumnEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& AttributeName_, const ::css::uno::Any& OldValue_, const ::css::uno::Any& NewValue_, const ::sal_Int32& ColumnIndex_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , AttributeName(AttributeName_)
    , OldValue(OldValue_)
    , NewValue(NewValue_)
    , ColumnIndex(ColumnIndex_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid { namespace detail {

struct theGridColumnEventType : public rtl::StaticWithInit< ::css::uno::Type *, theGridColumnEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.grid.GridColumnEvent" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "AttributeName" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "OldValue" );
        ::rtl::OUString the_name2( "NewValue" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name3( "ColumnIndex" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridColumnEvent const *) {
    return *detail::theGridColumnEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridColumnEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::grid::GridColumnEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDCOLUMNEVENT_HPP
