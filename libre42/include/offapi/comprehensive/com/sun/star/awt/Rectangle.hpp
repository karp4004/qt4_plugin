#ifndef INCLUDED_COM_SUN_STAR_AWT_RECTANGLE_HPP
#define INCLUDED_COM_SUN_STAR_AWT_RECTANGLE_HPP

#include "sal/config.h"

#include "com/sun/star/awt/Rectangle.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline Rectangle::Rectangle() SAL_THROW(())
    : X(0)
    , Y(0)
    , Width(0)
    , Height(0)
{
}

inline Rectangle::Rectangle(const ::sal_Int32& X_, const ::sal_Int32& Y_, const ::sal_Int32& Width_, const ::sal_Int32& Height_) SAL_THROW(())
    : X(X_)
    , Y(Y_)
    , Width(Width_)
    , Height(Height_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theRectangleType : public rtl::StaticWithInit< ::css::uno::Type *, theRectangleType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.Rectangle" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "X" );
        ::rtl::OUString the_name1( "Y" );
        ::rtl::OUString the_name2( "Width" );
        ::rtl::OUString the_name3( "Height" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::Rectangle const *) {
    return *detail::theRectangleType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::Rectangle const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::Rectangle >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_RECTANGLE_HPP
