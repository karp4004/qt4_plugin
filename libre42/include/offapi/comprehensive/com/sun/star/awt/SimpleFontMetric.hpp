#ifndef INCLUDED_COM_SUN_STAR_AWT_SIMPLEFONTMETRIC_HPP
#define INCLUDED_COM_SUN_STAR_AWT_SIMPLEFONTMETRIC_HPP

#include "sal/config.h"

#include "com/sun/star/awt/SimpleFontMetric.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline SimpleFontMetric::SimpleFontMetric() SAL_THROW(())
    : Ascent(0)
    , Descent(0)
    , Leading(0)
    , Slant(0)
    , FirstChar(0)
    , LastChar(0)
{
}

inline SimpleFontMetric::SimpleFontMetric(const ::sal_Int16& Ascent_, const ::sal_Int16& Descent_, const ::sal_Int16& Leading_, const ::sal_Int16& Slant_, const ::sal_Unicode& FirstChar_, const ::sal_Unicode& LastChar_) SAL_THROW(())
    : Ascent(Ascent_)
    , Descent(Descent_)
    , Leading(Leading_)
    , Slant(Slant_)
    , FirstChar(FirstChar_)
    , LastChar(LastChar_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theSimpleFontMetricType : public rtl::StaticWithInit< ::css::uno::Type *, theSimpleFontMetricType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.SimpleFontMetric" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "Ascent" );
        ::rtl::OUString the_name1( "Descent" );
        ::rtl::OUString the_name2( "Leading" );
        ::rtl::OUString the_name3( "Slant" );
        ::rtl::OUString the_tname1( "char" );
        ::rtl::OUString the_name4( "FirstChar" );
        ::rtl::OUString the_name5( "LastChar" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_CHAR, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_CHAR, the_tname1.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::SimpleFontMetric const *) {
    return *detail::theSimpleFontMetricType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::SimpleFontMetric const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::SimpleFontMetric >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_SIMPLEFONTMETRIC_HPP
