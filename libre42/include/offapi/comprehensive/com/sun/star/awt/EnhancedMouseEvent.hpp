#ifndef INCLUDED_COM_SUN_STAR_AWT_ENHANCEDMOUSEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_ENHANCEDMOUSEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/EnhancedMouseEvent.hdl"

#include "com/sun/star/awt/MouseEvent.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline EnhancedMouseEvent::EnhancedMouseEvent() SAL_THROW(())
    : ::css::awt::MouseEvent()
    , Target()
{
}

inline EnhancedMouseEvent::EnhancedMouseEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& Modifiers_, const ::sal_Int16& Buttons_, const ::sal_Int32& X_, const ::sal_Int32& Y_, const ::sal_Int32& ClickCount_, const ::sal_Bool& PopupTrigger_, const ::css::uno::Reference< ::css::uno::XInterface >& Target_) SAL_THROW(())
    : ::css::awt::MouseEvent(Source_, Modifiers_, Buttons_, X_, Y_, ClickCount_, PopupTrigger_)
    , Target(Target_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theEnhancedMouseEventType : public rtl::StaticWithInit< ::css::uno::Type *, theEnhancedMouseEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.EnhancedMouseEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.uno.XInterface" );
        ::rtl::OUString the_name0( "Target" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::awt::MouseEvent >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::EnhancedMouseEvent const *) {
    return *detail::theEnhancedMouseEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::EnhancedMouseEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::EnhancedMouseEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_ENHANCEDMOUSEEVENT_HPP
