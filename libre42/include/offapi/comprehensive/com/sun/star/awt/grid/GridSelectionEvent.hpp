#ifndef INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDSELECTIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDSELECTIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/grid/GridSelectionEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline GridSelectionEvent::GridSelectionEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , SelectedRowIndexes()
    , SelectedColumnIndexes()
{
}

inline GridSelectionEvent::GridSelectionEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Sequence< ::sal_Int32 >& SelectedRowIndexes_, const ::css::uno::Sequence< ::sal_Int32 >& SelectedColumnIndexes_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , SelectedRowIndexes(SelectedRowIndexes_)
    , SelectedColumnIndexes(SelectedColumnIndexes_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid { namespace detail {

struct theGridSelectionEventType : public rtl::StaticWithInit< ::css::uno::Type *, theGridSelectionEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.grid.GridSelectionEvent" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::sal_Int32 > >::get();
        ::rtl::OUString the_tname0( "[]long" );
        ::rtl::OUString the_name0( "SelectedRowIndexes" );
        ::rtl::OUString the_name1( "SelectedColumnIndexes" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace awt { namespace grid {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridSelectionEvent const *) {
    return *detail::theGridSelectionEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::grid::GridSelectionEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::grid::GridSelectionEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_GRID_GRIDSELECTIONEVENT_HPP
