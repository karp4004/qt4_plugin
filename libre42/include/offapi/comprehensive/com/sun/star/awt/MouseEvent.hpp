#ifndef INCLUDED_COM_SUN_STAR_AWT_MOUSEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_AWT_MOUSEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/awt/MouseEvent.hdl"

#include "com/sun/star/awt/InputEvent.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline MouseEvent::MouseEvent() SAL_THROW(())
    : ::css::awt::InputEvent()
    , Buttons(0)
    , X(0)
    , Y(0)
    , ClickCount(0)
    , PopupTrigger(false)
{
}

inline MouseEvent::MouseEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& Modifiers_, const ::sal_Int16& Buttons_, const ::sal_Int32& X_, const ::sal_Int32& Y_, const ::sal_Int32& ClickCount_, const ::sal_Bool& PopupTrigger_) SAL_THROW(())
    : ::css::awt::InputEvent(Source_, Modifiers_)
    , Buttons(Buttons_)
    , X(X_)
    , Y(Y_)
    , ClickCount(ClickCount_)
    , PopupTrigger(PopupTrigger_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theMouseEventType : public rtl::StaticWithInit< ::css::uno::Type *, theMouseEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.MouseEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "Buttons" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "X" );
        ::rtl::OUString the_name2( "Y" );
        ::rtl::OUString the_name3( "ClickCount" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name4( "PopupTrigger" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::awt::InputEvent >::get().getTypeLibType(), 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::MouseEvent const *) {
    return *detail::theMouseEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::MouseEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::MouseEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_MOUSEEVENT_HPP
