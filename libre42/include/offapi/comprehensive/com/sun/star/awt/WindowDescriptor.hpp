#ifndef INCLUDED_COM_SUN_STAR_AWT_WINDOWDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_AWT_WINDOWDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/awt/WindowDescriptor.hdl"

#include "com/sun/star/awt/Rectangle.hpp"
#include "com/sun/star/awt/WindowClass.hpp"
#include "com/sun/star/awt/XWindowPeer.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace awt {

inline WindowDescriptor::WindowDescriptor() SAL_THROW(())
    : Type(::css::awt::WindowClass_TOP)
    , WindowServiceName()
    , Parent()
    , ParentIndex(0)
    , Bounds()
    , WindowAttributes(0)
{
}

inline WindowDescriptor::WindowDescriptor(const ::css::awt::WindowClass& Type_, const ::rtl::OUString& WindowServiceName_, const ::css::uno::Reference< ::css::awt::XWindowPeer >& Parent_, const ::sal_Int16& ParentIndex_, const ::css::awt::Rectangle& Bounds_, const ::sal_Int32& WindowAttributes_) SAL_THROW(())
    : Type(Type_)
    , WindowServiceName(WindowServiceName_)
    , Parent(Parent_)
    , ParentIndex(ParentIndex_)
    , Bounds(Bounds_)
    , WindowAttributes(WindowAttributes_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace awt { namespace detail {

struct theWindowDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, theWindowDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.awt.WindowDescriptor" );
        ::cppu::UnoType< ::css::awt::WindowClass >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.WindowClass" );
        ::rtl::OUString the_name0( "Type" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "WindowServiceName" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::awt::XWindowPeer > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.awt.XWindowPeer" );
        ::rtl::OUString the_name2( "Parent" );
        ::rtl::OUString the_tname3( "short" );
        ::rtl::OUString the_name3( "ParentIndex" );
        ::cppu::UnoType< ::css::awt::Rectangle >::get();
        ::rtl::OUString the_tname4( "com.sun.star.awt.Rectangle" );
        ::rtl::OUString the_name4( "Bounds" );
        ::rtl::OUString the_tname5( "long" );
        ::rtl::OUString the_name5( "WindowAttributes" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname3.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname4.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname5.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace awt {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::awt::WindowDescriptor const *) {
    return *detail::theWindowDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::awt::WindowDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::awt::WindowDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_AWT_WINDOWDESCRIPTOR_HPP
