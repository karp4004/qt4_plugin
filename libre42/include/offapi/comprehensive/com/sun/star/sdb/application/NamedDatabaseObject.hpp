#ifndef INCLUDED_COM_SUN_STAR_SDB_APPLICATION_NAMEDDATABASEOBJECT_HPP
#define INCLUDED_COM_SUN_STAR_SDB_APPLICATION_NAMEDDATABASEOBJECT_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/application/NamedDatabaseObject.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sdb { namespace application {

inline NamedDatabaseObject::NamedDatabaseObject() SAL_THROW(())
    : Type(0)
    , Name()
{
}

inline NamedDatabaseObject::NamedDatabaseObject(const ::sal_Int32& Type_, const ::rtl::OUString& Name_) SAL_THROW(())
    : Type(Type_)
    , Name(Name_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace application { namespace detail {

struct theNamedDatabaseObjectType : public rtl::StaticWithInit< ::css::uno::Type *, theNamedDatabaseObjectType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sdb.application.NamedDatabaseObject" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Type" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "Name" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace application {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::application::NamedDatabaseObject const *) {
    return *detail::theNamedDatabaseObjectType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::application::NamedDatabaseObject const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::application::NamedDatabaseObject >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_APPLICATION_NAMEDDATABASEOBJECT_HPP
