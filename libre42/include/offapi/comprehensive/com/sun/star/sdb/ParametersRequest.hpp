#ifndef INCLUDED_COM_SUN_STAR_SDB_PARAMETERSREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_SDB_PARAMETERSREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/ParametersRequest.hdl"

#include "com/sun/star/container/XIndexAccess.hpp"
#include "com/sun/star/sdbc/XConnection.hpp"
#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdb {

inline ParametersRequest::ParametersRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , Parameters()
    , Connection()
{
    ::cppu::UnoType< ::css::sdb::ParametersRequest >::get();
}

inline ParametersRequest::ParametersRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::uno::Reference< ::css::container::XIndexAccess >& Parameters_, const ::css::uno::Reference< ::css::sdbc::XConnection >& Connection_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , Parameters(Parameters_)
    , Connection(Connection_)
{
    ::cppu::UnoType< ::css::sdb::ParametersRequest >::get();
}

ParametersRequest::ParametersRequest(ParametersRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), Parameters(the_other.Parameters), Connection(the_other.Connection) {}

ParametersRequest::~ParametersRequest() {}

ParametersRequest & ParametersRequest::operator =(ParametersRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    Parameters = the_other.Parameters;
    Connection = the_other.Connection;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace detail {

struct theParametersRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theParametersRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.sdb.ParametersRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::container::XIndexAccess > >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::sdbc::XConnection > >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "com.sun.star.container.XIndexAccess" );
        ::rtl::OUString sMemberName0( "Parameters" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "com.sun.star.sdbc.XConnection" );
        ::rtl::OUString sMemberName1( "Connection" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::ParametersRequest const *) {
    return *detail::theParametersRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::ParametersRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::ParametersRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_PARAMETERSREQUEST_HPP
