#ifndef INCLUDED_COM_SUN_STAR_SDB_DATABASEREGISTRATIONEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SDB_DATABASEREGISTRATIONEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/DatabaseRegistrationEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sdb {

inline DatabaseRegistrationEvent::DatabaseRegistrationEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Name()
    , OldLocation()
    , NewLocation()
{
}

inline DatabaseRegistrationEvent::DatabaseRegistrationEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& Name_, const ::rtl::OUString& OldLocation_, const ::rtl::OUString& NewLocation_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Name(Name_)
    , OldLocation(OldLocation_)
    , NewLocation(NewLocation_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace detail {

struct theDatabaseRegistrationEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDatabaseRegistrationEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sdb.DatabaseRegistrationEvent" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Name" );
        ::rtl::OUString the_name1( "OldLocation" );
        ::rtl::OUString the_name2( "NewLocation" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::DatabaseRegistrationEvent const *) {
    return *detail::theDatabaseRegistrationEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::DatabaseRegistrationEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::DatabaseRegistrationEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_DATABASEREGISTRATIONEVENT_HPP
