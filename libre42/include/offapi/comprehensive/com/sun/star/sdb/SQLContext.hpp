#ifndef INCLUDED_COM_SUN_STAR_SDB_SQLCONTEXT_HPP
#define INCLUDED_COM_SUN_STAR_SDB_SQLCONTEXT_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/SQLContext.hdl"

#include "com/sun/star/sdbc/SQLWarning.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdb {

inline SQLContext::SQLContext() SAL_THROW(())
    : ::css::sdbc::SQLWarning()
    , Details()
{
    ::cppu::UnoType< ::css::sdb::SQLContext >::get();
}

inline SQLContext::SQLContext(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& SQLState_, const ::sal_Int32& ErrorCode_, const ::css::uno::Any& NextException_, const ::rtl::OUString& Details_) SAL_THROW(())
    : ::css::sdbc::SQLWarning(Message_, Context_, SQLState_, ErrorCode_, NextException_)
    , Details(Details_)
{
    ::cppu::UnoType< ::css::sdb::SQLContext >::get();
}

SQLContext::SQLContext(SQLContext const & the_other): ::css::sdbc::SQLWarning(the_other), Details(the_other.Details) {}

SQLContext::~SQLContext() {}

SQLContext & SQLContext::operator =(SQLContext const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::sdbc::SQLWarning::operator =(the_other);
    Details = the_other.Details;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace detail {

struct theSQLContextType : public rtl::StaticWithInit< ::css::uno::Type *, theSQLContextType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.sdb.SQLContext" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::sdbc::SQLWarning >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "Details" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::SQLContext const *) {
    return *detail::theSQLContextType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::SQLContext const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::SQLContext >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_SQLCONTEXT_HPP
