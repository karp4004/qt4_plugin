#ifndef INCLUDED_COM_SUN_STAR_SDB_TABLEDEFINITION_HPP
#define INCLUDED_COM_SUN_STAR_SDB_TABLEDEFINITION_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace sdb {

class TableDefinition {
public:
    static ::css::uno::Reference< ::css::beans::XPropertySet > createDefault(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::beans::XPropertySet > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::beans::XPropertySet >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.sdb.TableDefinition" ), ::css::uno::Sequence< ::css::uno::Any >(), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.sdb.TableDefinition of type com.sun.star.beans.XPropertySet: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.sdb.TableDefinition of type com.sun.star.beans.XPropertySet" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::beans::XPropertySet > createWithName(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& Name) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= Name;
        ::css::uno::Reference< ::css::beans::XPropertySet > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::beans::XPropertySet >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.sdb.TableDefinition" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.sdb.TableDefinition of type com.sun.star.beans.XPropertySet: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.sdb.TableDefinition of type com.sun.star.beans.XPropertySet" ), the_context);
        }
        return the_instance;
    }

private:
    TableDefinition(); // not implemented
    TableDefinition(TableDefinition &); // not implemented
    ~TableDefinition(); // not implemented
    void operator =(TableDefinition); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_SDB_TABLEDEFINITION_HPP
