#ifndef INCLUDED_COM_SUN_STAR_SDB_ROWSETVETOEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SDB_ROWSETVETOEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/RowSetVetoException.hdl"

#include "com/sun/star/sdbc/SQLException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdb {

inline RowSetVetoException::RowSetVetoException() SAL_THROW(())
    : ::css::sdbc::SQLException()
{
    ::cppu::UnoType< ::css::sdb::RowSetVetoException >::get();
}

inline RowSetVetoException::RowSetVetoException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& SQLState_, const ::sal_Int32& ErrorCode_, const ::css::uno::Any& NextException_) SAL_THROW(())
    : ::css::sdbc::SQLException(Message_, Context_, SQLState_, ErrorCode_, NextException_)
{
    ::cppu::UnoType< ::css::sdb::RowSetVetoException >::get();
}

RowSetVetoException::RowSetVetoException(RowSetVetoException const & the_other): ::css::sdbc::SQLException(the_other) {}

RowSetVetoException::~RowSetVetoException() {}

RowSetVetoException & RowSetVetoException::operator =(RowSetVetoException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::sdbc::SQLException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace detail {

struct theRowSetVetoExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theRowSetVetoExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.sdb.RowSetVetoException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::sdbc::SQLException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::RowSetVetoException const *) {
    return *detail::theRowSetVetoExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::RowSetVetoException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::RowSetVetoException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_ROWSETVETOEXCEPTION_HPP
