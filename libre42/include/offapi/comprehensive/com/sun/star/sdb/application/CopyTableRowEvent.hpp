#ifndef INCLUDED_COM_SUN_STAR_SDB_APPLICATION_COPYTABLEROWEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SDB_APPLICATION_COPYTABLEROWEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/application/CopyTableRowEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/sdbc/XResultSet.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sdb { namespace application {

inline CopyTableRowEvent::CopyTableRowEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , SourceData()
    , Error()
{
}

inline CopyTableRowEvent::CopyTableRowEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::sdbc::XResultSet >& SourceData_, const ::css::uno::Any& Error_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , SourceData(SourceData_)
    , Error(Error_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace application { namespace detail {

struct theCopyTableRowEventType : public rtl::StaticWithInit< ::css::uno::Type *, theCopyTableRowEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sdb.application.CopyTableRowEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::sdbc::XResultSet > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.sdbc.XResultSet" );
        ::rtl::OUString the_name0( "SourceData" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "Error" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace application {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::application::CopyTableRowEvent const *) {
    return *detail::theCopyTableRowEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::application::CopyTableRowEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::application::CopyTableRowEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_APPLICATION_COPYTABLEROWEVENT_HPP
