#ifndef INCLUDED_COM_SUN_STAR_SDB_XOFFICEDATABASEDOCUMENT_HPP
#define INCLUDED_COM_SUN_STAR_SDB_XOFFICEDATABASEDOCUMENT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/sdb/XOfficeDatabaseDocument.hdl"

#include "com/sun/star/document/XDocumentSubStorageSupplier.hpp"
#include "com/sun/star/sdb/XFormDocumentsSupplier.hpp"
#include "com/sun/star/sdb/XReportDocumentsSupplier.hpp"
#include "com/sun/star/sdbc/XDataSource.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdb { namespace detail {

struct theXOfficeDatabaseDocumentType : public rtl::StaticWithInit< ::css::uno::Type *, theXOfficeDatabaseDocumentType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.sdb.XOfficeDatabaseDocument" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[3];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::sdb::XFormDocumentsSupplier > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::sdb::XReportDocumentsSupplier > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::document::XDocumentSubStorageSupplier > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[1] = { 0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.sdb.XOfficeDatabaseDocument::DataSource" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            3, aSuperTypes,
            1,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::XOfficeDatabaseDocument const *) {
    const ::css::uno::Type &rRet = *detail::theXOfficeDatabaseDocumentType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::sdbc::XDataSource > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "com.sun.star.sdbc.XDataSource" );
                ::rtl::OUString sAttributeName0( "com.sun.star.sdb.XOfficeDatabaseDocument::DataSource" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType0.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::sdb::XOfficeDatabaseDocument > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::sdb::XOfficeDatabaseDocument > >::get();
}

::css::uno::Type const & ::css::sdb::XOfficeDatabaseDocument::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::sdb::XOfficeDatabaseDocument > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SDB_XOFFICEDATABASEDOCUMENT_HPP
