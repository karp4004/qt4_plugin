#ifndef INCLUDED_COM_SUN_STAR_SDB_DOCUMENTSAVEREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_SDB_DOCUMENTSAVEREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/DocumentSaveRequest.hdl"

#include "com/sun/star/task/ClassifiedInteractionRequest.hpp"
#include "com/sun/star/ucb/XContent.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace sdb {

inline DocumentSaveRequest::DocumentSaveRequest() SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest()
    , Content()
    , Name()
{
    ::cppu::UnoType< ::css::sdb::DocumentSaveRequest >::get();
}

inline DocumentSaveRequest::DocumentSaveRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::css::uno::Reference< ::css::ucb::XContent >& Content_, const ::rtl::OUString& Name_) SAL_THROW(())
    : ::css::task::ClassifiedInteractionRequest(Message_, Context_, Classification_)
    , Content(Content_)
    , Name(Name_)
{
    ::cppu::UnoType< ::css::sdb::DocumentSaveRequest >::get();
}

DocumentSaveRequest::DocumentSaveRequest(DocumentSaveRequest const & the_other): ::css::task::ClassifiedInteractionRequest(the_other), Content(the_other.Content), Name(the_other.Name) {}

DocumentSaveRequest::~DocumentSaveRequest() {}

DocumentSaveRequest & DocumentSaveRequest::operator =(DocumentSaveRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::task::ClassifiedInteractionRequest::operator =(the_other);
    Content = the_other.Content;
    Name = the_other.Name;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace detail {

struct theDocumentSaveRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theDocumentSaveRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.sdb.DocumentSaveRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::task::ClassifiedInteractionRequest >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::ucb::XContent > >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "com.sun.star.ucb.XContent" );
        ::rtl::OUString sMemberName0( "Content" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "Name" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::DocumentSaveRequest const *) {
    return *detail::theDocumentSaveRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::DocumentSaveRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::DocumentSaveRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_DOCUMENTSAVEREQUEST_HPP
