#ifndef INCLUDED_COM_SUN_STAR_SDB_SQLERROREVENT_HPP
#define INCLUDED_COM_SUN_STAR_SDB_SQLERROREVENT_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/SQLErrorEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sdb {

inline SQLErrorEvent::SQLErrorEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Reason()
{
}

inline SQLErrorEvent::SQLErrorEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Any& Reason_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Reason(Reason_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace detail {

struct theSQLErrorEventType : public rtl::StaticWithInit< ::css::uno::Type *, theSQLErrorEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sdb.SQLErrorEvent" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "Reason" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::SQLErrorEvent const *) {
    return *detail::theSQLErrorEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::SQLErrorEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::SQLErrorEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_SQLERROREVENT_HPP
