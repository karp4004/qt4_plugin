#ifndef INCLUDED_COM_SUN_STAR_SDB_ROWSCHANGEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SDB_ROWSCHANGEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/sdb/RowsChangeEvent.hdl"

#include "com/sun/star/sdb/RowChangeEvent.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace sdb {

inline RowsChangeEvent::RowsChangeEvent() SAL_THROW(())
    : ::css::sdb::RowChangeEvent()
    , Bookmarks()
{
}

inline RowsChangeEvent::RowsChangeEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& Action_, const ::sal_Int32& Rows_, const ::css::uno::Sequence< ::css::uno::Any >& Bookmarks_) SAL_THROW(())
    : ::css::sdb::RowChangeEvent(Source_, Action_, Rows_)
    , Bookmarks(Bookmarks_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace sdb { namespace detail {

struct theRowsChangeEventType : public rtl::StaticWithInit< ::css::uno::Type *, theRowsChangeEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.sdb.RowsChangeEvent" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::uno::Any > >::get();
        ::rtl::OUString the_tname0( "[]any" );
        ::rtl::OUString the_name0( "Bookmarks" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::sdb::RowChangeEvent >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace sdb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::sdb::RowsChangeEvent const *) {
    return *detail::theRowsChangeEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::sdb::RowsChangeEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::sdb::RowsChangeEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SDB_ROWSCHANGEEVENT_HPP
