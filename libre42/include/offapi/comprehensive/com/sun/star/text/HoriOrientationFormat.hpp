#ifndef INCLUDED_COM_SUN_STAR_TEXT_HORIORIENTATIONFORMAT_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_HORIORIENTATIONFORMAT_HPP

#include "sal/config.h"

#include "com/sun/star/text/HoriOrientationFormat.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline HoriOrientationFormat::HoriOrientationFormat() SAL_THROW(())
    : XPos(0)
    , HorizontalOrientation(0)
    , HorizontalRelation(0)
    , PositionToggle(false)
{
}

inline HoriOrientationFormat::HoriOrientationFormat(const ::sal_Int32& XPos_, const ::sal_Int16& HorizontalOrientation_, const ::sal_Int16& HorizontalRelation_, const ::sal_Bool& PositionToggle_) SAL_THROW(())
    : XPos(XPos_)
    , HorizontalOrientation(HorizontalOrientation_)
    , HorizontalRelation(HorizontalRelation_)
    , PositionToggle(PositionToggle_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theHoriOrientationFormatType : public rtl::StaticWithInit< ::css::uno::Type *, theHoriOrientationFormatType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.text.HoriOrientationFormat" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "XPos" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "HorizontalOrientation" );
        ::rtl::OUString the_name2( "HorizontalRelation" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name3( "PositionToggle" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::HoriOrientationFormat const *) {
    return *detail::theHoriOrientationFormatType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::HoriOrientationFormat const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::HoriOrientationFormat >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_HORIORIENTATIONFORMAT_HPP
