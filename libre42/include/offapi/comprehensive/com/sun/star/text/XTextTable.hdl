#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTTABLE_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTTABLE_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace table { class XCell; } } } }
namespace com { namespace sun { namespace star { namespace table { class XTableColumns; } } } }
namespace com { namespace sun { namespace star { namespace table { class XTableRows; } } } }
#include "com/sun/star/text/XTextContent.hdl"
namespace com { namespace sun { namespace star { namespace text { class XTextTableCursor; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XTextTable : public ::css::text::XTextContent
{
public:

    // Methods
    virtual void SAL_CALL initialize( ::sal_Int32 nRows, ::sal_Int32 nColumns ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::table::XTableRows > SAL_CALL getRows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::table::XTableColumns > SAL_CALL getColumns() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::table::XCell > SAL_CALL getCellByName( const ::rtl::OUString& aCellName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getCellNames() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::text::XTextTableCursor > SAL_CALL createCursorByCellName( const ::rtl::OUString& aCellName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTextTable() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextTable const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XTextTable > *) SAL_THROW(());

#endif
