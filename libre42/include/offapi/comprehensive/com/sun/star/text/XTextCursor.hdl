#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTCURSOR_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTCURSOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/text/XTextRange.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XTextCursor : public ::css::text::XTextRange
{
public:

    // Methods
    virtual void SAL_CALL collapseToStart() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL collapseToEnd() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isCollapsed() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL goLeft( ::sal_Int16 nCount, ::sal_Bool bExpand ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL goRight( ::sal_Int16 nCount, ::sal_Bool bExpand ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL gotoStart( ::sal_Bool bExpand ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL gotoEnd( ::sal_Bool bExpand ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL gotoRange( const ::css::uno::Reference< ::css::text::XTextRange >& xRange, ::sal_Bool bExpand ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTextCursor() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextCursor const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XTextCursor > *) SAL_THROW(());

#endif
