#ifndef INCLUDED_COM_SUN_STAR_TEXT_INVALIDTEXTCONTENTEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_INVALIDTEXTCONTENTEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/text/InvalidTextContentException.hdl"

#include "com/sun/star/text/XTextContent.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace text {

inline InvalidTextContentException::InvalidTextContentException() SAL_THROW(())
    : ::css::uno::Exception()
    , TextContent()
{
    ::cppu::UnoType< ::css::text::InvalidTextContentException >::get();
}

inline InvalidTextContentException::InvalidTextContentException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Reference< ::css::text::XTextContent >& TextContent_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , TextContent(TextContent_)
{
    ::cppu::UnoType< ::css::text::InvalidTextContentException >::get();
}

InvalidTextContentException::InvalidTextContentException(InvalidTextContentException const & the_other): ::css::uno::Exception(the_other), TextContent(the_other.TextContent) {}

InvalidTextContentException::~InvalidTextContentException() {}

InvalidTextContentException & InvalidTextContentException::operator =(InvalidTextContentException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    TextContent = the_other.TextContent;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theInvalidTextContentExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInvalidTextContentExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.text.InvalidTextContentException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::css::uno::Reference< ::css::text::XTextContent > >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "com.sun.star.text.XTextContent" );
        ::rtl::OUString sMemberName0( "TextContent" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_INTERFACE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::InvalidTextContentException const *) {
    return *detail::theInvalidTextContentExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::InvalidTextContentException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::InvalidTextContentException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_INVALIDTEXTCONTENTEXCEPTION_HPP
