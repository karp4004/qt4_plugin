#ifndef INCLUDED_COM_SUN_STAR_TEXT_TEXTPOSITION_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_TEXTPOSITION_HPP

#include "sal/config.h"

#include "com/sun/star/text/TextPosition.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline TextPosition::TextPosition() SAL_THROW(())
    : Paragraph(0)
    , PositionInParagraph(0)
{
}

inline TextPosition::TextPosition(const ::sal_Int32& Paragraph_, const ::sal_Int32& PositionInParagraph_) SAL_THROW(())
    : Paragraph(Paragraph_)
    , PositionInParagraph(PositionInParagraph_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theTextPositionType : public rtl::StaticWithInit< ::css::uno::Type *, theTextPositionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.text.TextPosition" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Paragraph" );
        ::rtl::OUString the_name1( "PositionInParagraph" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::TextPosition const *) {
    return *detail::theTextPositionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::TextPosition const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::TextPosition >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_TEXTPOSITION_HPP
