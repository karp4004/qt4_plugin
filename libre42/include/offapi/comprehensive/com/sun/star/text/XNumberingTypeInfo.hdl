#ifndef INCLUDED_COM_SUN_STAR_TEXT_XNUMBERINGTYPEINFO_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XNUMBERINGTYPEINFO_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XNumberingTypeInfo : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::sal_Int16 > SAL_CALL getSupportedNumberingTypes() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getNumberingType( const ::rtl::OUString& NumberingIdentifier ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL hasNumberingType( const ::rtl::OUString& NumberingIdentifier ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getNumberingIdentifier( ::sal_Int16 NumberingType ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XNumberingTypeInfo() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XNumberingTypeInfo const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XNumberingTypeInfo > *) SAL_THROW(());

#endif
