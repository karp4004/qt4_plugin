#ifndef INCLUDED_COM_SUN_STAR_TEXT_XDOCUMENTINDEXMARK_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XDOCUMENTINDEXMARK_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/text/XTextContent.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XDocumentIndexMark : public ::css::text::XTextContent
{
public:

    // Methods
    virtual ::rtl::OUString SAL_CALL getMarkEntry() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMarkEntry( const ::rtl::OUString& aIndexEntry ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDocumentIndexMark() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XDocumentIndexMark const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XDocumentIndexMark > *) SAL_THROW(());

#endif
