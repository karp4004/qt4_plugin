#ifndef INCLUDED_COM_SUN_STAR_TEXT_XFORMFIELD_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XFORMFIELD_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace container { class XNameContainer; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XFormField : public ::css::uno::XInterface
{
public:

    // Methods
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::rtl::OUString SAL_CALL getFieldType() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual void SAL_CALL setFieldType( const ::rtl::OUString& fieldType ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::css::uno::Reference< ::css::container::XNameContainer > SAL_CALL getParameters() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFormField() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XFormField const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XFormField > *) SAL_THROW(());

#endif
