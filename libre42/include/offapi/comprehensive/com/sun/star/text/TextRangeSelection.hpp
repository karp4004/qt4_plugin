#ifndef INCLUDED_COM_SUN_STAR_TEXT_TEXTRANGESELECTION_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_TEXTRANGESELECTION_HPP

#include "sal/config.h"

#include "com/sun/star/text/TextRangeSelection.hdl"

#include "com/sun/star/text/TextPosition.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline TextRangeSelection::TextRangeSelection() SAL_THROW(())
    : Start()
    , End()
{
}

inline TextRangeSelection::TextRangeSelection(const ::css::text::TextPosition& Start_, const ::css::text::TextPosition& End_) SAL_THROW(())
    : Start(Start_)
    , End(End_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theTextRangeSelectionType : public rtl::StaticWithInit< ::css::uno::Type *, theTextRangeSelectionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.text.TextRangeSelection" );
        ::cppu::UnoType< ::css::text::TextPosition >::get();
        ::rtl::OUString the_tname0( "com.sun.star.text.TextPosition" );
        ::rtl::OUString the_name0( "Start" );
        ::rtl::OUString the_name1( "End" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::TextRangeSelection const *) {
    return *detail::theTextRangeSelectionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::TextRangeSelection const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::TextRangeSelection >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_TEXTRANGESELECTION_HPP
