#ifndef INCLUDED_COM_SUN_STAR_TEXT_TEXTMARKUPDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_TEXTMARKUPDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/text/TextMarkupDescriptor.hdl"

#include "com/sun/star/container/XStringKeyMap.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline TextMarkupDescriptor::TextMarkupDescriptor() SAL_THROW(())
    : nType(0)
    , aIdentifier()
    , nOffset(0)
    , nLength(0)
    , xMarkupInfoContainer()
{
}

inline TextMarkupDescriptor::TextMarkupDescriptor(const ::sal_Int32& nType_, const ::rtl::OUString& aIdentifier_, const ::sal_Int32& nOffset_, const ::sal_Int32& nLength_, const ::css::uno::Reference< ::css::container::XStringKeyMap >& xMarkupInfoContainer_) SAL_THROW(())
    : nType(nType_)
    , aIdentifier(aIdentifier_)
    , nOffset(nOffset_)
    , nLength(nLength_)
    , xMarkupInfoContainer(xMarkupInfoContainer_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theTextMarkupDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, theTextMarkupDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.text.TextMarkupDescriptor" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "nType" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "aIdentifier" );
        ::rtl::OUString the_name2( "nOffset" );
        ::rtl::OUString the_name3( "nLength" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::container::XStringKeyMap > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.container.XStringKeyMap" );
        ::rtl::OUString the_name4( "xMarkupInfoContainer" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::TextMarkupDescriptor const *) {
    return *detail::theTextMarkupDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::TextMarkupDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::TextMarkupDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_TEXTMARKUPDESCRIPTOR_HPP
