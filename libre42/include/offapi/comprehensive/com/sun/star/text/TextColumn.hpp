#ifndef INCLUDED_COM_SUN_STAR_TEXT_TEXTCOLUMN_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_TEXTCOLUMN_HPP

#include "sal/config.h"

#include "com/sun/star/text/TextColumn.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline TextColumn::TextColumn() SAL_THROW(())
    : Width(0)
    , LeftMargin(0)
    , RightMargin(0)
{
}

inline TextColumn::TextColumn(const ::sal_Int32& Width_, const ::sal_Int32& LeftMargin_, const ::sal_Int32& RightMargin_) SAL_THROW(())
    : Width(Width_)
    , LeftMargin(LeftMargin_)
    , RightMargin(RightMargin_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theTextColumnType : public rtl::StaticWithInit< ::css::uno::Type *, theTextColumnType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.text.TextColumn" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Width" );
        ::rtl::OUString the_name1( "LeftMargin" );
        ::rtl::OUString the_name2( "RightMargin" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::TextColumn const *) {
    return *detail::theTextColumnType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::TextColumn const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::TextColumn >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_TEXTCOLUMN_HPP
