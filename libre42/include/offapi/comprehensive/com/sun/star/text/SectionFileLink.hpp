#ifndef INCLUDED_COM_SUN_STAR_TEXT_SECTIONFILELINK_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_SECTIONFILELINK_HPP

#include "sal/config.h"

#include "com/sun/star/text/SectionFileLink.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline SectionFileLink::SectionFileLink() SAL_THROW(())
    : FileURL()
    , FilterName()
{
}

inline SectionFileLink::SectionFileLink(const ::rtl::OUString& FileURL_, const ::rtl::OUString& FilterName_) SAL_THROW(())
    : FileURL(FileURL_)
    , FilterName(FilterName_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theSectionFileLinkType : public rtl::StaticWithInit< ::css::uno::Type *, theSectionFileLinkType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.text.SectionFileLink" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "FileURL" );
        ::rtl::OUString the_name1( "FilterName" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::SectionFileLink const *) {
    return *detail::theSectionFileLinkType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::SectionFileLink const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::SectionFileLink >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_SECTIONFILELINK_HPP
