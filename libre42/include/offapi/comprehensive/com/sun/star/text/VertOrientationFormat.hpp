#ifndef INCLUDED_COM_SUN_STAR_TEXT_VERTORIENTATIONFORMAT_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_VERTORIENTATIONFORMAT_HPP

#include "sal/config.h"

#include "com/sun/star/text/VertOrientationFormat.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline VertOrientationFormat::VertOrientationFormat() SAL_THROW(())
    : YPos(0)
    , VerticalOrientation(0)
    , VerticalRelation(0)
{
}

inline VertOrientationFormat::VertOrientationFormat(const ::sal_Int32& YPos_, const ::sal_Int16& VerticalOrientation_, const ::sal_Int16& VerticalRelation_) SAL_THROW(())
    : YPos(YPos_)
    , VerticalOrientation(VerticalOrientation_)
    , VerticalRelation(VerticalRelation_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theVertOrientationFormatType : public rtl::StaticWithInit< ::css::uno::Type *, theVertOrientationFormatType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.text.VertOrientationFormat" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "YPos" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "VerticalOrientation" );
        ::rtl::OUString the_name2( "VerticalRelation" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::VertOrientationFormat const *) {
    return *detail::theVertOrientationFormatType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::VertOrientationFormat const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::VertOrientationFormat >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_VERTORIENTATIONFORMAT_HPP
