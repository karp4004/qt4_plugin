#ifndef INCLUDED_COM_SUN_STAR_TEXT_GRAPHICCROP_HPP
#define INCLUDED_COM_SUN_STAR_TEXT_GRAPHICCROP_HPP

#include "sal/config.h"

#include "com/sun/star/text/GraphicCrop.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace text {

inline GraphicCrop::GraphicCrop() SAL_THROW(())
    : Top(0)
    , Bottom(0)
    , Left(0)
    , Right(0)
{
}

inline GraphicCrop::GraphicCrop(const ::sal_Int32& Top_, const ::sal_Int32& Bottom_, const ::sal_Int32& Left_, const ::sal_Int32& Right_) SAL_THROW(())
    : Top(Top_)
    , Bottom(Bottom_)
    , Left(Left_)
    , Right(Right_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace text { namespace detail {

struct theGraphicCropType : public rtl::StaticWithInit< ::css::uno::Type *, theGraphicCropType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.text.GraphicCrop" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Top" );
        ::rtl::OUString the_name1( "Bottom" );
        ::rtl::OUString the_name2( "Left" );
        ::rtl::OUString the_name3( "Right" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace text {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::GraphicCrop const *) {
    return *detail::theGraphicCropType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::text::GraphicCrop const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::text::GraphicCrop >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TEXT_GRAPHICCROP_HPP
