#ifndef INCLUDED_COM_SUN_STAR_TEXT_XTEXTDOCUMENT_HDL
#define INCLUDED_COM_SUN_STAR_TEXT_XTEXTDOCUMENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/frame/XModel.hdl"
namespace com { namespace sun { namespace star { namespace text { class XText; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace text {

class SAL_NO_VTABLE XTextDocument : public ::css::frame::XModel
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::text::XText > SAL_CALL getText() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL reformat() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTextDocument() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::text::XTextDocument const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::text::XTextDocument > *) SAL_THROW(());

#endif
