#ifndef INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINDESCRIPTION_HPP
#define INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINDESCRIPTION_HPP

#include "sal/config.h"

#include "com/sun/star/plugin/PluginDescription.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace plugin {

inline PluginDescription::PluginDescription() SAL_THROW(())
    : PluginName()
    , Mimetype()
    , Extension()
    , Description()
{
}

inline PluginDescription::PluginDescription(const ::rtl::OUString& PluginName_, const ::rtl::OUString& Mimetype_, const ::rtl::OUString& Extension_, const ::rtl::OUString& Description_) SAL_THROW(())
    : PluginName(PluginName_)
    , Mimetype(Mimetype_)
    , Extension(Extension_)
    , Description(Description_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace plugin { namespace detail {

struct thePluginDescriptionType : public rtl::StaticWithInit< ::css::uno::Type *, thePluginDescriptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.plugin.PluginDescription" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "PluginName" );
        ::rtl::OUString the_name1( "Mimetype" );
        ::rtl::OUString the_name2( "Extension" );
        ::rtl::OUString the_name3( "Description" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace plugin {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::plugin::PluginDescription const *) {
    return *detail::thePluginDescriptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::plugin::PluginDescription const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::plugin::PluginDescription >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINDESCRIPTION_HPP
