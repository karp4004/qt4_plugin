#ifndef INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINVARIABLE_HPP
#define INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINVARIABLE_HPP

#include "sal/config.h"

#include "com/sun/star/plugin/PluginVariable.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace plugin { namespace detail {

struct thePluginVariableType : public rtl::StaticWithInit< ::css::uno::Type *, thePluginVariableType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.plugin.PluginVariable" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[2];
        ::rtl::OUString sEnumValue0( "VxDisplay" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "VxtAppContext" );
        enumValueNames[1] = sEnumValue1.pData;

        sal_Int32 enumValues[2];
        enumValues[0] = 0;
        enumValues[1] = 1;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::plugin::PluginVariable_VxDisplay,
            2, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace plugin {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::plugin::PluginVariable const *) {
    return *detail::thePluginVariableType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::plugin::PluginVariable const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::plugin::PluginVariable >::get();
}

#endif // INCLUDED_COM_SUN_STAR_PLUGIN_PLUGINVARIABLE_HPP
