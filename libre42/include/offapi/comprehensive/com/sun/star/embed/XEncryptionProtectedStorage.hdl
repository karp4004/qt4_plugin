#ifndef INCLUDED_COM_SUN_STAR_EMBED_XENCRYPTIONPROTECTEDSTORAGE_HDL
#define INCLUDED_COM_SUN_STAR_EMBED_XENCRYPTIONPROTECTEDSTORAGE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/NamedValue.hdl"
#include "com/sun/star/embed/XEncryptionProtectedSource2.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace embed {

class SAL_NO_VTABLE XEncryptionProtectedStorage : public ::css::embed::XEncryptionProtectedSource2
{
public:

    // Methods
    virtual void SAL_CALL setEncryptionAlgorithms( const ::css::uno::Sequence< ::css::beans::NamedValue >& aAlgorithms ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::beans::NamedValue > SAL_CALL getEncryptionAlgorithms() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XEncryptionProtectedStorage() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XEncryptionProtectedStorage const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::embed::XEncryptionProtectedStorage > *) SAL_THROW(());

#endif
