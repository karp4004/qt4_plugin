#ifndef INCLUDED_COM_SUN_STAR_EMBED_STATECHANGEINPROGRESSEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_STATECHANGEINPROGRESSEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/embed/StateChangeInProgressException.hdl"

#include "com/sun/star/embed/WrongStateException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace embed {

inline StateChangeInProgressException::StateChangeInProgressException() SAL_THROW(())
    : ::css::embed::WrongStateException()
    , TargetState(0)
{
    ::cppu::UnoType< ::css::embed::StateChangeInProgressException >::get();
}

inline StateChangeInProgressException::StateChangeInProgressException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& TargetState_) SAL_THROW(())
    : ::css::embed::WrongStateException(Message_, Context_)
    , TargetState(TargetState_)
{
    ::cppu::UnoType< ::css::embed::StateChangeInProgressException >::get();
}

StateChangeInProgressException::StateChangeInProgressException(StateChangeInProgressException const & the_other): ::css::embed::WrongStateException(the_other), TargetState(the_other.TargetState) {}

StateChangeInProgressException::~StateChangeInProgressException() {}

StateChangeInProgressException & StateChangeInProgressException::operator =(StateChangeInProgressException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::embed::WrongStateException::operator =(the_other);
    TargetState = the_other.TargetState;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace embed { namespace detail {

struct theStateChangeInProgressExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theStateChangeInProgressExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.embed.StateChangeInProgressException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::embed::WrongStateException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "long" );
        ::rtl::OUString sMemberName0( "TargetState" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::StateChangeInProgressException const *) {
    return *detail::theStateChangeInProgressExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::StateChangeInProgressException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::StateChangeInProgressException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_STATECHANGEINPROGRESSEXCEPTION_HPP
