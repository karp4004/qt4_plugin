#ifndef INCLUDED_COM_SUN_STAR_EMBED_USEBACKUPEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_USEBACKUPEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/embed/UseBackupException.hdl"

#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace embed {

inline UseBackupException::UseBackupException() SAL_THROW(())
    : ::css::io::IOException()
    , TemporaryFileURL()
{
    ::cppu::UnoType< ::css::embed::UseBackupException >::get();
}

inline UseBackupException::UseBackupException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& TemporaryFileURL_) SAL_THROW(())
    : ::css::io::IOException(Message_, Context_)
    , TemporaryFileURL(TemporaryFileURL_)
{
    ::cppu::UnoType< ::css::embed::UseBackupException >::get();
}

UseBackupException::UseBackupException(UseBackupException const & the_other): ::css::io::IOException(the_other), TemporaryFileURL(the_other.TemporaryFileURL) {}

UseBackupException::~UseBackupException() {}

UseBackupException & UseBackupException::operator =(UseBackupException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::io::IOException::operator =(the_other);
    TemporaryFileURL = the_other.TemporaryFileURL;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace embed { namespace detail {

struct theUseBackupExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theUseBackupExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.embed.UseBackupException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::io::IOException >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "TemporaryFileURL" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::UseBackupException const *) {
    return *detail::theUseBackupExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::UseBackupException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::UseBackupException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_USEBACKUPEXCEPTION_HPP
