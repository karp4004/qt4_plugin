#ifndef INCLUDED_COM_SUN_STAR_EMBED_NEEDSRUNNINGSTATEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_NEEDSRUNNINGSTATEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/embed/NeedsRunningStateException.hdl"

#include "com/sun/star/embed/WrongStateException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace embed {

inline NeedsRunningStateException::NeedsRunningStateException() SAL_THROW(())
    : ::css::embed::WrongStateException()
{
    ::cppu::UnoType< ::css::embed::NeedsRunningStateException >::get();
}

inline NeedsRunningStateException::NeedsRunningStateException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::embed::WrongStateException(Message_, Context_)
{
    ::cppu::UnoType< ::css::embed::NeedsRunningStateException >::get();
}

NeedsRunningStateException::NeedsRunningStateException(NeedsRunningStateException const & the_other): ::css::embed::WrongStateException(the_other) {}

NeedsRunningStateException::~NeedsRunningStateException() {}

NeedsRunningStateException & NeedsRunningStateException::operator =(NeedsRunningStateException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::embed::WrongStateException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace embed { namespace detail {

struct theNeedsRunningStateExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theNeedsRunningStateExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.embed.NeedsRunningStateException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::embed::WrongStateException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::NeedsRunningStateException const *) {
    return *detail::theNeedsRunningStateExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::NeedsRunningStateException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::NeedsRunningStateException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_NEEDSRUNNINGSTATEEXCEPTION_HPP
