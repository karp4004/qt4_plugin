#ifndef INCLUDED_COM_SUN_STAR_EMBED_XEMBEDOBJECTFACTORY_HDL
#define INCLUDED_COM_SUN_STAR_EMBED_XEMBEDOBJECTFACTORY_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
namespace com { namespace sun { namespace star { namespace embed { class XStorage; } } } }
#include "com/sun/star/io/IOException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace embed {

class SAL_NO_VTABLE XEmbedObjectFactory : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::uno::XInterface > SAL_CALL createInstanceUserInit( const ::css::uno::Sequence< ::sal_Int8 >& aClassID, const ::rtl::OUString& sClassName, const ::css::uno::Reference< ::css::embed::XStorage >& xStorage, const ::rtl::OUString& sEntName, ::sal_Int32 nEntryConnectionMode, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aArgs, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aObjectArgs ) /* throw (::css::lang::IllegalArgumentException, ::css::io::IOException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XEmbedObjectFactory() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XEmbedObjectFactory const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::embed::XEmbedObjectFactory > *) SAL_THROW(());

#endif
