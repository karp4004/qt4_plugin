#ifndef INCLUDED_COM_SUN_STAR_EMBED_XTRANSACTIONLISTENER_HDL
#define INCLUDED_COM_SUN_STAR_EMBED_XTRANSACTIONLISTENER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/EventObject.hdl"
#include "com/sun/star/lang/XEventListener.hdl"
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace embed {

class SAL_NO_VTABLE XTransactionListener : public ::css::lang::XEventListener
{
public:

    // Methods
    virtual void SAL_CALL preCommit( const ::css::lang::EventObject& aEvent ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL commited( const ::css::lang::EventObject& aEvent ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL preRevert( const ::css::lang::EventObject& aEvent ) /* throw (::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL reverted( const ::css::lang::EventObject& aEvent ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XTransactionListener() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XTransactionListener const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::embed::XTransactionListener > *) SAL_THROW(());

#endif
