#ifndef INCLUDED_COM_SUN_STAR_EMBED_UNREACHABLESTATEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_UNREACHABLESTATEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/embed/UnreachableStateException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace embed {

inline UnreachableStateException::UnreachableStateException() SAL_THROW(())
    : ::css::uno::Exception()
    , CurrentState(0)
    , NextState(0)
{
    ::cppu::UnoType< ::css::embed::UnreachableStateException >::get();
}

inline UnreachableStateException::UnreachableStateException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& CurrentState_, const ::sal_Int32& NextState_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , CurrentState(CurrentState_)
    , NextState(NextState_)
{
    ::cppu::UnoType< ::css::embed::UnreachableStateException >::get();
}

UnreachableStateException::UnreachableStateException(UnreachableStateException const & the_other): ::css::uno::Exception(the_other), CurrentState(the_other.CurrentState), NextState(the_other.NextState) {}

UnreachableStateException::~UnreachableStateException() {}

UnreachableStateException & UnreachableStateException::operator =(UnreachableStateException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    CurrentState = the_other.CurrentState;
    NextState = the_other.NextState;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace embed { namespace detail {

struct theUnreachableStateExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theUnreachableStateExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.embed.UnreachableStateException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[2];
        ::rtl::OUString sMemberType0( "long" );
        ::rtl::OUString sMemberName0( "CurrentState" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "long" );
        ::rtl::OUString sMemberName1( "NextState" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            2,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::UnreachableStateException const *) {
    return *detail::theUnreachableStateExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::UnreachableStateException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::UnreachableStateException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_UNREACHABLESTATEEXCEPTION_HPP
