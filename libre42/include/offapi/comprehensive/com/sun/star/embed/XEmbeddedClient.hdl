#ifndef INCLUDED_COM_SUN_STAR_EMBED_XEMBEDDEDCLIENT_HDL
#define INCLUDED_COM_SUN_STAR_EMBED_XEMBEDDEDCLIENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/embed/ObjectSaveVetoException.hdl"
#include "com/sun/star/embed/WrongStateException.hdl"
#include "com/sun/star/embed/XComponentSupplier.hdl"
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace embed {

class SAL_NO_VTABLE XEmbeddedClient : public ::css::embed::XComponentSupplier
{
public:

    // Methods
    virtual void SAL_CALL saveObject() /* throw (::css::embed::ObjectSaveVetoException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL visibilityChanged( ::sal_Bool bVisible ) /* throw (::css::embed::WrongStateException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XEmbeddedClient() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::XEmbeddedClient const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::embed::XEmbeddedClient > *) SAL_THROW(());

#endif
