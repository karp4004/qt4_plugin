#ifndef INCLUDED_COM_SUN_STAR_EMBED_INSERTEDOBJECTINFO_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_INSERTEDOBJECTINFO_HPP

#include "sal/config.h"

#include "com/sun/star/embed/InsertedObjectInfo.hdl"

#include "com/sun/star/beans/NamedValue.hpp"
#include "com/sun/star/embed/XEmbeddedObject.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace embed {

inline InsertedObjectInfo::InsertedObjectInfo() SAL_THROW(())
    : Object()
    , Options()
{
}

inline InsertedObjectInfo::InsertedObjectInfo(const ::css::uno::Reference< ::css::embed::XEmbeddedObject >& Object_, const ::css::uno::Sequence< ::css::beans::NamedValue >& Options_) SAL_THROW(())
    : Object(Object_)
    , Options(Options_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace embed { namespace detail {

struct theInsertedObjectInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theInsertedObjectInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.embed.InsertedObjectInfo" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::embed::XEmbeddedObject > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.embed.XEmbeddedObject" );
        ::rtl::OUString the_name0( "Object" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::NamedValue > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.beans.NamedValue" );
        ::rtl::OUString the_name1( "Options" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::InsertedObjectInfo const *) {
    return *detail::theInsertedObjectInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::InsertedObjectInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::InsertedObjectInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_INSERTEDOBJECTINFO_HPP
