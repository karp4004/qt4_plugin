#ifndef INCLUDED_COM_SUN_STAR_EMBED_INVALIDSTORAGEEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_INVALIDSTORAGEEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/embed/InvalidStorageException.hdl"

#include "com/sun/star/io/IOException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace embed {

inline InvalidStorageException::InvalidStorageException() SAL_THROW(())
    : ::css::io::IOException()
{
    ::cppu::UnoType< ::css::embed::InvalidStorageException >::get();
}

inline InvalidStorageException::InvalidStorageException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::io::IOException(Message_, Context_)
{
    ::cppu::UnoType< ::css::embed::InvalidStorageException >::get();
}

InvalidStorageException::InvalidStorageException(InvalidStorageException const & the_other): ::css::io::IOException(the_other) {}

InvalidStorageException::~InvalidStorageException() {}

InvalidStorageException & InvalidStorageException::operator =(InvalidStorageException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::io::IOException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace embed { namespace detail {

struct theInvalidStorageExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInvalidStorageExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.embed.InvalidStorageException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::io::IOException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::InvalidStorageException const *) {
    return *detail::theInvalidStorageExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::InvalidStorageException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::InvalidStorageException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_INVALIDSTORAGEEXCEPTION_HPP
