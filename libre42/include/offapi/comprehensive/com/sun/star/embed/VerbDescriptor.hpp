#ifndef INCLUDED_COM_SUN_STAR_EMBED_VERBDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_EMBED_VERBDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/embed/VerbDescriptor.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace embed {

inline VerbDescriptor::VerbDescriptor() SAL_THROW(())
    : VerbID(0)
    , VerbName()
    , VerbFlags(0)
    , VerbAttributes(0)
{
}

inline VerbDescriptor::VerbDescriptor(const ::sal_Int32& VerbID_, const ::rtl::OUString& VerbName_, const ::sal_Int32& VerbFlags_, const ::sal_Int32& VerbAttributes_) SAL_THROW(())
    : VerbID(VerbID_)
    , VerbName(VerbName_)
    , VerbFlags(VerbFlags_)
    , VerbAttributes(VerbAttributes_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace embed { namespace detail {

struct theVerbDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, theVerbDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.embed.VerbDescriptor" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "VerbID" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "VerbName" );
        ::rtl::OUString the_name2( "VerbFlags" );
        ::rtl::OUString the_name3( "VerbAttributes" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace embed {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::embed::VerbDescriptor const *) {
    return *detail::theVerbDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::embed::VerbDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::embed::VerbDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_EMBED_VERBDESCRIPTOR_HPP
