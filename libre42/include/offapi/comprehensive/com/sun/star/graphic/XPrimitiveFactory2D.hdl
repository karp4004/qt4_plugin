#ifndef INCLUDED_COM_SUN_STAR_GRAPHIC_XPRIMITIVEFACTORY2D_HDL
#define INCLUDED_COM_SUN_STAR_GRAPHIC_XPRIMITIVEFACTORY2D_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyValue.hdl"
namespace com { namespace sun { namespace star { namespace drawing { class XDrawPage; } } } }
namespace com { namespace sun { namespace star { namespace drawing { class XShape; } } } }
namespace com { namespace sun { namespace star { namespace graphic { class XPrimitive2D; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace graphic {

class SAL_NO_VTABLE XPrimitiveFactory2D : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::graphic::XPrimitive2D > > SAL_CALL createPrimitivesFromXShape( const ::css::uno::Reference< ::css::drawing::XShape >& xShape, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aParms ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::graphic::XPrimitive2D > > SAL_CALL createPrimitivesFromXDrawPage( const ::css::uno::Reference< ::css::drawing::XDrawPage >& xDrawPage, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aParms ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPrimitiveFactory2D() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::graphic::XPrimitiveFactory2D const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::graphic::XPrimitiveFactory2D > *) SAL_THROW(());

#endif
