#ifndef INCLUDED_COM_SUN_STAR_DRAWING_XSHAPE_HDL
#define INCLUDED_COM_SUN_STAR_DRAWING_XSHAPE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/awt/Point.hdl"
#include "com/sun/star/awt/Size.hdl"
#include "com/sun/star/beans/PropertyVetoException.hdl"
#include "com/sun/star/drawing/XShapeDescriptor.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace drawing {

class SAL_NO_VTABLE XShape : public ::css::drawing::XShapeDescriptor
{
public:

    // Methods
    virtual ::css::awt::Point SAL_CALL getPosition() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPosition( const ::css::awt::Point& aPosition ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::awt::Size SAL_CALL getSize() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setSize( const ::css::awt::Size& aSize ) /* throw (::css::beans::PropertyVetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XShape() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::XShape const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::drawing::XShape > *) SAL_THROW(());

#endif
