#ifndef INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPETEXTFRAME_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPETEXTFRAME_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/EnhancedCustomShapeTextFrame.hdl"

#include "com/sun/star/drawing/EnhancedCustomShapeParameterPair.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline EnhancedCustomShapeTextFrame::EnhancedCustomShapeTextFrame() SAL_THROW(())
    : TopLeft()
    , BottomRight()
{
}

inline EnhancedCustomShapeTextFrame::EnhancedCustomShapeTextFrame(const ::css::drawing::EnhancedCustomShapeParameterPair& TopLeft_, const ::css::drawing::EnhancedCustomShapeParameterPair& BottomRight_) SAL_THROW(())
    : TopLeft(TopLeft_)
    , BottomRight(BottomRight_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theEnhancedCustomShapeTextFrameType : public rtl::StaticWithInit< ::css::uno::Type *, theEnhancedCustomShapeTextFrameType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.EnhancedCustomShapeTextFrame" );
        ::cppu::UnoType< ::css::drawing::EnhancedCustomShapeParameterPair >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.EnhancedCustomShapeParameterPair" );
        ::rtl::OUString the_name0( "TopLeft" );
        ::rtl::OUString the_name1( "BottomRight" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::EnhancedCustomShapeTextFrame const *) {
    return *detail::theEnhancedCustomShapeTextFrameType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::EnhancedCustomShapeTextFrame const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::EnhancedCustomShapeTextFrame >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPETEXTFRAME_HPP
