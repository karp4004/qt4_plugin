#ifndef INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_TABBARBUTTON_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_TABBARBUTTON_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/framework/TabBarButton.hdl"

#include "com/sun/star/drawing/framework/XResourceId.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing { namespace framework {

inline TabBarButton::TabBarButton() SAL_THROW(())
    : ButtonLabel()
    , HelpText()
    , ResourceId()
{
}

inline TabBarButton::TabBarButton(const ::rtl::OUString& ButtonLabel_, const ::rtl::OUString& HelpText_, const ::css::uno::Reference< ::css::drawing::framework::XResourceId >& ResourceId_) SAL_THROW(())
    : ButtonLabel(ButtonLabel_)
    , HelpText(HelpText_)
    , ResourceId(ResourceId_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace framework { namespace detail {

struct theTabBarButtonType : public rtl::StaticWithInit< ::css::uno::Type *, theTabBarButtonType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.framework.TabBarButton" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "ButtonLabel" );
        ::rtl::OUString the_name1( "HelpText" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::drawing::framework::XResourceId > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.drawing.framework.XResourceId" );
        ::rtl::OUString the_name2( "ResourceId" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace framework {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::framework::TabBarButton const *) {
    return *detail::theTabBarButtonType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::framework::TabBarButton const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::framework::TabBarButton >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_TABBARBUTTON_HPP
