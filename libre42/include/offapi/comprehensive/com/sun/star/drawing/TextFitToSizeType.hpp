#ifndef INCLUDED_COM_SUN_STAR_DRAWING_TEXTFITTOSIZETYPE_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_TEXTFITTOSIZETYPE_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/TextFitToSizeType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theTextFitToSizeTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theTextFitToSizeTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.drawing.TextFitToSizeType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[4];
        ::rtl::OUString sEnumValue0( "NONE" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "PROPORTIONAL" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "ALLLINES" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "AUTOFIT" );
        enumValueNames[3] = sEnumValue3.pData;

        sal_Int32 enumValues[4];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::drawing::TextFitToSizeType_NONE,
            4, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::TextFitToSizeType const *) {
    return *detail::theTextFitToSizeTypeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::TextFitToSizeType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::TextFitToSizeType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_TEXTFITTOSIZETYPE_HPP
