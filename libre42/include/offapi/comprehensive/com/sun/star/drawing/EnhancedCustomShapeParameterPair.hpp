#ifndef INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPEPARAMETERPAIR_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPEPARAMETERPAIR_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/EnhancedCustomShapeParameterPair.hdl"

#include "com/sun/star/drawing/EnhancedCustomShapeParameter.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline EnhancedCustomShapeParameterPair::EnhancedCustomShapeParameterPair() SAL_THROW(())
    : First()
    , Second()
{
}

inline EnhancedCustomShapeParameterPair::EnhancedCustomShapeParameterPair(const ::css::drawing::EnhancedCustomShapeParameter& First_, const ::css::drawing::EnhancedCustomShapeParameter& Second_) SAL_THROW(())
    : First(First_)
    , Second(Second_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theEnhancedCustomShapeParameterPairType : public rtl::StaticWithInit< ::css::uno::Type *, theEnhancedCustomShapeParameterPairType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.EnhancedCustomShapeParameterPair" );
        ::cppu::UnoType< ::css::drawing::EnhancedCustomShapeParameter >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.EnhancedCustomShapeParameter" );
        ::rtl::OUString the_name0( "First" );
        ::rtl::OUString the_name1( "Second" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::EnhancedCustomShapeParameterPair const *) {
    return *detail::theEnhancedCustomShapeParameterPairType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::EnhancedCustomShapeParameterPair const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::EnhancedCustomShapeParameterPair >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPEPARAMETERPAIR_HPP
