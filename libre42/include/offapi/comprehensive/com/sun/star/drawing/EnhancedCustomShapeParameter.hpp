#ifndef INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPEPARAMETER_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPEPARAMETER_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/EnhancedCustomShapeParameter.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline EnhancedCustomShapeParameter::EnhancedCustomShapeParameter() SAL_THROW(())
    : Value()
    , Type(0)
{
}

inline EnhancedCustomShapeParameter::EnhancedCustomShapeParameter(const ::css::uno::Any& Value_, const ::sal_Int16& Type_) SAL_THROW(())
    : Value(Value_)
    , Type(Type_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theEnhancedCustomShapeParameterType : public rtl::StaticWithInit< ::css::uno::Type *, theEnhancedCustomShapeParameterType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.EnhancedCustomShapeParameter" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "Value" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "Type" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::EnhancedCustomShapeParameter const *) {
    return *detail::theEnhancedCustomShapeParameterType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::EnhancedCustomShapeParameter const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::EnhancedCustomShapeParameter >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_ENHANCEDCUSTOMSHAPEPARAMETER_HPP
