#ifndef INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_MODULECONTROLLER_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_MODULECONTROLLER_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/drawing/framework/XModuleController.hpp"
#include "com/sun/star/frame/XController.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace drawing { namespace framework {

class ModuleController {
public:
    static ::css::uno::Reference< ::css::drawing::framework::XModuleController > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Reference< ::css::frame::XController >& xController) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= xController;
        ::css::uno::Reference< ::css::drawing::framework::XModuleController > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::drawing::framework::XModuleController >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.drawing.framework.ModuleController" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ModuleController of type com.sun.star.drawing.framework.XModuleController: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.drawing.framework.ModuleController of type com.sun.star.drawing.framework.XModuleController" ), the_context);
        }
        return the_instance;
    }

private:
    ModuleController(); // not implemented
    ModuleController(ModuleController &); // not implemented
    ~ModuleController(); // not implemented
    void operator =(ModuleController); // not implemented
};

} } } } }

#endif // INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_MODULECONTROLLER_HPP
