#ifndef INCLUDED_COM_SUN_STAR_DRAWING_LINEDASH_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_LINEDASH_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/LineDash.hdl"

#include "com/sun/star/drawing/DashStyle.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline LineDash::LineDash() SAL_THROW(())
    : Style(::css::drawing::DashStyle_RECT)
    , Dots(0)
    , DotLen(0)
    , Dashes(0)
    , DashLen(0)
    , Distance(0)
{
}

inline LineDash::LineDash(const ::css::drawing::DashStyle& Style_, const ::sal_Int16& Dots_, const ::sal_Int32& DotLen_, const ::sal_Int16& Dashes_, const ::sal_Int32& DashLen_, const ::sal_Int32& Distance_) SAL_THROW(())
    : Style(Style_)
    , Dots(Dots_)
    , DotLen(DotLen_)
    , Dashes(Dashes_)
    , DashLen(DashLen_)
    , Distance(Distance_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theLineDashType : public rtl::StaticWithInit< ::css::uno::Type *, theLineDashType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.LineDash" );
        ::cppu::UnoType< ::css::drawing::DashStyle >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.DashStyle" );
        ::rtl::OUString the_name0( "Style" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "Dots" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name2( "DotLen" );
        ::rtl::OUString the_name3( "Dashes" );
        ::rtl::OUString the_name4( "DashLen" );
        ::rtl::OUString the_name5( "Distance" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::LineDash const *) {
    return *detail::theLineDashType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::LineDash const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::LineDash >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_LINEDASH_HPP
