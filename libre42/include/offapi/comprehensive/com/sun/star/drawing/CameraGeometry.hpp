#ifndef INCLUDED_COM_SUN_STAR_DRAWING_CAMERAGEOMETRY_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_CAMERAGEOMETRY_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/CameraGeometry.hdl"

#include "com/sun/star/drawing/Direction3D.hpp"
#include "com/sun/star/drawing/Position3D.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline CameraGeometry::CameraGeometry() SAL_THROW(())
    : vrp()
    , vpn()
    , vup()
{
}

inline CameraGeometry::CameraGeometry(const ::css::drawing::Position3D& vrp_, const ::css::drawing::Direction3D& vpn_, const ::css::drawing::Direction3D& vup_) SAL_THROW(())
    : vrp(vrp_)
    , vpn(vpn_)
    , vup(vup_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theCameraGeometryType : public rtl::StaticWithInit< ::css::uno::Type *, theCameraGeometryType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.CameraGeometry" );
        ::cppu::UnoType< ::css::drawing::Position3D >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.Position3D" );
        ::rtl::OUString the_name0( "vrp" );
        ::cppu::UnoType< ::css::drawing::Direction3D >::get();
        ::rtl::OUString the_tname1( "com.sun.star.drawing.Direction3D" );
        ::rtl::OUString the_name1( "vpn" );
        ::rtl::OUString the_name2( "vup" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::CameraGeometry const *) {
    return *detail::theCameraGeometryType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::CameraGeometry const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::CameraGeometry >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_CAMERAGEOMETRY_HPP
