#ifndef INCLUDED_COM_SUN_STAR_DRAWING_GLUEPOINT_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_GLUEPOINT_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/GluePoint.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline GluePoint::GluePoint() SAL_THROW(())
    : Position()
    , EscapeDirection(0)
    , PositionAbsolute(false)
    , Alignment(0)
{
}

inline GluePoint::GluePoint(const ::css::awt::Point& Position_, const ::sal_Int16& EscapeDirection_, const ::sal_Bool& PositionAbsolute_, const ::sal_Int16& Alignment_) SAL_THROW(())
    : Position(Position_)
    , EscapeDirection(EscapeDirection_)
    , PositionAbsolute(PositionAbsolute_)
    , Alignment(Alignment_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theGluePointType : public rtl::StaticWithInit< ::css::uno::Type *, theGluePointType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.GluePoint" );
        ::cppu::UnoType< ::css::awt::Point >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.Point" );
        ::rtl::OUString the_name0( "Position" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "EscapeDirection" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name2( "PositionAbsolute" );
        ::rtl::OUString the_name3( "Alignment" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::GluePoint const *) {
    return *detail::theGluePointType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::GluePoint const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::GluePoint >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_GLUEPOINT_HPP
