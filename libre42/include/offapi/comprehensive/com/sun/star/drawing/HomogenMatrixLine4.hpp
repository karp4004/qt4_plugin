#ifndef INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIXLINE4_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIXLINE4_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/HomogenMatrixLine4.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline HomogenMatrixLine4::HomogenMatrixLine4() SAL_THROW(())
    : Column1(0)
    , Column2(0)
    , Column3(0)
    , Column4(0)
{
}

inline HomogenMatrixLine4::HomogenMatrixLine4(const double& Column1_, const double& Column2_, const double& Column3_, const double& Column4_) SAL_THROW(())
    : Column1(Column1_)
    , Column2(Column2_)
    , Column3(Column3_)
    , Column4(Column4_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theHomogenMatrixLine4Type : public rtl::StaticWithInit< ::css::uno::Type *, theHomogenMatrixLine4Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.HomogenMatrixLine4" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "Column1" );
        ::rtl::OUString the_name1( "Column2" );
        ::rtl::OUString the_name2( "Column3" );
        ::rtl::OUString the_name3( "Column4" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::HomogenMatrixLine4 const *) {
    return *detail::theHomogenMatrixLine4Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::HomogenMatrixLine4 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::HomogenMatrixLine4 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIXLINE4_HPP
