#ifndef INCLUDED_COM_SUN_STAR_DRAWING_XDRAWPAGESUMMARIZER_HDL
#define INCLUDED_COM_SUN_STAR_DRAWING_XDRAWPAGESUMMARIZER_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace drawing { class XDrawPage; } } } }
namespace com { namespace sun { namespace star { namespace drawing { class XDrawPages; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace drawing {

class SAL_NO_VTABLE XDrawPageSummarizer : public ::css::uno::XInterface
{
public:

    // Methods
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::css::uno::Reference< ::css::drawing::XDrawPage > SAL_CALL summarize( const ::css::uno::Reference< ::css::drawing::XDrawPages >& xPages ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDrawPageSummarizer() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::XDrawPageSummarizer const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::drawing::XDrawPageSummarizer > *) SAL_THROW(());

#endif
