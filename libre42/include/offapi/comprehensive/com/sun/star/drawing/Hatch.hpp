#ifndef INCLUDED_COM_SUN_STAR_DRAWING_HATCH_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_HATCH_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/Hatch.hdl"

#include "com/sun/star/drawing/HatchStyle.hpp"
#include "com/sun/star/util/Color.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline Hatch::Hatch() SAL_THROW(())
    : Style(::css::drawing::HatchStyle_SINGLE)
    , Color(0)
    , Distance(0)
    , Angle(0)
{
}

inline Hatch::Hatch(const ::css::drawing::HatchStyle& Style_, const ::sal_Int32& Color_, const ::sal_Int32& Distance_, const ::sal_Int32& Angle_) SAL_THROW(())
    : Style(Style_)
    , Color(Color_)
    , Distance(Distance_)
    , Angle(Angle_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theHatchType : public rtl::StaticWithInit< ::css::uno::Type *, theHatchType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.Hatch" );
        ::cppu::UnoType< ::css::drawing::HatchStyle >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.HatchStyle" );
        ::rtl::OUString the_name0( "Style" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "Color" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name2( "Distance" );
        ::rtl::OUString the_name3( "Angle" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::Hatch const *) {
    return *detail::theHatchType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::Hatch const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::Hatch >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_HATCH_HPP
