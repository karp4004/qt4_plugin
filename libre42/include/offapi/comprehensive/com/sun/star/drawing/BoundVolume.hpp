#ifndef INCLUDED_COM_SUN_STAR_DRAWING_BOUNDVOLUME_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_BOUNDVOLUME_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/BoundVolume.hdl"

#include "com/sun/star/drawing/Position3D.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline BoundVolume::BoundVolume() SAL_THROW(())
    : min()
    , max()
{
}

inline BoundVolume::BoundVolume(const ::css::drawing::Position3D& min_, const ::css::drawing::Position3D& max_) SAL_THROW(())
    : min(min_)
    , max(max_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theBoundVolumeType : public rtl::StaticWithInit< ::css::uno::Type *, theBoundVolumeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.BoundVolume" );
        ::cppu::UnoType< ::css::drawing::Position3D >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.Position3D" );
        ::rtl::OUString the_name0( "min" );
        ::rtl::OUString the_name1( "max" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::BoundVolume const *) {
    return *detail::theBoundVolumeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::BoundVolume const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::BoundVolume >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_BOUNDVOLUME_HPP
