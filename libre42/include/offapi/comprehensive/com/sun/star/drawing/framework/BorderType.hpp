#ifndef INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_BORDERTYPE_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_BORDERTYPE_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/framework/BorderType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace drawing { namespace framework { namespace detail {

struct theBorderTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theBorderTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.drawing.framework.BorderType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[3];
        ::rtl::OUString sEnumValue0( "INNER_BORDER" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "OUTER_BORDER" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "TOTAL_BORDER" );
        enumValueNames[2] = sEnumValue2.pData;

        sal_Int32 enumValues[3];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::drawing::framework::BorderType_INNER_BORDER,
            3, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace framework {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::framework::BorderType const *) {
    return *detail::theBorderTypeType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::framework::BorderType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::framework::BorderType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_BORDERTYPE_HPP
