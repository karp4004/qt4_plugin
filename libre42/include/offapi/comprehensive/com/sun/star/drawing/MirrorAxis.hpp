#ifndef INCLUDED_COM_SUN_STAR_DRAWING_MIRRORAXIS_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_MIRRORAXIS_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/MirrorAxis.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theMirrorAxisType : public rtl::StaticWithInit< ::css::uno::Type *, theMirrorAxisType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.drawing.MirrorAxis" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[2];
        ::rtl::OUString sEnumValue0( "VERTICAL" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "HORIZONTAL" );
        enumValueNames[1] = sEnumValue1.pData;

        sal_Int32 enumValues[2];
        enumValues[0] = 0;
        enumValues[1] = 1;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::drawing::MirrorAxis_VERTICAL,
            2, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::MirrorAxis const *) {
    return *detail::theMirrorAxisType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::MirrorAxis const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::MirrorAxis >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_MIRRORAXIS_HPP
