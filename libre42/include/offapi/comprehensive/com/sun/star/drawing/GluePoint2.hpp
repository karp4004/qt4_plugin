#ifndef INCLUDED_COM_SUN_STAR_DRAWING_GLUEPOINT2_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_GLUEPOINT2_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/GluePoint2.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/drawing/Alignment.hpp"
#include "com/sun/star/drawing/EscapeDirection.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline GluePoint2::GluePoint2() SAL_THROW(())
    : Position()
    , IsRelative(false)
    , PositionAlignment(::css::drawing::Alignment_TOP_LEFT)
    , Escape(::css::drawing::EscapeDirection_SMART)
    , IsUserDefined(false)
{
}

inline GluePoint2::GluePoint2(const ::css::awt::Point& Position_, const ::sal_Bool& IsRelative_, const ::css::drawing::Alignment& PositionAlignment_, const ::css::drawing::EscapeDirection& Escape_, const ::sal_Bool& IsUserDefined_) SAL_THROW(())
    : Position(Position_)
    , IsRelative(IsRelative_)
    , PositionAlignment(PositionAlignment_)
    , Escape(Escape_)
    , IsUserDefined(IsUserDefined_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theGluePoint2Type : public rtl::StaticWithInit< ::css::uno::Type *, theGluePoint2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.GluePoint2" );
        ::cppu::UnoType< ::css::awt::Point >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.Point" );
        ::rtl::OUString the_name0( "Position" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "IsRelative" );
        ::cppu::UnoType< ::css::drawing::Alignment >::get();
        ::rtl::OUString the_tname2( "com.sun.star.drawing.Alignment" );
        ::rtl::OUString the_name2( "PositionAlignment" );
        ::cppu::UnoType< ::css::drawing::EscapeDirection >::get();
        ::rtl::OUString the_tname3( "com.sun.star.drawing.EscapeDirection" );
        ::rtl::OUString the_name3( "Escape" );
        ::rtl::OUString the_name4( "IsUserDefined" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname3.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::GluePoint2 const *) {
    return *detail::theGluePoint2Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::GluePoint2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::GluePoint2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_GLUEPOINT2_HPP
