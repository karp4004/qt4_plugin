#ifndef INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONBEZIERCOORDS_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONBEZIERCOORDS_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/PolyPolygonBezierCoords.hdl"

#include "com/sun/star/drawing/FlagSequenceSequence.hpp"
#include "com/sun/star/drawing/PointSequenceSequence.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline PolyPolygonBezierCoords::PolyPolygonBezierCoords() SAL_THROW(())
    : Coordinates()
    , Flags()
{
}

inline PolyPolygonBezierCoords::PolyPolygonBezierCoords(const ::css::uno::Sequence< ::css::uno::Sequence< ::css::awt::Point > >& Coordinates_, const ::css::uno::Sequence< ::css::uno::Sequence< ::css::drawing::PolygonFlags > >& Flags_) SAL_THROW(())
    : Coordinates(Coordinates_)
    , Flags(Flags_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct thePolyPolygonBezierCoordsType : public rtl::StaticWithInit< ::css::uno::Type *, thePolyPolygonBezierCoordsType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.PolyPolygonBezierCoords" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::cppu::UnoSequenceType< ::css::awt::Point > > >::get();
        ::rtl::OUString the_tname0( "[][]com.sun.star.awt.Point" );
        ::rtl::OUString the_name0( "Coordinates" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::cppu::UnoSequenceType< ::css::drawing::PolygonFlags > > >::get();
        ::rtl::OUString the_tname1( "[][]com.sun.star.drawing.PolygonFlags" );
        ::rtl::OUString the_name1( "Flags" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::PolyPolygonBezierCoords const *) {
    return *detail::thePolyPolygonBezierCoordsType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::PolyPolygonBezierCoords const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::PolyPolygonBezierCoords >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONBEZIERCOORDS_HPP
