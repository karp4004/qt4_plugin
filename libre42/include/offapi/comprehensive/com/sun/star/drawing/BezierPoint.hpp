#ifndef INCLUDED_COM_SUN_STAR_DRAWING_BEZIERPOINT_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_BEZIERPOINT_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/BezierPoint.hdl"

#include "com/sun/star/awt/Point.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline BezierPoint::BezierPoint() SAL_THROW(())
    : Position()
    , ControlPoint1()
    , ControlPoint2()
{
}

inline BezierPoint::BezierPoint(const ::css::awt::Point& Position_, const ::css::awt::Point& ControlPoint1_, const ::css::awt::Point& ControlPoint2_) SAL_THROW(())
    : Position(Position_)
    , ControlPoint1(ControlPoint1_)
    , ControlPoint2(ControlPoint2_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theBezierPointType : public rtl::StaticWithInit< ::css::uno::Type *, theBezierPointType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.BezierPoint" );
        ::cppu::UnoType< ::css::awt::Point >::get();
        ::rtl::OUString the_tname0( "com.sun.star.awt.Point" );
        ::rtl::OUString the_name0( "Position" );
        ::rtl::OUString the_name1( "ControlPoint1" );
        ::rtl::OUString the_name2( "ControlPoint2" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::BezierPoint const *) {
    return *detail::theBezierPointType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::BezierPoint const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::BezierPoint >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_BEZIERPOINT_HPP
