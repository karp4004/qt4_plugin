#ifndef INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_CONFIGURATIONCHANGEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_CONFIGURATIONCHANGEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/framework/ConfigurationChangeEvent.hdl"

#include "com/sun/star/drawing/framework/XConfiguration.hpp"
#include "com/sun/star/drawing/framework/XResourceId.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing { namespace framework {

inline ConfigurationChangeEvent::ConfigurationChangeEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Type()
    , Configuration()
    , ResourceId()
    , ResourceObject()
    , UserData()
{
}

inline ConfigurationChangeEvent::ConfigurationChangeEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::rtl::OUString& Type_, const ::css::uno::Reference< ::css::drawing::framework::XConfiguration >& Configuration_, const ::css::uno::Reference< ::css::drawing::framework::XResourceId >& ResourceId_, const ::css::uno::Reference< ::css::uno::XInterface >& ResourceObject_, const ::css::uno::Any& UserData_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Type(Type_)
    , Configuration(Configuration_)
    , ResourceId(ResourceId_)
    , ResourceObject(ResourceObject_)
    , UserData(UserData_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace framework { namespace detail {

struct theConfigurationChangeEventType : public rtl::StaticWithInit< ::css::uno::Type *, theConfigurationChangeEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.framework.ConfigurationChangeEvent" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "Type" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::drawing::framework::XConfiguration > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.drawing.framework.XConfiguration" );
        ::rtl::OUString the_name1( "Configuration" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::drawing::framework::XResourceId > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.drawing.framework.XResourceId" );
        ::rtl::OUString the_name2( "ResourceId" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get();
        ::rtl::OUString the_tname3( "com.sun.star.uno.XInterface" );
        ::rtl::OUString the_name3( "ResourceObject" );
        ::rtl::OUString the_tname4( "any" );
        ::rtl::OUString the_name4( "UserData" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name3.pData }, false },
            { { typelib_TypeClass_ANY, the_tname4.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace framework {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::framework::ConfigurationChangeEvent const *) {
    return *detail::theConfigurationChangeEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::framework::ConfigurationChangeEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::framework::ConfigurationChangeEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_FRAMEWORK_CONFIGURATIONCHANGEEVENT_HPP
