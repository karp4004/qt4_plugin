#ifndef INCLUDED_COM_SUN_STAR_DRAWING_DIRECTION3D_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_DIRECTION3D_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/Direction3D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline Direction3D::Direction3D() SAL_THROW(())
    : DirectionX(0)
    , DirectionY(0)
    , DirectionZ(0)
{
}

inline Direction3D::Direction3D(const double& DirectionX_, const double& DirectionY_, const double& DirectionZ_) SAL_THROW(())
    : DirectionX(DirectionX_)
    , DirectionY(DirectionY_)
    , DirectionZ(DirectionZ_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theDirection3DType : public rtl::StaticWithInit< ::css::uno::Type *, theDirection3DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.Direction3D" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "DirectionX" );
        ::rtl::OUString the_name1( "DirectionY" );
        ::rtl::OUString the_name2( "DirectionZ" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::Direction3D const *) {
    return *detail::theDirection3DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::Direction3D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::Direction3D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_DIRECTION3D_HPP
