#ifndef INCLUDED_COM_SUN_STAR_DRAWING_POSITION3D_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_POSITION3D_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/Position3D.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline Position3D::Position3D() SAL_THROW(())
    : PositionX(0)
    , PositionY(0)
    , PositionZ(0)
{
}

inline Position3D::Position3D(const double& PositionX_, const double& PositionY_, const double& PositionZ_) SAL_THROW(())
    : PositionX(PositionX_)
    , PositionY(PositionY_)
    , PositionZ(PositionZ_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct thePosition3DType : public rtl::StaticWithInit< ::css::uno::Type *, thePosition3DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.Position3D" );
        ::rtl::OUString the_tname0( "double" );
        ::rtl::OUString the_name0( "PositionX" );
        ::rtl::OUString the_name1( "PositionY" );
        ::rtl::OUString the_name2( "PositionZ" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_DOUBLE, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::Position3D const *) {
    return *detail::thePosition3DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::Position3D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::Position3D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_POSITION3D_HPP
