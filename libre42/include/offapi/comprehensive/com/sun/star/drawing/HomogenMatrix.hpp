#ifndef INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/HomogenMatrix.hdl"

#include "com/sun/star/drawing/HomogenMatrixLine.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline HomogenMatrix::HomogenMatrix() SAL_THROW(())
    : Line1()
    , Line2()
    , Line3()
    , Line4()
{
}

inline HomogenMatrix::HomogenMatrix(const ::css::drawing::HomogenMatrixLine& Line1_, const ::css::drawing::HomogenMatrixLine& Line2_, const ::css::drawing::HomogenMatrixLine& Line3_, const ::css::drawing::HomogenMatrixLine& Line4_) SAL_THROW(())
    : Line1(Line1_)
    , Line2(Line2_)
    , Line3(Line3_)
    , Line4(Line4_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theHomogenMatrixType : public rtl::StaticWithInit< ::css::uno::Type *, theHomogenMatrixType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.HomogenMatrix" );
        ::cppu::UnoType< ::css::drawing::HomogenMatrixLine >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.HomogenMatrixLine" );
        ::rtl::OUString the_name0( "Line1" );
        ::rtl::OUString the_name1( "Line2" );
        ::rtl::OUString the_name2( "Line3" );
        ::rtl::OUString the_name3( "Line4" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::HomogenMatrix const *) {
    return *detail::theHomogenMatrixType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::HomogenMatrix const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::HomogenMatrix >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX_HPP
