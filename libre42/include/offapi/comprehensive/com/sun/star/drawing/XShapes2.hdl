#ifndef INCLUDED_COM_SUN_STAR_DRAWING_XSHAPES2_HDL
#define INCLUDED_COM_SUN_STAR_DRAWING_XSHAPES2_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace drawing { class XShape; } } } }
#include "com/sun/star/drawing/XShapes.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace drawing {

class SAL_NO_VTABLE XShapes2 : public ::css::drawing::XShapes
{
public:

    // Methods
    virtual void SAL_CALL addTop( const ::css::uno::Reference< ::css::drawing::XShape >& xShape ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addBottom( const ::css::uno::Reference< ::css::drawing::XShape >& xShape ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XShapes2() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::XShapes2 const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::drawing::XShapes2 > *) SAL_THROW(());

#endif
