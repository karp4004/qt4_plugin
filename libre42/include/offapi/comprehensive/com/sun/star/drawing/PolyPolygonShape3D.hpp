#ifndef INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONSHAPE3D_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONSHAPE3D_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/PolyPolygonShape3D.hdl"

#include "com/sun/star/drawing/DoubleSequenceSequence.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline PolyPolygonShape3D::PolyPolygonShape3D() SAL_THROW(())
    : SequenceX()
    , SequenceY()
    , SequenceZ()
{
}

inline PolyPolygonShape3D::PolyPolygonShape3D(const ::css::uno::Sequence< ::css::uno::Sequence< double > >& SequenceX_, const ::css::uno::Sequence< ::css::uno::Sequence< double > >& SequenceY_, const ::css::uno::Sequence< ::css::uno::Sequence< double > >& SequenceZ_) SAL_THROW(())
    : SequenceX(SequenceX_)
    , SequenceY(SequenceY_)
    , SequenceZ(SequenceZ_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct thePolyPolygonShape3DType : public rtl::StaticWithInit< ::css::uno::Type *, thePolyPolygonShape3DType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.PolyPolygonShape3D" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::cppu::UnoSequenceType< double > > >::get();
        ::rtl::OUString the_tname0( "[][]double" );
        ::rtl::OUString the_name0( "SequenceX" );
        ::rtl::OUString the_name1( "SequenceY" );
        ::rtl::OUString the_name2( "SequenceZ" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::PolyPolygonShape3D const *) {
    return *detail::thePolyPolygonShape3DType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::PolyPolygonShape3D const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::PolyPolygonShape3D >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_POLYPOLYGONSHAPE3D_HPP
