#ifndef INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX4_HPP
#define INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX4_HPP

#include "sal/config.h"

#include "com/sun/star/drawing/HomogenMatrix4.hdl"

#include "com/sun/star/drawing/HomogenMatrixLine4.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace drawing {

inline HomogenMatrix4::HomogenMatrix4() SAL_THROW(())
    : Line1()
    , Line2()
    , Line3()
    , Line4()
{
}

inline HomogenMatrix4::HomogenMatrix4(const ::css::drawing::HomogenMatrixLine4& Line1_, const ::css::drawing::HomogenMatrixLine4& Line2_, const ::css::drawing::HomogenMatrixLine4& Line3_, const ::css::drawing::HomogenMatrixLine4& Line4_) SAL_THROW(())
    : Line1(Line1_)
    , Line2(Line2_)
    , Line3(Line3_)
    , Line4(Line4_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace drawing { namespace detail {

struct theHomogenMatrix4Type : public rtl::StaticWithInit< ::css::uno::Type *, theHomogenMatrix4Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.drawing.HomogenMatrix4" );
        ::cppu::UnoType< ::css::drawing::HomogenMatrixLine4 >::get();
        ::rtl::OUString the_tname0( "com.sun.star.drawing.HomogenMatrixLine4" );
        ::rtl::OUString the_name0( "Line1" );
        ::rtl::OUString the_name1( "Line2" );
        ::rtl::OUString the_name2( "Line3" );
        ::rtl::OUString the_name3( "Line4" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace drawing {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::drawing::HomogenMatrix4 const *) {
    return *detail::theHomogenMatrix4Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::drawing::HomogenMatrix4 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::drawing::HomogenMatrix4 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DRAWING_HOMOGENMATRIX4_HPP
