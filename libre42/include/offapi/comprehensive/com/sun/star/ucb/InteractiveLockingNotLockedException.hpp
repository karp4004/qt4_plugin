#ifndef INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGNOTLOCKEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGNOTLOCKEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/InteractiveLockingNotLockedException.hdl"

#include "com/sun/star/ucb/InteractiveLockingException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline InteractiveLockingNotLockedException::InteractiveLockingNotLockedException() SAL_THROW(())
    : ::css::ucb::InteractiveLockingException()
{
    ::cppu::UnoType< ::css::ucb::InteractiveLockingNotLockedException >::get();
}

inline InteractiveLockingNotLockedException::InteractiveLockingNotLockedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_, const ::rtl::OUString& Url_) SAL_THROW(())
    : ::css::ucb::InteractiveLockingException(Message_, Context_, Classification_, Url_)
{
    ::cppu::UnoType< ::css::ucb::InteractiveLockingNotLockedException >::get();
}

InteractiveLockingNotLockedException::InteractiveLockingNotLockedException(InteractiveLockingNotLockedException const & the_other): ::css::ucb::InteractiveLockingException(the_other) {}

InteractiveLockingNotLockedException::~InteractiveLockingNotLockedException() {}

InteractiveLockingNotLockedException & InteractiveLockingNotLockedException::operator =(InteractiveLockingNotLockedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::ucb::InteractiveLockingException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb { namespace detail {

struct theInteractiveLockingNotLockedExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInteractiveLockingNotLockedExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.ucb.InteractiveLockingNotLockedException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::ucb::InteractiveLockingException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveLockingNotLockedException const *) {
    return *detail::theInteractiveLockingNotLockedExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveLockingNotLockedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::InteractiveLockingNotLockedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_INTERACTIVELOCKINGNOTLOCKEDEXCEPTION_HPP
