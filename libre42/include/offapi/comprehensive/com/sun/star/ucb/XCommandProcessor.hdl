#ifndef INCLUDED_COM_SUN_STAR_UCB_XCOMMANDPROCESSOR_HDL
#define INCLUDED_COM_SUN_STAR_UCB_XCOMMANDPROCESSOR_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/ucb/Command.hdl"
#include "com/sun/star/ucb/CommandAbortedException.hdl"
namespace com { namespace sun { namespace star { namespace ucb { class XCommandEnvironment; } } } }
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Any.h"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ucb {

class SAL_NO_VTABLE XCommandProcessor : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::sal_Int32 SAL_CALL createCommandIdentifier() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Any SAL_CALL execute( const ::css::ucb::Command& aCommand, ::sal_Int32 CommandId, const ::css::uno::Reference< ::css::ucb::XCommandEnvironment >& Environment ) /* throw (::css::uno::Exception, ::css::ucb::CommandAbortedException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL abort( ::sal_Int32 CommandId ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XCommandProcessor() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::XCommandProcessor const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ucb::XCommandProcessor > *) SAL_THROW(());

#endif
