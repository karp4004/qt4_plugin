#ifndef INCLUDED_COM_SUN_STAR_UCB_COMMANDENVIRONMENT_HPP
#define INCLUDED_COM_SUN_STAR_UCB_COMMANDENVIRONMENT_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/task/XInteractionHandler.hpp"
#include "com/sun/star/ucb/XCommandEnvironment.hpp"
#include "com/sun/star/ucb/XProgressHandler.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

class CommandEnvironment {
public:
    static ::css::uno::Reference< ::css::ucb::XCommandEnvironment > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::css::uno::Reference< ::css::task::XInteractionHandler >& InteractionHandler, const ::css::uno::Reference< ::css::ucb::XProgressHandler >& ProgressHandler) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(2);
        the_arguments[0] <<= InteractionHandler;
        the_arguments[1] <<= ProgressHandler;
        ::css::uno::Reference< ::css::ucb::XCommandEnvironment > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::ucb::XCommandEnvironment >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.ucb.CommandEnvironment" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ucb.CommandEnvironment of type com.sun.star.ucb.XCommandEnvironment: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.ucb.CommandEnvironment of type com.sun.star.ucb.XCommandEnvironment" ), the_context);
        }
        return the_instance;
    }

private:
    CommandEnvironment(); // not implemented
    CommandEnvironment(CommandEnvironment &); // not implemented
    ~CommandEnvironment(); // not implemented
    void operator =(CommandEnvironment); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_UCB_COMMANDENVIRONMENT_HPP
