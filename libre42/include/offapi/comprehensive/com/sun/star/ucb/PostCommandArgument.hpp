#ifndef INCLUDED_COM_SUN_STAR_UCB_POSTCOMMANDARGUMENT_HPP
#define INCLUDED_COM_SUN_STAR_UCB_POSTCOMMANDARGUMENT_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/PostCommandArgument.hdl"

#include "com/sun/star/io/XInputStream.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace ucb {

inline PostCommandArgument::PostCommandArgument() SAL_THROW(())
    : Source()
    , Sink()
{
}

inline PostCommandArgument::PostCommandArgument(const ::css::uno::Reference< ::css::io::XInputStream >& Source_, const ::css::uno::Reference< ::css::uno::XInterface >& Sink_) SAL_THROW(())
    : Source(Source_)
    , Sink(Sink_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb { namespace detail {

struct thePostCommandArgumentType : public rtl::StaticWithInit< ::css::uno::Type *, thePostCommandArgumentType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.ucb.PostCommandArgument" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::io::XInputStream > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.io.XInputStream" );
        ::rtl::OUString the_name0( "Source" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.uno.XInterface" );
        ::rtl::OUString the_name1( "Sink" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::PostCommandArgument const *) {
    return *detail::thePostCommandArgumentType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::PostCommandArgument const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::PostCommandArgument >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_POSTCOMMANDARGUMENT_HPP
