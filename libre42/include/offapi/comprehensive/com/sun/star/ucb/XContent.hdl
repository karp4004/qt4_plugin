#ifndef INCLUDED_COM_SUN_STAR_UCB_XCONTENT_HDL
#define INCLUDED_COM_SUN_STAR_UCB_XCONTENT_HDL

#include "sal/config.h"

#include <exception>

namespace com { namespace sun { namespace star { namespace ucb { class XContentEventListener; } } } }
namespace com { namespace sun { namespace star { namespace ucb { class XContentIdentifier; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace ucb {

class SAL_NO_VTABLE XContent : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::ucb::XContentIdentifier > SAL_CALL getIdentifier() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getContentType() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL addContentEventListener( const ::css::uno::Reference< ::css::ucb::XContentEventListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL removeContentEventListener( const ::css::uno::Reference< ::css::ucb::XContentEventListener >& Listener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XContent() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::XContent const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::ucb::XContent > *) SAL_THROW(());

#endif
