#ifndef INCLUDED_COM_SUN_STAR_UCB_INTERACTIVENETWORKGENERALEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_INTERACTIVENETWORKGENERALEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/InteractiveNetworkGeneralException.hdl"

#include "com/sun/star/ucb/InteractiveNetworkException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline InteractiveNetworkGeneralException::InteractiveNetworkGeneralException() SAL_THROW(())
    : ::css::ucb::InteractiveNetworkException()
{
    ::cppu::UnoType< ::css::ucb::InteractiveNetworkGeneralException >::get();
}

inline InteractiveNetworkGeneralException::InteractiveNetworkGeneralException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::task::InteractionClassification& Classification_) SAL_THROW(())
    : ::css::ucb::InteractiveNetworkException(Message_, Context_, Classification_)
{
    ::cppu::UnoType< ::css::ucb::InteractiveNetworkGeneralException >::get();
}

InteractiveNetworkGeneralException::InteractiveNetworkGeneralException(InteractiveNetworkGeneralException const & the_other): ::css::ucb::InteractiveNetworkException(the_other) {}

InteractiveNetworkGeneralException::~InteractiveNetworkGeneralException() {}

InteractiveNetworkGeneralException & InteractiveNetworkGeneralException::operator =(InteractiveNetworkGeneralException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::ucb::InteractiveNetworkException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb { namespace detail {

struct theInteractiveNetworkGeneralExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theInteractiveNetworkGeneralExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.ucb.InteractiveNetworkGeneralException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::ucb::InteractiveNetworkException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveNetworkGeneralException const *) {
    return *detail::theInteractiveNetworkGeneralExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::InteractiveNetworkGeneralException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::InteractiveNetworkGeneralException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_INTERACTIVENETWORKGENERALEXCEPTION_HPP
