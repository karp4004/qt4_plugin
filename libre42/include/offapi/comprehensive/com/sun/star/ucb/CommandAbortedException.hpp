#ifndef INCLUDED_COM_SUN_STAR_UCB_COMMANDABORTEDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_UCB_COMMANDABORTEDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ucb/CommandAbortedException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace ucb {

inline CommandAbortedException::CommandAbortedException() SAL_THROW(())
    : ::css::uno::Exception()
{
    ::cppu::UnoType< ::css::ucb::CommandAbortedException >::get();
}

inline CommandAbortedException::CommandAbortedException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
{
    ::cppu::UnoType< ::css::ucb::CommandAbortedException >::get();
}

CommandAbortedException::CommandAbortedException(CommandAbortedException const & the_other): ::css::uno::Exception(the_other) {}

CommandAbortedException::~CommandAbortedException() {}

CommandAbortedException & CommandAbortedException::operator =(CommandAbortedException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ucb { namespace detail {

struct theCommandAbortedExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theCommandAbortedExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.ucb.CommandAbortedException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace ucb {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ucb::CommandAbortedException const *) {
    return *detail::theCommandAbortedExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ucb::CommandAbortedException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ucb::CommandAbortedException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_UCB_COMMANDABORTEDEXCEPTION_HPP
