#ifndef INCLUDED_COM_SUN_STAR_SECURITY_DOCUMENTDIGITALSIGNATURES_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_DOCUMENTDIGITALSIGNATURES_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/security/XDocumentDigitalSignatures.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace security {

class DocumentDigitalSignatures {
public:
    static ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures > createDefault(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.security.DocumentDigitalSignatures" ), ::css::uno::Sequence< ::css::uno::Any >(), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.security.DocumentDigitalSignatures of type com.sun.star.security.XDocumentDigitalSignatures: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.security.DocumentDigitalSignatures of type com.sun.star.security.XDocumentDigitalSignatures" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures > createWithVersion(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& ODFVersion) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(1);
        the_arguments[0] <<= ODFVersion;
        ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.security.DocumentDigitalSignatures" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.security.DocumentDigitalSignatures of type com.sun.star.security.XDocumentDigitalSignatures: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.security.DocumentDigitalSignatures of type com.sun.star.security.XDocumentDigitalSignatures" ), the_context);
        }
        return the_instance;
    }

    static ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures > createWithVersionAndValidSignature(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context, const ::rtl::OUString& ODFVersion, ::sal_Bool HasValidDocumentSignature) {
        assert(the_context.is());
        ::css::uno::Sequence< ::css::uno::Any > the_arguments(2);
        the_arguments[0] <<= ODFVersion;
        the_arguments[1] <<= HasValidDocumentSignature;
        ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::security::XDocumentDigitalSignatures >(the_context->getServiceManager()->createInstanceWithArgumentsAndContext(::rtl::OUString( "com.sun.star.security.DocumentDigitalSignatures" ), the_arguments, the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.security.DocumentDigitalSignatures of type com.sun.star.security.XDocumentDigitalSignatures: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.security.DocumentDigitalSignatures of type com.sun.star.security.XDocumentDigitalSignatures" ), the_context);
        }
        return the_instance;
    }

private:
    DocumentDigitalSignatures(); // not implemented
    DocumentDigitalSignatures(DocumentDigitalSignatures &); // not implemented
    ~DocumentDigitalSignatures(); // not implemented
    void operator =(DocumentDigitalSignatures); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_SECURITY_DOCUMENTDIGITALSIGNATURES_HPP
