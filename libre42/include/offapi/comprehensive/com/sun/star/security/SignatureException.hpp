#ifndef INCLUDED_COM_SUN_STAR_SECURITY_SIGNATUREEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_SIGNATUREEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/security/SignatureException.hdl"

#include "com/sun/star/security/CryptographyException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace security {

inline SignatureException::SignatureException() SAL_THROW(())
    : ::css::security::CryptographyException()
{
    ::cppu::UnoType< ::css::security::SignatureException >::get();
}

inline SignatureException::SignatureException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::security::CryptographyException(Message_, Context_)
{
    ::cppu::UnoType< ::css::security::SignatureException >::get();
}

SignatureException::SignatureException(SignatureException const & the_other): ::css::security::CryptographyException(the_other) {}

SignatureException::~SignatureException() {}

SignatureException & SignatureException::operator =(SignatureException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::security::CryptographyException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace security { namespace detail {

struct theSignatureExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theSignatureExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.security.SignatureException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::security::CryptographyException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::SignatureException const *) {
    return *detail::theSignatureExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::SignatureException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::SignatureException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_SIGNATUREEXCEPTION_HPP
