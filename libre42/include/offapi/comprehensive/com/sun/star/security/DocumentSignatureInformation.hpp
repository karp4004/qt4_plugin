#ifndef INCLUDED_COM_SUN_STAR_SECURITY_DOCUMENTSIGNATUREINFORMATION_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_DOCUMENTSIGNATUREINFORMATION_HPP

#include "sal/config.h"

#include "com/sun/star/security/DocumentSignatureInformation.hdl"

#include "com/sun/star/security/XCertificate.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace security {

inline DocumentSignatureInformation::DocumentSignatureInformation() SAL_THROW(())
    : Signer()
    , SignatureDate(0)
    , SignatureTime(0)
    , SignatureIsValid(false)
    , CertificateStatus(0)
    , PartialDocumentSignature(false)
{
}

inline DocumentSignatureInformation::DocumentSignatureInformation(const ::css::uno::Reference< ::css::security::XCertificate >& Signer_, const ::sal_Int32& SignatureDate_, const ::sal_Int32& SignatureTime_, const ::sal_Bool& SignatureIsValid_, const ::sal_Int32& CertificateStatus_, const ::sal_Bool& PartialDocumentSignature_) SAL_THROW(())
    : Signer(Signer_)
    , SignatureDate(SignatureDate_)
    , SignatureTime(SignatureTime_)
    , SignatureIsValid(SignatureIsValid_)
    , CertificateStatus(CertificateStatus_)
    , PartialDocumentSignature(PartialDocumentSignature_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace security { namespace detail {

struct theDocumentSignatureInformationType : public rtl::StaticWithInit< ::css::uno::Type *, theDocumentSignatureInformationType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.security.DocumentSignatureInformation" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::security::XCertificate > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.security.XCertificate" );
        ::rtl::OUString the_name0( "Signer" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "SignatureDate" );
        ::rtl::OUString the_name2( "SignatureTime" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name3( "SignatureIsValid" );
        ::rtl::OUString the_name4( "CertificateStatus" );
        ::rtl::OUString the_name5( "PartialDocumentSignature" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::DocumentSignatureInformation const *) {
    return *detail::theDocumentSignatureInformationType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::DocumentSignatureInformation const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::DocumentSignatureInformation >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_DOCUMENTSIGNATUREINFORMATION_HPP
