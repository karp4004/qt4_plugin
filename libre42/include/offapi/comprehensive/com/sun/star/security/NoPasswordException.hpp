#ifndef INCLUDED_COM_SUN_STAR_SECURITY_NOPASSWORDEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_NOPASSWORDEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/security/NoPasswordException.hdl"

#include "com/sun/star/uno/SecurityException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace security {

inline NoPasswordException::NoPasswordException() SAL_THROW(())
    : ::css::uno::SecurityException()
{
    ::cppu::UnoType< ::css::security::NoPasswordException >::get();
}

inline NoPasswordException::NoPasswordException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::SecurityException(Message_, Context_)
{
    ::cppu::UnoType< ::css::security::NoPasswordException >::get();
}

NoPasswordException::NoPasswordException(NoPasswordException const & the_other): ::css::uno::SecurityException(the_other) {}

NoPasswordException::~NoPasswordException() {}

NoPasswordException & NoPasswordException::operator =(NoPasswordException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::SecurityException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace security { namespace detail {

struct theNoPasswordExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theNoPasswordExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.security.NoPasswordException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::SecurityException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::NoPasswordException const *) {
    return *detail::theNoPasswordExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::NoPasswordException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::NoPasswordException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_NOPASSWORDEXCEPTION_HPP
