#ifndef INCLUDED_COM_SUN_STAR_SECURITY_CRYPTOGRAPHYEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_CRYPTOGRAPHYEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/security/CryptographyException.hdl"

#include "com/sun/star/uno/SecurityException.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace security {

inline CryptographyException::CryptographyException() SAL_THROW(())
    : ::css::uno::SecurityException()
{
    ::cppu::UnoType< ::css::security::CryptographyException >::get();
}

inline CryptographyException::CryptographyException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_) SAL_THROW(())
    : ::css::uno::SecurityException(Message_, Context_)
{
    ::cppu::UnoType< ::css::security::CryptographyException >::get();
}

CryptographyException::CryptographyException(CryptographyException const & the_other): ::css::uno::SecurityException(the_other) {}

CryptographyException::~CryptographyException() {}

CryptographyException & CryptographyException::operator =(CryptographyException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::SecurityException::operator =(the_other);
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace security { namespace detail {

struct theCryptographyExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theCryptographyExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.security.CryptographyException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::SecurityException >::get();

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            0,
            0 );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::CryptographyException const *) {
    return *detail::theCryptographyExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::CryptographyException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::CryptographyException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_CRYPTOGRAPHYEXCEPTION_HPP
