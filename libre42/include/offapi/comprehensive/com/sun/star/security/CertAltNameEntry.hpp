#ifndef INCLUDED_COM_SUN_STAR_SECURITY_CERTALTNAMEENTRY_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_CERTALTNAMEENTRY_HPP

#include "sal/config.h"

#include "com/sun/star/security/CertAltNameEntry.hdl"

#include "com/sun/star/security/ExtAltNameType.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace security {

inline CertAltNameEntry::CertAltNameEntry() SAL_THROW(())
    : Type(::css::security::ExtAltNameType_OTHER_NAME)
    , Value()
{
}

inline CertAltNameEntry::CertAltNameEntry(const ::css::security::ExtAltNameType& Type_, const ::css::uno::Any& Value_) SAL_THROW(())
    : Type(Type_)
    , Value(Value_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace security { namespace detail {

struct theCertAltNameEntryType : public rtl::StaticWithInit< ::css::uno::Type *, theCertAltNameEntryType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.security.CertAltNameEntry" );
        ::cppu::UnoType< ::css::security::ExtAltNameType >::get();
        ::rtl::OUString the_tname0( "com.sun.star.security.ExtAltNameType" );
        ::rtl::OUString the_name0( "Type" );
        ::rtl::OUString the_tname1( "any" );
        ::rtl::OUString the_name1( "Value" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_ANY, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::CertAltNameEntry const *) {
    return *detail::theCertAltNameEntryType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::CertAltNameEntry const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::CertAltNameEntry >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_CERTALTNAMEENTRY_HPP
