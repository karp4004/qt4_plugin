#ifndef INCLUDED_COM_SUN_STAR_SECURITY_XCERTIFICATE_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_XCERTIFICATE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/security/XCertificate.hdl"

#include "com/sun/star/security/XCertificateExtension.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/util/DateTime.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace security { namespace detail {

struct theXCertificateType : public rtl::StaticWithInit< ::css::uno::Type *, theXCertificateType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.security.XCertificate" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[17] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.security.XCertificate::Version" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.security.XCertificate::SerialNumber" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.security.XCertificate::IssuerName" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.security.XCertificate::SubjectName" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.security.XCertificate::NotValidBefore" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.security.XCertificate::NotValidAfter" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.security.XCertificate::IssuerUniqueID" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.security.XCertificate::SubjectUniqueID" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.security.XCertificate::Extensions" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.security.XCertificate::Encoded" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.security.XCertificate::SubjectPublicKeyAlgorithm" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.security.XCertificate::SubjectPublicKeyValue" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.security.XCertificate::SignatureAlgorithm" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );
        ::rtl::OUString sAttributeName13( "com.sun.star.security.XCertificate::SHA1Thumbprint" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName13.pData );
        ::rtl::OUString sAttributeName14( "com.sun.star.security.XCertificate::MD5Thumbprint" );
        typelib_typedescriptionreference_new( &pMembers[14],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName14.pData );
        ::rtl::OUString sMethodName0( "com.sun.star.security.XCertificate::findCertificateExtension" );
        typelib_typedescriptionreference_new( &pMembers[15],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );
        ::rtl::OUString sMethodName1( "com.sun.star.security.XCertificate::getCertificateUsage" );
        typelib_typedescriptionreference_new( &pMembers[16],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName1.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            17,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescriptionreference_release( pMembers[14] );
        typelib_typedescriptionreference_release( pMembers[15] );
        typelib_typedescriptionreference_release( pMembers[16] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::XCertificate const *) {
    const ::css::uno::Type &rRet = *detail::theXCertificateType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::sal_Int8 > >::get();
            ::cppu::UnoType< ::css::util::DateTime >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::uno::Reference< ::css::security::XCertificateExtension > > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "short" );
                ::rtl::OUString sAttributeName0( "com.sun.star.security.XCertificate::Version" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    3, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType0.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "[]byte" );
                ::rtl::OUString sAttributeName1( "com.sun.star.security.XCertificate::SerialNumber" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    4, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType1.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "string" );
                ::rtl::OUString sAttributeName2( "com.sun.star.security.XCertificate::IssuerName" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    5, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType2.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "string" );
                ::rtl::OUString sAttributeName3( "com.sun.star.security.XCertificate::SubjectName" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    6, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType3.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "com.sun.star.util.DateTime" );
                ::rtl::OUString sAttributeName4( "com.sun.star.security.XCertificate::NotValidBefore" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType4.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "com.sun.star.util.DateTime" );
                ::rtl::OUString sAttributeName5( "com.sun.star.security.XCertificate::NotValidAfter" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    8, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType5.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "[]byte" );
                ::rtl::OUString sAttributeName6( "com.sun.star.security.XCertificate::IssuerUniqueID" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    9, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType6.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "[]byte" );
                ::rtl::OUString sAttributeName7( "com.sun.star.security.XCertificate::SubjectUniqueID" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    10, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType7.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "[]com.sun.star.security.XCertificateExtension" );
                ::rtl::OUString sAttributeName8( "com.sun.star.security.XCertificate::Extensions" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    11, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType8.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "[]byte" );
                ::rtl::OUString sAttributeName9( "com.sun.star.security.XCertificate::Encoded" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    12, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType9.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "string" );
                ::rtl::OUString sAttributeName10( "com.sun.star.security.XCertificate::SubjectPublicKeyAlgorithm" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    13, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType10.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "[]byte" );
                ::rtl::OUString sAttributeName11( "com.sun.star.security.XCertificate::SubjectPublicKeyValue" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    14, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType11.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "string" );
                ::rtl::OUString sAttributeName12( "com.sun.star.security.XCertificate::SignatureAlgorithm" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    15, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType12.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType13( "[]byte" );
                ::rtl::OUString sAttributeName13( "com.sun.star.security.XCertificate::SHA1Thumbprint" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    16, sAttributeName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType13.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType14( "[]byte" );
                ::rtl::OUString sAttributeName14( "com.sun.star.security.XCertificate::MD5Thumbprint" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    17, sAttributeName14.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType14.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                typelib_Parameter_Init aParameters[1];
                ::rtl::OUString sParamName0( "oid" );
                ::rtl::OUString sParamType0( "[]byte" );
                aParameters[0].pParamName = sParamName0.pData;
                aParameters[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
                aParameters[0].pTypeName = sParamType0.pData;
                aParameters[0].bIn = sal_True;
                aParameters[0].bOut = sal_False;
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType0( "com.sun.star.security.XCertificateExtension" );
                ::rtl::OUString sMethodName0( "com.sun.star.security.XCertificate::findCertificateExtension" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    18, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sReturnType0.pData,
                    1, aParameters,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData };
                ::rtl::OUString sReturnType1( "long" );
                ::rtl::OUString sMethodName1( "com.sun.star.security.XCertificate::getCertificateUsage" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    19, sal_False,
                    sMethodName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sReturnType1.pData,
                    0, 0,
                    1, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::security::XCertificate > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::security::XCertificate > >::get();
}

::css::uno::Type const & ::css::security::XCertificate::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::security::XCertificate > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_XCERTIFICATE_HPP
