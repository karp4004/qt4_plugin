#ifndef INCLUDED_COM_SUN_STAR_SECURITY_EXTALTNAMETYPE_HPP
#define INCLUDED_COM_SUN_STAR_SECURITY_EXTALTNAMETYPE_HPP

#include "sal/config.h"

#include "com/sun/star/security/ExtAltNameType.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace security { namespace detail {

struct theExtAltNameTypeType : public rtl::StaticWithInit< ::css::uno::Type *, theExtAltNameTypeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.security.ExtAltNameType" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[9];
        ::rtl::OUString sEnumValue0( "OTHER_NAME" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "RFC822_NAME" );
        enumValueNames[1] = sEnumValue1.pData;
        ::rtl::OUString sEnumValue2( "DNS_NAME" );
        enumValueNames[2] = sEnumValue2.pData;
        ::rtl::OUString sEnumValue3( "DIRECTORY_NAME" );
        enumValueNames[3] = sEnumValue3.pData;
        ::rtl::OUString sEnumValue4( "URL" );
        enumValueNames[4] = sEnumValue4.pData;
        ::rtl::OUString sEnumValue5( "IP_ADDRESS" );
        enumValueNames[5] = sEnumValue5.pData;
        ::rtl::OUString sEnumValue6( "REGISTERED_ID" );
        enumValueNames[6] = sEnumValue6.pData;
        ::rtl::OUString sEnumValue7( "EDI_PARTY_NAME" );
        enumValueNames[7] = sEnumValue7.pData;
        ::rtl::OUString sEnumValue8( "X400_ADDRESS" );
        enumValueNames[8] = sEnumValue8.pData;

        sal_Int32 enumValues[9];
        enumValues[0] = 0;
        enumValues[1] = 1;
        enumValues[2] = 2;
        enumValues[3] = 3;
        enumValues[4] = 4;
        enumValues[5] = 5;
        enumValues[6] = 6;
        enumValues[7] = 7;
        enumValues[8] = 8;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::security::ExtAltNameType_OTHER_NAME,
            9, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace security {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::security::ExtAltNameType const *) {
    return *detail::theExtAltNameTypeType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::security::ExtAltNameType const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::security::ExtAltNameType >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SECURITY_EXTALTNAMETYPE_HPP
