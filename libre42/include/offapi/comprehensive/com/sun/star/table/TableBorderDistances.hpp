#ifndef INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDERDISTANCES_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDERDISTANCES_HPP

#include "sal/config.h"

#include "com/sun/star/table/TableBorderDistances.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline TableBorderDistances::TableBorderDistances() SAL_THROW(())
    : TopDistance(0)
    , IsTopDistanceValid(false)
    , BottomDistance(0)
    , IsBottomDistanceValid(false)
    , LeftDistance(0)
    , IsLeftDistanceValid(false)
    , RightDistance(0)
    , IsRightDistanceValid(false)
{
}

inline TableBorderDistances::TableBorderDistances(const ::sal_Int16& TopDistance_, const ::sal_Bool& IsTopDistanceValid_, const ::sal_Int16& BottomDistance_, const ::sal_Bool& IsBottomDistanceValid_, const ::sal_Int16& LeftDistance_, const ::sal_Bool& IsLeftDistanceValid_, const ::sal_Int16& RightDistance_, const ::sal_Bool& IsRightDistanceValid_) SAL_THROW(())
    : TopDistance(TopDistance_)
    , IsTopDistanceValid(IsTopDistanceValid_)
    , BottomDistance(BottomDistance_)
    , IsBottomDistanceValid(IsBottomDistanceValid_)
    , LeftDistance(LeftDistance_)
    , IsLeftDistanceValid(IsLeftDistanceValid_)
    , RightDistance(RightDistance_)
    , IsRightDistanceValid(IsRightDistanceValid_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theTableBorderDistancesType : public rtl::StaticWithInit< ::css::uno::Type *, theTableBorderDistancesType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.table.TableBorderDistances" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "TopDistance" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "IsTopDistanceValid" );
        ::rtl::OUString the_name2( "BottomDistance" );
        ::rtl::OUString the_name3( "IsBottomDistanceValid" );
        ::rtl::OUString the_name4( "LeftDistance" );
        ::rtl::OUString the_name5( "IsLeftDistanceValid" );
        ::rtl::OUString the_name6( "RightDistance" );
        ::rtl::OUString the_name7( "IsRightDistanceValid" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name7.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 8, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::TableBorderDistances const *) {
    return *detail::theTableBorderDistancesType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::TableBorderDistances const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::TableBorderDistances >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDERDISTANCES_HPP
