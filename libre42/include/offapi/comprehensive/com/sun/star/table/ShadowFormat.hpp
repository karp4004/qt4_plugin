#ifndef INCLUDED_COM_SUN_STAR_TABLE_SHADOWFORMAT_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_SHADOWFORMAT_HPP

#include "sal/config.h"

#include "com/sun/star/table/ShadowFormat.hdl"

#include "com/sun/star/table/ShadowLocation.hpp"
#include "com/sun/star/util/Color.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline ShadowFormat::ShadowFormat() SAL_THROW(())
    : Location(::css::table::ShadowLocation_NONE)
    , ShadowWidth(0)
    , IsTransparent(false)
    , Color(0)
{
}

inline ShadowFormat::ShadowFormat(const ::css::table::ShadowLocation& Location_, const ::sal_Int16& ShadowWidth_, const ::sal_Bool& IsTransparent_, const ::sal_Int32& Color_) SAL_THROW(())
    : Location(Location_)
    , ShadowWidth(ShadowWidth_)
    , IsTransparent(IsTransparent_)
    , Color(Color_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theShadowFormatType : public rtl::StaticWithInit< ::css::uno::Type *, theShadowFormatType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.table.ShadowFormat" );
        ::cppu::UnoType< ::css::table::ShadowLocation >::get();
        ::rtl::OUString the_tname0( "com.sun.star.table.ShadowLocation" );
        ::rtl::OUString the_name0( "Location" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "ShadowWidth" );
        ::rtl::OUString the_tname2( "boolean" );
        ::rtl::OUString the_name2( "IsTransparent" );
        ::rtl::OUString the_tname3( "long" );
        ::rtl::OUString the_name3( "Color" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ENUM, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname3.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::ShadowFormat const *) {
    return *detail::theShadowFormatType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::ShadowFormat const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::ShadowFormat >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_SHADOWFORMAT_HPP
