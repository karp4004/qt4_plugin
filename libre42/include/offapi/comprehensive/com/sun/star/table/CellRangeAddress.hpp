#ifndef INCLUDED_COM_SUN_STAR_TABLE_CELLRANGEADDRESS_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_CELLRANGEADDRESS_HPP

#include "sal/config.h"

#include "com/sun/star/table/CellRangeAddress.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline CellRangeAddress::CellRangeAddress() SAL_THROW(())
    : Sheet(0)
    , StartColumn(0)
    , StartRow(0)
    , EndColumn(0)
    , EndRow(0)
{
}

inline CellRangeAddress::CellRangeAddress(const ::sal_Int16& Sheet_, const ::sal_Int32& StartColumn_, const ::sal_Int32& StartRow_, const ::sal_Int32& EndColumn_, const ::sal_Int32& EndRow_) SAL_THROW(())
    : Sheet(Sheet_)
    , StartColumn(StartColumn_)
    , StartRow(StartRow_)
    , EndColumn(EndColumn_)
    , EndRow(EndRow_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theCellRangeAddressType : public rtl::StaticWithInit< ::css::uno::Type *, theCellRangeAddressType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.table.CellRangeAddress" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "Sheet" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "StartColumn" );
        ::rtl::OUString the_name2( "StartRow" );
        ::rtl::OUString the_name3( "EndColumn" );
        ::rtl::OUString the_name4( "EndRow" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::CellRangeAddress const *) {
    return *detail::theCellRangeAddressType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::CellRangeAddress const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::CellRangeAddress >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_CELLRANGEADDRESS_HPP
