#ifndef INCLUDED_COM_SUN_STAR_TABLE_BORDERLINE2_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_BORDERLINE2_HPP

#include "sal/config.h"

#include "com/sun/star/table/BorderLine2.hdl"

#include "com/sun/star/table/BorderLine.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline BorderLine2::BorderLine2() SAL_THROW(())
    : ::css::table::BorderLine()
    , LineStyle(0)
    , LineWidth(0)
{
}

inline BorderLine2::BorderLine2(const ::sal_Int32& Color_, const ::sal_Int16& InnerLineWidth_, const ::sal_Int16& OuterLineWidth_, const ::sal_Int16& LineDistance_, const ::sal_Int16& LineStyle_, const ::sal_uInt32& LineWidth_) SAL_THROW(())
    : ::css::table::BorderLine(Color_, InnerLineWidth_, OuterLineWidth_, LineDistance_)
    , LineStyle(LineStyle_)
    , LineWidth(LineWidth_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theBorderLine2Type : public rtl::StaticWithInit< ::css::uno::Type *, theBorderLine2Type >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.table.BorderLine2" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "LineStyle" );
        ::rtl::OUString the_tname1( "unsigned long" );
        ::rtl::OUString the_name1( "LineWidth" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_UNSIGNED_LONG, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::table::BorderLine >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::BorderLine2 const *) {
    return *detail::theBorderLine2Type::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::BorderLine2 const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::BorderLine2 >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_BORDERLINE2_HPP
