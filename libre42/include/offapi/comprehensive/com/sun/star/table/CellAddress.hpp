#ifndef INCLUDED_COM_SUN_STAR_TABLE_CELLADDRESS_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_CELLADDRESS_HPP

#include "sal/config.h"

#include "com/sun/star/table/CellAddress.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline CellAddress::CellAddress() SAL_THROW(())
    : Sheet(0)
    , Column(0)
    , Row(0)
{
}

inline CellAddress::CellAddress(const ::sal_Int16& Sheet_, const ::sal_Int32& Column_, const ::sal_Int32& Row_) SAL_THROW(())
    : Sheet(Sheet_)
    , Column(Column_)
    , Row(Row_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theCellAddressType : public rtl::StaticWithInit< ::css::uno::Type *, theCellAddressType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.table.CellAddress" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "Sheet" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "Column" );
        ::rtl::OUString the_name2( "Row" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::CellAddress const *) {
    return *detail::theCellAddressType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::CellAddress const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::CellAddress >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_CELLADDRESS_HPP
