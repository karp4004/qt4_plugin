#ifndef INCLUDED_COM_SUN_STAR_TABLE_BORDERLINE_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_BORDERLINE_HPP

#include "sal/config.h"

#include "com/sun/star/table/BorderLine.hdl"

#include "com/sun/star/util/Color.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline BorderLine::BorderLine() SAL_THROW(())
    : Color(0)
    , InnerLineWidth(0)
    , OuterLineWidth(0)
    , LineDistance(0)
{
}

inline BorderLine::BorderLine(const ::sal_Int32& Color_, const ::sal_Int16& InnerLineWidth_, const ::sal_Int16& OuterLineWidth_, const ::sal_Int16& LineDistance_) SAL_THROW(())
    : Color(Color_)
    , InnerLineWidth(InnerLineWidth_)
    , OuterLineWidth(OuterLineWidth_)
    , LineDistance(LineDistance_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theBorderLineType : public rtl::StaticWithInit< ::css::uno::Type *, theBorderLineType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.table.BorderLine" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Color" );
        ::rtl::OUString the_tname1( "short" );
        ::rtl::OUString the_name1( "InnerLineWidth" );
        ::rtl::OUString the_name2( "OuterLineWidth" );
        ::rtl::OUString the_name3( "LineDistance" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname1.pData, the_name3.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 4, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::BorderLine const *) {
    return *detail::theBorderLineType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::BorderLine const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::BorderLine >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_BORDERLINE_HPP
