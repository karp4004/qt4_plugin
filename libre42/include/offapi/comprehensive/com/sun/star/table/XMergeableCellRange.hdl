#ifndef INCLUDED_COM_SUN_STAR_TABLE_XMERGEABLECELLRANGE_HDL
#define INCLUDED_COM_SUN_STAR_TABLE_XMERGEABLECELLRANGE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/NoSupportException.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace table {

class SAL_NO_VTABLE XMergeableCellRange : public ::css::uno::XInterface
{
public:

    // Methods
    virtual void SAL_CALL merge() /* throw (::css::lang::NoSupportException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL split( ::sal_Int32 Columns, ::sal_Int32 Rows ) /* throw (::css::lang::NoSupportException, ::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL isMergeable() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XMergeableCellRange() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::XMergeableCellRange const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::table::XMergeableCellRange > *) SAL_THROW(());

#endif
