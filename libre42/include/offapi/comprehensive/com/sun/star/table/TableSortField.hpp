#ifndef INCLUDED_COM_SUN_STAR_TABLE_TABLESORTFIELD_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_TABLESORTFIELD_HPP

#include "sal/config.h"

#include "com/sun/star/table/TableSortField.hdl"

#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/table/TableSortFieldType.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline TableSortField::TableSortField() SAL_THROW(())
    : Field(0)
    , IsAscending(false)
    , IsCaseSensitive(false)
    , FieldType(::css::table::TableSortFieldType_AUTOMATIC)
    , CollatorLocale()
    , CollatorAlgorithm()
{
}

inline TableSortField::TableSortField(const ::sal_Int32& Field_, const ::sal_Bool& IsAscending_, const ::sal_Bool& IsCaseSensitive_, const ::css::table::TableSortFieldType& FieldType_, const ::css::lang::Locale& CollatorLocale_, const ::rtl::OUString& CollatorAlgorithm_) SAL_THROW(())
    : Field(Field_)
    , IsAscending(IsAscending_)
    , IsCaseSensitive(IsCaseSensitive_)
    , FieldType(FieldType_)
    , CollatorLocale(CollatorLocale_)
    , CollatorAlgorithm(CollatorAlgorithm_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theTableSortFieldType : public rtl::StaticWithInit< ::css::uno::Type *, theTableSortFieldType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.table.TableSortField" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Field" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "IsAscending" );
        ::rtl::OUString the_name2( "IsCaseSensitive" );
        ::cppu::UnoType< ::css::table::TableSortFieldType >::get();
        ::rtl::OUString the_tname2( "com.sun.star.table.TableSortFieldType" );
        ::rtl::OUString the_name3( "FieldType" );
        ::cppu::UnoType< ::css::lang::Locale >::get();
        ::rtl::OUString the_tname3( "com.sun.star.lang.Locale" );
        ::rtl::OUString the_name4( "CollatorLocale" );
        ::rtl::OUString the_tname4( "string" );
        ::rtl::OUString the_name5( "CollatorAlgorithm" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_ENUM, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname3.pData, the_name4.pData }, false },
            { { typelib_TypeClass_STRING, the_tname4.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::TableSortField const *) {
    return *detail::theTableSortFieldType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::TableSortField const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::TableSortField >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_TABLESORTFIELD_HPP
