#ifndef INCLUDED_COM_SUN_STAR_TABLE_TABLEORIENTATION_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_TABLEORIENTATION_HPP

#include "sal/config.h"

#include "com/sun/star/table/TableOrientation.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theTableOrientationType : public rtl::StaticWithInit< ::css::uno::Type *, theTableOrientationType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.table.TableOrientation" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[2];
        ::rtl::OUString sEnumValue0( "COLUMNS" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "ROWS" );
        enumValueNames[1] = sEnumValue1.pData;

        sal_Int32 enumValues[2];
        enumValues[0] = 0;
        enumValues[1] = 1;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::table::TableOrientation_COLUMNS,
            2, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::TableOrientation const *) {
    return *detail::theTableOrientationType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::TableOrientation const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::TableOrientation >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_TABLEORIENTATION_HPP
