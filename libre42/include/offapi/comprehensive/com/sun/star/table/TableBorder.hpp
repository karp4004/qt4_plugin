#ifndef INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDER_HPP
#define INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDER_HPP

#include "sal/config.h"

#include "com/sun/star/table/TableBorder.hdl"

#include "com/sun/star/table/BorderLine.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace table {

inline TableBorder::TableBorder() SAL_THROW(())
    : TopLine()
    , IsTopLineValid(false)
    , BottomLine()
    , IsBottomLineValid(false)
    , LeftLine()
    , IsLeftLineValid(false)
    , RightLine()
    , IsRightLineValid(false)
    , HorizontalLine()
    , IsHorizontalLineValid(false)
    , VerticalLine()
    , IsVerticalLineValid(false)
    , Distance(0)
    , IsDistanceValid(false)
{
}

inline TableBorder::TableBorder(const ::css::table::BorderLine& TopLine_, const ::sal_Bool& IsTopLineValid_, const ::css::table::BorderLine& BottomLine_, const ::sal_Bool& IsBottomLineValid_, const ::css::table::BorderLine& LeftLine_, const ::sal_Bool& IsLeftLineValid_, const ::css::table::BorderLine& RightLine_, const ::sal_Bool& IsRightLineValid_, const ::css::table::BorderLine& HorizontalLine_, const ::sal_Bool& IsHorizontalLineValid_, const ::css::table::BorderLine& VerticalLine_, const ::sal_Bool& IsVerticalLineValid_, const ::sal_Int16& Distance_, const ::sal_Bool& IsDistanceValid_) SAL_THROW(())
    : TopLine(TopLine_)
    , IsTopLineValid(IsTopLineValid_)
    , BottomLine(BottomLine_)
    , IsBottomLineValid(IsBottomLineValid_)
    , LeftLine(LeftLine_)
    , IsLeftLineValid(IsLeftLineValid_)
    , RightLine(RightLine_)
    , IsRightLineValid(IsRightLineValid_)
    , HorizontalLine(HorizontalLine_)
    , IsHorizontalLineValid(IsHorizontalLineValid_)
    , VerticalLine(VerticalLine_)
    , IsVerticalLineValid(IsVerticalLineValid_)
    , Distance(Distance_)
    , IsDistanceValid(IsDistanceValid_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace table { namespace detail {

struct theTableBorderType : public rtl::StaticWithInit< ::css::uno::Type *, theTableBorderType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.table.TableBorder" );
        ::cppu::UnoType< ::css::table::BorderLine >::get();
        ::rtl::OUString the_tname0( "com.sun.star.table.BorderLine" );
        ::rtl::OUString the_name0( "TopLine" );
        ::rtl::OUString the_tname1( "boolean" );
        ::rtl::OUString the_name1( "IsTopLineValid" );
        ::rtl::OUString the_name2( "BottomLine" );
        ::rtl::OUString the_name3( "IsBottomLineValid" );
        ::rtl::OUString the_name4( "LeftLine" );
        ::rtl::OUString the_name5( "IsLeftLineValid" );
        ::rtl::OUString the_name6( "RightLine" );
        ::rtl::OUString the_name7( "IsRightLineValid" );
        ::rtl::OUString the_name8( "HorizontalLine" );
        ::rtl::OUString the_name9( "IsHorizontalLineValid" );
        ::rtl::OUString the_name10( "VerticalLine" );
        ::rtl::OUString the_name11( "IsVerticalLineValid" );
        ::rtl::OUString the_tname2( "short" );
        ::rtl::OUString the_name12( "Distance" );
        ::rtl::OUString the_name13( "IsDistanceValid" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name6.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name7.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name8.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name9.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname0.pData, the_name10.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name11.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name12.pData }, false },
            { { typelib_TypeClass_BOOLEAN, the_tname1.pData, the_name13.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 14, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace table {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::table::TableBorder const *) {
    return *detail::theTableBorderType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::table::TableBorder const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::table::TableBorder >::get();
}

#endif // INCLUDED_COM_SUN_STAR_TABLE_TABLEBORDER_HPP
