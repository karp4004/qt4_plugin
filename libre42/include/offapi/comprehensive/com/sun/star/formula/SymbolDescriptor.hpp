#ifndef INCLUDED_COM_SUN_STAR_FORMULA_SYMBOLDESCRIPTOR_HPP
#define INCLUDED_COM_SUN_STAR_FORMULA_SYMBOLDESCRIPTOR_HPP

#include "sal/config.h"

#include "com/sun/star/formula/SymbolDescriptor.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace formula {

inline SymbolDescriptor::SymbolDescriptor() SAL_THROW(())
    : sName()
    , sExportName()
    , sSymbolSet()
    , nCharacter(0)
    , sFontName()
    , nCharSet(0)
    , nFamily(0)
    , nPitch(0)
    , nWeight(0)
    , nItalic(0)
{
}

inline SymbolDescriptor::SymbolDescriptor(const ::rtl::OUString& sName_, const ::rtl::OUString& sExportName_, const ::rtl::OUString& sSymbolSet_, const ::sal_Int32& nCharacter_, const ::rtl::OUString& sFontName_, const ::sal_Int16& nCharSet_, const ::sal_Int16& nFamily_, const ::sal_Int16& nPitch_, const ::sal_Int16& nWeight_, const ::sal_Int16& nItalic_) SAL_THROW(())
    : sName(sName_)
    , sExportName(sExportName_)
    , sSymbolSet(sSymbolSet_)
    , nCharacter(nCharacter_)
    , sFontName(sFontName_)
    , nCharSet(nCharSet_)
    , nFamily(nFamily_)
    , nPitch(nPitch_)
    , nWeight(nWeight_)
    , nItalic(nItalic_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace formula { namespace detail {

struct theSymbolDescriptorType : public rtl::StaticWithInit< ::css::uno::Type *, theSymbolDescriptorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.formula.SymbolDescriptor" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "sName" );
        ::rtl::OUString the_name1( "sExportName" );
        ::rtl::OUString the_name2( "sSymbolSet" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name3( "nCharacter" );
        ::rtl::OUString the_name4( "sFontName" );
        ::rtl::OUString the_tname2( "short" );
        ::rtl::OUString the_name5( "nCharSet" );
        ::rtl::OUString the_name6( "nFamily" );
        ::rtl::OUString the_name7( "nPitch" );
        ::rtl::OUString the_name8( "nWeight" );
        ::rtl::OUString the_name9( "nItalic" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name4.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name5.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name6.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name7.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name8.pData }, false },
            { { typelib_TypeClass_SHORT, the_tname2.pData, the_name9.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 10, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace formula {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::formula::SymbolDescriptor const *) {
    return *detail::theSymbolDescriptorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::formula::SymbolDescriptor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::formula::SymbolDescriptor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_FORMULA_SYMBOLDESCRIPTOR_HPP
