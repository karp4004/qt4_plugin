#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/dnd/DropTargetEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline DropTargetEvent::DropTargetEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Dummy(0)
{
}

inline DropTargetEvent::DropTargetEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int8& Dummy_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Dummy(Dummy_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd { namespace detail {

struct theDropTargetEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDropTargetEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.datatransfer.dnd.DropTargetEvent" );
        ::rtl::OUString the_tname0( "byte" );
        ::rtl::OUString the_name0( "Dummy" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DropTargetEvent const *) {
    return *detail::theDropTargetEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DropTargetEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::dnd::DropTargetEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETEVENT_HPP
