#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGGESTUREEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGGESTUREEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/dnd/DragGestureEvent.hdl"

#include "com/sun/star/datatransfer/dnd/XDragSource.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline DragGestureEvent::DragGestureEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , DragAction(0)
    , DragOriginX(0)
    , DragOriginY(0)
    , DragSource()
    , Event()
{
}

inline DragGestureEvent::DragGestureEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int8& DragAction_, const ::sal_Int32& DragOriginX_, const ::sal_Int32& DragOriginY_, const ::css::uno::Reference< ::css::datatransfer::dnd::XDragSource >& DragSource_, const ::css::uno::Any& Event_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , DragAction(DragAction_)
    , DragOriginX(DragOriginX_)
    , DragOriginY(DragOriginY_)
    , DragSource(DragSource_)
    , Event(Event_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd { namespace detail {

struct theDragGestureEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDragGestureEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.datatransfer.dnd.DragGestureEvent" );
        ::rtl::OUString the_tname0( "byte" );
        ::rtl::OUString the_name0( "DragAction" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "DragOriginX" );
        ::rtl::OUString the_name2( "DragOriginY" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::datatransfer::dnd::XDragSource > >::get();
        ::rtl::OUString the_tname2( "com.sun.star.datatransfer.dnd.XDragSource" );
        ::rtl::OUString the_name3( "DragSource" );
        ::rtl::OUString the_tname3( "any" );
        ::rtl::OUString the_name4( "Event" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_BYTE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name2.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_ANY, the_tname3.pData, the_name4.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 5, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DragGestureEvent const *) {
    return *detail::theDragGestureEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DragGestureEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::dnd::DragGestureEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGGESTUREEVENT_HPP
