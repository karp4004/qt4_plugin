#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGSOURCEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGSOURCEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/dnd/DragSourceEvent.hdl"

#include "com/sun/star/datatransfer/dnd/XDragSource.hpp"
#include "com/sun/star/datatransfer/dnd/XDragSourceContext.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline DragSourceEvent::DragSourceEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , DragSourceContext()
    , DragSource()
{
}

inline DragSourceEvent::DragSourceEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::datatransfer::dnd::XDragSourceContext >& DragSourceContext_, const ::css::uno::Reference< ::css::datatransfer::dnd::XDragSource >& DragSource_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , DragSourceContext(DragSourceContext_)
    , DragSource(DragSource_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd { namespace detail {

struct theDragSourceEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDragSourceEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.datatransfer.dnd.DragSourceEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::datatransfer::dnd::XDragSourceContext > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.datatransfer.dnd.XDragSourceContext" );
        ::rtl::OUString the_name0( "DragSourceContext" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::datatransfer::dnd::XDragSource > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.datatransfer.dnd.XDragSource" );
        ::rtl::OUString the_name1( "DragSource" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DragSourceEvent const *) {
    return *detail::theDragSourceEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DragSourceEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::dnd::DragSourceEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DRAGSOURCEEVENT_HPP
