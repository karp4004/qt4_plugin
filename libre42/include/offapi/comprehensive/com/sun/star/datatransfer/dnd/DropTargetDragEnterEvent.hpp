#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETDRAGENTEREVENT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETDRAGENTEREVENT_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/dnd/DropTargetDragEnterEvent.hdl"

#include "com/sun/star/datatransfer/DataFlavor.hpp"
#include "com/sun/star/datatransfer/dnd/DropTargetDragEvent.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline DropTargetDragEnterEvent::DropTargetDragEnterEvent() SAL_THROW(())
    : ::css::datatransfer::dnd::DropTargetDragEvent()
    , SupportedDataFlavors()
{
}

inline DropTargetDragEnterEvent::DropTargetDragEnterEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int8& Dummy_, const ::css::uno::Reference< ::css::datatransfer::dnd::XDropTargetDragContext >& Context_, const ::sal_Int8& DropAction_, const ::sal_Int32& LocationX_, const ::sal_Int32& LocationY_, const ::sal_Int8& SourceActions_, const ::css::uno::Sequence< ::css::datatransfer::DataFlavor >& SupportedDataFlavors_) SAL_THROW(())
    : ::css::datatransfer::dnd::DropTargetDragEvent(Source_, Dummy_, Context_, DropAction_, LocationX_, LocationY_, SourceActions_)
    , SupportedDataFlavors(SupportedDataFlavors_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd { namespace detail {

struct theDropTargetDragEnterEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDropTargetDragEnterEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.datatransfer.dnd.DropTargetDragEnterEvent" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::datatransfer::DataFlavor > >::get();
        ::rtl::OUString the_tname0( "[]com.sun.star.datatransfer.DataFlavor" );
        ::rtl::OUString the_name0( "SupportedDataFlavors" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SEQUENCE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::datatransfer::dnd::DropTargetDragEvent >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DropTargetDragEnterEvent const *) {
    return *detail::theDropTargetDragEnterEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DropTargetDragEnterEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::dnd::DropTargetDragEnterEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETDRAGENTEREVENT_HPP
