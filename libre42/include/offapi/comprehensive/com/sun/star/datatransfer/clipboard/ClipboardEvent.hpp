#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_CLIPBOARD_CLIPBOARDEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_CLIPBOARD_CLIPBOARDEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/clipboard/ClipboardEvent.hdl"

#include "com/sun/star/datatransfer/XTransferable.hpp"
#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace clipboard {

inline ClipboardEvent::ClipboardEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Contents()
{
}

inline ClipboardEvent::ClipboardEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::css::uno::Reference< ::css::datatransfer::XTransferable >& Contents_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Contents(Contents_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace clipboard { namespace detail {

struct theClipboardEventType : public rtl::StaticWithInit< ::css::uno::Type *, theClipboardEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.datatransfer.clipboard.ClipboardEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::datatransfer::XTransferable > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.datatransfer.XTransferable" );
        ::rtl::OUString the_name0( "Contents" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace clipboard {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::clipboard::ClipboardEvent const *) {
    return *detail::theClipboardEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::clipboard::ClipboardEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::clipboard::ClipboardEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_CLIPBOARD_CLIPBOARDEVENT_HPP
