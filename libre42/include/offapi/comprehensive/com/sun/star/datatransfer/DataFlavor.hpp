#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DATAFLAVOR_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DATAFLAVOR_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/DataFlavor.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer {

inline DataFlavor::DataFlavor() SAL_THROW(())
    : MimeType()
    , HumanPresentableName()
    , DataType()
{
}

inline DataFlavor::DataFlavor(const ::rtl::OUString& MimeType_, const ::rtl::OUString& HumanPresentableName_, const ::css::uno::Type& DataType_) SAL_THROW(())
    : MimeType(MimeType_)
    , HumanPresentableName(HumanPresentableName_)
    , DataType(DataType_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace detail {

struct theDataFlavorType : public rtl::StaticWithInit< ::css::uno::Type *, theDataFlavorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.datatransfer.DataFlavor" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "MimeType" );
        ::rtl::OUString the_name1( "HumanPresentableName" );
        ::rtl::OUString the_tname1( "type" );
        ::rtl::OUString the_name2( "DataType" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_TYPE, the_tname1.pData, the_name2.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 3, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::DataFlavor const *) {
    return *detail::theDataFlavorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::DataFlavor const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::DataFlavor >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DATAFLAVOR_HPP
