#ifndef INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETDROPEVENT_HPP
#define INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETDROPEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/datatransfer/dnd/DropTargetDropEvent.hdl"

#include "com/sun/star/datatransfer/XTransferable.hpp"
#include "com/sun/star/datatransfer/dnd/DropTargetEvent.hpp"
#include "com/sun/star/datatransfer/dnd/XDropTargetDropContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline DropTargetDropEvent::DropTargetDropEvent() SAL_THROW(())
    : ::css::datatransfer::dnd::DropTargetEvent()
    , Context()
    , DropAction(0)
    , LocationX(0)
    , LocationY(0)
    , SourceActions(0)
    , Transferable()
{
}

inline DropTargetDropEvent::DropTargetDropEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int8& Dummy_, const ::css::uno::Reference< ::css::datatransfer::dnd::XDropTargetDropContext >& Context_, const ::sal_Int8& DropAction_, const ::sal_Int32& LocationX_, const ::sal_Int32& LocationY_, const ::sal_Int8& SourceActions_, const ::css::uno::Reference< ::css::datatransfer::XTransferable >& Transferable_) SAL_THROW(())
    : ::css::datatransfer::dnd::DropTargetEvent(Source_, Dummy_)
    , Context(Context_)
    , DropAction(DropAction_)
    , LocationX(LocationX_)
    , LocationY(LocationY_)
    , SourceActions(SourceActions_)
    , Transferable(Transferable_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd { namespace detail {

struct theDropTargetDropEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDropTargetDropEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.datatransfer.dnd.DropTargetDropEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::datatransfer::dnd::XDropTargetDropContext > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.datatransfer.dnd.XDropTargetDropContext" );
        ::rtl::OUString the_name0( "Context" );
        ::rtl::OUString the_tname1( "byte" );
        ::rtl::OUString the_name1( "DropAction" );
        ::rtl::OUString the_tname2( "long" );
        ::rtl::OUString the_name2( "LocationX" );
        ::rtl::OUString the_name3( "LocationY" );
        ::rtl::OUString the_name4( "SourceActions" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::datatransfer::XTransferable > >::get();
        ::rtl::OUString the_tname3( "com.sun.star.datatransfer.XTransferable" );
        ::rtl::OUString the_name5( "Transferable" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name2.pData }, false },
            { { typelib_TypeClass_LONG, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_BYTE, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname3.pData, the_name5.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::datatransfer::dnd::DropTargetEvent >::get().getTypeLibType(), 6, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace datatransfer { namespace dnd {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DropTargetDropEvent const *) {
    return *detail::theDropTargetDropEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::datatransfer::dnd::DropTargetDropEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::datatransfer::dnd::DropTargetDropEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_DATATRANSFER_DND_DROPTARGETDROPEVENT_HPP
