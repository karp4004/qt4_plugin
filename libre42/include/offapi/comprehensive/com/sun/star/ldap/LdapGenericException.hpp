#ifndef INCLUDED_COM_SUN_STAR_LDAP_LDAPGENERICEXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_LDAP_LDAPGENERICEXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/ldap/LdapGenericException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace ldap {

inline LdapGenericException::LdapGenericException() SAL_THROW(())
    : ::css::uno::Exception()
    , ErrorCode(0)
{
    ::cppu::UnoType< ::css::ldap::LdapGenericException >::get();
}

inline LdapGenericException::LdapGenericException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::sal_Int32& ErrorCode_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , ErrorCode(ErrorCode_)
{
    ::cppu::UnoType< ::css::ldap::LdapGenericException >::get();
}

LdapGenericException::LdapGenericException(LdapGenericException const & the_other): ::css::uno::Exception(the_other), ErrorCode(the_other.ErrorCode) {}

LdapGenericException::~LdapGenericException() {}

LdapGenericException & LdapGenericException::operator =(LdapGenericException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    ErrorCode = the_other.ErrorCode;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace ldap { namespace detail {

struct theLdapGenericExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theLdapGenericExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.ldap.LdapGenericException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "long" );
        ::rtl::OUString sMemberName0( "ErrorCode" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace ldap {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::ldap::LdapGenericException const *) {
    return *detail::theLdapGenericExceptionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::ldap::LdapGenericException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::ldap::LdapGenericException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LDAP_LDAPGENERICEXCEPTION_HPP
