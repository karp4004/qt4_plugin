#ifndef INCLUDED_COM_SUN_STAR_REPORT_XSECTION_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_XSECTION_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XSection.hdl"

#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/container/XChild.hpp"
#include "com/sun/star/container/XContainer.hpp"
#include "com/sun/star/container/XEnumerationAccess.hpp"
#include "com/sun/star/drawing/XShapes.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/report/XGroup.hpp"
#include "com/sun/star/report/XReportDefinition.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/util/Color.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace report { namespace detail {

struct theXSectionType : public rtl::StaticWithInit< ::css::uno::Type *, theXSectionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.report.XSection" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[6];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::container::XChild > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::container::XContainer > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::drawing::XShapes > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< ::css::uno::Reference< ::css::container::XEnumerationAccess > >::get().getTypeLibType();
        aSuperTypes[4] = ::cppu::UnoType< ::css::uno::Reference< ::css::beans::XPropertySet > >::get().getTypeLibType();
        aSuperTypes[5] = ::cppu::UnoType< ::css::uno::Reference< ::css::lang::XComponent > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[14] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.report.XSection::Visible" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.report.XSection::Name" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.report.XSection::Height" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.report.XSection::BackColor" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.report.XSection::BackTransparent" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.report.XSection::ConditionalPrintExpression" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.report.XSection::ForceNewPage" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.report.XSection::NewRowOrCol" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.report.XSection::KeepTogether" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.report.XSection::CanGrow" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.report.XSection::CanShrink" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.report.XSection::RepeatSection" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.report.XSection::Group" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );
        ::rtl::OUString sAttributeName13( "com.sun.star.report.XSection::ReportDefinition" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName13.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            6, aSuperTypes,
            14,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace report {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XSection const *) {
    const ::css::uno::Type &rRet = *detail::theXSectionType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::beans::UnknownPropertyException >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::report::XGroup > >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportDefinition > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "boolean" );
                ::rtl::OUString sAttributeName0( "com.sun.star.report.XSection::Visible" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    24, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "string" );
                ::rtl::OUString sAttributeName1( "com.sun.star.report.XSection::Name" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    25, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "unsigned long" );
                ::rtl::OUString sAttributeName2( "com.sun.star.report.XSection::Height" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    26, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_UNSIGNED_LONG, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "long" );
                ::rtl::OUString sAttributeName3( "com.sun.star.report.XSection::BackColor" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    27, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "boolean" );
                ::rtl::OUString sAttributeName4( "com.sun.star.report.XSection::BackTransparent" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    28, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "string" );
                ::rtl::OUString sAttributeName5( "com.sun.star.report.XSection::ConditionalPrintExpression" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    29, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType5.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "short" );
                ::rtl::OUString sAttributeName6( "com.sun.star.report.XSection::ForceNewPage" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    30, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType6.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "short" );
                ::rtl::OUString sAttributeName7( "com.sun.star.report.XSection::NewRowOrCol" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    31, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType7.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "boolean" );
                ::rtl::OUString sAttributeName8( "com.sun.star.report.XSection::KeepTogether" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    32, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType8.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "boolean" );
                ::rtl::OUString sAttributeName9( "com.sun.star.report.XSection::CanGrow" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    33, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType9.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "boolean" );
                ::rtl::OUString sAttributeName10( "com.sun.star.report.XSection::CanShrink" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    34, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType10.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "boolean" );
                ::rtl::OUString sAttributeName11( "com.sun.star.report.XSection::RepeatSection" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    35, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType11.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "com.sun.star.report.XGroup" );
                ::rtl::OUString sAttributeName12( "com.sun.star.report.XSection::Group" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    36, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType12.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType13( "com.sun.star.report.XReportDefinition" );
                ::rtl::OUString sAttributeName13( "com.sun.star.report.XSection::ReportDefinition" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    37, sAttributeName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType13.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::report::XSection > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::report::XSection > >::get();
}

::css::uno::Type const & ::css::report::XSection::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::report::XSection > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REPORT_XSECTION_HPP
