#ifndef INCLUDED_COM_SUN_STAR_REPORT_META_XFUNCTIONMANAGER_HDL
#define INCLUDED_COM_SUN_STAR_REPORT_META_XFUNCTIONMANAGER_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/NoSuchElementException.hdl"
#include "com/sun/star/container/XIndexAccess.hdl"
#include "com/sun/star/lang/IndexOutOfBoundsException.hdl"
#include "com/sun/star/lang/WrappedTargetException.hdl"
#include "com/sun/star/lang/XComponent.hdl"
namespace com { namespace sun { namespace star { namespace report { namespace meta { class XFunctionCategory; } } } } }
namespace com { namespace sun { namespace star { namespace report { namespace meta { class XFunctionDescription; } } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace report { namespace meta {

class SAL_NO_VTABLE XFunctionManager : public ::css::lang::XComponent, public ::css::container::XIndexAccess
{
public:

    // Methods
    virtual ::css::uno::Reference< ::css::report::meta::XFunctionCategory > SAL_CALL getCategory( ::sal_Int32 position ) /* throw (::css::lang::IndexOutOfBoundsException, ::css::lang::WrappedTargetException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::report::meta::XFunctionDescription > SAL_CALL getFunctionByName( const ::rtl::OUString& name ) /* throw (::css::container::NoSuchElementException, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFunctionManager() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::meta::XFunctionManager const *);
} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::report::meta::XFunctionManager > *) SAL_THROW(());

#endif
