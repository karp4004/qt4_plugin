#ifndef INCLUDED_COM_SUN_STAR_REPORT_XREPORTCONTROLMODEL_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_XREPORTCONTROLMODEL_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XReportControlModel.hdl"

#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/container/XContainer.hpp"
#include "com/sun/star/container/XIndexContainer.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/report/XFormatCondition.hpp"
#include "com/sun/star/report/XReportComponent.hpp"
#include "com/sun/star/report/XReportControlFormat.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace report { namespace detail {

struct theXReportControlModelType : public rtl::StaticWithInit< ::css::uno::Type *, theXReportControlModelType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.report.XReportControlModel" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[4];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportComponent > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportControlFormat > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::container::XContainer > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< ::css::uno::Reference< ::css::container::XIndexContainer > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[4] = { 0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.report.XReportControlModel::DataField" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.report.XReportControlModel::PrintWhenGroupChange" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.report.XReportControlModel::ConditionalPrintExpression" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sMethodName0( "com.sun.star.report.XReportControlModel::createFormatCondition" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            4, aSuperTypes,
            4,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace report {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XReportControlModel const *) {
    const ::css::uno::Type &rRet = *detail::theXReportControlModelType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::beans::UnknownPropertyException >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();
            ::cppu::UnoType< ::css::uno::Exception >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "string" );
                ::rtl::OUString sAttributeName0( "com.sun.star.report.XReportControlModel::DataField" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    102, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType0.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "boolean" );
                ::rtl::OUString sAttributeName1( "com.sun.star.report.XReportControlModel::PrintWhenGroupChange" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    103, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType1.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "string" );
                ::rtl::OUString sAttributeName2( "com.sun.star.report.XReportControlModel::ConditionalPrintExpression" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    104, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType2.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.uno.Exception" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData };
                ::rtl::OUString sReturnType0( "com.sun.star.report.XFormatCondition" );
                ::rtl::OUString sMethodName0( "com.sun.star.report.XReportControlModel::createFormatCondition" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    105, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sReturnType0.pData,
                    0, 0,
                    2, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::report::XReportControlModel > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportControlModel > >::get();
}

::css::uno::Type const & ::css::report::XReportControlModel::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::report::XReportControlModel > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REPORT_XREPORTCONTROLMODEL_HPP
