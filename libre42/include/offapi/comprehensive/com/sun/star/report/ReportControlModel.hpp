#ifndef INCLUDED_COM_SUN_STAR_REPORT_REPORTCONTROLMODEL_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_REPORTCONTROLMODEL_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/report/XReportControlModel.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace report {

class ReportControlModel {
public:
    static ::css::uno::Reference< ::css::report::XReportControlModel > create(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::report::XReportControlModel > the_instance;
        try {
            the_instance = ::css::uno::Reference< ::css::report::XReportControlModel >(the_context->getServiceManager()->createInstanceWithContext(::rtl::OUString( "com.sun.star.report.ReportControlModel" ), the_context), ::css::uno::UNO_QUERY);
        } catch (const ::css::uno::RuntimeException &) {
            throw;
        } catch (const ::css::uno::Exception & the_exception) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.report.ReportControlModel of type com.sun.star.report.XReportControlModel: " ) + the_exception.Message, the_context);
        }
        if (!the_instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply service com.sun.star.report.ReportControlModel of type com.sun.star.report.XReportControlModel" ), the_context);
        }
        return the_instance;
    }

private:
    ReportControlModel(); // not implemented
    ReportControlModel(ReportControlModel &); // not implemented
    ~ReportControlModel(); // not implemented
    void operator =(ReportControlModel); // not implemented
};

} } } }

#endif // INCLUDED_COM_SUN_STAR_REPORT_REPORTCONTROLMODEL_HPP
