#ifndef INCLUDED_COM_SUN_STAR_REPORT_XREPORTENGINE_HDL
#define INCLUDED_COM_SUN_STAR_REPORT_XREPORTENGINE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/XPropertySet.hdl"
namespace com { namespace sun { namespace star { namespace frame { class XFrame; } } } }
namespace com { namespace sun { namespace star { namespace frame { class XModel; } } } }
#include "com/sun/star/lang/DisposedException.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/XComponent.hdl"
namespace com { namespace sun { namespace star { namespace report { class XReportDefinition; } } } }
namespace com { namespace sun { namespace star { namespace sdbc { class XConnection; } } } }
namespace com { namespace sun { namespace star { namespace task { class XStatusIndicator; } } } }
#include "com/sun/star/uno/Exception.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/util/URL.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace report {

class SAL_NO_VTABLE XReportEngine : public ::css::lang::XComponent, public ::css::beans::XPropertySet
{
public:

    // Attributes
    virtual ::css::uno::Reference< ::css::report::XReportDefinition > SAL_CALL getReportDefinition() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setReportDefinition( const ::css::uno::Reference< ::css::report::XReportDefinition >& _reportdefinition ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::sdbc::XConnection > SAL_CALL getActiveConnection() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setActiveConnection( const ::css::uno::Reference< ::css::sdbc::XConnection >& _activeconnection ) /* throw (::css::lang::IllegalArgumentException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::task::XStatusIndicator > SAL_CALL getStatusIndicator() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setStatusIndicator( const ::css::uno::Reference< ::css::task::XStatusIndicator >& _statusindicator ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getMaxRows() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMaxRows( ::sal_Int32 _maxrows ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    // Methods
    virtual ::css::uno::Reference< ::css::frame::XModel > SAL_CALL createDocumentModel() /* throw (::css::lang::DisposedException, ::css::lang::IllegalArgumentException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::frame::XModel > SAL_CALL createDocumentAlive( const ::css::uno::Reference< ::css::frame::XFrame >& frame ) /* throw (::css::lang::DisposedException, ::css::lang::IllegalArgumentException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::util::URL SAL_CALL createDocument() /* throw (::css::lang::DisposedException, ::css::lang::IllegalArgumentException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL interrupt() /* throw (::css::lang::DisposedException, ::css::uno::Exception, ::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XReportEngine() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XReportEngine const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::report::XReportEngine > *) SAL_THROW(());

#endif
