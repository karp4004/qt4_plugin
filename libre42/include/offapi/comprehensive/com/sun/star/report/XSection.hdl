#ifndef INCLUDED_COM_SUN_STAR_REPORT_XSECTION_HDL
#define INCLUDED_COM_SUN_STAR_REPORT_XSECTION_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/UnknownPropertyException.hdl"
#include "com/sun/star/beans/XPropertySet.hdl"
#include "com/sun/star/container/XChild.hdl"
#include "com/sun/star/container/XContainer.hdl"
#include "com/sun/star/container/XEnumerationAccess.hdl"
#include "com/sun/star/drawing/XShapes.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/XComponent.hdl"
namespace com { namespace sun { namespace star { namespace report { class XGroup; } } } }
namespace com { namespace sun { namespace star { namespace report { class XReportDefinition; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/util/Color.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace report {

class SAL_NO_VTABLE XSection : public ::css::container::XChild, public ::css::container::XContainer, public ::css::drawing::XShapes, public ::css::container::XEnumerationAccess, public ::css::beans::XPropertySet, public ::css::lang::XComponent
{
public:

    // Attributes
    virtual ::sal_Bool SAL_CALL getVisible() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setVisible( ::sal_Bool _visible ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getName() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setName( const ::rtl::OUString& _name ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_uInt32 SAL_CALL getHeight() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setHeight( ::sal_uInt32 _height ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getBackColor() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setBackColor( ::sal_Int32 _backcolor ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getBackTransparent() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setBackTransparent( ::sal_Bool _backtransparent ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getConditionalPrintExpression() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setConditionalPrintExpression( const ::rtl::OUString& _conditionalprintexpression ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getForceNewPage() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setForceNewPage( ::sal_Int16 _forcenewpage ) /* throw (::css::lang::IllegalArgumentException, ::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getNewRowOrCol() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setNewRowOrCol( ::sal_Int16 _newroworcol ) /* throw (::css::lang::IllegalArgumentException, ::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getKeepTogether() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setKeepTogether( ::sal_Bool _keeptogether ) /* throw (::css::lang::IllegalArgumentException, ::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getCanGrow() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setCanGrow( ::sal_Bool _cangrow ) /* throw (::css::lang::IllegalArgumentException, ::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getCanShrink() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setCanShrink( ::sal_Bool _canshrink ) /* throw (::css::lang::IllegalArgumentException, ::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getRepeatSection() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setRepeatSection( ::sal_Bool _repeatsection ) /* throw (::css::lang::IllegalArgumentException, ::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::report::XGroup > SAL_CALL getGroup() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::report::XReportDefinition > SAL_CALL getReportDefinition() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XSection() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XSection const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::report::XSection > *) SAL_THROW(());

#endif
