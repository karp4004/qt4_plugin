#ifndef INCLUDED_COM_SUN_STAR_REPORT_XREPORTDEFINITION_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_XREPORTDEFINITION_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XReportDefinition.hdl"

#include "com/sun/star/container/NoSuchElementException.hpp"
#include "com/sun/star/document/XDocumentSubStorageSupplier.hpp"
#include "com/sun/star/document/XEventBroadcaster.hpp"
#include "com/sun/star/document/XStorageBasedDocument.hpp"
#include "com/sun/star/document/XViewDataSupplier.hpp"
#include "com/sun/star/embed/XVisualObject.hpp"
#include "com/sun/star/frame/XLoadable.hpp"
#include "com/sun/star/frame/XModel.hpp"
#include "com/sun/star/lang/DisposedException.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/report/XFunctionsSupplier.hpp"
#include "com/sun/star/report/XGroups.hpp"
#include "com/sun/star/report/XReportComponent.hpp"
#include "com/sun/star/report/XSection.hpp"
#include "com/sun/star/sdbc/XConnection.hpp"
#include "com/sun/star/style/XStyleFamiliesSupplier.hpp"
#include "com/sun/star/ui/XUIConfigurationManagerSupplier.hpp"
#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/util/XCloseable.hpp"
#include "com/sun/star/util/XModifiable2.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace report { namespace detail {

struct theXReportDefinitionType : public rtl::StaticWithInit< ::css::uno::Type *, theXReportDefinitionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.report.XReportDefinition" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[12];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XModel > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::frame::XLoadable > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::embed::XVisualObject > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< ::css::uno::Reference< ::css::document::XStorageBasedDocument > >::get().getTypeLibType();
        aSuperTypes[4] = ::cppu::UnoType< ::css::uno::Reference< ::css::document::XViewDataSupplier > >::get().getTypeLibType();
        aSuperTypes[5] = ::cppu::UnoType< ::css::uno::Reference< ::css::util::XCloseable > >::get().getTypeLibType();
        aSuperTypes[6] = ::cppu::UnoType< ::css::uno::Reference< ::css::ui::XUIConfigurationManagerSupplier > >::get().getTypeLibType();
        aSuperTypes[7] = ::cppu::UnoType< ::css::uno::Reference< ::css::document::XDocumentSubStorageSupplier > >::get().getTypeLibType();
        aSuperTypes[8] = ::cppu::UnoType< ::css::uno::Reference< ::css::style::XStyleFamiliesSupplier > >::get().getTypeLibType();
        aSuperTypes[9] = ::cppu::UnoType< ::css::uno::Reference< ::css::util::XModifiable2 > >::get().getTypeLibType();
        aSuperTypes[10] = ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportComponent > >::get().getTypeLibType();
        aSuperTypes[11] = ::cppu::UnoType< ::css::uno::Reference< ::css::report::XFunctionsSupplier > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[23] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.report.XReportDefinition::MimeType" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.report.XReportDefinition::Caption" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.report.XReportDefinition::GroupKeepTogether" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.report.XReportDefinition::PageHeaderOption" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.report.XReportDefinition::PageFooterOption" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.report.XReportDefinition::Command" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.report.XReportDefinition::CommandType" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.report.XReportDefinition::Filter" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.report.XReportDefinition::EscapeProcessing" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.report.XReportDefinition::ActiveConnection" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.report.XReportDefinition::DataSourceName" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.report.XReportDefinition::ReportHeaderOn" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.report.XReportDefinition::ReportFooterOn" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );
        ::rtl::OUString sAttributeName13( "com.sun.star.report.XReportDefinition::PageHeaderOn" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName13.pData );
        ::rtl::OUString sAttributeName14( "com.sun.star.report.XReportDefinition::PageFooterOn" );
        typelib_typedescriptionreference_new( &pMembers[14],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName14.pData );
        ::rtl::OUString sAttributeName15( "com.sun.star.report.XReportDefinition::Groups" );
        typelib_typedescriptionreference_new( &pMembers[15],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName15.pData );
        ::rtl::OUString sAttributeName16( "com.sun.star.report.XReportDefinition::ReportHeader" );
        typelib_typedescriptionreference_new( &pMembers[16],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName16.pData );
        ::rtl::OUString sAttributeName17( "com.sun.star.report.XReportDefinition::PageHeader" );
        typelib_typedescriptionreference_new( &pMembers[17],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName17.pData );
        ::rtl::OUString sAttributeName18( "com.sun.star.report.XReportDefinition::Detail" );
        typelib_typedescriptionreference_new( &pMembers[18],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName18.pData );
        ::rtl::OUString sAttributeName19( "com.sun.star.report.XReportDefinition::PageFooter" );
        typelib_typedescriptionreference_new( &pMembers[19],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName19.pData );
        ::rtl::OUString sAttributeName20( "com.sun.star.report.XReportDefinition::ReportFooter" );
        typelib_typedescriptionreference_new( &pMembers[20],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName20.pData );
        ::rtl::OUString sMethodName0( "com.sun.star.report.XReportDefinition::getEventBroadcaster" );
        typelib_typedescriptionreference_new( &pMembers[21],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName0.pData );
        ::rtl::OUString sMethodName1( "com.sun.star.report.XReportDefinition::getAvailableMimeTypes" );
        typelib_typedescriptionreference_new( &pMembers[22],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_METHOD,
                                              sMethodName1.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            12, aSuperTypes,
            23,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescriptionreference_release( pMembers[14] );
        typelib_typedescriptionreference_release( pMembers[15] );
        typelib_typedescriptionreference_release( pMembers[16] );
        typelib_typedescriptionreference_release( pMembers[17] );
        typelib_typedescriptionreference_release( pMembers[18] );
        typelib_typedescriptionreference_release( pMembers[19] );
        typelib_typedescriptionreference_release( pMembers[20] );
        typelib_typedescriptionreference_release( pMembers[21] );
        typelib_typedescriptionreference_release( pMembers[22] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace report {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XReportDefinition const *) {
    const ::css::uno::Type &rRet = *detail::theXReportDefinitionType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::sdbc::XConnection > >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::report::XGroups > >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::report::XSection > >::get();
            ::cppu::UnoType< ::css::container::NoSuchElementException >::get();
            ::cppu::UnoType< ::css::lang::DisposedException >::get();
            ::cppu::UnoType< ::css::uno::Exception >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "string" );
                ::rtl::OUString sAttributeName0( "com.sun.star.report.XReportDefinition::MimeType" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    72, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType0.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "string" );
                ::rtl::OUString sAttributeName1( "com.sun.star.report.XReportDefinition::Caption" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    73, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "short" );
                ::rtl::OUString sAttributeName2( "com.sun.star.report.XReportDefinition::GroupKeepTogether" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    74, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType2.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "short" );
                ::rtl::OUString sAttributeName3( "com.sun.star.report.XReportDefinition::PageHeaderOption" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    75, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "short" );
                ::rtl::OUString sAttributeName4( "com.sun.star.report.XReportDefinition::PageFooterOption" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    76, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "string" );
                ::rtl::OUString sAttributeName5( "com.sun.star.report.XReportDefinition::Command" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    77, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType5.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "long" );
                ::rtl::OUString sAttributeName6( "com.sun.star.report.XReportDefinition::CommandType" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    78, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType6.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "string" );
                ::rtl::OUString sAttributeName7( "com.sun.star.report.XReportDefinition::Filter" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    79, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType7.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "boolean" );
                ::rtl::OUString sAttributeName8( "com.sun.star.report.XReportDefinition::EscapeProcessing" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    80, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType8.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "com.sun.star.sdbc.XConnection" );
                ::rtl::OUString sAttributeName9( "com.sun.star.report.XReportDefinition::ActiveConnection" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    81, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType9.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "string" );
                ::rtl::OUString sAttributeName10( "com.sun.star.report.XReportDefinition::DataSourceName" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    82, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType10.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "boolean" );
                ::rtl::OUString sAttributeName11( "com.sun.star.report.XReportDefinition::ReportHeaderOn" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    83, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType11.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "boolean" );
                ::rtl::OUString sAttributeName12( "com.sun.star.report.XReportDefinition::ReportFooterOn" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    84, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType12.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType13( "boolean" );
                ::rtl::OUString sAttributeName13( "com.sun.star.report.XReportDefinition::PageHeaderOn" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    85, sAttributeName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType13.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType14( "boolean" );
                ::rtl::OUString sAttributeName14( "com.sun.star.report.XReportDefinition::PageFooterOn" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    86, sAttributeName14.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType14.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType15( "com.sun.star.report.XGroups" );
                ::rtl::OUString sAttributeName15( "com.sun.star.report.XReportDefinition::Groups" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    87, sAttributeName15.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType15.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType16( "com.sun.star.report.XSection" );
                ::rtl::OUString sAttributeName16( "com.sun.star.report.XReportDefinition::ReportHeader" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.container.NoSuchElementException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    88, sAttributeName16.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType16.pData,
                    sal_True, 1, the_getExceptions, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType17( "com.sun.star.report.XSection" );
                ::rtl::OUString sAttributeName17( "com.sun.star.report.XReportDefinition::PageHeader" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.container.NoSuchElementException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    89, sAttributeName17.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType17.pData,
                    sal_True, 1, the_getExceptions, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType18( "com.sun.star.report.XSection" );
                ::rtl::OUString sAttributeName18( "com.sun.star.report.XReportDefinition::Detail" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    90, sAttributeName18.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType18.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType19( "com.sun.star.report.XSection" );
                ::rtl::OUString sAttributeName19( "com.sun.star.report.XReportDefinition::PageFooter" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.container.NoSuchElementException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    91, sAttributeName19.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType19.pData,
                    sal_True, 1, the_getExceptions, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType20( "com.sun.star.report.XSection" );
                ::rtl::OUString sAttributeName20( "com.sun.star.report.XReportDefinition::ReportFooter" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.container.NoSuchElementException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    92, sAttributeName20.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType20.pData,
                    sal_True, 1, the_getExceptions, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );

            typelib_InterfaceMethodTypeDescription * pMethod = 0;
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.lang.DisposedException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.uno.Exception" );
                ::rtl::OUString the_ExceptionName2( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData, the_ExceptionName2.pData };
                ::rtl::OUString sReturnType0( "com.sun.star.document.XEventBroadcaster" );
                ::rtl::OUString sMethodName0( "com.sun.star.report.XReportDefinition::getEventBroadcaster" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    93, sal_False,
                    sMethodName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sReturnType0.pData,
                    0, 0,
                    3, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            {
                ::rtl::OUString the_ExceptionName0( "com.sun.star.lang.DisposedException" );
                ::rtl::OUString the_ExceptionName1( "com.sun.star.uno.Exception" );
                ::rtl::OUString the_ExceptionName2( "com.sun.star.uno.RuntimeException" );
                rtl_uString * the_Exceptions[] = { the_ExceptionName0.pData, the_ExceptionName1.pData, the_ExceptionName2.pData };
                ::rtl::OUString sReturnType1( "[]string" );
                ::rtl::OUString sMethodName1( "com.sun.star.report.XReportDefinition::getAvailableMimeTypes" );
                typelib_typedescription_newInterfaceMethod( &pMethod,
                    94, sal_False,
                    sMethodName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sReturnType1.pData,
                    0, 0,
                    3, the_Exceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pMethod );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pMethod );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::report::XReportDefinition > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportDefinition > >::get();
}

::css::uno::Type const & ::css::report::XReportDefinition::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::report::XReportDefinition > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REPORT_XREPORTDEFINITION_HPP
