#ifndef INCLUDED_COM_SUN_STAR_REPORT_XREPORTCOMPONENT_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_XREPORTCOMPONENT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XReportComponent.hdl"

#include "com/sun/star/beans/PropertyVetoException.hpp"
#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/container/XChild.hpp"
#include "com/sun/star/drawing/XShape.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/XComponent.hpp"
#include "com/sun/star/report/XSection.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/util/XCloneable.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace report { namespace detail {

struct theXReportComponentType : public rtl::StaticWithInit< ::css::uno::Type *, theXReportComponentType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.report.XReportComponent" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[5];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::util::XCloneable > >::get().getTypeLibType();
        aSuperTypes[1] = ::cppu::UnoType< ::css::uno::Reference< ::css::container::XChild > >::get().getTypeLibType();
        aSuperTypes[2] = ::cppu::UnoType< ::css::uno::Reference< ::css::lang::XComponent > >::get().getTypeLibType();
        aSuperTypes[3] = ::cppu::UnoType< ::css::uno::Reference< ::css::drawing::XShape > >::get().getTypeLibType();
        aSuperTypes[4] = ::cppu::UnoType< ::css::uno::Reference< ::css::beans::XPropertySet > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[11] = { 0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.report.XReportComponent::Name" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.report.XReportComponent::Height" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.report.XReportComponent::PositionX" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.report.XReportComponent::PositionY" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.report.XReportComponent::Width" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.report.XReportComponent::ControlBorder" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.report.XReportComponent::ControlBorderColor" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.report.XReportComponent::PrintRepeatedValues" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.report.XReportComponent::MasterFields" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.report.XReportComponent::DetailFields" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.report.XReportComponent::Section" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            5, aSuperTypes,
            11,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace report {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XReportComponent const *) {
    const ::css::uno::Type &rRet = *detail::theXReportComponentType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::beans::PropertyVetoException >::get();
            ::cppu::UnoType< ::css::beans::UnknownPropertyException >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::report::XSection > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "string" );
                ::rtl::OUString sAttributeName0( "com.sun.star.report.XReportComponent::Name" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.PropertyVetoException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    21, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType0.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "long" );
                ::rtl::OUString sAttributeName1( "com.sun.star.report.XReportComponent::Height" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.PropertyVetoException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    22, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType1.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "long" );
                ::rtl::OUString sAttributeName2( "com.sun.star.report.XReportComponent::PositionX" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    23, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "long" );
                ::rtl::OUString sAttributeName3( "com.sun.star.report.XReportComponent::PositionY" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    24, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "long" );
                ::rtl::OUString sAttributeName4( "com.sun.star.report.XReportComponent::Width" );
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.PropertyVetoException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    25, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType4.pData,
                    sal_False, 0, 0, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "short" );
                ::rtl::OUString sAttributeName5( "com.sun.star.report.XReportComponent::ControlBorder" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    26, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType5.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "long" );
                ::rtl::OUString sAttributeName6( "com.sun.star.report.XReportComponent::ControlBorderColor" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    27, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType6.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "boolean" );
                ::rtl::OUString sAttributeName7( "com.sun.star.report.XReportComponent::PrintRepeatedValues" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    28, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType7.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "[]string" );
                ::rtl::OUString sAttributeName8( "com.sun.star.report.XReportComponent::MasterFields" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    29, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType8.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "[]string" );
                ::rtl::OUString sAttributeName9( "com.sun.star.report.XReportComponent::DetailFields" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    30, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType9.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "com.sun.star.report.XSection" );
                ::rtl::OUString sAttributeName10( "com.sun.star.report.XReportComponent::Section" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    31, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType10.pData,
                    sal_True, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::report::XReportComponent > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportComponent > >::get();
}

::css::uno::Type const & ::css::report::XReportComponent::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::report::XReportComponent > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REPORT_XREPORTCOMPONENT_HPP
