#ifndef INCLUDED_COM_SUN_STAR_REPORT_XFORMATTEDFIELD_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_XFORMATTEDFIELD_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XFormattedField.hdl"

#include "com/sun/star/report/XReportControlModel.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/util/XNumberFormatsSupplier.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace report { namespace detail {

struct theXFormattedFieldType : public rtl::StaticWithInit< ::css::uno::Type *, theXFormattedFieldType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.report.XFormattedField" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportControlModel > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[2] = { 0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.report.XFormattedField::FormatKey" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.report.XFormattedField::FormatsSupplier" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            2,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace report {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XFormattedField const *) {
    const ::css::uno::Type &rRet = *detail::theXFormattedFieldType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::uno::Reference< ::css::util::XNumberFormatsSupplier > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "long" );
                ::rtl::OUString sAttributeName0( "com.sun.star.report.XFormattedField::FormatKey" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    106, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "com.sun.star.util.XNumberFormatsSupplier" );
                ::rtl::OUString sAttributeName1( "com.sun.star.report.XFormattedField::FormatsSupplier" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    107, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_INTERFACE, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::report::XFormattedField > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::report::XFormattedField > >::get();
}

::css::uno::Type const & ::css::report::XFormattedField::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::report::XFormattedField > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REPORT_XFORMATTEDFIELD_HPP
