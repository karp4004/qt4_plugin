#ifndef INCLUDED_COM_SUN_STAR_REPORT_XFIXEDLINE_HDL
#define INCLUDED_COM_SUN_STAR_REPORT_XFIXEDLINE_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/UnknownPropertyException.hdl"
#include "com/sun/star/drawing/LineDash.hdl"
#include "com/sun/star/drawing/LineStyle.hdl"
#include "com/sun/star/report/XReportControlModel.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/util/Color.hdl"
#include "com/sun/star/uno/Reference.h"
#include "cppu/macros.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace report {

class SAL_NO_VTABLE XFixedLine : public ::css::report::XReportControlModel
{
public:

    // Attributes
    virtual ::sal_Int32 SAL_CALL getOrientation() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setOrientation( ::sal_Int32 _orientation ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::drawing::LineStyle SAL_CALL getLineStyle() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setLineStyle( ::css::drawing::LineStyle _linestyle ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::drawing::LineDash SAL_CALL getLineDash() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setLineDash( const ::css::drawing::LineDash& _linedash ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getLineColor() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setLineColor( ::sal_Int32 _linecolor ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getLineTransparence() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setLineTransparence( ::sal_Int16 _linetransparence ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getLineWidth() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setLineWidth( ::sal_Int32 _linewidth ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XFixedLine() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XFixedLine const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::report::XFixedLine > *) SAL_THROW(());

#endif
