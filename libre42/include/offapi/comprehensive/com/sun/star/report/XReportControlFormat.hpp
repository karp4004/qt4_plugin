#ifndef INCLUDED_COM_SUN_STAR_REPORT_XREPORTCONTROLFORMAT_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_XREPORTCONTROLFORMAT_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XReportControlFormat.hdl"

#include "com/sun/star/awt/FontDescriptor.hpp"
#include "com/sun/star/awt/FontSlant.hpp"
#include "com/sun/star/beans/UnknownPropertyException.hpp"
#include "com/sun/star/lang/IllegalArgumentException.hpp"
#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/style/VerticalAlignment.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/util/Color.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace report { namespace detail {

struct theXReportControlFormatType : public rtl::StaticWithInit< ::css::uno::Type *, theXReportControlFormatType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.report.XReportControlFormat" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[61] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.report.XReportControlFormat::ControlBackground" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.report.XReportControlFormat::ControlBackgroundTransparent" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.report.XReportControlFormat::ParaAdjust" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.report.XReportControlFormat::FontDescriptor" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.report.XReportControlFormat::FontDescriptorAsian" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.report.XReportControlFormat::FontDescriptorComplex" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.report.XReportControlFormat::ControlTextEmphasis" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.report.XReportControlFormat::CharEmphasis" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.report.XReportControlFormat::CharCombineIsOn" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.report.XReportControlFormat::CharCombinePrefix" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.report.XReportControlFormat::CharCombineSuffix" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.report.XReportControlFormat::CharHidden" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.report.XReportControlFormat::CharShadowed" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );
        ::rtl::OUString sAttributeName13( "com.sun.star.report.XReportControlFormat::CharContoured" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName13.pData );
        ::rtl::OUString sAttributeName14( "com.sun.star.report.XReportControlFormat::CharCaseMap" );
        typelib_typedescriptionreference_new( &pMembers[14],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName14.pData );
        ::rtl::OUString sAttributeName15( "com.sun.star.report.XReportControlFormat::CharLocale" );
        typelib_typedescriptionreference_new( &pMembers[15],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName15.pData );
        ::rtl::OUString sAttributeName16( "com.sun.star.report.XReportControlFormat::CharEscapement" );
        typelib_typedescriptionreference_new( &pMembers[16],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName16.pData );
        ::rtl::OUString sAttributeName17( "com.sun.star.report.XReportControlFormat::CharEscapementHeight" );
        typelib_typedescriptionreference_new( &pMembers[17],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName17.pData );
        ::rtl::OUString sAttributeName18( "com.sun.star.report.XReportControlFormat::CharAutoKerning" );
        typelib_typedescriptionreference_new( &pMembers[18],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName18.pData );
        ::rtl::OUString sAttributeName19( "com.sun.star.report.XReportControlFormat::CharKerning" );
        typelib_typedescriptionreference_new( &pMembers[19],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName19.pData );
        ::rtl::OUString sAttributeName20( "com.sun.star.report.XReportControlFormat::CharFlash" );
        typelib_typedescriptionreference_new( &pMembers[20],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName20.pData );
        ::rtl::OUString sAttributeName21( "com.sun.star.report.XReportControlFormat::CharRelief" );
        typelib_typedescriptionreference_new( &pMembers[21],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName21.pData );
        ::rtl::OUString sAttributeName22( "com.sun.star.report.XReportControlFormat::CharFontName" );
        typelib_typedescriptionreference_new( &pMembers[22],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName22.pData );
        ::rtl::OUString sAttributeName23( "com.sun.star.report.XReportControlFormat::CharFontStyleName" );
        typelib_typedescriptionreference_new( &pMembers[23],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName23.pData );
        ::rtl::OUString sAttributeName24( "com.sun.star.report.XReportControlFormat::CharFontFamily" );
        typelib_typedescriptionreference_new( &pMembers[24],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName24.pData );
        ::rtl::OUString sAttributeName25( "com.sun.star.report.XReportControlFormat::CharFontCharSet" );
        typelib_typedescriptionreference_new( &pMembers[25],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName25.pData );
        ::rtl::OUString sAttributeName26( "com.sun.star.report.XReportControlFormat::CharFontPitch" );
        typelib_typedescriptionreference_new( &pMembers[26],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName26.pData );
        ::rtl::OUString sAttributeName27( "com.sun.star.report.XReportControlFormat::CharColor" );
        typelib_typedescriptionreference_new( &pMembers[27],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName27.pData );
        ::rtl::OUString sAttributeName28( "com.sun.star.report.XReportControlFormat::CharUnderlineColor" );
        typelib_typedescriptionreference_new( &pMembers[28],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName28.pData );
        ::rtl::OUString sAttributeName29( "com.sun.star.report.XReportControlFormat::CharHeight" );
        typelib_typedescriptionreference_new( &pMembers[29],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName29.pData );
        ::rtl::OUString sAttributeName30( "com.sun.star.report.XReportControlFormat::CharUnderline" );
        typelib_typedescriptionreference_new( &pMembers[30],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName30.pData );
        ::rtl::OUString sAttributeName31( "com.sun.star.report.XReportControlFormat::CharWeight" );
        typelib_typedescriptionreference_new( &pMembers[31],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName31.pData );
        ::rtl::OUString sAttributeName32( "com.sun.star.report.XReportControlFormat::CharPosture" );
        typelib_typedescriptionreference_new( &pMembers[32],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName32.pData );
        ::rtl::OUString sAttributeName33( "com.sun.star.report.XReportControlFormat::CharStrikeout" );
        typelib_typedescriptionreference_new( &pMembers[33],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName33.pData );
        ::rtl::OUString sAttributeName34( "com.sun.star.report.XReportControlFormat::CharWordMode" );
        typelib_typedescriptionreference_new( &pMembers[34],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName34.pData );
        ::rtl::OUString sAttributeName35( "com.sun.star.report.XReportControlFormat::CharRotation" );
        typelib_typedescriptionreference_new( &pMembers[35],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName35.pData );
        ::rtl::OUString sAttributeName36( "com.sun.star.report.XReportControlFormat::CharScaleWidth" );
        typelib_typedescriptionreference_new( &pMembers[36],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName36.pData );
        ::rtl::OUString sAttributeName37( "com.sun.star.report.XReportControlFormat::VerticalAlign" );
        typelib_typedescriptionreference_new( &pMembers[37],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName37.pData );
        ::rtl::OUString sAttributeName38( "com.sun.star.report.XReportControlFormat::HyperLinkURL" );
        typelib_typedescriptionreference_new( &pMembers[38],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName38.pData );
        ::rtl::OUString sAttributeName39( "com.sun.star.report.XReportControlFormat::HyperLinkTarget" );
        typelib_typedescriptionreference_new( &pMembers[39],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName39.pData );
        ::rtl::OUString sAttributeName40( "com.sun.star.report.XReportControlFormat::HyperLinkName" );
        typelib_typedescriptionreference_new( &pMembers[40],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName40.pData );
        ::rtl::OUString sAttributeName41( "com.sun.star.report.XReportControlFormat::VisitedCharStyleName" );
        typelib_typedescriptionreference_new( &pMembers[41],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName41.pData );
        ::rtl::OUString sAttributeName42( "com.sun.star.report.XReportControlFormat::UnvisitedCharStyleName" );
        typelib_typedescriptionreference_new( &pMembers[42],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName42.pData );
        ::rtl::OUString sAttributeName43( "com.sun.star.report.XReportControlFormat::CharHeightAsian" );
        typelib_typedescriptionreference_new( &pMembers[43],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName43.pData );
        ::rtl::OUString sAttributeName44( "com.sun.star.report.XReportControlFormat::CharWeightAsian" );
        typelib_typedescriptionreference_new( &pMembers[44],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName44.pData );
        ::rtl::OUString sAttributeName45( "com.sun.star.report.XReportControlFormat::CharFontNameAsian" );
        typelib_typedescriptionreference_new( &pMembers[45],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName45.pData );
        ::rtl::OUString sAttributeName46( "com.sun.star.report.XReportControlFormat::CharFontStyleNameAsian" );
        typelib_typedescriptionreference_new( &pMembers[46],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName46.pData );
        ::rtl::OUString sAttributeName47( "com.sun.star.report.XReportControlFormat::CharFontFamilyAsian" );
        typelib_typedescriptionreference_new( &pMembers[47],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName47.pData );
        ::rtl::OUString sAttributeName48( "com.sun.star.report.XReportControlFormat::CharFontCharSetAsian" );
        typelib_typedescriptionreference_new( &pMembers[48],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName48.pData );
        ::rtl::OUString sAttributeName49( "com.sun.star.report.XReportControlFormat::CharFontPitchAsian" );
        typelib_typedescriptionreference_new( &pMembers[49],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName49.pData );
        ::rtl::OUString sAttributeName50( "com.sun.star.report.XReportControlFormat::CharPostureAsian" );
        typelib_typedescriptionreference_new( &pMembers[50],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName50.pData );
        ::rtl::OUString sAttributeName51( "com.sun.star.report.XReportControlFormat::CharLocaleAsian" );
        typelib_typedescriptionreference_new( &pMembers[51],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName51.pData );
        ::rtl::OUString sAttributeName52( "com.sun.star.report.XReportControlFormat::CharHeightComplex" );
        typelib_typedescriptionreference_new( &pMembers[52],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName52.pData );
        ::rtl::OUString sAttributeName53( "com.sun.star.report.XReportControlFormat::CharWeightComplex" );
        typelib_typedescriptionreference_new( &pMembers[53],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName53.pData );
        ::rtl::OUString sAttributeName54( "com.sun.star.report.XReportControlFormat::CharFontNameComplex" );
        typelib_typedescriptionreference_new( &pMembers[54],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName54.pData );
        ::rtl::OUString sAttributeName55( "com.sun.star.report.XReportControlFormat::CharFontStyleNameComplex" );
        typelib_typedescriptionreference_new( &pMembers[55],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName55.pData );
        ::rtl::OUString sAttributeName56( "com.sun.star.report.XReportControlFormat::CharFontFamilyComplex" );
        typelib_typedescriptionreference_new( &pMembers[56],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName56.pData );
        ::rtl::OUString sAttributeName57( "com.sun.star.report.XReportControlFormat::CharFontCharSetComplex" );
        typelib_typedescriptionreference_new( &pMembers[57],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName57.pData );
        ::rtl::OUString sAttributeName58( "com.sun.star.report.XReportControlFormat::CharFontPitchComplex" );
        typelib_typedescriptionreference_new( &pMembers[58],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName58.pData );
        ::rtl::OUString sAttributeName59( "com.sun.star.report.XReportControlFormat::CharPostureComplex" );
        typelib_typedescriptionreference_new( &pMembers[59],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName59.pData );
        ::rtl::OUString sAttributeName60( "com.sun.star.report.XReportControlFormat::CharLocaleComplex" );
        typelib_typedescriptionreference_new( &pMembers[60],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName60.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            61,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescriptionreference_release( pMembers[14] );
        typelib_typedescriptionreference_release( pMembers[15] );
        typelib_typedescriptionreference_release( pMembers[16] );
        typelib_typedescriptionreference_release( pMembers[17] );
        typelib_typedescriptionreference_release( pMembers[18] );
        typelib_typedescriptionreference_release( pMembers[19] );
        typelib_typedescriptionreference_release( pMembers[20] );
        typelib_typedescriptionreference_release( pMembers[21] );
        typelib_typedescriptionreference_release( pMembers[22] );
        typelib_typedescriptionreference_release( pMembers[23] );
        typelib_typedescriptionreference_release( pMembers[24] );
        typelib_typedescriptionreference_release( pMembers[25] );
        typelib_typedescriptionreference_release( pMembers[26] );
        typelib_typedescriptionreference_release( pMembers[27] );
        typelib_typedescriptionreference_release( pMembers[28] );
        typelib_typedescriptionreference_release( pMembers[29] );
        typelib_typedescriptionreference_release( pMembers[30] );
        typelib_typedescriptionreference_release( pMembers[31] );
        typelib_typedescriptionreference_release( pMembers[32] );
        typelib_typedescriptionreference_release( pMembers[33] );
        typelib_typedescriptionreference_release( pMembers[34] );
        typelib_typedescriptionreference_release( pMembers[35] );
        typelib_typedescriptionreference_release( pMembers[36] );
        typelib_typedescriptionreference_release( pMembers[37] );
        typelib_typedescriptionreference_release( pMembers[38] );
        typelib_typedescriptionreference_release( pMembers[39] );
        typelib_typedescriptionreference_release( pMembers[40] );
        typelib_typedescriptionreference_release( pMembers[41] );
        typelib_typedescriptionreference_release( pMembers[42] );
        typelib_typedescriptionreference_release( pMembers[43] );
        typelib_typedescriptionreference_release( pMembers[44] );
        typelib_typedescriptionreference_release( pMembers[45] );
        typelib_typedescriptionreference_release( pMembers[46] );
        typelib_typedescriptionreference_release( pMembers[47] );
        typelib_typedescriptionreference_release( pMembers[48] );
        typelib_typedescriptionreference_release( pMembers[49] );
        typelib_typedescriptionreference_release( pMembers[50] );
        typelib_typedescriptionreference_release( pMembers[51] );
        typelib_typedescriptionreference_release( pMembers[52] );
        typelib_typedescriptionreference_release( pMembers[53] );
        typelib_typedescriptionreference_release( pMembers[54] );
        typelib_typedescriptionreference_release( pMembers[55] );
        typelib_typedescriptionreference_release( pMembers[56] );
        typelib_typedescriptionreference_release( pMembers[57] );
        typelib_typedescriptionreference_release( pMembers[58] );
        typelib_typedescriptionreference_release( pMembers[59] );
        typelib_typedescriptionreference_release( pMembers[60] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace report {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XReportControlFormat const *) {
    const ::css::uno::Type &rRet = *detail::theXReportControlFormatType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::beans::UnknownPropertyException >::get();
            ::cppu::UnoType< ::css::awt::FontDescriptor >::get();
            ::cppu::UnoType< ::css::lang::Locale >::get();
            ::cppu::UnoType< ::css::awt::FontSlant >::get();
            ::cppu::UnoType< ::css::style::VerticalAlignment >::get();
            ::cppu::UnoType< ::css::lang::IllegalArgumentException >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "long" );
                ::rtl::OUString sAttributeName0( "com.sun.star.report.XReportControlFormat::ControlBackground" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    3, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType0.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "boolean" );
                ::rtl::OUString sAttributeName1( "com.sun.star.report.XReportControlFormat::ControlBackgroundTransparent" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    4, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType1.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "short" );
                ::rtl::OUString sAttributeName2( "com.sun.star.report.XReportControlFormat::ParaAdjust" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    5, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType2.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName3( "com.sun.star.report.XReportControlFormat::FontDescriptor" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    6, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType3.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName4( "com.sun.star.report.XReportControlFormat::FontDescriptorAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    7, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType4.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "com.sun.star.awt.FontDescriptor" );
                ::rtl::OUString sAttributeName5( "com.sun.star.report.XReportControlFormat::FontDescriptorComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    8, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType5.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "short" );
                ::rtl::OUString sAttributeName6( "com.sun.star.report.XReportControlFormat::ControlTextEmphasis" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    9, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType6.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "short" );
                ::rtl::OUString sAttributeName7( "com.sun.star.report.XReportControlFormat::CharEmphasis" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    10, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType7.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "boolean" );
                ::rtl::OUString sAttributeName8( "com.sun.star.report.XReportControlFormat::CharCombineIsOn" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    11, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType8.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "string" );
                ::rtl::OUString sAttributeName9( "com.sun.star.report.XReportControlFormat::CharCombinePrefix" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    12, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType9.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "string" );
                ::rtl::OUString sAttributeName10( "com.sun.star.report.XReportControlFormat::CharCombineSuffix" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    13, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType10.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "boolean" );
                ::rtl::OUString sAttributeName11( "com.sun.star.report.XReportControlFormat::CharHidden" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    14, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType11.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "boolean" );
                ::rtl::OUString sAttributeName12( "com.sun.star.report.XReportControlFormat::CharShadowed" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    15, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType12.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType13( "boolean" );
                ::rtl::OUString sAttributeName13( "com.sun.star.report.XReportControlFormat::CharContoured" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    16, sAttributeName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType13.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType14( "short" );
                ::rtl::OUString sAttributeName14( "com.sun.star.report.XReportControlFormat::CharCaseMap" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    17, sAttributeName14.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType14.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType15( "com.sun.star.lang.Locale" );
                ::rtl::OUString sAttributeName15( "com.sun.star.report.XReportControlFormat::CharLocale" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    18, sAttributeName15.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType15.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType16( "short" );
                ::rtl::OUString sAttributeName16( "com.sun.star.report.XReportControlFormat::CharEscapement" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    19, sAttributeName16.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType16.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType17( "byte" );
                ::rtl::OUString sAttributeName17( "com.sun.star.report.XReportControlFormat::CharEscapementHeight" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    20, sAttributeName17.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BYTE, sAttributeType17.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType18( "boolean" );
                ::rtl::OUString sAttributeName18( "com.sun.star.report.XReportControlFormat::CharAutoKerning" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    21, sAttributeName18.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType18.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType19( "short" );
                ::rtl::OUString sAttributeName19( "com.sun.star.report.XReportControlFormat::CharKerning" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    22, sAttributeName19.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType19.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType20( "boolean" );
                ::rtl::OUString sAttributeName20( "com.sun.star.report.XReportControlFormat::CharFlash" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    23, sAttributeName20.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType20.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType21( "short" );
                ::rtl::OUString sAttributeName21( "com.sun.star.report.XReportControlFormat::CharRelief" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    24, sAttributeName21.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType21.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType22( "string" );
                ::rtl::OUString sAttributeName22( "com.sun.star.report.XReportControlFormat::CharFontName" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    25, sAttributeName22.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType22.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType23( "string" );
                ::rtl::OUString sAttributeName23( "com.sun.star.report.XReportControlFormat::CharFontStyleName" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    26, sAttributeName23.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType23.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType24( "short" );
                ::rtl::OUString sAttributeName24( "com.sun.star.report.XReportControlFormat::CharFontFamily" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    27, sAttributeName24.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType24.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType25( "short" );
                ::rtl::OUString sAttributeName25( "com.sun.star.report.XReportControlFormat::CharFontCharSet" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    28, sAttributeName25.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType25.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType26( "short" );
                ::rtl::OUString sAttributeName26( "com.sun.star.report.XReportControlFormat::CharFontPitch" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    29, sAttributeName26.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType26.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType27( "long" );
                ::rtl::OUString sAttributeName27( "com.sun.star.report.XReportControlFormat::CharColor" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    30, sAttributeName27.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType27.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType28( "long" );
                ::rtl::OUString sAttributeName28( "com.sun.star.report.XReportControlFormat::CharUnderlineColor" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    31, sAttributeName28.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType28.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType29( "float" );
                ::rtl::OUString sAttributeName29( "com.sun.star.report.XReportControlFormat::CharHeight" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    32, sAttributeName29.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_FLOAT, sAttributeType29.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType30( "short" );
                ::rtl::OUString sAttributeName30( "com.sun.star.report.XReportControlFormat::CharUnderline" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    33, sAttributeName30.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType30.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType31( "float" );
                ::rtl::OUString sAttributeName31( "com.sun.star.report.XReportControlFormat::CharWeight" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    34, sAttributeName31.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_FLOAT, sAttributeType31.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType32( "com.sun.star.awt.FontSlant" );
                ::rtl::OUString sAttributeName32( "com.sun.star.report.XReportControlFormat::CharPosture" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    35, sAttributeName32.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_ENUM, sAttributeType32.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType33( "short" );
                ::rtl::OUString sAttributeName33( "com.sun.star.report.XReportControlFormat::CharStrikeout" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    36, sAttributeName33.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType33.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType34( "boolean" );
                ::rtl::OUString sAttributeName34( "com.sun.star.report.XReportControlFormat::CharWordMode" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    37, sAttributeName34.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType34.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType35( "short" );
                ::rtl::OUString sAttributeName35( "com.sun.star.report.XReportControlFormat::CharRotation" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    38, sAttributeName35.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType35.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType36( "short" );
                ::rtl::OUString sAttributeName36( "com.sun.star.report.XReportControlFormat::CharScaleWidth" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    39, sAttributeName36.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType36.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType37( "com.sun.star.style.VerticalAlignment" );
                ::rtl::OUString sAttributeName37( "com.sun.star.report.XReportControlFormat::VerticalAlign" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.lang.IllegalArgumentException" );
                ::rtl::OUString the_setExceptionName1( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData, the_setExceptionName1.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    40, sAttributeName37.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_ENUM, sAttributeType37.pData,
                    sal_False, 1, the_getExceptions, 2, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType38( "string" );
                ::rtl::OUString sAttributeName38( "com.sun.star.report.XReportControlFormat::HyperLinkURL" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    41, sAttributeName38.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType38.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType39( "string" );
                ::rtl::OUString sAttributeName39( "com.sun.star.report.XReportControlFormat::HyperLinkTarget" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    42, sAttributeName39.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType39.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType40( "string" );
                ::rtl::OUString sAttributeName40( "com.sun.star.report.XReportControlFormat::HyperLinkName" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    43, sAttributeName40.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType40.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType41( "string" );
                ::rtl::OUString sAttributeName41( "com.sun.star.report.XReportControlFormat::VisitedCharStyleName" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    44, sAttributeName41.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType41.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType42( "string" );
                ::rtl::OUString sAttributeName42( "com.sun.star.report.XReportControlFormat::UnvisitedCharStyleName" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    45, sAttributeName42.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType42.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType43( "float" );
                ::rtl::OUString sAttributeName43( "com.sun.star.report.XReportControlFormat::CharHeightAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    46, sAttributeName43.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_FLOAT, sAttributeType43.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType44( "float" );
                ::rtl::OUString sAttributeName44( "com.sun.star.report.XReportControlFormat::CharWeightAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    47, sAttributeName44.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_FLOAT, sAttributeType44.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType45( "string" );
                ::rtl::OUString sAttributeName45( "com.sun.star.report.XReportControlFormat::CharFontNameAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    48, sAttributeName45.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType45.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType46( "string" );
                ::rtl::OUString sAttributeName46( "com.sun.star.report.XReportControlFormat::CharFontStyleNameAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    49, sAttributeName46.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType46.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType47( "short" );
                ::rtl::OUString sAttributeName47( "com.sun.star.report.XReportControlFormat::CharFontFamilyAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    50, sAttributeName47.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType47.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType48( "short" );
                ::rtl::OUString sAttributeName48( "com.sun.star.report.XReportControlFormat::CharFontCharSetAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    51, sAttributeName48.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType48.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType49( "short" );
                ::rtl::OUString sAttributeName49( "com.sun.star.report.XReportControlFormat::CharFontPitchAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    52, sAttributeName49.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType49.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType50( "com.sun.star.awt.FontSlant" );
                ::rtl::OUString sAttributeName50( "com.sun.star.report.XReportControlFormat::CharPostureAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    53, sAttributeName50.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_ENUM, sAttributeType50.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType51( "com.sun.star.lang.Locale" );
                ::rtl::OUString sAttributeName51( "com.sun.star.report.XReportControlFormat::CharLocaleAsian" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    54, sAttributeName51.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType51.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType52( "float" );
                ::rtl::OUString sAttributeName52( "com.sun.star.report.XReportControlFormat::CharHeightComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    55, sAttributeName52.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_FLOAT, sAttributeType52.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType53( "float" );
                ::rtl::OUString sAttributeName53( "com.sun.star.report.XReportControlFormat::CharWeightComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    56, sAttributeName53.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_FLOAT, sAttributeType53.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType54( "string" );
                ::rtl::OUString sAttributeName54( "com.sun.star.report.XReportControlFormat::CharFontNameComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    57, sAttributeName54.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType54.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType55( "string" );
                ::rtl::OUString sAttributeName55( "com.sun.star.report.XReportControlFormat::CharFontStyleNameComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    58, sAttributeName55.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType55.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType56( "short" );
                ::rtl::OUString sAttributeName56( "com.sun.star.report.XReportControlFormat::CharFontFamilyComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    59, sAttributeName56.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType56.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType57( "short" );
                ::rtl::OUString sAttributeName57( "com.sun.star.report.XReportControlFormat::CharFontCharSetComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    60, sAttributeName57.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType57.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType58( "short" );
                ::rtl::OUString sAttributeName58( "com.sun.star.report.XReportControlFormat::CharFontPitchComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    61, sAttributeName58.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType58.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType59( "com.sun.star.awt.FontSlant" );
                ::rtl::OUString sAttributeName59( "com.sun.star.report.XReportControlFormat::CharPostureComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    62, sAttributeName59.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_ENUM, sAttributeType59.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType60( "com.sun.star.lang.Locale" );
                ::rtl::OUString sAttributeName60( "com.sun.star.report.XReportControlFormat::CharLocaleComplex" );
                ::rtl::OUString the_getExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_getExceptions[] = { the_getExceptionName0.pData };
                ::rtl::OUString the_setExceptionName0( "com.sun.star.beans.UnknownPropertyException" );
                rtl_uString * the_setExceptions[] = { the_setExceptionName0.pData };
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    63, sAttributeName60.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType60.pData,
                    sal_False, 1, the_getExceptions, 1, the_setExceptions );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::report::XReportControlFormat > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportControlFormat > >::get();
}

::css::uno::Type const & ::css::report::XReportControlFormat::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::report::XReportControlFormat > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REPORT_XREPORTCONTROLFORMAT_HPP
