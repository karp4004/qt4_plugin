#ifndef INCLUDED_COM_SUN_STAR_REPORT_XSHAPE_HPP
#define INCLUDED_COM_SUN_STAR_REPORT_XSHAPE_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/report/XShape.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/drawing/HomogenMatrix3.hpp"
#include "com/sun/star/report/XReportControlModel.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace report { namespace detail {

struct theXShapeType : public rtl::StaticWithInit< ::css::uno::Type *, theXShapeType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.report.XShape" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::report::XReportControlModel > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[6] = { 0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.report.XShape::ZOrder" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.report.XShape::Transformation" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.report.XShape::CustomShapeEngine" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.report.XShape::CustomShapeData" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.report.XShape::CustomShapeGeometry" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.report.XShape::Opaque" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            6,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace report {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XShape const *) {
    const ::css::uno::Type &rRet = *detail::theXShapeType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::drawing::HomogenMatrix3 >::get();
            ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::PropertyValue > >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "long" );
                ::rtl::OUString sAttributeName0( "com.sun.star.report.XShape::ZOrder" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    106, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_LONG, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "com.sun.star.drawing.HomogenMatrix3" );
                ::rtl::OUString sAttributeName1( "com.sun.star.report.XShape::Transformation" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    107, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "string" );
                ::rtl::OUString sAttributeName2( "com.sun.star.report.XShape::CustomShapeEngine" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    108, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "string" );
                ::rtl::OUString sAttributeName3( "com.sun.star.report.XShape::CustomShapeData" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    109, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRING, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "[]com.sun.star.beans.PropertyValue" );
                ::rtl::OUString sAttributeName4( "com.sun.star.report.XShape::CustomShapeGeometry" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    110, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "boolean" );
                ::rtl::OUString sAttributeName5( "com.sun.star.report.XShape::Opaque" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    111, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType5.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::report::XShape > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::report::XShape > >::get();
}

::css::uno::Type const & ::css::report::XShape::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::report::XShape > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_REPORT_XSHAPE_HPP
