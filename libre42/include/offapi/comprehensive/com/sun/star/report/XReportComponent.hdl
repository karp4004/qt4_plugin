#ifndef INCLUDED_COM_SUN_STAR_REPORT_XREPORTCOMPONENT_HDL
#define INCLUDED_COM_SUN_STAR_REPORT_XREPORTCOMPONENT_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/beans/PropertyVetoException.hdl"
#include "com/sun/star/beans/UnknownPropertyException.hdl"
#include "com/sun/star/beans/XPropertySet.hdl"
#include "com/sun/star/container/XChild.hdl"
#include "com/sun/star/drawing/XShape.hdl"
#include "com/sun/star/lang/IllegalArgumentException.hdl"
#include "com/sun/star/lang/XComponent.hdl"
namespace com { namespace sun { namespace star { namespace report { class XSection; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/util/XCloneable.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace report {

class SAL_NO_VTABLE XReportComponent : public ::css::util::XCloneable, public ::css::container::XChild, public ::css::lang::XComponent, public ::css::drawing::XShape, public ::css::beans::XPropertySet
{
public:

    // Attributes
    virtual ::rtl::OUString SAL_CALL getName() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setName( const ::rtl::OUString& _name ) /* throw (::css::beans::PropertyVetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getHeight() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setHeight( ::sal_Int32 _height ) /* throw (::css::beans::PropertyVetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getPositionX() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPositionX( ::sal_Int32 _positionx ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getPositionY() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPositionY( ::sal_Int32 _positiony ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getWidth() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setWidth( ::sal_Int32 _width ) /* throw (::css::beans::PropertyVetoException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int16 SAL_CALL getControlBorder() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setControlBorder( ::sal_Int16 _controlborder ) /* throw (::css::lang::IllegalArgumentException, ::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Int32 SAL_CALL getControlBorderColor() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setControlBorderColor( ::sal_Int32 _controlbordercolor ) /* throw (::css::lang::IllegalArgumentException, ::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::sal_Bool SAL_CALL getPrintRepeatedValues() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setPrintRepeatedValues( ::sal_Bool _printrepeatedvalues ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getMasterFields() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setMasterFields( const ::css::uno::Sequence< ::rtl::OUString >& _masterfields ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::rtl::OUString > SAL_CALL getDetailFields() /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual void SAL_CALL setDetailFields( const ::css::uno::Sequence< ::rtl::OUString >& _detailfields ) /* throw (::css::beans::UnknownPropertyException, ::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Reference< ::css::report::XSection > SAL_CALL getSection() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XReportComponent() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::report::XReportComponent const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::report::XReportComponent > *) SAL_THROW(());

#endif
