#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_MODULEINFO_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_MODULEINFO_HPP

#include "sal/config.h"

#include "com/sun/star/script/ModuleInfo.hdl"

#include "com/sun/star/uno/XInterface.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline ModuleInfo::ModuleInfo() SAL_THROW(())
    : ModuleObject()
    , ModuleType(0)
{
}

inline ModuleInfo::ModuleInfo(const ::css::uno::Reference< ::css::uno::XInterface >& ModuleObject_, const ::sal_Int32& ModuleType_) SAL_THROW(())
    : ModuleObject(ModuleObject_)
    , ModuleType(ModuleType_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theModuleInfoType : public rtl::StaticWithInit< ::css::uno::Type *, theModuleInfoType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.ModuleInfo" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::uno::XInterface > >::get();
        ::rtl::OUString the_tname0( "com.sun.star.uno.XInterface" );
        ::rtl::OUString the_name0( "ModuleObject" );
        ::rtl::OUString the_tname1( "long" );
        ::rtl::OUString the_name1( "ModuleType" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_INTERFACE, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::ModuleInfo const *) {
    return *detail::theModuleInfoType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::ModuleInfo const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::ModuleInfo >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_MODULEINFO_HPP
