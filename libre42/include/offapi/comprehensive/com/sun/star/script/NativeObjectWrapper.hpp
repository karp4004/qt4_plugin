#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_NATIVEOBJECTWRAPPER_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_NATIVEOBJECTWRAPPER_HPP

#include "sal/config.h"

#include "com/sun/star/script/NativeObjectWrapper.hdl"

#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script {

inline NativeObjectWrapper::NativeObjectWrapper() SAL_THROW(())
    : ObjectId()
{
}

inline NativeObjectWrapper::NativeObjectWrapper(const ::css::uno::Any& ObjectId_) SAL_THROW(())
    : ObjectId(ObjectId_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theNativeObjectWrapperType : public rtl::StaticWithInit< ::css::uno::Type *, theNativeObjectWrapperType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.NativeObjectWrapper" );
        ::rtl::OUString the_tname0( "any" );
        ::rtl::OUString the_name0( "ObjectId" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_ANY, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::NativeObjectWrapper const *) {
    return *detail::theNativeObjectWrapperType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::NativeObjectWrapper const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::NativeObjectWrapper >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_NATIVEOBJECTWRAPPER_HPP
