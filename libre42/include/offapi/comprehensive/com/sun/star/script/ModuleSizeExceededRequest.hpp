#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_MODULESIZEEXCEEDEDREQUEST_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_MODULESIZEEXCEEDEDREQUEST_HPP

#include "sal/config.h"

#include "com/sun/star/script/ModuleSizeExceededRequest.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace script {

inline ModuleSizeExceededRequest::ModuleSizeExceededRequest() SAL_THROW(())
    : ::css::uno::Exception()
    , Names()
{
    ::cppu::UnoType< ::css::script::ModuleSizeExceededRequest >::get();
}

inline ModuleSizeExceededRequest::ModuleSizeExceededRequest(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::css::uno::Sequence< ::rtl::OUString >& Names_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , Names(Names_)
{
    ::cppu::UnoType< ::css::script::ModuleSizeExceededRequest >::get();
}

ModuleSizeExceededRequest::ModuleSizeExceededRequest(ModuleSizeExceededRequest const & the_other): ::css::uno::Exception(the_other), Names(the_other.Names) {}

ModuleSizeExceededRequest::~ModuleSizeExceededRequest() {}

ModuleSizeExceededRequest & ModuleSizeExceededRequest::operator =(ModuleSizeExceededRequest const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    Names = the_other.Names;
    return *this;
}

} } } }

namespace com { namespace sun { namespace star { namespace script { namespace detail {

struct theModuleSizeExceededRequestType : public rtl::StaticWithInit< ::css::uno::Type *, theModuleSizeExceededRequestType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.script.ModuleSizeExceededRequest" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();

        typelib_CompoundMember_Init aMembers[1];
        ::rtl::OUString sMemberType0( "[]string" );
        ::rtl::OUString sMemberName0( "Names" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_SEQUENCE;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            1,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace script {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::ModuleSizeExceededRequest const *) {
    return *detail::theModuleSizeExceededRequestType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::ModuleSizeExceededRequest const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::ModuleSizeExceededRequest >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_MODULESIZEEXCEEDEDREQUEST_HPP
