#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_THEMASTERSCRIPTPROVIDERFACTORY_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_THEMASTERSCRIPTPROVIDERFACTORY_HPP

#include "sal/config.h"

#include <cassert>

#include "com/sun/star/script/provider/XScriptProviderFactory.hpp"
#include "com/sun/star/uno/DeploymentException.hpp"
#include "com/sun/star/uno/XComponentContext.hpp"
#include "com/sun/star/uno/Any.hxx"
#include "com/sun/star/uno/Reference.hxx"
#include "cppu/unotype.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace script { namespace provider {

class theMasterScriptProviderFactory {
public:
    static ::css::uno::Reference< ::css::script::provider::XScriptProviderFactory > get(::css::uno::Reference< ::css::uno::XComponentContext > const & the_context) {
        assert(the_context.is());
        ::css::uno::Reference< ::css::script::provider::XScriptProviderFactory > instance;
        if (!(the_context->getValueByName(::rtl::OUString( "/singletons/com.sun.star.script.provider.theMasterScriptProviderFactory" )) >>= instance) || !instance.is()) {
            throw ::css::uno::DeploymentException(::rtl::OUString( "component context fails to supply singleton com.sun.star.script.provider.theMasterScriptProviderFactory of type com.sun.star.script.provider.XScriptProviderFactory" ), the_context);
        }
        return instance;
    }

private:
    theMasterScriptProviderFactory(); // not implemented
    theMasterScriptProviderFactory(theMasterScriptProviderFactory &); // not implemented
    ~theMasterScriptProviderFactory(); // not implemented
    void operator =(theMasterScriptProviderFactory); // not implemented
};

} } } } }

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_THEMASTERSCRIPTPROVIDERFACTORY_HPP
