#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_VBA_VBASCRIPTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_VBA_VBASCRIPTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/script/vba/VBAScriptEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace script { namespace vba {

inline VBAScriptEvent::VBAScriptEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , Identifier(0)
    , ModuleName()
{
}

inline VBAScriptEvent::VBAScriptEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int32& Identifier_, const ::rtl::OUString& ModuleName_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , Identifier(Identifier_)
    , ModuleName(ModuleName_)
{
}

} } } } }

namespace com { namespace sun { namespace star { namespace script { namespace vba { namespace detail {

struct theVBAScriptEventType : public rtl::StaticWithInit< ::css::uno::Type *, theVBAScriptEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.script.vba.VBAScriptEvent" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "Identifier" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name1( "ModuleName" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } } }

namespace com { namespace sun { namespace star { namespace script { namespace vba {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::vba::VBAScriptEvent const *) {
    return *detail::theVBAScriptEventType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::vba::VBAScriptEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::vba::VBAScriptEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_VBA_VBASCRIPTEVENT_HPP
