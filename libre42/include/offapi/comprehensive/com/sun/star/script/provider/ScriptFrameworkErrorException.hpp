#ifndef INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_SCRIPTFRAMEWORKERROREXCEPTION_HPP
#define INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_SCRIPTFRAMEWORKERROREXCEPTION_HPP

#include "sal/config.h"

#include "com/sun/star/script/provider/ScriptFrameworkErrorException.hdl"

#include "com/sun/star/uno/Exception.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace script { namespace provider {

inline ScriptFrameworkErrorException::ScriptFrameworkErrorException() SAL_THROW(())
    : ::css::uno::Exception()
    , scriptName()
    , language()
    , errorType(0)
{
    ::cppu::UnoType< ::css::script::provider::ScriptFrameworkErrorException >::get();
}

inline ScriptFrameworkErrorException::ScriptFrameworkErrorException(const ::rtl::OUString& Message_, const ::css::uno::Reference< ::css::uno::XInterface >& Context_, const ::rtl::OUString& scriptName_, const ::rtl::OUString& language_, const ::sal_Int32& errorType_) SAL_THROW(())
    : ::css::uno::Exception(Message_, Context_)
    , scriptName(scriptName_)
    , language(language_)
    , errorType(errorType_)
{
    ::cppu::UnoType< ::css::script::provider::ScriptFrameworkErrorException >::get();
}

ScriptFrameworkErrorException::ScriptFrameworkErrorException(ScriptFrameworkErrorException const & the_other): ::css::uno::Exception(the_other), scriptName(the_other.scriptName), language(the_other.language), errorType(the_other.errorType) {}

ScriptFrameworkErrorException::~ScriptFrameworkErrorException() {}

ScriptFrameworkErrorException & ScriptFrameworkErrorException::operator =(ScriptFrameworkErrorException const & the_other) {
    //TODO: Just like its implicitly-defined counterpart, this function definition is not exception-safe
    ::css::uno::Exception::operator =(the_other);
    scriptName = the_other.scriptName;
    language = the_other.language;
    errorType = the_other.errorType;
    return *this;
}

} } } } }

namespace com { namespace sun { namespace star { namespace script { namespace provider { namespace detail {

struct theScriptFrameworkErrorExceptionType : public rtl::StaticWithInit< ::css::uno::Type *, theScriptFrameworkErrorExceptionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.script.provider.ScriptFrameworkErrorException" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;
        const ::css::uno::Type& rSuperType = ::cppu::UnoType< ::css::uno::Exception >::get();

        typelib_CompoundMember_Init aMembers[3];
        ::rtl::OUString sMemberType0( "string" );
        ::rtl::OUString sMemberName0( "scriptName" );
        aMembers[0].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[0].pTypeName = sMemberType0.pData;
        aMembers[0].pMemberName = sMemberName0.pData;
        ::rtl::OUString sMemberType1( "string" );
        ::rtl::OUString sMemberName1( "language" );
        aMembers[1].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_STRING;
        aMembers[1].pTypeName = sMemberType1.pData;
        aMembers[1].pMemberName = sMemberName1.pData;
        ::rtl::OUString sMemberType2( "long" );
        ::rtl::OUString sMemberName2( "errorType" );
        aMembers[2].eTypeClass = (typelib_TypeClass)::css::uno::TypeClass_LONG;
        aMembers[2].pTypeName = sMemberType2.pData;
        aMembers[2].pMemberName = sMemberName2.pData;

        typelib_typedescription_new(
            &pTD,
            (typelib_TypeClass)::css::uno::TypeClass_EXCEPTION, sTypeName.pData,
            rSuperType.getTypeLibType(),
            3,
            aMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );

        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_EXCEPTION, sTypeName ); // leaked
    }
};

} } } } } }

namespace com { namespace sun { namespace star { namespace script { namespace provider {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::script::provider::ScriptFrameworkErrorException const *) {
    return *detail::theScriptFrameworkErrorExceptionType::get();
}

} } } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::script::provider::ScriptFrameworkErrorException const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::script::provider::ScriptFrameworkErrorException >::get();
}

#endif // INCLUDED_COM_SUN_STAR_SCRIPT_PROVIDER_SCRIPTFRAMEWORKERROREXCEPTION_HPP
