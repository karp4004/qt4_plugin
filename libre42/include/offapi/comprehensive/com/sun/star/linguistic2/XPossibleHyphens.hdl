#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_XPOSSIBLEHYPHENS_HDL
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_XPOSSIBLEHYPHENS_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/Locale.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

class SAL_NO_VTABLE XPossibleHyphens : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::rtl::OUString SAL_CALL getWord() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::lang::Locale SAL_CALL getLocale() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::rtl::OUString SAL_CALL getPossibleHyphens() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    virtual ::css::uno::Sequence< ::sal_Int16 > SAL_CALL getHyphenationPositions() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XPossibleHyphens() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::XPossibleHyphens const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::linguistic2::XPossibleHyphens > *) SAL_THROW(());

#endif
