#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_DICTIONARYEVENT_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_DICTIONARYEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/linguistic2/DictionaryEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/linguistic2/XDictionaryEntry.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline DictionaryEvent::DictionaryEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , nEvent(0)
    , xDictionaryEntry()
{
}

inline DictionaryEvent::DictionaryEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& nEvent_, const ::css::uno::Reference< ::css::linguistic2::XDictionaryEntry >& xDictionaryEntry_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , nEvent(nEvent_)
    , xDictionaryEntry(xDictionaryEntry_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace linguistic2 { namespace detail {

struct theDictionaryEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDictionaryEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.linguistic2.DictionaryEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "nEvent" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::linguistic2::XDictionaryEntry > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.linguistic2.XDictionaryEntry" );
        ::rtl::OUString the_name1( "xDictionaryEntry" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::DictionaryEvent const *) {
    return *detail::theDictionaryEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::linguistic2::DictionaryEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::linguistic2::DictionaryEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_DICTIONARYEVENT_HPP
