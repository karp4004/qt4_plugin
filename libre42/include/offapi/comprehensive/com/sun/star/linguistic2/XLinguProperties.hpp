#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_XLINGUPROPERTIES_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_XLINGUPROPERTIES_HPP

#include "sal/config.h"

#include <exception>

#include "com/sun/star/linguistic2/XLinguProperties.hdl"

#include "com/sun/star/beans/XPropertySet.hpp"
#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/uno/RuntimeException.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace linguistic2 { namespace detail {

struct theXLinguPropertiesType : public rtl::StaticWithInit< ::css::uno::Type *, theXLinguPropertiesType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.linguistic2.XLinguProperties" );

        // Start inline typedescription generation
        typelib_InterfaceTypeDescription * pTD = 0;

        typelib_TypeDescriptionReference * aSuperTypes[1];
        aSuperTypes[0] = ::cppu::UnoType< ::css::uno::Reference< ::css::beans::XPropertySet > >::get().getTypeLibType();
        typelib_TypeDescriptionReference * pMembers[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
        ::rtl::OUString sAttributeName0( "com.sun.star.linguistic2.XLinguProperties::IsUseDictionaryList" );
        typelib_typedescriptionreference_new( &pMembers[0],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName0.pData );
        ::rtl::OUString sAttributeName1( "com.sun.star.linguistic2.XLinguProperties::IsIgnoreControlCharacters" );
        typelib_typedescriptionreference_new( &pMembers[1],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName1.pData );
        ::rtl::OUString sAttributeName2( "com.sun.star.linguistic2.XLinguProperties::IsSpellUpperCase" );
        typelib_typedescriptionreference_new( &pMembers[2],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName2.pData );
        ::rtl::OUString sAttributeName3( "com.sun.star.linguistic2.XLinguProperties::IsSpellWithDigits" );
        typelib_typedescriptionreference_new( &pMembers[3],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName3.pData );
        ::rtl::OUString sAttributeName4( "com.sun.star.linguistic2.XLinguProperties::IsSpellCapitalization" );
        typelib_typedescriptionreference_new( &pMembers[4],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName4.pData );
        ::rtl::OUString sAttributeName5( "com.sun.star.linguistic2.XLinguProperties::HyphMinLeading" );
        typelib_typedescriptionreference_new( &pMembers[5],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName5.pData );
        ::rtl::OUString sAttributeName6( "com.sun.star.linguistic2.XLinguProperties::HyphMinTrailing" );
        typelib_typedescriptionreference_new( &pMembers[6],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName6.pData );
        ::rtl::OUString sAttributeName7( "com.sun.star.linguistic2.XLinguProperties::HyphMinWordLength" );
        typelib_typedescriptionreference_new( &pMembers[7],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName7.pData );
        ::rtl::OUString sAttributeName8( "com.sun.star.linguistic2.XLinguProperties::DefaultLocale" );
        typelib_typedescriptionreference_new( &pMembers[8],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName8.pData );
        ::rtl::OUString sAttributeName9( "com.sun.star.linguistic2.XLinguProperties::IsHyphAuto" );
        typelib_typedescriptionreference_new( &pMembers[9],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName9.pData );
        ::rtl::OUString sAttributeName10( "com.sun.star.linguistic2.XLinguProperties::IsHyphSpecial" );
        typelib_typedescriptionreference_new( &pMembers[10],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName10.pData );
        ::rtl::OUString sAttributeName11( "com.sun.star.linguistic2.XLinguProperties::IsSpellAuto" );
        typelib_typedescriptionreference_new( &pMembers[11],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName11.pData );
        ::rtl::OUString sAttributeName12( "com.sun.star.linguistic2.XLinguProperties::IsSpellSpecial" );
        typelib_typedescriptionreference_new( &pMembers[12],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName12.pData );
        ::rtl::OUString sAttributeName13( "com.sun.star.linguistic2.XLinguProperties::IsWrapReverse" );
        typelib_typedescriptionreference_new( &pMembers[13],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName13.pData );
        ::rtl::OUString sAttributeName14( "com.sun.star.linguistic2.XLinguProperties::DefaultLocale_CJK" );
        typelib_typedescriptionreference_new( &pMembers[14],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName14.pData );
        ::rtl::OUString sAttributeName15( "com.sun.star.linguistic2.XLinguProperties::DefaultLocale_CTL" );
        typelib_typedescriptionreference_new( &pMembers[15],
                                              (typelib_TypeClass)::css::uno::TypeClass_INTERFACE_ATTRIBUTE,
                                              sAttributeName15.pData );

        typelib_typedescription_newMIInterface(
            &pTD,
            sTypeName.pData, 0, 0, 0, 0, 0,
            1, aSuperTypes,
            16,
            pMembers );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescriptionreference_release( pMembers[0] );
        typelib_typedescriptionreference_release( pMembers[1] );
        typelib_typedescriptionreference_release( pMembers[2] );
        typelib_typedescriptionreference_release( pMembers[3] );
        typelib_typedescriptionreference_release( pMembers[4] );
        typelib_typedescriptionreference_release( pMembers[5] );
        typelib_typedescriptionreference_release( pMembers[6] );
        typelib_typedescriptionreference_release( pMembers[7] );
        typelib_typedescriptionreference_release( pMembers[8] );
        typelib_typedescriptionreference_release( pMembers[9] );
        typelib_typedescriptionreference_release( pMembers[10] );
        typelib_typedescriptionreference_release( pMembers[11] );
        typelib_typedescriptionreference_release( pMembers[12] );
        typelib_typedescriptionreference_release( pMembers[13] );
        typelib_typedescriptionreference_release( pMembers[14] );
        typelib_typedescriptionreference_release( pMembers[15] );
        typelib_typedescription_release( (typelib_TypeDescription*)pTD );

        return new ::css::uno::Type( ::css::uno::TypeClass_INTERFACE, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::XLinguProperties const *) {
    const ::css::uno::Type &rRet = *detail::theXLinguPropertiesType::get();
    // End inline typedescription generation
    static bool bInitStarted = false;
    if (!bInitStarted)
    {
        ::osl::MutexGuard aGuard( ::osl::Mutex::getGlobalMutex() );
        if (!bInitStarted)
        {
            OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
            bInitStarted = true;
            ::cppu::UnoType< ::css::uno::RuntimeException >::get();
            ::cppu::UnoType< ::css::lang::Locale >::get();

            typelib_InterfaceAttributeTypeDescription * pAttribute = 0;
            {
                ::rtl::OUString sAttributeType0( "boolean" );
                ::rtl::OUString sAttributeName0( "com.sun.star.linguistic2.XLinguProperties::IsUseDictionaryList" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    10, sAttributeName0.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType0.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType1( "boolean" );
                ::rtl::OUString sAttributeName1( "com.sun.star.linguistic2.XLinguProperties::IsIgnoreControlCharacters" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    11, sAttributeName1.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType1.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType2( "boolean" );
                ::rtl::OUString sAttributeName2( "com.sun.star.linguistic2.XLinguProperties::IsSpellUpperCase" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    12, sAttributeName2.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType2.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType3( "boolean" );
                ::rtl::OUString sAttributeName3( "com.sun.star.linguistic2.XLinguProperties::IsSpellWithDigits" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    13, sAttributeName3.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType3.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType4( "boolean" );
                ::rtl::OUString sAttributeName4( "com.sun.star.linguistic2.XLinguProperties::IsSpellCapitalization" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    14, sAttributeName4.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType4.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType5( "short" );
                ::rtl::OUString sAttributeName5( "com.sun.star.linguistic2.XLinguProperties::HyphMinLeading" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    15, sAttributeName5.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType5.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType6( "short" );
                ::rtl::OUString sAttributeName6( "com.sun.star.linguistic2.XLinguProperties::HyphMinTrailing" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    16, sAttributeName6.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType6.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType7( "short" );
                ::rtl::OUString sAttributeName7( "com.sun.star.linguistic2.XLinguProperties::HyphMinWordLength" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    17, sAttributeName7.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_SHORT, sAttributeType7.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType8( "com.sun.star.lang.Locale" );
                ::rtl::OUString sAttributeName8( "com.sun.star.linguistic2.XLinguProperties::DefaultLocale" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    18, sAttributeName8.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType8.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType9( "boolean" );
                ::rtl::OUString sAttributeName9( "com.sun.star.linguistic2.XLinguProperties::IsHyphAuto" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    19, sAttributeName9.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType9.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType10( "boolean" );
                ::rtl::OUString sAttributeName10( "com.sun.star.linguistic2.XLinguProperties::IsHyphSpecial" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    20, sAttributeName10.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType10.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType11( "boolean" );
                ::rtl::OUString sAttributeName11( "com.sun.star.linguistic2.XLinguProperties::IsSpellAuto" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    21, sAttributeName11.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType11.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType12( "boolean" );
                ::rtl::OUString sAttributeName12( "com.sun.star.linguistic2.XLinguProperties::IsSpellSpecial" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    22, sAttributeName12.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType12.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType13( "boolean" );
                ::rtl::OUString sAttributeName13( "com.sun.star.linguistic2.XLinguProperties::IsWrapReverse" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    23, sAttributeName13.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_BOOLEAN, sAttributeType13.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType14( "com.sun.star.lang.Locale" );
                ::rtl::OUString sAttributeName14( "com.sun.star.linguistic2.XLinguProperties::DefaultLocale_CJK" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    24, sAttributeName14.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType14.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            {
                ::rtl::OUString sAttributeType15( "com.sun.star.lang.Locale" );
                ::rtl::OUString sAttributeName15( "com.sun.star.linguistic2.XLinguProperties::DefaultLocale_CTL" );
                typelib_typedescription_newExtendedInterfaceAttribute( &pAttribute,
                    25, sAttributeName15.pData,
                    (typelib_TypeClass)::css::uno::TypeClass_STRUCT, sAttributeType15.pData,
                    sal_False, 0, 0, 0, 0 );
                typelib_typedescription_register( (typelib_TypeDescription**)&pAttribute );
            }
            typelib_typedescription_release( (typelib_TypeDescription*)pAttribute );
        }
    }
    else
    {
        OSL_DOUBLE_CHECKED_LOCKING_MEMORY_BARRIER();
    }
    return rRet;
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::uno::Reference< ::css::linguistic2::XLinguProperties > const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::uno::Reference< ::css::linguistic2::XLinguProperties > >::get();
}

::css::uno::Type const & ::css::linguistic2::XLinguProperties::static_type(SAL_UNUSED_PARAMETER void *) {
    return ::getCppuType(static_cast< ::css::uno::Reference< ::css::linguistic2::XLinguProperties > * >(0));
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_XLINGUPROPERTIES_HPP
