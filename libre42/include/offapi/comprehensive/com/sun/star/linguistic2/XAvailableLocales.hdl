#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_XAVAILABLELOCALES_HDL
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_XAVAILABLELOCALES_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/lang/Locale.hdl"
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/XInterface.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

class SAL_NO_VTABLE XAvailableLocales : public ::css::uno::XInterface
{
public:

    // Methods
    virtual ::css::uno::Sequence< ::css::lang::Locale > SAL_CALL getAvailableLocales( const ::rtl::OUString& aServiceName ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XAvailableLocales() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::XAvailableLocales const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::linguistic2::XAvailableLocales > *) SAL_THROW(());

#endif
