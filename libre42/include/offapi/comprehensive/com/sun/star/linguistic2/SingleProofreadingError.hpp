#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_SINGLEPROOFREADINGERROR_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_SINGLEPROOFREADINGERROR_HPP

#include "sal/config.h"

#include "com/sun/star/linguistic2/SingleProofreadingError.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline SingleProofreadingError::SingleProofreadingError() SAL_THROW(())
    : nErrorStart(0)
    , nErrorLength(0)
    , nErrorType(0)
    , aRuleIdentifier()
    , aShortComment()
    , aFullComment()
    , aSuggestions()
    , aProperties()
{
}

inline SingleProofreadingError::SingleProofreadingError(const ::sal_Int32& nErrorStart_, const ::sal_Int32& nErrorLength_, const ::sal_Int32& nErrorType_, const ::rtl::OUString& aRuleIdentifier_, const ::rtl::OUString& aShortComment_, const ::rtl::OUString& aFullComment_, const ::css::uno::Sequence< ::rtl::OUString >& aSuggestions_, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aProperties_) SAL_THROW(())
    : nErrorStart(nErrorStart_)
    , nErrorLength(nErrorLength_)
    , nErrorType(nErrorType_)
    , aRuleIdentifier(aRuleIdentifier_)
    , aShortComment(aShortComment_)
    , aFullComment(aFullComment_)
    , aSuggestions(aSuggestions_)
    , aProperties(aProperties_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace linguistic2 { namespace detail {

struct theSingleProofreadingErrorType : public rtl::StaticWithInit< ::css::uno::Type *, theSingleProofreadingErrorType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.linguistic2.SingleProofreadingError" );
        ::rtl::OUString the_tname0( "long" );
        ::rtl::OUString the_name0( "nErrorStart" );
        ::rtl::OUString the_name1( "nErrorLength" );
        ::rtl::OUString the_name2( "nErrorType" );
        ::rtl::OUString the_tname1( "string" );
        ::rtl::OUString the_name3( "aRuleIdentifier" );
        ::rtl::OUString the_name4( "aShortComment" );
        ::rtl::OUString the_name5( "aFullComment" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::rtl::OUString > >::get();
        ::rtl::OUString the_tname2( "[]string" );
        ::rtl::OUString the_name6( "aSuggestions" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::PropertyValue > >::get();
        ::rtl::OUString the_tname3( "[]com.sun.star.beans.PropertyValue" );
        ::rtl::OUString the_name7( "aProperties" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name1.pData }, false },
            { { typelib_TypeClass_LONG, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name3.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name4.pData }, false },
            { { typelib_TypeClass_STRING, the_tname1.pData, the_name5.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname2.pData, the_name6.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname3.pData, the_name7.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 8, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::SingleProofreadingError const *) {
    return *detail::theSingleProofreadingErrorType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::linguistic2::SingleProofreadingError const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::linguistic2::SingleProofreadingError >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_SINGLEPROOFREADINGERROR_HPP
