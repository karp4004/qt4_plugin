#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_DICTIONARYLISTEVENT_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_DICTIONARYLISTEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/linguistic2/DictionaryListEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/linguistic2/DictionaryEvent.hpp"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline DictionaryListEvent::DictionaryListEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , nCondensedEvent(0)
    , aDictionaryEvents()
{
}

inline DictionaryListEvent::DictionaryListEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& nCondensedEvent_, const ::css::uno::Sequence< ::css::linguistic2::DictionaryEvent >& aDictionaryEvents_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , nCondensedEvent(nCondensedEvent_)
    , aDictionaryEvents(aDictionaryEvents_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace linguistic2 { namespace detail {

struct theDictionaryListEventType : public rtl::StaticWithInit< ::css::uno::Type *, theDictionaryListEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.linguistic2.DictionaryListEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "nCondensedEvent" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::linguistic2::DictionaryEvent > >::get();
        ::rtl::OUString the_tname1( "[]com.sun.star.linguistic2.DictionaryEvent" );
        ::rtl::OUString the_name1( "aDictionaryEvents" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname1.pData, the_name1.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 2, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::DictionaryListEvent const *) {
    return *detail::theDictionaryListEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::linguistic2::DictionaryListEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::linguistic2::DictionaryListEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_DICTIONARYLISTEVENT_HPP
