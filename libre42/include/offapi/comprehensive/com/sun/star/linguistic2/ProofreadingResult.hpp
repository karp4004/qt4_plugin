#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_PROOFREADINGRESULT_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_PROOFREADINGRESULT_HPP

#include "sal/config.h"

#include "com/sun/star/linguistic2/ProofreadingResult.hdl"

#include "com/sun/star/beans/PropertyValue.hpp"
#include "com/sun/star/lang/Locale.hpp"
#include "com/sun/star/linguistic2/SingleProofreadingError.hpp"
#include "com/sun/star/linguistic2/XProofreader.hpp"
#include "com/sun/star/text/XFlatParagraph.hpp"
#include "com/sun/star/uno/Reference.hxx"
#include "com/sun/star/uno/Sequence.hxx"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ProofreadingResult::ProofreadingResult() SAL_THROW(())
    : aDocumentIdentifier()
    , xFlatParagraph()
    , aText()
    , aLocale()
    , nStartOfSentencePosition(0)
    , nBehindEndOfSentencePosition(0)
    , nStartOfNextSentencePosition(0)
    , aErrors()
    , aProperties()
    , xProofreader()
{
}

inline ProofreadingResult::ProofreadingResult(const ::rtl::OUString& aDocumentIdentifier_, const ::css::uno::Reference< ::css::text::XFlatParagraph >& xFlatParagraph_, const ::rtl::OUString& aText_, const ::css::lang::Locale& aLocale_, const ::sal_Int32& nStartOfSentencePosition_, const ::sal_Int32& nBehindEndOfSentencePosition_, const ::sal_Int32& nStartOfNextSentencePosition_, const ::css::uno::Sequence< ::css::linguistic2::SingleProofreadingError >& aErrors_, const ::css::uno::Sequence< ::css::beans::PropertyValue >& aProperties_, const ::css::uno::Reference< ::css::linguistic2::XProofreader >& xProofreader_) SAL_THROW(())
    : aDocumentIdentifier(aDocumentIdentifier_)
    , xFlatParagraph(xFlatParagraph_)
    , aText(aText_)
    , aLocale(aLocale_)
    , nStartOfSentencePosition(nStartOfSentencePosition_)
    , nBehindEndOfSentencePosition(nBehindEndOfSentencePosition_)
    , nStartOfNextSentencePosition(nStartOfNextSentencePosition_)
    , aErrors(aErrors_)
    , aProperties(aProperties_)
    , xProofreader(xProofreader_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace linguistic2 { namespace detail {

struct theProofreadingResultType : public rtl::StaticWithInit< ::css::uno::Type *, theProofreadingResultType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.linguistic2.ProofreadingResult" );
        ::rtl::OUString the_tname0( "string" );
        ::rtl::OUString the_name0( "aDocumentIdentifier" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::text::XFlatParagraph > >::get();
        ::rtl::OUString the_tname1( "com.sun.star.text.XFlatParagraph" );
        ::rtl::OUString the_name1( "xFlatParagraph" );
        ::rtl::OUString the_name2( "aText" );
        ::cppu::UnoType< ::css::lang::Locale >::get();
        ::rtl::OUString the_tname2( "com.sun.star.lang.Locale" );
        ::rtl::OUString the_name3( "aLocale" );
        ::rtl::OUString the_tname3( "long" );
        ::rtl::OUString the_name4( "nStartOfSentencePosition" );
        ::rtl::OUString the_name5( "nBehindEndOfSentencePosition" );
        ::rtl::OUString the_name6( "nStartOfNextSentencePosition" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::linguistic2::SingleProofreadingError > >::get();
        ::rtl::OUString the_tname4( "[]com.sun.star.linguistic2.SingleProofreadingError" );
        ::rtl::OUString the_name7( "aErrors" );
        ::cppu::UnoType< ::cppu::UnoSequenceType< ::css::beans::PropertyValue > >::get();
        ::rtl::OUString the_tname5( "[]com.sun.star.beans.PropertyValue" );
        ::rtl::OUString the_name8( "aProperties" );
        ::cppu::UnoType< ::css::uno::Reference< ::css::linguistic2::XProofreader > >::get();
        ::rtl::OUString the_tname6( "com.sun.star.linguistic2.XProofreader" );
        ::rtl::OUString the_name9( "xProofreader" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name0.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname1.pData, the_name1.pData }, false },
            { { typelib_TypeClass_STRING, the_tname0.pData, the_name2.pData }, false },
            { { typelib_TypeClass_STRUCT, the_tname2.pData, the_name3.pData }, false },
            { { typelib_TypeClass_LONG, the_tname3.pData, the_name4.pData }, false },
            { { typelib_TypeClass_LONG, the_tname3.pData, the_name5.pData }, false },
            { { typelib_TypeClass_LONG, the_tname3.pData, the_name6.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname4.pData, the_name7.pData }, false },
            { { typelib_TypeClass_SEQUENCE, the_tname5.pData, the_name8.pData }, false },
            { { typelib_TypeClass_INTERFACE, the_tname6.pData, the_name9.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, 0, 10, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::ProofreadingResult const *) {
    return *detail::theProofreadingResultType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::linguistic2::ProofreadingResult const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::linguistic2::ProofreadingResult >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_PROOFREADINGRESULT_HPP
