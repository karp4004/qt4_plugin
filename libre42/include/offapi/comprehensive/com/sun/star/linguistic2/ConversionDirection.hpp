#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_CONVERSIONDIRECTION_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_CONVERSIONDIRECTION_HPP

#include "sal/config.h"

#include "com/sun/star/linguistic2/ConversionDirection.hdl"

#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/instance.hxx"

namespace com { namespace sun { namespace star { namespace linguistic2 { namespace detail {

struct theConversionDirectionType : public rtl::StaticWithInit< ::css::uno::Type *, theConversionDirectionType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString sTypeName( "com.sun.star.linguistic2.ConversionDirection" );

        // Start inline typedescription generation
        typelib_TypeDescription * pTD = 0;

        rtl_uString* enumValueNames[2];
        ::rtl::OUString sEnumValue0( "FROM_LEFT" );
        enumValueNames[0] = sEnumValue0.pData;
        ::rtl::OUString sEnumValue1( "FROM_RIGHT" );
        enumValueNames[1] = sEnumValue1.pData;

        sal_Int32 enumValues[2];
        enumValues[0] = 0;
        enumValues[1] = 1;

        typelib_typedescription_newEnum( &pTD,
            sTypeName.pData,
            (sal_Int32)::com::sun::star::linguistic2::ConversionDirection_FROM_LEFT,
            2, enumValueNames, enumValues );

        typelib_typedescription_register( (typelib_TypeDescription**)&pTD );
        typelib_typedescription_release( pTD );
        // End inline typedescription generation

        return new ::css::uno::Type( ::css::uno::TypeClass_ENUM, sTypeName ); // leaked
    }
};

} } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::ConversionDirection const *) {
    return *detail::theConversionDirectionType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::linguistic2::ConversionDirection const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::linguistic2::ConversionDirection >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_CONVERSIONDIRECTION_HPP
