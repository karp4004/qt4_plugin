#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_XDICTIONARY1_HDL
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_XDICTIONARY1_HDL

#include "sal/config.h"

#include <exception>

#include "com/sun/star/container/XNamed.hdl"
#include "com/sun/star/linguistic2/DictionaryType.hdl"
namespace com { namespace sun { namespace star { namespace linguistic2 { class XDictionaryEntry; } } } }
namespace com { namespace sun { namespace star { namespace linguistic2 { class XDictionaryEventListener; } } } }
#include "com/sun/star/uno/RuntimeException.hdl"
#include "com/sun/star/uno/Reference.h"
#include "com/sun/star/uno/Sequence.h"
#include "cppu/macros.hxx"
#include "rtl/ustring.hxx"
#include "sal/types.h"

namespace com { namespace sun { namespace star { namespace uno { class Type; } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

class SAL_NO_VTABLE XDictionary1 : public ::css::container::XNamed
{
public:

    // Methods
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::css::linguistic2::DictionaryType SAL_CALL getDictionaryType() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual void SAL_CALL setActive( ::sal_Bool bActivate ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL isActive() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Int16 SAL_CALL getCount() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Int16 SAL_CALL getLanguage() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual void SAL_CALL setLanguage( ::sal_Int16 nLang ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::css::uno::Reference< ::css::linguistic2::XDictionaryEntry > SAL_CALL getEntry( const ::rtl::OUString& aWord ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL addEntry( const ::css::uno::Reference< ::css::linguistic2::XDictionaryEntry >& xDicEntry ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL add( const ::rtl::OUString& aWord, ::sal_Bool bIsNegative, const ::rtl::OUString& aRplcText ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL remove( const ::rtl::OUString& aWord ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL isFull() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::css::uno::Sequence< ::css::uno::Reference< ::css::linguistic2::XDictionaryEntry > > SAL_CALL getEntries() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual void SAL_CALL clear() /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL addDictionaryEventListener( const ::css::uno::Reference< ::css::linguistic2::XDictionaryEventListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;
    SAL_DEPRECATED_INTERNAL("marked @deprecated in UNOIDL") virtual ::sal_Bool SAL_CALL removeDictionaryEventListener( const ::css::uno::Reference< ::css::linguistic2::XDictionaryEventListener >& xListener ) /* throw (::css::uno::RuntimeException, ::std::exception) */ = 0;

    static inline ::css::uno::Type const & SAL_CALL static_type(void * = 0);

protected:
    ~XDictionary1() throw () {} // avoid warnings about virtual members and non-virtual dtor
};


inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::XDictionary1 const *);
} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(const ::css::uno::Reference< ::css::linguistic2::XDictionary1 > *) SAL_THROW(());

#endif
