#ifndef INCLUDED_COM_SUN_STAR_LINGUISTIC2_LINGUSERVICEEVENT_HPP
#define INCLUDED_COM_SUN_STAR_LINGUISTIC2_LINGUSERVICEEVENT_HPP

#include "sal/config.h"

#include "com/sun/star/linguistic2/LinguServiceEvent.hdl"

#include "com/sun/star/lang/EventObject.hpp"
#include "com/sun/star/uno/Type.hxx"
#include "cppu/unotype.hxx"
#include "osl/mutex.hxx"
#include "rtl/ustring.h"
#include "rtl/ustring.hxx"
#include "rtl/instance.hxx"
#include "sal/types.h"
#include "typelib/typeclass.h"
#include "typelib/typedescription.h"

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline LinguServiceEvent::LinguServiceEvent() SAL_THROW(())
    : ::css::lang::EventObject()
    , nEvent(0)
{
}

inline LinguServiceEvent::LinguServiceEvent(const ::css::uno::Reference< ::css::uno::XInterface >& Source_, const ::sal_Int16& nEvent_) SAL_THROW(())
    : ::css::lang::EventObject(Source_)
    , nEvent(nEvent_)
{
}

} } } }

namespace com { namespace sun { namespace star { namespace linguistic2 { namespace detail {

struct theLinguServiceEventType : public rtl::StaticWithInit< ::css::uno::Type *, theLinguServiceEventType >
{
    ::css::uno::Type * operator()() const
    {
        ::rtl::OUString the_name( "com.sun.star.linguistic2.LinguServiceEvent" );
        ::rtl::OUString the_tname0( "short" );
        ::rtl::OUString the_name0( "nEvent" );
        ::typelib_StructMember_Init the_members[] = {
            { { typelib_TypeClass_SHORT, the_tname0.pData, the_name0.pData }, false } };
        ::typelib_TypeDescription * the_newType = 0;
        ::typelib_typedescription_newStruct(&the_newType, the_name.pData, ::cppu::UnoType< ::css::lang::EventObject >::get().getTypeLibType(), 1, the_members);
        ::typelib_typedescription_register(&the_newType);
        ::typelib_typedescription_release(the_newType);
        return new ::css::uno::Type(::css::uno::TypeClass_STRUCT, the_name); // leaked
    }
};
} } } } }

namespace com { namespace sun { namespace star { namespace linguistic2 {

inline ::css::uno::Type const & cppu_detail_getUnoType(SAL_UNUSED_PARAMETER ::css::linguistic2::LinguServiceEvent const *) {
    return *detail::theLinguServiceEventType::get();
}

} } } }

inline ::css::uno::Type const & SAL_CALL getCppuType(SAL_UNUSED_PARAMETER ::css::linguistic2::LinguServiceEvent const *) SAL_THROW(()) {
    return ::cppu::UnoType< ::css::linguistic2::LinguServiceEvent >::get();
}

#endif // INCLUDED_COM_SUN_STAR_LINGUISTIC2_LINGUSERVICEEVENT_HPP
