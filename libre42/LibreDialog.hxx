/** 
 * @file /qt4plugin/libre42/LibreDialog.hxx
 * @date 2014
 * @copyright (c) ЗАО "Голлард"
 * @author Олег В. Карпов [OlegKarpov]
 * @brief
 */

#ifndef LIBREDIALOG_HXX_
#define LIBREDIALOG_HXX_

#include <QFileDialog>

class LibreDialog: public QFileDialog
{
		Q_OBJECT
public:
		LibreDialog(QWidget* parent=0);

Q_SIGNALS:
	void filterChanged( const QString& filter );
	void selectionChanged();

};

#endif /* LIBREDIALOG_HXX_ */
