# Locate Libre library
# This module defines LIBEV_FOUND, Libre_INCLUDE_DIR and Libre_LIBRARIES standard variables

set(Libre_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	/usr/include
	
	/usr/lib/libreoffice/sdk/lib
	/usr/lib/libreoffice/ure-link/lib
	/usr/lib/libreoffice/program
	/usr/lib/libreoffice/ure-link/lib
	
	/usr/local/lib/libreoffice/sdk/lib
	/usr/local/lib/libreoffice/ure-link/lib
	/usr/local/lib/libreoffice/program
	/usr/local/lib/libreoffice/ure-link/lib
	
	/opt/libreoffice4.2/sdk/lib
	/opt/libreoffice4.2/ure-link/lib
	/opt/libreoffice4.2/program
	/opt/libreoffice4.2/ure-link/lib

	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(Libre_INCLUDE_DIR
	NAMES udkversion.mk
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES inc include libreoffice
	PATHS ${Libre_SEARCH_PATHS}
)


###############################
find_library(Libre_VCL
	NAMES vcllo
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_VCLPLUG
	NAMES vclplug_genlo
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_TL
	NAMES tllo
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_UTL
	NAMES utllo
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_SOT
	NAMES sotlo
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_UCBHELPER
	NAMES ucbhelper4gcc3
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_BASEGFX
	NAMES basegfxlo
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_COMHELPER
	NAMES comphelper
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_I18LANG
	NAMES i18nlangtag
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

find_library(Libre_I18UTIL
	NAMES i18nutilgcc3
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice program
	PATHS ${Libre_SEARCH_PATHS}
)

######################################

find_library(Libre_SDK
	NAMES uno_cppuhelpergcc3 
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice sdk
	PATHS ${Libre_SEARCH_PATHS}
)
	
find_library(Libre_URE
	NAMES libjvmaccessgcc3.so.3
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice ure-link
	PATHS ${Libre_SEARCH_PATHS}
)

find_path(Libre_PATH
	NAMES libvcllo.so
	HINTS
	$ENV{Libre_DIR}
	${Libre_DIR}
	PATH_SUFFIXES lib lib64 libreoffice
	PATHS ${Libre_SEARCH_PATHS}
)

message("Libre_PATH=" ${Libre_PATH})
message("Libre_VCL=" ${Libre_VCL})
message("Libre_VCLPLUG=" ${Libre_VCLPLUG})
message("Libre_SDK=" ${Libre_SDK})
message("Libre_URE=" ${Libre_URE})
message("Libre_TL=" ${Libre_TL})
message("Libre_UTL=" ${Libre_UTL})
message("Libre_SOT=" ${Libre_SOT})
message("Libre_UCBHELPER=" ${Libre_UCBHELPER})
message("Libre_BASEGFX=" ${Libre_BASEGFX})
message("Libre_COMHELPER=" ${Libre_COMHELPER})
message("Libre_I18LANG=" ${Libre_I18LANG})
message("Libre_I18UTIL=" ${Libre_I18UTIL})

if(	#Libre_SDK AND
	#Libre_URE AND
	#Libre_TL AND
	#Libre_UTL AND
	#Libre_SOT AND
	#Libre_UCBHELPER AND
	#Libre_BASEGFX AND
	#Libre_COMHELPER AND
	#Libre_I18LANG AND
	#Libre_I18UTIL AND
	Libre_VCL AND
	Libre_VCLPLUG
	)
	
    set(Libre_LIBRARIES 
	#${Libre_SDK} 
	#${Libre_URE}
	#${Libre_TL}
	#${Libre_UTL}
	#${Libre_SOT}
	#${Libre_UCBHELPER}
	#${Libre_BASEGFX}
	#${Libre_COMHELPER}
	#${Libre_I18LANG}
	#${Libre_I18UTIL}
	${Libre_VCL}
	${Libre_VCLPLUG}

    )		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set Libre_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(Libre DEFAULT_MSG Libre_LIBRARIES Libre_INCLUDE_DIR)
