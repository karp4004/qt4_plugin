# Locate LibAvfilter library
# This module defines LibAvfilter_FOUND, LibAvfilter_INCLUDE_DIR and LibAvfilter_LIBRARIES standard variables

set(LibAvfilter_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr/local/include
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LibAvfilter_INCLUDE_DIR
	NAMES avfilter.h
	HINTS
	$ENV{LibAvfilter_DIR}
	${LibAvfilter_DIR}
	PATH_SUFFIXES inc include libavfilter
	PATHS ${LibAvfilter_SEARCH_PATHS}
)

find_library(LibAvfilter_LIBRARY
	NAMES avfilter avfilter
	HINTS
	$ENV{LibAvfilter_DIR}
	${LibAvfilter_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LibAvfilter_SEARCH_PATHS}
)
	
if(LibAvfilter_LIBRARY)
    set(LibAvfilter_LIBRARIES "${LibAvfilter_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LibAvfilter_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LibAvfilter DEFAULT_MSG LibAvfilter_LIBRARIES LibAvfilter_INCLUDE_DIR)
