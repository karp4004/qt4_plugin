# Locate mysql library
# This module defines MYSQL_FOUND, MYSQL_INCLUDE_DIR and MYSQL_LIBRARIES standard variables

set(MYSQL_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(MYSQL_INCLUDE_DIR
	NAMES mysql.h
	HINTS
	$ENV{MYSQL_DIR}
	${MYSQL_DIR}
	PATH_SUFFIXES inc/mysql5/mysql inc/mysql include/mysql5/mysql include/mysql
	PATHS ${MYSQL_SEARCH_PATHS}
)

find_library(MYSQL_LIBRARY
	NAMES mysqlclient libmysql
	HINTS
	$ENV{MYSQL_DIR}
	${MYSQL_DIR}
	PATH_SUFFIXES lib/mysql5/mysql lib/mysql lib64/mysql5/mysql lib64/mysql
	PATHS ${MYSQL_SEARCH_PATHS}
)
	
if(MYSQL_LIBRARY)
    set(MYSQL_LIBRARIES "${MYSQL_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()

# handle the QUIETLY and REQUIRED arguments and set MYSQL_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(mysql DEFAULT_MSG MYSQL_LIBRARIES MYSQL_INCLUDE_DIR)
