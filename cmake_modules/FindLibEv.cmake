# Locate LIBEV library
# This module defines LIBEV_FOUND, LIBEV_INCLUDE_DIR and LIBEV_LIBRARIES standard variables

set(LIBEV_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LIBEV_INCLUDE_DIR
	NAMES ev.h
	HINTS
	$ENV{LIBEV_DIR}
	${LIBEV_DIR}
	PATH_SUFFIXES inc include libev
	PATHS ${LIBEV_SEARCH_PATHS}
)

find_library(LIBEV_LIBRARY
	NAMES ev
	HINTS
	$ENV{LIBEV_DIR}
	${LIBEV_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LIBEV_SEARCH_PATHS}
)
	
if(LIBEV_LIBRARY)
    set(LIBEV_LIBRARIES "${LIBEV_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LIBEV_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(libev DEFAULT_MSG LIBEV_LIBRARIES LIBEV_INCLUDE_DIR)
