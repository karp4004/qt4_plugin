# Locate LibSwscale library
# This module defines LibSwscale_FOUND, LibSwscale_INCLUDE_DIR and LibSwscale_LIBRARIES standard variables

set(LibSwscale_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr/local/include
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LibSwscale_INCLUDE_DIR
	NAMES swscale.h
	HINTS
	$ENV{LibSwscale_DIR}
	${LibSwscale_DIR}
	PATH_SUFFIXES inc include libswscale
	PATHS ${LibSwscale_SEARCH_PATHS}
)

find_library(LibSwscale_LIBRARY
	NAMES Swscale swscale
	HINTS
	$ENV{LibSwscale_DIR}
	${LibSwscale_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LibSwscale_SEARCH_PATHS}
)
	
if(LibSwscale_LIBRARY)
    set(LibSwscale_LIBRARIES "${LibSwscale_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LibSwscale_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LibSwscale DEFAULT_MSG LibSwscale_LIBRARIES LibSwscale_INCLUDE_DIR)
