# Locate odbc library
# This module defines ODBC_FOUND, ODBC_INCLUDE_DIR and ODBC_LIBRARIES standard variables

set(ODBC_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(ODBC_INCLUDE_DIR
	NAMES sql.h
	HINTS
	$ENV{ODBC_DIR}
	${ODBC_DIR}
	PATH_SUFFIXES inc include
	PATHS ${ODBC_SEARCH_PATHS}
)

if(APPLE) # for DarwinPorts
	find_library(ODBC_LIBRARY
		NAMES odbc odbc32
		HINTS
		$ENV{ODBC_DIR}
		${ODBC_DIR}
		PATH_SUFFIXES lib lib64
		PATHS ${ODBC_SEARCH_PATHS}
		NO_DEFAULT_PATH
	)
endif()

find_library(ODBC_LIBRARY
	NAMES odbc odbc32
	HINTS
	$ENV{ODBC_DIR}
	${ODBC_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${ODBC_SEARCH_PATHS}
)
	
if(ODBC_LIBRARY)
    set(ODBC_LIBRARIES "${ODBC_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()

# handle the QUIETLY and REQUIRED arguments and set ODBC_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(odbc DEFAULT_MSG ODBC_LIBRARIES ODBC_INCLUDE_DIR)
