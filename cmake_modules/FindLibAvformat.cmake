# Locate LibAvformat library
# This module defines LibAvformat_FOUND, LibAvformat_INCLUDE_DIR and LibAvformat_LIBRARIES standard variables

set(LibAvformat_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr/local/include
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LibAvformat_INCLUDE_DIR
	NAMES avformat.h
	HINTS
	$ENV{LibAvformat_DIR}
	${LibAvformat_DIR}
	PATH_SUFFIXES inc include libavformat
	PATHS ${LibAvformat_SEARCH_PATHS}
)

find_library(LibAvformat_LIBRARY
	NAMES avformat avformat
	HINTS
	$ENV{LibAvformat_DIR}
	${LibAvformat_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LibAvformat_SEARCH_PATHS}
)
	
if(LibAvformat_LIBRARY)
    set(LibAvformat_LIBRARIES "${LibAvformat_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LibAvformat_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LibAvformat DEFAULT_MSG LibAvformat_LIBRARIES LibAvformat_INCLUDE_DIR)
