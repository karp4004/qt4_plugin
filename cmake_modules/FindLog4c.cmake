# Locate log4c library
# This module defines LOG4C_FOUND, LOG4C_INCLUDE_DIR and LOG4C_LIBRARIES standard variables

set(LOG4C_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LOG4C_INCLUDE_DIR
	NAMES log4c.h
	HINTS
	$ENV{LOG4C_DIR}
	${LOG4C_DIR}
	PATH_SUFFIXES inc/mysql5/mysql inc/mysql include/mysql5/mysql include/mysql
	PATHS ${LOG4C_SEARCH_PATHS}
)

find_library(LOG4C_LIBRARY
	NAMES log4c
	HINTS
	$ENV{LOG4C_DIR}
	${LOG4C_DIR}
	PATH_SUFFIXES lib/mysql5/mysql lib/mysql lib64/mysql5/mysql lib64/mysql
	PATHS ${LOG4C_SEARCH_PATHS}
)
	
if(LOG4C_LIBRARY)
    set(LOG4C_LIBRARIES "${LOG4C_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()

# handle the QUIETLY and REQUIRED arguments and set LOG4C_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(log4c DEFAULT_MSG LOG4C_LIBRARIES LOG4C_INCLUDE_DIR)
