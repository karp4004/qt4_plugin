# Locate LIBSDL library
# This module defines LIBSDL_FOUND, LIBSDL_INCLUDE_DIR and LIBSDL_LIBRARIES standard variables

set(LIBSDL_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LIBSDL_INCLUDE_DIR
	NAMES SDL.h
	HINTS
	$ENV{LIBSDL_DIR}
	${LIBSDL_DIR}
	PATH_SUFFIXES inc include SDL
	PATHS ${LIBSDL_SEARCH_PATHS}
)

find_library(LIBSDL_LIBRARY
	NAMES SDL
	HINTS
	$ENV{LIBSDL_DIR}
	${LIBSDL_DIR}
	PATH_SUFFIXES lib x86_x64-linux-gnu
	PATHS ${LIBSDL_SEARCH_PATHS}
)
	
if(LIBSDL_LIBRARY)
    set(LIBSDL_LIBRARIES "${LIBSDL_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LIBSDL_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LIBSDL DEFAULT_MSG LIBSDL_LIBRARIES LIBSDL_INCLUDE_DIR)
