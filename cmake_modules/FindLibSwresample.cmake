# Locate LibSwresample library
# This module defines LibSwresample_FOUND, LibSwresample_INCLUDE_DIR and LibSwresample_LIBRARIES standard variables

set(LibSwresample_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr/local/include
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LibSwresample_INCLUDE_DIR
	NAMES swresample.h
	HINTS
	$ENV{LibSwresample_DIR}
	${LibSwresample_DIR}
	PATH_SUFFIXES inc include libswresample
	PATHS ${LibSwresample_SEARCH_PATHS}
)

find_library(LibSwresample_LIBRARY
	NAMES swresample
	HINTS
	$ENV{LibSwresample_DIR}
	${LibSwresample_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LibSwresample_SEARCH_PATHS}
)
	
if(LibSwresample_LIBRARY)
    set(LibSwresample_LIBRARIES "${LibSwresample_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LibSwresample_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LibSwresample DEFAULT_MSG LibSwresample_LIBRARIES LibSwresample_INCLUDE_DIR)
