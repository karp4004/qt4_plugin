# Locate oracle library
# This module defines ORACLE_FOUND, ORACLE_INCLUDE_DIR and ORACLE_LIBRARIES standard variables

set(ORACLE_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(ORACLE_INCLUDE_DIR
	NAMES oci.h
	HINTS
	$ENV{ORACLE_DIR}
	${ORACLE_DIR}
	$ENV{ORACLE_HOME}
	${ORACLE_HOME}
	PATH_SUFFIXES include sdk/include oci/include rdbms/public
	PATHS ${ORACLE_SEARCH_PATHS}
)

find_library(ORACLE_LIBRARY_OCCI
	NAMES libocci occi oraocci10
	HINTS
	$ENV{ORACLE_DIR}
	${ORACLE_DIR}
	$ENV{ORACLE_HOME}
	${ORACLE_HOME}
	PATH_SUFFIXES lib sdk/lib/msvc oci/lib/msvc lib64 sdk/lib64/msvc oci/lib64/msvc
	PATHS ${ORACLE_SEARCH_PATHS}
)

find_library(ORACLE_LIBRARY_CLNTSH
	NAMES libclntsh clntsh oci
	HINTS
	$ENV{ORACLE_DIR}
	${ORACLE_DIR}
	$ENV{ORACLE_HOME}
	${ORACLE_HOME}
	PATH_SUFFIXES lib sdk/lib/msvc oci/lib/msvc lib64 sdk/lib64/msvc oci/lib64/msvc
	PATHS ${ORACLE_SEARCH_PATHS}
)

find_library(ORACLE_LIBRARY_LNNZ
	NAMES libnnz10 nnz10 libnnz11 nnz11 ociw32
	HINTS
	$ENV{ORACLE_DIR}
	${ORACLE_DIR}
	$ENV{ORACLE_HOME}
	${ORACLE_HOME}
	PATH_SUFFIXES lib sdk/lib/msvc oci/lib/msvc lib64 sdk/lib64/msvc oci/lib64/msvc
	PATHS ${ORACLE_SEARCH_PATHS}
)
	
if(ORACLE_LIBRARY_OCCI AND ORACLE_LIBRARY_CLNTSH AND ORACLE_LIBRARY_LNNZ)
    set(ORACLE_LIBRARIES
    	${ORACLE_LIBRARY_OCCI}
    	${ORACLE_LIBRARY_CLNTSH}
    	${ORACLE_LIBRARY_LNNZ}
    )
endif()

# handle the QUIETLY and REQUIRED arguments and set ORACLE_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(oracle DEFAULT_MSG ORACLE_LIBRARIES ORACLE_INCLUDE_DIR)
