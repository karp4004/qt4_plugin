# Locate LibAvutil library
# This module defines LibAvutil_FOUND, LibAvutil_INCLUDE_DIR and LibAvutil_LIBRARIES standard variables

set(LibAvutil_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr/local/include
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LibAvutil_INCLUDE_DIR
	NAMES avutil.h
	HINTS
	$ENV{LibAvutil_DIR}
	${LibAvutil_DIR}
	PATH_SUFFIXES inc include libavutil
	PATHS ${LibAvutil_SEARCH_PATHS}
)

find_library(LibAvutil_LIBRARY
	NAMES avutil avutil
	HINTS
	$ENV{LibAvutil_DIR}
	${LibAvutil_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LibAvutil_SEARCH_PATHS}
)
	
if(LibAvutil_LIBRARY)
    set(LibAvutil_LIBRARIES "${LibAvutil_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LibAvutil_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LibAvutil DEFAULT_MSG LibAvutil_LIBRARIES LibAvutil_INCLUDE_DIR)
