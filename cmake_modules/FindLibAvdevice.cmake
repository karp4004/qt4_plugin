# Locate LibAvdevice library
# This module defines LibAvdevice_FOUND, LibAvdevice_INCLUDE_DIR and LibAvdevice_LIBRARIES standard variables

set(LibAvdevice_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr/local/include
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LibAvdevice_INCLUDE_DIR
	NAMES avdevice.h
	HINTS
	$ENV{LibAvdevice_DIR}
	${LibAvdevice_DIR}
	PATH_SUFFIXES inc include libavdevice
	PATHS ${LibAvdevice_SEARCH_PATHS}
)

find_library(LibAvdevice_LIBRARY
	NAMES avdevice avdevice
	HINTS
	$ENV{LibAvdevice_DIR}
	${LibAvdevice_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LibAvdevice_SEARCH_PATHS}
)
	
if(LibAvdevice_LIBRARY)
    set(LibAvdevice_LIBRARIES "${LibAvdevice_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LibAvdevice_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LibAvdevice DEFAULT_MSG LibAvdevice_LIBRARIES LibAvdevice_INCLUDE_DIR)
