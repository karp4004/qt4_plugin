# Locate libuuid library
# This module defines UUID_FOUND, UUID_INCLUDE_DIR and UUID_LIBRARIES standard variables

set(UUID_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(UUID_INCLUDE_DIR
	NAMES uuid/uuid.h
	HINTS
	$ENV{UUID_DIR}
	${UUID_DIR}
	PATH_SUFFIXES inc include
	PATHS ${UUID_SEARCH_PATHS}
)

find_library(UUID_LIBRARY
	NAMES uuid
	HINTS
	$ENV{UUID_DIR}
	${UUID_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${UUID_SEARCH_PATHS}
)
	
if(UUID_LIBRARY)
    set(UUID_LIBRARIES "${UUID_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set UUID_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(uuid DEFAULT_MSG UUID_LIBRARIES UUID_INCLUDE_DIR)
