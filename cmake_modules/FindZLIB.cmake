# Locate ZLIB library
# This module defines ZLIB_FOUND, ZLIB_INCLUDE_DIR and ZLIB_LIBRARIES standard variables

set(ZLIB_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(ZLIB_INCLUDE_DIR
	NAMES zlib.h
	HINTS
	$ENV{ZLIB_DIR}
	${ZLIB_DIR}
	PATH_SUFFIXES inc include
	PATHS ${ZLIB_SEARCH_PATHS}
)

find_library(ZLIB_LIBRARY
	NAMES z
	HINTS
	$ENV{ZLIB_DIR}
	${ZLIB_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${ZLIB_SEARCH_PATHS}
)
	
if(ZLIB_LIBRARY)
    set(ZLIB_LIBRARIES "${ZLIB_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set ZLIB_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(ZLIB DEFAULT_MSG ZLIB_LIBRARIES ZLIB_INCLUDE_DIR)
