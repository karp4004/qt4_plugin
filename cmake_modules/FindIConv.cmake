# Locate iconv library
# This module defines ICONV_FOUND, ICONV_INCLUDE_DIR and ICONV_LIBRARIES standard variables
# Also ICONV_SECOND_ARGUMENT_IS_CONST is set when second argument of iconv function is const.

set(ICONV_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(ICONV_INCLUDE_DIR
	NAMES iconv.h
	HINTS
	$ENV{ICONV_DIR}
	${ICONV_DIR}
	PATH_SUFFIXES inc include
	PATHS ${ICONV_SEARCH_PATHS}
)

find_library(ICONV_LIBRARY
	NAMES iconv libiconv libiconv-2 c
	HINTS
	$ENV{ICONV_DIR}
	${ICONV_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${ICONV_SEARCH_PATHS}
)
	
if(ICONV_LIBRARY)
    set(ICONV_LIBRARIES "${ICONV_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()

if(ICONV_INCLUDE_DIR AND ICONV_LIBRARIES)
	include(CheckCSourceCompiles)
	set(CMAKE_REQUIRED_INCLUDES ${ICONV_INCLUDE_DIR})
	set(CMAKE_REQUIRED_LIBRARIES ${ICONV_LIBRARIES})
	check_c_source_compiles("
		#include <iconv.h>
		int main(){
			iconv_t conv = 0;
			const char* in = 0;
			size_t ilen = 0;
			char* out = 0;
			size_t olen = 0;
			iconv(conv, &in, &ilen, &out, &olen);
			return 0;
		}
	" ICONV_SECOND_ARGUMENT_IS_CONST)
endif()
	
# handle the QUIETLY and REQUIRED arguments and set ICONV_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(iconv DEFAULT_MSG ICONV_LIBRARIES ICONV_INCLUDE_DIR)
