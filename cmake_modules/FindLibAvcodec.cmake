# Locate LibAvcodec library
# This module defines LibAvcodec_FOUND, LibAvcodec_INCLUDE_DIR and LibAvcodec_LIBRARIES standard variables

set(LibAvcodec_SEARCH_PATHS
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
	/usr/local
	/usr/local/include
	/usr
	/usr/include
	~/Library/Frameworks
	/Library/Frameworks
)
	
find_path(LibAvcodec_INCLUDE_DIR
	NAMES avcodec.h
	HINTS
	$ENV{LibAvcodec_DIR}
	${LibAvcodec_DIR}
	PATH_SUFFIXES inc include libavcodec
	PATHS ${LibAvcodec_SEARCH_PATHS}
)

find_library(LibAvcodec_LIBRARY
	NAMES avcodec avutil
	HINTS
	$ENV{LibAvcodec_DIR}
	${LibAvcodec_DIR}
	PATH_SUFFIXES lib lib64
	PATHS ${LibAvcodec_SEARCH_PATHS}
)
	
if(LibAvcodec_LIBRARY)
    set(LibAvcodec_LIBRARIES "${LibAvcodec_LIBRARY}")		# Could add "general" keyword, but it is optional
endif()
	
# handle the QUIETLY and REQUIRED arguments and set LibAvcodec_FOUND to TRUE if all listed variables are TRUE
include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(LibAvcodec DEFAULT_MSG LibAvcodec_LIBRARIES LibAvcodec_INCLUDE_DIR)
